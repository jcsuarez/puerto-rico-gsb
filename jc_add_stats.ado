capture: program drop jc_add_stats 
program jc_add_stats 
syntax , ene(real) [stat(string) ]
tempname temp_b temp_V

local N = r(df) 
if `ene' != 0 { 
	local N = `ene'
	} 

mat `temp_b' = r(estimate)
mat `temp_V' =  r(se)^2
local `stat'_EST = round(r(estimate),.001) 
local `stat'_SE = round(r(se),.001) 
local `stat'_pval = 2*ttail(`N',abs(r(estimate)/r(se)))

local `stat'_EST: di %4.3f ``stat'_EST'
local `stat'_SE: di %4.3f ``stat'_SE'
local `stat'_pval: di %4.3f ``stat'_pval'

/*
if (``stat'_pval' <= 0.01)  {
	local star "\sym{***}" 
	}	
	else if  (``stat'_pval'  > 0.01 & ``stat'_pval'  <= 0.05  ) {
	local star "\sym{**}" 
	}	
	else if (``stat'_pval' > 0.05 & ``stat'_pval'  <= 0.1  ) {
	local star "\sym{*}" 
	} 	
	else  {
	local star "" 
	}
local `stat'_EST "``stat'_EST'`star'"
*/
local `stat'_SE  "(``stat'_SE')"
*di "`stat'" 
*di "``stat'_EST'"
*di "``stat'_SE'"

estadd local `stat' = "``stat'_EST'"
estadd local `stat'_SE = "``stat'_SE'"
estadd local `stat'_pval = "``stat'_pval'"
end
