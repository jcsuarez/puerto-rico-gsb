* 06/2016
* Extract QCEW files for PR project
* extrac_naic3 keeps everything at 3 digit NAIC level

clear all
set more off
*cd "/Users/mattpanhans/Dropbox (JC's Data Emporium)/personal_income_employment_data/QCEW"
*cd "/Volumes/Seagate Backup Plus/JC RA Work/QCEW/PR" 

local i = 1
tempfile inter
foreach year of numlist 1990/2012 {
di `year'

import delimited "$data/QCEW/raw_annual/`year'.annual.singlefile.csv", clear rowrange(1) colrange (1:12)

gen naic3 = 0
replace naic3 = 3 if strlen(industry_code) == 3
replace naic3 = 2 if strlen(industry_code) == 2
keep if naic3 == 2 | naic3 == 3

destring(area_fips), replace force

gen fips_state = floor(area_fips/1000)
gen fips_county = area_fips - fips_state*1000
drop if area_fips == .

drop disclosure_code

if `i' == 1 save `inter', replace
	
if `i' > 1 {
	append using `inter'
	save  `inter', replace
}
	
	local i = `i'+1

}

drop if fips_county == .
save "$data/QCEW/extract_qcew_1990_2012_naic3.dta", replace


