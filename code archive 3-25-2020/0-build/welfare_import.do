clear
capture log close
set more off
capture close
***************************************************************
//Author: dgg
//Date: 20180427
//Task: import welfare transfers data
***************************************************************
snapshot erase _all

/* ADH makes a table using a data set with the following:
1- total transfers 
2- TAA benefits (don't actually make sense here)
3- Unemployment benefits
4- SSA Retirement benefits
5- SSA disability Benefits
6- Medical Benefits Federal Income Assist
7- federal income assist
8- educ/ training assist

I suggest the following categories given our data so we don't have to make assumptions on reweighting from state data
1- Current transfer receipts of individuals from governments
2- Unemployment insurance compensation
3- Retirement and disability insurance benefits
4- Medical benefits
5- Income maintenance benefits
6- Veterans' benefits
7- Education and training assistance 5/
*/

** importing data on total government transfers
import delimited "$data/BEA_CA35/CA35_1969_2016__ALL_AREAS.csv", clear varn(1) rowr(1:70356)

replace description = ltrim(description)
replace description = rtrim(description)
keep if description == "Current transfer receipts of individuals from governments"
destring v8-v55, replace ignore("(NA)" "(L)")
reshape long v, i(geofips) j(year)
rename v trans_tot

tempfile tran1
save "`tran1'", replace

** importing data on unemployment
import delimited "$data/BEA_CA35/CA35_1969_2016__ALL_AREAS.csv", clear varn(1) rowr(1:70356)

replace description = ltrim(description)
replace description = rtrim(description)
keep if description == "Unemployment insurance compensation" 
destring v8-v55, replace ignore("(NA)" "(L)")
reshape long v, i(geofips) j(year)
rename v trans_unemp

tempfile tran2
save "`tran2'", replace

** importing data on retirement and disability
import delimited "$data/BEA_CA35/CA35_1969_2016__ALL_AREAS.csv", clear varn(1) rowr(1:70356)

replace description = ltrim(description)
replace description = rtrim(description)
keep if description == "Retirement and disability insurance benefits" 
destring v8-v55, replace ignore("(NA)" "(L)")
reshape long v, i(geofips) j(year)
rename v trans_SSA

tempfile tran3
save "`tran3'", replace

** importing data on medical benefits
import delimited "$data/BEA_CA35/CA35_1969_2016__ALL_AREAS.csv", clear varn(1) rowr(1:70356)

replace description = ltrim(description)
replace description = rtrim(description)
keep if description == "Medical benefits" 
destring v8-v55, replace ignore("(NA)" "(L)")
reshape long v, i(geofips) j(year)
rename v trans_medical

tempfile tran4
save "`tran4'", replace

import delimited "$data/BEA_CA35/CA35_1969_2016__ALL_AREAS.csv", clear varn(1) rowr(1:70356)

replace description = ltrim(description)
replace description = rtrim(description)
keep if description == "Medicare benefits" 
destring v8-v55, replace ignore("(NA)" "(L)")
reshape long v, i(geofips) j(year)
rename v trans_medicare

tempfile tran4a
save "`tran4a'", replace
import delimited "$data/BEA_CA35/CA35_1969_2016__ALL_AREAS.csv", clear varn(1) rowr(1:70356)

replace description = ltrim(description)
replace description = rtrim(description)
keep if description == "Public assistance medical care benefits 2/" 
destring v8-v55, replace ignore("(NA)" "(L)")
reshape long v, i(geofips) j(year)
rename v trans_public_med

tempfile tran4b
save "`tran4b'", replace





** importing data on income replacement
import delimited "$data/BEA_CA35/CA35_1969_2016__ALL_AREAS.csv", clear varn(1) rowr(1:70356)

replace description = ltrim(description)
replace description = rtrim(description)
keep if description == "Income maintenance benefits" 
destring v8-v55, replace ignore("(NA)" "(L)")
reshape long v, i(geofips) j(year)
rename v trans_income

tempfile tran5
save "`tran5'", replace

** importing data on veterans benefits
import delimited "$data/BEA_CA35/CA35_1969_2016__ALL_AREAS.csv", clear varn(1) rowr(1:70356)

replace description = ltrim(description)
replace description = rtrim(description)
keep if description == "Veterans' benefits" 
destring v8-v55, replace ignore("(NA)" "(L)")
reshape long v, i(geofips) j(year)
rename v trans_vet

tempfile tran6
save "`tran6'", replace

** importing data on education
import delimited "$data/BEA_CA35/CA35_1969_2016__ALL_AREAS.csv", clear varn(1) rowr(1:70356)

replace description = ltrim(description)
replace description = rtrim(description)
keep if description == "Education and training assistance 5/" 
destring v8-v55, replace ignore("(NA)" "(L)")
reshape long v, i(geofips) j(year)
rename v trans_educ

merge 1:1 geofips year using "`tran1'"
drop _merge
merge 1:1 geofips year using "`tran2'"
drop _merge
merge 1:1 geofips year using "`tran3'"
drop _merge
merge 1:1 geofips year using "`tran4'"
drop _merge
merge 1:1 geofips year using "`tran4a'"
drop _merge
merge 1:1 geofips year using "`tran4b'"
drop _merge
merge 1:1 geofips year using "`tran5'"
drop _merge
merge 1:1 geofips year using "`tran6'"
drop _merge

drop geoname region table linecode description industryclassification

** renaming years
replace year = year + 1961

rename geofips pid
merge 1:1 pid year using "$additional/county population estimates/county_pop_ests.dta"
keep if _merge == 3
drop _merge

save "$data/BEA_CA35/tansfers_clean", replace


