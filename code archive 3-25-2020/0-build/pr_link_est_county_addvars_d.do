clear
set more off

use "$output_NETS/pr_link_est_county_d.dta", clear
rename fips95 fips_county

merge 1:1 fips_county using "$additional/county_level_data_all.dta"

drop _merge
rename fips_county fips

merge 1:1 fips using "$data/QCEW/extract_qcew_pharm.dta"
rename fips fips_county
drop _merge

save "$output_NETS/pr_link_est_county_addvars_d.dta", replace

**** Including employment link
use "$output_NETS/pr_link_est_county_emp.dta", clear
rename fips95 fips_county

merge 1:1 fips_county using "$additional/county_level_data_all.dta"

drop _merge
rename fips_county fips

merge 1:1 fips using "$data/QCEW/extract_qcew_pharm.dta"
rename fips fips_county
drop _merge

save "$output_NETS/pr_link_est_county_addvars_emp.dta", replace
