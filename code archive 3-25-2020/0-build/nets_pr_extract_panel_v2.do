clear
set more off
***************************************************************
//Filename: nets_pr_extract.do
//Author: mtp
//Date: 14 May 2016
//Task: Extract firms with an establishment in PR
//Updated: 12 February 2018 by dan
***************************************************************
* NOTE: _panel_v2.do creates panel of all firms that ever existed and had presence in PR

***Change working directory:
*import delimited "$netspath/NETS2012Basic.txt", clear rowrange(1:10000) colrange(1) varnames(1) delimiter(",")
*import delimited "$netspath/NETS2012Company.txt", clear rowrange(1:10000) colrange(1) varnames(1) delimiter(",")

// Feb 12, 2018 update (dan):
//					(1) Dropped the navy, hqduns95 == 161906193
// 					(2) Dropped the rest of the government, hqduns95 == 3261245
//					(3) Snapshot of PR presence in 1995 

/*
- start with file of all establishments as obs: locations by year, all hqduns by year.
- in the end I want all hqduns by year that have an establishment in PR

1. for each est, keep if it is in the PR in any year
2. keep hqduns for those years that the establishment is in the PR
3. flag those that change over time. Otherwise just keep hqduns
4. this leaves a list of hqduns that have a PR presence, as well as flags of those that change over time, but are associated with est in PR at some point

5. assume we fix the changing hqduns issue. only left with hqduns that have PR presence
6. merge to keep only establishments associated with those firms
7. drop those that are only ever in the PR
8. collapse to firm level. keep sales, num_est, and emp.



*/
*Q: duns 1001626. HQ duns changes in 1991, and then back to original number in 1997. How to treat them?


////////////////////////////////////////////////////////////////
/////// 2. PR Panel of Firms
////////////////////////////////////////////////////////////////
**** 1 - Create list of all hqduns that have an establishment in PR
forvalues v = 1/6 {
local start = 1 + 10000000*(`v'-1)
local end = 10000000*`v'
di `start'
di `end'

import delimited "$netspath/NETS2012Basic.txt", clear rowrange(`start':`end') colrange(1:126) varnames(1) delimiter(",")
keep dunsnumber fips* hqduns*

egen min_fips = rowmin(fips*)
egen max_fips = rowmax(fips*)
gen mover = 0
replace mover = 1 if min_fips != max_fips

gen fipsstate = floor(fipscounty/1000)

gen pr_presence = 0
replace pr_presence = 1 if fipsstate == 72
drop if pr_presence == 0 & mover == 0
drop if pr_presence == 0 & mover == 1 & max_fips < 72000
drop min_fips max_fips

egen long min_hqduns = rowmin(hqduns*)
egen long max_hqduns = rowmax(hqduns*)
gen change_hq = 0
replace change_hq = 1 if min_hqduns != max_hqduns

gen fips_hq = fipsstate if dunsnumber == min_hqduns

duplicates drop

save "$output_NETS2/pr_extract_panel_v2_hq`v'.dta", replace

}
	
** Append HQ files
use "$output_NETS2/pr_extract_panel_v2_hq1.dta", clear

forvalues v = 2/6 {
	append using "$output_NETS2/pr_extract_panel_v2_hq`v'.dta"
}

* for now, drop obs that change hq or move // 86251/89002
di _N
*drop if mover == 1
di _N
*drop if change_hq == 1
di _N

*keep min_hqduns fips_hq mover change_hq
// CHANGED TO hqduns95
keep hqduns95 fips_hq
rename hqduns95 hqduns
format hqduns %11.0f

bysort hqduns: egen temp = max(fips_hq)
replace fips_hq = temp if fips_hq == .
drop temp
duplicates drop

save "$output_NETS2/pr_extract_hq_panel_master_v2.dta", replace	// List of HQ's of firms with PR presence in any year








**** 2- 
// Want: Firms that existed before 1996 and that had an establishment in PR but that were based in US  (i.e., drop only PR firms)

** Import NETS data (~52.5 million observations total)
use "$output_NETS2/pr_extract_hq_panel_master_v2.dta", clear
*drop if hqduns == 72
tempfile hq_panel_master
save `hq_panel_master'

forvalues v = 1/6 {
local start = 1 + 10000000*(`v'-1)
local end = 10000000*`v'
di `start'
di `end'

import delimited "$netspath/NETS2012Basic.txt", clear rowrange(`start':`end') colrange(1:185) varnames(1) delimiter(",")

keep dunsnumber cbsa citycode emp* sales* estcat* fips* firstyear foreignown hqduns* industry industrygroup lastyear naics*
egen long hqduns = rowmin(hqduns*)
drop hqduns9* hqduns0* hqduns1*

merge m:1 hqduns using `hq_panel_master'
keep if _merge == 3		// keeps all establishments linked to a firm based in US (of firms with some PR presence)

drop _merge

save "$output_NETS2/pr_extract_panel_v2_`v'.dta", replace

}

** Append Est files
use "$output_NETS2/pr_extract_panel_v2_1.dta", clear

forvalues v = 2/6 {
	append using "$output_NETS2/pr_extract_panel_v2_`v'.dta"
	
}

** Reshape. This is currently est. level dataset
drop estcat* empc* cbsa citycode
rename fips_hq hqfips

egen min_fips = rowmin(fips*)
egen max_fips = rowmax(fips*)
gen mover = 0
replace mover = 1 if min_fips != max_fips

gen fipsstate = floor(fipscounty/1000)
replace hqfips = dunsnumber if dunsnumber == hqduns

********************
*getting rid of governmental and social non-profit orgs
********************
gen naic4 = floor(naics95/100)
replace naic4 = floor(naics95/10) if naic4 <1111
replace naic4 = floor(naics95) if naic4 <1111 
gen naic3 = floor(naics95/1000)
replace naic3 = floor(naics95/100) if naic3 <111
replace naic3 = floor(naics95/10) if naic3 <111
replace naic3 = floor(naics95) if naic3 <111
gen naic2 = floor(naics95/10000)
replace naic2 = floor(naics95/1000) if naic2 <11
replace naic2 = floor(naics95/100) if naic2 <11
replace naic2 = floor(naics95/10) if naic2 <11
replace naic2 = floor(naics95) if naic2 <11

drop if naic2 == 92 | naic3 == 813
* dropping USPS and federal government
drop if hqduns == 3261245 
* this is navy, drop
drop if hqduns == 161906193


keep dunsnumber emp* sales* fipsstate hqduns mover hqfips naic*
order hqduns dunsnumber fipsstate
sort hqduns dunsnumber fipsstate

gen industry_cd = .
replace industry_cd = 1 if naic3 == 311
replace industry_cd = 2 if naic3 == 314
replace industry_cd = 3 if naic3 == 315
replace industry_cd = 4 if naic3 == 325
replace industry_cd = 5 if naic4 == 3254
replace industry_cd = 6 if naic3 == 326
replace industry_cd = 7 if naic3 == 316
replace industry_cd = 8 if naic3 == 332
replace industry_cd = 9 if naic3 == 333
replace industry_cd = 10 if naic3 == 335
*replace industry_cd = 11 if naic3 == 
replace industry_cd = 12 if industry_cd == . & (naic2 == 31 | naic2 == 32 | naic2 == 33)
replace industry_cd = 13 if naic2 == 52 | naic2 == 53
replace industry_cd = 14 if naic2 == 54 | naic2 == 55 | naic2 == 61 | naic2 == 62 | naic2 == 71 | naic2 == 72 | naic2 == 81
replace industry_cd = 15 if naic2 == 42 | naic2 == 44 | naic2 == 45
replace industry_cd = 16 if industry_cd == .


label define l_naic3 1 "Food Mfg" 2 "Textile mill products" 3 "Apparel" 4 "Chemicals" 5 "Pharmaceuticals" ///
 6 "Rubber and Plastic" 7 "Leather" 8 "Fabricated metal" 9 "Machinery" 10 "Electrical equip" ///
 11 "Instruments" 12 "Other mfg" 13 "Finance, insurance, real estate" 14 "Services" ///
 15 "Wholesale and retail" 16 "Other non-mfg", replace

label values industry_cd l_naic3

gen inPR = 0
replace inPR = 1 if fipsstate == 72

/*
foreach y in 90 91 92 93 94 95 96 97 98 99 00 01 02 03 04 05 06 07 08 09 10 11 12 {
	gen fipsstate`y' = floor(fips`y'/1000)
	gen pr_presence`y' = 1 if fipsstate`y' == 72
}
*/

foreach y in 90 91 92 93 94 95 96 97 98 99 00 01 02 03 04 05 06 07 08 09 10 11 12 {
gen num_est`y' = 1 if emp`y' != .
}

collapse (sum) emp* (sum) sales* (sum) num_est*, by(hqduns inPR)

foreach v in 00 01 02 03 04 05 06 07 08 09 10 11 12 {
	rename emp`v' emp20`v'
	rename sales`v' sales20`v'
	rename num_est`v' num_est20`v'
}
forval v = 90/99 {
	rename emp`v' emp19`v'
	rename sales`v' sales19`v'
	rename num_est`v' num_est19`v'
}

reshape long emp sales num_est, i(hqduns inPR) j(year)

reshape wide emp sales num_est, i(hqduns year) j(inPR)

rename emp0 emp_US
rename sales0 sales_US
rename num_est0 num_est_US
rename emp1 emp_PR
rename sales1 sales_PR
rename num_est1 num_est_PR

gen emp_total = emp_US + emp_PR
gen sales_total = sales_US + sales_PR
gen num_est_total = num_est_US + num_est_PR

* Drop firms only ever in PR, or not in PR that slipped through
by hqduns: egen US_presence = max(emp_US)
drop if US_presence == .
drop US_presence
by hqduns: egen PR_presence = max(emp_PR)
drop if PR_presence == .
drop PR_presence

gen temp1 = (emp_PR > 0) * (year == 1995)
by hqduns: egen PR = max(temp1)
drop temp1

bys hqduns : egen tot_emp_US = mean(emp_US)

save "$output_NETS2/pr_extract_panel_v2.dta", replace

// Here dan added a duplicate version that drops other non-mfg industries, saved as v_3
{
** Append Est files
use "$output_NETS2/pr_extract_panel_v2_1.dta", clear

forvalues v = 2/6 {
	append using "$output_NETS2/pr_extract_panel_v2_`v'.dta"
	
}

** Reshape. This is currently est. level dataset
drop estcat* empc* cbsa citycode
rename fips_hq hqfips

egen min_fips = rowmin(fips*)
egen max_fips = rowmax(fips*)
gen mover = 0
replace mover = 1 if min_fips != max_fips

gen fipsstate = floor(fipscounty/1000)
replace hqfips = dunsnumber if dunsnumber == hqduns

********************
*getting rid of governmental and social non-profit orgs
********************
gen naic4 = floor(naics95/100)
gen naic3 = floor(naics95/1000)
gen naic2 = floor(naics95/10000)

drop if naic2 == 92 | naic3 == 813
* dropping USPS and federal government
drop if hqduns == 3261245 
* this is navy, drop
drop if hqduns == 161906193


keep dunsnumber emp* sales* fipsstate hqduns mover hqfips naic*
order hqduns dunsnumber fipsstate
sort hqduns dunsnumber fipsstate

gen industry_cd = .
replace industry_cd = 1 if naic3 == 311
replace industry_cd = 2 if naic3 == 314
replace industry_cd = 3 if naic3 == 315
replace industry_cd = 4 if naic3 == 325
replace industry_cd = 5 if naic4 == 3254
replace industry_cd = 6 if naic3 == 326
replace industry_cd = 7 if naic3 == 316
replace industry_cd = 8 if naic3 == 332
replace industry_cd = 9 if naic3 == 333
replace industry_cd = 10 if naic3 == 335
*replace industry_cd = 11 if naic3 == 
replace industry_cd = 12 if industry_cd == . & (naic2 == 31 | naic2 == 32 | naic2 == 33)
replace industry_cd = 13 if naic2 == 52 | naic2 == 53
replace industry_cd = 14 if naic2 == 54 | naic2 == 55 | naic2 == 61 | naic2 == 62 | naic2 == 71 | naic2 == 72 | naic2 == 81
replace industry_cd = 15 if naic2 == 42 | naic2 == 44 | naic2 == 45
replace industry_cd = 16 if industry_cd == .


label define l_naic3 1 "Food Mfg" 2 "Textile mill products" 3 "Apparel" 4 "Chemicals" 5 "Pharmaceuticals" ///
 6 "Rubber and Plastic" 7 "Leather" 8 "Fabricated metal" 9 "Machinery" 10 "Electrical equip" ///
 11 "Instruments" 12 "Other mfg" 13 "Finance, insurance, real estate" 14 "Services" ///
 15 "Wholesale and retail" 16 "Other non-mfg", replace

label values industry_cd l_naic3
rename industry_cd industry_cd_temp
gen industry_cd_temp1 = industry_cd_temp if industry_cd_temp != 16
bys hqduns: egen industry_cd = mode(industry_cd_temp1)
replace industry_cd = 16 if industry_cd == .
keep if industry_cd < 16

gen inPR = 0
replace inPR = 1 if fipsstate == 72

/*
foreach y in 90 91 92 93 94 95 96 97 98 99 00 01 02 03 04 05 06 07 08 09 10 11 12 {
	gen fipsstate`y' = floor(fips`y'/1000)
	gen pr_presence`y' = 1 if fipsstate`y' == 72
}
*/

foreach y in 90 91 92 93 94 95 96 97 98 99 00 01 02 03 04 05 06 07 08 09 10 11 12 {
gen num_est`y' = 1 if emp`y' != .
}

collapse (sum) emp* (sum) sales* (sum) num_est*, by(hqduns inPR)

foreach v in 00 01 02 03 04 05 06 07 08 09 10 11 12 {
	rename emp`v' emp20`v'
	rename sales`v' sales20`v'
	rename num_est`v' num_est20`v'
}
forval v = 90/99 {
	rename emp`v' emp19`v'
	rename sales`v' sales19`v'
	rename num_est`v' num_est19`v'
}

reshape long emp sales num_est, i(hqduns inPR) j(year)

reshape wide emp sales num_est, i(hqduns year) j(inPR)

rename emp0 emp_US
rename sales0 sales_US
rename num_est0 num_est_US
rename emp1 emp_PR
rename sales1 sales_PR
rename num_est1 num_est_PR

gen emp_total = emp_US + emp_PR
gen sales_total = sales_US + sales_PR
gen num_est_total = num_est_US + num_est_PR

* Drop firms only ever in PR, or not in PR that slipped through
by hqduns: egen US_presence = max(emp_US)
drop if US_presence == .
drop US_presence
by hqduns: egen PR_presence = max(emp_PR)
drop if PR_presence == .
drop PR_presence

gen temp1 = (emp_PR > 0) * (year == 1995)
by hqduns: egen PR = max(temp1)
drop temp1

bys hqduns : egen tot_emp_US = mean(emp_US)

save "$output_NETS2/pr_extract_panel_v3.dta", replace
}

***** Merge in Company names
***************************************

forvalues v = 1/6 {
local start = 1 + 10000000*(`v'-1)
local end = 10000000*`v'
di `start'
di `end'

import delimited "$netspath/NETS2012Company.txt", clear rowrange(`start':`end') colrange(1) varnames(1) delimiter(",")

rename dunsnumber hqduns

merge 1:m hqduns using "$output_NETS2/pr_extract_panel_v2.dta"
drop if _merge == 1		// keeps all establishments linked to a firm based in US (of firms with some PR presence)

drop _merge

save "$output_NETS2/pr_extract_panel_v2_company_`v'.dta", replace

}

** Append Est files
use "$output_NETS2/pr_extract_panel_v2_company_1.dta", clear

forvalues v = 2/6 {
	append using "$output_NETS2/pr_extract_panel_v2_company_`v'.dta"
	
}

duplicates drop
sort hqduns year
drop if zipcode == .
save "$output_NETS2/pr_extract_panel_v2_company.dta", replace


***** Merge in other info from Basic dataset
* Check why this last part takes forever to run...
***************************************


forvalues v = 1/6 {
local start = 1 + 10000000*(`v'-1)
local end = 10000000*`v'
di `start'
di `end'

import delimited "$netspath/NETS2012Basic.txt", clear rowrange(`start':`end') colrange(1) varnames(1) delimiter(",")

rename dunsnumber hqduns
egen temp = rowmin(naics*)
drop naics*
rename temp naics6
keep hqduns yearstart lastyear legalstat fipscounty foreignown industry industrygroup naics6

merge 1:m hqduns using "$output_NETS2/pr_extract_panel_v2_company.dta"
drop if _merge == 1		// keeps all establishments linked to a firm based in US (of firms with some PR presence)

drop _merge

save "$output_NETS2/pr_extract_panel_v2_companybasic_`v'.dta", replace

}

** Append Est files
use "$output_NETS2/pr_extract_panel_v2_companybasic_1.dta", clear

forvalues v = 2/6 {
	append using "$output_NETS2/pr_extract_panel_v2_companybasic_`v'.dta"
	
}

duplicates drop
sort hqduns year
order hqduns year company emp_US sales_US num_est_US emp_PR sales_PR num_est_PR emp_total sales_total num_est_total
drop if fipscounty == .		// drops exactly half

save "$output_NETS2/pr_extract_panel_v2_companybasic.dta", replace
