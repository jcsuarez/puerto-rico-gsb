

/* 
This Code selects the 682 firms we care about and then from those selects
the actual state and insustries we care about

*/

************************* The snippet below selects the 682 firms we care about from the jobs_ts_d file
clear all
set more off
ssc install dataout
snapshot erase _all

*** get list of hqduns that actually work by dropping gov't etc... 
use "$output_NETS/pr_extract_est.dta", clear

gen naic4 = floor(naics95/100)
gen naic3 = floor(naics95/1000)
gen naic2 = floor(naics95/10000)

********************
*getting rid of governmental and social non-profit orgs
********************
drop if naic2 == 92 | naic3 == 813
* dropping USPS and the federal government
drop if hqduns95 == 3261245 | hqduns95 == 161906193

* dropping firms with HQ in PR
drop if pr_based == 1 

* dropping county industries with no link
drop if pr_link == 0
bysort hqduns: egen temp_max = max(emp95)
gen temp_keep = (emp95 == temp_max)

keep if temp_keep == 1

* making a list of hqduns to use from the panel data
keep hqduns naic3 emp95
collapse (sum) emp95 , by(hqduns naic3)

rename naic3 naic3_
rename emp95 emp95_
by hqduns: gen aye = _n 

reshape wide naic3_ emp95_ , i(hqduns) j(aye)

duplicates drop 
tempfile tokeep 
save "`tokeep'"

** old code 
use "$output_NETS/pr_extract_panel.dta", clear 

** Keep only firms at start of the sample 
gen temp = emp_PR if year == 1990
bysort hqd: egen base_emp_PR = min(temp)
drop temp 
keep if base > 0  // This line drops 4,991 observations

** MErge in to keep 
merge m:1 hqduns95 using "`tokeep'"
keep if _merge == 3 // This drops 23 observations from the master, 214 from using

egen firm = group(hqd) 
sum firm, d

** Gen naics with max employment 
gen naic3 = naic3_1
replace naic3 = naic3_2 if emp95_2>emp95_1 & emp95_2!=. & emp95_1!=. 
replace naic3 = naic3_3 if emp95_3>emp95_2 & emp95_3!=. & emp95_2!=. 

keep hqduns naic3*
duplicates drop 
tempfile PRlist 
save "`PRlist'"

************************* Get firm identifiers and then merge into pr_extrc
use "$output_NETS/pr_extract_est.dta", clear
merge m:1 hqduns95 using "`PRlist'"
keep if _merge == 3 

* Confirm number of firms
egen firm = group(hqd) 
sum firm, d
drop firm 

***** Generate hq fips 
* First option 
gen hqfips = fipsstate95 if estcat95 == "Headquarters"
bys hqduns: egen max_hqfips = max(hqfips)
replace  hqfips = max_hqfips 
drop max_hqfips

* Second option 
gen hqfips2 = fipsstate95 if dunsnumber == hqduns 
bys hqduns: egen max_hqfips = max(hqfips2)										// DAN: changed this to max(hqfips2) from max(hqfips)	 
replace  hqfips2 = max_hqfips 
drop max_hqfips

* Third option 
bys hqduns: egen max_emp = max(emp95) 
gen hqfips3 = fipsstate95 if emp95 == max_emp
bys hqduns: egen max_hqfips = max(hqfips3)
replace  hqfips3 = max_hqfips 
drop max_hqfips

* Establishmetn count 
gen est = 1 

collapse (sum) est emp95 (first) hqf* naic3*, by(hqduns)

* Replace missing hqfips with fips of estab with max employment 
replace hqfips = hqfips3 if hqfips == . 
drop hqfips2 hqfips3
* keep only max employmet naics3 
drop naic3_*

* censusdivision
gen census_div = 1 if inlist(hqfips,9,23,25,33,44,50)
replace census_div = 2 if inlist(hqfips,34,36,42)
replace census_div = 3 if inlist(hqfips,18,17,26,39,55)
replace census_div = 4 if inlist(hqfips,19,20,27,29,31,38,46)
replace census_div = 5 if inlist(hqfips,10,11,12,13,24,37,45,51,54)
replace census_div = 6 if inlist(hqfips,1,21,28,47)
replace census_div = 7 if inlist(hqfips,5,22,40,48)
replace census_div = 8 if inlist(hqfips,4,8,16,35,30,49,32,56)
replace census_div = 9 if inlist(hqfips,2,6,15,41,53)
keep if census_div!=.

gen census_reg = 1 if inlist(census_div,1,2) 
replace census_reg = 2 if inlist(census_div,3,4) 
replace census_reg = 3 if inlist(census_div,5,6,7) 
replace census_reg = 4 if inlist(census_div,8,9) 

******* Group employmnt into Quintiles
centile emp95, c(0,20,40,60,80)
/*                                                     -- Binom. Interp. --
                                                          -- Binom. Interp. --
    Variable |       Obs  Percentile    Centile        [95% Conf. Interval]
-------------+-------------------------------------------------------------
       emp95 |       470          0           5               5           5*
             |                   20       326.2        247.2749     441.635
             |                   40      1543.4        1105.499    2109.842
             |                   60      5754.8        4626.668    7545.349
             |                   80     18672.2        14574.11    23877.43

			 */

gen q_emp = 1 if inrange(emp95,5,326) 
replace q_emp = 2 if inrange(emp95,326,1543) 
replace q_emp = 3 if inrange(emp95,1543,5754) 
replace q_emp = 4 if inrange(emp95,5754,18672) 
replace q_emp = 5 if emp95>18672

******** Group establishments into Quintiles
centile est, c(0,20,40,60,80)
/*
                                                    -- Binom. Interp. --
    Variable |       Obs  Percentile    Centile        [95% Conf. Interval]
-------------+-------------------------------------------------------------
         est |       470          0           2               2           2*
             |                   20         5.2               4           7
             |                   40          20        13.77379          29
             |                   60          72              53          97
             |                   80       206.4             158    236.8626


*/
gen q_est = 1 if inrange(est,2,5) 
replace q_est = 2 if inrange(est,5,20) 
replace q_est = 3 if inrange(est,20,72) 
replace q_est = 4 if inrange(est,72,206) 
replace q_est = 5 if est>206

* Total number of groups 
egen group_f = group(hqf)
sum group_f 

egen group_naic = group(naic3)
sum group_naic 

* Total number of insutry groups and sizes: 86000 bins
di 5*5*43*80
drop group_f group_naic

* Groups from merge: only 529 
gen firm_group = census_reg*10000000+naic3*10000+q_emp*100+q_est
egen group_n_f = group(firm_group)
sum group_n_f
drop group_n_f

save "$output_NETS_control/HQ_to_match.dta", replace

preserve
keep firm_group 
duplicates drop 
save "$output_NETS_control/firm_group_size.dta", replace
restore

* State industry groups Groups from merge: only 386
gen firm_group2 = census_reg*10000000+naic3*10000
egen group_n_f2 = group(firm_group2)
sum group_n_f2
drop group_n_f2

preserve
keep firm_group2 
duplicates drop 
save "$output_NETS_control/firm_group.dta", replace
restore


/* code to check names

use "$output_NETS2/pr_extract_panel_v2_companybasic", clear
keep hqduns company
duplicates drop

tempfile names
save "`names'", replace
snapshot restore 3
rename hqduns95 hqduns

merge 1:1 hqduns using "`names'"
keep if _merge == 3 & hqfips == 72
save "$output_NETS_control/pr_based_firm_names.dta", replace
