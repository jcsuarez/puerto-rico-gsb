/* 
This code goes over all the estabs in the NETS and selects: 
1- All estabs linked to control firms (with HQs in list by industry and size) 
2- Gathers employment and establishment counts by year 
3- Collapses at the hq level 
4- merges all files and collapses again 
*/

////////////////////////////////////////////////////////////////
/////// Get Establishemtns 
////////////////////////////////////////////////////////////////

clear
set more off

*10000000

****  - Create list of all hqduns that have an establishment in PR
forvalues v = 1/6 {
local start = 1 + 10000000*(`v'-1)
local end = 10000000*`v'
di `start'
di `end'

** Import NETS data (~52.5 million observations total)
import delimited "$netspath/NETS2012Basic.txt", clear rowrange(`start':`end') colrange(1:126) varnames(1) delimiter(",")

*import delimited "$netspath/NETS2012Basic.txt", clear rowrange(1:126) colrange(1:126) varnames(1) delimiter(",")

gen fipsstate95 = floor(fips95/1000)
replace fipsstate95 = . if estcat95 != "Headquarters"

* Clean data 
keep emp* hqduns95 fipsstate95
drop empc*

* Keep only data in list
merge m:1 hqduns95 using  "$output_NETS_control/hq_est_co_master.dta"
keep if _merge == 3
drop _merge 

foreach y in 90 91 92 93 94 95 96 97 98 99 00 01 02 03 04 05 06 07 08 09 10 11 12 {
gen num_est`y' = 1 if emp`y' != .
}
collapse (sum) emp* num_est* (max) fipsstate95, by(hqduns95 firm_group )

duplicates drop 
compress

save "$output_NETS_control/hq_est_co_firm_`v'.dta", replace

}

** Append HQ files
use "$output_NETS_control/hq_est_co_firm_1.dta", clear
forvalues v = 2/6 {
	append using "$output_NETS_control/hq_est_co_firm_`v'.dta"
}
collapse (sum) emp* num_est* (max) fipsstate95, by(hqduns95 firm_group)
save "$output_NETS_control/hq_est_co_firm_master.dta", replace

** Save in Long too 

foreach v in 00 01 02 03 04 05 06 07 08 09 10 11 12 {
	rename emp`v' emp20`v'
	rename num_est`v' num_est20`v'
}
forval v = 90/99 {
	rename emp`v' emp19`v'
	rename num_est`v' num_est19`v'
}

reshape long emp  num_est, i(hqduns firm_group) j(year)
save "$output_NETS_control/hq_est_co_firm_master_long.dta", replace


*** PLot times series graph 

collapse (sum) emp  num_est , by(year) 

replace emp = emp/1000000
/*
replace emp_PR = emp_PR/10000
replace num_est_US = num_est_US/10000
replace num_est_PR = num_est_PR/100
*/
twoway (scatter emp year, connect(line) xline(1995, lcolor(black) lpattern(dash))) ///
	   (scatter emp year, connect(line) lpattern(dash) ) ///
		, plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white)) ///
		xtitle("Year") xlab(1990(5)2011)
		
		
		ylab(5(2)18) ///
		legend(label(1 "US (1,000,000)") label(2 "PR (10,000)")) ///
		ytitle("Total Employment by Firms Exposed to S 936")  ///
		note("Employment of `num' Firms Exposed to S 936.")
