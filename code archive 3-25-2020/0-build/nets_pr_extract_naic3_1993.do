clear
set more off
snapshot erase _all
***************************************************************
//Filename: nets_pr_extract_naic3_1993.do
//Author: mtp + danny g
//Date: Oct 30 2018
//Task: Extract firms with an establishment in PR in 1993 (Robustness to 2005)
***************************************************************

////////////////////////////////////////////////////////////////
/////// 1. PR Link
////////////////////////////////////////////////////////////////
di("Part 1")

**** 1 - Create list of all hqduns that have an establishment in PR
forvalues v = 1/6 {
local start = 1 + 10000000*(`v'-1)
local end = 10000000*`v'
di `start'
di `end'

** Import NETS data (~52.5 million observations total)
import delimited "$netspath/NETS2012Basic.txt", clear rowrange(`start':`end') colrange(1:126) varnames(1) delimiter(",")

gen fipsstate = floor(fipscounty/1000)
gen fipsstate93 = floor(fips93/1000)

count if fipsstate == 72
gen pr_presence = 0
replace pr_presence = 1 if fipsstate93 == 72

keep if pr_presence == 1
gen pr_based = 1 if dunsnumber == hqduns93 & fipsstate93 == 72
keep hqduns* pr_based

duplicates drop

save "$output_NETS/pr_extract_hq`v'_93.dta", replace

}
	

** Append HQ files
use "$output_NETS/pr_extract_hq1_93.dta", clear

forvalues v = 2/6 {
	append using "$output_NETS/pr_extract_hq`v'_93.dta"
}

keep hqduns93 pr_based			// pr_based == 1 if obs is an HQ and in PR. missing otherwise
drop if hqduns93 == .
duplicates drop

replace pr_based = 0 if pr_based == .
bysort hqduns93: egen temp = max(pr_based)
replace pr_based = temp
drop temp
duplicates drop

save "$output_NETS/pr_extract_hq_master_93.dta", replace	// List of HQ's which have at least 1 est. in PR



**** 2 - Keep all establishments that are part of firm on hqduns list
forvalues v = 1/6 {
local start = 1 + 10000000*(`v'-1)
local end = 10000000*`v'
di `start'
di `end'

** Import NETS data (~52.5 million observations total)
import delimited "$netspath/NETS2012Basic.txt", clear rowrange(`start':`end') colrange(1:160) varnames(1) delimiter(",")

keep dunsnumber cbsa citycode emp93 estcat93 fips93 firstyear foreignown hqduns93 industry industrygroup lastyear naics93
drop if hqduns93 == .

merge m:1 hqduns93 using "$output_NETS/pr_extract_hq_master_93.dta" 
gen pr_link = 0
replace pr_link = 1 if _merge == 3
drop _merge

save "$output_NETS/pr_extract_est`v'_93.dta", replace

}

** Append Est files
use "$output_NETS/pr_extract_est1_93.dta", clear

forvalues v = 2/6 {
	append using "$output_NETS/pr_extract_est`v'_93.dta"
	
}

gen fipsstate95 = floor(fips93/1000)

drop if fips93 == .
save "$output_NETS/pr_extract_est_93.dta", replace	// 229,000 establishments with links to PR in '96


**** 3- Output descriptives and save dataset
snapshot erase _all
use "$output_NETS/pr_extract_est_93.dta", clear

gen naic4 = floor(naics93/100)
replace naic4 = floor(naics93/10) if naic4 <1111
replace naic4 = floor(naics93) if naic4 <1111 
gen naic3 = floor(naics93/1000)
replace naic3 = floor(naics93/100) if naic3 <111
replace naic3 = floor(naics93/10) if naic3 <111
replace naic3 = floor(naics93) if naic3 <111
gen naic2 = floor(naics93/10000)
replace naic2 = floor(naics93/1000) if naic2 <11
replace naic2 = floor(naics93/100) if naic2 <11
replace naic2 = floor(naics93/10) if naic2 <11
replace naic2 = floor(naics93) if naic2 <11

drop if naic2 == 92 | naic3 == 813
* dropping USPS and federal government
drop if hqduns == 3261245 
* this is navy, drop
drop if hqduns == 161906193

********************
*getting rid of governmental and social non-profit orgs
********************
drop if naic2 == 92 | naic3 == 813
* dropping USPS and federal government
drop if hqduns93 == 3261245 
* this is navy, drop  
drop if hqduns93 == 161906193

gen industry_cd = .
replace industry_cd = 1 if naic3 == 311
replace industry_cd = 2 if naic3 == 314
replace industry_cd = 3 if naic3 == 315
replace industry_cd = 4 if naic3 == 325
replace industry_cd = 5 if naic4 == 3254
replace industry_cd = 6 if naic3 == 326
replace industry_cd = 7 if naic3 == 316
replace industry_cd = 8 if naic3 == 332
replace industry_cd = 9 if naic3 == 333
replace industry_cd = 10 if naic3 == 335
*replace industry_cd = 11 if naic3 == 
replace industry_cd = 12 if industry_cd == . & (naic2 == 31 | naic2 == 32 | naic2 == 33)
replace industry_cd = 13 if naic2 == 52 | naic2 == 53
replace industry_cd = 14 if naic2 == 54 | naic2 == 55 | naic2 == 61 | naic2 == 62 | naic2 == 71 | naic2 == 72 | naic2 == 81
replace industry_cd = 15 if naic2 == 42 | naic2 == 44 | naic2 == 45
replace industry_cd = 16 if industry_cd == .

label define l_naic3 1 "Food Mfg" 2 "Textile mill products" 3 "Apparel" 4 "Chemicals" 5 "Pharmaceuticals" ///
 6 "Rubber and Plastic" 7 "Leather" 8 "Fabricated metal" 9 "Machinery" 10 "Electrical equip" ///
 11 "Instruments" 12 "Other mfg" 13 "Finance, insurance, real estate" 14 "Services" ///
 15 "Wholesale and retail" 16 "Other non-mfg", replace

label values industry_cd l_naic3

// MANUAL DROP OF LARGE RETAILERS FROM LINKS pr_link_MD
/*
WAL-MART 51957769
WALGREEN 8965063
SEARS 1629955
RADIOSHACK 8012635
MCDONALDS 41534264
KMART 8965873
CVS CARE 1338912
*/
gen pr_link_MD = pr_link
foreach i of numlist 51957769 8965063 1629955 8012635 41534264 8965873 1338912 {
replace pr_link_MD = 0 if hqduns == `i'
}

// MANUAL DROP OF UNKNOWN HQ FIRMS FROM LINKS pr_link_FD
gen HQ = (hqduns == dunsnumber)
bys hqduns: egen has_HQ = max(HQ)

gen pr_link_FD = pr_link
replace pr_link_FD = 0 if pr_based == 1
replace pr_link_FD = 0 if pr_based == 0 & has_HQ == 0

// New PR links that only include firms with a large number of EMP95 in the US
bys hqduns: egen tot_emp = sum(emp93)

** doing things in 3 tiers
local tier1 = 20000
local tier2 = 30000
local tier3 = 40000
local tier4 = 50000

gen pr_link_s1 = pr_link * (tot_emp > `tier1')
gen pr_link_s2 = pr_link * (tot_emp > `tier2')
gen pr_link_s3 = pr_link * (tot_emp > `tier3')
gen pr_link_s4 = pr_link * (tot_emp > `tier4')

// EMPLOYMENT VARIABLES emp95 pr_emp95
gen total = 1
gen pr_emp93 = pr_link * emp93

preserve
collapse (mean) pr_link* (sum) total emp93 pr_emp93, by(fips93)
save "$output_NETS/pr_link_est_county_n3_93.dta", replace
restore

preserve
collapse (mean) pr_link* (sum) total emp93 pr_emp93, by(fips93 naic3)
tsset fips93 naic3
tsfill, full
replace pr_link = 0 if pr_link == .
replace total = 0 if total == .
save "$output_NETS/pr_link_est_countyXindustry_n3_93.dta", replace
restore 

rename fips93 cty_fips
merge m:1 cty_fips using "$xwalk/cw_cty_czone.dta"

drop if _merge == 2
drop _merge

rename cty_fips fips93
rename czone czone93

preserve
collapse (mean) pr_link* (sum) total emp93 pr_emp93, by(czone93)
save "$output_NETS/pr_link_est_czone_n3_93.dta", replace
restore

preserve
collapse (mean) pr_link* (sum) total emp93 pr_emp93, by(czone93 naic3)
tsset czone93 naic3
tsfill, full
replace pr_link = 0 if pr_link == .
replace total = 0 if total == .
save "$output_NETS/pr_link_est_czoneXindustry_n3_93.dta", replace
restore 

gen total_hq = 0
replace total_hq = 1 if dunsnumber == hqduns93

preserve
drop if total_hq == 0
rename pr_link pr_linkhq
collapse (mean) pr_link* (sum) total_hq emp93 pr_emp93, by(fips93)
save "$output_NETS/pr_link_hq_county_n3_93.dta", replace
restore

preserve
drop if total_hq == 0
rename pr_link pr_linkhq
collapse (mean) pr_link* (sum) total_hq emp93 pr_emp93, by(fips93 naic3)
save "$output_NETS/pr_link_hq_countyXindustry_n3_93.dta", replace
restore 

// saving a list of hqduns of firms that have large US employment 
preserve
keep pr_link_s? hqduns
drop if pr_link_s1 == 0
duplicates drop
save "$output_NETS/large_link_firms_93.dta", replace
restore
