* 06/2016
* Extract QCEW files for PR project

clear all
set more off
*cd "/Users/mattpanhans/Dropbox (JC's Data Emporium)/personal_income_employment_data/QCEW"
*cd "/Volumes/Seagate Backup Plus/JC RA Work/QCEW/PR" 

local i = 1
tempfile inter

foreach year of numlist 1990/2012 {
di `year'

import delimited "$data/QCEW/raw_annual/`year'.annual.singlefile.csv", clear rowrange(1) colrange (1:12)
keep if industry_code == "10" | industry_code == "31-33" | industry_code == "42" | industry_code == "44-45" | industry_code == "52" | industry_code == "53" ///
 | industry_code == "54" | industry_code == "55" | industry_code == "61" | industry_code == "62" | industry_code == "71" | industry_code == "72" ///
 | industry_code == "81" | industry_code == "311" | industry_code == "314" | industry_code == "315" | industry_code == "325" | industry_code == "3254" ///
 | industry_code == "326" | industry_code == "316" | industry_code == "332" | industry_code == "333" | industry_code == "335"

destring(area_fips), replace force

gen fips_state = floor(area_fips/1000)
gen fips_county = area_fips - fips_state*1000
drop if area_fips == .

drop disclosure_code

if `i' == 1 save `inter', replace
	
if `i' > 1 {
	append using `inter'
	save  `inter', replace
}
	
	local i = `i'+1

}

drop if fips_county == .
save "$data/QCEW/extract_qcew_1990_2012.dta", replace
