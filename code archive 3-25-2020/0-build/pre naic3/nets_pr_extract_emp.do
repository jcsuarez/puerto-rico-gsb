clear
set more off
***************************************************************
//Filename: nets_pr_extract.do
//Author: mtp
//Date: 14 May 2016
//Task: Extract firms with an establishment in PR
***************************************************************

// Feb 5, 2018 update (dan):
//					(1) changing the file to calculate PR Link as a percent of employment
//						NOTE: This is employment based off of NETS which people generally don't like
//					(2) Droped the navy, hqduns95 == 161906193

////////////////////////////////////////////////////////////////
/////// 1. PR Link
////////////////////////////////////////////////////////////////
di("Part 1")

**** 1 - Create list of all hqduns that have an establishment in PR
forvalues v = 1/6 {
local start = 1 + 10000000*(`v'-1)
local end = 10000000*`v'
di `start'
di `end'

** Import NETS data (~52.5 million observations total)
import delimited "$netspath/NETS2012Basic.txt", clear rowrange(`start':`end') colrange(1:126) varnames(1) delimiter(",")

gen fipsstate = floor(fipscounty/1000)
gen fipsstate95 = floor(fips95/1000)

count if fipsstate == 72
gen pr_presence = 0
replace pr_presence = 1 if fipsstate95 == 72

keep if pr_presence == 1
gen pr_based = 1 if dunsnumber == hqduns95 & fipsstate95 == 72
keep hqduns* pr_based

duplicates drop

save "$output_NETS/pr_extract_hq`v'.dta", replace

}
	

** Append HQ files
use "$output_NETS/pr_extract_hq1.dta", clear

forvalues v = 2/6 {
	append using "$output_NETS/pr_extract_hq`v'.dta"
}

keep hqduns95 pr_based			// pr_based == 1 if obs is an HQ and in PR. missing otherwise
drop if hqduns95 == .
duplicates drop

replace pr_based = 0 if pr_based == .
bysort hqduns95: egen temp = max(pr_based)
replace pr_based = temp
drop temp
duplicates drop

save "$output_NETS/pr_extract_hq_master.dta", replace	// List of HQ's which have at least 1 est. in PR



**** 2 - Keep all establishments that are part of firm on hqduns list
forvalues v = 1/6 {
local start = 1 + 10000000*(`v'-1)
local end = 10000000*`v'
di `start'
di `end'

** Import NETS data (~52.5 million observations total)
import delimited "$netspath/NETS2012Basic.txt", clear rowrange(`start':`end') colrange(1:160) varnames(1) delimiter(",")

keep dunsnumber cbsa citycode emp95 estcat95 fips95 firstyear foreignown hqduns95 industry industrygroup lastyear naics95
drop if hqduns95 == .

merge m:1 hqduns95 using "$output_NETS/pr_extract_hq_master.dta" //variable hqduns95 does not uniquely identify observations in the using data
gen pr_link = 0
replace pr_link = 1 if _merge == 3
drop _merge

save "$output_NETS/pr_extract_est`v'.dta", replace

}

** Append Est files
use "$output_NETS/pr_extract_est1.dta", clear

forvalues v = 2/6 {
	append using "$output_NETS/pr_extract_est`v'.dta"
	
}

gen fipsstate95 = floor(fips95/1000)

drop if fips95 == .
save "$output_NETS/pr_extract_est.dta", replace	// 229,000 establishments with links to PR in '96



**** 3- Output descriptives and save dataset
use "$output_NETS/pr_extract_est.dta", clear

gen naic4 = floor(naics95/100)
gen naic3 = floor(naics95/1000)
gen naic2 = floor(naics95/10000)

********************
*getting rid of governmental and social non-profit orgs
********************
drop if naic2 == 92 | naic3 == 813
* dropping USPS and federal government
drop if hqduns95 == 3261245 
* this is navy, drop in future  
drop if hqduns95 == 161906193

gen industry_cd = .
replace industry_cd = 1 if naic3 == 311
replace industry_cd = 2 if naic3 == 314
replace industry_cd = 3 if naic3 == 315
replace industry_cd = 4 if naic3 == 325
replace industry_cd = 5 if naic4 == 3254
replace industry_cd = 6 if naic3 == 326
replace industry_cd = 7 if naic3 == 316
replace industry_cd = 8 if naic3 == 332
replace industry_cd = 9 if naic3 == 333
replace industry_cd = 10 if naic3 == 335
*replace industry_cd = 11 if naic3 == 
replace industry_cd = 12 if industry_cd == . & (naic2 == 31 | naic2 == 32 | naic2 == 33)
replace industry_cd = 13 if naic2 == 52 | naic2 == 53
replace industry_cd = 14 if naic2 == 54 | naic2 == 55 | naic2 == 61 | naic2 == 62 | naic2 == 71 | naic2 == 72 | naic2 == 81
replace industry_cd = 15 if naic2 == 42 | naic2 == 44 | naic2 == 45
replace industry_cd = 16 if industry_cd == .


label define l_naic3 1 "Food Mfg" 2 "Textile mill products" 3 "Apparel" 4 "Chemicals" 5 "Pharmaceuticals" ///
 6 "Rubber and Plastic" 7 "Leather" 8 "Fabricated metal" 9 "Machinery" 10 "Electrical equip" ///
 11 "Instruments" 12 "Other mfg" 13 "Finance, insurance, real estate" 14 "Services" ///
 15 "Wholesale and retail" 16 "Other non-mfg", replace

label values industry_cd l_naic3
 
// 2-5-2018: Generating variables for calc of pr_link 
gen total = 1
gen pr_emp95 = pr_link * emp95

 
// 2-5-2018: New files with emp95 and pr_emp95 added are saved with "_emp" extension 
preserve
collapse (mean) pr_link (sum) total emp95 pr_emp95, by(fips95)
save "$output_NETS/pr_link_est_county_emp.dta", replace
restore

preserve
collapse (mean) pr_link (sum) total emp95 pr_emp95, by(fips95 industry_cd)
tsset fips95 industry_cd
tsfill, full
replace pr_link = 0 if pr_link == .
replace total = 0 if total == .
replace emp95 = 0 if emp95 == .
replace pr_emp95 = 0 if pr_emp95 == .
save "$output_NETS/pr_link_est_countyXindustry_emp.dta", replace
restore 

rename fips95 cty_fips
merge m:1 cty_fips using "$xwalk/cw_cty_czone.dta"

drop if _merge == 2
drop _merge

rename cty_fips fips95
rename czone czone95

preserve
collapse (mean) pr_link (sum) total emp95 pr_emp95, by(czone95)
save "$output_NETS/pr_link_est_czone_emp.dta", replace
restore

preserve
collapse (mean) pr_link (sum) total emp95 pr_emp95, by(czone95 industry_cd)
tsset czone95 industry_cd
tsfill, full
replace pr_link = 0 if pr_link == .
replace total = 0 if total == .
replace emp95 = 0 if emp95 == .
replace pr_emp95 = 0 if pr_emp95 == .
save "$output_NETS/pr_link_est_czoneXindustry_emp.dta", replace
restore 

gen total_hq = 0
replace total_hq = 1 if dunsnumber == hqduns95

preserve
drop if total_hq == 0
rename pr_link pr_linkhq
collapse (mean) pr_linkhq (sum) total_hq emp95 pr_emp95, by(fips95)
save "$output_NETS/pr_link_hq_county_emp.dta", replace
restore

preserve
drop if total_hq == 0
rename pr_link pr_linkhq
collapse (mean) pr_linkhq (sum) total_hq emp95 pr_emp95, by(fips95 industry_cd)
save "$output_NETS/pr_link_hq_countyXindustry_emp.dta", replace
restore 



////////////////////////////////////////////////////////////////
/////// 2. PR Panel of Firms
////////////////////////////////////////////////////////////////
di("Part 2")

// Want: Firms that existed before 1996 and that had an establishment in PR but that were based in US  (i.e., drop only PR firms)

use "$output_NETS/pr_extract_hq_master.dta", clear
drop if pr_based == 1
tempfile hq_master
save `hq_master'

forvalues v = 1/6 {
local start = 1 + 10000000*(`v'-1)
local end = 10000000*`v'
di `start'
di `end'

** Import NETS data (~52.5 million observations total)
import delimited "$netspath/NETS2012Basic.txt", clear rowrange(`start':`end') colrange(1:185) varnames(1) delimiter(",")

keep dunsnumber cbsa citycode emp* sales* estcat* fips* firstyear foreignown hqduns95 industry industrygroup lastyear naics*
drop if hqduns95 == .

merge m:1 hqduns95 using `hq_master'
keep if _merge == 3		// keeps all establishments linked to a firm based in US (of firms with some PR presence)

drop _merge

save "$output_NETS/pr_extract_panel`v'.dta", replace

}

** Append Est files
use "$output_NETS/pr_extract_panel1.dta", clear

forvalues v = 2/6 {
	append using "$output_NETS/pr_extract_panel`v'.dta"
	
}

gen fipsstate95 = floor(fips95/1000)

drop if fips95 == .

** Reshape. This is currently est. level dataset
drop estcat* empc*

keep dunsnumber emp* sales* fipsstate95 hqduns95
order hqduns95 dunsnumber fipsstate95
sort hqduns95 dunsnumber fipsstate95

gen inPR = 0
replace inPR = 1 if fipsstate95 == 72

foreach y in 90 91 92 93 94 95 96 97 98 99 00 01 02 03 04 05 06 07 08 09 10 11 12 {
gen num_est`y' = 1 if emp`y' != .
}

collapse (sum) emp* (sum) sales* (sum) num_est*, by(hqduns inPR)

foreach v in 00 01 02 03 04 05 06 07 08 09 10 11 12 {
	rename emp`v' emp20`v'
	rename sales`v' sales20`v'
	rename num_est`v' num_est20`v'
}
forval v = 90/99 {
	rename emp`v' emp19`v'
	rename sales`v' sales19`v'
	rename num_est`v' num_est19`v'
}

reshape long emp sales num_est, i(hqduns inPR) j(year)

reshape wide emp sales num_est, i(hqduns year) j(inPR)

rename emp0 emp_US
rename sales0 sales_US
rename num_est0 num_est_US
rename emp1 emp_PR
rename sales1 sales_PR
rename num_est1 num_est_PR

gen emp_total = emp_US + emp_PR
gen sales_total = sales_US + sales_PR
gen num_est_total = num_est_US + num_est_PR

save "$output_NETS/pr_extract_panel.dta", replace

