set more off

// goal of this file is to see how large the tax credits were in comparison to the federal taxes paid by US companies 
// author: jcss
// date: 3-15-2018

************************************
* Grabbing and organizing the data
************************************

clear
use "$WRDS/ETR_compustat.dta", replace
replace PR = 0 if PR == .
gen nonPR = 1-PR

do "$analysis/jc_stat_out.ado"

// doing a cross section in 1995 of active firms with positive assets
keep if year == 1995
keep if at > 0 & at != .

// some size bins 
gen s1 = (emp_US > 20000) & emp_US != .
gen s2 = (emp_US > 30000) & emp_US != .
gen s3 = (emp_US > 40000) & emp_US != .
gen s4 = (emp_US > 50000) & emp_US != .

************************************
* percent of investment at pr_linked firms
************************************
sum capx  if PR 
scalar tot_capx = r(mean)*r(N)

sum capx
scalar tot_capx_tot = r(mean)*r(N)
di tot_capx 
di tot_capx_tot 

di tot_capx / tot_capx_tot
local pr_capex = 100 * tot_capx / tot_capx_tot
jc_stat_out,  number(`pr_capex') name("RESULTcapexI") replace(0) deci(1) figs(4)


************************************
* Industries of pr_linked firms
************************************
tostring naics, gen(naicsh)

gen naics4 = substr(naicsh,1,4)
encode naics4, generate(n4)

sum PR if naics4 == "3254", d
local pr = r(mean) * 100
di `pr'

jc_stat_out,  number(`pr') name("RESULTpharmaII") replace(0) deci(1) figs(3)


************************************
* Federal Taxes Paid by PR linked Firms
************************************
sum txfed   if PR 
scalar tot_tax = r(mean)*r(N)

sum txfed
scalar tot_tax_tot = r(mean)*r(N)
di tot_tax 
di tot_tax_tot

************************************
* Assign All 936 Tax Credits to thesefirms 
************************************
** Question: Dan are the data for txfed in 1995 dollars?  (yes, everything in compustat is in the source year nominal dollars.)
** Question: Dan are the data for txfed in millions of dollars? (yes, they are in millions of dollars)
scalar perc_tax = 3055.942/tot_tax
di perc_tax*100

local perc_tax = perc_tax * 100
jc_stat_out,  number(`perc_tax') name("RESULTII") replace(0) deci(0) figs(2)
jc_stat_out,  number(`perc_tax') name("RESULTIIb") replace(0) deci(1) figs(4)

************************************
* Employment in US by PR linked firms with data on Fed Tax Paid 
************************************
sum emp_US  if PR & txfed != . 
scalar tot_emp = r(mean)*r(N)
local N = r(N) 
jc_stat_out,  number(`N') name("RESULTXII") replace(0) deci(0) figs(3)

*** descriptive/jobs_ts_d.do has to run before this file
scalar perc_emp = tot_emp/($value * 1000000)
di perc_emp*100

local perc_emp = perc_emp * 100 
jc_stat_out,  number(`perc_emp') name("RESULTXIII") replace(0) deci(1) figs(4)

************************************
* Assign 936 Tax Credits to these firms based on the fraction of employment they represent 
************************************
** Question: Dan does this makes sense to you? 
scalar perc_tax_emp = perc_emp*3055.942/tot_tax
di perc_tax_emp*100

local perc_tax_emp = perc_tax_emp*100
jc_stat_out,  number(`perc_tax_emp') name("RESULTXXV") replace(0) deci(1) figs(3)

************************************
* How do tax credits compare to sales 
************************************
sum sale if txfed!=. & PR 
local sale  = r(mean) 
local sale_tot = r(mean)*r(N)

sum pi if txfed!=. & PR 
local pi  = r(mean) 

*** ** Profit ratio 
di `pi'/`sale'
local avg_pf = `pi'/`sale' *  100
jc_stat_out,  number(`avg_pf') name("RESULTXV") replace(0) deci(2) figs(4)

sum txfed if txfed!=. & PR 
local tx  = r(mean) 

sum pidom if txfed!=. & PR 
local pidom  = r(mean) 

** Average tax rate 
di `tx'/`pidom'
local avg_tax = round(`tx'/`pidom' *  100)
jc_stat_out,  number(`avg_tax') name("RESULTXIV") replace(0) deci(0) figs(2)

** Sales equivalent 
di 3.055942/((`tx'/`pidom')*(`pi'/`sale'))
local sales_eq =  4.9/((`avg_pf'/100)*(`avg_tax'/100))
jc_stat_out,  number(`sales_eq') name("RESULTXVI") replace(0) deci(0) figs(3)

** Fraction of current sales when all credits go to the secompanies
di 1000*3.055942/((`tx'/`pidom')*(`pi'/`sale'))/`sale_tot'

** Fraction of current sales when all credits go to the secompanies
di perc_emp*1000*3.055942/((`tx'/`pidom')*(`pi'/`sale'))/`sale_tot'



*** percent of all fed taxes from 219 firms
**** total corporate taxes of 198,242,735,000 from CATRB07 (Corporate complete report, 1995)
**** total corporate revenue of 11,853,634,626,000  
di tot_tax * 1000000 / 198242735000

** possessions tax credit in 1995 of 3,055,942,000
di 3055942000 / 198242735000

** total compustat taxes paid
di tot_tax_tot * 1000000 / 198242735000

*** labor percent semi-elasticities
estimates use "$output/comparison_emp"

// outputting scalars for the text
do "$analysis/jc_stat_out.ado"
local dif = -_b[_IyeaXPR_2012] * 100
local semi1 = `dif' / `perc_tax'
local semi2 = `dif' / `perc_tax_emp'
jc_stat_out,  number(`semi1') name("RESULTempsemilow") replace(0) deci(2) figs(4)
jc_stat_out,  number(`semi2') name("RESULTempsemihigh") replace(0) deci(2) figs(4)


