** this file runs a regression of industry ETR on industry possessions tax credits
** author: dan
** date: 09-26-2018

clear all
set more off
snapshot erase _all

** importing data
use "$SOI/aggregate_SOI", clear
replace variable = subinstr(variable,"_t_","",.)
replace variable = substr(variable,1,length(variable)-1) if substr(variable,length(variable)-2,length(variable)) == "_"

drop if variable == "manufact"
drop if variable == "tot_"
egen id = group(variable)

collapse (sum) Us *Income* Receipts (first) variable, by(year id) 

	* defining ETR variable based on income taxes paid (not total taxes paid)
	gen ETR = Income_Tax_After_Credits / Income_Subject_To_Tax // Income_Subject_To_Tax
	gen net_margin = Net_Income / Receipts
	gen tax = Income_Tax_After_Credits
	gen lntax = ln(tax)

	* defining Us credit as a portion of Taxable income
	gen pos_cred = Us_Possessions_Tax_Credit / Income_Subject_To_Tax
	replace pos_cred = 0 if pos_cred == .
	/* identifying major sectors and industries
	sort pos_cred
	br if year == 1995
	"324" petrol,"323" printing,"337" furniture,"322" paper,"327" nonferrous,"336" cars,"331" nonferrous,"321" sawmills
	
	cutoff choosen at 0.001
	*/
	gen raw_pos = Us_Possessions_Tax_Credit
	gen ln_pos = ln(raw_pos)
	
xtset id year	
gen d_pos_cred = pos_cred - l.pos_cred
gen d_ETR = ETR - l.ETR

gen d_raw_pos = raw_pos - l.raw_pos
gen d_tax = tax - l.tax



** Generating weights that are constant over time for each industry based on 1995 levels {
foreach var of var Income_Subject_To_Tax Net_Income Receipts {
gen `var'_temp = `var' if inlist(year,1994,1995)
bys id: egen `var'_base = mean(`var'_temp)
drop *temp

}
rename Income_Subject_To_Tax_base ISTT

snapshot save
** only using sample with variation in S936
keep if year <= 2006
*** regressions!
est clear
set more off

* version weighted

eststo reg_1: reg ETR pos_cred i.year i.id [aw=ISTT ], cl(id) 
	estadd local hasy "Yes" 
	estadd local hasid "Yes"
	estadd local hasistt "Yes"

eststo reg_2: reg ETR pos_cred i.year i.id  Net_Income Receipts [aw=ISTT ], cl(id) 
	estadd local hasy "Yes" 
	estadd local hasid "Yes"
	estadd local hasistt "Yes"
	estadd local hasni "Yes"

eststo reg_3: reg ETR pos_cred i.year i.id  Net_Income Receipts [aw=Net_Income_base ], cl(id) 
	estadd local hasy "Yes" 
	estadd local hasid "Yes"
	estadd local hasistt ""
	estadd local hasni "Yes"
	estadd local hasnib "Yes"

eststo reg_4: reg d_ETR d_pos_cred i.year  [aw=ISTT ], cl(id) 
	estadd local hasy "Yes" 
	estadd local hasid ""
	estadd local hasistt "Yes"
	estadd local hasdif "Yes"

eststo reg_5: reg d_ETR d_pos_cred i.year  Net_Income Receipts [aw=ISTT ], cl(id) 
	estadd local hasy "Yes" 
	estadd local hasid ""
	estadd local hasistt "Yes"
	estadd local hasdif "Yes"
	estadd local hasni "Yes"

eststo reg_6: reg d_ETR d_pos_cred i.year  Net_Income Receipts [aw=Net_Income_base ], cl(id) 
	estadd local hasy "Yes" 
	estadd local hasid ""
	estadd local hasistt ""
	estadd local hasdif "Yes"
	estadd local hasni "Yes"
	estadd local hasnib "Yes"


** outputting table weighted by taxable income
esttab reg_? using "$output/Tables/table_industry_ETR.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons nonum posthead(" ") ///
prefoot("") ///
postfoot(" ") ///

esttab reg_? using "$output/Tables/table_industry_ETR.tex", keep(pos_cred d_pos_cred)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s() /// 
coeflabel(d_pos_cred "\hspace{1em}Change in Possession Credits" pos_cred "\hspace{1em}Possession Credits") ///
preh("") noobs scalars(  "hasy Year Fixed Effects" ///
"hasid Industry Fixed Effects" "hasni Net Income and Sales Controls"  "hasdif Regression in First Differences" ///
"hasistt Taxable Income Weights" "hasnib Net Income Weights" ) label /// 
 prefoot("\hline") ///
postfoot( ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex)		


****************************************************************
* binscatter of the third column
****************************************************************
reg ETR i.year i.id  Net_Income Receipts [aw=Net_Income_base ]
predict etr_res
replace etr_res = ETR - etr_res

reg pos_cred i.year i.id  Net_Income Receipts [aw=Net_Income_base ]
predict pos_res
replace pos_res = pos_cred - pos_res

binscatter etr_res pos_res [aw = Net_Income_base] ///
	   , graphregion(fcolor(white)) bgcolor(white) ///
		xtitle("Residualized Posessions Credits / Income") ///
		ytitle("Residualized Tax Liabilities / Income") 
	graph export "$output/Graphs/SOI_beta_binscatter.pdf" , replace 

scatter etr_res pos_res [aw = Net_Income_base] ///
	   , graphregion(fcolor(white)) bgcolor(white) ///
		xtitle("Residualized Posessions Credits / Income") ///
		ytitle("Residualized Tax Liabilities / Income") 
	graph export "$output/Graphs/SOI_beta_scatter.pdf" , replace 

	
* version with more stuff included	
** scatterplot and line graph
foreach var of varlist ETR pos_cred { 
	capture: drop res_`var'
	reg `var' i.year i.id [aw=ISTT]  //  [aw=Receipts]
	predict res_`var', res
	}

gen line_y = .
gen line_x = .
replace line_y = 0.03 in 1
replace line_y = -.03 in 2
replace line_x = -.03 in 1
replace line_x = 0.03 in 2

	
foreach indep of varlist pos_cred {
* regression for scalars under figure
reg ETR `indep' i.year i.id [aw=ISTT] , cl(id) //
local b = round(_b[`indep'],.001)
local b : di %4.3f `b'
local se = round(_se[`indep'],.001)
local se : di %4.3f `se'
mat table = r(table)
local p = round(table[4,1],.001)
	
sum res_ETR [aw=ISTT],d
twoway  (lfitci  res_ETR res_`indep' [aw=ISTT], est(cl(id)) level(90) lcolor(black)) ///
		(scatter res_ETR res_`indep' [aw=ISTT], yline(`r(mean)',lcolor(black) lpattern(dash)) mfcolor(gs15)) ///
		(line line_y line_x, lcolor(red) lpattern(shortdash) lwidth(thick)),  /// 
		xlab(-.03(.02).03) ylab(-.1(.05).15) ///
		graphregion(fcolor(white)) ///
		xtitle("Industry Section 936 Credits Over Taxable Income (Residualized)") ///
		ytitle("Industy Effective Tax Rate (Residualized)") ///
		legend(order(3 - 2 1 4) label(3 "Data")  label(1 "90% CI") label(2 "Linear Fit") ///
		label(4 "No Replacement")) ///
		note("The slope of the regression line is `b' with standard error `se' and p-value `p'")
	
graph export "$output/Graphs/industry_ETR_graph_scatter_raw.pdf", replace		
}	

** Version with nicely binned scatter (manual for SE)
foreach var of varlist ETR pos_cred { 
	capture: drop res_`var'
	reg `var' i.year i.id [aw=ISTT] 
	predict res_`var', res
	}

	
** making measure of weighted quantile bins
foreach Q of var res_pos_cred {	
	qui sum `Q', d

	xtile q_`Q'=`Q' [aw=ISTT], nq(20)
	preserve 
	collapse (mean) res_ETR `Q' [aw=ISTT], by(q_`Q') 
	foreach y of var res_ETR {
	rename `y' q_`y'
	}
	rename `Q' q_m_`Q'
	tempfile qmeans
	save "`qmeans'", replace
	restore 
	merge m:1 q_`Q' using "`qmeans'"
	drop _merge 
}

capture: drop line_*
gen line_y = .
gen line_x = .
replace line_y = 0.012 in 1
replace line_y = -.012 in 2
replace line_x = -.012 in 1
replace line_x = 0.012 in 2
	
foreach indep of varlist pos_cred {	
reg ETR `indep' i.year i.id [aw=ISTT] , cl(id) //
local b = round(_b[`indep'],.0001)
local b : di %4.3f `b'
local se = round(_se[`indep'],.001)
local se : di %4.3f `se'
mat table = r(table)
local p = round(table[4,1],.001)
sum res_ETR [aw=ISTT],d
twoway  (lfitci  res_ETR res_`indep' [aw=ISTT], est(cl(id)) level(90) lcolor(black) range(-.012 .012)) ///
		(scatter q_res_ETR q_m_res_`indep', yline(`r(mean)',lcolor(black) lpattern(dash)) mcolor(black)) ///
		(line line_y line_x, lcolor(red) lpattern(shortdash) lwidth(thick)),  /// 
		xlab(-.01(.005).01) ylab(-.015(.005).015) ///
		graphregion(fcolor(white)) ///
		xtitle("Industry Section 936 Credits Over Taxable Income (Residualized)") ///
		ytitle("Industy Effective Tax Rate (Residualized)") ///
		legend(order(3 - 2 1 4) label(3 "Binned Data")  label(1 "90% CI") label(2 "Linear Fit") ///
		label(4 "No Replacement")) ///
		note("The slope of the regression line is  `b' with standard error `se' and p-value `p'")
	
graph export "$output/Graphs/industry_ETR_graph_scatter_bins.pdf", replace		
}	
	


** this file runs a regression of industry ETR on industry possessions tax credits
** author: dan
** date: 09-26-2018

clear all
set more off
snapshot erase _all

/* 
Goals of this exercise:
Test different measures of industry profits linked to PR to estimate changes in ETR
-Baseline estimates assign profits proportional to employment
-New Specifications:
	-Proportional to sales
		-NETS (need to redownload NETS to do this)
		-Compustat Sample
	-Proportional to capital
		-Compustat Sample 
*/

*** First using compustat to find proportion of sales, capex, and investment at PR firms
use "$WRDS/ETR_compustat_Segments.dta", replace
replace PR = 0 if PR == .
gen nonPR = 1-PR
 
** fixing naics to be constant at 1995 or first listing
preserve
collapse (firstnm) naicsh, by(gvkey)
rename naicsh f_naics
tempfile naics
save "`naics'",replace
restore

merge m:1 gvkey using "`naics'"
gen temp = naicsh if year == 1995
destring temp, replace
bys gvkey: egen naics = min(temp)
tostring naics, replace
replace naicsh = naics
replace naicsh = f_naics if naicsh == "."
drop naics temp f_naics _merge
 

*** cleaning up NAICS categories to work with SOI data
gen naics3 = substr(naicsh,1,3)
gen naics2 = substr(naics3,1,2)
replace naics3 = "11" if naics2 == "11"
replace naics3 = "21" if naics2 == "21"
replace naics3 = "23" if naics2 == "23"
replace naics3 = "42" if naics2 == "42"
replace naics3 = "44" if naics2 == "44"
replace naics3 = "45" if naics2 == "45" 
replace naics3 = "48" if naics2 == "48"
replace naics3 = "49" if naics2 == "49"
replace naics3 = "51" if naics2 == "51"
replace naics3 = "53" if naics2 == "53"
replace naics3 = "54" if naics2 == "54"
replace naics3 = "61" if naics2 == "61"
replace naics3 = "62" if naics2 == "62"
replace naics3 = "71" if naics2 == "71"
replace naics3 = "999" if naics2 == ""
drop if naics3 == "33" // dropping weird obs

** creating variables to sum at the industry level
gen capx_pr = capx * PR
gen ppent_pr = ppent * PR
gen sale_pr = sale * PR

collapse (mean) PR (sum) capx* ppent* sale sale_pr , by(naics3 year)
rename PR n3_PR 
gen n3 = naics3
gen n2_temp = substr(n3, 1,2)
egen n2 = group(n2_temp)	

gen link_capx = capx_pr / capx
gen link_ppent = ppent_pr / ppent
gen link_sale = sale_pr / sale

foreach Q of varlist capx ppent sale {
	sum link_`Q' [aw=`Q'] if year < 1996
	scalar link_`Q' = r(mean)
	sum link_`Q' [aw=`Q'] if year < 1996 & ~inlist(n3,"324","323","337","322","327","336","331","321")
	scalar v1_link_`Q' = r(mean)
	sum link_`Q' [aw=`Q'] if year < 1996 & inlist(n2,3,5,6,7,11,12)
	scalar v2_link_`Q' = r(mean) 
	sum link_`Q' [aw=`Q'] if year < 1996 & ~inlist(n3,"324","323","337","322","327","336","331","321") & inlist(n2,3,5,6,7,11,12)
	scalar v3_link_`Q' = r(mean)
}

	
**************************************************
* Making the 3 point estimates of the tax change for each sample
**************************************************
use "$output_NETS/industry_cat_emp_sums_v2", clear
gen n2 = substr(industry_code,1,2)
replace industry_code = "11" if n2 == "11"
replace industry_code = "21" if n2 == "21"
replace industry_code = "23" if n2 == "23"
replace industry_code = "42" if n2 == "42"
replace industry_code = "44" if n2 == "44"
replace industry_code = "45" if n2 == "45" 
replace industry_code = "48" if n2 == "48"
replace industry_code = "49" if n2 == "49"
replace industry_code = "51" if n2 == "51"
replace industry_code = "53" if n2 == "53"
replace industry_code = "54" if n2 == "54"
replace industry_code = "61" if n2 == "61"
replace industry_code = "62" if n2 == "62"
replace industry_code = "71" if n2 == "71"

collapse (sum) emp95 pr_emp annual_avg_estabs annual_avg_emplvl total_annual_wages, by(industry_code)
rename industry_code n3

* calculating a scalar for each of the 3 categories:
gen link = pr_emp / emp95
gen n2_temp = substr(n3, 1,2)
egen n2 = group(n2_temp)	

	sum link [aw=emp95]
	scalar base_link = r(mean)
	sum link [aw=emp95] if ~inlist(n3,"324","323","337","322","327","336","331","321")
	scalar v1_link = r(mean)
	sum link [aw=emp95] if inlist(n2,3,5,6,7,11,12)
	scalar v2_link = r(mean) 
	sum link [aw=emp95] if ~inlist(n3,"324","323","337","322","327","336","331","321") & inlist(n2,3,5,6,7,11,12)
	scalar v3_link = r(mean)

** importing SOI data
use "$SOI/aggregate_SOI", clear
replace variable = subinstr(variable,"_t_","",.)
replace variable = substr(variable,1,length(variable)-1) if substr(variable,length(variable)-2,length(variable)) == "_"

drop if variable == "manufact"
drop if variable == "tot_"
* generating 3 expansions, then we can calculate the counterfactual changes differently for each one:
expand 2, gen(new)
expand 2 if new == 0, gen(new2)
replace new = 2 if new2 == 1
expand 2 if new == 0, gen(new3)
replace new = 3 if new3 == 1
drop new2 new3

**** relevant categories for each one
gen relevant = (new == 0)
replace relevant = 1 if new == 1 & ~inlist(NAICS02,"324","323","337","322","327","336","331","321")
replace relevant = 1 if new == 2 & ~inlist(variable,"fin_real","ag","retail","service","wholesale","construction","mining")
replace relevant = 1 if new == 3 & ~inlist(NAICS02,"324","323","337","322","327","336","331","321") & ~inlist(variable,"fin_real","ag","retail","service","wholesale","construction","mining")

egen id = group(relevant)

collapse (sum) Us *Income* Receipts, by(year id new) 

	* defining ETR variable based on income taxes paid (not total taxes paid)
	gen ETR = Income_Tax_After_Credits / Income_Subject_To_Tax // Income_Subject_To_Tax
	gen net_margin = Net_Income / Receipts
	gen tax = Income_Tax_After_Credits
	gen lntax = ln(tax)

	* defining Us credit as a portion of Taxable income
	gen pos_cred = Us_Possessions_Tax_Credit / Income_Subject_To_Tax
	replace pos_cred = 0 if pos_cred == .
	gen raw_pos = Us_Possessions_Tax_Credit
	gen ln_pos = ln(raw_pos)
	

** Generating weights that are constant over time for each industry based on 1995 levels {
keep if id == 2
foreach var of var Us_Possessions_Tax_Credit Income_Subject_To_Tax Net_Income Receipts {
gen `var'_temp = `var' if inlist(year,1995) // 
bys new: egen `var'_base = mean(`var'_temp)
drop *temp

}
rename Income_Subject_To_Tax_base ISTT
rename Us_Possessions_Tax_Credit_base USPTC

** outputting some figures
local beta_base "0.816"

gen share = USPTC / ISTT
gen const_cred = share * Income_Subject_To_Tax
gen lost_cred = const_cred - Us_Possessions_Tax_Credit

gen p_data = (Income_Tax_After_Credits) / Income_Subject_To_Tax
gen p_new = (Income_Tax_After_Credits * base_link - `beta_base' * lost_cred ) / (Income_Subject_To_Tax * base_link) if new == 0
replace p_new = (Income_Tax_After_Credits * v1_link - `beta_base' * lost_cred ) / (Income_Subject_To_Tax * v1_link) if new == 1
replace p_new = (Income_Tax_After_Credits * v2_link - `beta_base' * lost_cred ) / (Income_Subject_To_Tax * v2_link) if new == 2
replace p_new = (Income_Tax_After_Credits * v3_link - `beta_base' * lost_cred ) / (Income_Subject_To_Tax * v3_link) if new == 3

gen p_new_capx = (Income_Tax_After_Credits * link_capx - `beta_base' * lost_cred ) / (Income_Subject_To_Tax * link_capx) if new == 0
replace p_new_capx = (Income_Tax_After_Credits * v1_link_capx - `beta_base' * lost_cred ) / (Income_Subject_To_Tax * v1_link_capx) if new == 1
replace p_new_capx = (Income_Tax_After_Credits * v2_link_capx - `beta_base' * lost_cred ) / (Income_Subject_To_Tax * v2_link_capx) if new == 2
replace p_new_capx = (Income_Tax_After_Credits * v3_link_capx - `beta_base' * lost_cred ) / (Income_Subject_To_Tax * v3_link_capx) if new == 3

gen p_new_ppent = (Income_Tax_After_Credits * link_ppent - `beta_base' * const_cred ) / (Income_Subject_To_Tax * link_ppent) if new == 0
replace p_new_ppent = (Income_Tax_After_Credits * v1_link_ppent - `beta_base' * lost_cred ) / (Income_Subject_To_Tax * v1_link_ppent) if new == 1
replace p_new_ppent = (Income_Tax_After_Credits * v2_link_ppent - `beta_base' * lost_cred ) / (Income_Subject_To_Tax * v2_link_ppent) if new == 2
replace p_new_ppent = (Income_Tax_After_Credits * v3_link_ppent - `beta_base' * lost_cred ) / (Income_Subject_To_Tax * v3_link_ppent) if new == 3

gen p_new_sale = (Income_Tax_After_Credits * link_sale - `beta_base' * lost_cred ) / (Income_Subject_To_Tax * link_sale) if new == 0
replace p_new_sale = (Income_Tax_After_Credits * v1_link_sale - `beta_base' * lost_cred ) / (Income_Subject_To_Tax * v1_link_sale) if new == 1
replace p_new_sale = (Income_Tax_After_Credits * v2_link_sale - `beta_base' * lost_cred ) / (Income_Subject_To_Tax * v2_link_sale) if new == 2
replace p_new_sale = (Income_Tax_After_Credits * v3_link_sale - `beta_base' * lost_cred ) / (Income_Subject_To_Tax * v3_link_sale) if new == 3
gen p_dif = p_data - p_new

sort new year
twoway (scatter p_new year if new == 3 & year > 1994 & year < 2008, c(line) lpattern(dash) ) ///
       (scatter p_data year if new == 3 & year > 1994 & year < 2008, c(line) lpattern(solid)) ///
       (scatter p_new_capx year if new == 3 & year > 1994 & year < 2008, c(line) lpattern(shortdash)) ///
       (scatter p_new_ppent year if new == 3 & year > 1994 & year < 2008, c(line) lpattern(dash_dot)) ///
	   , xlab(1995(2)2007) ylab(.16(.02).3) ///
	   	graphregion(fcolor(white)) bgcolor(white) ///
		xtitle("Year") ///
		ytitle("Effective Tax Rate for Exposed Industries", margin(medsmall)) ///
		legend(order(2 1 3 4) label(1 "Effective Tax Rate without S936 Repeal (Income Weights)") label(2 "Observed Effective Tax Rate") ///
		label(3 "Effective Tax Rate without S936 Repeal (Capex Weights)") label(4 "Effective Tax Rate without S936 Repeal (PPE Weights)") rows(4) ) 
graph export "$output/Graphs/SOI_counterfact_etr_ATOT.pdf" , replace 


twoway (scatter p_new year if new == 0 & year > 1994 & year < 2008, c(line) lpattern(dash) ) ///
       (scatter p_data year if new == 0 & year > 1994 & year < 2008, c(line) lpattern(solid)) ///
	   , xlab(1995(2)2007) ylab(.16(.02).3) ///
	   	graphregion(fcolor(white)) bgcolor(white) ///
		xtitle("Year") ///
		ytitle("Effective Tax Rate for Exposed Industries", margin(medsmall)) ///
		legend(order(2 1) label(1 "Effective Tax Rate without S936 Repeal (Income Weights)") label(2 "Observed Effective Tax Rate")  rows(2) ) 
graph export "$output/Graphs/SOI_counterfact_etr_ATOT_orig.pdf" , replace 


twoway (scatter p_new year if new == 3 & year > 1994 & year < 2008, c(line) lpattern(dash) ) ///
       (scatter p_data year if new == 3 & year > 1994 & year < 2008, c(line) lpattern(solid)) ///
	   , xlab(1995(2)2007) ylab(.16(.02).3) ///
	   	graphregion(fcolor(white)) bgcolor(white) ///
		xtitle("Year") ///
		ytitle("Effective Tax Rate for Exposed Industries", margin(medsmall)) ///
		legend(order(2 1) label(1 "Effective Tax Rate without S936 Repeal (Income Weights)") label(2 "Observed Effective Tax Rate")  rows(2) ) 
graph export "$output/Graphs/SOI_counterfact_etr_ATOT_orig_major.pdf" , replace 

* 2007 differences for tables
keep if year == 2007

sum p_dif if new == 0
scalar p_dif_v0 = r(mean)

sum p_dif if new == 1
scalar p_dif_v1 = r(mean)

sum p_dif if new == 2
scalar p_dif_v2 = r(mean)

sum p_dif if new == 3
scalar p_dif_v3 = r(mean)

foreach Q in capx ppent sale {
gen p_dif`Q' = p_data - p_new_`Q'

sum p_dif`Q' if new == 0
scalar p_dif_v0_`Q' = r(mean)

sum p_dif`Q' if new == 1
scalar p_dif_v1_`Q' = r(mean)

sum p_dif`Q' if new == 2
scalar p_dif_v2_`Q' = r(mean)

sum p_dif`Q' if new == 3
scalar p_dif_v3_`Q' = r(mean)

}



** outputting a table with the 4 numbers for each sample
eststo reg_1: reg ETR new
		estadd local pp_dif = string(p_dif_v0*100, "%8.3fc")
		estadd local pp_dif_a = string(p_dif_v0_capx*100, "%8.3fc")
		estadd local pp_dif_b = string(p_dif_v0_ppent*100, "%8.3fc")
		estadd local pp_dif_c = string(p_dif_v0_sale*100, "%8.3fc")
eststo reg_2: reg ETR new
		estadd local pp_dif = string(p_dif_v1*100, "%8.3fc")
		estadd local pp_dif_a = string(p_dif_v1_capx*100, "%8.3fc")
		estadd local pp_dif_b = string(p_dif_v1_ppent*100, "%8.3fc")
		estadd local pp_dif_c = string(p_dif_v1_sale*100, "%8.3fc")
eststo reg_3: reg ETR new
		estadd local pp_dif = string(p_dif_v2*100, "%8.3fc")
		estadd local pp_dif_a = string(p_dif_v2_capx*100, "%8.3fc")
		estadd local pp_dif_b = string(p_dif_v2_ppent*100, "%8.3fc")
		estadd local pp_dif_c = string(p_dif_v2_sale*100, "%8.3fc")
eststo reg_4: reg ETR new
		estadd local pp_dif = string(p_dif_v3*100, "%8.3fc")
		estadd local pp_dif_a = string(p_dif_v3_capx*100, "%8.3fc")
		estadd local pp_dif_b = string(p_dif_v3_ppent*100, "%8.3fc")
		estadd local pp_dif_c = string(p_dif_v3_sale*100, "%8.3fc")
		

esttab reg_? using "$output/Tables/table_SOI_ETR_robustness.tex", keep(_cons) stats() ///
b(3) se par label scalars("pp_dif Allocated by Employment" "pp_dif_a Allocated by Capex" "pp_dif_b Allocated by PPE" "pp_dif_c Allocated by Sales" ) ///
noobs nogap nomtitle tex replace nonum nocons postfoot( ///
"\hline S936 Exposed Industry &   & Y  &  & Y \\ " /// 
"S936 Exposed Sector &   &  &  Y & Y  \\ \hline\hline" ///
"\end{tabular} }" ) sty(tex)	 prefoot("") posthead("")
