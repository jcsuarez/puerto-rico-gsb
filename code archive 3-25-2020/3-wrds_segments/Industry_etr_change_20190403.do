** this file runs a regression of industry ETR on industry possessions tax credits
** author: dan
** date: 09-26-2018

clear all
set more off
snapshot erase _all

/* 
Goals of this exercise:
Test different measures of industry profits linked to PR to estimate changes in ETR
-Baseline estimates assign profits proportional to employment
-New Specifications:
	-Proportional to sales
		-NETS (need to redownload NETS to do this)
		-Compustat Sample
	-Proportional to capital
		-Compustat Sample 
*/

*** First using compustat to find proportion of sales, capex, and investment at PR firms
use "$WRDS/ETR_compustat_Segments.dta", replace
replace PR = 0 if PR == .
gen nonPR = 1-PR
 
** fixing naics to be constant at 1995 or first listing
preserve
collapse (firstnm) naicsh, by(gvkey)
rename naicsh f_naics
tempfile naics
save "`naics'",replace
restore

merge m:1 gvkey using "`naics'"
gen temp = naicsh if year == 1995
destring temp, replace
bys gvkey: egen naics = min(temp)
tostring naics, replace
replace naicsh = naics
replace naicsh = f_naics if naicsh == "."
drop naics temp f_naics _merge
 

*** cleaning up NAICS categories to work with SOI data
gen naics3 = substr(naicsh,1,3)
gen naics2 = substr(naics3,1,2)
replace naics3 = "11" if naics2 == "11"
replace naics3 = "21" if naics2 == "21"
replace naics3 = "23" if naics2 == "23"
replace naics3 = "42" if naics2 == "42"
replace naics3 = "44" if naics2 == "44"
replace naics3 = "45" if naics2 == "45" 
replace naics3 = "48" if naics2 == "48"
replace naics3 = "49" if naics2 == "49"
replace naics3 = "51" if naics2 == "51"
replace naics3 = "53" if naics2 == "53"
replace naics3 = "54" if naics2 == "54"
replace naics3 = "61" if naics2 == "61"
replace naics3 = "62" if naics2 == "62"
replace naics3 = "71" if naics2 == "71"
replace naics3 = "999" if naics2 == ""
drop if naics3 == "33" // dropping weird obs

** creating variables to sum at the industry level
gen capx_pr = capx * PR
gen ppent_pr = ppent * PR
gen sale_pr = sale * PR

collapse (mean) PR (sum) capx* ppent* sale sale_pr , by(naics3 year)
rename PR n3_PR 
gen n3 = naics3
gen n2_temp = substr(n3, 1,2)
egen n2 = group(n2_temp)	

gen link_capx = capx_pr / capx
gen link_ppent = ppent_pr / ppent
gen link_sale = sale_pr / sale

foreach Q of varlist capx ppent sale {
	sum link_`Q' [aw=`Q'] if year < 1996
	scalar link_`Q' = r(mean)
	sum link_`Q' [aw=`Q'] if year < 1996 & ~inlist(n3,"324","323","337","322","327","336","331","321")
	scalar v1_link_`Q' = r(mean)
	sum link_`Q' [aw=`Q'] if year < 1996 & inlist(n2,3,5,6,7,11,12)
	scalar v2_link_`Q' = r(mean) 
	sum link_`Q' [aw=`Q'] if year < 1996 & ~inlist(n3,"324","323","337","322","327","336","331","321") & inlist(n2,3,5,6,7,11,12)
	scalar v3_link_`Q' = r(mean)
}

	
**************************************************
* Making the 3 point estimates of the tax change for each sample
**************************************************
use "$output_NETS/industry_cat_emp_sums_v2", clear
gen n2 = substr(industry_code,1,2)
replace industry_code = "11" if n2 == "11"
replace industry_code = "21" if n2 == "21"
replace industry_code = "23" if n2 == "23"
replace industry_code = "42" if n2 == "42"
replace industry_code = "44" if n2 == "44"
replace industry_code = "45" if n2 == "45" 
replace industry_code = "48" if n2 == "48"
replace industry_code = "49" if n2 == "49"
replace industry_code = "51" if n2 == "51"
replace industry_code = "53" if n2 == "53"
replace industry_code = "54" if n2 == "54"
replace industry_code = "61" if n2 == "61"
replace industry_code = "62" if n2 == "62"
replace industry_code = "71" if n2 == "71"

collapse (sum) emp95 pr_emp annual_avg_estabs annual_avg_emplvl total_annual_wages, by(industry_code)
rename industry_code n3

* calculating a scalar for each of the 3 categories:
gen link = pr_emp / emp95
gen n2_temp = substr(n3, 1,2)
egen n2 = group(n2_temp)	

	sum link [aw=emp95]
	scalar base_link = r(mean)
	sum link [aw=emp95] if ~inlist(n3,"324","323","337","322","327","336","331","321")
	scalar v1_link = r(mean)
	sum link [aw=emp95] if inlist(n2,3,5,6,7,11,12)
	scalar v2_link = r(mean) 
	sum link [aw=emp95] if ~inlist(n3,"324","323","337","322","327","336","331","321") & inlist(n2,3,5,6,7,11,12)
	scalar v3_link = r(mean)

** importing SOI data
use "$SOI/aggregate_SOI", clear
replace variable = subinstr(variable,"_t_","",.)
replace variable = substr(variable,1,length(variable)-1) if substr(variable,length(variable)-2,length(variable)) == "_"

drop if variable == "manufact"
drop if variable == "tot_"
* generating 3 expansions, then we can calculate the counterfactual changes differently for each one:
expand 2, gen(new)
expand 2 if new == 0, gen(new2)
replace new = 2 if new2 == 1
expand 2 if new == 0, gen(new3)
replace new = 3 if new3 == 1
drop new2 new3

**** relevant categories for each one
gen relevant = (new == 0)
replace relevant = 1 if new == 1 & ~inlist(NAICS02,"324","323","337","322","327","336","331","321")
replace relevant = 1 if new == 2 & ~inlist(variable,"fin_real","ag","retail","service","wholesale","construction","mining")
replace relevant = 1 if new == 3 & ~inlist(NAICS02,"324","323","337","322","327","336","331","321") & ~inlist(variable,"fin_real","ag","retail","service","wholesale","construction","mining")

egen id = group(relevant)

collapse (sum) Us *Income* Receipts, by(year id new) 

	* defining ETR variable based on income taxes paid (not total taxes paid)
	gen ETR = Income_Tax_After_Credits / Income_Subject_To_Tax // Income_Subject_To_Tax
	gen net_margin = Net_Income / Receipts
	gen tax = Income_Tax_After_Credits
	gen lntax = ln(tax)

	* defining Us credit as a portion of Taxable income
	gen pos_cred = Us_Possessions_Tax_Credit / Income_Subject_To_Tax
	replace pos_cred = 0 if pos_cred == .
	gen raw_pos = Us_Possessions_Tax_Credit
	gen ln_pos = ln(raw_pos)
	

** Generating weights that are constant over time for each industry based on 1995 levels {
keep if id == 2
foreach var of var Us_Possessions_Tax_Credit Income_Subject_To_Tax Net_Income Receipts {
gen `var'_temp = `var' if inlist(year,1994,1995) // 
bys new: egen `var'_base = mean(`var'_temp)
drop *temp

}
rename Income_Subject_To_Tax_base ISTT
rename Us_Possessions_Tax_Credit_base USPTC

** outputting some figures
gen share = USPTC / ISTT
gen const_cred = share * Income_Subject_To_Tax
gen lost_cred = const_cred - Us_Possessions_Tax_Credit

gen p_data = (Income_Tax_After_Credits) / Income_Subject_To_Tax
gen p_new = (Income_Tax_After_Credits * base_link - 0.8 * lost_cred ) / (Income_Subject_To_Tax * base_link) if new == 0
replace p_new = (Income_Tax_After_Credits * v1_link - 0.8 * lost_cred ) / (Income_Subject_To_Tax * v1_link) if new == 1
replace p_new = (Income_Tax_After_Credits * v2_link - 0.8 * lost_cred ) / (Income_Subject_To_Tax * v2_link) if new == 2
replace p_new = (Income_Tax_After_Credits * v3_link - 0.8 * lost_cred ) / (Income_Subject_To_Tax * v3_link) if new == 3

gen p_new_capx = (Income_Tax_After_Credits * link_capx - 0.8 * lost_cred ) / (Income_Subject_To_Tax * link_capx) if new == 0
replace p_new_capx = (Income_Tax_After_Credits * v1_link_capx - 0.8 * lost_cred ) / (Income_Subject_To_Tax * v1_link_capx) if new == 1
replace p_new_capx = (Income_Tax_After_Credits * v2_link_capx - 0.8 * lost_cred ) / (Income_Subject_To_Tax * v2_link_capx) if new == 2
replace p_new_capx = (Income_Tax_After_Credits * v3_link_capx - 0.8 * lost_cred ) / (Income_Subject_To_Tax * v3_link_capx) if new == 3

gen p_new_ppent = (Income_Tax_After_Credits * link_ppent - 0.8 * const_cred ) / (Income_Subject_To_Tax * link_ppent) if new == 0
replace p_new_ppent = (Income_Tax_After_Credits * v1_link_ppent - 0.8 * lost_cred ) / (Income_Subject_To_Tax * v1_link_ppent) if new == 1
replace p_new_ppent = (Income_Tax_After_Credits * v2_link_ppent - 0.8 * lost_cred ) / (Income_Subject_To_Tax * v2_link_ppent) if new == 2
replace p_new_ppent = (Income_Tax_After_Credits * v3_link_ppent - 0.8 * lost_cred ) / (Income_Subject_To_Tax * v3_link_ppent) if new == 3

gen p_new_sale = (Income_Tax_After_Credits * link_sale - 0.8 * lost_cred ) / (Income_Subject_To_Tax * link_sale) if new == 0
replace p_new_sale = (Income_Tax_After_Credits * v1_link_sale - 0.8 * lost_cred ) / (Income_Subject_To_Tax * v1_link_sale) if new == 1
replace p_new_sale = (Income_Tax_After_Credits * v2_link_sale - 0.8 * lost_cred ) / (Income_Subject_To_Tax * v2_link_sale) if new == 2
replace p_new_sale = (Income_Tax_After_Credits * v3_link_sale - 0.8 * lost_cred ) / (Income_Subject_To_Tax * v3_link_sale) if new == 3
gen p_dif = p_data - p_new

sort new year
twoway (scatter p_new year if new == 0 & year > 1993 & year < 2008, c(line) lpattern(dash) ) ///
       (scatter p_data year if new == 0 & year > 1993 & year < 2008, c(line) lpattern(solid)) ///
	   , xlab(1995(2)2007) ylab(.14(.02).3) ///
	   	graphregion(fcolor(white)) bgcolor(white) ///
		xtitle("Year") ///
		ytitle("Effective Tax Rate for Exposed Industries", margin(medsmall)) ///
		legend(order(2 1) label(1 "Effective Tax Rate without S936 Repeal (ATOT)") label(2 "Observed Effective Tax Rate")  rows(3) ) 
graph export "$output/Graphs/SOI_counterfact_etr_ATOT.pdf" , replace 



* 2007 differences for tables
keep if year == 2007

sum p_dif if new == 0
scalar p_dif_v0 = r(mean)

sum p_dif if new == 1
scalar p_dif_v1 = r(mean)

sum p_dif if new == 2
scalar p_dif_v2 = r(mean)

sum p_dif if new == 3
scalar p_dif_v3 = r(mean)

foreach Q in capx ppent sale {
gen p_dif`Q' = p_data - p_new_`Q'

sum p_dif`Q' if new == 0
scalar p_dif_v0_`Q' = r(mean)

sum p_dif`Q' if new == 1
scalar p_dif_v1_`Q' = r(mean)

sum p_dif`Q' if new == 2
scalar p_dif_v2_`Q' = r(mean)

sum p_dif`Q' if new == 3
scalar p_dif_v3_`Q' = r(mean)

}



** outputting a table with the 4 numbers for each sample
eststo reg_1: reg ETR new
		estadd local pp_dif = string(p_dif_v0*100, "%8.3fc")
		estadd local pp_dif_a = string(p_dif_v0_capx*100, "%8.3fc")
		estadd local pp_dif_b = string(p_dif_v0_ppent*100, "%8.3fc")
		estadd local pp_dif_c = string(p_dif_v0_sale*100, "%8.3fc")
eststo reg_2: reg ETR new
		estadd local pp_dif = string(p_dif_v1*100, "%8.3fc")
		estadd local pp_dif_a = string(p_dif_v1_capx*100, "%8.3fc")
		estadd local pp_dif_b = string(p_dif_v1_ppent*100, "%8.3fc")
		estadd local pp_dif_c = string(p_dif_v1_sale*100, "%8.3fc")
eststo reg_3: reg ETR new
		estadd local pp_dif = string(p_dif_v2*100, "%8.3fc")
		estadd local pp_dif_a = string(p_dif_v2_capx*100, "%8.3fc")
		estadd local pp_dif_b = string(p_dif_v2_ppent*100, "%8.3fc")
		estadd local pp_dif_c = string(p_dif_v2_sale*100, "%8.3fc")
eststo reg_4: reg ETR new
		estadd local pp_dif = string(p_dif_v3*100, "%8.3fc")
		estadd local pp_dif_a = string(p_dif_v3_capx*100, "%8.3fc")
		estadd local pp_dif_b = string(p_dif_v3_ppent*100, "%8.3fc")
		estadd local pp_dif_c = string(p_dif_v3_sale*100, "%8.3fc")
		

esttab reg_? using "$output/Tables/table_SOI_ETR_robustness.tex", keep(_cons) stats() ///
b(3) se par label scalars("pp_dif Allocated by Employment" "pp_dif_a Allocated by Capex" "pp_dif_b Allocated by PPE" "pp_dif_c Allocated by Sales" ) ///
noobs nogap nomtitle tex replace nonum nocons postfoot( ///
"\hline S936 Exposed Industry &   & Y  &  & Y \\ " /// 
"S936 Exposed Sector &   &  &  Y & Y  \\ \hline\hline" ///
"\end{tabular} }" ) sty(tex)	 prefoot("") posthead("")
