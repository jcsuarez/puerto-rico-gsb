clear
set more off
snapshot erase _all
// This file 
// It builds off of the segments regression and the ETR variables
// Author: dan
// date 11-11-2018

/*
The results of this file showcase the fact that the taking the average change in 
ETR across industries is not equal to the average change in ETR using the TotT method.
*/
	

**************************************************
* Making the 3 point estimates of the tax change for each sample
**************************************************
use "$output_NETS/industry_cat_emp_sums_v2", clear
gen n2 = substr(industry_code,1,2)
replace industry_code = "11" if n2 == "11"
replace industry_code = "21" if n2 == "21"
replace industry_code = "23" if n2 == "23"
replace industry_code = "42" if n2 == "42"
replace industry_code = "44" if n2 == "44"
replace industry_code = "45" if n2 == "45" 
replace industry_code = "48" if n2 == "48"
replace industry_code = "49" if n2 == "49"
replace industry_code = "51" if n2 == "51"
replace industry_code = "53" if n2 == "53"
replace industry_code = "54" if n2 == "54"
replace industry_code = "61" if n2 == "61"
replace industry_code = "62" if n2 == "62"
replace industry_code = "71" if n2 == "71"

collapse (sum) emp95 pr_emp annual_avg_estabs annual_avg_emplvl total_annual_wages, by(industry_code)
rename industry_code n3

merge 1:1 n3 using "$output_NETS/ind_pr"
* calculating a scalar for each of the 3 categories:
gen link = pr_emp / emp95
gen n2_temp = substr(n3, 1,2)
egen n2 = group(n2_temp)	

gen naics_SOI = n3
replace naics_SOI = "11" if n2_temp == "11"
replace naics_SOI = "21" if n2_temp == "21"
replace naics_SOI = "22, 48-49" if n2_temp == "22" | n2_temp == "48" | n2_temp == "49"
replace naics_SOI = "23" if n2_temp == "23"
replace naics_SOI = "312-312" if inlist(n3,"311", "312")
replace naics_SOI = "313-314" if inlist(n3,"313", "314")
replace naics_SOI = "332, 333, 334, 335, 339" if inlist(n3,"332", "333", "335", "339")
replace naics_SOI = "42" if n2_temp == "42"
replace naics_SOI = "44-45" if inlist(n2_temp,"45", "44")
replace naics_SOI = "51, 54, 56, 61, 62, 71, 72, 81" if inlist(n2_temp,"51", "54", "56", "61", "62", "71", "72", "81")
replace naics_SOI = "52, 53, 55" if inlist(n2_temp,"52", "53", "55")

rename naics_SOI NAICS02
drop _merge
collapse (sum) emp95 pr_emp annual_avg_estabs annual_avg_emplvl total_annual_wages, by(NAICS02)
gen link = pr_emp / emp95

** getting the SOI variables for the relevant samples:	
merge 1:m NAICS02 using "$SOI/aggregate_SOI"
replace variable = subinstr(variable,"_t_","",.)
replace variable = substr(variable,1,length(variable)-1) if substr(variable,length(variable)-2,length(variable)) == "_"
replace NAICS02 = "311-312" if NAICS02 == "312-312"

keep if _merge == 3
drop _merge

	* defining ETR variable based on income taxes paid (not total taxes paid)
	gen ETR = Income_Tax_After_Credits / Income_Subject_To_Tax // Income_Subject_To_Tax
	gen net_margin = Net_Income / Receipts
	gen tax = Income_Tax_After_Credits
	gen lntax = ln(tax)

	* defining Us credit as a portion of Taxable income
	gen pos_cred = Us_Possessions_Tax_Credit / Income_Subject_To_Tax
	replace pos_cred = 0 if pos_cred == .
	gen raw_pos = Us_Possessions_Tax_Credit
	gen ln_pos = ln(raw_pos)
	

** Generating weights that are constant over time for each industry based on 1995 levels {
foreach var of var Us_Possessions_Tax_Credit Income_Subject_To_Tax Net_Income Receipts {
gen `var'_temp = `var' if inlist(year,1994,1995)
bys variable: egen `var'_base = mean(`var'_temp)
drop *temp

}
rename Income_Subject_To_Tax_base ISTT
rename Us_Possessions_Tax_Credit_base USPTC
keep if year == 2007

gen share = USPTC / ISTT
replace share = 0 if share == .

gen change = 100 * share * .8 / link

keep emp95 pr_emp Receipts* Net_Income* link share change NAICS02 ISTT USPTC annual_avg_estabs annual_avg_emplvl total_annual_wages

save "$SOI/aggregate_SOI_merged_NETS_QCEW", replace

sum change [aw=pr_emp], d
sum change [aw=emp95], d
sum change [aw=ISTT], d
sum change [aw= annual_avg_emplvl ], d
sum change [aw= Receipts_base ], d
sum change [aw= Net_Income_base ], d
sum change, d
