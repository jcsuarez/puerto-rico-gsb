* 
* Runs Event-Study Analysis robustness table with  
* Author: dan
* date of update: 20170129

* Note this code is adapted from es_county_d in January 2017

clear all
set more off
 
// First Define Command for ES data 
capture program drop ES_graph_data
program define ES_graph_data
syntax,  level(real) [  yti(string) tshifter(real) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
rename es_b es_b_`yti'
svmat time
svmat es_ci_l
rename es_ci_l es_ci_l`yti'
svmat es_ci_h 
rename es_ci_h es_ci_h`yti'
}

end  
 
// Second Define Command for ES 
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(dash))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Estimate") label(2 "`level'% CI")) ///
	ytitle("`yti'") ///
	note("`note'") ylab("`ylab'")
graph export "`outname'.pdf", replace	

end 


// A - Merge Data for Analysis 
// A.0. Get regional crosswalk read 
use "$xwalk/cw_cty_czone.dta", clear
rename cty_fips pid 
duplicates drop 
tempfile cross
save "`cross'"

// A.0. Get link data 
use "$output_NETS/pr_link_est_countyXindustry_d.dta", clear 
gen pr_emp = pr_link*total if industry_cd < 13
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)

gen pr_l = tot_pr/tot_emp 
keep pr_l fips
rename  fips pid
rename pr_l pr_link_ind
duplicates drop 
tempfile pr 
save "`pr'"

use "$output_NETS/pr_link_est_county_d.dta", clear 
rename  fips pid
tempfile pr2 
save "`pr2'" 

// A.1. Get QCEW data 
use "$qcewdata/extract_qcew_1990_2012_naic3.dta", clear
keep if strlen(industry_code) == 3
drop if industry_code == "101" | industry_code == "102"

order fips_state fips_county year industry_code
drop if fips_county == 0 & fips_state != 72
drop if fips_state == 72 & fips_county != 0
keep if own_code == 5		// 0-total covered; 1 Federal; 2 State; 3 Local; 4 Intl Govt; 5 Private.
keep fips_state fips_county year industry_code annual_avg_estabs annual_avg_emplvl total_annual_wages
drop if fips_state == 78

collapse (sum) annual_avg_estabs (sum) annual_avg_emplvl (sum) total_annual_wages, by(fips_state fips_county year industry_code)

// A.3. Fill in gaps
gen pid = 1000*fips_state+fips_county

// A.6. Merge in pr link 
merge m:1 pid using "`pr'"
keep if _merg == 3
drop _merge 

merge m:1 pid using "`pr2'"
drop if year == . 
drop if pr_link == . 
drop _merge 
rename pr_link pr_link_all

*replace pr_link_all = pr_link_ind

sum pr_link_al, d 

drop if year == . 

*collapse (rawsum) annual_avg_estabs (rawsum) annual_avg_emplvl (rawsum) total_annual_wages (mean) pr_link_ind (mean) pr_link_all [fw=total] , by( fips_state pid year industry_code) // Dan: this isn't actually collapsing anything


egen pid2 = group(pid indus)

tsset pid2 year
tsfill, full

foreach var of varlist fips_state  {
bysort pid2: egen temp = max(`var')
replace `var' = temp
drop temp
}

foreach var of varlist annual_avg_estabs annual_avg_emplvl total_annual_wages {
replace `var' = 0 if `var' == .
}

// A.4.  Growth rates. Base year: 1995
gen temp = annual_avg_emplvl if year == 1995
bysort pid2: egen base_emp = max(temp)
drop temp

gen emp_growth = (annual_avg_emplvl - base_emp)/base
replace emp_growth = 0 if emp_growth == . 


// Income 
gen inc = total_annual_wages
* /annual_avg_emplvl
gen temp = inc if year == 1995
bysort pid2: egen base_inc = max(temp)
drop temp

gen inc_growth = (inc - base_inc)/base_inc
replace inc_growth = 0 if inc_growth == . 

// Estabs 
gen estab = annual_avg_estabs
gen temp = estab if year == 1995
bysort pid2: egen base_estab = max(temp)
drop temp

gen estab_growth = (estab - base_estab)/base_estab
replace estab_growth = 0 if estab_growth == . 


// A.5. Define PR Growth Data
/*
gen temp = emp_growth if fips_state == 72
bysort year ind: egen emp_growth_PR = max(temp) 
drop temp
drop if emp_growth_PR == . 
*/



** For now, keep only balanced industries . Matt: can you fix it so we don't have to do this? 
bys pid2: gen count = _N
keep if count == 23 
drop count 

drop if base_emp ==0 

sum base_emp if year == 1995, d
gen wgt=base_emp/(r(N) * r(mean))
winsor wgt, p(.01) gen(wgt_w)

sum pr_link_i $pr_wgt if base_emp > 0, d 
replace pr_link_i = pr_link_i/(r(p75)-r(p25))

sum pr_link_al  $pr_wgt if base_emp > 0, d 
replace pr_link_al = pr_link_al/(r(p75)-r(p25))

tsset pid2 year 
egen ind_st = group(ind fips_state)

// B. Graphs with All PR Link 
cd "$output" 
set more off 

drop if base_emp ==0 

*keep if base_emp > 400  
winsor base_emp , g(w_emp_base) p(.025)
*replace base_emp = w_emp_base
sum base_emp, d 
*drop if base_emp <= r(p1) 
*drop if base_emp >= r(p99)
destring industry_code, replace

xtile bin_PR_i = pr_link_i if pr_link_i != 0, n(3)
replace bin_PR_i = 4 if pr_link_i == 0
*xtile bin_PR_a = pr_link_a if pr_link_a != 0, n(3)
*replace bin_PR_a = 4 if pr_link_a == 0

xi i.year*i.bin_PR_i $pra_control, noomit
drop _IyeaX*1995* _IyeaXbin*4 // dropping year 1995 and PR_Link_i == 0 

est clear 
set more off 
eststo emp_2:  reghdfe emp_growth _IyeaX* [aw=base_emp] , a(Y=i.year#i.industr A=i.pid) cl(fips_state indust)
gen year_hat = Y - A + _b[_cons]					
	
	/* pre 5.2 for reghdfe
	predict D, d
	gen year_hat = D - A // Note D includes the Constant while Y and A do not, so year_hat = Y + constant
						 // https://www.statalist.org/forums/forum/general-stata-discussion/general/1327487-reghdfe-and-predict
						 // Further note the constant in this case is about 0.083 so it matters quite a bit
	*/	
* saving yearly PR interaction terms	
qui { 
mat b = e(b)
svmat b
}

* Generating a single variable dependent on year that has the PR interaction effect
egen year_beta1 = rowfirst(b1)
egen year_beta2 = rowfirst(b2)
egen year_beta3 = rowfirst(b3)
forval j = 1/3{
forval k = 1/5  {
local i = `j' + (`k' - 1) * 3

qui sum b`i'
local b_mean = r(mean)
replace b`i' = `b_mean' if b`i' == . 
local year  = `k' +  1989
replace year_beta`j' = b`i' if year == `year'

}
forval k = 6/22 {
local i = `j' + (`k' - 1) * 3

qui sum b`i'
local b_mean = r(mean)
replace b`i' = `b_mean' if b`i' ==. 
local year  = `k' +  1990
replace year_beta`j' = b`i' if year == `year'
}
}

drop b??
replace year_beta1 = 0 if year_beta1 == .
replace year_beta2 = 0 if year_beta2 == .
replace year_beta3 = 0 if year_beta3 == .

collapse (mean) year_hat (first) year_beta? [fw=base_emp], by(year)	

replace year_beta1 = (year_beta1 + year_hat) * 100
replace year_beta2 = (year_beta2 + year_hat) * 100
replace year_beta3 = (year_beta3 + year_hat) * 100
 
* graphing
 graph twoway (scatteri -10 1990.5   -10 1991.25 , recast(area) color(gs12) ) ///
     (scatteri 30 1990.5   30 1991.25 , recast(area) color(gs12) ) ///
     (scatteri -10 2001   -10 2002 , recast(area) color(gs12) ) ///
     (scatteri 30 2001   30 2002 , recast(area) color(gs12) ) ///
     (scatteri -10 2007.75   -10 2009.75 , recast(area) color(gs12) ) ///
     (scatteri 30 2007.75   30 2009.75 , recast(area) color(gs12) ) ///  
	(scatter year_beta1 year, lcolor(navy) mcolor(navy) c(direct) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(dash)) msize(small)) ///
    (scatter year_beta3 year, lcolor(orange) mcolor(orange) c(direct) lpattern(dash) msize(small)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white))  ///
	xtitle("Year") xlab(1990(5)2012) bgcolor(white) ///
	legend(on order (7 8 1) label(7 "Low S936 Exposure")  ///
	label(8 "High S936 Exposure") label(1 "Recession") r(1)) ///
	yti("Percentage Change in Employment", margin(medium))
graph export "$output/Graphs/fig_7_extra_nonpar_shading.pdf", replace	
