* This .do file makes a massive table of all ES Graph Data in one graph
*
* Author: Dan Garrett
* Date of Creation: 1-29-2018

clear
set more off 

*** Importing long data 
use "$qcewdata/ES_graph.dta", clear

* moving times around to kind of work
replace spec1_time = spec1_time - .1
replace spec2_time = spec2_time - .1
*replace spec3_time = spec3_time - .4
replace spec4_time = spec4_time - .2
replace spec5_time = spec5_time + .1
replace spec6_time = spec6_time + .1

foreach i of numlist 1 2 4 5 6 {
gen spec`i'_time_a = spec`i'_time - .033
gen spec`i'_time_b = spec`i'_time + .033
}

foreach Q of var *spec?_es_* {
replace `Q' = `Q' // * 100 
}


// DAN's new graph just combining # 2 from conspuma and CZ
 twoway (line cons_spec1_es_b spec1_time			, lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid)))  ///
		(rcap cons_spec1_es_ci_l cons_spec1_es_ci_h spec1_time	, lcolor(navy)  lpattern(solid))  ///
		(line czone_spec1_es_b spec2_time							, lcolor(orange)  lpattern(dash))      ///
		(rcap czone_spec1_es_ci_l czone_spec1_es_ci_h spec2_time	, lcolor(orange)  lpattern(dash))   /// 
		(line spec2_inst_es_b spec6_time							, lcolor(red)  lpattern(shortdash))      ///
		(rcap spec2_inst_es_ci_l spec2_inst_es_ci_h spec6_time	, lcolor(red)  lpattern(shortdash))   /// 
		(line spec2_es_b spec4_time							, lcolor(black)  lpattern(longdash))      ///
		(rcap spec2_es_ci_l spec2_es_ci_h spec4_time	, lcolor(black)  lpattern(longdash))   /// 
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") ytitle("Percent Employment Growth" , height(3) margin(medsmall))  ///
	xlab(1990(5)2012) bgcolor(white) ///
	legend(label(1 "Consupuma") ///
	label(3 "Commuting Zone") label(5 "Instrument for Employment Link") label(7 "Benchmark") order(7 1 3 5)) 
	
graph export "$output/Graphs/fig_7_robust_cons_and_cz.pdf", replace	

// md, fd, and compu
*** Importing long data 
merge 1:1 year using  "$qcewdata/ES_graph_md.dta"

drop *_time

* moving times around to kind of work
gen spec1_time = year - .2
gen spec2_time = year - .066
gen spec4_time = year + .066
gen spec5_time = year + .2


 twoway (line spec2_md_es_b spec1_time						, lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid)))  ///
		(rcap spec2_md_es_ci_l spec2_md_es_ci_h spec1_time	, lcolor(navy)  lpattern(solid))  ///
		(line spec2_fd_es_b spec2_time						, lcolor(orange)  lpattern(dash))      ///
		(rcap spec2_fd_es_ci_l spec2_fd_es_ci_h spec2_time	, lcolor(orange)  lpattern(dash))   /// 
		(line spec2_compu_es_b spec5_time							, lcolor(red)  lpattern(shortdash))      ///
		(rcap spec2_compu_es_ci_l spec2_compu_es_ci_h spec5_time	, lcolor(red)  lpattern(shortdash))   /// 
		(line spec2_es_b spec4_time							, lcolor(black)  lpattern(longdash))      ///
		(rcap spec2_es_ci_l spec2_es_ci_h spec4_time	, lcolor(black)  lpattern(longdash))   /// 
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") ytitle("Percent Employment Growth" , height(3) margin(medsmall))  ///
	xlab(1990(5)2012) bgcolor(white) ///
	legend(label(1 "Large Retailers Dropped") ///
	label(3 "Uncertain HQ Dropped") label(5 "Non-Compustat Dropped") label(7 "Benchmark") order(7 1 3 5)) 
	
graph export "$output/Graphs/fig_7_robust_md_and_fd.pdf", replace	

// differently combined versions

 twoway (line cons_spec1_es_b spec1_time			, lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid)))  ///
		(rcap cons_spec1_es_ci_l cons_spec1_es_ci_h spec1_time	, lcolor(navy)  lpattern(solid))  ///
		(line czone_spec1_es_b spec2_time							, lcolor(orange)  lpattern(dash))      ///
		(rcap czone_spec1_es_ci_l czone_spec1_es_ci_h spec2_time	, lcolor(orange)  lpattern(dash))   /// 
		(line spec2_PC_es_b spec5_time							, lcolor(red)  lpattern(shortdash))      ///
		(rcap spec2_PC_es_ci_l spec2_PC_es_ci_h spec5_time	, lcolor(red)  lpattern(shortdash))   /// 
		(line spec2_es_b spec4_time							, lcolor(black)  lpattern(longdash))      ///
		(rcap spec2_es_ci_l spec2_es_ci_h spec4_time	, lcolor(black)  lpattern(longdash))   /// 
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") ytitle("Percent Employment Growth" , height(3) margin(medsmall))  ///
	xlab(1990(5)2012) bgcolor(white) ///
	legend(label(1 "Consupuma") ///
	label(3 "Commuting Zone") label(5 "Per Capita") label(7 "Benchmark") order(7 1 3 5)) 

graph export "$output/Graphs/fig_7_robust_new1.pdf", replace	


 twoway (line spec2_md_es_b spec1_time						, lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid)))  ///
		(rcap spec2_md_es_ci_l spec2_md_es_ci_h spec1_time	, lcolor(navy)  lpattern(solid))  ///
		(line spec2_fd_es_b spec2_time						, lcolor(orange)  lpattern(dash))      ///
		(rcap spec2_fd_es_ci_l spec2_fd_es_ci_h spec2_time	, lcolor(orange)  lpattern(dash))   /// 
		(line spec2_compu_es_b spec5_time							, lcolor(red)  lpattern(shortdash))      ///
		(rcap spec2_compu_es_ci_l spec2_compu_es_ci_h spec5_time	, lcolor(red)  lpattern(shortdash))   /// 
		(line spec2_inst_es_b spec4_time							, lcolor(black)  lpattern(longdash))      ///
		(rcap spec2_inst_es_ci_l spec2_inst_es_ci_h spec4_time	, lcolor(black)  lpattern(longdash))   /// 
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") ytitle("Percent Employment Growth" , height(3) margin(medsmall))  ///
	xlab(1990(5)2012) bgcolor(white) ///
	legend(label(1 "Large Retailers Dropped") ///
	label(3 "Uncertain HQ Dropped") label(5 "Non-Compustat Dropped") label(7 "Instrument for Employment Link") order(7 1 3 5)) 


graph export "$output/Graphs/fig_7_robust_new2.pdf", replace


************************************************
* graph with 1993, large firms, state-by-year FE, and 1
************************************************
capture: drop _merge
merge 1:1 year using "$qcewdata/ES_graph_sy_fe.dta"

 twoway (line spec1_es_b spec1_time						, lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid)))  ///
		(rcap spec1_es_ci_l spec1_es_ci_h spec1_time	, lcolor(navy)  lpattern(solid))  ///
		(line large_spec5_es_b spec2_time						, lcolor(orange)  lpattern(dash))      ///
		(rcap large_spec5_es_ci_l large_spec5_es_ci_h spec2_time	, lcolor(orange)  lpattern(dash))   /// 
		(line y95_spec1_es_b spec5_time							, lcolor(red)  lpattern(shortdash))      ///
		(rcap y95_spec1_es_ci_l y95_spec1_es_ci_h spec5_time	, lcolor(red)  lpattern(shortdash))   /// 
		(line y93_spec1_es_b spec4_time							, lcolor(black)  lpattern(longdash))      ///
		(rcap y93_spec1_es_ci_l y93_spec1_es_ci_h spec4_time	, lcolor(black)  lpattern(longdash))   /// 
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") ytitle("Percent Employment Growth" , height(3) margin(medsmall))  ///
	xlab(1990(5)2012) bgcolor(white) ///
	legend(label(1 "State Time Trends and FE") ///
	label(3 "Firms > 50,000 Employees") label(5 "Establishment Links in 1993") label(7 "S936 Firms in 1993") order(1 3 5 7)) 

graph export "$output/Graphs/fig_7_robust_new3.pdf", replace
