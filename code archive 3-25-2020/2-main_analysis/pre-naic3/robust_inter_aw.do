//update 03-05-2018: Dan added aweights


use "$data/data_additional/St Tax Structure 05_10_2017/state_taxes_analysis.dta", clear
keep if year == 1990 
keep fips salestax propertytax corporate_rate GDP rev_totaltaxes rec_val rdcred rdcred trainingsubsidy jobcreationcred corpinctax totalincentives investment_credit 
rename fips statefips
tempfile incentives 
save "`incentives'"


use "$data/data_additional/Misallocation 5_10_2017/misallocation_paneldata.dta", clear
keep if year == 1990 
keep income_rate_avg fipstate
rename fips statefips
tempfile incentives2 
save "`incentives2'"

* 06/2016
* Merge NTES Link Data and QCEW Outcome Data 
* Runs Event-Study Analyses 

clear all
set more off

/*
******* Set paths 
* For JC 
global dropbox "/Users/jcsuarez/Dropbox (JC's Data Emporium)/RESEARCH/ra tasks"
* For Matt 
*global dropbox "/Users/mattpanhans/Dropbox (JC's Data Emporium)"

**** Relative Paths 
global linkdata "$dropbox/matt ra work/Puerto Rico/output/base_1995"
global qcewdata "$dropbox/matt ra work/Puerto Rico/QCEW"     
global output   "$dropbox/matt ra work/Puerto Rico/QCEW/output"
global cross 	"$dropbox/../completed projects/Local_Econ_Corp_Tax/Data/Regional Crosswalk Data/Conspuma2CTY"
 */
 
// First Define Command for ES 
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(dash))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Estimate") label(2 "`level'% CI")) ///
	ytitle("`yti'") ///
	note("`note'") ylab("`ylab'")
graph export "`outname'.pdf", replace	

end 


// A - Merge Data for Analysis 
// A.0. Get regional crosswalk read 
use "$xwalk/cw_cty_czone.dta", clear
rename cty_fips pid 
duplicates drop 
tempfile cross
save "`cross'"

// A.0. Get link data 
use "$output_NETS/pr_link_est_countyXindustry_d.dta", clear 
gen pr_emp = pr_link*total if industry_cd < 13
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)

gen pr_l = tot_pr/tot_emp 
keep pr_l fips
rename  fips pid
rename pr_l pr_link_ind
duplicates drop 
tempfile pr 
save "`pr'" 

use "$output_NETS/pr_link_est_county_d.dta", clear 
rename  fips pid
tempfile pr2 
save "`pr2'" 

// A.1. Get QCEW data 
use "$qcewdata/extract_qcew_1990_2012.dta", clear


order fips_state fips_county year industry_code
drop if fips_county == 0 & fips_state != 72
drop if fips_state == 72 & fips_county != 0
keep if own_code == 5		// 0-total covered; 1 Federal; 2 State; 3 Local; 4 Intl Govt; 5 Private.
keep fips_state fips_county year industry_code annual_avg_estabs annual_avg_emplvl total_annual_wages
drop if fips_state == 78

// A.2. Label industries		
gen industry_cd = .
replace industry_cd = 0 if industry_code == "10"	// "Total, all industries"
replace industry_cd = 1 if industry_code == "311"
replace industry_cd = 2 if industry_code == "314"
replace industry_cd = 3 if industry_code == "315"
replace industry_cd = 4 if industry_code == "325"	// note: include 3254
replace industry_cd = 5 if industry_code == "3254"
replace industry_cd = 6 if industry_code == "326"
replace industry_cd = 7 if industry_code == "316"
replace industry_cd = 8 if industry_code == "332"
replace industry_cd = 9 if industry_code == "333"
replace industry_cd = 10 if industry_code == "335"
*replace industry_cd = 11 if naic3 == 
replace industry_cd = 12 if industry_code == "31-33"
replace industry_cd = 13 if industry_code == "52" | industry_code == "53"
replace industry_cd = 14 if inlist(industry_code,"54","55","61","62","71","72","81")
replace industry_cd = 15 if inlist(industry_code,"42","44-45")
*replace industry_cd = 16 if industry_code == .

label define l_naic3 0 "Total, all industries" 1 "Food Mfg" 2 "Textile mill products" 3 "Apparel" 4 "Chemicals" 5 "Pharmaceuticals" ///
 6 "Rubber and Plastic" 7 "Leather" 8 "Fabricated metal" 9 "Machinery" 10 "Electrical equip" ///
 11 "Instruments" 12 "Other mfg" 13 "Finance, insurance, real estate" 14 "Services" ///
 15 "Wholesale and retail" 16 "Other non-mfg", replace
label values industry_cd l_naic3

drop if industry_cd == 0
collapse (sum) annual_avg_estabs (sum) annual_avg_emplvl (sum) total_annual_wages, by(fips_state fips_county year industry_cd)

// A.3. Fill in gaps
gen pid = 1000*fips_state+fips_county

// A.6. Merge in pr link 
merge m:1 pid using "`pr'"
keep if _merg == 3
drop _merge 

merge m:1 pid using "`pr2'"
drop if year == . 
drop if pr_link == . 
drop _merge 
rename pr_link pr_link_all

*replace pr_link_all = pr_link_ind

sum pr_link_al, d 
drop if year == . 

collapse (rawsum) annual_avg_estabs (rawsum) annual_avg_emplvl (rawsum) total_annual_wages (mean) pr_link_ind (mean) pr_link_all [fw=total] , by( fips_state pid year industry_cd)


egen pid2 = group(pid indus)

tsset pid2 year
tsfill, full

foreach var of varlist fips_state  {
bysort pid2: egen temp = max(`var')
replace `var' = temp
drop temp
}

foreach var of varlist annual_avg_estabs annual_avg_emplvl total_annual_wages {
replace `var' = 0 if `var' == .
}

// A.4.  Growth rates. Base year: 1995
gen temp = annual_avg_emplvl if year == 1995
bysort pid2: egen base_emp = max(temp)
drop temp

gen emp_growth = (annual_avg_emplvl - base_emp)/base
replace emp_growth = 0 if emp_growth == . 


// Income 
gen inc = total_annual_wages/annual_avg_emplvl
gen temp = inc if year == 1995
bysort pid2: egen base_inc = max(temp)
drop temp

gen inc_growth = (inc - base_inc)/base_inc
replace inc_growth = 0 if inc_growth == . 

// Estabs 
gen estab = annual_avg_estabs
gen temp = estab if year == 1995
bysort pid2: egen base_estab = max(temp)
drop temp

gen estab_growth = (estab - base_estab)/base_estab
replace estab_growth = 0 if estab_growth == . 


// A.5. Define PR Growth Data
/*
gen temp = emp_growth if fips_state == 72
bysort year ind: egen emp_growth_PR = max(temp) 
drop temp
drop if emp_growth_PR == . 
*/



** For now, keep only balanced industries . Matt: can you fix it so we don't have to do this? 
bys pid2: gen count = _N
keep if count == 23 
drop count 

sum pr_link_i, d 
replace pr_link_i = pr_link_i/(r(p75)-r(p25))

sum pr_link_al, d 
replace pr_link_al = pr_link_al/(r(p75)-r(p25))


// Add demographics and other shocks 
gen fips_county = pid 
merge m:1 fips_county using "$additional/county_level_data_all.dta"
keep if _merge == 3 
drop _merge 

merge m:1 statefips  using "`incentives'"
keep if _merge == 3 
drop _merge 

merge m:1 statefips using "`incentives2'"
keep if _merge == 3 
drop _merge 


** Add nafta variables 
merge m:1 fips_county using "$output/import_hakobyanlaren_nafta.dta"
drop if _merge == 2 
drop _merge 

tsset pid2 year 
egen ind_st = group(ind fips_state)


**** Merge in share of firm size dist 
merge m:1 pid year using "$output/CBP_firm_size_dist"
drop if _merge == 2
drop _merge 

foreach var of varlist n1_4-n250p { 
	sum `var', d
	replace `var' = `var'/(r(p75)-r(p25))
	
	gen temp = `var' if year == 1995
	bysort pid2: egen base_`var' = max(temp)
	drop temp

	gen `var'_growth = (`var' - base_`var')/base_`var'
	replace `var'_growth = 0 if `var'_growth == . 
}


*gen ln_pr = ln(1+pr_link_a) 
*egen std_ln = std(ln_pr)
*winsor std_ln , g(w_std_ln) p(.01) 

bys statefips  : egen state_pop = total(population )
replace rev_totaltaxes = rev_tot/state_pop
replace capital_stock = capital_stock/state_pop

egen std_total = std(totalincentives )
egen std_job = std(jobcreationcred )
egen std_train = std(trainingsubsidy )
egen std_mw = std(realmw  )
egen std_rtw = std(rgtowork  )
egen std_rd = std(rdcred )
egen std_ic = std(investment_credit )
egen std_corp = std(corporate_rate  )
egen std_ptax = std(income_rate_avg  )
egen std_prop = std(propertytax )
egen std_sales = std(salestax )
*egen std_capital = std(capital_stock )
egen std_trade = std(d_tradeusch_pw )
egen std_rev = std(rev_totaltaxes )
egen std_routine = std(l_sh_routine33a )
egen std_nafta = std(locdt_noag)

egen std_n1 = std( n5_9_growth )
egen std_n2 = std( n10_19_growth ) 
egen std_n3 = std( n20_49_growth )
egen std_n4 = std( n50_99_growth )
egen std_n5 = std( n100_249_growth ) 
egen std_n6 = std( n250p_growth )

/*
egen std_n1 = std(n1_4)
egen std_n2 = std(n20_49)
egen std_n3 = std(n250p)
*/

foreach var of varlist std_* { 
	winsor `var', g(w_`var') p(.01)
	replace `var' = w_`var'
}


sum pr_link_ind [fw=base_emp] if inrange(year,2004,2008), d
gen cen_pr_link_i = pr_link_i -r(mean) 

// Added aweights 3-5-2018 ***************

sum base_emp, d
gen wgt=base_emp/(r(N) * r(mean))
winsor wgt, p(.01) gen(wgt_w)

** Robustness tables 
est clear 
eststo rob_0: reghdfe emp_growth cen_pr_link_i [aw=wgt] if inrange(year,2004,2008) ,  a( i.year  i.industr) cl(fips_state  i.industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hass "Yes"		
foreach var of varlist std_* { 
	eststo rob_`var': reghdfe emp_growth cen_pr_link_i `var' [aw=wgt] if inrange(year,2004,2008) ,  a( i.year  i.industr) cl(fips_state  i.industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hass "Yes"		

	} 
eststo rob_all: reghdfe emp_growth cen_pr_link_i std_* [aw=wgt] if inrange(year,2004,2008) ,  a( i.year  i.industr) cl(fips_state  i.industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hass "Yes"		

label var cen_pr_link_i "Robustness of Effect of Exposure to S936 on Employment Growth 2004-2008"
	coefplot rob_0 rob_std_total rob_std_job rob_std_train rob_std_mw ///
	rob_std_rtw rob_std_rd rob_std_ic rob_std_corp rob_std_ptax rob_std_prop ///
	rob_std_sales rob_std_trade rob_std_rev rob_std_routine rob_std_nafta rob_all ,  keep(cen_pr_link_i) ///
	graphregion(color(white)) vertical ylab(-.1(.02)0) yline(0) yline( -.0728106, lpattern(dash) lcolor(navy)) ///
	legend(  label(2 "Base") label(4 "Incentives") ///
	label(6 "Job") ///
	label(8 "Training") ///
	label(10 "Min Wage") ///
	label(12 "Rgt to Wrk") ///
	label(14 "RD Cred") ///
	label(16 "Inv Cred") ///
	label(18 "Corp Tax") ///
	label(20 "Pers Tax") ///
	label(22 "Prop Tax") ///
	label(24 "Sales Tax") ///
	label(26 "Trade (China)") ///
	label(28 "Revenue") ///
	label(30 "Routine") ///	
	label(32 "Trade (Nafta)") ///	
	label(34 "All") cols(5))
	
	graph export "$output/Graphs/robust.pdf", replace 
	
** Robustness tables 
est clear 
eststo rob_0: reghdfe emp_growth cen_pr_link_i [aw=wgt] if inrange(year,2004,2008) ,  a( i.year  i.fips_st i.industr) cl(fips_state  i.industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hass "Yes"		
foreach var of varlist std_* { 
	eststo rob_`var': reghdfe emp_growth cen_pr_link_i `var' [aw=wgt] if inrange(year,2004,2008) ,  a( i.year  i.fips_st i.industr) cl(fips_state  i.industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hass "Yes"		

	} 
eststo rob_all: reghdfe emp_growth cen_pr_link_i std_* [aw=wgt] if inrange(year,2004,2008) ,  a( i.year  i.fips_st i.industr) cl(fips_state  i.industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hass "Yes"			
	
	
esttab rob* using "$output/Tables/table_robust_QCEW.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons ///
postfoot("")

esttab rob*   using "$output/Tables/table_robust_QCEW.tex",   ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s() noobs scalars(  "hasy Year Fixed Effects" "hasind Industry Fixed Effects" ///
"hass State Fixed Effects") /// 
coeflabel(cen_pr_link_i "Exposure to Section 936 "  /// 
std_total "Total Incentives (Bartik)"  /// 
std_job "Job Creation Incentive" /// 
std_train "Job Training Subsidy" /// 
std_mw "Real Minimum Wage" /// 
std_rtw "Right to Work" /// 
std_rd "RD Tax Credit" /// 
std_ic "Investment Tax Credit" /// 
std_corp "Corporate Income Tax" /// 
std_ptax "Personal Income Tax" /// 
std_prop "Property Tax"  /// 
std_sales "Sales Tax" /// 
std_capital "Capital Stock" /// 
std_trade "Trade Exposure (ADH)" /// 
std_rev "State Revenue Per Capita" /// 
std_routine "Share Routine Workers (Autor)" ///
pr_link_ind "\hspace{1em}Exposure to Section 936 ") ///
label /// 
 prefoot("\hline") ///
postfoot( ///
"  \\ \hline\hline" ///
"\end{tabular} }" ) append sty(tex)


** Separate into two tables 

esttab rob_0 rob_std_total rob_std_job rob_std_train ///
rob_std_mw rob_std_rtw rob_std_rd rob_std_ic rob_std_corp using "$output/Tables/table_robust_QCEW_1.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons ///
postfoot("")

esttab rob_0 rob_std_total rob_std_job rob_std_train ///
rob_std_mw rob_std_rtw rob_std_rd rob_std_ic rob_std_corp   using "$output/Tables/table_robust_QCEW_1.tex",   ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s() noobs scalars(  "hasy Year Fixed Effects" "hasind Industry Fixed Effects" ///
"hass State Fixed Effects") /// 
coeflabel(cen_pr_link_i "Exposure to Section 936 "  /// 
std_total "Total Incentives (Bartik)"  /// 
std_job "Job Creation Incentive" /// 
std_train "Job Training Subsidy" /// 
std_mw "Real Minimum Wage" /// 
std_rtw "Right to Work" /// 
std_rd "RD Tax Credit" /// 
std_ic "Investment Tax Credit" /// 
std_corp "Corporate Income Tax" /// 
std_ptax "Personal Income Tax" /// 
std_prop "Property Tax"  /// 
std_sales "Sales Tax" /// 
std_capital "Capital Stock" /// 
std_trade "Trade Exposure (ADH)" /// 
std_rev "State Revenue Per Capita" /// 
std_routine "Share Routine Workers (Autor)" ///
pr_link_ind "\hspace{1em}Exposure to Section 936 ") ///
label /// 
 prefoot("\hline") ///
postfoot( ///
"  \\ \hline\hline" ///
"\end{tabular} }" ) append sty(tex)


esttab rob_0 rob_std_ptax rob_std_prop rob_std_sales /// 
 rob_std_trade rob_std_rev rob_std_routine rob_all ///
 using "$output/Tables/table_robust_QCEW_2.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons ///
postfoot("")

esttab rob_0 rob_std_ptax rob_std_prop rob_std_sales /// 
 rob_std_trade rob_std_rev rob_std_routine rob_all ///
 using "$output/Tables/table_robust_QCEW_2.tex",   ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s() noobs scalars(  "hasy Year Fixed Effects" "hasind Industry Fixed Effects" ///
"hass State Fixed Effects") /// 
coeflabel(cen_pr_link_i "Exposure to Section 936 "  /// 
std_total "Total Incentives (Bartik)"  /// 
std_job "Job Creation Incentive" /// 
std_train "Job Training Subsidy" /// 
std_mw "Real Minimum Wage" /// 
std_rtw "Right to Work" /// 
std_rd "RD Tax Credit" /// 
std_ic "Investment Tax Credit" /// 
std_corp "Corporate Income Tax" /// 
std_ptax "Personal Income Tax" /// 
std_prop "Property Tax"  /// 
std_sales "Sales Tax" /// 
std_capital "Capital Stock" /// 
std_trade "Trade Exposure (ADH)" /// 
std_rev "State Revenue Per Capita" /// 
std_routine "Share Routine Workers (Autor)" ///
pr_link_ind "\hspace{1em}Exposure to Section 936 ") ///
label /// 
 prefoot("\hline") ///
postfoot( ///
"  \\ \hline\hline" ///
"\end{tabular} }" ) append sty(tex)


// First take at interactions 
est clear 
eststo rob_0: reghdfe emp_growth cen_pr_link_i [aw=wgt] if inrange(year,2004,2008) ,  a( i.year  i.fips_st i.industr) cl(fips_state  i.industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hass "Yes"		
	estadd local hasc "Yes"		
foreach var of varlist std_* { 
		eststo rob_`var': reghdfe emp_growth cen_pr_link_i std_* c.cen_pr_link_i#c.`var' [aw=wgt] if inrange(year,2004,2008) ,  a( i.year  i.fips_st i.industr) cl(fips_state  i.industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hass "Yes"		
	estadd local hasc "Yes"		
	} 
eststo rob_all: reghdfe emp_growth cen_pr_link_i std_* c.cen_pr_link_i#(c.std_*) [aw=wgt] if inrange(year,2004,2008) ,  a( i.year  i.fips_st i.industr) cl(fips_state industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hass "Yes"		
	estadd local hasc "Yes"		

esttab rob* ///
 using "$output/Tables/table_inter_QCEW.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons ///
postfoot("")

esttab rob* ///
   using "$output/Tables/table_inter_QCEW.tex", drop(std*)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s() noobs scalars(  "hasy Year Fixed Effects" "hasind Industry Fixed Effects" ///
"hass State Fixed Effects" "hasc Includes all Controls") /// 
coeflabel(cen_pr_link_i "Exposure to Section 936 "  /// 
c.cen_pr_link_i#c.std_total "Total Incentives (Bartik)"  /// 
c.cen_pr_link_i#c.std_job "Job Creation Incentive" /// 
c.cen_pr_link_i#c.std_train "Job Training Subsidy" /// 
c.cen_pr_link_i#c.std_mw "Real Minimum Wage" /// 
c.cen_pr_link_i#c.std_rtw "Right to Work" /// 
c.cen_pr_link_i#c.std_rd "RD Tax Credit" /// 
c.cen_pr_link_i#c.std_ic "Investment Tax Credit" /// 
c.cen_pr_link_i#c.std_corp "Corporate Income Tax" /// 
c.cen_pr_link_i#c.std_ptax "Personal Income Tax" /// 
c.cen_pr_link_i#c.std_prop "Property Tax"  /// 
c.cen_pr_link_i#c.std_sales "Sales Tax" /// 
c.cen_pr_link_i#c.std_capital "Capital Stock" /// 
c.cen_pr_link_i#c.std_trade "Trade Exposure (ADH)" /// 
c.cen_pr_link_i#c.std_rev "State Revenue Per Capita" /// 
c.cen_pr_link_i#c.std_routine "Share Routine Workers (Autor)" ///
c.cen_pr_link_i#c.pr_link_ind "\hspace{1em}Exposure to Section 936 ") ///
label /// 
 prefoot("\hline") ///
postfoot( ///
"  \\ \hline\hline" ///
"\end{tabular} }" ) append sty(tex)

** Split tables 

esttab rob_0 rob_std_total rob_std_job rob_std_train ///
rob_std_mw rob_std_rtw rob_std_rd rob_std_ic rob_std_corp ///
 using "$output/Tables/table_inter_QCEW_1.tex", keep(cen_pr_link_i) stats() ///
cells(b(fmt(3)) se(par) p) label  ///
noobs nogap nomtitle tex replace nocons ///
postfoot("Interaction of Exposure with :") coeflabel(cen_pr_link_i "Exposure to Section 936 ")

esttab rob_0 rob_std_total rob_std_job rob_std_train ///
rob_std_mw rob_std_rtw rob_std_rd rob_std_ic rob_std_corp ///
  using "$output/Tables/table_inter_QCEW_1.tex", drop(std* cen_pr_link_i)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s() noobs scalars(  "hasy Year Fixed Effects" "hasind Industry Fixed Effects" ///
"hass State Fixed Effects" "hasc Includes all Controls") /// 
coeflabel(cen_pr_link_i "Exposure to Section 936 "  /// 
c.cen_pr_link_i#c.std_total "Total Incentives (Bartik)"  /// 
c.cen_pr_link_i#c.std_job "Job Creation Incentive" /// 
c.cen_pr_link_i#c.std_train "Job Training Subsidy" /// 
c.cen_pr_link_i#c.std_mw "Real Minimum Wage" /// 
c.cen_pr_link_i#c.std_rtw "Right to Work" /// 
c.cen_pr_link_i#c.std_rd "RD Tax Credit" /// 
c.cen_pr_link_i#c.std_ic "Investment Tax Credit" /// 
c.cen_pr_link_i#c.std_corp "Corporate Income Tax" /// 
c.cen_pr_link_i#c.std_ptax "Personal Income Tax" /// 
c.cen_pr_link_i#c.std_prop "Property Tax"  /// 
c.cen_pr_link_i#c.std_sales "Sales Tax" /// 
c.cen_pr_link_i#c.std_capital "Capital Stock" /// 
c.cen_pr_link_i#c.std_trade "Trade Exposure (ADH)" /// 
c.cen_pr_link_i#c.std_rev "State Revenue Per Capita" /// 
c.cen_pr_link_i#c.std_routine "Share Routine Workers (Autor)" ///
c.cen_pr_link_i#c.pr_link_ind "\hspace{1em}Exposure to Section 936 ") ///
label /// 
 prefoot("\hline") ///
postfoot( ///
"  \\ \hline\hline" ///
"\end{tabular} }" ) preh(" ") append sty(tex)

esttab  rob_0 rob_std_ptax rob_std_prop rob_std_sales /// 
 rob_std_trade rob_std_rev rob_std_routine rob_all ///
 using "$output/Tables/table_inter_QCEW_2.tex", coll(none) keep(cen_pr_link_i) stats() ///
cells(b(fmt(3)) se(par) p) label  ///
noobs nogap nomtitle tex replace nocons ///
postfoot("Interaction of Exposure with :")  coeflabel(cen_pr_link_i "Exposure to Section 936 ")

esttab  rob_0 rob_std_ptax rob_std_prop rob_std_sales /// 
 rob_std_trade rob_std_rev rob_std_routine rob_all ///
  using "$output/Tables/table_inter_QCEW_2.tex", drop(std* cen_pr_link_i)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s() noobs scalars(  "hasy Year Fixed Effects" "hasind Industry Fixed Effects" ///
"hass State Fixed Effects" "hasc Includes all Controls") /// 
coeflabel(cen_pr_link_i "Exposure to Section 936 "  /// 
c.cen_pr_link_i#c.std_total "Total Incentives (Bartik)"  /// 
c.cen_pr_link_i#c.std_job "Job Creation Incentive" /// 
c.cen_pr_link_i#c.std_train "Job Training Subsidy" /// 
c.cen_pr_link_i#c.std_mw "Real Minimum Wage" /// 
c.cen_pr_link_i#c.std_rtw "Right to Work" /// 
c.cen_pr_link_i#c.std_rd "RD Tax Credit" /// 
c.cen_pr_link_i#c.std_ic "Investment Tax Credit" /// 
c.cen_pr_link_i#c.std_corp "Corporate Income Tax" /// 
c.cen_pr_link_i#c.std_ptax "Personal Income Tax" /// 
c.cen_pr_link_i#c.std_prop "Property Tax"  /// 
c.cen_pr_link_i#c.std_sales "Sales Tax" /// 
c.cen_pr_link_i#c.std_capital "Capital Stock" /// 
c.cen_pr_link_i#c.std_trade "Trade Exposure (ADH)" /// 
c.cen_pr_link_i#c.std_rev "State Revenue Per Capita" /// 
c.cen_pr_link_i#c.std_routine "Share Routine Workers (Autor)" ///
c.cen_pr_link_i#c.pr_link_ind "\hspace{1em}Exposure to Section 936 ") ///
label /// 
 prefoot("\hline") ///
postfoot( ///
"  \\ \hline\hline" ///
"\end{tabular} }" ) preh(" ")  append sty(tex)




/*

gen inter = _b[c.cen_pr_link_i#c.std_rd]*c.cen_pr*c.std_rd ///
+_b[c.cen_pr_link_i#c.std_corp]*c.cen_pr*c.std_corp+_b[c.cen_pr_link_i#c.std_mw]*std_mw ///
+_b[c.cen_pr_link_i#c.std_ptax]*c.cen_pr*c.std_ptax+_b[c.cen_pr_link_i#c.std_ic]*c.cen_pr*c.std_ic ///
+_b[c.cen_pr_link_i#c.std_total]*c.cen_pr*c.std_total +_b[c.cen_pr_link_i#c.std_job]*c.cen_pr*c.std_job ///
+_b[c.cen_pr_link_i#c.std_train]*c.cen_pr*c.std_train+_b[c.cen_pr_link_i#c.std_prop]*c.cen_pr*c.std_prop ///
+_b[c.cen_pr_link_i#c.std_sales]*c.cen_pr*c.std_sales+_b[c.cen_pr_link_i#c.std_capital]*c.cen_pr*c.std_capital ///
+_b[c.cen_pr_link_i#c.std_trade]*c.cen_pr*c.std_trade+_b[c.cen_pr_link_i#c.std_rev]*c.cen_pr*c.std_rev ///
+_b[c.cen_pr_link_i#c.std_routine]*c.cen_pr*c.std_routine

egen std_inter = std(inter)
eststo rob_`var': reghdfe emp_growth cen_pr_link_i std_*  [fw=base_emp] if inrange(year,2004,2008) ,  a( i.year  i.fips_st i.industr) cl(fips_state industr) 

gen tot_eff = _b[cen_pr_link_i]+_b[std_inter]*std_inter 
winsor tot_eff , g(w_tot_eff) p(.01)

preserve 
keep w_tot_eff pid 
duplicates drop 
gen county = pid
maptile w_tot_eff , geo(county1990) stateoutline(linewidthstyle)  nquantiles(3) fcolor(BuRd)
restore 

drop std_total std_train  std_job std_sales std_prop std_rd 
drop inter 
drop std_inter 
drop tot_eff 
drop w_tot_eff 
*/
// Second take at interactions 
*eststo int_0: reghdfe emp_growth cen_pr_link_i [fw=base_emp] if inrange(year,2004,2008) ,  a( i.year  i.fips_st i.industr) cl(fips_state  i.industr) 
*foreach var of varlist std_* { 
*	eststo rob_`var': reghdfe emp_growth cen_pr_link_i std_* c.cen_pr_link_i#c.`var' [fw=base_emp] if inrange(year,2004,2008) ,  a( i.year  i.fips_st i.industr) cl(fips_state  i.industr) 
*} 
*eststo rob_`var': 
reghdfe emp_growth cen_pr_link_i std_* c.cen_pr_link_i#(c.std_*) [aw=wgt] if inrange(year,2004,2008) ,  a( i.year  i.fips_st i.industr) cl(fips_state industr) 

gen inter = _b[c.cen_pr_link_i#c.std_corp]*c.std_corp+_b[c.cen_pr_link_i#c.std_mw]*std_mw ///
+_b[c.cen_pr_link_i#c.std_ptax]*c.std_ptax+_b[c.cen_pr_link_i#c.std_ic]*c.std_ic ///
+_b[c.cen_pr_link_i#c.std_trade]*c.std_trade+_b[c.cen_pr_link_i#c.std_rev]*c.std_rev ///
+_b[c.cen_pr_link_i#c.std_routine]*c.std_routine

reghdfe emp_growth cen_pr_link_i std_* c.inter#c.cen_pr_link_i [aw=wgt] if inrange(year,2004,2008) ,  a( i.year  i.fips_st i.industr) cl(fips_state industr) 

gen tot_eff = _b[cen_pr_link_i]+_b[c.inter#c.cen_pr_link_i]*inter 

sum tot_eff [fw=base_emp] , d 
gen w_tot_eff = tot_eff 
replace w_tot_eff = r(p1) if tot_eff<r(p1)
replace w_tot_eff = r(p99) if tot_eff>r(p99)

winsor tot_eff , g(w_tot_eff2) p(.05)

preserve 
keep w_tot_eff pid base_emp
duplicates drop 
gen county = pid
sum w_tot_eff [aw=wgt]
cdfplot w_tot_eff [aw=wgt] , yline(0) xline(0, lcolor(black) lpattern(dash) ) xline(`r(mean)', lcolor(red))	graphregion(fcolor(white)) ///
	xtitle("Esimates of Heterogeneous Effect") ///
	ytitle("Cummulative Distribution Function") ylab(0(.25)1) xlab(-.2(.05).05)
graph export "$output/Graphs/het_eff_cdf.pdf", replace 

keep w_tot_eff pid 
duplicates drop 
gen county = pid
maptile w_tot_eff , geo(county1990) stateoutline(linewidthstyle)  nquantiles(4)   revcolor
graph export "$output/Graphs/het_eff_map.pdf", replace 
restore 
