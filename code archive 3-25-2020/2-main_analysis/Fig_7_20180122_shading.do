* 
* Runs Event-Study Analysis robustness table with  
* Author: Linh
* date of creation: 20180216
* date of update: 20180216

* Note this code is adapted from es_county_d in January 2018

// Update 20180129: (1) changed labels and included values on graph

clear all
set more off
 
// First Define Command for ES data 
capture program drop ES_graph_data
program define ES_graph_data
syntax,  level(real) [  yti(string) tshifter(real) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
rename es_b es_b_`yti'
svmat time
svmat es_ci_l
rename es_ci_l es_ci_l`yti'
svmat es_ci_h 
rename es_ci_h es_ci_h`yti'
}

end  
 
// Second Define Command for ES 
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(dash))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Estimate") label(2 "`level'% CI")) ///
	ytitle("`yti'") ///
	note("`note'") ylab("`ylab'")
graph export "`outname'.pdf", replace	

end 


// A - Merge Data for Analysis 
// A.0. Get regional crosswalk read 
use "$xwalk/cw_cty_czone.dta", clear
rename cty_fips pid 
duplicates drop 
tempfile cross
save "`cross'"

// A.0. Get link data 
use "$output_NETS/pr_link_est_countyXindustry_d.dta", clear 
gen pr_emp = pr_link*total if industry_cd < 13
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)

gen pr_l = tot_pr/tot_emp 
keep pr_l fips
rename  fips pid
rename pr_l pr_link_ind
duplicates drop 
tempfile pr 
save "`pr'"

use "$output_NETS/pr_link_est_county_d.dta", clear 
rename  fips pid
tempfile pr2 
save "`pr2'" 

// A.1. Get QCEW data 
use "$qcewdata/extract_qcew_1990_2012_naic3.dta", clear
keep if strlen(industry_code) == 3
drop if industry_code == "101" | industry_code == "102"

order fips_state fips_county year industry_code
drop if fips_county == 0 & fips_state != 72
drop if fips_state == 72 & fips_county != 0
keep if own_code == 5		// 0-total covered; 1 Federal; 2 State; 3 Local; 4 Intl Govt; 5 Private.
keep fips_state fips_county year industry_code annual_avg_estabs annual_avg_emplvl total_annual_wages
drop if fips_state == 78

collapse (sum) annual_avg_estabs (sum) annual_avg_emplvl (sum) total_annual_wages, by(fips_state fips_county year industry_code)

// A.3. Fill in gaps
gen pid = 1000*fips_state+fips_county

// A.6. Merge in pr link 
merge m:1 pid using "`pr'"
keep if _merg == 3
drop _merge 

merge m:1 pid using "`pr2'"
drop if year == . 
drop if pr_link == . 
drop _merge 
rename pr_link pr_link_all

*replace pr_link_all = pr_link_ind

sum pr_link_al, d 

drop if year == . 

*collapse (rawsum) annual_avg_estabs (rawsum) annual_avg_emplvl (rawsum) total_annual_wages (mean) pr_link_ind (mean) pr_link_all [fw=total] , by( fips_state pid year industry_code) // Dan: this isn't actually collapsing anything

egen pid2 = group(pid indus)

tsset pid2 year
tsfill, full

foreach var of varlist fips_state  {
bysort pid2: egen temp = max(`var')
replace `var' = temp
drop temp
}

foreach var of varlist annual_avg_estabs annual_avg_emplvl total_annual_wages {
replace `var' = 0 if `var' == .
}

// A.4.  Growth rates. Base year: 1995
gen temp = annual_avg_emplvl if year == 1995
bysort pid2: egen base_emp = max(temp)
drop temp

gen emp_growth = (annual_avg_emplvl - base_emp)/base
replace emp_growth = 0 if emp_growth == . 


// Income 
gen inc = total_annual_wages
* /annual_avg_emplvl
gen temp = inc if year == 1995
bysort pid2: egen base_inc = max(temp)
drop temp

gen inc_growth = (inc - base_inc)/base_inc
replace inc_growth = 0 if inc_growth == . 

// Estabs 
gen estab = annual_avg_estabs
gen temp = estab if year == 1995
bysort pid2: egen base_estab = max(temp)
drop temp

gen estab_growth = (estab - base_estab)/base_estab
replace estab_growth = 0 if estab_growth == . 

bys pid2: gen count = _N
keep if count == 23 
drop count 

drop if base_emp ==0 

sum base_emp if year == 1995, d
gen wgt=base_emp/(r(N) * r(mean))
winsor wgt, p(.01) gen(wgt_w)

sum pr_link_i $pr_wgt if base_emp > 0, d 
replace pr_link_i = pr_link_i/(r(p75)-r(p25))

sum pr_link_al  $pr_wgt if base_emp > 0, d 
replace pr_link_al = pr_link_al/(r(p75)-r(p25))

tsset pid2 year 
egen ind_st = group(ind fips_state)

// B. Graphs with All PR Link 
cd "$output" 
set more off 
xi i.year|pr_link_i  $pra_control, noomit
drop _IyeaXp*1995

destring industry_code, replace

est clear 
set more off 
eststo emp_2:  reghdfe emp_growth _IyeaX* [aw=base_emp] , a(Y=i.year#i.industr A=i.pid) cl(fips_state indust)
gen year_hat = Y - A + _b[_cons]					

	/* Old Version:
						// Note D includes the Constant while Y and A do not, so year_hat = Y + constant
						 // https://www.statalist.org/forums/forum/general-stata-discussion/general/1327487-reghdfe-and-predict
						 // Further note the constant in this case is about 0.083 so it matters quite a bit
						*/
* saving yearly PR interaction terms	
qui { 
mat b = e(b)
svmat b
}

* Generating a single variable dependent on year that has the PR interaction effect
egen year_beta = rowfirst(b1)
forval i = 1/5  {
qui sum b`i'
local b_mean = r(mean)
replace b`i' = `b_mean' if b`i' ==. 
local year  = `i' +  1989
replace year_beta = b`i' if year == `year'
}
forval i = 6/22 {
qui sum b`i'
local b_mean = r(mean)
replace b`i' = `b_mean' if b`i' ==. 
local year  = `i' +  1990
replace year_beta = b`i' if year == `year'
}

// scalars for use in the text
do "$analysis/jc_stat_out.ado"
sum year_beta if year == 2006 // 2006 coefficient for scalar output
local b = -r(mean)*100
jc_stat_out,  number(`b') name("RESULTXXVIII") replace(0) deci(1) figs(3)

drop b1-b22
replace year_beta = 0 if year_beta == .

* Multiplying interactions by different levels of PR link to find effects
* NOTE I USED base employment weights to find the moments so it is not 29% 0s.
sum pr_link_i [fw=base_emp], d
 local pr_link_mean = r(mean)
 local pr_link_25 = r(p25)
 local pr_link_5 = r(p5)
 local pr_link_75 = r(p75)
 local pr_link_95 = r(p95)
 
gen  var_pr_link_mean = `pr_link_mean' * year_beta	
gen  var_pr_link_25 = `pr_link_25' * year_beta	
gen  var_pr_link_5 = `pr_link_5' * year_beta	
gen  var_pr_link_75 = `pr_link_75' * year_beta
gen  var_pr_link_95 = `pr_link_95' * year_beta		

* collapsing data to graph
collapse (mean) year_hat var_* [fw=base_emp], by(year)	
	
* rescalling	
replace year_hat = year_hat * 100
foreach Q of var  var_* {
replace `Q' = `Q' * 100 + year_hat
}
 
* graphing
 twoway (scatteri -10 1990.5   -10 1991.25 , recast(area) color(gs12) ) ///
     (scatteri 30 1990.5   30 1991.25 , recast(area) color(gs12) ) ///
     (scatteri -10 2001   -10 2002 , recast(area) color(gs12) ) ///
     (scatteri 30 2001   30 2002 , recast(area) color(gs12) ) ///
     (scatteri -10 2007.75   -10 2009.75 , recast(area) color(gs12) ) ///
     (scatteri 30 2007.75   30 2009.75 , recast(area) color(gs12) ) /// 
 (scatter var_pr_link_mean year, msymbol(0) lcolor(navy)  mcolor(navy) c(direct) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(dash)) msize(small) lpattern(solid)) ///
    (scatter var_pr_link_5 year, msymbol(D) lcolor(orange)  mcolor(orange) c(direct) lpattern(dash) msize(small)) ///
    (scatter var_pr_link_25 year, msymbol(T) lcolor(red) mcolor(red) c(direct) lpattern(longdash) msize(small)) ///
    (scatter var_pr_link_75 year, msymbol(S) lcolor(green) mcolor(green) c(direct) lpattern(shortdash) msize(small)) ///
    (scatter var_pr_link_95 year, msymbol(+) lcolor(black) mcolor(black) c(direct) lpattern(dash_dot) msize(small)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(on order (7 8 9 10 11 1) label(7 "Mean S936 Exposure") label(9 "25th Percentile Exposure")  label(10 "75th Percentile Exposure") ///
	 label(11 "95th Percentile Exposure") label(8 "5th Percentile Exposure") label(1 "Recession")) ///
	yti("Percentage Change in Employment")
graph export "$output/Graphs/fig_7_extra_with_dots_shading.pdf", replace



// scalars for use in the text
do "$analysis/jc_stat_out.ado"

/*
	7 is \RESULT29a, 22 is \RESULT29b, 15 is \RESULT29c, and 32 is \RESULT29d all from Fig_7_20180122_shading.do
*/

sum var_pr_link_5 if year == 2006
local p5 = round(r(mean))
sum var_pr_link_mean if year == 2006
local pmean = round(r(mean))
local b1 = `p5'-`pmean'
local b2 = `p5'
local b3 = `pmean'
local b4 = `b1' / `p5' * 100
jc_stat_out,  number(`b1') name("RESULTXXIXa") replace(0) deci(0) figs(1)
jc_stat_out,  number(`b2') name("RESULTXXIXb") replace(0) deci(0) figs(2)
jc_stat_out,  number(`b3') name("RESULTXXIXc") replace(0) deci(0) figs(2)
jc_stat_out,  number(`b4') name("RESULTXXIXd") replace(0) deci(0) figs(2)
