* 
* Combines results from many different sets of robustness tests into a single table  
* Author: dan
* original date: 4-18-2018

clear all
set more off

// getting some empty variables so loops work easily

foreach i of numlist 7 8 9 11 12 13 14 {
gen A`i' = .
}
gen conspuma = .
gen czone = .

gen PC = .
gen emp = .

foreach Q of var A7 A8 A11 A12 A13 A14 PC A9 {

estimates use "$output/table_QCEW_`Q'_spec1"
estimates store `Q'_spec1
}

* pulling in all county estimates to make a table with all county estimates + cons and czone

foreach Q of num 1 2 3 4 5 {

estimates use 	"$output/table_QCEW_spec`Q'"
estimates store QCEW_spec`Q'
}

** state by year fixed effects version 
estimates use "$output/table_QCEW_sy_FE"	
estimates store sy_FE

foreach Q of var conspuma czone A9 A11 A12 A13 A14 {
foreach j of var emp {

estimates use "$output/table_pre_post_QCEW_`Q'_`j'"
estimates store `Q'_`j'


estimates use "$output/table_pre_post_QCEW_`Q'_pr`j'"
estimates store `Q'_pr`j'

}
}

// Outputting a single table with base panel on top, pr panel on bottom 

esttab *_emp using "$output/Tables/table_pre_post_long_robust.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons nonum posthead(" ") ///
prefoot(" ") ///
postfoot(" ") ///


esttab *_emp  using "$output/Tables/table_pre_post_long_robust.tex", keep(pr_link_ind )  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s() noobs /// 
coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 " pr_link_all "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{1}{l}{\textbf{Years: 2004-2008}}") postfoot("\hline") append sty(tex)

esttab *_premp using "$output/Tables/table_pre_post_long_robust.tex", keep(pr_link_ind)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s()  /// 
coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{1}{l}{\textbf{Years: 1990-1995}}") noobs label /// 
 prefoot("\hline") ///
postfoot(  ///
"Year Fixed Effects & Y & Y & Y & Y & Y & Y & Y \\" ///
"Industry Fixed Effects & Y & Y &  & Y & Y & Y & Y \\" ///
"County Geography &  &   & Y & Y  &  Y & Y  & Y  \\" ///
"Conspuma Geography & Y &   &   &   &   &   &   \\" ///
"Czone Geography &   & Y &   &   &   &   &   \\" ///
"Manufacturing Labor Percent &   &   & Y &   &   &   &   \\" ///
"Employment Instrument &   &   &   & Y &   &   &   \\" ///
"Large Retailers Dropped from Links &   &   &   &   & Y &   &   \\" ///
"No US Headquarter Dropped from Links &   &   &   &   &   & Y &   \\" ///
"Only Compustat Sample Links &   &   &   &   &   &   & Y \\" ///
"  \hline\hline" ///
"\end{tabular} }" ) append sty(tex)


// annual affects table for the first specifications
esttab A11_spec1 A12_spec1 A13_spec1 A14_spec1 PC_spec1 A9_spec1 using "$output/Tables/table_ES_QCEW_robcomb_0523.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons ///
postfoot("\multicolumn{1}{l}{\textbf{Exposure to Section 936}}") nonum posthead("")


esttab A11_spec1 A12_spec1 A13_spec1 A14_spec1 PC_spec1 A9_spec1 using "$output/Tables/table_ES_QCEW_robcomb_0523.tex",  preh("") /// 
b(3) se par mlab(none) coll(none) s() noobs nogap keep(_IyeaXpr__*) /// 
coeflabel(_IyeaXpr__1990 "\hspace{1em}X 1990 " /// 
_IyeaXpr__1991 "\hspace{1em}X 1991 " /// 
_IyeaXpr__1992 "\hspace{1em}X 1992 " /// 
_IyeaXpr__1993 "\hspace{1em}X 1993 " /// 
_IyeaXpr__1994 "\hspace{1em}X 1994 " /// 
_IyeaXpr__1996 "\hline \hspace{1em}X 1996 " /// 
_IyeaXpr__1997 "\hspace{1em}X 1997 " /// 
_IyeaXpr__1998 "\hspace{1em}X 1998 " /// 
_IyeaXpr__1999 "\hspace{1em}X 1999 " /// 
_IyeaXpr__2000 "\hspace{1em}X 2000 " /// 
_IyeaXpr__2001 "\hspace{1em}X 2001 " /// 
_IyeaXpr__2002 "\hspace{1em}X 2002 " /// 
_IyeaXpr__2003 "\hspace{1em}X 2003 " /// 
_IyeaXpr__2004 "\hspace{1em}X 2004 " /// 
_IyeaXpr__2005 "\hspace{1em}X 2005 " /// 
_IyeaXpr__2006 "\hspace{1em}X 2006 " /// 
_IyeaXpr__2007 "\hspace{1em}X 2007 " /// 
_IyeaXpr__2008 "\hspace{1em}X 2008 " /// 
_IyeaXpr__2009 "\hspace{1em}X 2009 " /// 
_IyeaXpr__2010 "\hspace{1em}X 2010 " /// 
_IyeaXpr__2011 "\hspace{1em}X 2011 " /// 
_IyeaXpr__2012 "\hspace{1em}X 2012 " )  label /// 
 prefoot("\hline") ///
postfoot( ///
"Year Fixed Effects & Y & Y & Y & Y & Y & Y \\" ///
"Year by Industry Fixed Effects & Y & Y & Y & Y &  &  \\" ///
"Employment Instrument &   Y &   &   &  & &   \\" ///
"Large Retailers Dropped from Links &    & Y &   &  & &   \\" ///
"No US Headquarter Dropped from Links &    &   & Y &  & &  \\" ///
"Only Compustat Sample Links &  &   &   & Y & & \\"  ///
"Employment Growth Per Capita  &   &   &   & & Y & \\"  ///
"Manufacturing Employment Per Capita  &   &   &  &  & & Y \\"  ///
"  \hline\hline" ///
"\end{tabular} }" ) append sty(tex)

** version adding SY FE
esttab A11_spec1 A12_spec1 A13_spec1 A14_spec1 sy_FE PC_spec1 A9_spec1 using "$output/Tables/table_ES_QCEW_robcomb_0924.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons ///
postfoot("\multicolumn{1}{l}{\textbf{Exposure to Section 936}}") nonum posthead("")


esttab A11_spec1 A12_spec1 A13_spec1 A14_spec1 sy_FE PC_spec1 A9_spec1 using "$output/Tables/table_ES_QCEW_robcomb_0924.tex",  preh("") /// 
b(3) se par mlab(none) coll(none) s() noobs nogap keep(_IyeaXpr__*) /// 
coeflabel(_IyeaXpr__1990 "\hspace{1em}X 1990 " /// 
_IyeaXpr__1991 "\hspace{1em}X 1991 " /// 
_IyeaXpr__1992 "\hspace{1em}X 1992 " /// 
_IyeaXpr__1993 "\hspace{1em}X 1993 " /// 
_IyeaXpr__1994 "\hspace{1em}X 1994 " /// 
_IyeaXpr__1996 "\hline \hspace{1em}X 1996 " /// 
_IyeaXpr__1997 "\hspace{1em}X 1997 " /// 
_IyeaXpr__1998 "\hspace{1em}X 1998 " /// 
_IyeaXpr__1999 "\hspace{1em}X 1999 " /// 
_IyeaXpr__2000 "\hspace{1em}X 2000 " /// 
_IyeaXpr__2001 "\hspace{1em}X 2001 " /// 
_IyeaXpr__2002 "\hspace{1em}X 2002 " /// 
_IyeaXpr__2003 "\hspace{1em}X 2003 " /// 
_IyeaXpr__2004 "\hspace{1em}X 2004 " /// 
_IyeaXpr__2005 "\hspace{1em}X 2005 " /// 
_IyeaXpr__2006 "\hspace{1em}X 2006 " /// 
_IyeaXpr__2007 "\hspace{1em}X 2007 " /// 
_IyeaXpr__2008 "\hspace{1em}X 2008 " /// 
_IyeaXpr__2009 "\hspace{1em}X 2009 " /// 
_IyeaXpr__2010 "\hspace{1em}X 2010 " /// 
_IyeaXpr__2011 "\hspace{1em}X 2011 " /// 
_IyeaXpr__2012 "\hspace{1em}X 2012 " )  label /// 
 prefoot("\hline") ///
postfoot( ///
"Year Fixed Effects & Y & Y & Y & Y & Y & Y & Y \\" ///
"Year by Industry Fixed Effects & Y & Y & Y & Y & Y & & \\" ///
"Employment Instrument &   Y &   &   &  & & &  \\" ///
"Large Retailers Dropped from Links &    & Y &   &  & & &   \\" ///
"No US Headquarter Dropped from Links &    &   & Y & & & &  \\" ///
"Only Compustat Sample Links &  &   &   & Y & & & & \\"  ///
"State-by-Year Fixed Effects &  &   &   & & Y & & \\"  ///
"Employment Growth Per Capita  &   &   &   & & & Y & \\"  ///
"Manufacturing Employment Per Capita  &   &   & &  &  & & Y \\"  ///
"  \hline\hline" ///
"\end{tabular} }" ) append sty(tex)



// New Main Table with extra columns form Conspuma and Czone

esttab QCEW_spec? A7_spec1 A8_spec1 using "$output/Tables/table_ES_QCEW_0518.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons ///
postfoot("\multicolumn{1}{l}{\textbf{Exposure to Section 936}}") nonum posthead("")


esttab QCEW_spec? A7_spec1 A8_spec1 using "$output/Tables/table_ES_QCEW_0518.tex",  preh("") ///
 b(3) se par mlab(none) coll(none) s() noobs nogap keep(_IyeaXpr__*)  /// 
coeflabel(_IyeaXpr__1990 "\hspace{1em}X 1990 " /// 
_IyeaXpr__1991 "\hspace{1em}X 1991 " /// 
_IyeaXpr__1992 "\hspace{1em}X 1992 " /// 
_IyeaXpr__1993 "\hspace{1em}X 1993 " /// 
_IyeaXpr__1994 "\hspace{1em}X 1994 " /// 
_IyeaXpr__1996 "\hline \hspace{1em}X 1996 " /// 
_IyeaXpr__1997 "\hspace{1em}X 1997 " /// 
_IyeaXpr__1998 "\hspace{1em}X 1998 " /// 
_IyeaXpr__1999 "\hspace{1em}X 1999 " /// 
_IyeaXpr__2000 "\hspace{1em}X 2000 " /// 
_IyeaXpr__2001 "\hspace{1em}X 2001 " /// 
_IyeaXpr__2002 "\hspace{1em}X 2002 " /// 
_IyeaXpr__2003 "\hspace{1em}X 2003 " /// 
_IyeaXpr__2004 "\hspace{1em}X 2004 " /// 
_IyeaXpr__2005 "\hspace{1em}X 2005 " /// 
_IyeaXpr__2006 "\hspace{1em}X 2006 " /// 
_IyeaXpr__2007 "\hspace{1em}X 2007 " /// 
_IyeaXpr__2008 "\hspace{1em}X 2008 " /// 
_IyeaXpr__2009 "\hspace{1em}X 2009 " /// 
_IyeaXpr__2010 "\hspace{1em}X 2010 " /// 
_IyeaXpr__2011 "\hspace{1em}X 2011 " /// 
_IyeaXpr__2012 "\hspace{1em}X 2012 " )   label /// 
 prefoot("\hline") ///
postfoot( ///
"Year by Industry Fixed Effects & Y & Y & Y & Y &  Y & Y & Y  \\" ///
"Location Fixed Effects &  &  Y &   &   &  & &   \\" ///
"Industry-by-County Fixed Effects &   &  & Y &   &  & &   \\" ///
"Winsorized Weights &    &  & & Y &  & &  \\" ///
"Drops Small County-Industries ($<$1000) &  &  & &   & Y & & \\"  ///
"Conspuma Geography  &   &   &   &  & & Y & \\"  ///
"Commuting Zone Geography  &   &   &   &  & & & Y \\"  ///
"  \hline\hline" ///
"\end{tabular} }" ) append sty(tex)

