* Original Date: 06/2016
* Merge NTES Link Data and QCEW Outcome Data 
* Runs Event-Study Analyses 
* Update date: 20180129
* Updated by dan

// Update 20180129 notes: (1) Fixes X axis labels to include 0 and relevant negatives
// 						  (2) Adds a second version with raw PR_links instead of residualized
//						  (3) added quotation marks to local tempfile names

clear all
set more off

*global consp 	"$dropbox/../completed projects/Local_Econ_Corp_Tax/Programs/Conspuma Programs/Decade Analysis"


// A - Merge Data for Analysis 

// A.0. Get regional crosswalk read 
use "$xwalk/consp2cty9-22-13.dta", clear
gen pid = 1000*state+county
keep conspuma pid 
duplicates drop 
tempfile cross
save "`cross'"

// A.1. Get link data for selected industries 
use "$output_NETS/pr_link_est_countyXindustry_d.dta", clear 
gen pr_emp = pr_link*total if industry_cd < 13
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)

gen pr_l = tot_pr/tot_emp 
keep pr_l fips tot_emp
rename  fips pid
rename pr_l pr_link_ind
duplicates drop 

merge 1:1 pid using "`cross'"
keep if _merge == 3
drop _merge 

collapse (mean) pr_link  [fw=tot_emp], by(conspuma)

sum pr_ , d
*replace pr_ = pr_/(r(sd))

tempfile pr 
save "`pr'" 

// A.1. Get link data for all industries 
use "$output_NETS/pr_link_est_county_d.dta", clear 
rename  fips pid

merge 1:1 pid using "`cross'"
keep if _merge == 3
drop _merge 

collapse (mean) pr_link  [fw=tot], by(conspuma)

sum pr_ , d
*replace pr_ = pr_/(r(sd))

tempfile pr2 
save "`pr2'"

// Get wage changes by skill level 
use "$consp/conspuma-fed-data-102111.dta", clear 
keep conspuma year dSmlwage dUmlwage dSadjlwage dUadjlwage
tempfile wageskill
save "`wageskill'"

// A.2 Merge into analysis data 
use "$consp/conspuma-dec-092313.dta", clear 
merge 1:1 conspuma year using "`wageskill'"
keep if _merge == 3
drop _merge 

merge m:1 conspuma using "`pr'"
keep if _merge == 3
drop _merge 

merge m:1 conspuma using "`pr2'"
keep if _merge == 3
drop _merge 

*replace pr_link = pr_link/100
*replace pr_link_i = pr_link_i/100
*replace pr_link = pr_link_i



sum pr_link_i, d 
replace pr_link_i = pr_link_i/(r(p75)-r(p25))

sum pr_link, d 
replace pr_link = pr_link_/(r(p75)-r(p25))

replace pr_link = pr_link_i


*replace epop = 1 
* Regressions on Wage 
eststo aw_d : areg dadjlwage   c.pr_link i.year  [aw=epop] if year > 1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year > 1990
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

eststo aw_a : areg dadjlwage   c.pr_link i.year  [aw=epop] if year ==1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year == 1990 
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

* Regressions on Wage 
eststo awS_d : areg dSadjlwage   c.pr_link i.year  [aw=epop] if year > 1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year > 1990
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

eststo awS_a : areg dSadjlwage   c.pr_link i.year  [aw=epop] if year ==1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year == 1990 
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

* Regressions on Wage 
eststo awU_d : areg dUadjlwage   c.pr_link i.year  [aw=epop] if year > 1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year > 1990
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

eststo awU_a : areg dUadjlwage   c.pr_link i.year  [aw=epop] if year ==1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year == 1990 
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

* Regressions on Rent 
eststo ar_d :areg dadjlrent   c.pr_link i.year  [aw=epop] if year > 1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year > 1990
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

eststo ar_a :areg dadjlrent   c.pr_link i.year  [aw=epop] if year ==1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year == 1990 
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

* Regressions on Rent 
eststo av_d :areg dadjlvalue   c.pr_link i.year  [aw=epop] if year > 1990   , a(state_fips) cl(state_fips )
count if e(smaple) & year > 1990
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

eststo av_a :areg dadjlvalue   c.pr_link i.year  [aw=epop] if year ==1990   , a(state_fips) cl(state_fips )
count if e(smaple) & year == 1990 
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

* Regressions on Wage 
eststo mw_d :areg dmlwage   c.pr_link i.year  [aw=epop] if year > 1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year > 1990
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

eststo mw_a :areg dmlwage   c.pr_link i.year  [aw=epop] if year ==1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year == 1990 
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

* Regressions on Wage 
eststo mwS_d : areg dSmlwage   c.pr_link i.year  [aw=epop] if year > 1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year > 1990
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

eststo mwS_a : areg dSmlwage   c.pr_link i.year  [aw=epop] if year ==1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year == 1990 
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

* Regressions on Wage 
eststo mwU_d : areg dUmlwage   c.pr_link i.year  [aw=epop] if year > 1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year > 1990
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

eststo mwU_a : areg dUmlwage   c.pr_link i.year  [aw=epop] if year ==1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year == 1990 
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"


* Regressions on Rent 
eststo mr_d :areg dmlrent   c.pr_link i.year  [aw=epop] if year > 1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year > 1990
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

eststo mr_a :areg dmlrent   c.pr_link i.year  [aw=epop] if year ==1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year == 1990 
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

* Regressions on Rent 
eststo mv_d :areg dmlvalue   c.pr_link i.year  [aw=epop] if year > 1990   , a(state_fips) cl(state_fips )
count if e(smaple) & year > 1990
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

eststo mv_a :areg dmlvalue   c.pr_link i.year  [aw=epop] if year ==1990   , a(state_fips) cl(state_fips )
count if e(smaple) & year == 1990 
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

esttab m*_d a*d using "$output/Tables/table_conspuma.tex", keep(_cons) stats() ///
b(3) se par label coeflabel(pr_link "Exposure to Section 936 ") ///
noobs nogap nomtitle tex replace nocons postfoot("")

esttab m*_d a*d using "$output/Tables/table_conspuma.tex", keep(pr_link)  ///
cells(b(fmt(3)) se(par) p)  coll(none) noobs scalars( "N_uw Observations" "hasy Year Fixed Effects" "hass State Fixed Effects") label /// 
coeflabel(pr_link "Exposure to Section 936 ") mlab("Wage" "HS Wage" "LS Wage" "Rent" "Home Value" "Adj. Wage"  "HS Adj. Wage" "LS Adj. Wage" "Adj. Rent" "Adj. Home Value" ) ///
preh("\multicolumn{6}{l}{\textbf{Stacked Differences After 1990}}\\") postfoot("\hline") append sty(tex)

esttab m*_a a*a using "$output/Tables/table_conspuma.tex", keep(pr_link)  ///
cells(b(fmt(3)) se(par) p)  coll(none) noobs scalars( "N_uw Observations" "hasy Year Fixed Effects" "hass State Fixed Effects") label /// 
coeflabel(pr_link "Exposure to Section 936 ")  mlab("Wage" "HS Wage" "LS Wage" "Rent" "Home Value" "Adj. Wage"  "HS Adj. Wage" "LS Adj. Wage" "Adj. Rent" "Adj. Home Value" )  ///
preh("\multicolumn{6}{l}{\textbf{Difference Before 1990}}\\") prefoot("\hline") ///
postfoot( ///
"  \\ \hline\hline" ///
"\end{tabular} }" ) append sty(tex)

** Graphs 


foreach var of varlist d*adjlwage dadjlrent dadjlvalue pr_link d*mlwage dmlrent dmlvalue { 
	capture: drop res_`var'
	reg `var' i.year i.state_fips [aw=epop]
	predict res_`var', res
	sum `var' [aw=epop]
	}

	sum pr_link [aw=epop]
	replace res_pr_link = res_pr_link+r(mean)

snapshot erase _all	
snapshot save
foreach res_pr_link of var res_pr_link pr_link {	
	snapshot restore 1
	xtile q_`res_pr_link'=`res_pr_link', nq(20)
	gen after = (year>1990)
	preserve 
	collapse (mean)res_dSmlwage (mean)res_dUmlwage (mean)res_dmlwage (mean) res_dmlrent (mean) `res_pr_link'  [aw=epop] , by(q_`res_pr_link' after) 
	rename res_dSmlwage q_res_dSmlwage
	rename res_dUmlwage q_res_dUmlwage
	rename `res_pr_link' q_m_`res_pr_link'
	rename res_dmlwage q_res_dmlwage
	rename res_dmlrent q_res_dmlrent
	tempfile qmeans
	save "`qmeans'"
	restore 
	merge m:1 q_`res_pr_link' after using "`qmeans'"
	drop _merge 

	local i = cond(`res_pr_link' == res_pr_link ,1 ,2)

*** Graphs for wages 
sum res_dmlwage [aw=epop] if year == 1990,d
twoway  scatter res_dmlwage `res_pr_link' [aw=epop] if year ==1990 & inrange(res_dmlwage,`r(p1)',`r(p99)')   ///
		, yline(`r(mean)',lcolor(black) lpattern(dash)) mfcolor(gs15) ///
		|| lfitci  res_dmlwage res_pr_link [aw=epop]  if year == 1990, level(90) lcolor(black) ///
		|| scatter q_res_dmlwage q_m_`res_pr_link' if year ==1990 & inrange(res_dmlwage,`r(p1)',`r(p99)') ,  ///
		xlab(-.01(.01).06) ylab(-.15(.05).15) mcolor(black) ///
		graphregion(fcolor(white)) ///
		xtitle("Exposure to Section 936") ///
		ytitle("Residualized Wage Growth") ///
		legend(order(1 4 3 2) label(1 "Data (weighted by population)")  label(2 "90% CI") label(3 "Linear Fit") ///
		label(4 "Binned Data by Ventiles")) 
	
graph export "$output/Graphs/conspuma_wage_pre_`i'.pdf", replace		
		
sum res_dmlwage [aw=epop] if year > 1990,d
twoway  scatter res_dmlwage `res_pr_link' [aw=epop] if year > 1990 & inrange(res_dmlwage,`r(p1)',`r(p99)')   ///
		, yline(`r(mean)',lcolor(black) lpattern(dash)) mfcolor(gs15) ///
		|| lfitci  res_dmlwage res_pr_link [aw=epop]  if year > 1990, level(90) lcolor(black) ///
		|| scatter q_res_dmlwage q_m_`res_pr_link' if year > 1990 & inrange(res_dmlwage,`r(p1)',`r(p99)') ,  ///
		xlab(-.01(.01).06) ylab(-.15(.05).15) mcolor(black) ///
		graphregion(fcolor(white)) ///
		xtitle("Exposure to Section 936") ///
		ytitle("Residualized Wage Growth") ///
		legend(order(1 4 3 2) label(1 "Data (weighted by population)")  label(2 "90% CI") label(3 "Linear Fit") ///
		label(4 "Binned Data by Ventiles")) 	
  
graph export "$output/Graphs/conspuma_wage_post_`i'.pdf", replace		  
  
  
  
** Grpahs for LS wages 
sum res_dUmlwage [aw=epop] if year == 1990,d
twoway  scatter res_dUmlwage `res_pr_link' [aw=epop] if year ==1990 & inrange(res_dUmlwage,`r(p1)',`r(p99)')   ///
		, yline(`r(mean)',lcolor(black) lpattern(dash)) mfcolor(gs15) ///
		|| lfitci  res_dUmlwage res_pr_link [aw=epop]  if year == 1990, level(90) lcolor(black) ///
		|| scatter q_res_dUmlwage q_m_`res_pr_link' if year ==1990 & inrange(res_dUmlwage,`r(p1)',`r(p99)') ,  ///
		xlab(-.01(.01).06) ylab(-.15(.05).15) mcolor(black) ///
		graphregion(fcolor(white)) ///
		xtitle("Exposure to Section 936") ///
		ytitle("Residualized Wage Growth") ///
		legend(order(1 4 3 2) label(1 "Data (weighted by population)")  label(2 "90% CI") label(3 "Linear Fit") ///
		label(4 "Binned Data by Ventiles")) 
	
graph export "$output/Graphs/conspuma_wage_LS_pre_`i'.pdf", replace		
		
sum res_dUmlwage [aw=epop] if year > 1990,d
twoway  scatter res_dUmlwage `res_pr_link' [aw=epop] if year > 1990 & inrange(res_dUmlwage,`r(p1)',`r(p99)')   ///
		, yline(`r(mean)',lcolor(black) lpattern(dash)) mfcolor(gs15) ///
		|| lfitci  res_dUmlwage res_pr_link [aw=epop]  if year > 1990, level(90) lcolor(black) ///
		|| scatter q_res_dUmlwage q_m_`res_pr_link' if year > 1990 & inrange(res_dUmlwage,`r(p1)',`r(p99)') ,  ///
		xlab(-.01(.01).06) ylab(-.15(.05).15) mcolor(black) ///
		graphregion(fcolor(white)) ///
		xtitle("Exposure to Section 936") ///
		ytitle("Residualized Wage Growth") ///
		legend(order(1 4 3 2) label(1 "Data (weighted by population)")  label(2 "90% CI") label(3 "Linear Fit") ///
		label(4 "Binned Data by Ventiles")) 	
  
graph export "$output/Graphs/conspuma_wage_LS_post_`i'.pdf", replace		  
  
    
  
** Graphs for rents 
sum res_dmlrent [aw=epop] if year == 1990,d
twoway  scatter res_dmlrent `res_pr_link' [aw=epop] if year ==1990 & inrange(res_dmlrent,`r(p1)',`r(p99)')   ///
		, yline(`r(mean)',lcolor(black) lpattern(dash)) mfcolor(gs15) ///
		|| lfitci  res_dmlrent res_pr_link [aw=epop]  if year == 1990, level(90) lcolor(black) ///
		|| scatter q_res_dmlrent q_m_`res_pr_link' if year ==1990 & inrange(res_dmlrent,`r(p1)',`r(p99)') ,  ///
		xlab(-.01(.01).06) ylab(-.3(.1).3) mcolor(black) ///
		graphregion(fcolor(white)) ///
		xtitle("Exposure to Section 936") ///
		ytitle("Residualized Rent Growth") ///
		legend(order(1 4 3 2) label(1 "Data (weighted by population)")  label(2 "90% CI") label(3 "Linear Fit") ///
		label(4 "Binned Data by Ventiles")) 
	
graph export "$output/Graphs/conspuma_rent_pre_`i'.pdf", replace		
		
sum res_dmlrent [aw=epop] if year > 1990,d
twoway  scatter res_dmlrent `res_pr_link' [aw=epop] if year > 1990 & inrange(res_dmlrent,`r(p1)',`r(p99)')   ///
		, yline(`r(mean)',lcolor(black) lpattern(dash)) mfcolor(gs15) ///
		|| lfitci  res_dmlrent res_pr_link [aw=epop]  if year > 1990, level(90) lcolor(black) ///
		|| scatter q_res_dmlrent q_m_`res_pr_link' if year > 1990 & inrange(res_dmlrent,`r(p1)',`r(p99)') ,  ///
		xlab(-.01(.01).06) ylab(-.3(.1).3) mcolor(black) ///
		graphregion(fcolor(white)) ///
		xtitle("Exposure to Section 936") ///
		ytitle("Residualized Rent Growth") ///
		legend(order(1 4 3 2) label(1 "Data (weighted by population)")  label(2 "90% CI") label(3 "Linear Fit") ///
		label(4 "Binned Data by Ventiles")) 	
  
graph export "$output/Graphs/conspuma_rent_post_`i'.pdf", replace		  
    
}
  
	
	

/*
sum res_dadjlwage [aw=epop] if year == 1990,d
twoway  scatter res_dadjlwage res_pr_link [aw=epop] if year ==1990 & inrange(res_dadjlwage,`r(p1)',`r(p99)')   ///
		, yline(`r(mean)',lcolor(black) lpattern(dash)) mfcolor(gs15) ///
		|| lfitci  res_dadjlwage res_pr_link [aw=epop]  if year == 1990, xlab(0.01(.01).07) ylab(-.15(.05).15) level(90)
 
sum dadjlrent [aw=epop] if year >1990,d
twoway  scatter dadjlrent res_pr_link [aw=epop] if year >1990 & inrange(dadjlrent,`r(p1)',`r(p99)')  ///
		, yline(`r(mean)',lcolor(black) lpattern(dash)) mfcolor(gs15) ///
		|| lfitci  dadjlrent res_pr_link [aw=epop]  if year > 1990 , xlab(0.01(.01).07) ylab(-.25(.1).5) level(90)

sum dadjlrent [aw=epop] if year == 1990,d
twoway  scatter dadjlrent res_pr_link [aw=epop] if year ==1990 & inrange(dadjlrent,`r(p1)',`r(p99)')   ///
		, yline(`r(mean)',lcolor(black) lpattern(dash)) mfcolor(gs15) ///
		|| lfitci  dadjlrent res_pr_link [aw=epop]  if year == 1990, xlab(0.01(.01).07) ylab(-.25(.1).5) level(90)

 
*/
 
 
 
 
 
 
 
 
