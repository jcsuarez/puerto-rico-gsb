* 06/2016
* Merge NTES Link Data and QCEW Outcome Data 
* Runs Event-Study Analyses 

clear all
set more off
 
// First Define Command for ES 
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(dash))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Estimate") label(2 "`level'% CI")) ///
	ytitle("`yti'") ///
	note("`note'") ylab("`ylab'")
graph export "`outname'.pdf", replace	

end 


// A - Merge Data for Analysis 

// A.0. Get link data 
use "$output_NETS/pr_link_est_countyXindustry_d.dta", clear 
gen pr_emp = pr_link*total if industry_cd < 13
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)

gen pr_l = tot_pr/tot_emp 
keep pr_l fips
rename  fips pid
rename pr_l pr_link_ind
duplicates drop 
tempfile pr 
save "`pr'" 

use "$output_NETS/pr_link_est_county_d.dta", clear 
rename  fips pid
tempfile pr2 
save "`pr2'" 

// A.0. Get IRS data 
use "$irs/irs_income.dta", clear
keep RETURNS GROSSINCOME fips* year 
rename RETURNS emp 
rename GROSSINCOME inc 
keep if year >= 1990
keep if year < 2013

order fips_state fips_county year 
duplicates drop 

// A.3. Fill in gaps
gen pid = 1000*fips_state+fips_county

tsset pid year
tsfill, full

// A.4.  Growth rates. Base year: 1995

foreach var of varlist inc emp { 
	gen temp = `var' if year == 1995
	bysort pid: egen base_`var' = max(temp)
	drop temp

	gen `var'_growth = (`var' - base_`var')/base_`var'
	replace `var'_growth = 0 if `var'_growth == . 
}
	
tempfile irs_dat
save "`irs_dat'", replace
	
	// A.1. Get BEA data 
use "$bea/personalincome_employment_allyears.dta", clear
keep Employment PersonalIncome2 state_fips county_fips year 
rename Emp emp 
rename Per inc 
keep if year >= 1990
keep if year < 2013

rename state_fips fips_state 
rename county_fips fips_county
order fips_state fips_county year 

// A.3. Fill in gaps
gen pid = 1000*fips_state+fips_county

tsset pid year
tsfill, full

// A.4.  Growth rates. Base year: 1995

rename inc inc_BEA
rename emp emp_BEA

foreach var of varlist inc_BEA emp_BEA { 
	gen temp = `var' if year == 1995
	bysort pid: egen base_`var' = max(temp)
	drop temp

	gen `var'_growth = (`var' - base_`var')/base_`var'
	replace `var'_growth = 0 if `var'_growth == . 
}

merge 1:1 pid year using "`irs_dat'"	
drop _merge
	
// A.6. Merge in pr link 
merge m:1 pid using "`pr'"
keep if _merg == 3
drop _merge 

merge m:1 pid using "`pr2'"
drop if year == . 
drop if pr_link == . 
drop _merge 
rename pr_link pr_link_all


** For now, keep only balanced industries . Matt: can you fix it so we don't have to do this? 
drop if base_emp ==0 

sum base_emp if year == 1995, d
gen wgt=base_emp/(r(N) * r(mean))
winsor wgt, p(.01) gen(wgt_w)

sum pr_link_i $pr_wgt if base_emp > 0, d 
replace pr_link_i = pr_link_i/(r(p75)-r(p25))

sum pr_link_al  $pr_wgt if base_emp > 0, d 
replace pr_link_al = pr_link_al/(r(p75)-r(p25))

// B. Graphs with All PR Link 
cd "$output" 
set more off 
	
eststo emp_2IRS:   reghdfe emp_growth  c.pr_link_i $pra_control2   [aw=base_emp] if inrange(year,2004,2008) , a(i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"	
	
eststo premp_2IRS:   reghdfe emp_growth  c.pr_link_i   $pra_control2  [aw=base_emp] if inrange(year,1990,1995) , a(i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"	
	
eststo inc_2IRS:  reghdfe inc_growth c.pr_link_i  $pra_control2 [aw=base_emp] if inrange(year,2004,2008) , a(i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"	
	
eststo princ_2IRS:	reghdfe inc_growth  c.pr_link_i   $pra_control2  [aw=base_emp] if inrange(year,1990,1995) , a(i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"		

	
eststo emp_2:   reghdfe emp_BEA_growth  c.pr_link_i   $pra_control2  [aw=base_emp] if inrange(year,2004,2008) , a(i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"	
	
eststo premp_2:   reghdfe emp_BEA_growth  c.pr_link_i  $pra_control2   [aw=base_emp] if inrange(year,1990,1995) , a(i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"	
	
eststo inc_2:  reghdfe inc_BEA_growth c.pr_link_i  $pra_control2 [aw=base_emp] if inrange(year,2004,2008) , a(i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"	
	
eststo princ_2:	reghdfe inc_BEA_growth  c.pr_link_i   $pra_control2  [aw=base_emp] if inrange(year,1990,1995) , a(i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"		

	

clear
set more off 

// getting some empty variables so loops work 
gen county = .
gen conspuma = .
gen czone = .

gen emp = .
gen est = .
gen inc = .

foreach Q of var county conspuma czone {
foreach j of var emp est inc {

estimates use "$output/table_pre_post_QCEW_`Q'_`j'"
estimates store `Q'_`j'


estimates use "$output/table_pre_post_QCEW_`Q'_pr`j'"
estimates store `Q'_pr`j'

}
}

// Outputting a single table with base panel on top, pr panel on bottom 
local list "emp_2IRS emp_2 county_emp  conspuma_emp czone_emp inc_2IRS inc_2 county_inc  conspuma_inc czone_inc"
local listpr " premp_2IRS premp_2 county_premp conspuma_premp czone_premp princ_2IRS princ_2 county_princ conspuma_princ czone_princ"

esttab `list' using "$output/Tables/table_pre_post_long.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons nonum posthead(" & \multicolumn{5}{c}{Employment Growth}& \multicolumn{5}{c}{Income Growth} \\ \cmidrule(lr){2-6} \cmidrule(lr){7-11}") ///
prefoot("\textbf{Geography} & \multicolumn{3}{c}{County}& Conspuma & C-Zone & \multicolumn{3}{c}{County}& Conspuma & C-Zone \\ \cmidrule(lr){2-4} \cmidrule(lr){7-9}") ///
postfoot("\textbf{Data Source} & IRS & BEA & QCEW  & QCEW & QCEW & IRS & BEA & QCEW  & QCEW  & QCEW   \\ ") ///


esttab `list'  using "$output/Tables/table_pre_post_long.tex", keep(pr_link_ind )  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s() noobs /// 
coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 " pr_link_all "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{1}{l}{\textbf{Years: 2004-2008}}") postfoot("\hline") append sty(tex)

esttab `listpr' using "$output/Tables/table_pre_post_long.tex", keep(pr_link_ind)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s()  /// 
coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{1}{l}{\textbf{Years: 1990-1995}}") noobs scalars(  "hasy Year Fixed Effects") label /// 
 prefoot("\hline") ///
postfoot( ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex)	


// For presentation: one table for employment 
local list "emp_2IRS emp_2 county_emp  conspuma_emp czone_emp"
local listpr " premp_2IRS premp_2 county_premp conspuma_premp czone_premp"

esttab `list' using "$output/Tables/table_pre_post_long_emp.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons nonum posthead(" & \multicolumn{5}{c}{Employment Growth} \\ \cmidrule(lr){2-6} ") ///
prefoot("\textbf{Geography} & \multicolumn{3}{c}{County}& Conspuma & C-Zone  \\ \cmidrule(lr){2-4}  ") ///
postfoot("\textbf{Data Source} & IRS & BEA & QCEW  & QCEW & QCEW   \\ ") ///

esttab `list'  using "$output/Tables/table_pre_post_long_emp.tex", keep(pr_link_ind )  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s() noobs /// 
coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 " pr_link_all "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{1}{l}{\textbf{Years: 2004-2008}}") postfoot("\hline") append sty(tex)

esttab `listpr' using "$output/Tables/table_pre_post_long_emp.tex", keep(pr_link_ind)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s()  /// 
coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{1}{l}{\textbf{Years: 1990-1995}}") noobs scalars(  "hasy Year Fixed Effects") label /// 
 prefoot("\hline") ///
postfoot( ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex)	



// For presentation: one table  for income 
local list " inc_2IRS inc_2 county_inc  conspuma_inc czone_inc"
local listpr " princ_2IRS princ_2 county_princ conspuma_princ czone_princ"

esttab `list' using "$output/Tables/table_pre_post_long_inc.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons nonum posthead(" & \multicolumn{5}{c}{Income Growth} \\ \cmidrule(lr){2-6} ") ///
prefoot("\textbf{Geography} & \multicolumn{3}{c}{County}& Conspuma & C-Zone  \\ \cmidrule(lr){2-4}  ") ///
postfoot("\textbf{Data Source} & IRS & BEA & QCEW  & QCEW & QCEW   \\ ") ///

esttab `list'  using "$output/Tables/table_pre_post_long_inc.tex", keep(pr_link_ind )  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s() noobs /// 
coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 " pr_link_all "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{1}{l}{\textbf{Years: 2004-2008}}") postfoot("\hline") append sty(tex)

esttab `listpr' using "$output/Tables/table_pre_post_long_inc.tex", keep(pr_link_ind)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s()  /// 
coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{1}{l}{\textbf{Years: 1990-1995}}") noobs scalars(  "hasy Year Fixed Effects") label /// 
 prefoot("\hline") ///
postfoot( ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex)	
