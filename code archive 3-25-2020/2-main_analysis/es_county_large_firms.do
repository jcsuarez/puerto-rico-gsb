* Runs Event-Study Analysis with shocks from large (>20,000 emp in the US in 1995) firms
* Author: dan 
* date: 10-29-2018

clear all
set more off
snapshot erase _all

// First Define Command for ES data 
capture program drop ES_graph_data
program define ES_graph_data
syntax,  level(real) yti(string) tshifter(real)


qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 


forval i = 1/23 { 
	mat time = (time,`i'-6+1995 + `tshifter')
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci_l es_ci_h time 
svmat es_b 
rename es_b `yti'_es_b
svmat time
rename time `yti'_time
svmat es_ci_l
rename es_ci_l `yti'_es_ci_l
svmat es_ci_h 
rename es_ci_h `yti'_es_ci_h
}

end  
 
// Second Define Command for ES 
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(dash))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Estimate") label(2 "`level'% CI")) ///
	ytitle("`yti'") ///
	note("`note'") ylab("`ylab'")
graph export "`outname'.pdf", replace	

end 


// A - Merge Data for Analysis 
// A.0. Get regional crosswalk read 
use "$xwalk/cw_cty_czone.dta", clear
rename cty_fips pid 
duplicates drop 
tempfile cross
save "`cross'"

// A.0. Get link data 
use "$output_NETS/pr_link_est_countyXindustry_naic3.dta", clear 
gen pr_emp = pr_link*total if inrange(naic3,300,340)
gen pr_emp_s1 = pr_link_s1*total if inrange(naic3,300,340)
gen pr_emp_s2 = pr_link_s2*total if inrange(naic3,300,340)
gen pr_emp_s3 = pr_link_s3*total if inrange(naic3,300,340)
gen pr_emp_s4 = pr_link_s4*total if inrange(naic3,300,340)
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr    = total(pr_emp)
bys fips: egen tot_pr_s1 = total(pr_emp_s1)
bys fips: egen tot_pr_s2 = total(pr_emp_s2)
bys fips: egen tot_pr_s3 = total(pr_emp_s3)
bys fips: egen tot_pr_s4 = total(pr_emp_s4)

gen pr_l = tot_pr/tot_emp 
gen pr_l_s1 = tot_pr_s1/tot_emp 
gen pr_l_s2 = tot_pr_s2/tot_emp 
gen pr_l_s3 = tot_pr_s3/tot_emp 
gen pr_l_s4 = tot_pr_s4/tot_emp 
keep pr_l pr_l_* fips
rename  fips pid
rename pr_l    pr_link_ind
rename pr_l_s1 pr_link_s1_ind
rename pr_l_s2 pr_link_s2_ind
rename pr_l_s3 pr_link_s3_ind
rename pr_l_s4 pr_link_s4_ind
duplicates drop 
tempfile pr 
save "`pr'" 

use "$output_NETS/pr_link_est_county_naic3.dta", clear 
rename  fips pid
tempfile pr2 
save "`pr2'" 

// A.1. Get QCEW data 
use "$qcewdata/extract_qcew_1990_2012_naic3.dta", clear
keep if strlen(industry_code) == 3
drop if industry_code == "101" | industry_code == "102"

order fips_state fips_county year industry_code
drop if fips_county == 0 & fips_state != 72
drop if fips_state == 72 & fips_county != 0
keep if own_code == 5		// 0-total covered; 1 Federal; 2 State; 3 Local; 4 Intl Govt; 5 Private.
keep fips_state fips_county year industry_code annual_avg_estabs annual_avg_emplvl total_annual_wages
drop if fips_state == 78
collapse (sum) annual_avg_estabs (sum) annual_avg_emplvl (sum) total_annual_wages, by(fips_state fips_county year industry_code)

// A.3. Fill in gaps
gen pid = 1000*fips_state+fips_county

// A.6. Merge in pr link 
merge m:1 pid using "`pr'"
keep if _merg == 3
drop _merge 

merge m:1 pid using "`pr2'"
drop if year == . 
drop if pr_link == . 
drop _merge 
rename pr_link pr_link_all

*replace pr_link_all = pr_link_ind

sum pr_link_al, d 

drop if year == . 

*collapse (rawsum) annual_avg_estabs (rawsum) annual_avg_emplvl (rawsum) total_annual_wages (mean) pr_link_ind (mean) pr_link_all [fw=total] , by( fips_state pid year industry_code) // Dan: this isn't actually collapsing anything

egen pid2 = group(pid indus)

tsset pid2 year
tsfill, full

foreach var of varlist fips_state  {
bysort pid2: egen temp = max(`var')
replace `var' = temp
drop temp
}

foreach var of varlist annual_avg_estabs annual_avg_emplvl total_annual_wages {
replace `var' = 0 if `var' == .
}

// A.4.  Growth rates. Base year: 1995
gen temp = annual_avg_emplvl if year == 1995
bysort pid2: egen base_emp = max(temp)
drop temp

gen emp_growth = (annual_avg_emplvl - base_emp)/base
replace emp_growth = 0 if emp_growth == . 

// Income 
gen inc = total_annual_wages
* /annual_avg_emplvl
gen temp = inc if year == 1995
bysort pid2: egen base_inc = max(temp)
drop temp

gen inc_growth = (inc - base_inc)/base_inc
replace inc_growth = 0 if inc_growth == . 

// Estabs 
gen estab = annual_avg_estabs
gen temp = estab if year == 1995
bysort pid2: egen base_estab = max(temp)
drop temp

gen estab_growth = (estab - base_estab)/base_estab
replace estab_growth = 0 if estab_growth == . 

** For now, keep only balanced industries . Matt: can you fix it so we don't have to do this? 
bys pid2: gen count = _N
keep if count == 23 
drop count 

drop if base_emp ==0 

sum base_emp if year == 1995, d
gen wgt=base_emp/(r(N) * r(mean))
winsor wgt, p(.01) gen(wgt_w)

sum pr_link_ind $pr_wgt if base_emp > 0, d 
replace pr_link_i = pr_link_i/(r(p75)-r(p25))
sum pr_link_ind $pr_wgt if base_emp > 0, d 

scalar sd_main = r(sd)

** v1 normalizes each to the baseline case
** v2 normalizes each individually
** v3 normalizes to the same SD
sum pr_link_s1_ind $pr_wgt if base_emp > 0, d 
*replace pr_link_s1_ind = pr_link_s1_ind/(r(p75)-r(p25))
replace pr_link_s1_ind = pr_link_s1_ind * sd_main / r(sd)

sum pr_link_s2_ind $pr_wgt if base_emp > 0, d 
*replace pr_link_s2_ind = pr_link_s2_ind/(r(p75)-r(p25))
replace pr_link_s2_ind = pr_link_s2_ind * sd_main / r(sd)

sum pr_link_s3_ind $pr_wgt if base_emp > 0, d 
*replace pr_link_s3_ind = pr_link_s3_ind/(r(p75)-r(p25))
replace pr_link_s3_ind = pr_link_s3_ind * sd_main / r(sd)

sum pr_link_s4_ind $pr_wgt if base_emp > 0, d 
*replace pr_link_s4_ind = pr_link_s4_ind/(r(p75)-r(p25))
replace pr_link_s4_ind = pr_link_s4_ind * sd_main / r(sd)

tsset pid2 year 
egen ind_st = group(ind fips_state)

// B. Graphs with All PR Link 
cd "$output" 
set more off 

*keep if base_emp > 400  
winsor base_emp , g(w_emp_base) p(.025)
destring industry_code, replace
est clear 
set more off

xi i.year|pr_link_ind $pra_control, noomit
drop _IyeaXp*1995
eststo emp_1:  reghdfe emp_growth _IyeaXpr__* [aw=wgt]  , a(i.pid  i.year#i.industr ) cl(fips_state indust )
	estadd local hasiy "Yes"
    estadd local hascty "Yes"
	
ES_graph_data , level(95) yti("spec1") tshifter(-.2)

xi i.year|pr_link_s1_ind $pra_control, noomit
drop _IyeaXp*1995
eststo emp_2:  reghdfe emp_growth _IyeaX* [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust )
	estadd local hasiy "Yes"
	estadd local hascty "Yes"

ES_graph_data , level(95) yti("spec2")	tshifter(-0.1)

xi i.year|pr_link_s2_ind $pra_control, noomit
drop _IyeaXp*1995
eststo emp_3:  reghdfe emp_growth _IyeaX* [aw=wgt]  , a( i.pid i.year#i.indust ) cl(fips_state indust )
	estadd local hasiy "Yes"
	estadd local hascty "Yes"

ES_graph_data , level(95) yti("spec3")	tshifter(0)

xi i.year|pr_link_s3_ind $pra_control, noomit
drop _IyeaXp*1995
eststo emp_4:  reghdfe emp_growth _IyeaX* [aw=wgt] , a( i.pid i.year#i.indust ) cl(fips_state indust )
	estadd local hasiy "Yes"
	estadd local hascty "Yes"

ES_graph_data , level(95) yti("spec4")	tshifter(0.1)

xi i.year|pr_link_s4_ind $pra_control, noomit
drop _IyeaXp*1995
eststo emp_5:  reghdfe emp_growth _IyeaX* [aw=wgt] , a( i.pid i.year#i.indust ) cl(fips_state indust )
	estadd local hasiy "Yes"
	estadd local hascty "Yes"

ES_graph_data , level(95) yti("spec5")	tshifter(0.2)
ES_graph_data , level(95) yti("large_spec5")	tshifter(0.2)


* Graph output mirroring figure 3 in http://gabriel-zucman.eu/files/teaching/FuestEtal16.pdf
 twoway (line spec1_es_b spec1_time, lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid)))  ///
		(rcap spec1_es_ci_l spec1_es_ci_h spec1_time,  lcolor(navy) lpattern(dash))  ///
		(line spec2_es_b spec2_time, lcolor(orange) lpattern(solid))      ///
		(rcap spec2_es_ci_l spec2_es_ci_h spec2_time,  lcolor(orange) lpattern(dash))   /// 
		(line spec3_es_b spec3_time, lcolor(black) lpattern(solid))     ///
		(rcap spec3_es_ci_l spec3_es_ci_h spec3_time,  lcolor(black) lpattern(dash))    ///
		(line spec4_es_b spec4_time, lcolor(green) lpattern(solid))    ///
		(rcap spec4_es_ci_l spec4_es_ci_h spec4_time,  lcolor(green) lpattern(dash))    ///
		(line spec5_es_b spec5_time, lcolor(red) lpattern(solid))   ///
		(rcap spec5_es_ci_l spec5_es_ci_h spec5_time,  lcolor(red) lpattern(dash))   ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Baseline") ///
	label(3 ">20,000")   ///
	label(5 ">30,000")  ///
	label(7 ">40,000") label(9 ">50,000")  ///
	r(2) order( 1 3 5 7 9)) ///
	ytitle("Percent Employment Growth") 
graph export "$output/fig-7-shock_size_v3.pdf", replace	

	
esttab emp* using "$output/Tables/table_ES_QCEW_shock_size_v3.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons ///
postfoot("\multicolumn{1}{l}{\textbf{Exposure to Section 936}}") nonum posthead("")


esttab emp*   using "$output/Tables/table_ES_QCEW_shock_size_v3.tex",  preh("") ///
 b(3) se par mlab(none) coll(none) s() noobs nogap keep(_IyeaXpr__*)  /// 
coeflabel(_IyeaXpr__1990 "\hspace{1em}X 1990 " /// 
_IyeaXpr__1991 "\hspace{1em}X 1991 " /// 
_IyeaXpr__1992 "\hspace{1em}X 1992 " /// 
_IyeaXpr__1993 "\hspace{1em}X 1993 " /// 
_IyeaXpr__1994 "\hspace{1em}X 1994 " /// 
_IyeaXpr__1996 "\hline \hspace{1em}X 1996 " /// 
_IyeaXpr__1997 "\hspace{1em}X 1997 " /// 
_IyeaXpr__1998 "\hspace{1em}X 1998 " /// 
_IyeaXpr__1999 "\hspace{1em}X 1999 " /// 
_IyeaXpr__2000 "\hspace{1em}X 2000 " /// 
_IyeaXpr__2001 "\hspace{1em}X 2001 " /// 
_IyeaXpr__2002 "\hspace{1em}X 2002 " /// 
_IyeaXpr__2003 "\hspace{1em}X 2003 " /// 
_IyeaXpr__2004 "\hspace{1em}X 2004 " /// 
_IyeaXpr__2005 "\hspace{1em}X 2005 " /// 
_IyeaXpr__2006 "\hspace{1em}X 2006 " /// 
_IyeaXpr__2007 "\hspace{1em}X 2007 " /// 
_IyeaXpr__2008 "\hspace{1em}X 2008 " /// 
_IyeaXpr__2009 "\hspace{1em}X 2009 " /// 
_IyeaXpr__2010 "\hspace{1em}X 2010 " /// 
_IyeaXpr__2011 "\hspace{1em}X 2011 " /// 
_IyeaXpr__2012 "\hspace{1em}X 2012 " )   scalars(  "hasiy Industry-by-Year Fixed Effects" "hascty County Fixed Effects" ) label /// 
 prefoot("\hline") ///
postfoot( ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex)

keep if large_spec5_time != .
keep large_spec* year
drop *_time
drop if large_spec1_es_b == .

merge 1:1 year using "$qcewdata/ES_graph_sy_fe.dta"
drop _merge
save "$qcewdata/ES_graph_sy_fe.dta", replace
