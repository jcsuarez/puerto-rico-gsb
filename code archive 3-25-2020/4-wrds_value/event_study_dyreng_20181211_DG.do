clear
capture log close
set more off
capture close
snapshot erase _all
set matsize 8000
***************************************************************
//Filename: event_study_ETR.do
//Author: dgg
//Date: 20180105
//Task: Event studies for 2 dates

// This file in particular focuses on the 1993 event for interaction terms
//  using Dyreng et al. control variables.

// This file takes a different approadh than the other event study files that pool the
// estimation of existing relationships and CARs into the same procedure.

// Here, I estimate the CARs first and then regress the CARs on firm characteristics (as in Ohrn)
***************************************************************

*stocks1990-1996 is the old file with only exact name matches
use "$WRDS/stocks_1992-1995_PR", replace // this includes non-PR firms
{
***** Organizing data
* making a list of even dates (checked manually for correctness)
local event_dates "12100 13068" // 13083"
*11647 is 11/21/1991 Pryor introduces the Prescription Drug Cost Containment Act in previous week.
*12053 is 1/3/1993
*12100 is Feb 16, 1993, the Tuesday after Clinton and Pryor talked
*12145 is 4-2-1993
*12183 is 5-10-1993
*12257 is 7-23-1993
*12276 is 8-11-1993
*12913 is 5-10-1995
*13016 is 8-21-1995 (post weekend)
*13040 is 9-14-1995
*13068 is 10-12-1995
*13083 is 10-27-1995 (note the overlap)
*13291 is 5-22-1996
*13338 is july 8, 1996, the monday after the Senate voted (post-weekend)
*13362 is August 1, 1996, congress killed 936
}


*generating numbers of trading days instead of actual dates
sort permno date
gen year = year(date)
by permno: gen datenum=_n 
foreach event_date in `event_dates' {
by permno: gen target`event_date'=datenum if date==`event_date'
egen td`event_date'=min(target`event_date'), by(permno)
drop target`event_date'
gen dif`event_date'=datenum-td`event_date'
}
* Defining the estimation and event windows (counting windows to make sure they
* 											 actually are trading on those days)
local window = 100
local min_obs = 70
local prewindow = 5

foreach event_date in `event_dates' {
by permno: gen event`event_date'=1 if dif`event_date'>=0 & dif`event_date'<=15 & ret != .
replace event`event_date'=0 if event`event_date' == .

*including some pre-period
by permno: gen pre_event`event_date'=1 if dif`event_date'>=-`prewindow' & dif`event_date'<=15 & ret != .
replace pre_event`event_date'=0 if pre_event`event_date' == .

* estimation windows will also be used to cap the event windows so there is only 1 dummy
* variable when outputting tables.
forvalues i=0(1)5 {
local j = `i' * 3

by permno: gen estimation`event_date'_`i'=1 if dif`event_date'>= -`window' & dif`event_date'<=`j' & ret != .
replace estimation`event_date'_`i'=0 if estimation`event_date'_`i' == .
* cutting out estimation on firms with limited pre-estimation periods
by permno: egen sample_size = sum(estimation`event_date'_`i')
replace estimation`event_date'_`i'=0 if sample_size < `min_obs' 

* Getting a non-event sample for estimating counterfactuals and CARs
by permno: gen pre_est`event_date'_`i'=1 if dif`event_date'>= -`window' & dif`event_date'< 0 & ret != .
replace pre_est`event_date'_`i'=0 if pre_est`event_date'_`i' == .

*dropping those firms without enough observations for estimation in the pre-sample
replace pre_est`event_date'_`i'=0 if sample_size < `min_obs' 
drop sample_size

* generating interactions for graphical explanation of event studies
gen event_int_`event_date'_`i' =  pre_event`event_date' * (dif`event_date' + `prewindow' + 1) * estimation`event_date'_`i'
}
}

expand 2 if estimation12100_5 == 1, gen(eve1)
expand 2 if estimation13068_5 == 1, gen(eve2)

gen pooled_eve = eve1 + eve2
keep if pooled_eve == 1

*Sometimes dividends are given in two lines, dropping those (does not affect return, price, etc.)
duplicates drop permno date, force

tempfile events
save "`events'", replace

*** Pulling in the FF 3 factors, momentum, and risk free rate.
{
* These data also include market returns but they are less precise than those from CRSP

import delimited "$WRDS/F-F_Research_Data_Factors_daily.txt",  clear delim(" ", collapse) stringc(1)
*turning dates into a form that matches what we already have
rename date date_1
gen date = date(date_1, "YMD")
format date %tdNN/DD/CCYY
drop date_1

tempfile factors
save "`factors'", replace

**** merging factors with stock returns
use "`events'", clear

merge m:1 date using "`factors'"
keep if _merge == 3
drop _merge
save "`events'", replace

* now momentum
import delimited "$WRDS/F-F_Momentum_Factor_daily.txt",  clear delim(" ", collapse) stringc(1)
*turning dates into a form that matches what we already have
rename date date_1
gen date = date(date_1, "YMD")
format date %tdNN/DD/CCYY
drop date_1

tempfile factors
save "`factors'", replace

**** merging factors with stock returns
use "`events'", clear

merge m:1 date using "`factors'"
keep if _merge == 3
drop _merge
rename mom MOM

save "`events'", replace
}

**** Importing information regarding company financials 
// ISSUE: merging on year gives mising values, but merging together is probably false
// For now just using 1992 or earliest financials 
*rename cusip cusip_1
*rename ncusip cusip
merge m:1 cusip using "$WRDS/ETR_compustat_1992.dta"
drop if permno == .

************************************************
* Estimating abnormal returns using 4 factors
************************************************
xtset permno datenum

*creating values from previous trading day for value weighting
*NOTE: Some prices are coded as negatives (not -99 that means missing) so I use the absolute value
*gen val = abs(l.prc * l.shrout)
egen id=group(permno eve1)

* Now testing for significance over longer intervals (Directly implementing Coups regressions)
local spec1 "c.mktrf"
local spec2 "c.mktrf c.smb c.hml c.MOM"

set more off
estimates drop _all

* Regression for CARs
forvalues w=1(1)3 {
forvalues k=1(1)2 {
* spec specific samples
local spec1_other "if pre_est12100_5 == 1 "
local spec2_other "if pre_est13068_5 == 1 "
local spec2_other "if pre_est12100_5 == 1 | pre_est13068_5 == 1 "
qui eststo base_reg_`k'_`w': reg ret i.id#(`spec`k'') i.id `spec`w'_other', r
predict rethat_`k'_`w'

gen AR_`k'_`w' = ret - rethat_`k'_`w'
drop rethat_`k'_`w'
}
}

forvalues k=1(1)2 {
forvalues i=0(1)5 {
local j = `i' * 3 + 2

bysort id: egen CAR_`k'_`i'_1 = sum(AR_`k'_1) if dif12100 > -1 & dif12100 < `j'
bysort id: egen CAR_`k'_`i'_2 = sum(AR_`k'_2) if dif13068 > -1 & dif13068 < `j'
bysort id: egen CAR_`k'_`i'_3 = sum(AR_`k'_3) if (dif13068 > -1 & dif13068 < `j') |  (dif12100 > -1 & dif12100 < `j') 

}
*bysort id: egen CAR_`k'_6_1_1 = sum(AR_`k'_`w') if dif12100 > -6 & dif12100 < `j'
}
* collapsing to the firm level
 collapse (firstnm) permno-_merge  AR_1_1-CAR_2_5_3, by(id)

* With all firms, this takes forever to run so I save the collapsed result
save "$WRDS/ETR_event_CARs_PR.dta", replace
 
 
 
 
use "$WRDS/ETR_event_CARs_PR.dta", clear
keep if PR == 1

set more off
estimates drop _all

gen any_rd = (rd>0) & rd !=. 
gen any_ads = (ads>0) & ads !=. 
gen ln_at = logAT

foreach i of var CAR* {
replace `i' = `i'* 100
}

* for now, only base CAPM model
rename naics naicsh_long
tostring naicsh_long,replace
gen naicsh = substr(naicsh_long,1,2)
egen naics=group(naicsh)


******* Additional specifications from meeting on Tuesday (with demeaned x variables)
/*
-table 1 
	- no industry controls (one with, one without)
	-const and ETR, logAT, RD, ADVERT, Ratio of gross profits to operating capital (separately) 
	-do one version with and one with change 
	
	-defining operating capital as total assets minus intangibles
		-SOME SOURCES SAY IT IS ACTUALLY WORKING CAPITAL? check def
*/
*gen gpop = gp / (at - intan)
gen gpop = gp / (at)
gen PRexp = emp_PR / (emp_PR + emp_US)
drop etr_1 etr_2

gen day1993 = (eve1 == 1)
gen day1995 = 1 - day1993

** merging with the dyreng data:
capture: drop _merge
merge 1:1 gvkey year using "$additional/Dyreng_Tax_Havens/dyreng_data_merged.dta", force
drop if _merge == 2

gen other_haven = 0
replace other_haven = 1 if ir_mention > 0 & ir_mention != .
replace other_haven = 1 if ba_mention > 0 & ba_mention != .
replace other_haven = 1 if hk_mention > 0 & hk_mention != .
replace other_haven = 1 if sp_mention > 0 & sp_mention != .
replace other_haven = 1 if ci_mention > 0 & ci_mention != .
replace other_haven = 1 if sw_mention > 0 & sw_mention != .
bys gvkey: egen temp = max(other_haven)
replace other_haven = temp
drop temp

rename etr_1 etr_1_dyreng
rename etr_2 etr_2_dyreng



forval w = 1/3{
foreach i of var etr-nol btd etr_change any_rd any_ads gpop PRexp other_haven {
qui sum `i' if CAR_1_3_`w' != ., d
gen mean`i' = r(mean)
gen sd`i' = r(sd)
gen `i'_`w' = (`i' - mean`i') / sd`i'
drop mean`i' sd`i'
}
forval j = 1/2{
* de-meaning all of the control (including dummies, so interpretation will be relative to average)
bysort naics: egen meanCAR_`j'_3_`w' = mean(CAR_`j'_3_`w')
egen ag_meanCAR_`j'_3_`w' = mean(CAR_`j'_3_`w')
gen CAR_`j'_3_`w'_demean = CAR_`j'_3_`w' - meanCAR_`j'_3_`w' + ag_meanCAR_`j'_3_`w'
drop meanCAR_`j'_3_`w' ag_meanCAR_`j'_3_`w'

qui eststo reg_1_`j'_`w':  areg CAR_`j'_3_`w' rd_`w' any_rd_`w'  , a(naics)  robust
	estadd local ind1 "Yes"


qui eststo reg_2_`j'_`w':  areg CAR_`j'_3_`w' rd_`w' any_rd_`w' gpop_`w' , a(naics)  robust
	estadd local ind1 "Yes"


qui eststo reg_3_`j'_`w':  areg CAR_`j'_3_`w' rd_`w' any_rd_`w' gpop_`w' any_ads_`w' , a(naics)  robust
	estadd local ind1 "Yes"


qui eststo reg_4_`j'_`w':  areg CAR_`j'_3_`w' rd_`w' any_rd_`w' gpop_`w' any_ads_`w' other_haven_`w' , a(naics)  robust
	estadd local ind1 "Yes"


qui eststo reg_5_`j'_`w':  areg CAR_`j'_3_`w' rd_`w' any_rd_`w' gpop_`w' any_ads_`w' other_haven_`w' PRexp_`w', a(naics)  robust
	estadd local ind1 "Yes"


eststo reg_6_`j'_`w':  reg CAR_`j'_3_`w'_demean rd_`w' any_rd_`w' any_ads_`w' gpop_`w' other_haven_`w' PRexp_`w' day1993 day1995, robust nocons  // 
	estadd local ind1 "Yes"

esttab reg_*_`j'_`w' using "$output/Tables/ETR_event_20181211_`j'_`w'.tex", keep(_cons) stats() ///
b(3) se par label coeflabel(event_dummy "\hspace{1em}Event") ///
noobs nogap tex replace nocons postfoot("") nomti

esttab reg_*_`j'_`w' using "$output/Tables/ETR_event_20181211_`j'_`w'.tex", keep(_cons rd_`w' gpop_`w' any_ads_`w' other_haven_`w' PRexp_`w' day*) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) order(_cons day*)  ///
varlabel(_cons "\hspace{1em}Exposure to Section 936" day1993 "\hspace{1em}February 16, 1993" day1995 "\hspace{1em}October 12, 1995"  ///
any_ads_`w' "\hspace{1em}Any Advertising" rd_`w' "\hspace{1em}Research \& Development" gpop_`w' "\hspace{1em}Gross Profit / Operating Assets"  ///
etr_change_`w' "\hspace{1em}Change in ETR" PRexp_`w' "\hspace{1em}Relative PR Employment" other_haven_`w' "\hspace{1em}Reported Other Haven Access") ///
scalars( N "ind1 Industry Fixed Effects" ) label /// 
preh("") prefoot("\hline")  postfoot( ///
"\hline " ///
"\end{tabular} }" ) append sty(tex) nonum
}
}


// scalars for use in the text
do "$analysis/jc_stat_out.ado"

estimates restore reg_1_1_3
local b = _b[rd_3]
jc_stat_out,  number(`b') name("RESULTXXVII") replace(0) deci(2) figs(3)
