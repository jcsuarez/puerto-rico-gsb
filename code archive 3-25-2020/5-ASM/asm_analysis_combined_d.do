


///////////////////// Get data from PR Link 
*** Open PR link data by industry and create measure by state-3 digit naics 
use "$output_NETS/pr_link_est_countyXindustry_d.dta", clear 
** For Manuf Industries 
gen pr_emp = pr_link*total if industry_cd < 13
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)
gen pr_link_all = tot_pr/tot_emp 
drop pr_emp

** By Industry 
gen pr_emp = pr_link*total 
gen pr_link_ind = pr_emp/tot_emp

keep fips95 industry_cd tot_emp pr_link*
gen fips_state = round(fips95/1000,1)

collapse (mean) pr_link_ind (mean) pr_link_all [fw=tot_emp] , by(fips_state indus)
tempfile pr 
save "`pr'"

///////////////////// Get data from ASM
use "$ASM/combined_2017_21_15.dta", clear

// Label industries		
gen naics_2 = naics
tostring naics_2, replace 
gen industry_cd = .
replace industry_cd = 1 if naics_2 == "311"
replace industry_cd = 2 if naics_2 == "314"
replace industry_cd = 3 if naics_2 == "315"
replace industry_cd = 4 if naics_2 == "325"	// note: include 3254
replace industry_cd = 5 if naics_2 == "3254"
replace industry_cd = 6 if naics_2 == "326"
replace industry_cd = 7 if naics_2 == "316"
replace industry_cd = 8 if naics_2 == "332"
replace industry_cd = 9 if naics_2 == "333"
replace industry_cd = 10 if naics_2 == "335"
replace industry_cd = 12 if industry_cd == .

label define l_naic3 0 "Total, all industries" 1 "Food Mfg" 2 "Textile mill products" 3 "Apparel" 4 "Chemicals" 5 "Pharmaceuticals" ///
 6 "Rubber and Plastic" 7 "Leather" 8 "Fabricated metal" 9 "Machinery" 10 "Electrical equip" ///
 11 "Instruments" 12 "Other mfg" 13 "Finance, insurance, real estate" 14 "Services" ///
 15 "Wholesale and retail" 16 "Other non-mfg", replace
label values industry_cd l_naic3

drop if industry_cd == 0

// Collapse at 2-digit industry by state level 
collapse (sum) numemp (sum) annpayT (sum) avgprodwrkrs ///
		 (sum) prodwrkrwageT (sum) prodwrkrshrT (sum) totcostmatT ///
		 (sum) totvalshipT (sum) valaddT (sum) capexT, by(year state fips_state industry_cd)

/////// Merge data 
merge m:1 industry_cd fips_state using "`pr'"
keep if _merge == 3 
drop _merge 
egen pid = group(industry_cd fips)
tsset pid year 

*gen earnings = annpayT/numemp
gen earnings = prodwrkrwageT/avgprodwrkrs

////// Make percentage changes 
foreach var of varlist numemp annpayT avgprodwrkrs prodwrkrwageT prodwrkrshrT totcostmatT totvalshipT capexT valaddT earnings {
	gen temp = `var' if year == 1995
	bysort pid: egen base_`var' = max(temp)
	drop temp
	gen `var'_growth = (`var' - base_`var')/base_`var'
	}
////// Keep only large obs. Otherwise small industryXstate drive large effects 
// drop if base_numemp < 1000 	
	
foreach var of varlist pr_link* {
	sum `var', d
	replace `var' = `var'/r(sd)
*	replace `var' = `var'/(r(p75)-r(p25))	
	}

// There are large outliers in the pr link. Here choose to winsorize the large values
winsor pr_link_i, p(.01) g(w_pr_link_i) high
replace pr_link_i=w_pr_link_i
// Similar results if instead drop large values 
*sum pr_link_i , d
*scalar p99_pr = r(p99)
*drop if pr_link_i >= p99_pr

winsor base_numemp, p(.01) g(w_base_numemp)
gen oth_man = (industry == 12)
gen oth_pr = oth_man*pr_link_i
gen mai_pr = (1-oth_man)*pr_link_i

gen pr_link_post = (year>=1997)*pr_link_i
gen post_oth_pr = (year>=1997)*oth_pr
gen post_mai_pr = (year>=1997)*mai_pr

keep if base_capexT>1000

est clear 
*keep if base_numemp<500000
**** Regression on Capx
eststo reg_capx_1: reghdfe capexT_growth pr_link_post c.pr_link_i ,  a(i.year i.industry_cd ) vce(cluster fips)	
	count if e(smaple) 
	estadd scalar N_uw = r(N)
	estadd local hasy "Yes"
	estadd local hasind "Yes"

eststo reg_capx_2: reghdfe capexT_growth  pr_link_post c.pr_link_i ,  a(i.year i.fips i.industry_cd ) vce(cluster fips)	
	count if e(smaple) 
	estadd scalar N_uw = r(N)
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassta "Yes"	
	
eststo reg_capx_3: reghdfe capexT_growth pr_link_post c.pr_link_i ,  a(i.pid i.year) vce(cluster fips)	
	count if e(smaple) 
	estadd scalar N_uw = r(N)
	estadd local hasy "Yes"
	estadd local hassta_i "Yes"

eststo reg_capx_4: reghdfe capexT_growth pr_link_post c.pr_link_i ,  a(i.pid i.year#i.fips  ) vce(cluster fips)	
	count if e(smaple) 
	estadd scalar N_uw = r(N)
	estadd local hasy_s "Yes"
	estadd local hassta_i "Yes"

eststo reg_capx_5: reghdfe capexT_growth pr_link_post c.pr_link_i ,  a(i.pid i.year#i.industry_cd ) vce(cluster fips)	
	count if e(smaple) 
	estadd scalar N_uw = r(N)
	estadd local hasy_i "Yes"
	estadd local hassta_i "Yes"
/*
eststo reg_capx_6: reghdfe capexT_growth mai_pr oth_pr post_mai_pr post_oth_pr,  a(i.pid i.year ) vce(cluster fips)	
	count if e(smaple) 
	estadd scalar N_uw = r(N)
	estadd local hasy "Yes"
	estadd local hassta_i "Yes"
*/
**** Regression on Employment, Earnings and Value Added 
eststo reg_emp_1: reghdfe numemp_growth pr_link_post c.pr_link_i ,  a(i.pid i.year) vce(cluster fips)	
	count if e(smaple) 
	estadd scalar N_uw = r(N)
	estadd local hasy "Yes"
	estadd local hassta_i "Yes"

eststo reg_emp_2: reghdfe numemp_growth mai_pr oth_pr post_mai_pr post_oth_pr ,  a(i.pid i.year ) vce(cluster fips)	
	count if e(smaple) 
	estadd scalar N_uw = r(N)
	estadd local hasy "Yes"
	estadd local hassta_i "Yes"
	
eststo reg_earn_1: reghdfe prodwrkrwageT_growth pr_link_post c.pr_link_i ,  a(i.pid i.year ) vce(cluster fips)	
	count if e(smaple) 
	estadd scalar N_uw = r(N)
	estadd local hasy "Yes"
	estadd local hassta_i "Yes"

eststo reg_earn_2: reghdfe prodwrkrwageT_growth mai_pr oth_pr post_mai_pr post_oth_pr ,  a(i.pid i.year) vce(cluster fips)	
	count if e(smaple) 
	estadd scalar N_uw = r(N)
	estadd local hasy "Yes"
	estadd local hassta_i "Yes"

eststo reg_val_1:reghdfe valaddT_growth pr_link_post c.pr_link_i ,  a(i.pid i.year ) vce(cluster fips)	
	count if e(smaple) 
	estadd scalar N_uw = r(N)
	estadd local hasy "Yes"
	estadd local hassta_i "Yes"

eststo reg_val_2:reghdfe valaddT_growth mai_pr oth_pr post_mai_pr post_oth_pr,  a(i.pid i.year ) vce(cluster fips)
	count if e(smaple) 
	estadd scalar N_uw = r(N)
	estadd local hasy "Yes"
	estadd local hassta_i "Yes"
	
esttab reg*capx* using "$output/Tables/table_asm_capx_20171215.tex", drop(*) stats() ///
b(3) se par label coll(none)  ///
noobs nogap nomtitle tex replace nocons postfoot("") nonum posthead("")

esttab reg*capx* using "$output/Tables/table_asm_capx_20171215.tex", keep(pr_link_post)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) noobs scalars( "N_uw Observations" "hasy Year Fixed Effects" "hasind Industry Fixed Effects" ///
"hassta State Fixed Effects" "hassta_i State X Industry Fixed Effects"   "hasy_s Year X State Fixed Effects" "hasy_i Year X Industry Fixed Effects") label /// 
coeflabel(pr_link_post "\hspace{1em}Exposure to Section 936 X Post"  post_mai_pr "\hspace{1em}I(Main Industry) X Exposure to Section 936" post_oth_pr "\hspace{1em}I(Other Industry) X Exposure to Section 936") ///
preh("\multicolumn{1}{l}{\textbf{Growth in Capital Expenditures}}") postfoot( ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex)

/*
esttab reg_emp* reg_earn* reg_val* using "$output/Tables/table_asm_capx_20171215.tex", keep(pr_link_post  post_mai_pr post_oth_pr )  ///
cells(b(fmt(3)) se(par) p) noobs mlab("Employment" "Employment" "Wage Bill" "Wage Bill" "Value Added" "Value Added") coll(none) ///
scalars( "N_uw Observations" "hasy Year Fixed Effects" "hassta_i State X Industry Fixed Effects") label /// 
coeflabel(pr_link_post "\hspace{1em}Exposure to Section 936"  post_mai_pr "\hspace{1em}I(Main Industry) X Exposure to Section 936" post_oth_pr "\hspace{1em}I(Other Industry) X Exposure to Section 936") ///
preh("\multicolumn{6}{l}{\textbf{Employment, Wage Bill, and Value Added}}\\ \hline") prefoot("\hline") ///
postfoot( ///
"  \\ \hline\hline" ///
"\end{tabular} }" ) append sty(tex)	
*/	
*keep if year > 1989

xi i.year|pr_link_i , noomit 
forval i=1987(1)2014 { 
	capture: label var _IyeaXpr__`i' " "
} 
forval i=1987(2)2014 { 
	capture: label var _IyeaXpr__`i' "`i'"
} 
*gen post = (year>=1997)
drop _IyeaXpr__1995
*ln_cap
set more off
reghdfe capexT_g _IyeaXpr* ,  a(i.industry_cd i.year) vce(cluster fips) nocons
coefplot , vertical xline(8.5, lcolor(black)) yline(0)  level(90) ciopts(recast(rcap)) ///
	xtitle("Year") graphregion(fcolor(white)) ///
	ytitle("Effect of Exposure to S936 on CAPX") 
graph export "$output/Graphs/capx_asm.pdf", replace 

/*
	65 is \RESULT20
	350 is \RESULT21a
	223 is \RESULT21b with all estimates coming from asm_analysis_combined_d.do
*/

estimates restore reg_capx_5

do "$analysis/jc_stat_out.ado"

local b = -_b[pr_link_post] * 100
jc_stat_out,  number(`b') name("RESULTXX") replace(0) deci(0) figs(2)

sum capexT if year == 1995
local base = r(mean) / 1000
jc_stat_out,  number(`base') name("RESULTXXIa") replace(0) deci(0) figs(3)
local new = -_b[pr_link_post] * `base' 
jc_stat_out,  number(`new') name("RESULTXXIb") replace(0) deci(0) figs(3)


/*
asasfd
reghdfe numemp_g _IyeaXpr* ,  a(i.year i.industry_cd) vce(cluster fips)
coefplot , vertical xline(5.5) yline(0) xt(#10) level(90)

reghdfe capexT_g _IyeaXpr* ,  a(i.pid i.post#i.industry_cd i.year) vce(cluster fips)
coefplot , vertical xline(5.5) yline(0) xt(#10) level(90)

	
	
*** Other outcomes 
*reghdfe earnings_growth c.pr_link_i [fw=base_numemp],  a(i.year i.industry_cd ) vce(cluster fips)	

*reghdfe earnings_growth c.pr_link_i oth_pr [fw=base_numemp],  a(i.year#i.fips i.year#i.industry_cd ) vce(cluster fips)	

*reghdfe prodwrkrshrT_growth c.pr_link_i [fw=base_numemp],  a(i.year i.industry_cd ) vce(cluster fips)	

*reghdfe prodwrkrshrT_growth c.pr_link_i oth_pr [fw=base_numemp],  a(i.year#i.fips i.year#i.industry_cd ) vce(cluster fips)	









