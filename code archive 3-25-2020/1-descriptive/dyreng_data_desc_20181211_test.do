clear
capture log close
set more off
capture close
***************************************************************
//Author: dgg
//Date: 20180418
//Task: merge Dyreng data with compustat data
***************************************************************
snapshot erase _all

/*
The basic goals of this exercise:
-- first question: to what extent are PR firms in other tax havens in 1997 (first year)? Are non-PR firms in other tax havens? 
--second question: are PR firms more likely to open new tax havens after repeal of 936? 
-- third question: if so, where do these firms go? 
*/

use "$additional/Dyreng_Tax_Havens/dyreng_data_merged", replace

/* One option using yearly variation
xi i.year|PR , noomit
drop _IyeaXPR_1995
reghdfe th_mention _IyeaX* PR i.year#(c.totalcount c.ncountries) if year > 1992,  a(i.n3 i.year) cl(n3)

*/

/* Other option using pre-post variation
*/
gen post = year > 1995
xi i.post|PR , noomit
*drop _IyeaXPR_1995
egen ny_FE = group(n3 year)

/*
reghdfe th_mention PR i.year#(c.totalcount c.ncountries) if year > 1992 & year < 1996,  a(i.n3 i.year) cl(n3)
reghdfe th_mention PR i.year#(c.totalcount c.ncountries) if year > 1995,  a(i.n3 i.year) cl(n3)

reghdfe taxhaven PR i.year#(c.totalcount c.ncountries) if year > 1992 & year < 1996,  a(i.n3 i.year) cl(n3)
reghdfe taxhaven PR i.year#(c.totalcount c.ncountries) if year > 1995,  a(i.n3 i.year) cl(n3)*/

est clear
eststo reg_ir_pre: reghdfe ir_mention PR c.totalcount c.ncountries if year > 1992 & year < 1996,  a(i.n3 i.year) cl(ny_FE)
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hasu "Yes"
	estadd local hast "Yes"
eststo reg_ci_pre: reghdfe ci_mention PR c.totalcount c.ncountries if year > 1992 & year < 1996,  a(i.n3 i.year) cl(ny_FE)
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hasu "Yes"
	estadd local hast "Yes"
eststo reg_sw_pre: reghdfe sw_mention PR c.totalcount c.ncountries if year > 1992 & year < 1996,  a(i.n3 i.year) cl(ny_FE)
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hasu "Yes"
	estadd local hast "Yes"
eststo reg_sp_pre: reghdfe sp_mention PR c.totalcount c.ncountries if year > 1992 & year < 1996,  a(i.n3 i.year) cl(ny_FE)
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hasu "Yes"
	estadd local hast "Yes"
eststo reg_ba_pre: reghdfe ba_mention PR c.totalcount c.ncountries if year > 1992 & year < 1996,  a(i.n3 i.year) cl(ny_FE)
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hasu "Yes"
	estadd local hast "Yes"
eststo reg_hk_pre: reghdfe hk_mention PR c.totalcount c.ncountries if year > 1992 & year < 1996,  a(i.n3 i.year) cl(ny_FE)
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hasu "Yes"
	estadd local hast "Yes"

eststo reg_ir_post: reghdfe ir_mention PR c.totalcount c.ncountries if year > 2003 & year < 2009,  a(i.n3 i.year) cl(ny_FE)
eststo reg_ci_post: reghdfe ci_mention PR c.totalcount c.ncountries if year > 2003 & year < 2009,  a(i.n3 i.year) cl(ny_FE)
eststo reg_sw_post: reghdfe sw_mention PR c.totalcount c.ncountries if year > 2003 & year < 2009,  a(i.n3 i.year) cl(ny_FE)
eststo reg_sp_post: reghdfe sp_mention PR c.totalcount c.ncountries if year > 2003 & year < 2009,  a(i.n3 i.year) cl(ny_FE)
eststo reg_ba_post: reghdfe ba_mention PR c.totalcount c.ncountries if year > 2003 & year < 2009,  a(i.n3 i.year) cl(ny_FE)
eststo reg_hk_post: reghdfe hk_mention PR c.totalcount c.ncountries if year > 2003 & year < 2009,  a(i.n3 i.year) cl(ny_FE)

** wide table
esttab reg_??_pre using "$output/Tables/table_tax_haven_presence.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons nonum posthead(" & Ireland & Cayman Islands & Switzerland & Singapore & Barbados & Hong Kong \\ ") ///
prefoot("") ///
postfoot("") ///

esttab reg_??_post  using "$output/Tables/table_tax_haven_presence.tex", keep(PR)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s() noobs /// 
coeflabel(PR "\hspace{1em}Exposure to Section 936 " pr_link_all "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{1}{l}{\textbf{Years: 2004-2008}}") postfoot("\hline") append sty(tex)

esttab reg_??_pre using "$output/Tables/table_tax_haven_presence.tex", keep(PR)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s()  /// 
coeflabel(PR "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{1}{l}{\textbf{Years: 1993-1995}}") noobs scalars(  "hasy Year Fixed Effects" "hasind Industry Fixed Effects" "hasu Unique Country Count" "hast Total Country Count") label /// 
 prefoot("\hline") ///
postfoot( ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex)	


** long table (AEA pnp style without pvalues)
esttab reg_ir_pre reg_ir_post using "$output/Tables/table_tax_haven_presence_long.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons nonum posthead(" & Pre & Post \\ ") ///
prefoot("") ///
postfoot("") ///

esttab reg_ir_pre reg_ir_post  using "$output/Tables/table_tax_haven_presence_long.tex", keep(PR)  ///
cells(b(fmt(3)) se(par)) mlab(none) coll(none) s() noobs nonum /// 
coeflabel(PR "Ireland" pr_link_all "\hspace{1em}Exposure to Section 936 ") ///
preh("") postfoot("") append sty(tex)    prefoot("")

esttab reg_ci_pre reg_ci_post  using "$output/Tables/table_tax_haven_presence_long.tex", keep(PR)  ///
cells(b(fmt(3)) se(par)) mlab(none) coll(none) s() noobs nonum /// 
coeflabel(PR "Cayman Islands" pr_link_all "\hspace{1em}Exposure to Section 936 ") ///
preh("") postfoot("") append sty(tex)    prefoot("") posth("")

esttab reg_sw_pre reg_sw_post  using "$output/Tables/table_tax_haven_presence_long.tex", keep(PR)  ///
cells(b(fmt(3)) se(par)) mlab(none) coll(none) s() noobs nonum /// 
coeflabel(PR "Switzerland" pr_link_all "\hspace{1em}Exposure to Section 936 ") ///
preh("") postfoot("") append sty(tex)    prefoot("") posth("")

esttab reg_sp_pre reg_sp_post  using "$output/Tables/table_tax_haven_presence_long.tex", keep(PR)  ///
cells(b(fmt(3)) se(par)) mlab(none) coll(none) s() noobs nonum /// 
coeflabel(PR "Singapore" pr_link_all "\hspace{1em}Exposure to Section 936 ") ///
preh("") postfoot("") append sty(tex)    prefoot("") posth("")

esttab reg_ba_pre reg_ba_post  using "$output/Tables/table_tax_haven_presence_long.tex", keep(PR)  ///
cells(b(fmt(3)) se(par)) mlab(none) coll(none) s() noobs nonum /// 
coeflabel(PR "Barbados" pr_link_all "\hspace{1em}Exposure to Section 936 ") ///
preh("") postfoot("") append sty(tex)    prefoot("") posth("")

esttab reg_hk_pre reg_hk_post  using "$output/Tables/table_tax_haven_presence_long.tex", keep(PR)  ///
cells(b(fmt(3)) se(par)) mlab(none) coll(none) s()  posth("")  /// 
coeflabel(PR "Hong Kong") ///
preh("") noobs nonum label /// 
 prefoot("") ///
postfoot( ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex)	


/*
** long table (AEA pnp style without pvalues)
esttab reg_ir_pre reg_ir_post using "$output/Tables/table_tax_haven_presence_long.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons nonum posthead(" & Pre & Post \\ ") ///
prefoot("") ///
postfoot("") ///

esttab reg_ir_pre reg_ir_post  using "$output/Tables/table_tax_haven_presence_long.tex", keep(PR)  ///
cells(b(fmt(3)) se(par)) mlab(none) coll(none) s() noobs nonum /// 
coeflabel(PR "Exposure to \S936 " pr_link_all "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{1}{l}{\textbf{Ireland}}") postfoot("") append sty(tex)    prefoot("")

esttab reg_ci_pre reg_ci_post  using "$output/Tables/table_tax_haven_presence_long.tex", keep(PR)  ///
cells(b(fmt(3)) se(par)) mlab(none) coll(none) s() noobs nonum /// 
coeflabel(PR "Exposure to \S936 " pr_link_all "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{1}{l}{\textbf{Ireland}}") postfoot("") append sty(tex)    prefoot("")

esttab reg_sw_pre reg_sw_post  using "$output/Tables/table_tax_haven_presence_long.tex", keep(PR)  ///
cells(b(fmt(3)) se(par)) mlab(none) coll(none) s() noobs nonum /// 
coeflabel(PR "Exposure to \S936 " pr_link_all "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{1}{l}{\textbf{Ireland}}") postfoot("") append sty(tex)    prefoot("")

esttab reg_sp_pre reg_sp_post  using "$output/Tables/table_tax_haven_presence_long.tex", keep(PR)  ///
cells(b(fmt(3)) se(par)) mlab(none) coll(none) s() noobs nonum /// 
coeflabel(PR "Exposure to \S936 " pr_link_all "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{1}{l}{\textbf{Ireland}}") postfoot("") append sty(tex)    prefoot("")

esttab reg_ba_pre reg_ba_post  using "$output/Tables/table_tax_haven_presence_long.tex", keep(PR)  ///
cells(b(fmt(3)) se(par)) mlab(none) coll(none) s() noobs nonum /// 
coeflabel(PR "Exposure to \S936 " pr_link_all "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{1}{l}{\textbf{Ireland}}") postfoot("") append sty(tex)    prefoot("")

esttab reg_hk_pre reg_hk_post  using "$output/Tables/table_tax_haven_presence_long.tex", keep(PR)  ///
cells(b(fmt(3)) se(par)) mlab(none) coll(none) s()  /// 
coeflabel(PR "Exposure to \S936 ") ///
preh("\multicolumn{1}{l}{\textbf{Hong Kong}}") noobs nonum label /// 
 prefoot("") ///
postfoot( ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex)	


/*
** second version without total country counts, which I no longer think make sense
est clear
eststo reg_ir_pre: reg ir_mention PR c.ncountries i.n3 i.year if year > 1992 & year < 1996 
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hasu "Yes"
eststo reg_ci_pre: reg ci_mention PR c.ncountries i.n3 i.year if year > 1992 & year < 1996
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hasu "Yes"
eststo reg_sw_pre: reg sw_mention PR c.ncountries i.n3 i.year if year > 1992 & year < 1996
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hasu "Yes"
eststo reg_sp_pre: reg sp_mention PR c.ncountries i.n3 i.year if year > 1992 & year < 1996
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hasu "Yes"
eststo reg_ba_pre: reg ba_mention PR c.ncountries i.n3 i.year if year > 1992 & year < 1996
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hasu "Yes"
eststo reg_hk_pre: reg hk_mention PR c.ncountries i.n3 i.year if year > 1992 & year < 1996
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hasu "Yes"

sureg 	(ir_mention PR c.ncountries i.n3 i.year) ///
		(ci_mention PR c.ncountries i.n3 i.year) ///
		(sw_mention PR c.ncountries i.n3 i.year) ///
		(sp_mention PR c.ncountries i.n3 i.year) ///
		(ba_mention PR c.ncountries i.n3 i.year) ///
		(hk_mention PR c.ncountries i.n3 i.year)  if year > 1992 & year < 1996
	
	
eststo reg_ir_post: reg ir_mention PR c.ncountries i.n3 i.year if year > 2003 & year < 2009
eststo reg_ci_post: reg ci_mention PR c.ncountries i.n3 i.year if year > 2003 & year < 2009
eststo reg_sw_post: reg sw_mention PR c.ncountries i.n3 i.year if year > 2003 & year < 2009
eststo reg_sp_post: reg sp_mention PR c.ncountries i.n3 i.year if year > 2003 & year < 2009
eststo reg_ba_post: reg ba_mention PR c.ncountries i.n3 i.year if year > 2003 & year < 2009
eststo reg_hk_post: reg hk_mention PR c.ncountries i.n3 i.year if year > 2003 & year < 2009

sureg 	(ir_mention PR c.ncountries i.n3 i.year) ///
		(ci_mention PR c.ncountries i.n3 i.year) ///
		(sw_mention PR c.ncountries i.n3 i.year) ///
		(sp_mention PR c.ncountries i.n3 i.year) ///
		(ba_mention PR c.ncountries i.n3 i.year) ///
		(hk_mention PR c.ncountries i.n3 i.year)  if year > 2003 & year < 2009

esttab reg_??_pre using "$output/Tables/table_tax_haven_presence_test.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons nonum posthead(" & Ireland & Cayman Islands & Switzerland & Singapore & Barbados & Hong Kong \\ ") ///
prefoot("") ///
postfoot("") ///

esttab reg_??_post  using "$output/Tables/table_tax_haven_presence_test.tex", keep(PR)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s() noobs /// 
coeflabel(PR "\hspace{1em}Exposure to Section 936 " pr_link_all "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{1}{l}{\textbf{Years: 2004-2008}}") postfoot("\hline") append sty(tex)

esttab reg_??_pre using "$output/Tables/table_tax_haven_presence_test.tex", keep(PR)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s()  /// 
coeflabel(PR "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{1}{l}{\textbf{Years: 1993-1995}}") noobs scalars(  "hasy Year Fixed Effects" "hasind Industry Fixed Effects" "hasu Unique Country Count" ) label /// 
 prefoot("\hline") ///
postfoot( ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex)	





eststo reg_ir_post: probit ir_mention PR c.ncountries i.n3 i.year if year > 2003 & year < 2009, cl(gvkey)
eststo reg_ci_post: probit ci_mention PR c.ncountries i.n3 i.year if year > 2003 & year < 2009, cl(gvkey)
eststo reg_sw_post: probit sw_mention PR c.ncountries i.n3 i.year if year > 2003 & year < 2009, cl(gvkey)
eststo reg_sp_post: probit sp_mention PR c.ncountries i.n3 i.year if year > 2003 & year < 2009, cl(gvkey)
eststo reg_ba_post: probit ba_mention PR c.ncountries i.n3 i.year if year > 2003 & year < 2009, cl(gvkey)
eststo reg_hk_post: probit hk_mention PR c.ncountries i.n3 i.year if year > 2003 & year < 2009, cl(gvkey)


eststo reg_ir_post: probit ir_mention PR c.ncountries i.year if year > 2003 & year < 2009, cl(gvkey)
eststo reg_ci_post: probit ci_mention PR c.ncountries i.year if year > 2003 & year < 2009, cl(gvkey)
eststo reg_sw_post: probit sw_mention PR c.ncountries i.year if year > 2003 & year < 2009, cl(gvkey)
eststo reg_sp_post: probit sp_mention PR c.ncountries i.year if year > 2003 & year < 2009, cl(gvkey)
eststo reg_ba_post: probit ba_mention PR c.ncountries i.year if year > 2003 & year < 2009, cl(gvkey)
eststo reg_hk_post: probit hk_mention PR c.ncountries i.year if year > 2003 & year < 2009, cl(gvkey)
