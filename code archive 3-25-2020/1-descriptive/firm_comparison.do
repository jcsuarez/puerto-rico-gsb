** this file does a sample comparison of several characters for both Compustat Sample and NETS samples
** author: dan
** date: 9-24-2018

/* goals:
-make a table showing match of moments for compustat treated and control samples
-make a table showing match og moments for NETS treated and control sampl (matched thing)
*/
clear all
set more off
snapshot erase _all

** starting with the Compustat Sample:
*** Importing long data 
use "$WRDS/ETR_compustat.dta", replace

keep if year == 1995

* labeling variables for display
label var PR "Puerto Rico Presence"
label var etr_change "Change in ETR"

local General "etr txfed MNE nol logAT"
local Sales "rd ads"
local Assets "ppe lev spec btd intang"
local Physical "capex"

foreach Q of varlist `General' `Sales' `Assets' `Physical' {
gen `Q'_p = `Q' if PR == 1
replace `Q' = . if PR == 1
label var `Q' "Control"
label var `Q'_p "Treatment"
}

local General "etr etr_p txfed txfed_p MNE MNE_p nol nol_p logAT logAT_p"
local Sales "rd rd_p ads ads_p"
local Assets "ppe ppe_p lev lev_p btd btd_p intang intang_p"
local Physical "capex capex_p"


estpost tabstat `General' `Sales' `Assets' `Physical', statistics(count mean sd p25 p50 p75) ///
	columns(statistics)
est store tempB

esttab tempB using "$output/sum_stats_compu_20180924.tex", replace ///
	refcat(etr "\emph{Effective Tax Rate}" txfed "\emph{Federal Taxes Paid}" ///
	MNE "\emph{Multinational}"  nol "\emph{Net Operating Loss}"  ///
	logAT "\emph{Log Assets}" rd "\emph{Research and Development}" ads "\emph{Any Advertising}" ///
	ppe "\emph{Plants, Property, and Equipment}" lev "\emph{Leverage}" btd "\emph{Book to Debt}" ///
	intang "\emph{Intangibles}" capex "\emph{Capital Expenditures}", nolab) /// 
	cells("count mean(fmt(3)) sd(fmt(3)) p25(fmt(2)) p50(fmt(2)) p75(fmt(2))" ) nonum label nomtitle tex ///
	collabels("Count" "Mean" "SD" "$25^{th}$" "$50^{th}$" "75$^{th}$") noobs sfmt(3)
	
** same thing with nets sample	
use "$output_NETS_control/pr_extract_est_fake.dta", clear

gen naic4 = floor(naics95/100)
replace naic4 = floor(naics95/10) if naic4 <1111
replace naic4 = floor(naics95) if naic4 <1111 
gen naic3 = floor(naics95/1000)
replace naic3 = floor(naics95/100) if naic3 <111
replace naic3 = floor(naics95/10) if naic3 <111
replace naic3 = floor(naics95) if naic3 <111
gen naic2 = floor(naics95/10000)
replace naic2 = floor(naics95/1000) if naic2 <11
replace naic2 = floor(naics95/100) if naic2 <11
replace naic2 = floor(naics95/10) if naic2 <11
replace naic2 = floor(naics95) if naic2 <11

drop if naic2 == 92 | naic3 == 813
* dropping USPS and federal government
drop if hqduns == 3261245 
* this is navy, drop
drop if hqduns == 161906193
 
gen estabs = 1
keep if pr_link == 1
collapse (sum) estabs emp95, by(hqduns)

gen fake_link = 1

tempfile fake_shock
save "`fake_shock'"

**actual pr shock
use "$output/NETS_20171129_v2/pr_extract_panel_v3.dta", clear

keep if year == 1995 
keep if num_est_PR > 0
rename emp_US emp95
rename num_est_US estabs
rename hqduns hqduns95

keep emp95 estabs hqduns95
gen fake_link = 0

append using "`fake_shock'"

*** generating the table 
gen emp95_t = emp95 if fake_link == 0
gen estabs_t = estabs if fake_link == 0

gen emp95_c = emp95 if fake_link == 1
gen estabs_c = estabs if fake_link == 1

label var estabs_c "Control Sample"
label var emp95_c "Control Sample"
label var estabs_t "Treatment Sample"
label var emp95_t "Treatment Sample"


estpost tabstat estabs_c estabs_t emp95_c emp95_t, statistics(count mean sd p25 p50 p75) ///
	columns(statistics)
est store tempB

esttab tempB using "$output/sum_stats_nets_20180924.tex", replace ///
	refcat(estabs_c "\emph{US Establishments}" emp95_c "\emph{US Employment}" ///
	, nolab) /// 
	cells("count mean(fmt(3)) sd(fmt(3)) p25(fmt(2)) p50(fmt(2)) p75(fmt(2))" ) nonum label nomtitle tex ///
	collabels("Count" "Mean" "SD" "$25^{th}$" "$50^{th}$" "75$^{th}$") noobs sfmt(3)
	







	
	