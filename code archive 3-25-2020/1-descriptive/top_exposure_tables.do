//  Making a table listing to geographies in terms of exposure
* 2-05-2018
* Use Geography level data

clear all
set more off
snapshot erase _all
***********************************************
* County (Names from county_addvars)
use "$output_NETS/pr_link_est_county_addvars_emp.dta", clear

keep pr_link QName population
drop if QName == ""
drop if pr_link == .

local pop_thresh = 100000
drop if population < `pop_thresh'

sort pr_link
gen id = _n

keep if (id < 11 |  id > 447)

tempfile temp1
save "`temp1'"

use "$output_NETS/pr_link_est_county_addvars_emp.dta", clear
keep emp95 pr_emp95  QName population
gen pr_link_emp = pr_emp95 / emp95

drop if QName == ""
drop if pr_link == .

local pop_thresh = 100000
drop if population < `pop_thresh'

sort pr_link_emp
gen id = _n

keep if (id < 11 |  id > 447)

rename QName QName2

merge 1:1 id using "`temp1'"
order id QName pr_link QName2 pr_link_emp
keep id QName pr_link QName2 pr_link_emp
format pr_link* %4.3fc

cd "$output/Tables"

label var QName "County Name" 
label var id "Place"
label var QName2 "County Name" 
label var pr_link "Section 936 Establishment Exposure"
label var pr_link_emp "Section 936 Employment Exposure"  

listtex id QName pr_link QName2 pr_link_emp using "$output/Tables/county_exposure.tex" ///
, replace rstyle(tabular) headlines("\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}\begin{tabular}{l*{4}{c}} \hline\hline Rank & County & S936 Est Exposure & County & S936 Emp Exposure \\ \hline ") ///
footlines(" \hline\hline\end{tabular} ")


//  end("\\")
//BAD CODE:
// dataout, tex replace save(county_exposure) nohead

***********************************************
* Czone (NO NAMES CURRENTLY)
// A.0. Get regional crosswalk read 
use "$xwalk/cw_cty_czone.dta", clear
rename cty_fips pid 
duplicates drop 
tempfile cross
save "`cross'"

use "$output_NETS/pr_link_est_county_addvars_emp.dta", clear
rename fips_county pid

merge m:1 pid using "`cross'"
gen pr_link_emp = pr_emp95 / emp95

keep if _merge == 3
drop _merge

* aggregating up to czone

collapse (mean) pr_link (mean) pr_link_emp [fw=emp95] , by(czone)

sort pr_link
gen id = _n

tempfile temp1
save "`temp1'"

sort pr_link_emp
drop id
gen id = _n

rename czone czone2

merge 1:1 id using "`temp1'"
keep if (id < 11 |  id > 713)

order id czone pr_link czone2 pr_link_emp
keep id czone pr_link czone2 pr_link_emp
format pr_link* %4.3fc

cd "$output/Tables"
listtex id czone pr_link czone2 pr_link_emp using "$output/Tables/czone_exposure.tex" ///
, replace rstyle(tabular) headlines("\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}\begin{tabular}{l*{4}{c}} \hline\hline Rank & Commuting Zone & S936 Est Exposure & Commuting Zone & S936 Emp Exposure \\ \hline ") ///
footlines(" \hline\hline\end{tabular} ")



***********************************************
* Conspuma (NO NAMES CURRENTLY)
// A.0. Get regional crosswalk read 
use "$xwalk/consp2cty9-22-13.dta", clear
gen pid = 1000*state+county
keep conspuma pid 
duplicates drop 
tempfile cross
save "`cross'"

use "$output_NETS/pr_link_est_county_addvars_emp.dta", clear
rename fips_county pid

merge m:1 pid using "`cross'"
* aggregating up to conspuma
gen pr_link_emp = pr_emp95 / emp95

keep if _merge == 3
drop _merge

* aggregating up to czone
drop czone
rename conspuma czone
collapse (mean) pr_link (mean) pr_link_emp [fw=emp95] , by(czone)

sort pr_link
gen id = _n

tempfile temp1
save "`temp1'"

sort pr_link_emp
drop id
gen id = _n

rename czone czone2

merge 1:1 id using "`temp1'"
keep if (id < 11 |  id > 486)

order id czone pr_link czone2 pr_link_emp
keep id czone pr_link czone2 pr_link_emp
format pr_link* %4.3fc

cd "$output/Tables"
listtex id czone pr_link czone2 pr_link_emp using "$output/Tables/conspuma_exposure.tex" ///
, replace rstyle(tabular) headlines("\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}\begin{tabular}{l*{4}{c}} \hline\hline Rank & Conspuma & S936 Est Exposure & Conspuma & S936 Emp Exposure \\ \hline ") ///
footlines(" \hline\hline\end{tabular} ")
