********* This file merge additional data and runs balance table

use "$data/data_additional/St Tax Structure 05_10_2017/state_taxes_analysis.dta", clear
keep if year == 1990 
keep fips salestax propertytax corporate_rate GDP rev_totaltaxes rec_val rdcred rdcred trainingsubsidy jobcreationcred corpinctax totalincentives investment_credit 
rename fips statefips
tempfile incentives 
save "`incentives'"


use "$data/data_additional/Misallocation 5_10_2017/misallocation_paneldata.dta", clear
keep if year == 1990 
keep income_rate_avg fipstate
rename fips statefips
tempfile incentives2 
save "`incentives2'"

  
*use "$output/pr_link_est_county_addvars.dta", clear 
use "$output_NETS/pr_link_est_county_addvars.dta", clear 
merge m:1 statefips using "`incentives'"
drop if _merge == 2 
drop _merge 

merge m:1 statefips using "`incentives2'"
drop if _merge == 2 
drop _merge 

** Add nafta variables 
merge 1:1 fips_county using "$output/import_hakobyanlaren_nafta.dta"
drop if _merge == 2 
drop _merge 

gen ln_pop = ln(population ) 
gen ln_GDP = ln(GDP) 
gen ln_rev = ln(rev_to)
gen rev_gdp = rev_tot/GDP 

gen zero_pr = (pr_link == 0) 

**** Balance table and graphs 
label var realmw "Minimum Wage"
label var rgtowork "Right to Work"
label var income_rate_avg "Personal Income Tax"
label var rec_val "R&D Credit" 
label var rev_gdp "State Revenue/GDP"
label var d_tradeusch_pw "Trade Exposure (China)"
label var locdt_noag "Trade Exposure (Nafta)"
label var l_sh_routine33a "Share of Routine Labor"

label var labor_participation_rate "Labor Force Participation"
label var pct_retail "Percentage Retail"
label var pct_agriculture "Percentage Agriculture"
label var pct_manufacturing_durable "Percentage Manufacturing Durable" 
label var pct_manufacturing_nondurable "Percentage Manufacturing Non-Durable"
label var capital_stock "Capital Stock"
label var pct_college "Percentage College"
label var pct_less_HS "Percentage Less than HS"
label var pct_white "Percentage White"
label var pct_black "Percentage Black"

local base " ln_pop "   
local spec1 " realmw rgtowork income_rate_avg salestax propertytax corpinctax rec_val rev_gdp d_tradeusch_pw l_sh_routine33a locdt_noag " 
local spec2a "labor_participation_rate pct_retail pct_agriculture pct_manufacturing_durable pct_manufacturing_nondurable capital_stock  pct_college  pct_less_HS pct_white  pct_black "
local spec2 "`base' `spec2a'" 

set more off 
foreach var in pr_link `spec2' `spec1'  { 
	summ `var' [fw=pop]
	replace `var' = (`var'-r(mean))/r(sd)
} 

capture: gen cons = 1
local FE1 "cons"
local FE2 "statefips"

*** Make Graphs 
est clear 
forval i = 1/2 {   
		qui: eststo reg_`i': areg pr_link  `spec`i'' [fw=pop] , a(`FE`i'') cl(statefips ) 
	} 	
** Note even using the weighting from ADH there is no significant correlation
*qui: eststo reg_7: reg pr_link `FE2' `spec2' if inrange(pr_link,r(p1),r(p99))   [aw=timepwt48]	 , cl(statefips ) 	
	
esttab reg_* , 	star(* .1 ** .05 *** .01)  keep( `spec1' `spec2') drop(ln_pop)

est restore reg_1
coefplot  , drop(_cons ln_pop) xline(0) level(95) xtitle("Standardized Effect") graphregion(color(white))  xlab(-1(.25)1) ciopts(recast(rcap))
graph export "$output/Graphs/balance1.pdf", replace 

est restore reg_2
coefplot  , drop(_cons ln_pop *statefips*) xline(0) level(95) xtitle("Standardized Effect") graphregion(color(white)) xlab(-1(.25)1) ciopts(recast(rcap))
graph export "$output/Graphs/balance2.pdf", replace 

******************* Tables 
est clear 

local base " ln_pop "   
local spec1 " realmw rgtowork income_rate_avg salestax propertytax corpinctax rec_val rev_gdp d_tradeusch_pw l_sh_routine33a locdt_noag" 
local spec2a "labor_participation_rate pct_retail pct_agriculture pct_manufacturing_durable pct_manufacturing_nondurable capital_stock  pct_college  pct_less_HS pct_white  pct_black "
local spec2 "`base' `spec2a'" 

capture: gen cons = 1
local FE1 "cons"
local FE2 "statefips"

gen COV = . 
est clear
local i = 1
foreach var of varlist `spec1' {
	replace COV = . 
	replace COV = `var'
	qui: eststo reg1_`i': areg pr_link COV `spec1' [fw=pop] , a(`FE1') cl(statefips ) 
	local i=`i'+1
}

esttab reg1_* , 	star(* .1 ** .05 *** .01)   keep(COV)

local i = 1
foreach var of varlist `spec2a' {
	replace COV = . 
	replace COV = `var'
	qui: eststo reg2_`i': areg pr_link COV `spec2' [fw=pop] , a(`FE2') cl(statefips ) 
	local i=`i'+1
}

esttab reg2_* , 	star(* .1 ** .05 *** .01)   keep(COV)

********************* make outputs 

esttab reg1_1 reg1_2 reg1_3 reg1_4 reg1_5 using "$output/Tables/table_balance_20171107.tex", keep(COV)  ///
cells(b(fmt(3)) se(par) p) coll(none) s() noobs mlab("Min Wage" "Right to Work" "Pers. Income Tax" "Sales Tax" "Prop. Tax")  /// 
coeflabel(COV "\hspace{1em}Exposure to Section 936") ///
postfoot("\hline") replace sty(tex)

** NOTE: DAN TOOK OUT reg1_11 because the other rows all have 5 specs and that was what was represented in the draft 4-18-2018
esttab reg1_6 reg1_7 reg1_8 reg1_9 reg1_10 using "$output/Tables/table_balance_20171107.tex", keep(COV)  ///
cells(b(fmt(3)) se(par) p)  coll(none) s() noobs ///
mlab("Corporate Tax" "R\&D Credit" "State Revenue/GDP" "Trade Exposure (China)" "Share of Routine Labor"  "Trade Exposure (Nafta)" ) /// 
coeflabel(COV "\hspace{1em}Exposure to Section 936") ///
preh("") postfoot("\hline") append sty(tex)

esttab reg2_1 reg2_2 reg2_3 reg2_4 reg2_5 using "$output/Tables/table_balance_20171107.tex", keep(COV)  ///
cells(b(fmt(3)) se(par) p)  coll(none) s() noobs /// 
mlab("LFPR" "\% Retail" "\% Agriuclture" "\% Manuf. Durable" "\% Manuf. Non-Durable")  ///
coeflabel(COV "\hspace{1em}Exposure to Section 936") ///
preh("") postfoot("\hline") append sty(tex)

esttab reg2_6 reg2_7 reg2_8 reg2_9 reg2_10 using "$output/Tables/table_balance_20171107.tex", keep(COV)  ///
cells(b(fmt(3)) se(par) p)  coll(none) s() noobs /// 
mlab("Capital Stock" "\% College" "\% Less HS" "\% White" "\% Black") ///
coeflabel(COV "\hspace{1em}Exposure to Section 936") ///
preh("") prefoot("\hline") ///
postfoot( ///
"  \\ \hline\hline" ///
"\end{tabular} }" ) append sty(tex)
