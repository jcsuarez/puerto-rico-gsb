//// Get PR firm data 

clear all
set more off
*ssc install dataout
snapshot erase _all


// Second Define Command for ES 
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(solid)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Estimate") label(2 "`level'% CI")) ///
	ytitle("`yti'") ///
	note("`note'") ylab("`ylab'")
graph export "`outname'.pdf", replace	

end 



*** get list of hqduns that actually work by dropping gov't etc... 
use "$output_NETS/pr_extract_est.dta", clear

gen naic4 = floor(naics95/100)
gen naic3 = floor(naics95/1000)
gen naic2 = floor(naics95/10000)

********************
*getting rid of governmental and social non-profit orgs
********************
drop if naic2 == 92 | naic3 == 813
* dropping USPS and the federal government
drop if hqduns95 == 3261245 | hqduns95 == 161906193

* dropping firms with HQ in PR
drop if pr_based == 1 

* dropping county industries with no link
drop if pr_link == 0
bysort hqduns: egen temp_max = max(emp95)
gen temp_keep = (emp95 == temp_max)

keep if temp_keep == 1

* making a list of hqduns to use from the panel data
keep hqduns naic3
bysort hqduns: egen m_naic3 = mode(naic3), minmode
drop naic3
duplicates drop 
tempfile tokeep 
save "`tokeep'"

** old code 
use "$output_NETS/pr_extract_panel.dta", clear 

** Keep only firms at start of the sample 
gen temp = emp_PR if year == 1990
bysort hqd: egen base_emp_PR = min(temp)
drop temp 
keep if base > 0  // This line drops 4,991 observations

** MErge in to keep 
merge m:1 hqduns95 using "`tokeep'"
keep if _merge == 3 // This drops 23 observations from the master, 214 from using

egen firm = group(hqd) 
sum firm, d
snapshot save

keep hqd year emp_US num_est_US 
gen PR = 1 

tempfile PRdata
save "`PRdata'" 

/// Get control data 
* Get control Data 
use "$output_NETS_control/hq_est_co_firm_master.dta", clear
drop if emp95 < 6
drop if num_est95 < 3

*sample 1000, count by(firm_group)
keep if fipsstate95 != . 
drop if fipsstate95 > 56
rename fipsstate95 hqfips
 
gen firm_group2 = floor(firm_group/10000)
gen census_div = floor(firm_group2/1000)
gen naic3 = firm_group2-1000*census_div
drop firm_group2
gen PR = 0 

drop hqfips census_div naic3 PR firm_group 

foreach y in 90 91 92 93 94 95 96 97 98 99 00 01 02 03 04 05 06 07 08 09 10 11 12 {
	rename emp`y' emp_`y'
	rename num_est`y' num_`y'
}

reshape long emp_ num_ , i(hqduns) j(year) string
 
destring year , replace 
replace year = year+1900 if year > 50 
replace year = year+2000 if year < 50 
sort year
rename emp_ emp_US
rename num_ num_est_US
gen PR = 0 

append using "`PRdata'"

// emp growth 
gen temp = emp_US if year == 1995
bysort hqduns95 : egen base_emp = max(temp)
drop temp

gen emp_growth = (emp_US - base_emp)/base_emp
replace emp_growth = 0 if emp_growth == . 

// estab growth 
gen temp = num_est_US if year == 1995
bysort hqduns95 : egen base_est = max(temp)
drop temp

gen est_growth = (num_est_US - base_est)/base_est
replace est_growth = 0 if est_growth == . 


drop if base_emp ==0 
sum base_emp if year == 1995 & PR , d
gen wgt=base_emp/(r(N) * r(mean)) if PR 

sum base_emp if year == 1995 & PR==0 , d
replace wgt=base_emp/(r(N) * r(mean)) if PR==0 

** Make balanced panel by dropping firms that drop out
bys hqduns95 : egen min_emp = min(emp_US) 
drop if min_emp == 0 


xi i.year|PR, noomit 
drop _IyeaXP*1995
  
 est clear
 
eststo emp_1:  reghdfe emp_growth _IyeaXP* ///
  [aw=wgt] , a(year)  vce(cluster hqduns)
  
ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
outname("$output/Graphs/NETS_firm") ylab("-.12(.02).04")

estimates save 	"$output/comparison_emp", replace 

// outputting scalars for the text
do "$analysis/jc_stat_out.ado"
local dif = -_b[_IyeaXPR_2012] * 100
jc_stat_out,  number(`dif') name("RESULTIV") replace(0) deci(1) figs(3)

/*
eststo emp_2:  reghdfe est_growth _IyeaXP* ///
  [aw=wgt] , a(year) vce(cluster hqduns)
  
 ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Est{sub:it}-Est{sub:i1995})/Est{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
outname("$output/Graphs/NETS_firm_est") ylab("-.08(.02).04")	 
 */

 
 esttab emp* using "$output/Tables/table_ES_QCEW_comparison.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons ///
postfoot("\multicolumn{1}{l}{\textbf{Exposure to Section 936}}") nonum posthead("")


esttab emp*   using "$output/Tables/table_ES_QCEW_comparison.tex",  preh("") ///
 b(3) se par mlab("Employment Growth" "Establishment Growth") s() noobs nogap  /// 
coeflabel(_IyeaXPR_1990 "\hspace{1em}X 1990 " /// 
_IyeaXPR_1991 "\hspace{1em}X 1991 " /// 
_IyeaXPR_1992 "\hspace{1em}X 1992 " /// 
_IyeaXPR_1993 "\hspace{1em}X 1993 " /// 
_IyeaXPR_1994 "\hspace{1em}X 1994 " /// 
_IyeaXPR_1996 "\hline \hspace{1em}X 1996 " /// 
_IyeaXPR_1997 "\hspace{1em}X 1997 " /// 
_IyeaXPR_1998 "\hspace{1em}X 1998 " /// 
_IyeaXPR_1999 "\hspace{1em}X 1999 " /// 
_IyeaXPR_2000 "\hspace{1em}X 2000 " /// 
_IyeaXPR_2001 "\hspace{1em}X 2001 " /// 
_IyeaXPR_2002 "\hspace{1em}X 2002 " /// 
_IyeaXPR_2003 "\hspace{1em}X 2003 " /// 
_IyeaXPR_2004 "\hspace{1em}X 2004 " /// 
_IyeaXPR_2005 "\hspace{1em}X 2005 " /// 
_IyeaXPR_2006 "\hspace{1em}X 2006 " /// 
_IyeaXPR_2007 "\hspace{1em}X 2007 " /// 
_IyeaXPR_2008 "\hspace{1em}X 2008 " /// 
_IyeaXPR_2009 "\hspace{1em}X 2009 " /// 
_IyeaXPR_2010 "\hspace{1em}X 2010 " /// 
_IyeaXPR_2011 "\hspace{1em}X 2011 " /// 
_IyeaXPR_2012 "\hspace{1em}X 2012 " )  label /// 
 prefoot("") ///
postfoot( ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex)







