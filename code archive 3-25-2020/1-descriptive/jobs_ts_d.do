//  making a plot of the number of jobs in PR and the US of firms with a link to PR
* 12-22-2017
* Use Firm Level Data 
* Runs Event-Study Analyses 

// Update 2-12-2018: (1) The large firms table and the TS graph seem to have different results.
//					 (2) 

clear
do "$analysis/jc_stat_out.ado"
set more off
ssc install dataout
snapshot erase _all

*** get list of hqduns that actually work by dropping gov't etc... 
use "$output_NETS/pr_extract_est.dta", clear

gen naic4 = floor(naics95/100)
gen naic3 = floor(naics95/1000)
gen naic2 = floor(naics95/10000)

********************
*getting rid of governmental and social non-profit orgs
********************
drop if naic2 == 92 | naic3 == 813
* dropping USPS and the federal government
drop if hqduns95 == 3261245 | hqduns95 == 161906193

* dropping firms with HQ in PR
drop if pr_based == 1 

* dropping county industries with no link
drop if pr_link == 0
bysort hqduns: egen temp_max = max(emp95)
gen temp_keep = (emp95 == temp_max)

keep if temp_keep == 1

* making a list of hqduns to use from the panel data
keep hqduns naic3
bysort hqduns: egen m_naic3 = mode(naic3), minmode
drop naic3
duplicates drop 
tempfile tokeep 
save "`tokeep'"

** old code 
use "$output_NETS/pr_extract_panel.dta", clear 

** Keep only firms at start of the sample 
gen temp = emp_PR if year == 1990
bysort hqd: egen base_emp_PR = min(temp)
drop temp 
keep if base > 0  // This line drops 4,991 observations

** MErge in to keep 
merge m:1 hqduns95 using "`tokeep'"
keep if _merge == 3 // This drops 23 observations from the master, 214 from using

egen firm = group(hqd) 
sum firm, d
snapshot save

/*
*/

collapse (sum) emp_US (sum) emp_PR (sum) num_est_PR (sum) num_est_US , by(year) 
replace emp_US = emp_US/1000000
replace emp_PR = emp_PR/10000
replace num_est_US = num_est_US/10000
replace num_est_PR = num_est_PR/100

twoway (scatter emp_US year, connect(line) xline(1995, lcolor(black) lpattern(dash))) ///
	   (scatter emp_PR year, connect(line) lpattern(dash) ) ///
		, plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white)) ///
		xtitle("Year") xlab(1990(5)2011) ylab(5(2)18) ///
		legend(label(1 "US (1,000,000)") label(2 "PR (10,000)")) ///
		ytitle("Total Employment by Firms Exposed to S 936")  ///
		note("Employment of `num' Firms Exposed to S 936.")
	
graph export "$output/Graphs/ts_jobs_d.pdf", replace		   

twoway (scatter num_est_US year, connect(line) xline(1995, lcolor(black) lpattern(dash))) ///
	   (scatter num_est_PR year, connect(line) lpattern(dash) ) ///
		, plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white)) ///
		xtitle("Year") xlab(1990(5)2011) ylab(5(2)18) ///
		legend(label(1 "US (10,000)") label(2 "PR (100)")) ///
		ytitle("Total Establishments by Firms Exposed to S 936")  ///
		note("Establishments of `num' Firms Exposed to S 936.")

graph export "$output/Graphs/ts_ests_d.pdf", replace		


** getting list of 10 firms with most US employment in 1995
snapshot restore 1

// outputting scalars for the paper
egen test = group(hqduns)
sum test
local b = r(max)
jc_stat_out,  number(`b') name("RESULTVIII") replace(0) deci(0) figs(3)
drop test
sum emp_PR if year == 1990
sum emp_US if year == 1990
local b = r(mean) * r(N) / 1000000
jc_stat_out,  number(`b') name("RESULTX") replace(0) deci(1) figs(3)

sum emp_US if year == 1995
local b = r(mean) * r(N) / 1000000
jc_stat_out,  number(`b') name("RESULTXI") replace(0) deci(0) figs(2)
global value = `b'

rename hqduns95 hqduns
drop _merge 

gen ind = m_naic3

* dan changed to 1995 on 10-24-2018

bys year: egen tot_US = sum (emp_US)
bys year: egen tot_PR = sum (emp_PR)
bys year: egen tot = sum (emp_total)
gen temp_1 = tot * (year == 1995)
gen temp_2 = tot * (year == 2006)

egen tot_1995 = max(temp_1)
egen tot_2006 = max(temp_2)

gen tot_change = tot_2006 - tot_1995
gen tot_pchange = (tot_2006 - tot_1995) / tot_1995

keep if year == 1995
local minPR "1500"
keep if emp_PR >= `minPR'


/*
gsort - emp_PR
gen id = _n
drop if id > 20
*/
tempfile topten
save "`topten'", replace
* merging in names and such 
use "$output_NETS2/pr_extract_panel_v2_company.dta", clear
merge m:1 hqd using "`topten'"
keep if _merge == 3
drop _merge

gen emp06 = emp_total *(year == 2006)
bys hqduns: egen emp2006 = max(emp06)
gen emp90 = emp_total *(year == 1990)
bys hqduns: egen emp1990 = max(emp90)
gen emp95 = emp_total *(year == 1995)
bys hqduns: egen emp1995 = max(emp95)

gen emp_change = emp2006 - emp1995
gen emp_pchange = (emp2006 - emp1995) / emp1995 

keep hqd company emp_US emp_PR emp_change emp_pchange year ind tot_US tot_PR tot_change tot_pchange
keep if year == 1995
duplicates drop
drop if emp_PR < 1500

sum emp_US
local b = round(r(mean) * r(N),10000)
jc_stat_out,  number(`b') name("RESULTIX") replace(0) deci(0) figs(7)

gsort -emp_PR
keep company emp_US emp_PR ind emp_change emp_pchange tot_US tot_PR tot_change tot_pchange
replace company = subinstr(company, "&", "\&",.)
label var company "Company"
label var emp_US "US Employment"
label var emp_PR "Puerto Rico Employment"
rename company Company
rename emp_US US
rename emp_PR PR
rename emp_change Change
rename emp_pchange Percent

expand 2, gen(new)
bysort new: egen totUS = sum(US)
bysort new: egen totPR = sum(PR)
bysort new: egen totchange = sum(Change)
gen totpchange = totchange / (totUS + totPR)
bysort new: gen id = _n
replace ind = . if new == 1
replace Company = "\hline \textbf{Sub Total}" if new == 1 & id == 1
replace Company = "\hline \textbf{Total}" if new == 1 & id == 2
drop if new == 1 & id > 2
replace US = totUS if Company == "\hline \textbf{Sub Total}"
replace PR = totPR if Company == "\hline \textbf{Sub Total}"
replace US = tot_US if Company == "\hline \textbf{Total}"
replace PR = tot_PR if Company == "\hline \textbf{Total}"
replace Change = totchange if Company == "\hline \textbf{Sub Total}"
replace Percent = totpchange if Company == "\hline \textbf{Sub Total}"
replace Change = tot_change if Company == "\hline \textbf{Total}"
replace Percent = tot_pchange if Company == "\hline \textbf{Total}"
keep Company US PR ind Change Percent
form US PR %12.0gc
rename US US2
rename PR PR2
rename Change Change2

replace Percent = Percent * 100
rename Percent Percent2
gen US = string(US2, "%12.0gc")
gen PR = string(PR2, "%12.0gc")
gen Change = string(Change2, "%12.0gc")
gen Percent = string(Percent2, "%6.1fc")
replace Percent = Percent + "\%"
keep Company US PR ind Change Percent

replace Company = ltrim(Company) 
replace Company = rtrim(Company) 

* industries manually from googling (done for top 1990 firms)
gen Industry = "Chemicals"
replace Industry = "Wholesale and Retail" if Company == "BAXTER INTERNATIONAL INC"  ///
| Company == "PXC \& M HOLDINGS INC" | Company == "FLUOR CORPORATION" | Company == "KMART CORPORATION" ///
| Company == "SEARS ROEBUCK AND CO" | Company == "AVON PRODUCTS INC"
replace Industry = "Food Mfg" if Company == "UNI GROUP INC" | Company == "SARA LEE CORPORATION"  ///
| Company == "H J HEINZ COMPANY" | Company  == "RJR ACQUISITION CORP"
replace Industry = "Other Mfg" if Company == "DIGITAL EQUIPMENT CORPORATION" | Company == "JOHNSON \& JOHNSON" | Company == "AMR CORPORATION"
replace Industry = " " if ind == .
replace Industry = "Other Non-Mfg" if Company == "CBS CORPORATION"  | Company == "BERKSHIRE HATHAWAY INC"
replace Industry = "Electrical Equipment" if Company == "GENERAL ELECTRIC COMPANY"
replace Industry = "Services" if Company == "FLUOR CORPORATION"
replace Industry = "Beverages" if Company == "PEPSICO INC"

keep Company US PR Industry Change Percent

replace Company = "WESTINGHOUSE (LATER CBS)" if Company == "CBS CORPORATION" 

*tostring US PR, replace format(%12.0gc)

cd "$output/Tables"
drop Change Percent
dataout, tex replace save(large_firms_v2) noauto


/* This doesn't work because NAICS are badly defined for this purpose (NAICS differently recorded at establishment level, hard to pull out main one)
*label define l_naic3 1 "Food Mfg" 2 "Textile mill products" 3 "Apparel" 4 "Chemicals" 5 "Pharmaceuticals" ///
* 6 "Rubber and Plastic" 7 "Leather" 8 "Fabricated metal" 9 "Machinery" 10 "Electrical equip" ///
* 11 "Instruments" 12 "Other mfg" 13 "Finance, insurance, real estate" 14 "Services" ///
* 15 "Wholesale and retail" 16 "Other non-mfg", replace
gen m_naic3 = ind
gen m_naic2 = floor(ind/10)
replace m_naic2 = m_naic3 if m_naic2 < 10

gen Industry = " "
replace Industry = "Food Mfg" if m_naic3 == 311
replace Industry = "Textile Mill Products" if m_naic3 == 314
replace Industry = "Apparel" if m_naic3 == 315
replace Industry = "Chemicals" if m_naic3 == 325
replace Industry = "Rubber and Plastic" if m_naic3 == 326
replace Industry = "Other Mfg" if Industry == " " & (m_naic2 == 31 | m_naic2 == 32 | m_naic2 == 33)
replace Industry = "Finance, Insurance, and Real Estate" if m_naic2 == 52 | m_naic2 == 53
replace Industry = "Services" if m_naic2 == 54 | m_naic2 == 55 | m_naic2 == 61 | m_naic2 == 62 | m_naic2 == 71 | m_naic2 == 72 | m_naic2 == 81
replace Industry = "Wholesale and Retail" if m_naic2 == 42 | m_naic2 == 44 | m_naic2 == 45
replace Industry = "Other Non-Mfg" if Industry == " " & m_naic2 != .
*/

** replace string = substring(string, 1,strlen(string)-3)+",”+substring(string, strlen(string)-3, strlen(string))
