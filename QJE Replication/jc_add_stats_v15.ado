capture: program drop jc_add_stats_v15
program jc_add_stats_v15 
syntax , ene(real) [stat(string) ]
tempname temp_b temp_V

local N = r(df) 
if `ene' != 0 { 
	local N = `ene'
	} 

mat b = r(b)
scalar b = b[1,1]
mat V = r(V)
scalar se = sqrt(V[1,1])
local `stat'_EST = round(b,.001) 
local `stat'_SE = round(se,.001) 
local `stat'_pval = 2*ttail(`N',abs(b/se))

local `stat'_EST: di %6.0fc ``stat'_EST'
local `stat'_SE: di %6.0fc ``stat'_SE'
local `stat'_pval: di %4.3fc ``stat'_pval'

/*
if (``stat'_pval' <= 0.01)  {
	local star "\sym{***}" 
	}	
	else if  (``stat'_pval'  > 0.01 & ``stat'_pval'  <= 0.05  ) {
	local star "\sym{**}" 
	}	
	else if (``stat'_pval' > 0.05 & ``stat'_pval'  <= 0.1  ) {
	local star "\sym{*}" 
	} 	
	else  {
	local star "" 
	}
local `stat'_EST "``stat'_EST'`star'"
*/
local `stat'_SE  "(``stat'_SE')"
*di "`stat'" 
*di "``stat'_EST'"
*di "``stat'_SE'"

estadd local `stat' = "``stat'_EST'"
estadd local `stat'_SE = "``stat'_SE'"
estadd local `stat'_pval = "``stat'_pval'"
end
