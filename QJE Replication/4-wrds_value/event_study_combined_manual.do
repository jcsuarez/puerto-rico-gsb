clear
capture log close
set more off
capture close
snapshot erase _all
set matsize 8000
***************************************************************
//Filename: event_study_20170730.do
//Author: dgg
//Date: 20170808
//Task: Event studies all dates for PR project
// 		Goal is to make 3 tables with 5 specs each
//			Spec1: CAPM with full sample
//			Spec2: FF3 + M with full sample, (value weighted?)
***************************************************************

*stocks1990-1996 is the old file with only exact name matches
use "$WRDS/stocks1991-1996", replace

* rescalling the outcome
replace ret = ret * 100

{
***** Organizing data
* making a list of even dates (checked manually for correctness)
local event_dates "12100 13068 13083"
*11647 is 11/21/1991 Pryor introduces the Prescription Drug Cost Containment Act in previous week.
*12053 is 1/3/1993
*12100 is Feb 16, 1993, the Tuesday after Clinton and Pryor talked
*12145 is 4-2-1993
*12183 is 5-10-1993
*12257 is 7-23-1993
*12276 is 8-11-1993
*12913 is 5-10-1995
*13016 is 8-21-1995 (post weekend)
*13040 is 9-14-1995
*13068 is 10-12-1995
*13083 is 10-27-1995 (note the overlap)
*13291 is 5-22-1996
*13338 is july 8, 1996, the monday after the Senate voted (post-weekend)
*13362 is August 1, 1996, congress killed 936
}


*generating numbers of trading days instead of actual dates
sort permno date
by permno: gen datenum=_n 
foreach event_date in `event_dates' {
by permno: gen target`event_date'=datenum if date==`event_date'
egen td`event_date'=min(target`event_date'), by(permno)
drop target`event_date'
gen dif`event_date'=datenum-td`event_date'
}
* Defining the estimation and event windows (counting windows to make sure they
* 											 actually are trading on those days)
local window = 100
local min_obs = 70
local prewindow = 5

foreach event_date in `event_dates' {
by permno: gen event`event_date'=1 if dif`event_date'>=0 & dif`event_date'<=15 & ret != .
replace event`event_date'=0 if event`event_date' == .

*including some pre-period
by permno: gen pre_event`event_date'=1 if dif`event_date'>=-`prewindow' & dif`event_date'<=15 & ret != .
replace pre_event`event_date'=0 if pre_event`event_date' == .

* estimation windows will also be used to cap the event windows so there is only 1 dummy
* variable when outputting tables.
forvalues i=0(1)5 {
local j = `i' * 3

by permno: gen estimation`event_date'_`i'=1 if dif`event_date'>= -`window' & dif`event_date'<=`j' & ret != .
replace estimation`event_date'_`i'=0 if estimation`event_date'_`i' == .
* cutting out estimation on firms with limited pre-estimation periods
by permno: egen sample_size = sum(estimation`event_date'_`i')
replace estimation`event_date'_`i'=0 if sample_size < `min_obs' 

* Getting a non-event sample for estimating counterfactuals and CARs
by permno: gen pre_est`event_date'_`i'=1 if dif`event_date'>= -`window' & dif`event_date'< 0 & ret != .
replace pre_est`event_date'_`i'=0 if pre_est`event_date'_`i' == .

*dropping those firms without enough observations for estimation in the pre-sample
replace pre_est`event_date'_`i'=0 if sample_size < `min_obs' 
drop sample_size

* generating interactions for graphical explanation of event studies
gen event_int_`event_date'_`i' =  pre_event`event_date' * (dif`event_date' + `prewindow' + 1) * estimation`event_date'_`i'
}
}
expand 2 if estimation12100_5 == 1, gen(eve1)
expand 2 if estimation13068_5 == 1, gen(eve2)

gen pooled_eve = eve1 + eve2
keep if pooled_eve == 1

*Sometimes dividends are given in two lines, dropping those (does not affect return, price, etc.)
duplicates drop permno date, force

tempfile events
save "`events'", replace

*** Pulling in the FF 3 factors, momentum, and risk free rate.
{
* These data also include market returns but they are less precise than those from CRSP

import delimited "$WRDS/F-F_Research_Data_Factors_daily.txt",  clear delim(" ", collapse) stringc(1)
*turning dates into a form that matches what we already have
rename date date_1
gen date = date(date_1, "YMD")
format date %tdNN/DD/CCYY
drop date_1

tempfile factors
save "`factors'", replace

**** merging factors with stock returns
use "`events'", clear

merge m:1 date using "`factors'"
keep if _merge == 3
drop _merge
save "`events'", replace

* now momentum
import delimited "$WRDS/F-F_Momentum_Factor_daily.txt",  clear delim(" ", collapse) stringc(1)
*turning dates into a form that matches what we already have
rename date date_1
gen date = date(date_1, "YMD")
format date %tdNN/DD/CCYY
drop date_1

tempfile factors
save "`factors'", replace

**** merging factors with stock returns
use "`events'", clear

merge m:1 date using "`factors'"
keep if _merge == 3
drop _merge
rename mom MOM

save "`events'", replace
}


************************************************
* Estimating abnormal returns using 4 factors
************************************************
xtset permno datenum

*creating values from previous trading day for value weighting
*NOTE: Some prices are coded as negatives (not -99 that means missing) so I use the absolute value
gen val = abs(l.prc * l.shrout)
egen id=group(permno eve1)

* Now testing for significance over longer intervals (Directly implementing Coups regressions)
local spec1 "c.mktrf"
local spec2 "c.mktrf c.smb c.hml c.MOM"
local spec3 "c.mktrf"
local spec4 "c.mktrf c.smb c.hml c.MOM"
local spec5 "c.mktrf"
local spec6 "c.mktrf c.smb c.hml c.MOM"

set more off
estimates drop _all

* Pooled regression for Feb 16, 1993 and Oct 12, 1995

forvalues k=1(1)6 {
forvalues i=0(1)5 {
* spec specific samples
local spec1_other "if estimation12100_`i' == 1 | estimation13068_`i' == 1"
local spec2_other "if estimation12100_`i' == 1 | estimation13068_`i' == 1"
local spec3_other "if estimation12100_`i' == 1 "
local spec4_other "if estimation12100_`i' == 1 "
local spec5_other "if estimation13068_`i' == 1 "
local spec6_other "if estimation13068_`i' == 1 "

local j = `i' * 3
qui gen event_dummy= event12100 / (1 + `j') + event13068 / (1 + `j')
qui eststo reg_`i'_`k': areg ret i.id#(`spec`k'') event_dummy `spec`k'_other', r a(id)
drop event_dummy
}

*Using the pre-period for a seventh column
local spec3_other "if eve1 == 1 "
local spec4_other "if eve1 == 1 "
local spec5_other "if eve2 == 1 "
local spec6_other "if eve2 == 1 "
qui gen event_dummy= pre_event12100 / (21) + pre_event13068 / (21)
qui eststo reg_6_`k': areg ret i.id#(`spec`k'') event_dummy `spec`k'_other', r a(id)
drop event_dummy
}

// scalars for use in the text
do "$analysis/jc_stat_out.ado"

estimates restore reg_4_1
local b = _b[event_dummy]
jc_stat_out,  number(`b') name("RESULTXXVI") replace(0) deci(1) figs(3)


* Outputting tables mirroring Coups paper
esttab reg_?_1 using "$output/Tables/pooled_event_20171214.tex", keep(_cons) stats() ///
b(3) se par label coeflabel(event_dummy "\hspace{1em}Event") ///
noobs nogap tex replace nocons postfoot("") mtit("(0,0)" "(0,3)" "(0,6)" "(0,9)" "(0,12)" "(0,15)" "(-5,15)") nonum

estout reg_?_1 using "$output/Tables/pooled_event_20171214.tex", keep(event_dummy) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) ///
varlabel(event_dummy "\hspace{1em}Event") ///
preh("\multicolumn{8}{l}{\textbf{Pooled CAPM}}\\") prefoot("\hline")  postfoot(" \hline ")  append sty(tex)

estout reg_?_2 using "$output/Tables/pooled_event_20171214.tex", keep(event_dummy) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) ///
varlabel(event_dummy "\hspace{1em}Event") ///
preh("\multicolumn{8}{l}{\textbf{Pooled Fama-French 3 Factors Plus Momentum}}\\") prefoot("\hline")  postfoot( ///
" \hline "  ) append sty(tex)

estout reg_?_3 using "$output/Tables/pooled_event_20171214.tex", keep(event_dummy) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) ///
varlabel(event_dummy "\hspace{1em}Event") ///
preh("\multicolumn{8}{l}{\textbf{February 16, 1993 Event CAPM}}\\") prefoot("\hline")  postfoot(" \hline ")  append sty(tex)

estout reg_?_4 using "$output/Tables/pooled_event_20171214.tex", keep(event_dummy) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) ///
varlabel(event_dummy "\hspace{1em}Event") ///
preh("\multicolumn{8}{l}{\textbf{February 16, 1993 Event Fama-French 3 Factors Plus Momentum}}\\") prefoot("\hline")  postfoot( ///
" \hline " ) append sty(tex)

estout reg_?_5 using "$output/Tables/pooled_event_20171214.tex", keep(event_dummy) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) ///
varlabel(event_dummy "\hspace{1em}Event") ///
preh("\multicolumn{8}{l}{\textbf{October 12, 1995 CAPM}}\\") prefoot("\hline")  postfoot(" \hline ")  append sty(tex)

estout reg_?_6 using "$output/Tables/pooled_event_20171214.tex", keep(event_dummy) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) stats(N, nostar /// 
labels("Observations") fmt(%9.0fc 3)) ///
varlabel(event_dummy "\hspace{1em}Event") ///
preh("\multicolumn{8}{l}{\textbf{October 12, 1995 Fama-French 3 Factors Plus Momentum}}\\") prefoot("\hline")  postfoot( ///
" \hline " ///
"\end{tabular} }" ) append sty(tex)


***********************************************************
* Making a graph for the 16 day version of each ( i = 5 )
***********************************************************
set more off
local spec1 "c.mktrf"
local spec2 "`spec1' c.smb c.hml c.MOM"
local event_dates "12100 13068"

local i = 5
forvalues k=1(1)2 {
* with lags and leads
gen x`k' = 0
replace x`k' = event_int_12100_`i' + event_int_13068_`i'
eststo reg_`k': reg ret i.id i.id#(`spec`k'') ib0.x`k' if estimation12100_`i'==1 | ///
 estimation13068_`i'==1,  r

}

*
* Aggregating up individual estimates of AR into CAR (Matching Investment graph style
forvalues k=1(1)2 {

* part 1
estimates restore reg_`k'
gen fig_b_`k'_pre = 0
gen fig_se_`k'_pre = 0
gen fig_b_`k' = 0
gen fig_se_`k' = 0

*counting up from t = -prewindow
local agg "1.x"
forvalues i=1(1)21 {
lincom (`agg')

local j = `i' + 1
replace fig_b_`k'_pre=r(estimate) in `j'
replace fig_se_`k'_pre=r(se) in `j'
local agg "`agg' + `j'.x"
}
*counting up from the event
local agg "6.x"
forvalues i=6(1)21 {
lincom (`agg')

local j = `i' + 1
replace fig_b_`k'=r(estimate) in `j'
replace fig_se_`k'=r(se) in `j'
local agg "`agg' + `j'.x"
}
gen fig_upper_`k'_pre=fig_b_`k'_pre+1.645*fig_se_`k'_pre
gen fig_lower_`k'_pre=fig_b_`k'_pre-1.645*fig_se_`k'_pre
gen fig_upper_`k'=fig_b_`k'+1.645*fig_se_`k'
gen fig_lower_`k'=fig_b_`k'-1.645*fig_se_`k'
}
gen fig_t = _n - 7 in 1/22
gen fig_t2 = fig_t + .2

* Actual graphs
twoway (line fig_b_1_pre fig_t if fig_t<16 & fig_t>-8, msymbol(diamond) xline(-0.0, lc(gs11)) c(l) lpattern(dash) lcolor(cranberry) lwidth(medthick)) ///
	(rcap fig_upper_1_pre fig_lower_1_pre fig_t  if fig_t<16 & fig_t>-8, lpattern(dash) lcolor(cranberry) c(l) m(i) lwidth(medthick) yline(0, lcolor(black))) ///
	(line fig_b_1 fig_t2 if fig_t<16 & fig_t>-8, msymbol(diamond) c(l) lpattern(solid) lcolor(navy) lwidth(medthick)) ///
	(rcap fig_upper_1 fig_lower_1 fig_t2  if fig_t<16 & fig_t>-8, lpattern(solid) lcolor(navy) c(l) m(i) lwidth(medthick)), /// 
	xlabel(-6 (3) 15) ///
	graphregion(fcolor(white)) ///
	xtitle("Days from Event") ///
	ytitle("CAR (Percent)") ///
	legend(order(1 2 3 4) label(1 "Cumulative Abnormal Return with Leads")  label(2 "90% CI") ///
	label(3 "Cumulative Abnormal Return without Leads") label(4 "90% CI"))
graph export "$output/Graphs/pooled_event_20171214_CAPM.pdf", replace

twoway (line fig_b_2_pre fig_t if fig_t<16 & fig_t>-8, msymbol(diamond) xline(-0.0, lc(gs11)) c(l) lpattern(dash) lcolor(cranberry) lwidth(medthick)) ///
	(rcap fig_upper_2_pre fig_lower_2_pre fig_t  if fig_t<16 & fig_t>-8, lpattern(dash) lcolor(cranberry) c(l) m(i) lwidth(medthick) yline(0, lcolor(black))) ///
	(line fig_b_2 fig_t2 if fig_t<16 & fig_t>-8, msymbol(diamond) c(l) lpattern(solid) lcolor(navy) lwidth(medthick)) ///
	(rcap fig_upper_2 fig_lower_2 fig_t2  if fig_t<16 & fig_t>-8, lpattern(solid) lcolor(navy) c(l) m(i) lwidth(medthick)), /// 
	xlabel(-6 (3) 15) ///
	graphregion(fcolor(white)) ///
	xtitle("Days from Event") ///
	ytitle("CAR (Percent)") ///
	legend(order(1 2 3 4) label(1 "Cumulative Abnormal Return with Leads")  label(2 "90% CI") ///
	label(3 "Cumulative Abnormal Return without Leads") label(4 "90% CI")) 
graph export "$output/Graphs/pooled_event_20171214_FF.pdf", replace
