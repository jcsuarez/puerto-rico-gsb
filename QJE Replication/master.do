* This .do file runs all cleaning and reduced form analysis
* Author: JCSS
* Date: 11/27/2017
* updated: 02/19/2018

** For Dan
global dropbox "C:/Users/dangg/Dropbox (JC's Data Emporium)/Puerto Rico"
global dropbox "C:/Users/Dan's Work PC/Dropbox (JC's Data Emporium)/Puerto Rico"

** For JC Laptop
global dropbox "/Users/jcsuarez/Dropbox (JC's Data Emporium)/RESEARCH/Puerto Rico"
** For JC on backup drive in Office 
global dropbox "/Volumes/DropboxBackup/Dropbox/Dropbox (JC's Data Emporium)/RESEARCH/Puerto Rico"
** For JC on Work drive in Office 
global dropbox "/Volumes/DropboxOffice/DropboxOffice/Dropbox (JC's Data Emporium)/RESEARCH/Puerto Rico"
** For JC on backup drive at Home
global dropbox "/Volumes/New Seagate Backup Plus Drive/Backup/Dropbox (JC's Data Emporium)/RESEARCH/Puerto Rico"
** For JC on Linux Laptop 
global dropbox "/media/jcsuarez/LargoDrive/Dropbox (JC's Data Emporium)/RESEARCH/Puerto Rico"

** Install Packages 
ssc install listtex, replace all 
ssc install estout, replace all 
ssc install cdfplot, replace all
ssc install winsor, replace all
ssc install reghdfe, replace all
ssc install tuples, replace all
ssc install coefplot, replace all 
ssc install spmap, replace all 
ssc install maptile, replace all 
ssc install ivreg2, replace all 
ssc install ftools, replace all
ssc install ivreghdfe, replace all 
ssc install ivreg2hdfe, replace all
ssc install reg2hdfe, replace all 
ssc install moremata, replace all
ssc install tmpdir, replace all
maptile_install using "http://files.michaelstepner.com/geo_county1990.zip"
maptile_install using "http://files.michaelstepner.com/geo_state.zip"

** Global Dropbox Paths (raw to clean data)
global netspath 	"/Volumes/My Passport/"  // These data live in an external hard drive 
global output 		"$dropbox/Programs/output"
global data 		"$dropbox/Data"
global analysis 	"$dropbox/Programs"
global linkdata		"$dropbox/Programs/output/base_1995"
global xwalk		"$dropbox/Data/Cross Walk"
global qcewdata		"$dropbox/Data/QCEW"
global bea			"$dropbox/Data/BEA"
global consp		"$dropbox/Data/conspuma"
global additional 	"$dropbox/Data/data_additional"
global output_NETS 	"$dropbox/Programs/output/NETS_20171128"
global output_NETS2 "$dropbox/Programs/output/NETS_20171129_v2"
global output_NETS_control "$dropbox/Programs/output/NETS_control"
global WRDS			"$dropbox/Data/WRDS"
global ASM			"$dropbox/Data/ASM"
global SOI			"$dropbox/Data/SOI Data"
global irs 			"$dropbox/Data/IRS"     

*** Start
cd "$analysis"
clear
set more off 
	
*initializing ado files to format additional stats
do "$analysis/jc_add_stats.ado"
do "$analysis/jc_stat_out.ado"

********** NETS Pr link build: Run on External Drive 
do "$analysis/0-build/nets_pr_extract.do"						// Extract firms with an establishment in PR, runs on external harddrive
do "$analysis/0-build/nets_pr_extract_panel_v2.do"				// Extract firms with an establishment in PR in panel format, runs on external harddrive
do "$analysis/0-build/nets_pr_extract_naic3.do"					// Extract firms with an establishment in PR, runs on external harddrive and includes all NAICS3 codes (and md, fd, and emp)

********** Build Data for NETS Controls: Run on External Drive 
do "$analysis/0-build/Nets_control_panel/get_pr_exposed_characteristics_region2.do"	// selects the 682 firms we care about and then from those selects
											// the actual state and insustries we care about (census region and 3 digit NAICS)
 																						
do "$analysis/0-build/Nets_control_panel/get_HQs_controls_region2.do" 		// This code goes over all the estabs in the NETS and selects: 
										//	1- Those that are headquarters only (hqduns == duns definition)
										//	2- Those that are around before 1995 (in 1995 and 1990)
										//	3- Drops Industries and firms that are suspect (gov related duns and NAICS)
										//	4- Keeps Those in 3d NAICS-census region combos that match PR exposed firms 
										//	5- Saves the HQDuns for these firms 
																						
do "$analysis/0-build/Nets_control_panel/get_HQs_size_region2.do"		// This code goes over all the estabs in the NETS and selects: 
										// 1- All estabs linked to control firms (with HQs in list) 
										// 2- Gathers just employment and establishments in 1995
										// 3- collapses at hq level 
										// 4- merges all files and collapses again 
										// 5- keeps only those firms in the relevant size categories (emp and est quitiles)
										// 6- This reduces the number of potential HQ firms. 

do "$analysis/0-build/Nets_control_panel/get_control_panel_region2.do" 		// This code goes over all the estabs in the NETS and selects: 
										//	1- All estabs linked to control firms (with HQs in list by industry and size) 
										//	2- Gathers employment and establishment counts by year 
										//	3- Collapses at the hq level 
										//	4- merges all files and collapses again 
********** NETS FAKE Pr link build: Run on External Drive 
do "$analysis/0-build/placebo_nets_shock.do" 					// Generates placebo shock of similar firms that are not exposed to S 936 


********** Build Data for Analysis
do "$analysis/0-build/percent_pharma.do" 	 					// creates county level pharma exposure variables (124 positive values)  
do "$analysis/0-build/county_level_data_all.do"   				// Import additional county-level data from various sources, saves "county_level_data_all.dta"
do "$analysis/0-build/compustat_merge_manual.do" 				// Merge existing NETS data with COMPUSTAT data
do "$analysis/0-build/compustat_ETR_calc.do" 					// Include ETR variables in compustat data
do "$analysis/0-build/compustat_ETR_calc_segments.do" 			// Merge compustat segments data with ETR calculation variables
do "$analysis/0-build/qcew_extract_naic3.do"					// Extract QCEW files: extrac_naic3 keeps everything at 3 digit NAIC level
do "$analysis/0-build/clean_ASM.do" 							// Imports and cleans ASM data
do "$analysis/0-build/nafta_merge.do" 	 						// saves import_hakobyanlaren_nafta.dta with nafta exposure 
do "$analysis/0-build/CBP_firm_size_extract.do" 	 			// extracts firm size distribution 
do "$analysis/0-build/pr_link_est_county_addvars.do"			// Merge NETS and county-level data
do "$analysis/0-build/welfare_import.do" 						// welfare transfers

********** additional build files with some graphs and scalars										
do "$analysis/0-build/SOI_import.do" 								// Import some basic IRS SOI stats about US possessions tax credit
do "$analysis/0-build/pr_link_compustat_sample.do" 					// PR link made with only compustat sample and some relevant scalars

********** Run Descriptive Analysis 
do "$analysis/1-descriptive/balance_table.do" 				// merge additional data and runs balance table
do "$analysis/1-descriptive/maps_d.do"						// Maps PR link
do "$analysis/1-descriptive/descriptive.do"					// Merge NTES Link Data and QCEW Outcome Data: Produce descriptive graphs 
do "$analysis/1-descriptive/jobs_ts_d.do" 					// Use Firm Level Data: Runs Event-Study Analyses, includes list of firms with more than 1500 PR employees
do "$analysis/3-wrds_segments/tax_credits_to_fed_taxes_ratio.do" 		// Contextualizes tax credit size and outputs scalars for paper. 

do "$analysis/1-descriptive/jobs_ts_co_d.do" 					// Uses control NETS firms and PR firms to create time series graphs of jobs lost 
do "$analysis/1-descriptive/jobs_ts_co_firm_d_2018_03_09.zo" 	// Tried to do same as above but with firm-level Fixed Effects. 

do "$analysis/1-descriptive/top_exposure_tables.do" 			// includes list of firms with more than 1500 PR employees
do "$analysis/1-descriptive/model_graphs.do" 			// 

********** Run Analysis on Local Labor Markets 
do "$analysis/2-main_analysis/robust_inter_aw.do"						// Event-study with extra covariates
do "$analysis/2-main_analysis/robust_inter_figure.do"					// Event-study with interactions on extra terms
do "$analysis/2-main_analysis/conspuma_analysis_d_2018_03_01.do"		// Before and after on local conspuma outcomes (rent, wage, etc)
do "$analysis/2-main_analysis/ADH_county_regression_levels.do" 			// ADH style table and figure, differences to 1995 (2005 is the relevant coefficient for long dif.) 
do "$analysis/2-main_analysis/ADH_county_regression.do" 				// ADH style table and figure, differences to 1995 (2005 is the relevant coefficient for long dif.) 

********** Run Analysis on Local Labor Markets (using aweights instead of fweights) FIRST 3 MUST BE RUN IN ORDER
do "$analysis/2-main_analysis/es_county_aw.do" 						// Runs Event-Study Analysis robustness table (county level) 
do "$analysis/2-main_analysis/es_conspuma_aw.do" 					// Runs Event-Study Analysis robustness table (conspuma level)
do "$analysis/2-main_analysis/es_czone_aw.do"						// Runs Event-Study Analysis robustness table  
do "$analysis/2-main_analysis/es_county_emp_inst.do" 				// Instruments emp with est shock.  Runs Event-Study Analysis robustness table (county level) 
do "$analysis/2-main_analysis/es_county_aw_md.do" 					// Manually deletes walmart, walgreens, mcdonalds, radioshack, etc. 
do "$analysis/2-main_analysis/es_county_aw_per_capita_20180523.do" 	// Per capita regressions
do "$analysis/2-main_analysis/es_county_aw_fd.do" 					// Doesn't include PR based firms or firms without a US HQ
do "$analysis/2-main_analysis/es_county_aw_compu_sample_naics3.do" 	// Doesn't include firms not in compustat (RAW)
do "$analysis/2-main_analysis/es_county_aw_pr_link_a.do"			// Runs Event-Study Analysis robustness table using all industry exposure (county level) 
do "$analysis/2-main_analysis/es_county_aw_fake.do" 				// FAKE shock finds no result. Runs Event-Study Analysis robustness table (county level)  Only runs figure
do "$analysis/2-main_analysis/es_county_aw_pharma.do" 				// Controls for share of pharma. Runs Event-Study Analysis robustness table (county level) Only runs figure

********** Figures
* Figure 8 with recession shading:
do "$analysis/2-main_analysis/Fig_7_20180122_shading.do"  			// exports extra with dots and shading (parametric)
do "$analysis/2-main_analysis/Fig_7_20180128_shading.do" 			// non-parametric with shading

do "$analysis/2-main_analysis/het_effects_county_aw.do" 			// heterogeneous effects by industy (naic2-naic4)
do "$analysis/2-main_analysis/het_effects_county_aw_Mian_Sufi.do" 	// heterogeneous effects by industy (Mian and Sufi Tradable definitions)
do "$analysis/2-main_analysis/het_effects_county_aw_Mian_Sufi_2.do" // heterogeneous effects by industy (Mian and Sufi Tradable definitions)

do "$analysis/2-main_analysis/ES_Graphs_all.do" 					// Combination of first 7 event studies with combined specs in line graphs
do "$analysis/2-main_analysis/es_county_d_IRS_and_BEA.do" 			// Event-study with IRS data and BEA data (includes 3 geography specs)
do "$analysis/2-main_analysis/combined_appendix_tables_20180515.do"		// Combination of first 7 event studies with combined specs into long diff table
do "$analysis/2-main_analysis/transfers_county_regression_20180430.do" 	// effect of S936 exposure on transfer payments, long diff tables

********** Run Analysis on Industry-Level Outcomes 
do "$analysis/5-ASM/asm_analysis_combined_d.do"						// Regressions of investment, employment on PR link using American Survey of Manufacturers data.

********** Run Analysis on Firm-Level Outcomes 
do "$analysis/3-wrds_segments/investment_responses_2018_07_03.do" 		// foreign share and I/K specs all in one place
do "$analysis/3-wrds_segments/investment_responses_2018_07_11_ftax.do" 	// change in federal taxes paid as a percent of pretax income
do "$analysis/3-wrds_segments/cap_emp_binscatter.do" 					// binscatter of capex and employment 
do "$analysis/3-wrds_segments/depreciation_calcs.do" 					// graph of capital stock change with investment decrease 
do "$analysis/3-wrds_segments/PR_presense_probit_JC.do" 				// graph of capital stock change with investment decrease 

********** Run Analysis on Firm-Level Outcomes : Stock Prices 
do "$analysis/4-wrds_value/event_study_combined_manual.do"			// Stock price event study using base compustat sample
do "$analysis/4-wrds_value/event_study_ETR_PR_20170115_JC.do"		// Stock price event study including ETR variables
do "$analysis/2-main_analysis/long_diff_table.do" 					// Contains some scalars for headline results
do "$analysis/2-main_analysis/Sec63scalars.do" 						// Contains some scalars for headline results
//end of run on 7/7/18

* New QR Regression split over different runs 
do "$analysis/2-main_analysis/qreg_data_prep.do"
do "$analysis/2-main_analysis/qr_run_1.do"
do "$analysis/2-main_analysis/qr_run_2.do"
do "$analysis/2-main_analysis/qr_run_3.do"
do "$analysis/2-main_analysis/qr_run_4.do"
do "$analysis/2-main_analysis/qr_run_5.do"
do "$analysis/2-main_analysis/qr_graph_table.do"
*do "$analysis/2-main_analysis/qreg_county_2_bstrap.do"				// Event-study outputs $output/Tables/table_qreg_2_bstrap












// OLD:
/*
do "$analysis/2-main_analysis/es_county_emp.do" 								// Runs Event-Study Analysis robustness table (county level) 
do "$analysis/2-main_analysis/es_county_d.do" 									// Runs Event-Study Analysis robustness table (county level) 
do "$analysis/2-main_analysis/es_conspuma_d.do" 								// Runs Event-Study Analysis robustness table (conspuma level)
do "$analysis/2-main_analysis/es_czone_d.do"									// Runs Event-Study Analysis robustness table  
do "$analysis/2-main_analysis/robust_inter.do"									// Event-study
do "$analysis/2-main_analysis/Fig_7_20170122.do" 								// parametric with extra dots
do "$analysis/2-main_analysis/Fig_7_20170128.do" 								// non-parametric without shading
do "$analysis/2-main_analysis/es_county_aw_naic3_test.do" 						// Runs Event-Study Analysis robustness table using all 3d naics and old PR links
do "$analysis/2-main_analysis/es_county_dan.do" 								// Defines prlink at industry-place level (effect without accounting for spillovers) 
do "$analysis/2-main_analysis/es_czone_emp_inst.do" 							// Instruments emp with est shock.  Runs Event-Study Analysis robustness table (czone level) only outputs figure
do "$analysis/2-main_analysis/qreg_county_2.do"									// Event-study
do "$analysis/2-main_analysis/es_county_aw_naic3.do" 							// Runs Event-Study Analysis robustness table using all 3d naics
do "$analysis/2-main_analysis/es_county_aw_fsd_naics3.do" 							// Controls for firm size distribution. Runs Event-Study Analysis robustness table (county level) 
do "$analysis/2-main_analysis/long_diff_table.do" 				// long difference table combining first 3 event studies with extra outcomes
do "$analysis/3-wrds_segments/segments_analysis_expandedsample_JC.do"		// Change in foreign shate investment for firms with compustat segments data 
do "$analysis/3-wrds_segments/segments_analysis_IK_20180226.do" 		// I/K graph and table (must be before cap_emp_binscatter) 	


// SET OF TABLES TRYING TO COMBINE INTO A SINGLE TABLE (run on 4-18-2018 for tasks #2 and #3) 
do "$analysis/2-main_analysis/es_conspuma_aw.do" 					// A7
do "$analysis/2-main_analysis/es_czone_aw.do"						// A8
do "$analysis/2-main_analysis/ADH_county_regression.do" 			// A9
do "$analysis/2-main_analysis/es_county_emp_inst.do" 				// A11
do "$analysis/2-main_analysis/es_county_aw_md.do" 					// A12
do "$analysis/2-main_analysis/es_county_aw_fd.do" 					// A13
do "$analysis/2-main_analysis/es_county_aw_compu_sample_naics3.do"  // A14	

* The one below crashes, not sure we use it anyways
//do "$analysis/2-main_analysis/es_county_aw_fsd.do" 				// Controls for firm size distribution. Runs Event-Study Analysis robustness table (county level) 

/*
********** Run Analysis on Local Labor Markets (using Employment link instead of establishment link)
do "$analysis/2-main_analysis/es_county_emp_aw.do" 								// Runs Event-Study Analysis robustness table (county level) 
do "$analysis/2-main_analysis/es_conspuma_emp.do" 								// Runs Event-Study Analysis robustness table (conspuma level)
do "$analysis/2-main_analysis/es_czone_emp.do"									// Runs Event-Study Analysis robustness table  
*/

do "$analysis/0-build/qcew_extract.do"						// Extract QCEW files


/*
********** additional files with employment data from NETS
do "$analysis/0-build/nets_pr_extract_emp.do"						// Extract firms with an establishment in PR, runs on external harddrive and includes employment variables
do "$analysis/0-build/pr_link_est_county_addvars_d.do"				// Merge NETS (including employment and county-level data
do "$analysis/0-build/nets_pr_extract_manual_drop.do"				// Extract firms with an establishment in PR dropping some manually picked large firms, runs on external harddrive and includes all NAICS3 codes
do "$analysis/0-build/nets_pr_extract_foreign_frop.do"				// Extract firms with an establishment in PR exluding PR and potentially foreign based, runs on external harddrive and includes all NAICS3 codes
*/										

do "$analysis/1-descriptive/ETR_desc.do" 						// Descriptive stats for compustat ETR characteristics in the sample (split PR and non PR)
do "$analysis/1-descriptive/ETR_Flavor_Table.do" 				// Saves ETR characteristics for 10 largest firms in a separate data set (used in footnote 12)


