* This .do file merges county level exposure to the great recession with PR exposure
* Author: Dan Garrett
* Date: March 2, 2020
clear 
snapshot erase _all

/* goals:
1- binscatter of PR exposure and Mian-Sufi drop
2- Event study with and without MS controls
3- event study, top half vs. bottom half in MS measure
*/

***************************************************
*** starting with the measures of exposure to PR
***************************************************
// A.0. Get regional crosswalk read 
use "$xwalk/cw_cty_czone.dta", clear
rename cty_fips pid 
duplicates drop 
tempfile cross
save "`cross'"

// A.0. Get link data 
use "$output_NETS/pr_link_est_countyXindustry_d.dta", clear 
gen pr_emp = pr_link*total if industry_cd < 13
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)

gen pr_l = tot_pr/tot_emp 
keep pr_l fips
rename  fips pid
rename pr_l pr_link_ind
duplicates drop 
tempfile pr 
save "`pr'" 

use "$output_NETS/pr_link_est_county_d.dta", clear 
rename  fips pid
tempfile pr2 
save "`pr2'" 

// A.1. Get QCEW data 
use "$qcewdata/extract_qcew_1990_2012_naic3.dta", clear
keep if strlen(industry_code) == 3
drop if industry_code == "101" | industry_code == "102"

order fips_state fips_county year industry_code
drop if fips_county == 0 & fips_state != 72
drop if fips_state == 72 & fips_county != 0
keep if own_code == 5		// 0-total covered; 1 Federal; 2 State; 3 Local; 4 Intl Govt; 5 Private.
keep fips_state fips_county year industry_code annual_avg_estabs annual_avg_emplvl total_annual_wages
drop if fips_state == 78
collapse (sum) annual_avg_estabs (sum) annual_avg_emplvl (sum) total_annual_wages, by(fips_state fips_county year industry_code)

// A.3. Fill in gaps
gen pid = 1000*fips_state+fips_county

// A.6. Merge in pr link 
merge m:1 pid using "`pr'"
keep if _merg == 3
drop _merge 

merge m:1 pid using "`pr2'"
drop if year == . 
drop if pr_link == . 
drop _merge 
rename pr_link pr_link_all

*replace pr_link_all = pr_link_ind

sum pr_link_al, d 

drop if year == . 

*collapse (rawsum) annual_avg_estabs (rawsum) annual_avg_emplvl (rawsum) total_annual_wages (mean) pr_link_ind (mean) pr_link_all [fw=total] , by( fips_state pid year industry_code) // Dan: this isn't actually collapsing anything

egen pid2 = group(pid indus)

tsset pid2 year
tsfill, full

foreach var of varlist fips_state  {
bysort pid2: egen temp = max(`var')
replace `var' = temp
drop temp
}

foreach var of varlist annual_avg_estabs annual_avg_emplvl total_annual_wages {
replace `var' = 0 if `var' == .
}

// A.4.  Growth rates. Base year: 1995
gen temp = annual_avg_emplvl if year == 1995
bysort pid2: egen base_emp = max(temp)
drop temp

gen emp_growth = (annual_avg_emplvl - base_emp)/base
replace emp_growth = 0 if emp_growth == . 


// Income 
gen inc = total_annual_wages
* /annual_avg_emplvl
gen temp = inc if year == 1995
bysort pid2: egen base_inc = max(temp)
drop temp

gen inc_growth = (inc - base_inc)/base_inc
replace inc_growth = 0 if inc_growth == . 

// Estabs 
gen estab = annual_avg_estabs
gen temp = estab if year == 1995
bysort pid2: egen base_estab = max(temp)
drop temp

gen estab_growth = (estab - base_estab)/base_estab
replace estab_growth = 0 if estab_growth == . 

** For now, keep only balanced industries . Matt: can you fix it so we don't have to do this? 
bys pid2: gen count = _N
keep if count == 23 
drop count 

drop if base_emp ==0 

sum base_emp if year == 1995, d
gen wgt=base_emp/(r(N) * r(mean))
winsor wgt, p(.01) gen(wgt_w)

sum pr_link_i $pr_wgt if base_emp > 0, d 
replace pr_link_i = pr_link_i/(r(p75)-r(p25))

sum pr_link_al  $pr_wgt if base_emp > 0, d 
replace pr_link_al = pr_link_al/(r(p75)-r(p25))

tsset pid2 year 
egen ind_st = group(ind fips_state)

tempfile pr_exp
save "`pr_exp'", replace

***************************************************
** Pulling in and saving the housing price changes from mian and sufi
***************************************************

use "$dropbox/Data/MianSufiEconometrica_PublicReplicationFiles/miansufieconometrica_countylevel", clear
rename fips pid

// NOTE: Mian and Sufi collected a whole lot of county level variables in 2007, I drop those:
keep pid netwp_h

merge 1:m pid using "`pr_exp'"
drop _merge

***************************************************
** Programs for graphing the Event Studies
***************************************************

// First Define Command for ES data 
capture program drop ES_graph_data
program define ES_graph_data
syntax,  level(real) yti(string) tshifter(real)


qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 


forval i = 1/23 { 
	mat time = (time,`i'-6+1995 + `tshifter')
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci_l es_ci_h time 
svmat es_b 
rename es_b `yti'_es_b
svmat time
rename time `yti'_time
svmat es_ci_l
rename es_ci_l `yti'_es_ci_l
svmat es_ci_h 
rename es_ci_h `yti'_es_ci_h
}

end  
 
// Second Define Command for ES 
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(dash))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Estimate") label(2 "`level'% CI")) ///
	ytitle("`yti'") ///
	note("`note'") ylab("`ylab'")
graph export "`outname'.pdf", replace	

end 


***************************************************
** Binscatter
***************************************************

** two versions of the binscatter: 1 raw, 1 residualized for state FE
ssc install binscatter, replace

** Raw
* getting slope and p-value
reg netwp_h pr_link_ind [aw=base_emp] if year == 1995 & netwp != ., vce(cluster fips_state) 

local b = round(_b[pr_link_ind],.001)
local se = round(_se[pr_link_ind],.001)
mat table = r(table) // b, se, t, and pvalue for first independent variable
local p = round(table[4,1],.001)
binscatter netwp_h pr_link_i  [aw=base_emp],  /// 
		graphregion(fcolor(white)) ///
		xtitle("Exposure to S936 (IQR)") ///
		ytitle("Change in Housing Net Worth, 2006-2009") ///
		note("The slope of the regression line is  `b' with standard error `se' and p-value `p'")
graph export "$output/mian_sufi_bin_20200304.pdf", replace 

* residualized
capture: drop res_netwp res_pr_link_i
areg netwp [aw=base_emp] if year == 1995 & netwp != ., a(fips_state) 
predict res_netwp, residuals
areg pr_link_i [aw=base_emp] if year == 1995 & netwp != ., a(fips_state) 
predict res_pr_link_i, residuals

reghdfe netwp pr_link_ind [aw=base_emp] if year == 1995 & netwp != ., a(fips_state) cl(fips_state) 

local b = round(_b[pr_link_ind],.001)
local se = round(_se[pr_link_ind],.001)
mat table = r(table) // b, se, t, and pvalue for first independent variable
local p = round(table[4,1],.001)
binscatter  res_pr_link_i res_netwp [aw=base_emp],  /// 
		graphregion(fcolor(white)) ///
		xtitle("Residualized Exposure to S936 (IQR)") ///
		ytitle("Residualized Change in Housing Net Worth, 2006-2009") ///
		note("The slope of the regression line is  `b' with standard error `se' and p-value `p'")
graph export "$output/mian_sufi_bin_20200304_resid.pdf", replace 


***************************************************
** Regressions
***************************************************

// B. Graphs with All PR Link 
cd "$output" 
set more off 
xi i.year|pr_link_i  $pra_control, noomit
drop _IyeaXp*1995

*keep if base_emp > 400  
winsor base_emp , g(w_emp_base) p(.025)
*replace base_emp = w_emp_base
sum base_emp, d 
*drop if base_emp <= r(p1) 
*drop if base_emp >= r(p99)
destring industry_code, replace
est clear 
set more off

/*  // regressions we need
reghdfe emp_growth _IyeaX* if netwp != . [aw=wgt] , a( i.pid i.year#i.indust ) cl(fips_state indust )
reghdfe emp_growth _IyeaX* c.netwp#i.year if netwp != . [aw=wgt] , a( i.pid i.year#i.indust ) cl(fips_state indust )
sum netw
sum netw [aw=base_emp], d
reghdfe emp_growth _IyeaX* if netwp != . & netwp >= -.059176 [aw=wgt] , a( i.pid i.year#i.indust ) cl(fips_state indust )
reghdfe emp_growth _IyeaX* if netwp != . & netwp < -.059176 [aw=wgt] , a( i.pid i.year#i.indust ) cl(fips_state indust )
*/

eststo emp_1:  reghdfe emp_growth _IyeaX* [aw=wgt]  , a(i.year#i.industr ) cl(fips_state indust ) nocons
	estadd local hasiy "Yes"
    estadd local hascty ""

ES_graph_data , level(95) yti("spec1")	tshifter(-0.1)
ES_graph , level(95) yti("Percent Change in Employment") ///
note("Notes: (Emp{sub:ict}-Emp{sub:ic1995})/Emp{sub:ic1995}={&alpha}{sub:c}+{&gamma}{sub:it}+{&beta}{sub:t}S936 Exposure{sub:c}+{&epsilon}{sub:ict}. SE's clustered by State and Industry.") ///
outname("$output/Graphs/ES_miansufi_nocontrol") ylab("-.15(.02).04")

eststo emp_2:  reghdfe emp_growth _IyeaX* c.netwp#i.year [aw=wgt]  , a(i.year#i.industr ) cl(fips_state indust ) nocons
	estadd local hasiy "Yes"
	estadd local hascty "Yes"

ES_graph_data , level(95) yti("spec2")	tshifter(0)
twoway (scatter spec2_es_b spec2_time if  spec1_time != ., lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black)) lpattern(solid))   ///
		(rcap spec2_es_ci_l spec2_es_ci_h spec2_time if  spec1_time != .,  lcolor(navy) lpattern(dash))   ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Estimate Including Mian-Sufi Control") label(2 "95% CI") ///
	r(1) order( 1 2)) ///
	ytitle("Percent Change in Employment") 
graph export "$output/Graphs/ES_miansufi_control.pdf", replace	


* Graph output mirroring figure 3 in http://gabriel-zucman.eu/files/teaching/FuestEtal16.pdf
 twoway (line spec1_es_b spec1_time if  spec1_time != ., lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black)) lpattern(solid))  ///
		(rcap spec1_es_ci_l spec1_es_ci_h spec1_time if  spec1_time != .,  lcolor(navy) lpattern(solid))  ///
		(line spec2_es_b spec2_time if  spec1_time != ., lcolor(maroon) lpattern(dash_dot))      ///
		(rcap spec2_es_ci_l spec2_es_ci_h spec2_time if  spec1_time != .,  lcolor(maroon) lpattern(dash_dot))   /// 
		/// (line spec3_es_b spec3_time if  spec1_time != ., lcolor(red) lpattern(solid))   ///
		/// (rcap spec3_es_ci_l spec3_es_ci_h spec3_time if  spec1_time != .,  lcolor(red) lpattern(solid))   ///
		/// (line spec4_es_b spec4_time if  spec1_time != ., lcolor(purple) lpattern(dash))    ///
		/// (rcap spec4_es_ci_l spec4_es_ci_h spec4_time if  spec1_time != .,  lcolor(purple) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "(1)") ///
	label(3 "(2)")   ///
	/// label(5 "(3)")  /// 
	/// label(7 "(4)")  ///
	r(1) order( - "Specification:" 1 3 )) ///
	ytitle("Percent Change in Employment") 
graph export "$output/miansufi_ES_1-2.pdf", replace	

	
esttab emp* using "$output/Tables/table_ES_miansufi_20200303.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons ///
postfoot("\multicolumn{1}{l}{\textbf{Exposure to Section 936}}") nonum posthead("")


esttab emp*   using "$output/Tables/table_ES_miansufi_20200303.tex",  preh("") ///
 b(3) se par mlab(none) coll(none) s() noobs nogap keep(_IyeaXpr__*)  /// 
coeflabel(_IyeaXpr__1990 "\hspace{1em}X 1990 " /// 
_IyeaXpr__1991 "\hspace{1em}X 1991 " /// 
_IyeaXpr__1992 "\hspace{1em}X 1992 " /// 
_IyeaXpr__1993 "\hspace{1em}X 1993 " /// 
_IyeaXpr__1994 "\hspace{1em}X 1994 " /// 
_IyeaXpr__1996 "\hline \hspace{1em}X 1996 " /// 
_IyeaXpr__1997 "\hspace{1em}X 1997 " /// 
_IyeaXpr__1998 "\hspace{1em}X 1998 " /// 
_IyeaXpr__1999 "\hspace{1em}X 1999 " /// 
_IyeaXpr__2000 "\hspace{1em}X 2000 " /// 
_IyeaXpr__2001 "\hspace{1em}X 2001 " /// 
_IyeaXpr__2002 "\hspace{1em}X 2002 " /// 
_IyeaXpr__2003 "\hspace{1em}X 2003 " /// 
_IyeaXpr__2004 "\hspace{1em}X 2004 " /// 
_IyeaXpr__2005 "\hspace{1em}X 2005 " /// 
_IyeaXpr__2006 "\hspace{1em}X 2006 " /// 
_IyeaXpr__2007 "\hspace{1em}X 2007 " /// 
_IyeaXpr__2008 "\hspace{1em}X 2008 " /// 
_IyeaXpr__2009 "\hspace{1em}X 2009 " /// 
_IyeaXpr__2010 "\hspace{1em}X 2010 " /// 
_IyeaXpr__2011 "\hspace{1em}X 2011 " /// 
_IyeaXpr__2012 "\hspace{1em}X 2012 " )   scalars(  "hasiy Industry-by-Year Fixed Effects" "hascty 2008 Recession Exposure-by-Year Controls" ///
"hasicty Smallest House Price Declines, 2006-2009" ///
"hasww Largest House Price Declines, 2006-2009" ) label /// 
 prefoot("\hline") ///
postfoot( ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex)

***************************************************
* wide table with 3 specifications pooling effects in 2004-2008
***************************************************


* making an interaction term for PR and NETWP
sum netwp [aw=wgt], d
gen z_netwp = (netwp - r(mean)) / r(sd)
sum pr_link_ind [aw=wgt], d
gen z_pr_link_ind = pr_link_ind - r(mean)

gen inter = z_pr_link_ind * z_netwp

eststo regr1: reghdfe emp_growth z_pr_link_ind  [aw=wgt] if inrange(year,2004,2008) , a(i.year#i.industr ) cl(fips_state indust ) nocons
	estadd local hasiy "Yes"
eststo regr2: reghdfe emp_growth z_pr_link_ind z_netwp  [aw=wgt] if inrange(year,2004,2008) , a(i.year#i.industr ) cl(fips_state indust ) nocons
	estadd local hasiy "Yes"
eststo regr3: reghdfe emp_growth z_pr_link_ind z_netwp inter [aw=wgt] if inrange(year,2004,2008) , a(i.year#i.industr ) cl(fips_state indust ) nocons
	estadd local hasiy "Yes"

eststo prregr1: reghdfe emp_growth z_pr_link_ind  [aw=wgt] if inrange(year,1990,1995) , a(i.year#i.industr ) cl(fips_state indust ) nocons
	estadd local hasiy "Yes"
eststo prregr2: reghdfe emp_growth z_pr_link_ind z_netwp  [aw=wgt] if inrange(year,1990,1995) , a(i.year#i.industr ) cl(fips_state indust ) nocons
	estadd local hasiy "Yes"
eststo prregr3: reghdfe emp_growth z_pr_link_ind z_netwp inter [aw=wgt] if inrange(year,1990,1995) , a(i.year#i.industr ) cl(fips_state indust ) nocons
	estadd local hasiy "Yes"


esttab regr*  using "$output/Tables/mian_sufi_test_pooled.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons ///
postfoot("")

*postfoot(" & \multicolumn{2}{l}{Employment Growth}& \multicolumn{2}{l}{Establishment Growth} &\multicolumn{2}{l}{Income Growth} \\")

esttab regr*  using "$output/Tables/mian_sufi_test_pooled.tex", keep(z_pr_link_ind z_netwp inter)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s() noobs nonum /// 
coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 (IQR)" z_netwp "\hspace{1em}Net Housing Wealth Change, 2006-09 (SD)" inter "\hspace{1em}Interaction" ) ///
preh("\multicolumn{6}{l}{\textbf{Years: 2004-2008}}\\") postfoot("\hline") append sty(tex)

esttab prregr*  using "$output/Tables/mian_sufi_test_pooled.tex", keep(z_pr_link_ind z_netwp inter)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s()  nonum /// 
coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 (IQR)" z_netwp "\hspace{1em}Net Housing Wealth Change, 2006-09 (SD)" inter "\hspace{1em}Interaction" ) ///
preh("\multicolumn{6}{l}{\textbf{Years: 1990-1995}}\\") noobs scalars(  "hasiy Industry-by-Year Fixed Effects" ) label /// 
 prefoot("\hline") ///
postfoot( ///
"  \\ \hline\hline" ///
"\end{tabular} }" ) append sty(tex)



esttab regr*  using "$output/Tables/mian_sufi_test_pooled_nopre.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons ///
postfoot("")

*postfoot(" & \multicolumn{2}{l}{Employment Growth}& \multicolumn{2}{l}{Establishment Growth} &\multicolumn{2}{l}{Income Growth} \\")

esttab regr1 regr2 regr3  using "$output/Tables/mian_sufi_test_pooled_nopre.tex", keep(z_pr_link_ind z_netwp inter)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s() nonum  /// 
coeflabel(z_pr_link_ind "\hspace{1em}Exposure to Section 936 (IQR)" z_netwp "\hspace{1em}Net Housing Wealth Change, 2006-09 (SD)" inter "\hspace{1em}Interaction" ) ///
preh("\multicolumn{4}{l}{\textbf{Years: 2004-2008}}\\") noobs scalars(  "hasiy Industry-by-Year Fixed Effects" ) label /// 
 prefoot("\hline") ///
postfoot( ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex)





/*
eststo emp_3:  reghdfe emp_growth _IyeaX* c.netwp#i.year [aw=wgt] if netwp != 0 & netwp >= -.059176, a(i.year#i.industr ) cl(fips_state indust ) nocons
	estadd local hasiy "Yes"
	estadd local hasicty "Yes"

ES_graph_data , level(95) yti("spec3")	tshifter(0.1)
twoway (scatter spec3_es_b spec3_time if  spec1_time != ., lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black)) lpattern(solid))   ///
		(rcap spec3_es_ci_l spec3_es_ci_h spec3_time if  spec1_time != .,  lcolor(navy) lpattern(dash))   ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Estimate") label(2 "95% CI") ///
	r(1) order( 1 2)) ///
	ytitle("Percent Change in Employment") 
graph export "$output/Graphs/ES_miansufi_tophalf.pdf", replace	

eststo emp_4:  reghdfe emp_growth _IyeaX* c.netwp#i.year [aw=wgt] if netwp != 0 & netwp < -.059176, a(i.year#i.industr ) cl(fips_state indust ) nocons
	estadd local hasiy "Yes"
	estadd local hasww "Yes"

ES_graph_data , level(95) yti("spec4")	tshifter(0.2)
twoway (scatter spec4_es_b spec4_time if  spec1_time != ., lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black)) lpattern(solid))   ///
		(rcap spec4_es_ci_l spec4_es_ci_h spec4_time if  spec1_time != .,  lcolor(navy) lpattern(dash))   ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Estimate") label(2 "95% CI") ///
	r(1) order( 1 2)) ///
	ytitle("Percent Change in Employment") 
graph export "$output/Graphs/ES_miansufi_bothalf.pdf", replace	
