* This file merges county data with the migration data from Hassan et al. (2018)
* author: dan
* date: 10-9-2018

clear all
set more off
snapshot erase _all

/* code from Hassan we are approximately replicating here:
***************************************************************************************************************************
* Table 3: Second-stage: The effect of Ancestry on FDI
***************************************************************************************************************************
cd "${wkdir}"
use "Input/Replication.dta", replace

// Get rid of 2010 immigration
rename immi_nc_d_X_o_ndv_10 XXimmi_nc_d_X_o_ndv_10

*************************** Table Three Panel A ********************************

// Panel A Column One
qui ivreg2hdfe, depvar(country_dummy) en(log_ancestry_2010) ex(dist distance_lat) iv(immi_nc_d_X_o_ndv_*) id1(country_code1990) id2(state_county_code_1990) cluster(country_code1990) 
estimates store TableThreeA_1
 
// Panel A Column Two
qui ivreg2hdfe, depvar(country_dummy) en(log_ancestry_2010) ex(dist distance_lat) iv(immi_nc_d_X_o_ndv_*  pcFour*) id1(country_code1990) id2(state_county_code_1990) cluster(country_code1990) 
estimates store TableThreeA_2

// Panel A Column Three
qui ivreg2hdfe, depvar(country_dummy) en(log_ancestry_2010) ex(dist distance_lat) iv(immi_nc_d_X_o_ndv_* pcFour*) id1(region_country_code) id2(county_continent_code) cluster(country_code1990) 
estimates store TableThreeA_3

// Panel A Column Four
qui ivreg2hdfe, depvar(country_dummy) en(log_ancestry_2010) ex(dist dist2 dist3 distance_lat distance_lat2 distance_lat3) iv(immi_nc_d_X_o_ndv_* pcFour*) id1(region_country_code) id2(county_continent_code) cluster(country_code1990) 
estimates store TableThreeA_4

// Panel A Column Five
qui ivreg2hdfe, depvar(country_dummy) en(log_ancestry_2010) ex(dist dist2 dist3 distance_lat distance_lat2 distance_lat3 dist_Cosine) iv(immi_nc_d_X_o_ndv_* pcFour*) id1(region_country_code) id2(county_continent_code) cluster(country_code1990) 
estimates store TableThreeA_5

// Panel A Column Six
rename XXimmi_nc_d_X_o_ndv_10 immi_nc_d_X_o_ndv_10
qui ivreg2hdfe, depvar(country_dummy) en(log_ancestry_2010) ex(dist distance_lat) iv(immi_nc_d_X_o_ndv_* pcFour*) id1(region_country_code) id2(county_continent_code) cluster(country_code1990) 
estimates store TableThreeA_6
rename immi_nc_d_X_o_ndv_10 XXimmi_nc_d_X_o_ndv_10

// Panel A Column Seven
qui ivreg2hdfe, depvar(country_dummy) en(log_ancestry_2010) ex(dist distance_lat) iv(immi_nc_d_X_o_ndv_* pcFour*) id1(state_country_code) id2(county_continent_code) cluster(country_code1990) 
estimates store TableThreeA_7
*/

// PROPOSED DIFFERENCES:
* Switch to 1990 from 2010
* keep data only with PR
* do 2 different regressions that answer different questions:
	* 1. can the exact regressions explain the number of PR linked establishments? (iv for ancestry using above regression)
	* 2. Use the ancestry IV as an exogenous variable that only affects S936 exposure but not growth in future years.
	
ssc install ivreg2hdfe, replace	

// (1) Importing the data from Hassan
use "$additional/Hassan Replication Files/Input/replication.dta", clear

// Get rid of 1990-2010 immigration
rename immi_nc_d_X_o_ndv_10 XXimmi_nc_d_X_o_ndv_10
rename immi_nc_d_X_o_ndv_9 XXimmi_nc_d_X_o_ndv_9
rename immi_nc_d_X_o_ndv_8 XXimmi_nc_d_X_o_ndv_8

// Keeping only PR
keep if country_name == "Puerto Rico"

// using pid 
gen pid = 1000 * state_code_1990 + county_code_1990

tempfile hassan
save "`hassan'", replace

// (2) Importing data from NETS and QCEW to merge with 
use "$output_NETS/pr_link_est_countyXindustry_d.dta", clear 
gen pr_est = pr_link*total if industry_cd < 13
bys fips: egen tot_est = total(total) 
bys fips: egen tot_pr = total(pr_est)

gen pr_l = tot_pr/tot_est 
keep pr_l fips tot_pr tot_est
rename  fips pid 
rename pr_l pr_link_ind
duplicates drop 

merge 1:1 pid using "`hassan'"
keep if _merge == 3
drop _merge 

// (3) saving data for analysis in a separate file:
save "$data/county_immigration.dta", replace



