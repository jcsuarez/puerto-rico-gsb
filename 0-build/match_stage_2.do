* Get data from PR
use "$output_NETS_control/HQ_to_match.dta", clear
gen PR=1 
rename est num_est95
drop q_emp q_est
tempfile PRdata
save `PRdata'

* Get control Data 
use "$output_NETS_control/hq_est_co_firm_master.dta", clear
drop if emp95 < 6
drop if num_est95 < 3

*sample 1000, count by(firm_group)
keep if fipsstate95 != . 
drop if fipsstate95 > 56
rename fipsstate95 hqfips
 
gen firm_group2 = floor(firm_group/10000)
gen census_div = floor(firm_group2/1000)
gen naic3 = firm_group2-1000*census_div
drop firm_group2
gen PR = 0 

keep hqduns95 num_est95 emp95 census_div naic3 firm_group PR hqfips

append using `PRdata'

drop if hqfips > 56 

gen id = _n 

* Drop firms in control that are in treatment 
bys hqduns95 : gen count = _N 
drop if count ==2 & PR ==0 
drop count 

capture: drop omitn 
capture: drop co*
teffects nnmatch (hqduns95 emp95 num_est95 hqfips naic3 ) (PR), ///
	osample(omitn) metric(euclidean) generate(co) ematch(census_div)
	
preserve
keep if PR == 0 
drop omit co*
foreach var of varlist * { 
	rename `var' co_`var'
} 
rename co_id co1
tempfile cont
save `cont'
restore 
	
merge m:1 co1 using `cont'	
keep if _merge ==3 
drop _merge 

mean emp95 co_emp95	if emp95 <10000
	
teffects nnmatch (hqduns95 emp95 num_est95 hqfips  ) (PR) if ~omit, ///
	 metric(euclidean) generate(co) ematch(firm_group)	tle(1) atet
	
drop co2-co10	
replace co1 = _n if PR==0
bys co1: gen count =_N

mean emp95 num_est95 naic3 hqfips if count >1 , over(PR)

gen census_div = 1 if inlist(hqfips,9,23,25,33,44,50)
replace census_div = 2 if inlist(hqfips,34,36,42)
replace census_div = 3 if inlist(hqfips,18,17,26,39,55)
replace census_div = 4 if inlist(hqfips,19,20,27,29,31,38,46)
replace census_div = 5 if inlist(hqfips,10,11,12,13,24,37,45,51,54)
replace census_div = 6 if inlist(hqfips,1,21,28,47)
replace census_div = 7 if inlist(hqfips,5,22,40,48)
replace census_div = 8 if inlist(hqfips,4,8,16,35,30,49,32,56)
replace census_div = 9 if inlist(hqfips,2,6,15,41,53)



