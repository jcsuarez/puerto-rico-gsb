clear
capture log close
set more off
capture close
***************************************************************
//Filename: nets_pr_extract.do
//Author: mtp
//Date: 14 May 2016
//Task: Extract firms with an establishment in PR
***************************************************************
* NOTE: _panel_v2.do creates panel of all firms that ever existed and had presence in PR

***Change working directory:
cd "/Users/mattpanhans/Dropbox (JC's Data Emporium)/PR_nets/extract"
global filepath "/Volumes/Seagate Backup Plus/JC RA Work/NETS_2013_release"
*import delimited "$filepath/NETS2012Basic.txt", clear rowrange(1:10000) colrange(1) varnames(1) delimiter(",")
*import delimited "$filepath/NETS2012Company.txt", clear rowrange(1:10000) colrange(1) varnames(1) delimiter(",")



/*
- start with file of all establishments as obs: locations by year, all hqduns by year.
- in the end I want all hqduns by year that have an establishment in PR

1. for each est, keep if it is in the PR in any year
2. keep hqduns for those years that the establishment is in the PR
3. flag those that change over time. Otherwise just keep hqduns
4. this leaves a list of hqduns that have a PR presence, as well as flags of those that change over time, but are associated with est in PR at some point

5. assume we fix the changing hqduns issue. only left with hqduns that have PR presence
6. merge to keep only establishments associated with those firms
7. drop those that are only ever in the PR
8. collapse to firm level. keep sales, num_est, and emp.



*/
*Q: duns 1001626. HQ duns changes in 1991, and then back to original number in 1997. How to treat them?



////////////////////////////////////////////////////////////////
/////// 2. PR Panel of Firms
////////////////////////////////////////////////////////////////
**** 1 - Create list of all hqduns that have an establishment in PR
forvalues v = 1/6 {
local start = 1 + 10000000*(`v'-1)
local end = 10000000*`v'
di `start'
di `end'

import delimited "$filepath/NETS2012Basic.txt", clear rowrange(`start':`end') colrange(1:126) varnames(1) delimiter(",")
keep dunsnumber fips* hqduns*

egen min_fips = rowmin(fips*)
egen max_fips = rowmax(fips*)
gen mover = 0
replace mover = 1 if min_fips != max_fips

gen fipsstate = floor(fipscounty/1000)

gen pr_presence = 0
replace pr_presence = 1 if fipsstate == 72
drop if pr_presence == 0 & mover == 0
drop if pr_presence == 0 & mover == 1 & max_fips < 72000
drop min_fips max_fips

egen long min_hqduns = rowmin(hqduns*)
egen long max_hqduns = rowmax(hqduns*)
gen change_hq = 0
replace change_hq = 1 if min_hqduns != max_hqduns

gen fips_hq = fipsstate if dunsnumber == min_hqduns

duplicates drop

save pr_extract_panel_hq`v'.dta, replace

}
	
** Append HQ files
use pr_extract_panel_hq1.dta, clear

forvalues v = 2/6 {
	append using pr_extract_panel_hq`v'.dta
}

* for now, drop obs that change hq or move
drop if mover == 1
drop if change_hq == 1

*keep min_hqduns fips_hq mover change_hq
keep min_hqduns fips_hq
rename min_hqduns hqduns
format hqduns %11.0f

bysort hqduns: egen temp = max(fips_hq)
replace fips_hq = temp if fips_hq == .
drop temp
duplicates drop


save pr_extract_hq_panel_master.dta, replace	// List of HQ's of firms with PR presence in any year








**** 2- 
// Want: Firms that existed before 1996 and that had an establishment in PR but that were based in US  (i.e., drop only PR firms)

** Import NETS data (~52.5 million observations total)
use pr_extract_hq_panel_master.dta, clear
*drop if hqduns == 72
tempfile hq_panel_master
save `hq_panel_master'

forvalues v = 1/6 {
local start = 1 + 10000000*(`v'-1)
local end = 10000000*`v'
di `start'
di `end'

import delimited "$filepath/NETS2012Basic.txt", clear rowrange(`start':`end') colrange(1:185) varnames(1) delimiter(",")

keep dunsnumber cbsa citycode emp* sales* estcat* fips* firstyear foreignown hqduns* industry industrygroup lastyear naics*
egen long hqduns = rowmin(hqduns*)
drop hqduns9* hqduns0* hqduns1*

merge m:1 hqduns using `hq_panel_master'
keep if _merge == 3		// keeps all establishments linked to a firm based in US (of firms with some PR presence)

drop _merge

save pr_extract_panel_v2_`v'.dta, replace

}

** Append Est files
use pr_extract_panel_v2_1.dta, clear

forvalues v = 2/6 {
	append using pr_extract_panel_v2_`v'.dta
	
}



** Reshape. This is currently est. level dataset
drop estcat* empc* cbsa citycode
rename fips_hq hqfips

egen min_fips = rowmin(fips*)
egen max_fips = rowmax(fips*)
gen mover = 0
replace mover = 1 if min_fips != max_fips

gen fipsstate = floor(fipscounty/1000)
replace hqfips = dunsnumber if dunsnumber == hqduns

keep dunsnumber emp* sales* fipsstate hqduns mover hqfips
order hqduns dunsnumber fipsstate
sort hqduns dunsnumber fipsstate

gen inPR = 0
replace inPR = 1 if fipsstate == 72


/*
foreach y in 90 91 92 93 94 95 96 97 98 99 00 01 02 03 04 05 06 07 08 09 10 11 12 {
	gen fipsstate`y' = floor(fips`y'/1000)
	gen pr_presence`y' = 1 if fipsstate`y' == 72
}
*/


foreach y in 90 91 92 93 94 95 96 97 98 99 00 01 02 03 04 05 06 07 08 09 10 11 12 {
gen num_est`y' = 1 if emp`y' != .
}

collapse (sum) emp* (sum) sales* (sum) num_est*, by(hqduns inPR)

foreach v in 00 01 02 03 04 05 06 07 08 09 10 11 12 {
	rename emp`v' emp20`v'
	rename sales`v' sales20`v'
	rename num_est`v' num_est20`v'
}
forval v = 90/99 {
	rename emp`v' emp19`v'
	rename sales`v' sales19`v'
	rename num_est`v' num_est19`v'
}

reshape long emp sales num_est, i(hqduns inPR) j(year)

reshape wide emp sales num_est, i(hqduns year) j(inPR)

rename emp0 emp_US
rename sales0 sales_US
rename num_est0 num_est_US
rename emp1 emp_PR
rename sales1 sales_PR
rename num_est1 num_est_PR

gen emp_total = emp_US + emp_PR
gen sales_total = sales_US + sales_PR
gen num_est_total = num_est_US + num_est_PR

* Drop firms only ever in PR
by hqduns: egen US_presence = max(emp_US)
drop if US_presence == .
drop US_presence

save ../output/pr_extract_panel_v2.dta, replace


