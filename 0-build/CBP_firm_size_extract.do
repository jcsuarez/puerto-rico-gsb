use "$additional/CBP_from_WBCT/countyCBP.dta", clear 
keep if inrange(year,1990,2011)
keep fips* n* year
gen pid = fipstate*1000 + fipscty
drop fips* naics
drop n1000_*
egen n250p = rowtotal(n250_499 n500_999 n1000  )
keep year pid n1_4 n5_9 n10_19 n20_49 n50_99 n100_249 n250p
egen tot_e = rowtotal( n*)
foreach var of varlist n* { 
	sum `var'
	replace `var' = `var'/tot_e
	rename  `var' `var'_
} 

drop tot_e
duplicates drop 
compress 

reshape wide n1_4_ n5_9_ n10_19_ n20_49_ n50_99_ n100_249_ n250p_ , i(pid) j(year) 
foreach xx in n1_4 n5_9 n10_19 n20_49 n50_99 n100_249 n250p { 
	gen `xx'_2012 = `xx'_2011
} 
reshape long n1_4_ n5_9_ n10_19_ n20_49_ n50_99_ n100_249_ n250p_ , i(pid) j(year) 
foreach xx in n1_4 n5_9 n10_19 n20_49 n50_99 n100_249 n250p { 
	rename `xx'_ `xx'
} 

save "$output/CBP_firm_size_dist" , replace 




/*
use "$additional/CBP_from_WBCT/countyCBP.dta", clear 
keep if year == 1995
keep fips* n*
gen pid = fipstate*1000 + fipscty
drop fips* naics
drop n1000_*
egen n250p = rowtotal(n250_499 n500_999 n1000  )
keep pid n1_4 n5_9 n10_19 n20_49 n50_99 n100_249 n250p
egen tot_e = rowtotal( n*)
foreach var of varlist n* { 
	sum `var'
	replace `var' = `var'/tot_e
} 
replace n1_4 = n1_4+n5_9+n10_19
drop n5_9 n10_19
replace n20_49 = n20_49+n50_99+n100_249
drop n50_99 n100_249

drop tot_e
duplicates drop 
compress 
save "$output/CBP_firm_size_dist" , replace 


