clear
capture log close
set more off
capture close
***************************************************************
//Author: dgg
//Date: 20180418
//Task: merge Dyreng data with compustat data
***************************************************************
snapshot erase _all

/*
The basic goals of this exercise:
-- first question: to what extent are PR firms in other tax havens in 1997 (first year)? Are non-PR firms in other tax havens? 
--second question: are PR firms more likely to open new tax havens after repeal of 936? 
-- third question: if so, where do these firms go? 

*/

use "$WRDS/ETR_compustat.dta", replace
replace PR = 0 if PR == .
gen nonPR = 1-PR

destring cik, replace
drop if cik == .
drop if gvkey == 15403

tempfile dyreng_comp
save "`dyreng_comp'", replace

*********************************
* dyreng data
*********************************
use  "$additional/Dyreng_Tax_Havens/country31may2015.dta", replace
rename ciknumber cik
gen year = year(datadate)

gen pr_country = (country == "Puerto Rico") * countrycount
gen ir_country = (country == "Ireland") * countrycount
gen ba_country = (country == "Barbados") * countrycount
gen hk_country = (country == "Hong Kong") * countrycount
gen sp_country = (country == "Singapore") * countrycount
gen ci_country = (country == "Cayman Islands") * countrycount
gen sw_country = (country == "Switzerland") * countrycount

*** generating set of new countries of operation and new "taxhaven" mentions
bys cik country (year): gen new_country = _n
replace new_country = 0 if new_country > 1

gen largehaven = 0
replace largehaven = (taxhaven == 1) if inlist(country,"Ireland","Barbados","Hong Kong","Singapore","Cayman Islands","Switzerland")
bys cik country (year): gen new_haven = _n
replace new_haven = 0 if new_haven > 1 | taxhaven == 0
bys cik country (year): gen new_lhaven = _n
replace new_haven = 0 if new_lhaven > 1 | largehaven == 0

collapse (mean) totalcount (sum) *_country taxhaven new_haven new_lhaven (mean) ncountries, by(year cik)

merge 1:1 year cik using  "`dyreng_comp'"

*** looking at firms in PR as of 1995
replace emp_PR = 0 if emp_PR == .

drop if _merge == 1

foreach Q of var pr_country ir_country ba_country hk_country sp_country ci_country sw_country {
replace `Q' = 0 if `Q' == .
}
gen pr_mention = pr_country > 0
gen ir_mention = ir_country > 0
gen ba_mention = ba_country > 0
gen hk_mention = hk_country > 0
gen sp_mention = sp_country > 0
gen ci_mention = ci_country > 0
gen sw_mention = sw_country > 0

replace totalcount = 0 if totalcount == .
replace ncountries = 0 if ncountries == .
replace taxhaven = 0 if taxhaven == .
gen th_mention = taxhaven > 0 

gen any_new_haven = (new_haven > 0) * (new_haven != .)
gen any_new_lhaven = (new_lhaven > 0) * (new_lhaven != .)
gen any_new_country = (new_country > 0) * (new_country != .)


// outcomes of interest: 
// -- th_mention over time
// -- pr_mention over time
// -- ncountries over time (seeking more tax havens else)

bys cik: egen min_year = min(year)
bys cik: egen max_year = max(year)
* Run only on data until 2012 
keep if year < 2013
*keep if year > 1993
* Run only on firms wth obs before and after 1996 
keep if min_year < 1996 & max_year > 1996

* naics controls
tostring naics, gen(naicsh)

gen naics3 = substr(naicsh,1,3)
encode naics3, generate(n3)
gen naics2 = substr(naicsh,1,2)
encode naics2, generate(n2)

capture: drop _merge
save "$additional/Dyreng_Tax_Havens/dyreng_data_merged", replace

// getting to data needed to PNP version:

keep taxhaven *_mention n3 year PR ncountries year


** variable for mentioning any haven
gen anyhaven = taxhaven > 0
replace anyhaven = 0 if taxhaven == .
drop taxhaven pr_mention th_mention

drop if year < 2003 & year > 1995
drop if year > 2008 | year < 1993
egen ny_FE = group(n3 year)
save "C:/Users/Dan's Work PC/Dropbox (JC's Data Emporium)/Puerto Rico/AEAPP_Draft/AEAPP_Submission/Data/table3_data.dta", replace
*/

/*
xi i.year|PR , noomit
drop _IyeaXPR_1995

est clear

eststo reg1: reghdfe new_haven _IyeaX* PR if year > 1992 ,  a(i.n3 i.year) cl(n3)
eststo reg2: reghdfe new_haven _IyeaX*  if year > 1992 ,  a(i.n3 i.year i.cik) cl(n3)

eststo reg3: reghdfe any_new_haven _IyeaX* PR  if year > 1992 ,  a(i.n3 i.year) cl(n3)
eststo reg4: reghdfe any_new_haven _IyeaX*  if year > 1992 ,  a(i.n3 i.year i.cik) cl(n3)

eststo reg5: reghdfe new_country _IyeaX* PR  if year > 1992 ,  a(i.n3 i.year) cl(n3)
eststo reg6: reghdfe new_country _IyeaX*  if year > 1992,  a(i.n3 i.year i.cik) cl(n3)

eststo reg7: reghdfe any_new_country _IyeaX* PR  if year > 1992 ,  a(i.n3 i.year) cl(n3)
eststo reg8: reghdfe any_new_country _IyeaX*  if year > 1992 ,  a(i.n3 i.year i.cik) cl(n3)

esttab reg? using "$output/Tables/dyreng_data_table_expansion.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons ///
prefoot(" & \multicolumn{2}{c}{New Tax Haven} & \multicolumn{2}{c}{Any New Tax Haven} & \multicolumn{2}{c}{New Countries} & \multicolumn{2}{c}{Any New Countries}  \\ \cmidrule(lr){2-3} \cmidrule(lr){4-5}\cmidrule(lr){6-7}\cmidrule(lr){8-9} ") ///
postfoot("\multicolumn{1}{l}{\textbf{Exposure to Section 936}}") nonum posthead("")

esttab reg?  using "$output/Tables/dyreng_data_table_expansion.tex",  preh("") ///
 b(3) se par mlab(none) coll(none) s() noobs nogap  /// 
coeflabel(_IyeaXPR_1990 "\hspace{1em}X 1990 " /// 
_IyeaXPR_1991 "\hspace{1em}X 1991 " /// 
_IyeaXPR_1992 "\hspace{1em}X 1992 " /// 
_IyeaXPR_1993 "\hspace{1em}X 1993 " /// 
_IyeaXPR_1994 "\hspace{1em}X 1994 " /// 
_IyeaXPR_1996 "\hline \hspace{1em}X 1996 " /// 
_IyeaXPR_1997 "\hspace{1em}X 1997 " /// 
_IyeaXPR_1998 "\hspace{1em}X 1998 " /// 
_IyeaXPR_1999 "\hspace{1em}X 1999 " /// 
_IyeaXPR_2000 "\hspace{1em}X 2000 " /// 
_IyeaXPR_2001 "\hspace{1em}X 2001 " /// 
_IyeaXPR_2002 "\hspace{1em}X 2002 " /// 
_IyeaXPR_2003 "\hspace{1em}X 2003 " /// 
_IyeaXPR_2004 "\hspace{1em}X 2004 " /// 
_IyeaXPR_2005 "\hspace{1em}X 2005 " /// 
_IyeaXPR_2006 "\hspace{1em}X 2006 " /// 
_IyeaXPR_2007 "\hspace{1em}X 2007 " /// 
_IyeaXPR_2008 "\hspace{1em}X 2008 " /// 
_IyeaXPR_2009 "\hspace{1em}X 2009 " /// 
_IyeaXPR_2010 "\hspace{1em}X 2010 " /// 
_IyeaXPR_2011 "\hspace{1em}X 2011 " /// 
_IyeaXPR_2012 "\hspace{1em}X 2012 " )  label /// 
 prefoot("\hline") ///
postfoot( ///
"Year Fixed Effects & Y & Y & Y & Y & Y & Y & Y & Y \\" ///
"Industry Fixed Effects & Y &  & Y &  & Y &  & Y &  \\" ///
"Firm Fixed Effects &  & Y &  & Y &  & Y &  & Y \\" ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex)


est clear

eststo reg1: reghdfe new_haven _IyeaX* PR totalcount ncountries if year > 1992 ,  a(i.n3 i.year) cl(n3)
eststo reg2: reghdfe new_haven _IyeaX*  totalcount ncountries  if year > 1992 ,  a(i.n3 i.year i.cik) cl(n3)

eststo reg3: reghdfe any_new_haven _IyeaX* PR  totalcount ncountries  if year > 1992 ,  a(i.n3 i.year) cl(n3)
eststo reg4: reghdfe any_new_haven _IyeaX*  totalcount ncountries  if year > 1992 ,  a(i.n3 i.year i.cik) cl(n3)

eststo reg5: reghdfe new_country _IyeaX* PR  totalcount ncountries  if year > 1992 ,  a(i.n3 i.year) cl(n3)
eststo reg6: reghdfe new_country _IyeaX*  totalcount ncountries  if year > 1992,  a(i.n3 i.year i.cik) cl(n3)

eststo reg7: reghdfe any_new_country _IyeaX* PR  totalcount ncountries  if year > 1992 ,  a(i.n3 i.year) cl(n3)
eststo reg8: reghdfe any_new_country _IyeaX*  totalcount ncountries  if year > 1992 ,  a(i.n3 i.year i.cik) cl(n3)

esttab reg? using "$output/Tables/dyreng_data_table_expansion.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons ///
prefoot(" & \multicolumn{2}{c}{New Tax Haven} & \multicolumn{2}{c}{Any New Tax Haven} & \multicolumn{2}{c}{New Countries} & \multicolumn{2}{c}{Any New Countries}  \\ \cmidrule(lr){2-3} \cmidrule(lr){4-5}\cmidrule(lr){6-7}\cmidrule(lr){8-9} ") ///
postfoot("\multicolumn{1}{l}{\textbf{Section 936}}") nonum posthead("")

esttab reg?  using "$output/Tables/dyreng_data_table_expansion.tex",  preh("") ///
 b(3) se par mlab(none) coll(none) s() noobs nogap  /// 
coeflabel(_IyeaXPR_1990 "\hspace{1em}X 1990 " /// 
_IyeaXPR_1991 "\hspace{1em}X 1991 " /// 
_IyeaXPR_1992 "\hspace{1em}X 1992 " /// 
_IyeaXPR_1993 "\hspace{1em}X 1993 " /// 
_IyeaXPR_1994 "\hspace{1em}X 1994 " /// 
_IyeaXPR_1996 "\hline \hspace{1em}X 1996 " /// 
_IyeaXPR_1997 "\hspace{1em}X 1997 " /// 
_IyeaXPR_1998 "\hspace{1em}X 1998 " /// 
_IyeaXPR_1999 "\hspace{1em}X 1999 " /// 
_IyeaXPR_2000 "\hspace{1em}X 2000 " /// 
_IyeaXPR_2001 "\hspace{1em}X 2001 " /// 
_IyeaXPR_2002 "\hspace{1em}X 2002 " /// 
_IyeaXPR_2003 "\hspace{1em}X 2003 " /// 
_IyeaXPR_2004 "\hspace{1em}X 2004 " /// 
_IyeaXPR_2005 "\hspace{1em}X 2005 " /// 
_IyeaXPR_2006 "\hspace{1em}X 2006 " /// 
_IyeaXPR_2007 "\hspace{1em}X 2007 " /// 
_IyeaXPR_2008 "\hspace{1em}X 2008 " /// 
_IyeaXPR_2009 "\hspace{1em}X 2009 " /// 
_IyeaXPR_2010 "\hspace{1em}X 2010 " /// 
_IyeaXPR_2011 "\hspace{1em}X 2011 " /// 
_IyeaXPR_2012 "\hspace{1em}X 2012 " )  label /// 
 prefoot("\hline") ///
postfoot( ///
"Year FE & Y & Y & Y & Y & Y & Y & Y & Y \\" ///
"Size Controls & Y & Y & Y & Y & Y & Y & Y & Y \\" ///
"Industry FE & Y &  & Y &  & Y &  & Y &  \\" ///
"Firm FE &  & Y &  & Y &  & Y &  & Y \\" ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex) 
