* This file creates several new versions of PR_LINK at the county (pid) level
* author: dan
* date: 2018-10-22

clear all
set more off
snapshot erase _all

* basic data
use "$output_NETS/pr_link_est_countyXindustry_d.dta", clear 
gen pr_emp_all = pr_link*total
gen pr_emp = pr_link*total if industry_cd < 13
gen pr_emp1 = pr_link*total if inlist(industry_cd,1) // food, beverages, tobacco
gen pr_emp2 = pr_link*total if inlist(industry_cd,2,3,7) // Clothing, apparel, leather
gen pr_emp3 = pr_link*total if inlist(industry_cd,4,6) // Chemicals and plastics (non pharma)
gen pr_emp4 = pr_link*total if inlist(industry_cd,5) // Pharma
gen pr_emp5 = pr_link*total if inlist(industry_cd,10,11) // Electronics and instruments
gen pr_emp6 = pr_link*total if inlist(industry_cd,8,9,12) // other manufacturing
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr_all = total(pr_emp_all) 
bys fips: egen tot_pr = total(pr_emp)
bys fips: egen tot_pr1 = total(pr_emp1)
bys fips: egen tot_pr2 = total(pr_emp2)
bys fips: egen tot_pr3 = total(pr_emp3)
bys fips: egen tot_pr4 = total(pr_emp4)
bys fips: egen tot_pr5 = total(pr_emp5)
bys fips: egen tot_pr6 = total(pr_emp6)

gen prl_all = tot_pr_all/tot_emp
gen prl_base = tot_pr/tot_emp

gen prl_food = tot_pr1/tot_emp
gen prl_food_inv = prl_all - tot_pr1/tot_emp

gen prl_cloth = tot_pr2/tot_emp
gen prl_cloth_inv = prl_all - tot_pr2/tot_emp

gen prl_chem = tot_pr3/tot_emp
gen prl_chem_inv = prl_all - tot_pr3/tot_emp

gen prl_pharm = tot_pr4/tot_emp
gen prl_pharm_inv = prl_all - tot_pr4/tot_emp

gen prl_elec = tot_pr5/tot_emp 
gen prl_elec_inv = prl_all - tot_pr5/tot_emp

gen prl_other = tot_pr6/tot_emp 
gen prl_other_inv = prl_all - tot_pr6/tot_emp
 
keep prl* fips
rename  fips pid

duplicates drop 

tempfile pr2 
save "`pr2'", replace

****************************************************
* making links to general pharma and control firms
****************************************************
* pharma 
rename pid fips
merge m:1 fips using "$data/QCEW/extract_qcew_pharm.dta"
rename fips pid
drop if _merge == 2
drop _merge 
drop pharm_annual_avg_estabs pharm_total_annual_wages 
rename pharm_annual_avg_emplvl pharma

save "`pr2'", replace

* fake shock exposure
use "$output_NETS_control/pr_link_est_countyXindustry_d_fake.dta", clear 
gen fake_emp_all = pr_link*total
gen fake_emp = pr_link*total if industry_cd < 13
gen fake_emp1 = pr_link*total if inlist(industry_cd,1) // food, beverages, tobacco
gen fake_emp2 = pr_link*total if inlist(industry_cd,2,3,7) // Clothing, apparel, leather
gen fake_emp3 = pr_link*total if inlist(industry_cd,4,6) // Chemicals and plastics (non pharma)
gen fake_emp4 = pr_link*total if inlist(industry_cd,5) // Pharma
gen fake_emp5 = pr_link*total if inlist(industry_cd,10,11) // Electronics and instruments
gen fake_emp6 = pr_link*total if inlist(industry_cd,8,9,12) // other manufacturing
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_fake_all = total(fake_emp_all) 
bys fips: egen tot_fake = total(fake_emp)
bys fips: egen tot_fake1 = total(fake_emp1)
bys fips: egen tot_fake2 = total(fake_emp2)
bys fips: egen tot_fake3 = total(fake_emp3)
bys fips: egen tot_fake4 = total(fake_emp4)
bys fips: egen tot_fake5 = total(fake_emp5)
bys fips: egen tot_fake6 = total(fake_emp6)

gen fakel_all = tot_fake_all/tot_emp
gen fakel_base = tot_fake/tot_emp

gen fakel_food = tot_fake1/tot_emp
gen fakel_food_inv = fakel_all - tot_fake1/tot_emp

gen fakel_cloth = tot_fake2/tot_emp
gen fakel_cloth_inv = fakel_all - tot_fake2/tot_emp

gen fakel_chem = tot_fake3/tot_emp
gen fakel_chem_inv = fakel_all - tot_fake3/tot_emp

gen fakel_pharm = tot_fake4/tot_emp
gen fakel_pharm_inv = fakel_all - tot_fake4/tot_emp

gen fakel_elec = tot_fake5/tot_emp 
gen fakel_elec_inv = fakel_all - tot_fake5/tot_emp

gen fakel_other = tot_fake6/tot_emp 
gen fakel_other_inv = fakel_all - tot_fake6/tot_emp
 
keep fakel* fips
rename  fips pid

duplicates drop 

merge 1:1 pid using "`pr2'"
drop _merge

** saving file for future use
save "$additional/county_shocks", replace

