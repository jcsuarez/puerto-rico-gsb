
*** Start
cd "$analysis"
clear
set more off 

////


clear
set more off
snapshot erase _all
/// Generates Fake Shock using firm demographics to those similar to PR exposed firms 

/// Get control data 
* Get control Data 
use "$output_NETS_control/hq_est_co_firm_master.dta", clear
drop if emp95 < 6
drop if num_est95 < 3

keep if fipsstate95 != . 
drop if fipsstate95 > 56
rename fipsstate95 hqfips

set seed 12345
sample 10, count by(firm_group)

keep hqduns95 
duplicates drop 


// Drop Firms accidentally in both 
*merge 1:1 hqduns95 using "$output_NETS/pr_extract_hq_master.dta"
*drop if _merge == 3 & pr_b ==1 
*keep if _merge ==1

save "$output_NETS_control/pr_extract_hq_master_fake.dta", replace

*10000000
**** 2 - Keep all establishments that are part of firm on hqduns list
forvalues v = 1/6 {
local start = 1 + 10000000*(`v'-1)
local end = 10000000*`v'
di `start'
di `end'

** Import NETS data (~52.5 million observations total)
import delimited "$netspath/NETS2012Basic.txt", clear rowrange(`start':`end') colrange(1:160) varnames(1) delimiter(",")

keep dunsnumber cbsa citycode emp95 estcat95 fips95 firstyear foreignown hqduns95 industry industrygroup lastyear naics95
drop if hqduns95 == .

merge m:1 hqduns95 using "$output_NETS_control/pr_extract_hq_master_fake.dta" //variable hqduns95 does not uniquely identify observations in the using data
gen pr_link = 0
replace pr_link = 1 if _merge == 3
drop _merge

save "$output_NETS_control/pr_extract_est`v'.dta", replace

}

** Append Est files
use "$output_NETS_control/pr_extract_est1.dta", clear 									// contains pr_based == 1 

forvalues v = 2/6 {
	append using "$output_NETS_control/pr_extract_est`v'.dta"
	
}

gen fipsstate95 = floor(fips95/1000)

drop if fips95 == .
save "$output_NETS_control/pr_extract_est_fake.dta", replace	// 229,000 establishments with links to PR in '96



**** 3- Output descriptives and save dataset
use "$output_NETS_control/pr_extract_est_fake.dta", clear

gen naic4 = floor(naics95/100)
replace naic4 = floor(naics95/10) if naic4 <1111
replace naic4 = floor(naics95) if naic4 <1111 
gen naic3 = floor(naics95/1000)
replace naic3 = floor(naics95/100) if naic3 <111
replace naic3 = floor(naics95/10) if naic3 <111
replace naic3 = floor(naics95) if naic3 <111
gen naic2 = floor(naics95/10000)
replace naic2 = floor(naics95/1000) if naic2 <11
replace naic2 = floor(naics95/100) if naic2 <11
replace naic2 = floor(naics95/10) if naic2 <11
replace naic2 = floor(naics95) if naic2 <11

drop if naic2 == 92 | naic3 == 813
* dropping USPS and federal government
drop if hqduns == 3261245 
* this is navy, drop
drop if hqduns == 161906193

snapshot save
keep naic4
duplicates drop
save "$output_NETS_control/naic4s", replace

snapshot restore 1

********************
*getting rid of governmental and social non-profit orgs
********************
drop if naic2 == 92 | naic3 == 813
* dropping USPS and federal government
drop if hqduns95 == 3261245 
* this is navy, drop in future  hqduns95 == 161906193

gen industry_cd = .
replace industry_cd = 1 if naic3 == 311
replace industry_cd = 2 if naic3 == 314
replace industry_cd = 3 if naic3 == 315
replace industry_cd = 4 if naic3 == 325
replace industry_cd = 5 if naic4 == 3254
replace industry_cd = 6 if naic3 == 326
replace industry_cd = 7 if naic3 == 316
replace industry_cd = 8 if naic3 == 332
replace industry_cd = 9 if naic3 == 333
replace industry_cd = 10 if naic3 == 335
*replace industry_cd = 11 if naic3 == 
replace industry_cd = 12 if industry_cd == . & (naic2 == 31 | naic2 == 32 | naic2 == 33)
replace industry_cd = 13 if naic2 == 52 | naic2 == 53
replace industry_cd = 14 if naic2 == 54 | naic2 == 55 | naic2 == 61 | naic2 == 62 | naic2 == 71 | naic2 == 72 | naic2 == 81
replace industry_cd = 15 if naic2 == 42 | naic2 == 44 | naic2 == 45
replace industry_cd = 16 if industry_cd == .


label define l_naic3 1 "Food Mfg" 2 "Textile mill products" 3 "Apparel" 4 "Chemicals" 5 "Pharmaceuticals" ///
 6 "Rubber and Plastic" 7 "Leather" 8 "Fabricated metal" 9 "Machinery" 10 "Electrical equip" ///
 11 "Instruments" 12 "Other mfg" 13 "Finance, insurance, real estate" 14 "Services" ///
 15 "Wholesale and retail" 16 "Other non-mfg", replace

label values industry_cd l_naic3
 
gen total = 1

preserve
collapse (mean) pr_link (sum) total, by(fips95)
save "$output_NETS_control/pr_link_est_county_d_fake.dta", replace
restore

preserve
collapse (mean) pr_link (sum) total, by(fips95 industry_cd)
tsset fips95 industry_cd
tsfill, full
replace pr_link = 0 if pr_link == .
replace total = 0 if total == .
save "$output_NETS_control/pr_link_est_countyXindustry_d_fake.dta", replace
restore 

