clear
set more off

// This file assembles annual estimates for working age population at the pid level
// goal is two have a single dataset with 3 vars: pid, year, and working age pop 15-64


* v1 is year, v2 is pid, v3 is age group, v4 is sex group, v5 is ethnic origin, v6 is pop
forval i = 1/10 {
local j = `i' + 1989

import delimited "$additional/county population estimates/stch-icen`j'.txt", delimiter(" ", collapse) clear
tempfile file`i'
save "`file`i''", replace
}

use "`file1'", clear
forval i = 2/10 {
append using "`file`i''"
}

gen year = 1900 + v1
rename v6 working_pop
rename v2 pid

/*
The key for Age group code is as follows:
		0 = <1 year
		1 = 1-4 years
		2 = 5-9 years
		3 = 10-14 years
		4 = 15-19 years
		5 = 20-24 years
		6 = 25-29 years
		7 = 30-34 years
		8 = 35-39 years
		9 = 40-44 years
		10 = 45-49 years
		11 = 50-54 years
		12 = 55-59 years
		13 = 60-64 years
		14 = 65-69 years
		15 = 70-74 years
		16 = 75-79 years
		17 = 80-84 years
		18 = 85 years and over
*/

keep if v3 > 3  // keeping over 15
keep if v3 < 14 // keeping under 65

collapse (sum) working_pop, by(pid year)

save "`file1'", replace

*** 2000-2010

import delimited "$additional/county population estimates/co-est00int-agesex-5yr.csv", delimiter(comma, collapse) clear

gen pid = 1000* state + county
keep if agegrp > 3  // keeping over 15
keep if agegrp < 14 // keeping under 65

// sex = 0 for both, 1 for males, 2 for females: doublecounts without dropping
keep if sex == 0

collapse (sum) popestimate*, by(pid)

reshape long popestimate, i(pid) j(year)

rename popestimate working_pop

tempfile file2
save "`file2'", replace

*** 2011-2012

import delimited "$additional/county population estimates/cc-est2016-alldata.csv", delimiter(comma, collapse) clear

keep if year == 4 | year == 5 // 2011 and 2012 estimates 
replace year = year + 2007

gen pid = 1000* state + county
keep if agegrp > 3  // keeping over 15
keep if agegrp < 14 // keeping under 65

rename tot_pop working_pop

collapse (sum) working_pop, by(pid year)

append using "`file1'"
append using "`file2'"

sort pid year 

save "$additional/county population estimates/county_pop_ests.dta", replace
