//// Get PR firm data 

clear all
set more off
*ssc install dataout
snapshot erase _all


// Second Define Command for ES 
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(solid)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Estimate") label(2 "`level'% CI")) ///
	ytitle("`yti'") ///
	note("`note'") ylab("`ylab'")
graph export "`outname'.pdf", replace	

end 



*** get list of hqduns that actually work by dropping gov't etc... 
use "$output_NETS/pr_extract_est.dta", clear

gen naic4 = floor(naics95/100)
gen naic3 = floor(naics95/1000)
gen naic2 = floor(naics95/10000)

********************
*getting rid of governmental and social non-profit orgs
********************
drop if naic2 == 92 | naic3 == 813
* dropping USPS and the federal government
drop if hqduns95 == 3261245 | hqduns95 == 161906193

* dropping firms with HQ in PR
drop if pr_based == 1 

* dropping county industries with no link
drop if pr_link == 0
bysort hqduns: egen temp_max = max(emp95)
gen temp_keep = (emp95 == temp_max)

keep if temp_keep == 1

* making a list of hqduns to use from the panel data
keep hqduns naic3
bysort hqduns: egen m_naic3 = mode(naic3), minmode
drop naic3
duplicates drop 
tempfile tokeep 
save "`tokeep'"

** old code 
use "$output_NETS/pr_extract_panel.dta", clear 

** Keep only firms at start of the sample 
gen temp = emp_PR if year == 1990
bysort hqd: egen base_emp_PR = min(temp)
drop temp 
keep if base > 0  // This line drops 4,991 observations

** MErge in to keep 
merge m:1 hqduns95 using "`tokeep'"
keep if _merge == 3 // This drops 23 observations from the master, 214 from using

egen firm = group(hqd) 
sum firm, d
snapshot save

keep hqd year emp_US num_est_US m_naic3
gen PR = 1 

tempfile PRdata
save "`PRdata'" 

/// Get control data 
* Get control Data 
use "$output_NETS_control/hq_est_co_firm_master.dta", clear
drop if emp95 < 6
drop if num_est95 < 3

*sample 1000, count by(firm_group)
keep if fipsstate95 != . 
drop if fipsstate95 > 56
rename fipsstate95 hqfips
 
gen firm_group2 = floor(firm_group/10000)
gen census_div = floor(firm_group2/1000)
gen naic3 = firm_group2-1000*census_div
drop firm_group2
gen PR = 0 

rename naic3 m_naic3
drop hqfips census_div  PR firm_group 

foreach y in 90 91 92 93 94 95 96 97 98 99 00 01 02 03 04 05 06 07 08 09 10 11 12 {
	rename emp`y' emp_`y'
	rename num_est`y' num_`y'
}

reshape long emp_ num_ , i(hqduns m_nai) j(year) string
 
destring year , replace 
replace year = year+1900 if year > 50 
replace year = year+2000 if year < 50 
sort year
rename emp_ emp_US
rename num_ num_est_US
gen PR = 0 

append using "`PRdata'"

************************************************
* major sectors and industries (from SOI, matching compustat regressions)
************************************************

tostring m_naic3, gen(n3)
gen naic2 = substr(n3,1,2)
gen major_sec = inlist(naic2,"22","31","32","33","48","49")
gen major_ind = ~inlist(n3,"324","323","337","322","327","336","331","321") * major_sec
drop naic2 n3

// emp growth 
gen temp = emp_US if year == 1995
bysort hqduns95 : egen base_emp = max(temp)
drop temp

gen emp_growth = (emp_US - base_emp)/base_emp
replace emp_growth = 0 if emp_growth == . 

// estab growth 
gen temp = num_est_US if year == 1995
bysort hqduns95 : egen base_est = max(temp)
drop temp

gen est_growth = (num_est_US - base_est)/base_est
replace est_growth = 0 if est_growth == . 


drop if base_emp ==0 
sum base_emp if year == 1995 & PR , d
gen wgt=base_emp/( r(mean)) if PR 

sum base_emp if year == 1995 & PR==0 , d
replace wgt=base_emp/( r(mean)) if PR==0 

winsor wgt, p(.01) g(w_wgt) 

** Make balanced panel by dropping firms that drop out
bys hqduns95 : egen min_emp = min(emp_US) 
drop if min_emp == 0 

set matsize 4000
* DFL 
gen naic2 = floor(m_nai/10)
gen pharma = (m_naic3 == 325)
capture: drop q_wgt
xtile q_wgt = base_emp if year == 1995, n(20) 
logit PR i.q_wgt#i.naic2

capture: drop phat min_phat w w_w w_wgt
predict phat, pr 
bys hqduns: egen min_phat = min(phat) 
* ATE
* gen w = wgt*(PR/min_phat+(1-PR)/(1-min_phat))
* ATOT
gen w = w_wgt*(PR+(1-PR)*min_phat/(1-min_phat))

xi i.year|PR, noomit 
drop _IyeaXP*1995
 /* 
gen chemicals = (m_naic3==325)  
gen elect = (inlist(m_naic3,333,335))
gen food = (inlist(m_naic3,311,312))  

gen het_index = chemi +2*elect +3*food
  */
est clear


/**** Dan: Make 4 tables: 
1- Event studies
2- Reg on post years
3- Reg on post years w/ indXyr FEs
4- DID: Only keep PR_post2 variable 
5- Make a beautiful mess of event studies (use your judgement) 
*/

**** Event Studies 
*Establishments
snapshot save
est clear
eststo est_0:  reghdfe est_growth _IyeaXP*  ///
   [aw=wgt] , a(i.year)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"    

ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Est{sub:it}-Est{sub:i1995})/Est{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
outname("$output/Graphs/NETS_firm_est") ylab("-.08(.02).04")  

eststo est_1:  reghdfe est_growth _IyeaXP*  ///
   [aw=wgt] , a(i.year#i.naic2)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"    

eststo est_2:  reghdfe est_growth _IyeaXP*  ///
  if major_sec  [aw=wgt] , a(i.year#i.naic2)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"      

eststo est_3:  reghdfe est_growth _IyeaXP* ///
  if major_ind [aw=wgt] , a(i.year#i.naic2)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"     

eststo est_4:  reghdfe est_growth _IyeaXP* ///
  if major_ind [aw=w] , a(i.year#i.naic2)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"       

******** event study table
esttab est_? using "$output/Tables/NETS_firm_est_ES_20190618.tex", drop(*) stats() ///
b(3) se par label ///
noobs nogap nomtitle tex replace  nocons postfoot("\multicolumn{5}{l}{\textbf{Exposure to Section 936}} \\") prefoot("") posthead("")

esttab est_? using "$output/Tables/NETS_firm_est_ES_20190618.tex", keep(_IyeaXPR_*) ///
 b(3) se par mlab(none) coll(none) s() noobs nonum nogap scalars("N Observations"  )  sfmt(0) /// 
varlabel(_IyeaXPR_1990 "\hspace{1em}X 1990 " /// 
_IyeaXPR_1991 "\hspace{1em}X 1991 " /// 
_IyeaXPR_1992 "\hspace{1em}X 1992 " /// 
_IyeaXPR_1993 "\hspace{1em}X 1993 " /// 
_IyeaXPR_1994 "\hspace{1em}X 1994 " /// 
_IyeaXPR_1996 "\hline \hspace{1em}X 1996 " /// 
_IyeaXPR_1997 "\hspace{1em}X 1997 " /// 
_IyeaXPR_1998 "\hspace{1em}X 1998 " /// 
_IyeaXPR_1999 "\hspace{1em}X 1999 " /// 
_IyeaXPR_2000 "\hspace{1em}X 2000 " /// 
_IyeaXPR_2001 "\hspace{1em}X 2001 " /// 
_IyeaXPR_2002 "\hspace{1em}X 2002 " /// 
_IyeaXPR_2003 "\hspace{1em}X 2003 " /// 
_IyeaXPR_2004 "\hspace{1em}X 2004 " /// 
_IyeaXPR_2005 "\hspace{1em}X 2005 " /// 
_IyeaXPR_2006 "\hspace{1em}X 2006 " /// 
_IyeaXPR_2007 "\hspace{1em}X 2007 " /// 
_IyeaXPR_2008 "\hspace{1em}X 2008 " /// 
_IyeaXPR_2009 "\hspace{1em}X 2009 " /// 
_IyeaXPR_2010 "\hspace{1em}X 2010 " /// 
_IyeaXPR_2011 "\hspace{1em}X 2011 " /// 
_IyeaXPR_2012 "\hspace{1em}X 2012 ")  posth("\hline") ///
preh("")  postfoot( ///
"\hline Year Fixed Effects    & Y & Y & Y & Y & Y \\ " ///
"NAICS-by-Year Fixed Effects    & & Y & Y & Y & Y \\ " ///
"S936 Exposed Sector     & & & Y & Y & Y  \\ "  ///
"S936 Exposed Industry & & &   & Y  & Y  \\ " /// 
"DFL Weights & & & & &  Y   \\  \hline\hline" ///
"\end{tabular} }" ) append sty(tex)







snapshot restore 1

* employment
est clear
eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
   [aw=wgt] , a(i.year)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"    

ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
outname("$output/Graphs/NETS_firm_DFL_06_11_2019_r0") ylab("-.12(.02).04")  

rename es_b1 es_0
rename time1 t_0
rename es_ci_l1 es_l0
rename es_ci_h1 es_h0 

eststo emp_1:  reghdfe emp_growth _IyeaXP*  ///
   [aw=wgt] , a(i.year#i.naic2)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"    

ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
outname("$output/Graphs/NETS_firm_DFL_06_11_2019_r1") ylab("-.12(.02).04")  

rename es_b1 es_1
rename time1 t_1
rename es_ci_l1 es_l1
rename es_ci_h1 es_h1 

eststo emp_2:  reghdfe emp_growth _IyeaXP*  ///
  if major_sec  [aw=wgt] , a(i.year#i.naic2)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"      

ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
outname("$output/Graphs/NETS_firm_DFL_06_11_2019_r2") ylab("-.12(.02).04")  

rename es_b1 es_2
rename time1 t_2
rename es_ci_l1 es_l2
rename es_ci_h1 es_h2  

eststo emp_3:  reghdfe emp_growth _IyeaXP* ///
  if major_ind [aw=wgt] , a(i.year#i.naic2)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"     

ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
outname("$output/Graphs/NETS_firm_DFL_06_11_2019_r3") ylab("-.12(.02).04")  

rename es_b1 es_3
rename time1 t_3
rename es_ci_l1 es_l3
rename es_ci_h1 es_h3  

eststo emp_4:  reghdfe emp_growth _IyeaXP* ///
  if major_ind [aw=w] , a(i.year#i.naic2)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"       
  
ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
outname("$output/Graphs/NETS_firm_DFL_06_11_2019_r4") ylab("-.12(.02).04")    
*graph export "$output/Graphs/NETS_firm_DFL_06_04_2019.pdf", replace 

rename es_b1 es_4
rename time1 t_4
rename es_ci_l1 es_l4
rename es_ci_h1 es_h4  

replace t_0 = t_0 - 0.2
replace t_1 = t_1 - 0.1
replace t_3 = t_3 + 0.1
replace t_4 = t_4 + 0.2
 graph twoway (scatter es_0 t_0, lcolor(maroon) connect(line) mcolor(maroon) msymbol(plus) yline(0 , lcolor(red)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
				(rcap es_l0 es_h0 t_0, lcolor(maroon) lpattern(dash)) ///
				(scatter es_1 t_1, lcolor(navy) connect(line) mcolor(navy) msymbol(circle) msize(small)) ///
				(rcap es_l1 es_h1 t_1, lcolor(navy) lpattern(dash)) ///
				(scatter es_2 t_2, lcolor(orange) connect(line) mcolor(orange) msymbol(square) msize(small)) ///
				(rcap es_l2 es_h2 t_2, lcolor(orange) lpattern(dash)) ///
				(scatter es_3 t_3, lcolor(green) connect(line) mcolor(green) msymbol(diamond) msize(small)) ///
				(rcap es_l3 es_h3 t_3, lcolor(green) lpattern(dash)) ///
				(scatter es_4 t_4, lcolor(black) connect(line) mcolor(black) msymbol(triangle) msize(small)) ///
				(rcap es_l4 es_h4 t_4, lcolor(black) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white))  ///
	xtitle("Year") xlab(1990(5)2007) bgcolor(white) ///
	legend(on order (1 2 3 4 5 6 7 8 9 10) label(1 "All Firms")  ///
	label(2 "95% CI") label(3 "All firms with FE")  ///
	label(4 "95% CI") label(5 "Major Sectors with FE")  ///
	label(6 "95% CI") label(7 "Major Industries with FE")  ///
	label(8 "95% CI") label(9 "Major Industries + DFL with FE")  ///
	label(10 "95% CI") r(5)) ///
	yti("Effect of S936 on Firm Employment", margin(medium))
graph export "$output/Graphs/NETS_firm_DFL_06_11_2019_mess.pdf", replace 

******** event study table

esttab emp_? using "$output/Tables/NETS_firm_ES_20190605.tex", drop(*) stats() ///
b(3) se par label ///
noobs nogap nomtitle tex replace  nocons postfoot("\multicolumn{5}{l}{\textbf{Exposure to Section 936}} \\") prefoot("") posthead("")

esttab emp_? using "$output/Tables/NETS_firm_ES_20190605.tex", keep(_IyeaXPR_*) ///
 b(3) se par mlab(none) coll(none) s() noobs nonum nogap scalars("N Observations"  )  sfmt(0) /// 
varlabel(_IyeaXPR_1990 "\hspace{1em}X 1990 " /// 
_IyeaXPR_1991 "\hspace{1em}X 1991 " /// 
_IyeaXPR_1992 "\hspace{1em}X 1992 " /// 
_IyeaXPR_1993 "\hspace{1em}X 1993 " /// 
_IyeaXPR_1994 "\hspace{1em}X 1994 " /// 
_IyeaXPR_1996 "\hline \hspace{1em}X 1996 " /// 
_IyeaXPR_1997 "\hspace{1em}X 1997 " /// 
_IyeaXPR_1998 "\hspace{1em}X 1998 " /// 
_IyeaXPR_1999 "\hspace{1em}X 1999 " /// 
_IyeaXPR_2000 "\hspace{1em}X 2000 " /// 
_IyeaXPR_2001 "\hspace{1em}X 2001 " /// 
_IyeaXPR_2002 "\hspace{1em}X 2002 " /// 
_IyeaXPR_2003 "\hspace{1em}X 2003 " /// 
_IyeaXPR_2004 "\hspace{1em}X 2004 " /// 
_IyeaXPR_2005 "\hspace{1em}X 2005 " /// 
_IyeaXPR_2006 "\hspace{1em}X 2006 " /// 
_IyeaXPR_2007 "\hspace{1em}X 2007 " /// 
_IyeaXPR_2008 "\hspace{1em}X 2008 " /// 
_IyeaXPR_2009 "\hspace{1em}X 2009 " /// 
_IyeaXPR_2010 "\hspace{1em}X 2010 " /// 
_IyeaXPR_2011 "\hspace{1em}X 2011 " /// 
_IyeaXPR_2012 "\hspace{1em}X 2012 ")  posth("\hline") ///
preh("")  postfoot( ///
"\hline Year Fixed Effects    & Y & Y & Y & Y & Y \\ " ///
"NAICS-by-Year Fixed Effects    & & Y & Y & Y & Y \\ " ///
"S936 Exposed Sector     & & & Y & Y & Y  \\ "  ///
"S936 Exposed Industry & & &   & Y  & Y  \\ " /// 
"DFL Weights & & & & &  Y   \\  \hline\hline" ///
"\end{tabular} }" ) append sty(tex)





*** Regression on post years 
est clear
eststo emp_1:  reghdfe emp_growth PR  ///
  if inrange(year,2004,2008) [aw=wgt] , a(year )  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"       
  
eststo emp_2:  reghdfe emp_growth PR  ///
 if inrange(year,2004,2008) &  major_sec [aw=wgt] , a(year )  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"         
  
eststo emp_3:  reghdfe emp_growth PR  ///
if inrange(year,2004,2008) &  major_sec & major_ind  [aw=wgt] , a(year)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"           
  
eststo emp_4:  reghdfe emp_growth PR  ///
if inrange(year,2004,2008) &  major_sec & major_ind  [aw=w] , a(year)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"      
  
********** long diff table without industry by year FE  
esttab emp_? using "$output/Tables/NETS_longdiff_20190605.tex", drop(*) stats() ///
b(3) se par label ///
noobs nogap nomtitle tex replace nocons postfoot("") prefoot("") posthead("")
 
 esttab emp_? using "$output/Tables/NETS_longdiff_20190605.tex", keep(PR) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) nonum noobs scalars("N Observations" ) ///
varlabel(PR "\hspace{1em}Exposure to Section 936")  posth("\hline") ///
preh("\multicolumn{5}{l}{\textbf{Employment Change by 2004-2008}}\\")  postfoot( ///
"\hline Year Fixed Effects     & Y & Y & Y & Y \\ " ///
"S936 Exposed Sector     &  & Y & Y & Y  \\ "  ///
"S936 Exposed Industry &  &   & Y  & Y  \\ " /// 
"DFL Weights & & &  &  Y   \\  \hline\hline" ///
"\end{tabular} }" ) append sty(tex) 
  
  
  
  *** Regression on post years with IndX Yr FEs

est clear 
eststo emp_1:  reghdfe emp_growth PR  ///
  if inrange(year,2004,2008) [aw=wgt] , a(year )  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"       
estimates save "$output/comparison_emp_yearFE", replace

eststo emp_2:  reghdfe emp_growth PR  ///
  if inrange(year,2004,2008) [aw=wgt] , a(year#naic2 )  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"         
  
eststo emp_3:  reghdfe emp_growth PR  ///
 if inrange(year,2004,2008) &  major_sec [aw=wgt] , a(year#naic2 )  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"         
  
eststo emp_4:  reghdfe emp_growth PR  ///
if inrange(year,2004,2008) &  major_sec & major_ind  [aw=wgt] , a(year#naic2)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"           
  
eststo emp_5:  reghdfe emp_growth PR  ///
if inrange(year,2004,2008) &  major_sec & major_ind  [aw=w] , a(year#naic2)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"           

********** long diff table with industry by year FE  
esttab emp_? using "$output/Tables/NETS_longdiff_20190605_FE.tex", drop(*) stats() ///
b(3) se par label ///
noobs nogap nomtitle tex replace  nocons postfoot("") prefoot("") posthead("")
 
 esttab emp_? using "$output/Tables/NETS_longdiff_20190605_FE.tex", keep(PR) ///
cells(b(fmt(3)) se(par) p) mlab(none) nonum coll(none) noobs scalars("N Observations" ) ///
varlabel(PR "\hspace{1em}Exposure to Section 936")  posth("\hline") ///
preh("\multicolumn{5}{l}{\textbf{Employment Change by 2004-2008}}\\")  postfoot( ///
"\hline Year Fixed Effects   & Y  & Y & Y & Y & Y \\ " ///
"NAICS-by-Year Fixed Effects   &  & Y & Y & Y & Y \\ " ///
"S936 Exposed Sector     & & & Y & Y & Y  \\ "  ///
"S936 Exposed Industry &  & &   & Y  & Y  \\ " /// 
"DFL Weights & & & & &  Y   \\  \hline\hline" ///
"\end{tabular} }" ) append sty(tex) 
  


  
*********************************************  
*** split out heterogeneity table
*********************************************
gen chemicals = (m_naic3==325)  
gen elect = inlist(m_naic3,333,335)
gen food = inlist(m_naic3,311,312) 

gen PR_int1 = PR * chemicals
gen PR_int2 = PR * food
gen PR_int3 = PR * elect
gen PR_int4 = PR *    (1-chemicals) * (1-elect) * (1-food)


est clear 
capture: drop PR_int
gen PR_int = PR
eststo emp_1:  reghdfe emp_growth PR_int  ///
  if inrange(year,2004,2008) [aw=wgt] , a(year )  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"       
  
replace PR_int = PR_int1
eststo emp_2:  reghdfe emp_growth PR_int PR_int?   ///
  if inrange(year,2004,2008) [aw=wgt] , a(year )  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"       

replace PR_int = PR_int2
eststo emp_3:  reghdfe emp_growth PR_int PR_int?  ///
  if inrange(year,2004,2008) [aw=wgt] , a(year )  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"  
  
replace PR_int = PR_int3
eststo emp_4:  reghdfe emp_growth PR_int PR_int?  ///
  if inrange(year,2004,2008) [aw=wgt] , a(year )  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"  

replace PR_int = PR_int4
eststo emp_5:  reghdfe emp_growth PR_int PR_int?  ///
  if inrange(year,2004,2008) [aw=wgt] , a(year )  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"  
  
  
esttab emp_? using "$output/Tables/NETS_longdiff_20190605_Het.tex", drop(*) stats() ///
b(3) se par label ///
noobs nogap nomtitle tex replace  nocons postfoot("") prefoot("") posthead("")
 
 esttab emp_? using "$output/Tables/NETS_longdiff_20190605_Het.tex", keep(PR_int) ///
cells(b(fmt(3)) se(par) p) mlab("All" "Chemicals" "Food" "Electronics" "Other" ) nonum coll(none) noobs scalars("N Observations" ) ///
varlabel(PR_int "\hspace{1em}Exposure to Section 936")  posth("\hline") ///
preh("\multicolumn{5}{l}{\textbf{Employment Change by 2004-2008}}\\")  postfoot( ///
"\hline Year Fixed Effects   & Y  & Y & Y & Y & Y \\  \hline\hline" ///
"\end{tabular} }" ) append sty(tex) 
  
  
  reghdfe emp_growth i.PR##i.m_naic3  ///
  if inrange(year,2004,2008) [aw=wgt] , a(year )  vce(cluster hqduns) noconst     
  
  
/*



*** DD excluding implementation years (DAN REMOVED hqduns FE since they don't do anything and aren't included eslewhere)
gen PR_post = PR*(year> 1995 & year<=1996)
gen PR_post2 = PR*(year> 1996)

/*
eststo emp_0:  reghdfe emp_growth PR PR_post PR_post2 PR ///
  [aw=wgt] , a(year )  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"     
*/
est clear
eststo emp_1:  reghdfe emp_growth PR PR_post PR_post2 PR ///
  [aw=wgt] , a(year#naic2 )  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"     
 /* 
eststo emp_2:  reghdfe emp_growth  PR_post PR_post2  PR ///
  [aw=wgt] , a(year#naic2 hqduns)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"       
 */ 
eststo emp_2:  reghdfe emp_growth  PR_post PR_post2 PR ///
 if  major_sec [aw=wgt] , a(year#naic2 )  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"         
  
eststo emp_3:  reghdfe emp_growth  PR_post PR_post2 PR ///
if  major_sec & major_ind  [aw=wgt] , a(year#naic2 )  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"           
  
eststo emp_4:  reghdfe emp_growth  PR_post PR_post2 PR ///
if   major_sec & major_ind  [aw=w]  , a(year#naic2 )  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"             

********** DD table with industry by year FE 
esttab emp_? using "$output/Tables/NETS_DD_20190605_FE.tex", drop(*) stats() ///
b(3) se par label ///
noobs nogap nomtitle tex replace  nocons postfoot("") prefoot("") posthead("")
 
 esttab emp_? using "$output/Tables/NETS_DD_20190605_FE.tex", keep(PR_post2) ///
cells(b(fmt(3)) se(par) p) mlab(none) nonum coll(none) noobs scalars("N Observations" ) ///
varlabel(PR_post2 "\hspace{1em}Exposure to Section 936 X Post")  posth("\hline") ///
preh("")  postfoot( ///
"\hline NAICS-by-Year Fixed Effects     & Y & Y & Y & Y \\ " ///
"S936 Exposed Sector     &  & Y & Y & Y  \\ "  ///
"S936 Exposed Industry &  &   & Y  & Y  \\ " /// 
"DFL Weights & & &  &  Y   \\  \hline\hline" ///
"\end{tabular} }" ) append sty(tex)    


