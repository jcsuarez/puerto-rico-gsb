** this file calculates the PDV of lost tax credits from 1996-2006 (maybe end at 2005?)
** author: dan
** date: 10-23-2018

clear all
set more off
snapshot erase _all

* bringing in annual inflation variable
import excel "$additional/bls inflation/SeriesReport-20181023131916_6d1c96.xlsx", sheet("BLS Data Series") cellrange(A12:P41) firstrow clear
keep Year Annual
rename Year year
rename Annual cpi

tempfile cpi
save "`cpi'"

** importing data
use "$SOI/aggregate_SOI", clear
replace variable = subinstr(variable,"_t_","",.)
replace variable = substr(variable,1,length(variable)-1) if substr(variable,length(variable)-2,length(variable)) == "_"

drop if variable == "manufact"
drop if variable == "tot_"
egen id = group(variable)

collapse (sum) Us *Income* Receipts , by(year) 

merge 1:1 year using "`cpi'"
drop _merge
sum cpi if year == 2017

foreach Q of var Us_Possessions_Tax_Credit Income_Subject_To_Tax Income_Tax Income_Tax_After_Credits Income_Tax_Before_Credits Net_Income Receipts {
replace `Q' = `Q' * r(mean) / cpi
}

** calculating losses
gen percent = Us_Possessions_Tax_Credit / Income_Subject_To_Tax 
sum percent if year == 1995
gen counter = r(mean) * Income_Subject_To_Tax
gen loss = counter - Us_Possessions_Tax_Credit

*** generating some discount values

gen DV1 = 1
gen DV2 = 1 / (1 + 0.07)^(year - 1996)
gen DV3 = 1 / (1 + 0.10)^(year - 1996)

*** generating PDV of losses

gen PDV1 = DV1 * loss * (inrange(year,1996,2005))
gen PDV2 = DV2 * loss * (inrange(year,1996,2005))
gen PDV3 = DV3 * loss * (inrange(year,1996,2005))

** values of interest
sum PDV1
di r(mean) * r(N)
sum PDV2
di r(mean) * r(N)
sum PDV3
di r(mean) * r(N)
