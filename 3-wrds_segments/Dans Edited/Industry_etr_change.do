** this file runs a regression of industry ETR on industry possessions tax credits
** author: dan
** date: 09-26-2018

clear all
set more off
snapshot erase _all

** importing data
use "$SOI/aggregate_SOI", clear
replace variable = subinstr(variable,"_t_","",.)
replace variable = substr(variable,1,length(variable)-1) if substr(variable,length(variable)-2,length(variable)) == "_"

drop if variable == "manufact"
drop if variable == "tot_"
egen id = group(variable)

collapse (sum) Us *Income* Receipts (first) variable, by(year id) 

	* defining ETR variable based on income taxes paid (not total taxes paid)
	gen ETR = Income_Tax_After_Credits / Income_Subject_To_Tax // Income_Subject_To_Tax
	gen net_margin = Net_Income / Receipts
	gen tax = Income_Tax_After_Credits
	gen lntax = ln(tax)

	* defining Us credit as a portion of Taxable income
	gen pos_cred = Us_Possessions_Tax_Credit / Income_Subject_To_Tax
	replace pos_cred = 0 if pos_cred == .
	gen raw_pos = Us_Possessions_Tax_Credit
	gen ln_pos = ln(raw_pos)
	
xtset id year	
gen d_pos_cred = pos_cred - l.pos_cred
gen d_ETR = ETR - l.ETR

gen d_raw_pos = raw_pos - l.raw_pos
gen d_tax = tax - l.tax



** Generating weights that are constant over time for each industry based on 1995 levels {
foreach var of var Income_Subject_To_Tax Net_Income Receipts {
gen `var'_temp = `var' if inlist(year,1994,1995)
bys id: egen `var'_base = mean(`var'_temp)
drop *temp

}
rename Income_Subject_To_Tax_base ISTT

snapshot save
** only using sample with variation in S936
keep if year <= 2006
*** regressions!
est clear
set more off

* version weighted

eststo reg_1: reg ETR pos_cred i.year i.id [aw=ISTT ], cl(id) 
	estadd local hasy "Yes" 
	estadd local hasid "Yes"
	estadd local hasistt "Yes"

eststo reg_2: reg ETR pos_cred i.year i.id  Net_Income Receipts [aw=ISTT ], cl(id) 
	estadd local hasy "Yes" 
	estadd local hasid "Yes"
	estadd local hasistt "Yes"
	estadd local hasni "Yes"

eststo reg_3: reg ETR pos_cred i.year i.id  Net_Income Receipts [aw=Net_Income_base ], cl(id) 
	estadd local hasy "Yes" 
	estadd local hasid "Yes"
	estadd local hasistt ""
	estadd local hasni "Yes"
	estadd local hasnib "Yes"

eststo reg_4: reg d_ETR d_pos_cred i.year  [aw=ISTT ], cl(id) 
	estadd local hasy "Yes" 
	estadd local hasid ""
	estadd local hasistt "Yes"
	estadd local hasdif "Yes"

eststo reg_5: reg d_ETR d_pos_cred i.year  Net_Income Receipts [aw=ISTT ], cl(id) 
	estadd local hasy "Yes" 
	estadd local hasid ""
	estadd local hasistt "Yes"
	estadd local hasdif "Yes"
	estadd local hasni "Yes"

eststo reg_6: reg d_ETR d_pos_cred i.year  Net_Income Receipts [aw=Net_Income_base ], cl(id) 
	estadd local hasy "Yes" 
	estadd local hasid ""
	estadd local hasistt ""
	estadd local hasdif "Yes"
	estadd local hasni "Yes"
	estadd local hasnib "Yes"


** outputting table weighted by taxable income
esttab reg_? using "$output/Tables/table_industry_ETR.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons nonum posthead(" ") ///
prefoot("") ///
postfoot(" ") ///

esttab reg_? using "$output/Tables/table_industry_ETR.tex", keep(pos_cred d_pos_cred)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s() /// 
coeflabel(d_pos_cred "\hspace{1em}Change in Possession Credits" pos_cred "\hspace{1em}Possession Credits") ///
preh("") noobs scalars(  "hasy Year Fixed Effects" ///
"hasid Industry Fixed Effects" "hasni Net Income and Sales Controls"  "hasdif Regression in First Differences" ///
"hasistt Taxable Income Weights" "hasnib Net Income Weights" ) label /// 
 prefoot("\hline") ///
postfoot( ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex)		
	
** looking into correlations across tax credits:
gen pos_cred_d = d.Us_Possessions_Tax_Credit
/*
foreach var of var Alternative_Minimum_Tax-Us_Possessions_Tax_Credit {
gen `var'_d = d.`var'
correl `var'_d pos_cred_d
correl `var'_d pos_cred_d [aw=Receipts]
}
*/
	
**** making some figures showing the annual changes in possessions tax credits and total tax bills
** scatterplot and line graph
foreach var of varlist ETR pos_cred { 
	capture: drop res_`var'
	reg `var' i.year i.id [aw=ISTT]  //  [aw=Receipts]
	predict res_`var', res
	}

gen line_y = .
gen line_x = .
replace line_y = 0.03 in 1
replace line_y = -.03 in 2
replace line_x = -.03 in 1
replace line_x = 0.03 in 2

	
foreach indep of varlist pos_cred {
* regression for scalars under figure
reg ETR `indep' i.year i.id [aw=ISTT] , cl(id) //
local b = round(_b[`indep'],.001)
local b : di %4.3f `b'
local se = round(_se[`indep'],.001)
local se : di %4.3f `se'
mat table = r(table)
local p = round(table[4,1],.001)
	
sum res_ETR [aw=ISTT],d
twoway  (lfitci  res_ETR res_`indep' [aw=ISTT], est(cl(id)) level(90) lcolor(black)) ///
		(scatter res_ETR res_`indep' [aw=ISTT], yline(`r(mean)',lcolor(black) lpattern(dash)) mfcolor(gs15)) ///
		(line line_y line_x, lcolor(red) lpattern(shortdash) lwidth(thick)),  /// 
		xlab(-.03(.02).03) ylab(-.1(.05).15) ///
		graphregion(fcolor(white)) ///
		xtitle("Industry Section 936 Credits Over Taxable Income (Residualized)") ///
		ytitle("Industy Effective Tax Rate (Residualized)") ///
		legend(order(3 - 2 1 4) label(3 "Data")  label(1 "90% CI") label(2 "Linear Fit") ///
		label(4 "No Replacement")) ///
		note("The slope of the regression line is `b' with standard error `se' and p-value `p'")
	
graph export "$output/Graphs/industry_ETR_graph_scatter_raw.pdf", replace		
}	

** Version with nicely binned scatter (manual for SE)
foreach var of varlist ETR pos_cred { 
	capture: drop res_`var'
	reg `var' i.year i.id [aw=ISTT] 
	predict res_`var', res
	}

	
** making measure of weighted quantile bins
foreach Q of var res_pos_cred {	
	qui sum `Q', d

	xtile q_`Q'=`Q' [aw=ISTT], nq(20)
	preserve 
	collapse (mean) res_ETR `Q' [aw=ISTT], by(q_`Q') 
	foreach y of var res_ETR {
	rename `y' q_`y'
	}
	rename `Q' q_m_`Q'
	tempfile qmeans
	save "`qmeans'", replace
	restore 
	merge m:1 q_`Q' using "`qmeans'"
	drop _merge 
}

capture: drop line_*
gen line_y = .
gen line_x = .
replace line_y = 0.012 in 1
replace line_y = -.012 in 2
replace line_x = -.012 in 1
replace line_x = 0.012 in 2
	
foreach indep of varlist pos_cred {	
reg ETR `indep' i.year i.id [aw=ISTT] , cl(id) //
local b = round(_b[`indep'],.0001)
local b : di %4.3f `b'
local se = round(_se[`indep'],.001)
local se : di %4.3f `se'
mat table = r(table)
local p = round(table[4,1],.001)
sum res_ETR [aw=ISTT],d
twoway  (lfitci  res_ETR res_`indep' [aw=ISTT], est(cl(id)) level(90) lcolor(black) range(-.012 .012)) ///
		(scatter q_res_ETR q_m_res_`indep', yline(`r(mean)',lcolor(black) lpattern(dash)) mcolor(black)) ///
		(line line_y line_x, lcolor(red) lpattern(shortdash) lwidth(thick)),  /// 
		xlab(-.01(.005).01) ylab(-.015(.005).015) ///
		graphregion(fcolor(white)) ///
		xtitle("Industry Section 936 Credits Over Taxable Income (Residualized)") ///
		ytitle("Industy Effective Tax Rate (Residualized)") ///
		legend(order(3 - 2 1 4) label(3 "Binned Data")  label(1 "90% CI") label(2 "Linear Fit") ///
		label(4 "No Replacement")) ///
		note("The slope of the regression line is  `b' with standard error `se' and p-value `p'")
	
graph export "$output/Graphs/industry_ETR_graph_scatter_bins.pdf", replace		
}	


**** something with the totals over time?
** making a version with breaking into high incidence industries
snapshot restore 1

sort pos_cred
sum pos_cred [aw= Income_Subject_To_Tax ] if year == 1995, d	
/* over median is:
man_minerals_ 0.0007
man_trans_
trans_ut
man_plastics_  0.003

man_comps_elec_ 0.009
man_mach_et_al_
man_foodbevtob_
man_apparel_
man_textile_
man_leather_
man_chemicals_ 0.03
*/

gen temp =  (pos_cred > 0.009) *(year == 1995)
bys id: egen exposed = max(temp)
drop temp

collapse (sum) Us *Income* Receipts  , by(year exposed)

gen share = Us_Possessions_Tax_Credit / Income_Subject_To_Tax
sum share if year == 1995 & exposed == 1, d												
gen const_cred = r(max) * Income_Subject_To_Tax if exposed == 1
sum share if year == 1995 & exposed == 0, d												
replace const_cred = r(max) * Income_Subject_To_Tax if exposed == 0

gen lost_cred = const_cred - Us_Possessions_Tax_Credit

** making some relative variables

* gen counterfactual
gen never_pos = Us_Possessions_Tax_Credit + Income_Tax_After_Credits			// Tax Liability with same behavior with no s936
gen tax_counter = Income_Tax_After_Credits -  0.8 * lost_cred							// Tax Liability without rolling back s936 with constant behavior 

gen p_never_pos = (Us_Possessions_Tax_Credit + Income_Tax_After_Credits) / Income_Subject_To_Tax
gen p_tax_counter = (Income_Tax_After_Credits - 0.8 * lost_cred) / Income_Subject_To_Tax
gen p_data = (Income_Tax_After_Credits) / Income_Subject_To_Tax

** outputting some graphs (Intention to treat)
twoway (scatter p_never_pos year if exposed == 1 & year > 1994 & year < 2008, c(line) lpattern(dash) ) ///
       (scatter p_data year if exposed == 1 & year > 1994 & year < 2008, c(line) lpattern(solid)) ///
	   , xlab(1995(2)2007) ylab(.14(.02).3) ///
	   	graphregion(fcolor(white)) bgcolor(white) ///
		xtitle("Year") ///
		ytitle("Effective Tax Rate for Exposed Industries", margin(medsmall)) ///
		legend(order(2 1) label(1 "Effective Tax Rate with No S936 (ITT)") label(2 "Observed Effective Tax Rate")  rows(3) ) 
graph export "$output/Graphs/SOI_counterfact_etr1_exp.pdf" , replace 

twoway (scatter p_tax_counter year if exposed == 1 & year > 1994 & year < 2008, c(line) lpattern(dash) ) ///
       (scatter p_data year if exposed == 1 & year > 1994 & year < 2008, c(line) lpattern(solid)) ///
	   , xlab(1995(2)2007) ylab(.14(.02).3) ///
	   	graphregion(fcolor(white)) bgcolor(white) ///
		xtitle("Year") ///
		ytitle("Effective Tax Rate for Exposed Industries", margin(medsmall)) ///
		legend(order(2 1) label(1 "Effective Tax Rate with Continuing S936 (ITT)") label(2 "Observed Effective Tax Rate")  rows(3) ) 
graph export "$output/Graphs/SOI_counterfact_etr2_exp.pdf" , replace 	
	* non exp
twoway (scatter p_never_pos year if exposed == 0 & year > 1994 & year < 2008, c(line) lpattern(dash) ) ///
       (scatter p_data year if exposed == 0 & year > 1994 & year < 2008 , c(line) lpattern(solid)) ///
	   , xlab(1995(2)2007) ylab(.14(.02).3) ///
	   	graphregion(fcolor(white)) bgcolor(white) ///
		xtitle("Year") ///
		ytitle("Effective Tax Rate for Exposed Industries", margin(medsmall)) ///
		legend(order(2 1) label(1 "Effective Tax Rate with No S936 (ITT)") label(2 "Observed Effective Tax Rate")  rows(3) ) 
graph export "$output/Graphs/SOI_counterfact_etr1_nexp.pdf" , replace 

twoway (scatter p_tax_counter year if exposed == 0 & year > 1994 & year < 2008, c(line) lpattern(dash) ) ///
       (scatter p_data year if exposed == 0 & year > 1994 & year < 2008, c(line) lpattern(solid)) ///
	   , xlab(1995(2)2007) ylab(.14(.02).3) ///
	   	graphregion(fcolor(white)) bgcolor(white) ///
		xtitle("Year") ///
		ytitle("Effective Tax Rate for Exposed Industries", margin(medsmall)) ///
		legend(order(2 1) label(1 "Effective Tax Rate with Continuing S936 (ITT)") label(2 "Observed Effective Tax Rate")  rows(3) ) 
graph export "$output/Graphs/SOI_counterfact_etr2_nexp.pdf" , replace 	
		
*** trying to think more seriously about Treatment on the treated:

	*first, a variable that shows counterfactual ETR without substitution to other credits (for ever $1 of lost S936, firms only pay $0.80 more in taxes)
	* adding back in $0.20 on each dollar
	gen tax_counter2 = Income_Tax_After_Credits - 0.8 * lost_cred							// Tax Liability without substituting to other tax avoidance 
	gen p_tax_counter2 = (Income_Tax_After_Credits - 0.8 * lost_cred) / Income_Subject_To_Tax

	* second, find the percent of firms in the "intention to treat industries" who are actually treated:
		* (1) Employment percent of both the total and of each category (top exposed quartile by taxable income is only 10% of employment in QCEW and NETS)
		rename exposed high_exp_ind
		merge m:1 high_exp_ind using "$output_NETS/industry_cat_emp_sums"
		drop _merge
	
		* making totals
		foreach Q of var emp95-total_annual_wages {
		sum `Q'
		gen tot_`Q' = r(min) + r(max) // only 2 values
		}
			
			*totals
			gen treat_share_tot_NETS = tot_pr_emp / tot_emp95
			gen treat_share_tot_QCEW = tot_pr_emp / tot_annual_avg_emplvl          	// comparing NETS to NETS makes more sense

			*separated by top quartile to bottom quartiles
			gen treat_share_NETS = pr_emp / emp95
			gen treat_share_QCEW = pr_emp / annual_avg_emplvl          	// comparing NETS to NETS makes more sense

			
		* from the literature: (unable to find existing estimate)
** making new treatment on the treated variable saying all credits come from PR firms, income allocated by employment share
gen p_tax_counter_dif = (Income_Tax_After_Credits * treat_share_NETS - 0.8 *lost_cred) / (Income_Subject_To_Tax * treat_share_NETS)

sort high_e year
twoway (scatter p_tax_counter_dif year if high_exp_ind == 0 & year > 1994 & year < 2008, c(line) lpattern(dash) ) ///
       (scatter p_data year if high_exp_ind == 0 & year > 1994 & year < 2008, c(line) lpattern(solid)) ///
	   , xlab(1995(2)2007) ylab(.14(.02).3) ///
	   	graphregion(fcolor(white)) bgcolor(white) ///
		xtitle("Year") ///
		ytitle("Effective Tax Rate for Exposed Industries", margin(medsmall)) ///
		legend(order(2 1) label(1 "Effective Tax Rate with Continuing S936 (TOTT)") label(2 "Observed Effective Tax Rate")  rows(3) ) 
graph export "$output/Graphs/SOI_tott_etr2_nexp.pdf" , replace 

twoway (scatter p_tax_counter_dif year if high_exp_ind == 1 & year > 1994 & year < 2008, c(line) lpattern(dash) ) ///
       (scatter p_data year if high_exp_ind == 1 & year > 1994 & year < 2008, c(line) lpattern(solid)) ///
	   , xlab(1995(2)2007) ylab(.14(.02).3) ///
	   	graphregion(fcolor(white)) bgcolor(white) ///
		xtitle("Year") ///
		ytitle("Effective Tax Rate for Exposed Industries", margin(medsmall)) ///
		legend(order(2 1) label(1 "Effective Tax Rate with Continuing S936 (TOTT)") label(2 "Observed Effective Tax Rate")  rows(3) ) 
graph export "$output/Graphs/SOI_tott_etr2_exp.pdf" , replace 

*********** version in totals
collapse (sum) Us *Income* Receipts  emp95 pr_emp annual_avg_emplvl , by(year)


gen share = Us_Possessions_Tax_Credit / Income_Subject_To_Tax
sum share if year == 1995, d													// Any reason for difference between 1994 and 1995?
gen const_cred = r(max) * Income_Subject_To_Tax

gen lost_cred = const_cred - Us_Possessions_Tax_Credit

** making some relative variables

* gen counterfactual
gen never_pos = Us_Possessions_Tax_Credit + Income_Tax_After_Credits			// Tax Liability with same behavior with no s936
gen tax_counter = Income_Tax_After_Credits - 0.8 * lost_cred							// Tax Liability without rolling back s936 adjusted for behavior

gen p_never_pos = (Us_Possessions_Tax_Credit + Income_Tax_After_Credits) / Income_Subject_To_Tax
gen p_tax_counter = (Income_Tax_After_Credits - 0.8 * lost_cred) / Income_Subject_To_Tax
gen p_data = (Income_Tax_After_Credits) / Income_Subject_To_Tax

** outputting some intention to treat graphs
twoway (scatter p_never_pos year if year > 1994 & year < 2008 , c(line) lpattern(dash) ) ///
       (scatter p_data year if year > 1994  , c(line) lpattern(solid)) ///
	   , xlab(1995(2)2007) ylab(.14(.02).3) ///
	   	graphregion(fcolor(white)) bgcolor(white) ///
		xtitle("Year") ///
		ytitle("Effective Tax Rate", margin(medsmall)) ///
		legend(order(2 1) label(1 "Effective Tax Rate with No S936 (ITT)") label(2 "Observed Effective Tax Rate")  rows(3) ) 
graph export "$output/Graphs/SOI_counterfact_etr1.pdf" , replace 

twoway (scatter p_tax_counter year if year > 1994 & year < 2008, c(line) lpattern(dash) ) ///
       (scatter p_data year if year > 1994 & year < 2008 , c(line) lpattern(solid)) ///
	   , xlab(1995(2)2007) ylab(.14(.02).3) ///
	   	graphregion(fcolor(white)) bgcolor(white) ///
		xtitle("Year") ///
		ytitle("Effective Tax Rate", margin(medsmall)) ///
		legend(order(2 1) label(1 "Effective Tax Rate with Continuing S936 (ITT)") label(2 "Observed Effective Tax Rate")  rows(3) ) 
graph export "$output/Graphs/SOI_counterfact_etr2.pdf" , replace 	
	
* outputting some treatment on the treated graphs	
gen treat_share_NETS = pr_emp / emp95
gen p_tax_counter_tot = (Income_Tax_After_Credits * treat_share_NETS - 0.8 *lost_cred) / (Income_Subject_To_Tax * treat_share_NETS)

twoway (scatter p_tax_counter_tot year if year > 1994 & year < 2008, c(line) lpattern(dash) ) ///
       (scatter p_data year if year > 1994 & year < 2008, c(line) lpattern(solid)) ///
	   , xlab(1995(2)2007) ylab(.14(.02).3) ///
	   	graphregion(fcolor(white)) bgcolor(white) ///
		xtitle("Year") ///
		ytitle("Effective Tax Rate for Exposed Industries", margin(medsmall)) ///
		legend(order(2 1) label(1 "Effective Tax Rate with Continuing S936 (TOTT)") label(2 "Observed Effective Tax Rate")  rows(3) ) 
graph export "$output/Graphs/SOI_tott_etr2.pdf" , replace 
