set more off 
clear
local nn = 16
set obs `nn'
gen year = _n-6


/* Depreciation rates come from: 
https://www.bea.gov/national/pdf/BEA_depreciation_rates.pdf
same thing: https://www.bea.gov/national/FA2004/Tablecandtext.pdf
*/ 

// Categories are all under  "General industrial, including materials handling equipment" (different measures in different categories)
local delta1 = .0247 // Office Builsingds 
local delta2 = .0686 // Machinery 
local delta3 = .1225   // Computer and electronic products 

local beta = .26

forval i = 1/3 { 
	gen K`i' = 1 
} 	


/*
replace K1= (1-`delta1'*`beta')^year if year >=0
replace K2= (1-`delta2'*`beta')^year if year >=0
replace K3= (1-`delta3'*`beta')^year if year >=0
*/

/* This comes from

K_t = (1-delta)K_t-1 + I 

I_0 = \alpha K_0 

I_t = (1-beta) \alpha K_0 

\alpha = \delta 

So 

K_t = (1-delta)K_t-1 + (1-beta)delta K_0

*/


forval i = 7/`nn' { 
	forval j = 1/3 { 
		replace K`j' =  K`j'[`i'-1]*(1-`delta`j'' + (1-`beta')*`delta`j''*1/K`j'[`i'-1])  in `i'
	}
} 


/* 
forval i = 1/3 { 
	replace K`i' = 100*(K`i'-1)
} 	
*/

twoway (scatter K1 year , lcolor(navy) mcolor(navy) connect(line) lpattern(solid) xline(0) )  ///
	   (scatter K2 year , lcolor(orange) mcolor(orange) msymbol(D) lpattern(dash) connect(line) )  ///
	   (scatter K3 year , lcolor(green) mcolor(green) msymbol(S) lpattern(dash_dot) connect(line) ) , ///
	   ylab(0(.25)1) ytitle("Capital Stock Relative to Steady State") graphregion(color(white)) ///
	xline(0) bgcolor(white) xtitle("Years from 26% Investment Decrease")  ///
	legend(label(1 "Buildings, {&delta} = 0.025") ///
	label(2 "Machinery, {&delta} = 0.069") label(3 "Electronics, {&delta} = 0.123"))
graph export "$output/Graphs/depreciation_calc.pdf", replace	

*** outputting some scalars
do "$analysis/jc_stat_out.ado"


jc_stat_out,  number(`delta1') name("RESULTbuilddep") replace(0) deci(3) figs(4)
jc_stat_out,  number(`delta2') name("RESULTmachdep") replace(0) deci(3) figs(4)
jc_stat_out,  number(`delta3') name("RESULTcompdep") replace(0) deci(3) figs(4)

sum K1 if year == 10
local K1_10 = 100 -  r(mean) * 100
di `K1_10'
jc_stat_out,  number(`K1_10') name("RESULTbuildloss") replace(0) deci(1) figs(3)

sum K2 if year == 10
local K2_10 = 100 -  r(mean) * 100
di `K2_10'
jc_stat_out,  number(`K2_10') name("RESULTmachloss") replace(0) deci(1) figs(4)

sum K3 if year == 10
local K3_10 = 100 -  r(mean) * 100
di `K3_10'
jc_stat_out,  number(`K3_10') name("RESULTcomploss") replace(0) deci(1) figs(4)

