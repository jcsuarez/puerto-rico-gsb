
local y ftax_t
reghdfe `y'_1 _IyeaX* PR IHS_rev log_tot_emp i.year [aw=DFL] ///
 if  ~inlist(n3,"324","323","337","322","327","336","331","321") & ///
 inlist(n2,3,5,6,7,11,12) & pi > 4, cl(gvkey)   a(i.year#i.n2)
 
mat b = 0 
mat b_ub = 0 
mat b_lb = 0 
mat time = 0 

lincom (_IyeaXPR_1991 +_IyeaXPR_1992)/2
mat b = (b,r(estimate))
mat b_ub = (b_ub,r(ub))
mat b_lb = (b_lb,r(lb))
mat time = (time,1991)

lincom (_IyeaXPR_1991 +_IyeaXPR_1992+_IyeaXPR_1993)/3
mat b = (b,r(estimate))
mat b_ub = (b_ub,r(ub))
mat b_lb = (b_lb,r(lb))
mat time = (time,1992)

lincom (_IyeaXPR_1992 +_IyeaXPR_1993+_IyeaXPR_1994)/3
mat b = (b,r(estimate))
mat b_ub = (b_ub,r(ub))
mat b_lb = (b_lb,r(lb))
mat time = (time,1993)

lincom (_IyeaXPR_1993 +_IyeaXPR_1994)/2
mat b = (b,r(estimate))
mat b_ub = (b_ub,r(ub))
mat b_lb = (b_lb,r(lb))
mat time = (time,1994)

lincom (_IyeaXPR_1996 +_IyeaXPR_1997)/2
mat b = (b,r(estimate))
mat b_ub = (b_ub,r(ub))
mat b_lb = (b_lb,r(lb))
mat time = (time,1996)

lincom (_IyeaXPR_1996 +_IyeaXPR_1997+_IyeaXPR_1998)/3
mat b = (b,r(estimate))
mat b_ub = (b_ub,r(ub))
mat b_lb = (b_lb,r(lb))
mat time = (time,1997)

lincom (_IyeaXPR_1997 +_IyeaXPR_1998+_IyeaXPR_1999)/3
mat b = (b,r(estimate))
mat b_ub = (b_ub,r(ub))
mat b_lb = (b_lb,r(lb))
mat time = (time,1998)

lincom (_IyeaXPR_1998 +_IyeaXPR_1999+_IyeaXPR_2000)/3
mat b = (b,r(estimate))
mat b_ub = (b_ub,r(ub))
mat b_lb = (b_lb,r(lb))
mat time = (time,1999)

lincom (_IyeaXPR_1999 +_IyeaXPR_2000+_IyeaXPR_2001)/3
mat b = (b,r(estimate))
mat b_ub = (b_ub,r(ub))
mat b_lb = (b_lb,r(lb))
mat time = (time,2000)

lincom (_IyeaXPR_2000 +_IyeaXPR_2001+_IyeaXPR_2002)/3
mat b = (b,r(estimate))
mat b_ub = (b_ub,r(ub))
mat b_lb = (b_lb,r(lb))
mat time = (time,2001)

lincom (_IyeaXPR_2001 +_IyeaXPR_2002+_IyeaXPR_2003)/3
mat b = (b,r(estimate))
mat b_ub = (b_ub,r(ub))
mat b_lb = (b_lb,r(lb))
mat time = (time,2002)

lincom (_IyeaXPR_2002 +_IyeaXPR_2003+_IyeaXPR_2004)/3
mat b = (b,r(estimate))
mat b_ub = (b_ub,r(ub))
mat b_lb = (b_lb,r(lb))
mat time = (time,2003)

lincom (_IyeaXPR_2003 +_IyeaXPR_2004+_IyeaXPR_2005)/3
mat b = (b,r(estimate))
mat b_ub = (b_ub,r(ub))
mat b_lb = (b_lb,r(lb))
mat time = (time,2004)

lincom (_IyeaXPR_2004 +_IyeaXPR_2005+_IyeaXPR_2006)/3
mat b = (b,r(estimate))
mat b_ub = (b_ub,r(ub))
mat b_lb = (b_lb,r(lb))
mat time = (time,2005)

lincom (_IyeaXPR_2005+_IyeaXPR_2006)/2
mat b = (b,r(estimate))
mat b_ub = (b_ub,r(ub))
mat b_lb = (b_lb,r(lb))
mat time = (time,2006)

mat b = b[1,2...]'
mat b_lb = b_lb[1,2...]'
mat b_ub = b_ub[1,2...]'
mat time = time[1,2...]'

capture: drop b1 b_lb1 b_ub1 time1
svmat b
svmat b_lb
svmat b_ub
svmat time

twoway (scatter b1 time) (rcap b_lb1 b_ub1 time) ,  yline(0) 

