set more off
ssc install winsor
*ssc install coefplot

// goal of this file is to test what kinds of firms are in PR (descriptive)
// author: dan
// date: 3-12-2018

************************************
* Grabbing and organizing the data
************************************

clear all 
use "$WRDS/ETR_compustat.dta", replace
replace PR = 0 if PR == .
gen nonPR = 1-PR

// doing a cross section in 1995 of active firms with positive assets
keep if year == 1995
keep if at > 0 & at != .

// using 4d naics controls
tostring naics, replace
gen n4 = substr(naics,1,4)
gen n3 = substr(naics,1,3)
destring n3, replace
gen n2 = substr(naics,1,2)
destring n2, replace

gen pharma = (n4 == "3254")

replace ads = 0 if ads == .
replace rd  = 0 if rd  == .
replace dltt= 0 if dltt== . 

// winsorizing rd/assets and dltt/assets
replace rd = rd / at
qui sum rd, d
replace rd = r(p99) if rd >r(p99)
replace ads = ads / at
qui sum ads, d
replace ads = r(p99) if ads >r(p99)
replace dltt = dltt / at
qui sum dltt, d
replace dltt = r(p99) if dltt >r(p99)

gen any_ads = ads > 0
gen any_rd  = rd  > 0

gen gpop = gp / (at)
gen PRexp = emp_PR / (emp_PR + emp_US)
gen lnat = ln(at)

probit PR i.n3 lnat pharma rd any_rd gpop any_ads etr,  vce(cl n2)
gen count = 1 if e(sample)
keep if count == 1
drop count
gen prof = (ni > 0) * (ni != .)

foreach i of var etr-nol btd etr_change gpop PRexp at lnat dltt ni {
qui sum `i', d
gen mean`i' = r(mean)
gen sd`i' = r(sd)
replace `i' = (`i' - mean`i') / sd`i'
drop mean`i' sd`i'
}

local spec1 "i.n3 lnat pharma "
local spec2 "i.n3 lnat pharma etr "
local spec3 "i.n3 lnat pharma etr rd any_rd "
local spec4 "i.n3 lnat pharma etr rd any_rd gpop dltt"
local spec5 "i.n3 lnat pharma etr rd any_rd gpop dltt any_ads ads"

est clear

forval i = 1/5 {
eststo reg_`i':  probit PR `spec`i'',  vce(cl n2)
	estadd local ind1 ""
	estadd local ind13 "Yes"
}
	
esttab reg_* using "$output/Tables/pr_presence.tex", keep(_cons) stats() ///
b(3) se par label coeflabel(event_dummy "\hspace{1em}Event") ///
noobs nogap tex replace nocons postfoot("") nomti nodep

esttab reg_* using "$output/Tables/pr_presence.tex", keep(any_rd gpop dltt etr lnat pharma any_ads) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) order(_cons day*)  ///
varlabel(any_ads "\hspace{1em}Any Advertising" any_rd "\hspace{1em}Any Research \& Development" gpop "\hspace{1em}Gross Profit / Operating Assets"  ///
etr "\hspace{1em}ETR" pharma "\hspace{1em}NAICS 3254 (Pharma)" lnat "\hspace{1em}ln(Assets)" dltt "\hspace{1em}Long-term Debt / Assets" ///
) ///
scalars( N "ind13 3 Digit NAICS Industry Fixed Effects" ) label /// 
preh("") prefoot("\hline")  postfoot( ///
"\hline " ///
"\end{tabular} }" ) append sty(tex) nonum nomti  nodep


predict phat_PR, pr 

twoway (kdensity phat_PR if PR & phat_PR > .03)  (kdensity phat_PR if PR==0 & phat_PR > .03) 

twoway (kdensity phat_PR if PR & phat_PR > .15)  (kdensity phat_PR if PR==0 & phat_PR > .15) 

br if phat_PR > .15 & PR == 0 

/*
*saving the data for the AEAPP submission
keep PR n3 lnat pharma etr rd any_rd gpop dltt any_ads ads n2

* Running the main regression once to keep only relevant sample with full data
	eststo marg_reg_1: probit PR i.n3 lnat pharma etr rd any_rd gpop dltt any_ads ads,  vce(cl n2)
	keep if e(sample) == 1
	
save "C:/Users/Dan's Work PC/Dropbox (JC's Data Emporium)/Puerto Rico/AEAPP_Draft/AEAPP_Submission/Data/table1_data.dta", replace

*/

*** making a short version that also include the margins
eststo marg_reg_1: probit PR i.n3 lnat pharma etr rd any_rd gpop dltt any_ads ads,  vce(cl n2)
keep if e(sample) == 1

foreach i of var lnat  etr rd  gpop dltt  ads {
qui sum `i', d
gen mean`i' = r(mean)
gen sd`i' = r(sd)
replace `i' = (`i' - mean`i') / sd`i'
drop mean`i' sd`i'
}
eststo marg_reg_1: probit PR i.n3 lnat  etr rd  gpop dltt pharma any_rd any_ads ads,  vce(cl n2)
estadd local indl3 = "Yes"

margins, dydx(lnat gpop pharma any_rd any_ads) post
eststo marg_reg_marg
sum PR
estadd local pbase = string(r(mean), "%8.3fc")
estadd local indl3 = "Yes"

esttab marg_reg_1 marg_reg_marg using "$output/Tables/pr_presence_marg.tex", keep(_cons) stats() ///
b(3) se par label coeflabel(event_dummy "\hspace{1em}Event") ///
noobs nogap tex replace nocons postfoot("") nomti nodep nonum prefoot(" & Estimates & Marginal \\ ") posthead("")

esttab marg_reg_1 marg_reg_marg using "$output/Tables/pr_presence_marg.tex", keep(lnat pharma any_rd gpop any_ads) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) order(_cons day*)  ///
varlabel(any_ads "\hspace{1em}Any Advertising" any_rd "\hspace{1em}Any Research \& Development" gpop "\hspace{1em}Gross Profit / Operating Assets"  ///
etr "\hspace{1em}ETR" pharma "\hspace{1em}NAICS 3254 (Pharma)" lnat "\hspace{1em}ln(Assets)" dltt "\hspace{1em}Long-term Debt / Assets" ///
) ///
scalars( N "indl3 3 Digit NAICS Industry Fixed Effects" "pbase Mean Probability of \S936 Exposure") label /// 
preh("") prefoot("\hline")  postfoot( ///
"\hline " ///
"\end{tabular} }" ) append sty(tex) nonum nomti  nodep


