//// Get PR firm data 

clear all
set more off
*ssc install dataout
snapshot erase _all


// Second Define Command for ES 
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(solid)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Estimate") label(2 "`level'% CI")) ///
	ytitle("`yti'") ///
	note("`note'") ylab("`ylab'")
graph export "`outname'.pdf", replace	

end 



*** get list of hqduns that actually work by dropping gov't etc... 
use "$output_NETS/pr_extract_est.dta", clear

gen naic4 = floor(naics95/100)
gen naic3 = floor(naics95/1000)
gen naic2 = floor(naics95/10000)

********************
*getting rid of governmental and social non-profit orgs
********************
drop if naic2 == 92 | naic3 == 813
* dropping USPS and the federal government
drop if hqduns95 == 3261245 | hqduns95 == 161906193

* dropping firms with HQ in PR
drop if pr_based == 1 

* dropping county industries with no link
drop if pr_link == 0
bysort hqduns: egen temp_max = max(emp95)
gen temp_keep = (emp95 == temp_max)

keep if temp_keep == 1

* making a list of hqduns to use from the panel data
keep hqduns naic3
bysort hqduns: egen m_naic3 = mode(naic3), minmode
drop naic3
duplicates drop 
tempfile tokeep 
save "`tokeep'"

** old code 
use "$output_NETS/pr_extract_panel.dta", clear 

** Keep only firms at start of the sample 
gen temp = emp_PR if year == 1990
bysort hqd: egen base_emp_PR = min(temp)
drop temp 
keep if base > 0  // This line drops 4,991 observations

** MErge in to keep 
merge m:1 hqduns95 using "`tokeep'"
keep if _merge == 3 // This drops 23 observations from the master, 214 from using

egen firm = group(hqd) 
sum firm, d
snapshot save

keep hqd year emp_US num_est_US m_naic3
gen PR = 1 

tempfile PRdata
save "`PRdata'" 

/// Get control data 
* Get control Data 
use "$output_NETS_control/hq_est_co_firm_master.dta", clear
drop if emp95 < 6
drop if num_est95 < 3

*sample 1000, count by(firm_group)
keep if fipsstate95 != . 
drop if fipsstate95 > 56
rename fipsstate95 hqfips
 
gen firm_group2 = floor(firm_group/10000)
gen census_div = floor(firm_group2/1000)
gen naic3 = firm_group2-1000*census_div
drop firm_group2
gen PR = 0 

rename naic3 m_naic3
drop hqfips census_div  PR firm_group 

foreach y in 90 91 92 93 94 95 96 97 98 99 00 01 02 03 04 05 06 07 08 09 10 11 12 {
	rename emp`y' emp_`y'
	rename num_est`y' num_`y'
}

reshape long emp_ num_ , i(hqduns m_nai) j(year) string
 
destring year , replace 
replace year = year+1900 if year > 50 
replace year = year+2000 if year < 50 
sort year
rename emp_ emp_US
rename num_ num_est_US
gen PR = 0 

append using "`PRdata'"

// emp growth 
gen temp = emp_US if year == 1995
bysort hqduns95 : egen base_emp = max(temp)
drop temp

gen emp_growth = (emp_US - base_emp)/base_emp
replace emp_growth = 0 if emp_growth == . 

// estab growth 
gen temp = num_est_US if year == 1995
bysort hqduns95 : egen base_est = max(temp)
drop temp

gen est_growth = (num_est_US - base_est)/base_est
replace est_growth = 0 if est_growth == . 


drop if base_emp ==0 
sum base_emp if year == 1995 & PR , d
gen wgt=base_emp/( r(mean)) if PR 

sum base_emp if year == 1995 & PR==0 , d
replace wgt=base_emp/( r(mean)) if PR==0 

*replace wgt= base_emp

** Make balanced panel by dropping firms that drop out
bys hqduns95 : egen min_emp = min(emp_US) 
drop if min_emp == 0 

set matsize 4000
* DFL 
gen naic2 = floor(m_nai/10)
gen pharma = (m_naic3 == 325)
capture: drop q_wgt
*xtile q_wgt = wgt if year == 1995, n(10) 
*logit PR i.q_wgt#i.pharma
* OK with 30 q wght: logit PR i.q_wgt#i.naic2
xtile q_wgt = wgt if year == 1995, n(20) 
logit PR i.q_wgt#i.naic2


capture: drop phat min_phat w w_phat
predict phat, pr 
winsor phat, p(.01) g(w_phat) 
replace phat = w_phat
bys hqduns: egen min_phat = min(phat) 
* ATE
* gen w = wgt*(PR/min_phat+(1-PR)/(1-min_phat))
* ATOT
gen w = wgt*(PR+(1-PR)*min_phat/(1-min_phat))

xi i.year|PR, noomit 
drop _IyeaXP*1995
  
gen chemicals = (m_naic3==325)  
gen elect = (inlist(m_naic3,333,335))
gen food = (inlist(m_naic3,311,312))  

gen het_index = chemi +2*elect +3*food
  
est clear

gen PR_post = PR*(year> 1995) 
***** Generate 
*local if1_1 = "~inlist(n3,"324","323","337","322","327","336","331","321")"
*local if2_1 = "inlist(n2,3,5,6,7,11,12)"	

eststo emp_1:  reghdfe emp_growth PR PR_post  ///
  [aw=wgt] , a(year)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"     
  
eststo emp_1:  reghdfe emp_growth PR PR_post  ///
  [aw=wgt] , a(year hqduns)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"       
  
eststo emp_1:  reghdfe emp_growth PR PR_post  ///
if  `if2_`j''  [aw=wgt] , a(year hqduns)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"         
  
eststo emp_1:  reghdfe emp_growth PR PR_post  ///
  [aw=wgt] , a(i.year#i.naic2 hqduns)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"           
  
  
  
eststo emp_1:  reghdfe emp_growth PR  ///
  [aw=wgt] if inrange(year,2004,2008) , a(year hqduns)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"       
  
  eststo emp_1:  reghdfe emp_growth _IyeaXP*  ///
  [aw=wgt] , a(i.year#i.naic2)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"     
