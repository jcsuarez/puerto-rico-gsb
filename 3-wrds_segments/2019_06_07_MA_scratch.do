

after investment_ETR_joint_2019_03_27_ftax_JC_06_06_2019

generate ma_ftax_1 = min(ftax,100) if ftax != . 

generate ma_ftax_1 = max(min(ftax,100),0) if ftax != . 

drop *ma_*
generate ma_ftax_1 = max(ftax,0) if ftax != . 
generate mma_ftax_1 = (ma_ftax_1+L1.ma_ftax)/2
generate mmma_ftax_1 = (ma_ftax_1+L1.ma_ftax+L2.ma_ftax)/3


local if2_1 = "inlist(n2,3,5,6,7,11,12)"	
local j = 1 
local y ma_ftax 
*areg `y' ib(last).year#i.n2 PR PR_post IHS_rev log_tot_emp if pi > 4 & samp1 == 1 , cl(gvkey) a(year)
*xtreg `y' ib(last).year#i.n2 PR PR_post IHS_rev log_tot_emp [aw=DFL]  if  pi > 4 & samp1 == 1  ///
* &  ~inlist(n3,"324","323","337","322","327","336","331","321") & `if2_`j'' , vce(cl gvkey)  fe

reghdfe `y'_1 _IyeaX* PR IHS_rev log_tot_emp if  pi > 4, cl(gvkey)   a(year#n2)
coefplot , keep(_IyeaX*) vertical xline(4.5 , lcolor(black)) yline(0) level(90) ///
	xtitle("Year") graphregion(fcolor(white)) ciopts(recast(rcap)) ///
	ytitle("Effect of Exposure to S936 on ``y'_name'") 
	
reghdfe `y'_1 _IyeaX* PR IHS_rev log_tot_emp i.year [aw=DFL_t] ///
 if  ~inlist(n3,"324","323","337","322","327","336","331","321") & ///
 inlist(n2,3,5,6,7,11,12) & pi > 4, cl(gvkey)   a(i.year#i.n2 gvkey)
local plot_mean = (_b[_IyeaXPR_1991] + _b[_IyeaXPR_1992] + _b[_IyeaXPR_1993] + _b[_IyeaXPR_1994] )/ 4
coefplot , keep(_IyeaX*) vertical xline(4.5 , lcolor(black)) yline(0) level(90) ///
	xtitle("Year") graphregion(fcolor(white)) ciopts(recast(rcap)) ///
	ytitle("Effect of Exposure to S936 on ``y'_name'") 



lincom (_IyeaXPR_1991 )
lincom (_IyeaXPR_1992 )
lincom (_IyeaXPR_1993 )
lincom (_IyeaXPR_1994 )

lincom (_IyeaXPR_1991 +_IyeaXPR_1992)/2
lincom (_IyeaXPR_1991 +_IyeaXPR_1992+_IyeaXPR_1993)/3
lincom (_IyeaXPR_1992 +_IyeaXPR_1993+_IyeaXPR_1994)/3
lincom (_IyeaXPR_1993 +_IyeaXPR_1994)/2
lincom (_IyeaXPR_1996 +_IyeaXPR_1997)/2
lincom (_IyeaXPR_1997 +_IyeaXPR_1998+_IyeaXPR_1999)/3
lincom (_IyeaXPR_1998 +_IyeaXPR_1999+_IyeaXPR_2000)/3
lincom (_IyeaXPR_1999 +_IyeaXPR_2000+_IyeaXPR_2001)/3
lincom (_IyeaXPR_2000 +_IyeaXPR_2001+_IyeaXPR_2002)/3
lincom (_IyeaXPR_2001 +_IyeaXPR_2002+_IyeaXPR_2003)/3
lincom (_IyeaXPR_2002 +_IyeaXPR_2003+_IyeaXPR_2004)/3
lincom (_IyeaXPR_2003 +_IyeaXPR_2004+_IyeaXPR_2005)/3
lincom (_IyeaXPR_2004 +_IyeaXPR_2005+_IyeaXPR_2006)/3
lincom (_IyeaXPR_2005+_IyeaXPR_2006)/2



