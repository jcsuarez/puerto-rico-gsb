* This .do file makes summary statistics of the ETR Data (similar to that used by Dyreng et al.)
*
* Author: Dan Garrett
* Date: 1-6-2018

clear
set more off 

*** Importing long data 
use "$WRDS/ETR_compustat.dta", clear

* sample cleaning from Dyreng et al. **********
// assets <10mill
// nonmissing TXPD and PI 
// nonnegative PI
// (NOT CURRENTLY USED) must have 5 years of data

keep if at > 10
keep if pi != . & txpd != .
keep if pi > 0
drop if logAT == . | rd == . | ads == . | ppe == . | intang == . | capex ==. | spec == . | nol == .

* labeling variables for display
label var PR "Puerto Rico Presence"
label var etr_change "Change in ETR"


local General "PR etr etr_change MNE nol logAT"
local Sales "rd ads"
local Assets "ppe lev spec btd intang"
local Physical "capex"

estpost tabstat `General' `Sales' `Assets' `Physical', statistics(count mean sd p5 p25 p50 p75 p95) ///
	columns(statistics)
est store tempA

estpost tabstat `General' `Sales' `Assets' `Physical' if PR == 1, statistics(count mean sd p5 p25 p50 p75 p95)  ///
	columns(statistics)
est store tempB

esttab tempA using "$output/sum_stats_ETR_20170106_all.tex", replace ///
	refcat(PR "\emph{Firm Year Characteristics}" rd "\emph{Percent of Sales}"  ppe "\emph{Percent of Assets}" capex "\emph{Percent of Physical Capital}", nolab) /// 
	cells("count mean(fmt(3)) sd(fmt(3)) p5(fmt(2)) p25(fmt(2)) p50(fmt(2)) p75(fmt(2)) p95(fmt(2))" ) nonum label nomtitle tex collabels("Count" "Mean" "SD" "$5^{th}$" "$25^{th}$" "$50^{th}$" "75$^{th}$" "$95^{th}$") noobs sfmt(3)

esttab tempB using "$output/sum_stats_ETR_20170106_PR.tex", replace ///
	refcat(PR "\emph{Firm Year Characteristics}" rd "\emph{Percent of Sales}"  ppe "\emph{Percent of Assets}" capex "\emph{Percent of Physical Capital}", nolab) /// 
	cells("count mean(fmt(3)) sd(fmt(3)) p5(fmt(2)) p25(fmt(2)) p50(fmt(2)) p75(fmt(2)) p95(fmt(2))" ) nonum label nomtitle tex collabels("Count" "Mean" "SD" "$5^{th}$" "$25^{th}$" "$50^{th}$" "75$^{th}$" "$95^{th}$") noobs sfmt(3)

******** correlations

estpost cor PR-btd, matrix
est store er_cors

esttab er_cors using "$output/effective_rate_cors.tex", replace not unstack compress noobs label
** book tax gap correlations look weird because it is generally negative (firms pay less tax than statutorily owed)
	

