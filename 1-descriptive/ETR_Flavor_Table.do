* This .do file makes a table of basic stats of major firms using ETR and Segments Compustat data
*
* Author: Dan Garrett
* Date: 1-15-2018

clear
set more off 

*** Importing long data 
use "$WRDS/ETR_compustat_segments.dta", clear

/*
* sample cleaning from Dyreng et al. **********
// assets <10mill
// nonmissing TXPD and PI 
// nonnegative PI
// (NOT CURRENTLY USED) must have 5 years of data

keep if at > 10
keep if pi != . & txpd != .
keep if pi > 0
drop if logAT == . | rd == . | ads == . | ppe == . | intang == . | capex ==. | spec == . | nol == .

*/
* labeling variables for display
label var PR "Puerto Rico Presence"
label var etr_change "Change in ETR"

keep if company == "BAXTER INTERNATIONAL INC" | company == "GENERAL ELECTRIC CO" | ///
company == "JOHNSON & JOHNSON" | company == "ABBOTT LABORATORIES" |  ///
company == "BRISTOL-MYERS SQUIBB CO" | company == "WARNER-LAMBERT CO" |  ///
company == "CITIGROUP" | company == "COCA-COLA CO" | company == "ELI LILLY & CO" | ///
company == "INTEL CORP" | company == "PEPSICO INC"  // Names from both lists that show up as PR for us

* getting average variables in sets of years: 1991-1995 1996-2000 2001-2005
gen part1 = (1990<year)*(year<1996)
gen part2 = (1995<year)*(year<2001)
gen part3 = (2000<year)*(year<2007)

rename capx physical_invest 
rename capex percent_ppe_invest

foreach Q of var emp* etr fs_capx tot_capx physical_invest percent_ppe_invest {
forval i = 1/3 {
gen `Q'_part`i' = `Q' if part`i' == 1
}
}

keep company *_part*
collapse (mean) emp_US_part1-percent_ppe_invest_part3, by(company)

save "$WRDS/ETR_Flavor_table_data.dta", replace
