//// Get PR firm data 
clear all
set more off
ssc install dataout
snapshot erase _all

*** get list of hqduns that actually work by dropping gov't etc... 
use "$output_NETS/pr_extract_est.dta", clear

gen naic4 = floor(naics95/100)
gen naic3 = floor(naics95/1000)
gen naic2 = floor(naics95/10000)

********************
*getting rid of governmental and social non-profit orgs
********************
drop if naic2 == 92 | naic3 == 813
* dropping USPS and the federal government
drop if hqduns95 == 3261245 | hqduns95 == 161906193

* dropping firms with HQ in PR
drop if pr_based == 1 

* dropping firms with no link
drop if pr_link == 0
bysort hqduns: egen temp_max = max(emp95)
gen temp_keep = (emp95 == temp_max)

keep if temp_keep == 1

* making a list of hqduns to use from the panel data
keep hqduns naic3
bysort hqduns: egen m_naic3 = mode(naic3), minmode
drop naic3
duplicates drop 
tempfile tokeep 
save "`tokeep'"

** old code 
use "$output_NETS/pr_extract_panel.dta", clear 

** Keep only firms at start of the sample 
gen temp = emp_PR if year == 1990
bysort hqd: egen base_emp_PR = min(temp)
drop temp 
keep if base > 0  // This line drops 4,991 observations

** MErge in to keep 
merge m:1 hqduns95 using "`tokeep'"
keep if _merge == 3 // This drops 23 observations from the master, 214 from using

egen firm = group(hqd) 
sum firm, d
snapshot save

/*
*/

collapse (sum) emp_US (sum) emp_PR (sum) num_est_PR (sum) num_est_US , by(year) 

tempfile PRdata
save "`PRdata'" 


/// Get control data 
* Get control Data 
use "$output_NETS_control/hq_est_co_firm_master.dta", clear

// Drop Firms accidentally in both 
*merge 1:1 hqduns95 using "$output_NETS/pr_extract_hq_master.dta"
*drop if _merge == 3 & pr_b ==1 
*keep if _merge ==1

*drop if emp95 < 6
*drop if num_est95 < 3

*set seed 12345
*sample 1000, count by(firm_group)
keep if fipsstate95 != . 
drop if fipsstate95 > 56
rename fipsstate95 hqfips
 
gen firm_group2 = floor(firm_group/10000)
gen census_div = floor(firm_group2/1000)
gen naic3 = firm_group2-1000*census_div
drop firm_group2
gen PR = 0 

drop hqfips census_div naic3 PR firm_group 
replace hqduns = 1
collapse (sum) emp* num_* , by(hqduns)

foreach y in 90 91 92 93 94 95 96 97 98 99 00 01 02 03 04 05 06 07 08 09 10 11 12 {
	rename emp`y' emp_`y'
	rename num_est`y' num_`y'
}

reshape long emp_ num_ , i(hqduns) j(year) string
drop hqduns  
destring year , replace 
replace year = year+1900 if year > 50 
replace year = year+2000 if year < 50 
sort year
rename emp_ emp_co
rename num_ num_co

merge 1:1 year using "`PRdata'"
drop _merge 

replace emp_US = emp_US/1000000
replace emp_co = emp_co/1000000

gen rat = emp_co / emp_US if year == 1995 
sum rat 
replace emp_co = emp_co /`r(mean)'


twoway (scatter emp_co year , connect(line) lpattern(dash) lcolor(white) color(white)) ///
		(scatter emp_US year, connect(line) xline(1995, lcolor(black) lpattern(dash))) ///	   
		, plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white)) ///
		xtitle("Year") xlab(1990(5)2011) ylab(5(2)12) ///
		legend(label(2 "S936 Firms (1,000,000) ") label(1 " ")) ///
		ytitle("Total Employment by Firms Exposed to S 936")  
		
		graph export "$output/Graphs/ts_co_1.pdf", replace		

twoway (scatter emp_co year , connect(line) lpattern(dash) ) ///
		(scatter emp_US year, connect(line) xline(1995, lcolor(black) lpattern(dash))) ///
		, plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white)) ///
		xtitle("Year") xlab(1990(5)2011) ylab(5(2)12) ///
		legend(label(2 "S936 Firms (1,000,000) ") label(1 "Non-S936 Firms (1,000,000) ")) ///
		ytitle("Total Employment by Firms Exposed to S 936")  
		
		graph export "$output/Graphs/ts_co_2.pdf", replace				

sum emp_US if year == 1995 
gen base_emp = `r(mean)'

gen diff = (emp_US-emp_co)
gen diff2 = (emp_US-emp_co) /emp_US
gen diff3 = (emp_US-emp_co) /base_emp

// scalars for v21 page 18
do "$analysis/jc_stat_out.ado"
sum diff if year == 2006
local dif1 = round(-r(mean) * 1000, 10) // thousands
jc_stat_out,  number(`dif1') name("RESULTXXIIa") replace(0) deci(0) figs(3)
sum diff if year == 2009
local dif2 = round(-r(mean), .1) // millions
jc_stat_out,  number(`dif2') name("RESULTXXIIb") replace(0) deci(1) figs(3)


/*
twoway (scatter diff2 year, connect(line) xline(1995, lcolor(black) lpattern(dash))) ///
		, yline(0, lcolor(black)) plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white)) ///
		xtitle("Year") xlab(1990(5)2011) ylab(-.3(.05)0) ///
		ytitle("Total Employment by Firms Exposed to S 936")  ///
		note("Employment of `num' Firms Exposed to S 936.")
		
twoway (scatter diff3 year, connect(line) xline(1995, lcolor(black) lpattern(dash))) ///
		, yline(0, lcolor(black)) plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white)) ///
		xtitle("Year") xlab(1990(5)2011) ylab(-.18(.02)0) ///
		ytitle("Total Employment by Firms Exposed to S 936")  ///
		note("Employment of `num' Firms Exposed to S 936.")		
		*/
		
twoway (scatter diff year, connect(line) xline(1995, lcolor(black) lpattern(dash))) ///
		, yline(0, lcolor(black)) plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white)) ///
		xtitle("Year") xlab(1990(5)2011) ylab(-1(.2).2) ///
		ytitle("Jobs Lost by S936 Firms (1,000,000)") 

		graph export "$output/Graphs/ts_co_3.pdf", replace		
		
gen rat2 = num_co / num_est_US if year == 1995 
sum rat2 
replace num_co = num_co /`r(mean)'		
		

gen diff_e = (num_est_US-num_co)
gen diff2_e = (num_est_US-num_co) /num_est_US

twoway (scatter diff2_e year, connect(line) xline(1995, lcolor(black) lpattern(dash))) ///
		, yline(0, lcolor(black)) plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white)) ///
		xtitle("Year") xlab(1990(5)2011) ylab(-.3(.05)0) ///
		ytitle("Total Employment by Firms Exposed to S 936")  ///
		note("Employment of `num' Firms Exposed to S 936.")
		
		
twoway (scatter diff_e year, connect(line) xline(1995, lcolor(black) lpattern(dash))) ///
		, yline(0, lcolor(black)) plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white)) ///
		xtitle("Year") xlab(1990(5)2011) ylab(-1(.2)0) ///
		ytitle("Total Employment by Firms Exposed to S 936")  ///
		note("Employment of `num' Firms Exposed to S 936.")
