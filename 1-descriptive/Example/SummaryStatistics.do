*** Created by: September 8, 2017
*** Updated by: September 8, 2017

********************************************************************************
********************************************************************************
*** This file generate summary statistics for major variables
********************************************************************************
********************************************************************************

clear
set more off
cd "$tablepath"

use "$dtapath/Tables/clean.dta", clear

	****************************************************************************
	*** program to set balanced panel
	****************************************************************************
	capture program drop BalPanel
	program define BalPanel
	version 13.0
	syntax, var(varlist)
	
	keep if ~missing(`var')
	cap drop obs
	by userid, sort: gen obs = _N
	keep if obs == 5
	
	cap drop id
	by userid, sort: gen id = 1 if _n==1
	replace id = sum(id)
	replace id = . if missing(userid)
	xtset id year
	
	end
	
BalPanel, var(year)

********************************************************************************
*** clean sample
********************************************************************************
/// drop firms that changed ownership during 5 years
by userid, sort: gen owner_check = owner2[1]
drop if owner2 != owner_check
drop owner_check
BalPanel, var(year)

/// drop domestic pilot firms/had PT (somehow) from treatment group --> sample for baseline results but not for robustness checks. Should keep these firms in the summary statistics. 
*drop if owner2 == 1 & (pilot2 == 0|yes2 == 1)
*BalPanel, var(year)
********************************************************************************
*** define locals
********************************************************************************
/// generate IHS transformation
gen ihsequipnw = log(invequip + sqrt(invequip^2 + 1))
winsor ihsequipnw, gen(ihsequip) p(.01)
gen ihstotnw = log(invtot + sqrt(invtot^2 + 1))
winsor ihstotnw, gen(ihstot) p(.01)
gen ihsbuildnw = log(invbuild + sqrt(invbuild^2 + 1))
winsor ihsbuildnw, gen(ihsbuild) p(.01)
/// winsorize investment level vars
rename invtot invtotnw
rename invequip invequipnw
rename invbuilding invbuildingnw
winsor invtotnw, gen(invtot) p(.01)
winsor invequipnw, gen(invequip) p(.01)
winsor invbuildingnw, gen(invbuilding) p(.01)
/// winsorize stock value vars
rename fa_nv_e_defl fa_nv_e_defl_nw
rename fa_prod_nv_e_defl fa_prod_nv_e_defl_nw
rename fa_prod_equip_nv_e_defl fa_prod_equip_nv_e_defl_nw
rename fa_prod_building_nv_e_defl fa_prod_building_nv_e_defl_nw
winsor fa_nv_e_defl_nw, g(fa_nv_e_defl) p(.01)
winsor fa_prod_nv_e_defl_nw, g(fa_prod_nv_e_defl) p(.01)
winsor fa_prod_equip_nv_e_defl_nw, g(fa_prod_equip_nv_e_defl) p(.01)
winsor fa_prod_building_nv_e_defl_nw, g(fa_prod_building_nv_e_defl) p(.01)
/// winsorize other characteristics 
rename asset_e asset_e_nw 
rename sales_d_vat1 sales_d_vat1_nw
rename bincome bincome_nw 
rename wage wage_nw
rename bcash_in_cf bcash_in_cf_nw
rename debt debt_nw
rename vat_in vat_in_nw
rename vat_out vat_out_nw
winsor asset_e_nw, g(asset_e) p(.01)
winsor sales_d_vat1_nw, g(sales_d_vat1) p(.01)
winsor bincome_nw, g(bincome) p(.01)
winsor wage_nw, g(wage) p(.01)
winsor bcash_in_cf_nw, g(bcash_in_cf) p(.01)
winsor debt_nw, g(debt) p(.01)
winsor vat_in_nw, g(vat_in) p(.01)
winsor vat_out_nw, g(vat_out) p(.01)

/// winsorize the number of workers
rename wkr wkr_nw
winsor wkr_nw, g(wkr) p(.01)

/// ratios, log investments and ihs investments are already winsorized

/// label vars
label var invtot "Total Investment"
label var rtot1  "Total Investment"
label var ltot   "Total Investment"
label var ihstot "Total Investment"

label var invequip "Equipment Investment"
label var requip1  "Equipment Investment"
label var lequip   "Equipment Investment"
label var ihsequip  "Equipment Investment"

label var invbuilding "Building Investment"
label var rbuild1     "Building Investment"
label var lbuild      "Building Investment"
label var ihsbuild    "Building Investment"

label var fa_nv_e_defl "Fixed Assets"
label var fa_prod_nv_e_defl "Fixed Assets for Prod."
label var fa_prod_equip_nv_e_defl "Equipment"
label var fa_prod_building_nv_e_defl "Buildings"

label var asset_e "Total Assets"
label var sales_d_vat1 "Sales"
label var bincome "Business Income"
label var wage "Wage and Bonus"
label var bcash_in_cf "Business Cash Inflow"
label var debt "Debt"
label var vat_in "Input VAT"
label var vat_out "Output VAT"
label var wkr "\# of Workers"

/// define locals
local varlist1 = "invtot invequip invbuilding" //investment levels 
local varlist2 = "fa_nv_e_defl fa_prod_nv_e_defl fa_prod_equip_nv_e_defl fa_prod_building_nv_e_defl" //capital stock vars
local varlist3 = "asset_e sales_d_vat1 bincome wage bcash_in_cf debt vat_in vat_out" //other characteristics
local varlist4 = "rtot1 requip1 rbuild1 ltot lequip lbuild ihstot ihsequip ihsbuild" //investment ratios and employment (no need to re-scale)
local varlist5 = "wkr"

/// re-scale to millions from thousands
forval i = 1(1)3 {
foreach var in `varlist`i'' {
		replace `var' = `var'/1000
}
}

local varlist = "`varlist1' `varlist4' `varlist2' `varlist3' `varlist5'"
********************************************************************************
*** summarize vars
********************************************************************************
eststo clear

/* version 1: all firms, full distribtion
local tablename "SummaryStats_temp"
eststo sum1: estpost tabstat `varlist1', stat(mean sd p25 p50 p75 N) col(statistics)
esttab sum1 using "`tablename'.tex", replace noobs nomtitle nonumber ///
cells("mean(fmt(%9.3fc)) sd(fmt(%9.3fc)) p25(fmt(%9.3fc)) p50(fmt(%9.3fc)) p75(fmt(%9.3fc)) count(fmt(%9.0fc))")

forval i = 2(1)4 {
	eststo sum`i': estpost tabstat `varlist`i'', stat(mean sd p25 p50 p75 N) col(statistics)
	esttab sum`i' using "`tablename'.tex", append noobs nomtitle nonumber ///
	cells("mean(fmt(%9.3fc)) sd(fmt(%9.3fc)) p25(fmt(%9.3fc)) p50(fmt(%9.3fc)) p75(fmt(%9.3fc)) count(fmt(%9.0fc))")
}
*/

/// version 2: all firms, domestic, foreign, only report mean, std and number of obs
eststo all:      estpost summarize `varlist' 				//all firms 
eststo domestic: estpost summarize `varlist' if owner2 == 1 //domestic firms
eststo foreign:  estpost summarize `varlist' if owner2 == 0 //foreign firms

local tablename "SummaryStats_temp"
esttab all domestic foreign using "`tablename'.tex", replace noobs nomtitle nonumber label ///
cells("mean(pattern(1 1 1) fmt(%9.2fc)) sd(pattern(1 1 1) fmt(%9.2fc)) count(pattern(1 1 1) fmt(%9.0fc))") ///
mgroups("All Firms" "Domestic Firms" "Foreign Firms", pattern(1 1 1) ///
prefix(\multicolumn{@span}{c}{) suffix(})   ///
span erepeat(\cmidrule(lr){@span}))

