** this file does a sample comparison of several characters for both Compustat Sample and NETS samples
** author: dan
** date: 9-24-2018

/* goals:
-make a table showing match of moments for compustat treated and control samples
-make a table showing match og moments for NETS treated and control sampl (matched thing)
*/

* update 2-4-2019: adding DFL weights to the control sample
* update 2-4-2019: making the comparison groups in separate columns instead of rows

clear all
set more off
snapshot erase _all

** starting with the Compustat Sample:
*** Importing long data 
use "$WRDS/ETR_compustat.dta", replace

keep if year == 1995

* labeling variables for display
label var PR "Puerto Rico Presence"

label var etr_change "Change in ETR"
label var txfed "Federal Taxes Paid"
label var MNE "Multinational"
label var nol "Net Operating Loss"
label var logAT "Natural Log of Assets"


label var rd "Research and Development"
label var ads "Advertising"
label var ppe "Plants, Property, and Equipment"
label var lev "Leverage Ratio"
label var btd "Book to Debt Ratio"
label var intang "Intangibles"

label var capex "Capital Expenditures"

local General "etr txfed MNE nol logAT"
local Sales "rd ads"
local Assets "ppe lev btd intang"
local Physical "capex"

local vars "`General' `Sales' `Physical' `Assets'"

* some DFL weights
tostring naics, gen(naic3)
gen n3 = substr(naic3,1,3)
xtile rev = sale if year == 1995 , n(20) 
destring n3, replace
/*
logit PR i.n3

capture: drop phat min_phat w w_phat
predict phat, pr 
winsor phat, p(.01) g(w_phat) 
replace phat = w_phat
bys gvkey: egen min_phat = min(phat) 
* ATE
* gen DFL = (PR/min_phat+(1-PR)/(1-min_phat))
* ATOT
gen DFL = (PR+(1-PR)*min_phat/(1-min_phat))
*/
replace rd = 1 if rd > 1 & rd != .

estpost tabstat `vars' if PR == 1, statistics(mean sd count ) ///
	columns(statistics)
est store tempA

/*
estpost tabstat `vars' [aw=DFL] if PR == 0, statistics(mean sd count ) ///
	columns(statistics)
est store tempB
*/
estpost tabstat `vars' if PR == 0, statistics(mean sd count ) ///
	columns(statistics)
est store tempC

esttab tempA tempC using "$output/Tables/sum_stats_compu_20190221.tex", replace ///
	refcat(etr "\emph{General Characteristics}" rd "\emph{Spending}" ppe "\emph{Capital and Financing}",) /// 
	cells("mean(fmt(3)) sd(fmt(3)) count(fmt(0))" ) nonum label nomtitle tex ///
	collabels("Mean" "SD" "Count" ) noobs mgroups("S936 Firms" "All Control Firms", pattern(1 1 1) ///
prefix(\multicolumn{@span}{c}{) suffix(})   ///
span erepeat(\cmidrule(lr){@span}))
	
******************************************************************
** same thing with nets sample	
******************************************************************
*** get list of hqduns that actually work
use "$output_NETS/pr_extract_est.dta", clear

gen naic4 = floor(naics95/100)
gen naic3 = floor(naics95/1000)
gen naic2 = floor(naics95/10000)

*getting rid of governmental and social non-profit orgs
drop if naic2 == 92 | naic3 == 813
* dropping USPS and the federal government
drop if hqduns95 == 3261245 | hqduns95 == 161906193

* dropping firms with HQ in PR
drop if pr_based == 1 

* dropping county industries with no link
drop if pr_link == 0
bysort hqduns: egen temp_max = max(emp95)
gen temp_keep = (emp95 == temp_max)

keep if temp_keep == 1

* making a list of hqduns to use from the panel data
keep hqduns naic3
bysort hqduns: egen m_naic3 = mode(naic3), minmode
drop naic3
duplicates drop 
tempfile tokeep 
save "`tokeep'"

** old code 
use "$output_NETS/pr_extract_panel.dta", clear 

** Keep only firms at start of the sample 
gen temp = emp_PR if year == 1990
bysort hqd: egen base_emp_PR = min(temp)
drop temp 
keep if base > 0  // This line drops 4,991 observations

** MErge in to keep 
merge m:1 hqduns95 using "`tokeep'"
keep if _merge == 3 // This drops 23 observations from the master, 214 from using

egen firm = group(hqd) 
sum firm, d
snapshot save

keep hqd year emp_US num_est_US m_naic3
gen PR = 1 

tempfile PRdata
save "`PRdata'" 

/// Get control data 
* Get control Data 
use "$output_NETS_control/hq_est_co_firm_master.dta", clear
drop if emp95 < 6
drop if num_est95 < 3

*sample 1000, count by(firm_group)
keep if fipsstate95 != . 
drop if fipsstate95 > 56
rename fipsstate95 hqfips
 
gen firm_group2 = floor(firm_group/10000)
gen census_div = floor(firm_group2/1000)
gen naic3 = firm_group2-1000*census_div
drop firm_group2
gen PR = 0 

rename naic3 m_naic3
drop hqfips census_div  PR firm_group 

foreach y in 90 91 92 93 94 95 96 97 98 99 00 01 02 03 04 05 06 07 08 09 10 11 12 {
	rename emp`y' emp_`y'
	rename num_est`y' num_`y'
}

reshape long emp_ num_ , i(hqduns m_nai) j(year) string
 
destring year , replace 
replace year = year+1900 if year > 50 
replace year = year+2000 if year < 50 
sort year
rename emp_ emp_US
rename num_ num_est_US
gen PR = 0 

append using "`PRdata'"
keep if year == 1995
gsort - PR
duplicates drop hqduns, force
save "`PRdata'", replace

** merging in fake shock
use "$output_NETS_control/pr_extract_est_fake.dta", clear

gen naic4 = floor(naics95/100)
replace naic4 = floor(naics95/10) if naic4 <1111
replace naic4 = floor(naics95) if naic4 <1111 
gen naic3 = floor(naics95/1000)
replace naic3 = floor(naics95/100) if naic3 <111
replace naic3 = floor(naics95/10) if naic3 <111
replace naic3 = floor(naics95) if naic3 <111
gen naic2 = floor(naics95/10000)
replace naic2 = floor(naics95/1000) if naic2 <11
replace naic2 = floor(naics95/100) if naic2 <11
replace naic2 = floor(naics95/10) if naic2 <11
replace naic2 = floor(naics95) if naic2 <11

drop if naic2 == 92 | naic3 == 813
* dropping USPS and federal government
drop if hqduns == 3261245 
* this is navy, drop
drop if hqduns == 161906193
 
keep if pr_link == 1
keep hqduns
duplicates drop
gen fake_link = 1

merge 1:1 hqduns using "`PRdata'"

*** generating the table 
* DFL 
drop if emp_US ==0 
sum emp_US if PR , d
gen wgt=emp_US/( r(mean)) if PR 

sum emp_US if PR==0 , d
replace wgt=emp_US/( r(mean)) if PR==0 

capture: drop q_wgt
gen naic2 = floor(m_nai/10)
xtile q_wgt = wgt, n(20) 
/*
logit PR i.q_wgt#i.naic2

capture: drop phat min_phat w w_phat
predict phat, pr 
winsor phat, p(.01) g(w_phat) 
replace phat = w_phat
bys hqduns: egen min_phat = min(phat) 
* ATE
* gen w = wgt*(PR/min_phat+(1-PR)/(1-min_phat))
* ATOT
gen w = wgt*(PR+(1-PR)*min_phat/(1-min_phat))

gen DFL = w / wgt // not multiplicatively counting employment
*/
label var num_est_US "Establishments"
label var emp_US "Employment"

local vars "emp_US num_est_US"
estpost tabstat `vars' if PR == 1, statistics(mean sd count ) ///
	columns(statistics)
est store tempA
/*
estpost tabstat `vars' [aw=DFL] if PR == 0, statistics(mean sd count ) ///
	columns(statistics)
est store tempB
*/
estpost tabstat `vars' if fake_link == 1, statistics(mean sd count ) ///
	columns(statistics)
est store tempC

esttab tempA tempC using "$output/Tables/sum_stats_nets_20190221.tex", replace ///
	cells("mean(fmt(3)) sd(fmt(3)) count(fmt(0))" ) nonum label nomtitle tex ///
	collabels("Mean" "SD" "Count" ) noobs mgroups("S936 Firms" "Random Control Firm Sample", pattern(1 1 1) ///
prefix(\multicolumn{@span}{c}{) suffix(})   ///
span erepeat(\cmidrule(lr){@span}))





/* 
	
** wide version
local General "etr txfed MNE nol logAT"
local Sales "rd ads"
local Assets "ppe lev spec btd intang"
local Physical "capex"

foreach Q of varlist `General' `Sales' `Assets' `Physical' {
gen `Q'_p = `Q' if PR == 1
replace `Q' = . if PR == 1
label var `Q' "Control"
label var `Q'_p "Treatment"
}

	
	