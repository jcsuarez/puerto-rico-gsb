* 06/2016
* Maps pr link 

clear all
set more off 

// Prep cross walk for conspuma
use "$xwalk/consp2cty9-22-13.dta", clear
gen pid = 1000*state+county
keep conspuma pid 
duplicates drop 
** fixing miami-dade county
replace pid = 12025 if pid == 12086
tempfile cross
save "`cross'"

// Prep cross walk for CZone
use "$xwalk/cw_cty_czone.dta", clear
rename cty_fips pid 
drop cty_l 
duplicates drop 
tempfile cross2
save "`cross2'"

// Get link data 
use "$output_NETS/pr_link_est_county_d.dta", clear 
rename fips95 pid
gen fips_state = floor(pid/1000)
drop if fips_state == 72
** fixing miami-dade county
replace pid = 12025 if pid == 12086

// Merge Cross walks 
merge 1:1 pid using "`cross'"
keep if _merge == 3
drop _merge 

merge 1:1 pid using "`cross2'"
keep if _merge == 3
drop _merge 

// Get Means at State, CZone, and Conspuma 
drop total 
replace pr = pr*100

* State 
preserve 
collapse (mean) pr [fw=cty_pop], by(fips_state) 
rename pr pr_link_state
tempfile link_state
save "`link_state'"
restore 

* Czone 
preserve 
collapse (mean) pr [fw=cty_pop], by(czone) 
rename pr pr_link_czone
tempfile link_czone
save "`link_czone'"
restore 

* Conspuma 
preserve 
collapse (mean) pr [fw=cty_pop], by(conspuma) 
rename pr pr_link_conspuma
tempfile link_conspuma
save "`link_conspuma'"
restore 


merge m:1 fips_state using "`link_state'"
drop _merge 
merge m:1 czone using "`link_czone'"
drop _merge 
merge m:1 conspuma using "`link_conspuma'"
drop _merge 

* Residualized Link 
reg pr_link i.fips_state
predict res_pr_link, res
winsor res_pr_link, gen(w_res_pr_link) p(.01)
egen std_w_res_pr_link = std(w_res_pr_link)

gen county = pid
** fixing miami-dade county
replace county = 12025 if county == 12086
// Make Maps 
maptile pr_link , geo(county1990) stateoutline(linewidthstyle)  nquantiles(5) res(.5)
* fcolor(BuRd)
graph export "$output/Graphs/map_pr_link_d.pdf", replace 
graph export "$output/Graphs/map_pr_link_d.png", replace 

maptile std_w_res_pr_link , geo(county1990) stateoutline(linewidthstyle)  nquantiles(5)   res(.5)
graph export "$output/Graphs/map_pr_link_std_d.pdf", replace 
graph export "$output/Graphs/map_pr_link_std_d.png", replace 

maptile pr_link_state , geo(county1990) stateoutline(linewidthstyle)  nquantiles(5)   res(.5)
graph export "$output/Graphs/map_pr_link_state_d.pdf", replace 
graph export "$output/Graphs/map_pr_link_state_d.png", replace 

maptile pr_link_czone , geo(county1990) stateoutline(linewidthstyle)  nquantiles(5)   res(.5)
graph export "$output/Graphs/map_pr_link_czone_d.pdf", replace 
graph export "$output/Graphs/map_pr_link_czone_d.png", replace 

maptile pr_link_conspuma , geo(county1990) stateoutline(linewidthstyle)  nquantiles(5)   res(.5)
graph export "$output/Graphs/map_pr_link_conspuma_d.pdf", replace 
graph export "$output/Graphs/map_pr_link_conspuma_d.png", replace 



// Make mapd of employment measure of pr link 

use "$output_NETS/pr_link_est_county_emp.dta", clear 
gen county = fips95
** fixing miami-dade county
replace county = 12025 if county == 12086

replace pr_link = pr_emp95 / emp95
replace pr_l = pr_l*100
winsor pr_link, gen(w_pr_link) p(.05)

maptile w_pr_link , geo(county1990) stateoutline(linewidthstyle)  nquantiles(5) res(.5)
* fcolor(BuRd)
graph export "$output/Graphs/map_pr_link_emp_d.pdf", replace 
graph export "$output/Graphs/map_pr_link_emp_d.png", replace 
