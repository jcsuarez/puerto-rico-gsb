/*
** This file estimates the percent of employment from PR firms in NAICS: 
man_comps_elec_ 334
man_mach_et_al_ 332 333 335 339
man_foodbevtob_ 311 312
man_apparel_ 315
man_textile_ 313 314
man_leather_ 316
man_chemicals_ 325
*/
** Author: dan
** date: 10-3-2018

clear all
set more off
snapshot erase _all

** first finding total employment in industries from QCEW:
use "$qcewdata/extract_qcew_1990_2012_naic3.dta", clear
keep if strlen(industry_code) == 3
drop if industry_code == "101" | industry_code == "102"
drop if fips_county == 0 & fips_state != 72
drop if fips_state == 72 & fips_county != 0
keep if own_code == 5		// 0-total covered; 1 Federal; 2 State; 3 Local; 4 Intl Govt; 5 Private.
keep fips_state fips_county year industry_code annual_avg_estabs annual_avg_emplvl total_annual_wages
drop if fips_state == 78

keep if year == 1995
gen high_exp_ind = 0

local indust "334 332 333 335 339 311 312 315 313 316 325"
foreach Q of num `indust' {
replace high_exp_ind = 1 if industry_code == "`Q'"
}

collapse (sum) annual_avg_estabs annual_avg_emplvl total_annual_wages, by(high_exp_ind)

tempfile qcew_emp
save "`qcew_emp'", replace

*** finding NETS employment at the high_exp_ind level
use "$output_NETS/pr_extract_est.dta", clear

drop if pr_based == 1
drop if fips95 > 56999

gen naic4 = floor(naics95/100)
replace naic4 = floor(naics95/10) if naic4 <1111
replace naic4 = floor(naics95) if naic4 <1111 
gen naic3 = floor(naics95/1000)
replace naic3 = floor(naics95/100) if naic3 <111
replace naic3 = floor(naics95/10) if naic3 <111
replace naic3 = floor(naics95) if naic3 <111
gen naic2 = floor(naics95/10000)
replace naic2 = floor(naics95/1000) if naic2 <11
replace naic2 = floor(naics95/100) if naic2 <11
replace naic2 = floor(naics95/10) if naic2 <11
replace naic2 = floor(naics95) if naic2 <11

drop if naic2 == 92 | naic3 == 813
* dropping USPS and federal government
drop if hqduns == 3261245 
* this is navy, drop
drop if hqduns == 161906193

gen high_exp_ind = 0
local indust "334 332 333 335 339 311 312 315 313 316 325"
foreach Q of num `indust' {
replace high_exp_ind = 1 if naic3 == `Q'
}

gen pr_emp = pr_link * emp95

collapse (sum) emp95 pr_emp, by(high_exp_ind)

merge 1:1 high_exp_ind using "`qcew_emp'"

** saving output for use elsewhere
drop _merge
save "$output_NETS/industry_cat_emp_sums", replace

**********************************************************
* Making a second version to mactch IK samples:
**********************************************************
use "$qcewdata/extract_qcew_1990_2012_naic3.dta", clear
keep if strlen(industry_code) == 3
drop if industry_code == "101" | industry_code == "102"
drop if fips_county == 0 & fips_state != 72
drop if fips_state == 72 & fips_county != 0
keep if own_code == 5		// 0-total covered; 1 Federal; 2 State; 3 Local; 4 Intl Govt; 5 Private.
keep fips_state fips_county year industry_code annual_avg_estabs annual_avg_emplvl total_annual_wages
drop if fips_state == 78

keep if year == 1995

collapse (sum) annual_avg_estabs annual_avg_emplvl total_annual_wages, by(industry_code)

tempfile qcew_emp
save "`qcew_emp'", replace

*** finding NETS employment at the high_exp_ind level
use "$output_NETS/pr_extract_est.dta", clear

drop if pr_based == 1
drop if fips95 > 56999

gen naic4 = floor(naics95/100)
replace naic4 = floor(naics95/10) if naic4 <1111
replace naic4 = floor(naics95) if naic4 <1111 
gen naic3 = floor(naics95/1000)
replace naic3 = floor(naics95/100) if naic3 <111
replace naic3 = floor(naics95/10) if naic3 <111
replace naic3 = floor(naics95) if naic3 <111
gen naic2 = floor(naics95/10000)
replace naic2 = floor(naics95/1000) if naic2 <11
replace naic2 = floor(naics95/100) if naic2 <11
replace naic2 = floor(naics95/10) if naic2 <11
replace naic2 = floor(naics95) if naic2 <11

drop if naic2 == 92 | naic3 == 813
* dropping USPS and federal government
drop if hqduns == 3261245 
* this is navy, drop
drop if hqduns == 161906193

** epxanding
gen pr_emp = pr_link * emp95

collapse (sum) emp95 pr_emp, by(naic3)
tostring naic3, gen(industry_code)

merge 1:1 industry_code using "`qcew_emp'"

** saving output for use elsewhere
drop _merge
save "$output_NETS/industry_cat_emp_sums_v2", replace



