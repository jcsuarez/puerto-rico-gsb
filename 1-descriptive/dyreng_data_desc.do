clear
capture log close
set more off
capture close
***************************************************************
//Author: dgg
//Date: 20180418
//Task: merge Dyreng data with compustat data
***************************************************************
snapshot erase _all

/*
The basic goals of this exercise:
-- first question: to what extent are PR firms in other tax havens in 1997 (first year)? Are non-PR firms in other tax havens? 
--second question: are PR firms more likely to open new tax havens after repeal of 936? 
-- third question: if so, where do these firms go? 
*/

use "$additional/Dyreng_Tax_Havens/dyreng_data_merged", replace

/* One option using yearly variation
xi i.year|PR , noomit
drop _IyeaXPR_1995
reghdfe th_mention _IyeaX* PR i.year#(c.totalcount c.ncountries) if year > 1992,  a(i.n3 i.year) cl(n3)

*/

/* Other option using pre-post variation
*/
gen post = year > 1995
xi i.post|PR , noomit
*drop _IyeaXPR_1995
egen ny_FE = group(n3 year)

/*
reghdfe th_mention PR i.year#(c.totalcount c.ncountries) if year > 1992 & year < 1996,  a(i.n3 i.year) cl(n3)
reghdfe th_mention PR i.year#(c.totalcount c.ncountries) if year > 1995,  a(i.n3 i.year) cl(n3)

reghdfe taxhaven PR i.year#(c.totalcount c.ncountries) if year > 1992 & year < 1996,  a(i.n3 i.year) cl(n3)
reghdfe taxhaven PR i.year#(c.totalcount c.ncountries) if year > 1995,  a(i.n3 i.year) cl(n3)*/

est clear
eststo reg_ir_pre: reghdfe ir_mention PR c.totalcount c.ncountries if year > 1992 & year < 1996,  a(i.n3 i.year) cl(ny_FE)
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hasu "Yes"
	estadd local hast "Yes"
eststo reg_ci_pre: reghdfe ci_mention PR c.totalcount c.ncountries if year > 1992 & year < 1996,  a(i.n3 i.year) cl(ny_FE)
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hasu "Yes"
	estadd local hast "Yes"
eststo reg_sw_pre: reghdfe sw_mention PR c.totalcount c.ncountries if year > 1992 & year < 1996,  a(i.n3 i.year) cl(ny_FE)
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hasu "Yes"
	estadd local hast "Yes"
eststo reg_sp_pre: reghdfe sp_mention PR c.totalcount c.ncountries if year > 1992 & year < 1996,  a(i.n3 i.year) cl(ny_FE)
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hasu "Yes"
	estadd local hast "Yes"
eststo reg_ba_pre: reghdfe ba_mention PR c.totalcount c.ncountries if year > 1992 & year < 1996,  a(i.n3 i.year) cl(ny_FE)
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hasu "Yes"
	estadd local hast "Yes"
eststo reg_hk_pre: reghdfe hk_mention PR c.totalcount c.ncountries if year > 1992 & year < 1996,  a(i.n3 i.year) cl(ny_FE)
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hasu "Yes"
	estadd local hast "Yes"

eststo reg_ir_post: reghdfe ir_mention PR c.totalcount c.ncountries if year > 2003 & year < 2009,  a(i.n3 i.year) cl(ny_FE)
eststo reg_ci_post: reghdfe ci_mention PR c.totalcount c.ncountries if year > 2003 & year < 2009,  a(i.n3 i.year) cl(ny_FE)
eststo reg_sw_post: reghdfe sw_mention PR c.totalcount c.ncountries if year > 2003 & year < 2009,  a(i.n3 i.year) cl(ny_FE)
eststo reg_sp_post: reghdfe sp_mention PR c.totalcount c.ncountries if year > 2003 & year < 2009,  a(i.n3 i.year) cl(ny_FE)
eststo reg_ba_post: reghdfe ba_mention PR c.totalcount c.ncountries if year > 2003 & year < 2009,  a(i.n3 i.year) cl(ny_FE)
eststo reg_hk_post: reghdfe hk_mention PR c.totalcount c.ncountries if year > 2003 & year < 2009,  a(i.n3 i.year) cl(ny_FE)


esttab reg_??_pre using "$output/Tables/table_pre_post_tax_haven_presence.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons nonum posthead(" & Ireland & Cayman Islands & Switzerland & Singapore & Barbados & Hong Kong \\ ") ///
prefoot("") ///
postfoot("") ///

esttab reg_??_post  using "$output/Tables/table_pre_post_tax_haven_presence.tex", keep(PR)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s() noobs /// 
coeflabel(PR "\hspace{1em}Exposure to Section 936 " pr_link_all "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{1}{l}{\textbf{Years: 2004-2008}}") postfoot("\hline") append sty(tex)

esttab reg_??_pre using "$output/Tables/table_pre_post_tax_haven_presence.tex", keep(PR)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s()  /// 
coeflabel(PR "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{1}{l}{\textbf{Years: 1993-1995}}") noobs scalars(  "hasy Year Fixed Effects" "hasind Industry Fixed Effects" "hasu Unique Country Count" "hast Total Country Count") label /// 
 prefoot("\hline") ///
postfoot( ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex)	



