// This file estimates semi-elasticities with SOI-estimated tax changes
// includes industry-level variation
// Author: Lysle Boller
// Date: April 25, 2020

clear all
set more off
snapshot erase _all
set seed 12345

********************************************************************************
* Pull in compustat to get gross profit-based size weights for ETR calculation *
********************************************************************************
use "$data/Replication Data/compu_NETS_ETR.dta", clear
*tab year, sum(gp)

*use "$data/Replication Data/R&R2023/compu_NETS_ETR.dta", clear
*tab year, sum(gp)

xtset gvkey year
gsort gvkey year
drop n2
gen n2 = substr(n3,1,2)
merge m:1 n3 using "$output/NAICS_crosswalk"
drop if _merge==2
rename _merge _merge1
rename variable variable_n3 
merge m:m n2 using "$output/NAICS_crosswalk"
drop if _merge==2
drop _merge

// gen flag = variable==variable_n3
// tab flag
// tab flag if _merge1==3
// tab flag if _merge==3

replace variable_n3 = variable if variable_n3==""
drop _merge* variable
rename variable_n3 variable


* weight by variable
local weight_var "gp"
collapse (sum) `weight_var' if year==1995, by(variable PR)
by variable: egen sum_var = sum(`weight_var')
gen indshr = `weight_var' / sum_var



// local weight_var "gp"
// collapse (sum) `weight_var', by(variable PR year)
// by variable: egen sum_var = sum(`weight_var')
// gen indshr = `weight_var' / sum_var



* impute 0 for non-naics data
// by variable: egen cnt = count(PR)
// preserve
// 	tempfile impute
// 	keep if cnt == 1
// 	expand 2, gen(dup)
// 	replace indshr = 0 if dup == 1
// 	drop if dup != 1
// 	drop dup
// 	replace PR = 1
// 	replace cnt = 2
// 	save `impute'
// restore 

* get a full n3 X PR X capex share data
// append using "`impute'"
// drop cnt
gsort variable PR
tab variable PR, summ(indshr)

* Get Aggregate GP
egen gp_agg = sum(gp), by(variable)

* Clean PR shares
drop if missing(variable)
gen temp1 = indshr*(PR)
egen indshr_PR = max(temp1), by(variable)
duplicates drop variable, force
replace indshr = indshr_PR 

keep variable gp_agg indshr
order variable gp_agg indshr
tempfile pr_indshr
save "`pr_indshr'"


***********
* Impute Effective Tax Rates for SOI Industries
***********

* Import SOI data
use "$SOI/aggregate_SOI", clear
/* replace variable = subinstr(variable,"_t_","",.)
replace variable = substr(variable,1,length(variable)-1) if substr(variable,length(variable)-2,length(variable)) == "_" */

drop if variable == "manufact"
drop if variable == "tot_"

gen ETR = Income_Tax_After_Credits / Income_Subject_To_Tax
gen pos_cred = Us_Possessions_Tax_Credit / Income_Subject_To_Tax
replace pos_cred = 0 if pos_cred == .

** Generating weights that are constant over time for each industry based on 1995 levels {
foreach var of var Us_Possessions_Tax_Credit Income_Subject_To_Tax Net_Income Receipts {
bys variable: gen `var'_temp = `var' if inlist(year,1995) //
bys variable: egen `var'_base = mean(`var'_temp)
drop *temp
}

sort variable year
keep if year > 1994 & year < 2008

* renaming variable to be more usable
rename Income_Subject_To_Tax_base ISTT
rename Us_Possessions_Tax_Credit_base USPTC

// **** 0927 merge Share of PR capex by industry ("variable")
merge m:1 variable using "`pr_indshr'", nogen
// gen indshr = .21


* Observed effective tax rates
gen p_data = (Income_Tax_After_Credits + Us_Possessions_Tax_Credit) / (Income_Subject_To_Tax)
replace p_data = (Income_Tax_After_Credits) / (Income_Subject_To_Tax) if p_data == .


* Exposed firms: industry-specific shares
gen p_new = ((Income_Tax_After_Credits + Us_Possessions_Tax_Credit) * indshr - Us_Possessions_Tax_Credit) / (Income_Subject_To_Tax * indshr)
replace p_new = p_data if p_new == .

* Exposed firms: Constant shares
gen p_new_fixp = ((Income_Tax_After_Credits + Us_Possessions_Tax_Credit)*.21 - Us_Possessions_Tax_Credit) / (Income_Subject_To_Tax*.21)
// ISTT
replace p_new_fixp = p_data if p_new_fixp == .


* Industry shares of gross profits (in 1995)
egen gp_all = sum(gp_agg), by(year)
gen gp_share = gp_agg/gp_all

sum gp_share if indshr>0.05 & year==1995
sum gp_share if indshr<=0.05 & year==1995
sum gp_share if indshr==0 & year==1995


* New Figure 1.C *
preserve
	keep if indshr>0.0
	collapse (mean) p_data p_new p_new_fixp [aw=gp_share], by(year) 
	*collapse (mean) p_data p_new p_new_fixp , by(year) 
	keep if year > 1994 & year < 2008
	sort year
	sum p_data if year==1995
	local p1 = r(mean)
	sum p_new if year==1995
	local p2 = r(mean)
	sum p_new_fixp if year==1995
	local p3 = r(mean)
	
	replace p_new = p_new - `p2' + `p1'
	replace p_new_ = p_new_ - `p3' + `p1'

	
	* Record last year
	sum p_data if year==2007
	local p1 = r(mean)
	sum p_new if year==2007
	local p2 = r(mean)
	sum p_new_fixp if year==2007
	local p3 = r(mean)	
	
	twoway (scatter p_data year, c(line) lpattern(dash) ) ///
		   (scatter p_new year, c(line) lpattern(solid)) ///
		   (scatter p_new_ year, c(line) lpattern(solid)) ///
		   , xlab(1995(2)2007) ///
			graphregion(fcolor(white)) bgcolor(white) ///
			xtitle("Year") ylab(0.20(0.02)0.32) ///
			ytitle("Effective Tax Rate for Exposed Industries", margin(medsmall)) ///
			legend(order(3 2 1) pos(6) ///
			label(1 "Unexposed Firms") ///
			label(2 "S936 Firms: Industry Shares") ///
			label(3 "S936 Firms") rows(1) ) 	
graph export "$output/Graphs/kevin/SOI_ETR_Observed_GPwgt.pdf", replace  	 	

	dis round(100*(`p2' - `p1'),.01)
	dis round(100*(`p3' - `p1'),.01)

restore




tempfile soi_data
save "`soi_data'"

***************************************************************
********* Merge with Compustat Data: Set Sample First *********
***************************************************************
use "$data/Replication Data/newNETS_Clean_0923.dta", clear
/*
* Make sample restrictions first *
tab idbflag PR if year==1995
tab fic
drop if PR==0 & idbflag=="D"
*drop if dup_tag>0

xtile PR_sizeDist = ppe95 if PR==1, n(3)
xtile PR_sizeDist10 = ppe95 if PR==1, n(10)

tab PR if year==1995, sum(ppe95)
tab PR_sizeDist if year==1995, sum(ppe95)
drop if PR_sizeDist10<7
*/


***************************************************************
********* Now merge to SOI data to get tax_impute var *********
***************************************************************
tostring m_naic3, gen(n3)
* Encode NAICS2 as a string
tostring naic2, gen(n2string)
drop naic2
rename n2string n2
* Merge in the SOI industry definitions
merge m:1 n3 using "$output/NAICS_crosswalk"
rename _merge _merge_n3
frame create naics2_crosswalk
frame naics2_crosswalk: use "$output/NAICS_crosswalk"
frame naics2_crosswalk: drop if strlen(n3) == 3
frame naics2_crosswalk: tab n2 // Check unique
frlink m:1 n2, frame(naics2_crosswalk)
frget variable_n2 = variable, from(naics2_crosswalk)
replace variable = variable_n2 if variable == ""
drop _merge*
* Merge in the imputed tax rates
merge m:1 year variable using "`soi_data'"

**********************************************************************************
* Fill missing ETRs with earliest and latest available imputed ETR for each firm *
**********************************************************************************

* Drop groups with no match in the SOI data
bys hqduns95: egen has_etr = total(p_data)
keep if has_etr > 0
drop has_etr

* Impute pre-period with earliest tax rate available
foreach var of var p_data p_new p_new_fixp {
gen byte missing_`var' = missing(`var')
bys hqduns95 (missing_`var' year) : gen first_`var' = `var'[1]
bys hqduns95 (year) : gen missing_pre_`var' = sum(1 - missing_`var') == 0
bys hqduns95 (year) : replace `var' = first_`var' if missing_pre_`var' == 1
gen ok = 1 - missing_`var'
bys hqduns95 (ok year) : gen last_`var' = `var'[_N]
bys hqduns95 (year) : replace `var' = last_`var' if `var' == .
drop ok
drop missing_`var'
drop missing_pre_`var'
}

/* * Looks like NAICS code 337 is missing year 1997 for some reason
frame put _all, into(temp)
frame temp: keep if p_data == .
frame temp: desc */

* Compute counterfactual rates
gen tax_impute = p_data
replace tax_impute = p_new if PR
replace tax_impute = tax_impute * 100


gen tax_impute2 = p_data
replace tax_impute2 = p_new_fixp if PR
replace tax_impute2 = tax_impute2 * 100


** below is already implemented in clean_compu_analyis.do - DFL_ppe weight construction
// ********************************************************************************
// ***************************** Export for analysis ******************************
// ********************************************************************************
// * some DFL weights first, then drop (for entropy balancing)
// destring n2, replace
// xtile ppe95_binDFL = ppent if year == 1995 , n(5) 
// logit PR i.n2##i.ppe95_binDFL if year == 1995
// drop phat min_phat w_phat
// predict phat, pr 
// *winsor phat, p(.01) g(w_phat) 
// *replace phat = w_phat
// bys gvkey: egen min_phat = min(phat) 
// * ATOT
// gen DFL2 = (PR+(1-PR)*min_phat/(1-min_phat))


tab year PR, sum(tax_impute)

tostring m_naic3, gen(naic2)
replace naic2 = substr(naic2, 1,2)
replace naic2 = substr(naic2,1,1) if substr(naic2,1,1)=="3"
destring naic2, replace


save "$data/Replication Data/newNETS_SOI_impute_0923", replace


