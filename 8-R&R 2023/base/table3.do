***************************************************************
//Filename: table3_clean.do
//Author: eddie yu
//Date: 2023-09-20
//Task: This script creates data for table3 (Semi-elasticity of Investment w.r.t. ETR - PR_v49.pdf)
//		it semi-elasticities with SOI-estimated tax changes
***************************************************************

/* Data sources:
NETS and Compustat
*/

clear all
set more off
snapshot erase _all

*******************************************************
* Starting with data
*******************************************************
use "$data/Replication Data/R&R2023/table3_data", replace

* formatting, labeling, and descriptive scalar creation
local FE_fs_capx "n3"
local FE_capex_base "gvkey"
local FE_capex_base_p "gvkey"
local FE_capex_base_1 "gvkey"
local FE_ftax "gvkey"

local FE_fs_capx_word "Industry"
local FE_capex_base_word "Firm"
local FE_capex_base_p_word "Firm"
local FE_capex_base_1_word "Firm"
local FE_ftax_word "Firm"

local fs_capx_sum "fs_capx"
local capex_base_sum "IK"
local capex_base_p_sum "IKIK"
local capex_base_1_sum "IK_1"
local ftax_sum "ftax"

local fs_capx_scalars1 ""
local capex_base_scalars1 "avg Sample Average I/K in 2006"
local capex_base_p_scalars1 "avg Sample Average I/K in 2006 Relative to 1995"
local capex_base_1_scalars1 "avg Sample Average I/K in 2006"
local ftax_scalars1 ""

local fs_capx_scalars2 ""
local capex_base_scalars2 "elas Percent of 2006 Average"
local capex_base_p_scalars2 "elas Percent of 2006 Average"
local capex_base_1_scalars2 "elas Percent of 2006 Average"
local ftax_scalars2 ""

local fs_capx_scalars3 ""
local capex_base_scalars3 "elast Semi-elasticity of Investment"
local capex_base_p_scalars3 "elast Semi-elasticity of Investment"
local capex_base_1_scalars3 "elast Semi-elasticity of Investment"
local ftax_scalars3 ""

local fs_capx_scalars4 ""
local capex_base_scalars4 "pp_dif Change in Effective Tax Rate"
local capex_base_p_scalars4 "pp_dif Change in Effective Tax Rate"
local capex_base_1_scalars4 "pp_dif Change in Effective Tax Rate"
local ftax_scalars4 ""

local fs_capx_title "Change in Foreign Share of Investment"
local capex_base_title "Change in Investment: $ \frac{I}{K_{1990-1995}} $"
local capex_base_p_title "Percent Change in Investment: $ \frac{I}{I_{1990-1995}} - 1 $"
local capex_base_1_title "Change in Investment: $ \frac{I}{K_{1990-1995}} $"
local ftax_title "Change in Federal Taxes Paid as a Percent of Pretax Income"
xtset gvkey year

* scalar values from SOI data analysis:
scalar p_dif_v0 = .0572734
scalar p_dif_v1 = .0699663
scalar p_dif_v2 = .0465333
scalar p_dif_v3 = .0601001

xtset gvkey year

*****************************************************
* one step instrument ()
*****************************************************

* one step normalization
replace capex_base = capex_base * 100

est clear
local j = 1
local i = 0
local y = "capex_base"
	local i = `i' + 1
	eststo reg_`y'_`j'_`i': areg `y' ib(last).year tax_impute IHS_rev log_tot_emp, cl(gvkey)   a(year)
	sum ``y'_sum' if e(sample) & year == 2006, d
		estadd local avg = string(r(mean), "%8.3fc")
	est save "$output/R&R2023/ster/base/table3_`i'", replace
	
	local i = `i' + 1
	eststo reg_`y'_`j'_`i': areg `y' ib(last).year#i.n2 tax_impute IHS_rev log_tot_emp, cl(gvkey)   a(year)
	sum ``y'_sum' if e(sample) & year == 2006, d
		estadd local avg = string(r(mean), "%8.3fc")
	est save "$output/R&R2023/ster/base/table3_`i'", replace
	
	local i = `i' + 1
	eststo reg_`y'_`j'_`i': xtreg `y' ib(last).year#i.n2 tax_impute IHS_rev log_tot_emp ,  vce(cl gvkey)  fe
	sum ``y'_sum' if e(sample) & year == 2006, d
		estadd local avg = string(r(mean), "%8.3fc")
	est save "$output/R&R2023/ster/base/table3_`i'", replace
	
	local i = `i' + 1
	eststo reg_`y'_`j'_`i': xtreg `y' ib(last).year#i.n2 tax_impute IHS_rev log_tot_emp if  exp_sec  , vce(cl gvkey)  fe
	sum ``y'_sum' if e(sample) & year == 2006, d
		estadd local avg = string(r(mean), "%8.3fc")
	est save "$output/R&R2023/ster/base/table3_`i'", replace
	
	local i = `i' + 1
	eststo reg_`y'_`j'_`i': xtreg  `y' ib(last).year#i.n2 tax_impute IHS_rev log_tot_emp if  exp_ind  , vce(cl gvkey)  fe
	sum ``y'_sum' if e(sample) & year == 2006, d
		estadd local avg = string(r(mean), "%8.3fc")
	est save "$output/R&R2023/ster/base/table3_`i'", replace
	
	local i = `i' + 1
	eststo reg_`y'_`j'_`i': xtreg  `y' ib(last).year#i.n2 tax_impute IHS_rev log_tot_emp [aw=DFL_ppe]  if exp_ind  , vce(cl gvkey)  fe
	sum ``y'_sum'  if e(sample) & year == 2006, d
		estadd local avg = string(r(mean), "%8.3fc")
	est save "$output/R&R2023/ster/base/table3_`i'", replace

esttab reg_`y'_1_? using "$output/R&R2023/Tables/table3.tex", keep(_cons) stats() ///
b(3) se par label ///
noobs nogap nomtitle tex replace nonum nocons postfoot("") prefoot("") posthead("")

esttab reg_`y'_1_? using "$output/R&R2023/Tables/table3.tex", keep(tax_impute) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) noobs scalars("N Observations" "``y'_scalars1'" ) ///
varlabel(PR_post "\hspace{1em}Exposure to Section 936 X Post" tax_impute "\hspace{1em}Effective Tax Rate")  posth("\hline") ///
preh("\multicolumn{7}{l}{\textbf{``y'_title'}}\\")  postfoot( ///
"\hline Year Fixed Effects    & Y & Y & Y & Y & Y & Y \\ " ///
"NAICS-by-Year Fixed Effects   &  & Y & Y & Y & Y & Y \\ " ///
"`FE_`y'_word' Fixed Effects  &   &  & Y & Y & Y & Y \\ "  ///
"S936 Exposed Sector &  &  & & Y  & Y & Y \\ " ///
"S936 Exposed Industry & & & &  &  Y & Y  \\ " ///
"DFL Weights & & & & & & Y \\  \hline\hline" ///
"\end{tabular} }" ) append sty(tex)

