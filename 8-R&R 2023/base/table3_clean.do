***************************************************************
//Filename: table3_clean.do
//Author: eddie yu
//Date: 2023-09-20
//Task: This script creates data for table3 (Semi-elasticity of Investment w.r.t. ETR - PR_v49.pdf)

***************************************************************

clear all
set more off
snapshot erase _all

*******************************************************
* Starting with SOI tax rates by year from figure 1, panel C
*******************************************************
use "$data/Replication Data/figure1_c_data", replace

* renaming variable to be more usable
rename Income_Subject_To_Tax_base ISTT
rename Us_Possessions_Tax_Credit_base USPTC

** outputting the aggregate figure
local base_link = .0923148 // from NETS, share of US employment at firms with operations in PR
local beta_base "0.816" // from column (3) of Table A1

* share of income subject to tax that is covered by the US Possession tax credit
gen share = USPTC / ISTT

* Counterfactual US possessions tax credit holding share of income constant
gen const_cred = share * Income_Subject_To_Tax

* dollars of lost credits due to the rollback
gen lost_cred = const_cred - Us_Possessions_Tax_Credit

* oberserved effective tax rates
gen p_data = (Income_Tax_After_Credits) / Income_Subject_To_Tax

* Predicted tax rates had S936 not been repealed, correcting for 20% adjustment to other credits
gen p_new = (Income_Tax_After_Credits * `base_link' - `beta_base' * lost_cred ) / (Income_Subject_To_Tax * `base_link')

* relative increase in taxes for PR firms with rollback
gen p_dif = p_data - p_new

keep year p_data p_dif
*******************************************************
* next using the data from table 2
*******************************************************
merge 1:m year using "$data/Replication Data/R&R2023/table2_data"
replace p_data = 0.2761941 if year < 1995
replace p_dif = 0 if year < 1995
drop _merge

gen tax_impute = p_data
replace tax_impute = tax_impute + p_dif if year > 1995 & PR
replace tax_impute = tax_impute * 100

* pulling in tax variation
merge 1:1 gvkey year using "$data/Replication Data/R&R2023/table4_data"
drop _merge

save "$data/Replication Data/R&R2023/table3_data", replace
