***************************************************************
//Filename: figure5_clean.do
//Author: eddie yu
//Date: 2023-09-20
//Task: Given size-cutoff data of compu_NETS_ETR_sizecutoff.dta,
//		this script produces dataset figure5_data, figure6_data,
//		figureA4_data, figureA5_data, figureA6_data, figureA7_data.
***************************************************************


/* Data sources:
-Compustat
-NETS to determine PR firms
*/

clear all
set more off 
snapshot erase _all

************************************
* Grabbing and organizing the data
************************************
use "$data/Replication Data/compu_NETS_ETR_sizecutoff.dta", clear 

* actual dataset for regressions
snapshot save

keep gvkey capex_base   year n2 PR IHS_rev log_tot_emp DFL_ppe ///
ppe95_bins10 _webal exp_sec exp_ind
save "$data/Replication Data/R&R2023/figure5_data", replace

* Save figure6_data with additional controls
snapshot restore 1
keep gvkey fs_capx year n2 PR IHS_rev log_tot_emp DFL_ppe ///
ppe95_bins10 _webal exp_sec exp_ind
save "$data/Replication Data/R&R2023/figure6_data", replace

snapshot restore 1
keep gvkey capex_base capex_base_p capex_base_1  year n2 PR IHS_rev log_tot_emp ///
IK IK_1 IKIK DFL_ppe ppe95_bins10 _webal exp_sec exp_ind
save "$data/Replication Data/R&R2023/figureA4_data", replace

snapshot restore 1
keep gvkey capex_base capex_base_p capex_base_1  year n2 PR IHS_rev log_tot_emp /// 
IK IK_1 IKIK DFL_ppe ppe95_bins10 _webal exp_sec exp_ind
save "$data/Replication Data/R&R2023/figureA4_data", replace

snapshot restore 1
keep gvkey capex_base_p capex_base_1  year n2 PR IHS_rev log_tot_emp DFL_ppe ///
ppe95_bins10 _webal exp_sec exp_ind
save "$data/Replication Data/R&R2023/figureA5_data", replace

snapshot restore 1
keep gvkey capex_base_1  year n2 PR IHS_rev log_tot_emp DFL_ppe ///
ppe95_bins10 _webal exp_sec exp_ind
save "$data/Replication Data/R&R2023/figureA6_data", replace

snapshot restore 1
keep gvkey capex_base capex_base_p capex_base_1  year n2 PR IHS_rev log_tot_emp DFL_ppe n3 ///
ppe95_bins10 _webal exp_sec exp_ind
save "$data/Replication Data/R&R2023/figureA7_data", replace
