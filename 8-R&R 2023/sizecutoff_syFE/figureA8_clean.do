***************************************************************
//Filename: figureA8_clean.do
//Author: eddie yu
//Date: 2023-09-20
//Task: Given size-cutoff data of compu_NETS_ETR_sizecutoff.dta,
//		this script produces dataset figureA8_data for running Figure A8.
***************************************************************


/* Data sources:
-Compustat
-NETS to determine PR firms
*/

* starting point for all panels is 3-wrds_segments/investment_ETR_joint_2019_06_16

clear all
set more off 
snapshot erase _all

************************************
* Grabbing and organizing the data
************************************
use "$data/Replication Data/compu_NETS_ETR_sizecutoff.dta", clear 

** NEED TO IMPLEMENT LATER!
