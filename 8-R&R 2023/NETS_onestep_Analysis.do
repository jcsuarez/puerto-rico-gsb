// This file recreates Figure 5
// Author: Dan Garrett
// Date: 3-31-2020

/* Data sources:
various sources including NETS and compustat
*/

clear all
set more off
snapshot erase _all 

*** *** *** *** ***  *** *** ***  *** *** *** *** *** ***
*** *** *** Section (2): Baseline Regressions *** *** ***
*** *** *** *** ***  *** *** ***  *** *** *** *** *** ***
use "$data/Replication Data/newNETS_SOI_impute_0923", clear

preserve
	collapse (mean) tax_impute, by(PR year) 
	keep if year > 1994 & year < 2008
	sort year

	sum tax_impute if year==1995 & PR==0
	local p1 = r(mean)
	sum tax_impute if year==1995 & PR==1
	local p2 = r(mean)
	
	replace tax_impute = tax_impute - `p2' + `p1' if PR==1
	
	twoway (scatter tax_impute year if PR==0, c(line) lpattern(dash) ) ///
		   (scatter tax_impute year if PR==1, c(line) lpattern(solid)) ///
		   , xlab(1995(2)2007) ///
			graphregion(fcolor(white)) bgcolor(white) ///
			xtitle("Year") ///
			ytitle("Effective Tax Rate for Exposed Industries", margin(medsmall)) ///
			legend(order(2 1 ) pos(6) label(1 "Control firm ETR") label(2 "PR firm ETR")  rows(2) ) 	
restore

preserve
	collapse (mean) tax_impute2, by(PR year) 
	keep if year > 1994 & year < 2008
	sort year
	
	sum tax_impute if year==1995 & PR==0
	local p1 = r(mean)
	sum tax_impute if year==1995 & PR==1
	local p2 = r(mean)
	
	replace tax_impute = tax_impute - `p2' + `p1' if PR==1
	
	twoway (scatter tax_impute year if PR==0, c(line) lpattern(dash) ) ///
		   (scatter tax_impute year if PR==1, c(line) lpattern(solid)) ///
		   , xlab(1995(2)2007) ///
			graphregion(fcolor(white)) bgcolor(white) ///
			xtitle("Year") ///
			ytitle("Effective Tax Rate for Exposed Industries", margin(medsmall)) ///
			legend(order(2 1 ) pos(6) label(1 "Control firm ETR") label(2 "PR firm ETR")  rows(2) ) 	
restore

*keep if emp_bins3 == 3


** Now do DFL
xtile binDFL = base_emp if year == 1995 , n(20) 
logit PR i.naic2##i.binDFL if year == 1995
capture: drop phat min_phat w w_phat DFL?
predict phat, pr 
*winsor phat, p(.01) g(w_phat) 
*replace phat = w_phat
bys hqduns95: egen min_phat = min(phat) 
* ATOT
gen DFL2 = wgt*(PR+(1-PR)*min_phat/(1-min_phat))



tab binDFL PR if year==1995, sum(base_emp)

capture: drop pre post? ET_*
gen pre = (year<1995)
gen post1 = (year>1995)*(year<=2003)
gen post2 = (year>2003)
*(year<=2006)
*gen post3 = (year>2006)


foreach var in pre post1 post2  {
	gen ET_`var' = (`var'==1)*PR
}



global level "PR"
global spec1 " [aw=wgt],vce(cluster hqduns) nocon a($level year)"
global spec2 " [aw=wgt],vce(cluster hqduns) nocon a($level naic2##year)"
global spec3 "if major_sec [aw=wgt],vce(cluster hqduns) nocon a($level naic2##year)"
global spec4 "if major_ind [aw=wgt],vce(cluster hqduns) nocon a($level naic2##year)"
global spec5 "if major_ind [aw=DFL2], vce(cluster hqduns) nocon a($level naic2##year)"



global specname1 "Baseline"
global specname2 "NAIC2"
global specname3 "Exp. Sector"
global specname4 "Exp. Industry"
global specname5 "Exp. Industry + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"



capture: drop emp_growth_w
winsor2 emp_growth, by(PR year) cuts(10 100)

capture: drop emp_growth100
gen emp_growth100 = 100*emp_growth

foreach spec in spec1 spec2 spec3 spec4 spec5 {
	*eststo `spec'_sto1: qui reghdfe emp_growth100 ET_* PR $`spec'
	eststo `spec'_sto2: qui reghdfe tax_impute ET_* PR $`spec'
	eststo `spec'_sto3: qui reghdfe tax_impute2 ET_* PR $`spec'

	*eststo `spec'_sto3: qui reghdfe emp_growth100 tax_impute $`spec'
}

*** "Industry-specific rho weight" ***
esttab spec?_sto2, keep(ET_post2) cells(b p)
*** "Constant rho weight" ***
esttab spec?_sto3, keep(ET_post2) cells(b p)



* PR-by-year instruments
local dep1 = "(tax_impute = PR##i.year)"
local dep2 = "(tax_impute2 = PR##i.year)"
foreach spec in spec1 spec2 spec3 spec4 spec5 {
	eststo `spec'_sto4: qui ivreghdfe emp_growth100 `dep1' $`spec' 
	eststo `spec'_sto5: qui ivreghdfe emp_growth100 `dep2' $`spec' 
}

*** "Industry-specific rho weight" ***
esttab spec?_sto4, keep(tax_impute) cells(b p)
*** "Constant rho weight" ***
esttab spec?_sto5, keep(tax_impute2) cells(b p)


* PR-by-post instruments ( PR##i.post3 )
*drop if year>2006
local dep1 = "(tax_impute = PR##i.post2) PR##i.post1 PR##i.pre"
local dep2 = "(tax_impute2 = PR##i.post2) PR##i.post1 PR##i.pre"
foreach spec in spec1 spec2 spec3 spec4 spec5 {
	eststo `spec'_sto4: qui ivreghdfe emp_growth100 `dep1' $`spec' 
	eststo `spec'_sto5: qui ivreghdfe emp_growth100 `dep2' $`spec' 
}

*** "Industry-specific rho weight" ***
esttab spec?_sto4, keep(tax_impute) cells(b p)
*** "Constant rho weight" ***
esttab spec?_sto5, keep(tax_impute2) cells(b p)


