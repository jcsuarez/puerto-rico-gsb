global ster "/Users/jisangyu/JC's Data Emporium Dropbox/Eddie Yu/Puerto Rico/Programs/output/R&R2023/ster"
global overleaf "/Users/jisangyu/JC's Data Emporium Dropbox/Eddie Yu/앱/Overleaf/PR table/tables"
******************************** Set Directory *********************************

foreach path in base_nocontrol { // base base_nocontrol sizecutoff_syFE sizecutoff_syFirmFE
	cd "/Users/jisangyu/JC's Data Emporium Dropbox/Eddie Yu/Puerto Rico/Programs/output/R&R2023/ster/`path'"
	eststo clear

	******************************** Load Estimates ********************************
	*** Call the non-SOI tables ***
	foreach i in "table4" "table5" "tableA8" "tableA9" { // "table2" "tableA5" "tableA6" "table4" "table5" "SOI_PR_by_year" "SOI_longdiff" "table3" "IHS_ftax" "tableA8" "tableA9" "IHS_gtax"
		forval j = 1/6 {
			estimates use `i'_`j'
			estimates store `i'_spec`j'
		}
	}

// 	*** Call these separately to shorten the names ***
// 	forval j = 1/7 {
// 			*** SOI_PR_by_year_instruments_positive_ETR ***
// 			estimates use SOI_PR_by_year_instruments_positive_ETR_`j'
// 			estimates store SOI_year_ins_pos_spec`j'
// 			*** SOI_longdiff_positive_ETR ***
// 			estimates use SOI_longdiff_positive_ETR_`j'
// 			estimates store SOI_longdiff_pos_spec`j'
// 	}

	********************************* Make Tables **********************************
	*** First Table *** 
// 	esttab table2_spec* using "$ster/table_`path'.tex", keep(PR_post2) /// 
// 		cells(b(fmt(3) star) se(par) p) ///
// 		coeflabel(PR_post2 "Investment-to-Capital Ratio (Table 2)") /// 
// 		nogap nomtitle tex wrap replace label nocons collabels(none) varwidth(32) nomtitle ///
// 		star(* .1 ** .05 *** .01)      /// 
// 		prefoot("") postfoot("")
//
// 	esttab tableA5_spec* using "$ster/table_`path'.tex", keep(PR_post2) ///  
// 		cells(b(fmt(3) star) se(par) p) ///
// 		coeflabel(PR_post2 "Percent Change in Investment (Table A.5)") /// 
// 		tex wrap append label mlab(none) collabels(none) nonum varwidth(32) nomtitle /// 
// 		star(* .1 ** .05 *** .01)      /// 
// 		prefoot("") postfoot("") preh("") 
//
// 	esttab tableA6_spec* using "$ster/table_`path'.tex", keep(PR_post2) ///  
// 		cells(b(fmt(3) star) se(par) p) ///
// 		coeflabel(PR_post2 "Investment-to-Capital Ratio, Winsorized at 1\% (Table A.6)") /// 
// 		tex wrap append mlab(none) label  collabels(none) nonum  varwidth(32) nomtitle /// 
// 		star(* .1 ** .05 *** .01)      /// 
// 		prefoot("") postfoot("") preh("") 
		
	esttab table4_spec* using "$overleaf/table_`path'.tex", keep(PR_post) ///  
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(PR_post "Federal Taxes Paid as a Percent of Global Pretax Income (Table 4)") /// 
		nogap nomtitle tex wrap replace label nocons collabels(none) varwidth(32) nomtitle ///
		star(* .1 ** .05 *** .01)      /// 
		prefoot("") postfoot("") p

	esttab table5_spec* using "$overleaf/table_`path'.tex", keep(PR_post) ///  
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(PR_post "Foreign Share of Investment (Table 5)") /// 
		tex wrap append mlab(none) label collabels(none) nonum varwidth(32) nomtitle /// 
		star(* .1 ** .05 *** .01) prefoot("") /// 
		preh("") postfoot("\hline"  /// 
		"Year Fixed Effects         		& Y & Y & Y & Y & Y & Y  \\    "      /// 
		"NAICS-by-Year Fixed Effects        &   & Y & Y & Y & Y & Y  \\    "      /// 
		"Industry Fixed Effects				&   &   & Y & Y & Y & Y  \\    "      /// 
		"S936 Exposed Sector                &   &   &   & Y & Y & Y  \\    "      /// 
		"S936 Exposed Industry              &   &   &   &   & Y & Y  \\    "      /// 
		"DFL Weights						&   &   &   &   &   & Y  \\  \hline\hline" /// 
		"\\ " /// 
		"\end{tabular} }" )

	
	*** Second Table ***
// 	esttab table3_spec* using "$ster/table_tax_`path'.tex", keep(tax_impute) ///  
// 		cells(b(fmt(3) star) se(par) p) ///
// 		coeflabel(tax_impute "Semi-Elasticity of Investment w.r.t. ETR (SOI) (Table 3)") /// 
// 		nogap nomtitle tex wrap replace label nocons collabels(none) varwidth(32) nomtitle ///
// 		star(* .1 ** .05 *** .01)      /// 
// 		prefoot("") postfoot("")
//		
// 	esttab SOI_PR_by_year_spec* using "$ster/table_tax_`path'.tex", keep(tax_impute) ///  
// 		cells(b(fmt(3) star) se(par) p) ///
// 		coeflabel(tax_impute "PR-by-Year Instruments") /// 
// 		tex wrap append label mlab(none) collabels(none) nonum varwidth(32) nomtitle /// 
// 		star(* .1 ** .05 *** .01)      /// 
// 		prefoot("") postfoot("") preh("") 
//		
// 	esttab SOI_year_ins_pos_spec* using "$ster/table_tax_`path'.tex", keep(tax_impute) ///  
// 		cells(b(fmt(3) star) se(par) p) ///
// 		coeflabel(tax_impute "PR-by-Year Instruments (Positive ETR)") /// 
// 		tex wrap append label mlab(none) collabels(none) nonum varwidth(32) nomtitle /// 
// 		star(* .1 ** .05 *** .01)      /// 
// 		prefoot("") postfoot("") preh("") 
//		
// 	esttab SOI_longdiff_spec* using "$ster/table_tax_`path'.tex", keep(_nl_1) ///  
// 		cells(b(fmt(3) star) se(par) p) ///
// 		coeflabel(_nl_1  "SOI Long Difference 2SLS") /// 
// 		tex wrap append label mlab(none) collabels(none) nonum varwidth(32) nomtitle /// 
// 		star(* .1 ** .05 *** .01)      /// 
// 		prefoot("") postfoot("") preh("") 
//		
// 	esttab SOI_longdiff_pos_spec* using "$ster/table_tax_`path'.tex", keep(_nl_1) ///  
// 		cells(b(fmt(3) star) se(par) p) ///
// 		coeflabel(_nl_1  "SOI Long Difference 2SLS (Positive ETR)") /// 
// 		tex wrap append label mlab(none) collabels(none) nonum varwidth(32) nomtitle /// 
// 		star(* .1 ** .05 *** .01)      /// 
// 		prefoot("") postfoot("") preh("") 
		
	esttab tableA8_spec* using "$overleaf/table_tax_`path'.tex", keep(PR_post) ///  
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(PR_post "Federal Tax Paid as Percent of Pretax Income (Table A.8)") /// 
		nogap nomtitle tex wrap replace label nocons collabels(none) varwidth(32) nomtitle ///
		star(* .1 ** .05 *** .01)      /// 
		prefoot("") postfoot("")
		
	esttab tableA9_spec* using "$overleaf/table_tax_`path'.tex", keep(PR_post) ///  
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(PR_post "Change in Total Effective Tax Rate (Table A.9)") /// 
		tex wrap append label mlab(none) collabels(none) nonum varwidth(32) nomtitle /// 
		star(* .1 ** .05 *** .01) prefoot("")  /// 
		preh("") postfoot("\hline"  /// 
		"Year Fixed Effects         		& Y & Y & Y & Y & Y & Y  \\    "      /// 
		"NAICS-by-Year Fixed Effects        &   & Y & Y & Y & Y & Y  \\    "      /// 
		"Industry Fixed Effects				&   &   & Y & Y & Y & Y  \\    "      /// 
		"S936 Exposed Sector                &   &   &   & Y & Y & Y  \\    "      /// 
		"S936 Exposed Industry              &   &   &   &   & Y & Y  \\    "      /// 
		"DFL Weights						&   &   &   &   &   & Y  \\  \hline\hline" /// 
		"\\ " /// 
		"\end{tabular} }" )
// 		prefoot("") postfoot("") preh("") 
//
// 	esttab IHS_ftax_spec* using "$ster/table_tax_`path'.tex", keep(PR_post2) ///  
// 		cells(b(fmt(3) star) se(par) p) ///
// 		coeflabel(PR_post2 "IHS Federal Tax Level") /// 
// 		tex wrap append label mlab(none) collabels(none)nonum varwidth(32) nomtitle /// 
// 		star(* .1 ** .05 *** .01)      /// 
// 		prefoot("") postfoot("") preh("") 
//		
// 	esttab IHS_gtax_spec* using "$ster/table_tax_`path'.tex", keep(PR_post2) /// 
// 		cells(b(fmt(3) star) se(par) p) ///
// 		coeflabel(PR_post2 "IHS Global Tax Level") /// 
// 		tex wrap append label mlab(none)  collabels(none) nonum  varwidth(32) nomtitle /// 
// 		star(* .1 ** .05 *** .01) prefoot("") /// 
// 		preh("") postfoot("\hline"  /// 
// 		"Size-by-Year Fixed Effects         & Y & Y & Y & Y & Y & Y & Y \\    "      /// 
// 		"Firm Fixed Effects                 &   & Y & Y & Y & Y & Y & Y \\    "      /// 
// 		"S936 Exposed Sector                &   &   & Y & Y &   &   &   \\    "      /// 
// 		"S936 Exposed Industry              &   &   &   & Y &   &   &   \\    "      /// 
// 		"Size-by-Industry-by-Year FE        &   &   &   &   & Y & Y & Y \\    "      /// 
// 		"DFL Weights				        &   &   &   &   &   & Y &   \\    "      /// 
// 		"Entropy Balancing Weights          &   &   &   &   &   &   & Y \\    \hline\hline" /// 
// 		"\\ " /// 
// 		"\end{tabular} }" )
}


foreach path in sizecutoff_syFirmFE { // base base_nocontrol sizecutoff_syFE sizecutoff_syFirmFE
	cd "/Users/jisangyu/JC's Data Emporium Dropbox/Eddie Yu/Puerto Rico/Programs/output/R&R2023/ster/`path'"
	eststo clear

	******************************** Load Estimates ********************************
	*** Call the non-SOI tables ***
	foreach i in "table2" "tableA5" "tableA6" "table4" "table5" "SOI_PR_by_year" "SOI_longdiff" "table3" "IHS_ftax" "tableA8" "tableA9" "IHS_gtax"  { 
		forval j = 1/7 {
			estimates use `i'_`j'
			estimates store `i'_spec`j'
		}
	}

	*** Call these separately to shorten the names ***
	forval j = 1/7 {
			*** SOI_PR_by_year_instruments_positive_ETR ***
			estimates use SOI_PR_by_year_instruments_positive_ETR_`j'
			estimates store SOI_year_ins_pos_spec`j'
			*** SOI_longdiff_positive_ETR ***
			estimates use SOI_longdiff_positive_ETR_`j'
			estimates store SOI_longdiff_pos_spec`j'
	}

	********************************* Make Tables **********************************
	*** First Table *** 
	esttab table2_spec* using "$overleaf/table_`path'.tex", keep(PR_post2) /// 
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(PR_post2 "Investment-to-Capital Ratio (Table 2)") /// 
		nogap nomtitle tex wrap replace label nocons collabels(none) varwidth(32) nomtitle ///
		star(* .1 ** .05 *** .01)      /// 
		prefoot("") postfoot("")

	esttab tableA5_spec* using "$overleaf/table_`path'.tex", keep(PR_post2) ///  
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(PR_post2 "Percent Change in Investment (Table A.5)") /// 
		tex wrap append label mlab(none) collabels(none) nonum varwidth(32) nomtitle /// 
		star(* .1 ** .05 *** .01)      /// 
		prefoot("") postfoot("") preh("") 

	esttab tableA6_spec* using "$overleaf/table_`path'.tex", keep(PR_post2) ///  
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(PR_post2 "Investment-to-Capital Ratio, Winsorized at 1\% (Table A.6)") /// 
		tex wrap append mlab(none) label  collabels(none) nonum  varwidth(32) nomtitle /// 
		star(* .1 ** .05 *** .01)      /// 
		prefoot("") postfoot("") preh("") 
		
	esttab table4_spec* using "$overleaf/table_`path'.tex", keep(PR_post2) ///  
		cells(b(fmt(3) star) se(par) p) ///
		tex wrap append label mlab(none) collabels(none) nonum varwidth(32) nomtitle /// 
		coeflabel(PR_post2 "Federal Taxes Paid as a Percent of Global Pretax Income (Table 4)") /// 
		star(* .1 ** .05 *** .01)      /// 
		prefoot("") postfoot("") preh("") 

	esttab table5_spec* using "$overleaf/table_`path'.tex", keep(PR_post2) ///  
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(PR_post2 "Foreign Share of Investment (Table 5)") /// 
		tex wrap append mlab(none) label collabels(none) nonum varwidth(32) nomtitle /// 
		star(* .1 ** .05 *** .01) prefoot("") /// 
		preh("") postfoot("\hline"  /// 
		"Size-by-Year Fixed Effects         & Y & Y & Y & Y & Y & Y & Y \\    "      /// 
		"Firm Fixed Effects                 &   & Y & Y & Y & Y & Y & Y \\    "      /// 
		"S936 Exposed Sector                &   &   & Y & Y &   &   &   \\    "      /// 
		"S936 Exposed Industry              &   &   &   & Y &   &   &   \\    "      /// 
		"Size-by-Industry-by-Year FE        &   &   &   &   & Y & Y & Y \\    "      /// 
		"DFL Weights				        &   &   &   &   &   & Y &   \\    "      /// 
		"Entropy Balancing Weights          &   &   &   &   &   &   & Y \\    \hline\hline" /// 
		"\\ " /// 
		"\end{tabular} }" )

	
	*** Second Table ***
	esttab table3_spec* using "$overleaf/table_tax_`path'.tex", keep(tax_impute) ///  
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(tax_impute "Semi-Elasticity of Investment w.r.t. ETR (SOI) (Table 3)") /// 
		nogap nomtitle tex wrap replace label nocons collabels(none) varwidth(32) nomtitle ///
		star(* .1 ** .05 *** .01)      /// 
		prefoot("") postfoot("")
		
	esttab SOI_PR_by_year_spec* using "$overleaf/table_tax_`path'.tex", keep(tax_impute) ///  
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(tax_impute "PR-by-Year Instruments") /// 
		tex wrap append label mlab(none) collabels(none) nonum varwidth(32) nomtitle /// 
		star(* .1 ** .05 *** .01)      /// 
		prefoot("") postfoot("") preh("") 
		
	esttab SOI_year_ins_pos_spec* using "$overleaf/table_tax_`path'.tex", keep(tax_impute) ///  
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(tax_impute "PR-by-Year Instruments (Positive ETR)") /// 
		tex wrap append label mlab(none) collabels(none) nonum varwidth(32) nomtitle /// 
		star(* .1 ** .05 *** .01)      /// 
		prefoot("") postfoot("") preh("") 
		
	esttab SOI_longdiff_spec* using "$overleaf/table_tax_`path'.tex", keep(_nl_1) ///  
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(_nl_1  "SOI Long Difference 2SLS") /// 
		tex wrap append label mlab(none) collabels(none) nonum varwidth(32) nomtitle /// 
		star(* .1 ** .05 *** .01)      /// 
		prefoot("") postfoot("") preh("") 
		
	esttab SOI_longdiff_pos_spec* using "$overleaf/table_tax_`path'.tex", keep(_nl_1) ///  
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(_nl_1  "SOI Long Difference 2SLS (Positive ETR)") /// 
		tex wrap append label mlab(none) collabels(none) nonum varwidth(32) nomtitle /// 
		star(* .1 ** .05 *** .01)      /// 
		prefoot("") postfoot("") preh("") 
		
	esttab tableA8_spec* using "$overleaf/table_tax_`path'.tex", keep(PR_post) ///  
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(PR_post "Federal Tax Paid as Percent of Pretax Income (Table A.8)") /// 
		tex wrap append label mlab(none) collabels(none) nonum varwidth(32) nomtitle /// 
		star(* .1 ** .05 *** .01)      /// 
		prefoot("") postfoot("") preh("") 
		
	esttab tableA9_spec* using "$overleaf/table_tax_`path'.tex", keep(PR_post) ///  
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(PR_post "Change in Total Effective Tax Rate (Table A.9)") /// 
		tex wrap append label mlab(none) collabels(none) nonum varwidth(32) nomtitle /// 
		star(* .1 ** .05 *** .01)      /// 
		prefoot("") postfoot("") preh("") 

	esttab IHS_ftax_spec* using "$overleaf/table_tax_`path'.tex", keep(PR_post2) ///  
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(PR_post2 "IHS Federal Tax Level") /// 
		tex wrap append label mlab(none) collabels(none)nonum varwidth(32) nomtitle /// 
		star(* .1 ** .05 *** .01)      /// 
		prefoot("") postfoot("") preh("") 
		
	esttab IHS_gtax_spec* using "$overleaf/table_tax_`path'.tex", keep(PR_post2) /// 
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(PR_post2 "IHS Global Tax Level") /// 
		tex wrap append label mlab(none)  collabels(none) nonum  varwidth(32) nomtitle /// 
		star(* .1 ** .05 *** .01) prefoot("") /// 
		preh("") postfoot("\hline"  /// 
		"Size-by-Year Fixed Effects         & Y & Y & Y & Y & Y & Y & Y \\    "      /// 
		"Firm Fixed Effects                 &   & Y & Y & Y & Y & Y & Y \\    "      /// 
		"S936 Exposed Sector                &   &   & Y & Y &   &   &   \\    "      /// 
		"S936 Exposed Industry              &   &   &   & Y &   &   &   \\    "      /// 
		"Size-by-Industry-by-Year FE        &   &   &   &   & Y & Y & Y \\    "      /// 
		"DFL Weights				        &   &   &   &   &   & Y &   \\    "      /// 
		"Entropy Balancing Weights          &   &   &   &   &   &   & Y \\    \hline\hline" /// 
		"\\ " /// 
		"\end{tabular} }" )
}
