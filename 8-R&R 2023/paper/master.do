clear all
set more off
snapshot erase _all 

global dropbox "/Users/jisangyu/JC's Data Emporium Dropbox/Eddie Yu/Puerto Rico"
* Set STATA to search ado files in dropbox, for maptile *
sysdir set PERSONAL "$dropbox/Programs/ado"

** Install Packages 
// ssc install listtex, replace all 
// ssc install binscatter, replace all
// ssc install estout, replace all 
// ssc install cdfplot, replace all
// ssc install winsor, replace all
// ssc install reghdfe, replace all
// ssc install tuples, replace all
// ssc install coefplot, replace all 
// ssc install spmap, replace all 
// ssc install maptile, replace all 
// ssc install ivreg2, replace all 
// ssc install ftools, replace all
// ssc install ivreghdfe, replace all 
// ssc install ivreg2hdfe, replace all
// ssc install reg2hdfe, replace all 
// ssc install moremata, replace all
// ssc install tmpdir, replace all
// ssc install ranktest, replace all
// ssc install dataout, replace
// maptile_install using "http://files.michaelstepner.com/geo_county1990.zip"
// maptile_install using "http://files.michaelstepner.com/geo_state.zip"

** Global Dropbox Paths (raw to clean data)
global output 		"$dropbox/Programs/output"
global data 		"$dropbox/Data"
global analysis 	"$dropbox/Programs"
global linkdata		"$dropbox/Programs/output/base_1995"
global xwalk		"$dropbox/Data/Cross Walk"
global qcewdata		"$dropbox/Data/QCEW"
global bea			"$dropbox/Data/BEA"
global consp		"$dropbox/Data/conspuma"
global additional 	"$dropbox/Data/data_additional"
global output_NETS 	"$dropbox/Programs/output/NETS_20171128"
global output_NETS2 "$dropbox/Programs/output/NETS_20171129_v2"
global output_NETS_control "$dropbox/Programs/output/NETS_control"
global WRDS			"$dropbox/Data/WRDS"
global ASM			"$dropbox/Data/ASM"
global SOI			"$dropbox/Data/SOI Data"
global irs 			"$dropbox/Data/IRS"
global randr		"$analysis/8-R&R 2023/paper"
global overleaf		"/Users/jisangyu/JC's Data Emporium Dropbox/Eddie Yu/앱/Overleaf/PR table"
global ster			"$output/R&R2023/ster/paper"
// global result       "$randr/out"
global result		"/Users/jisangyu/JC's Data Emporium Dropbox/Eddie Yu/앱/Overleaf/PuertoRico"

cd "$analysis"

* Build data: Make data from newly pulled compustat historical segments data
/* IMPORTANT: All data produced/to be used reside in Replication Data/R&R2023/paper */
do "$randr/build/compustat_merge_manual.do"
do "$randr/build/compustat_ETR_calc_segments.do"
do "$randr/build/construct_SOI_NAICS_xwalk.do" // create crosswalk between SOI industry codes and NAICS
do "$randr/build/build_NETS.do" /* creates Replication Data/R&R2023/NETS_emp.dta  */


	** CLEAN DATA **
	/* We mostly use Narrow Comparison : drop bottom 60% of 1995 PPE Deciles */
	do "$randr/clean/clean_compu_for_analysis_sizecutoff.do" /* drop if filter; creates compu_NETS_ETR_sizecutoff.dta */
	// also creates ET_post*, PR_post* interaction variables for long-difference
	
	do "$randr/clean/clean_compu_for_analysis.do" /* drop if filter; creates compu_NETS_ETR.dta */
	
	do "$randr/clean/impute_ETR_SOI.do" // use soi-naics cross walk to impute PR calibration numbers, produced SOI_imputed_ETR.dta
	
	do "$randr/clean/merge_SOI_to_compu.do" // merge SOI_imputed_ETR.dta to compu_NETS_ETR_sizecutoff.dta, produces onestep_compu_SOI_clean.dta 
	
	do "$randr/clean/merge_SOI_to_NETS.do" // merge SOI_imputed_ETR.dta to NETS, produces "NETS_emp_SOI_imputed.dta"
	// then constructs DFL weights, post1 & post2 (long diff) produces "NETS_emp_SOI_DFL.dta"
	
	do "$randr/clean/pr_treat_taxhaven_clean.do" // creates pr treatment variables data & tax haven usage data

	
	
	do "$randr/clean/table2_clean.do" // creates table 2 etc data from compu_NETS_ETR_sizecutoff.dta
// 	do "$randr/clean/table4_clean.do" // creates table 4, A8, A9 data /* drop if filter */ no longer in use!

	do "$randr/clean/figure4_clean.do" // creates figure 5, A6-A9 data
	do "$randr/clean/figureA9_clean.do" // creates figure7_data_new from ASM, pr_link_estXindustry_d.dta


	** OUTPUT **
	do "$randr/run/figure1_c.do"
	
	
	*** Compustat (Table 2) : Investment/ETR/IV panels
	do "$randr/run/Compu_Investment.do"
	do "$randr/run/Compu_ETR.do"
	do "$randr/run/Compu_IV.do"
	do "$randr/run/create_compu_table.do"
	
	*** NETS : Employment/ETR/IV panels
	do "$randr/run/NETS_Employment.do"
	do "$randr/run/NETS_ETR.do"
	do "$randr/run/NETS_IV.do"
	do "$randr/run/create_NETS_table.do"
	do "$randr/run/NETS_figure6.do"
	
	** Lorenz Curve **
	do "$randr/run/figureA2.do"
