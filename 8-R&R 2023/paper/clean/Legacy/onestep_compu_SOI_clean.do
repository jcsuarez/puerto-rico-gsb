// This file estimates semi-elasticities with SOI-estimated tax changes
// includes industry-level variation
// Author: Lysle Boller
// Date: April 25, 2020

clear all
set more off
snapshot erase _all
// ** industry level calibration kills ETR to small numbers, one shot estimate of
// ** industry share 0.22 (which is derived by ppent/pretax income/gross profit) works better
// ** look at line 90 gen indshr = 0.22 for reference instead
// ***********
// * calibrate industry-level share of capex flowing to PR in year 1995
// ***********
//
// use "$data/Replication Data/paper/compu_NETS_ETR.dta", clear
// xtset gvkey year
// gsort gvkey year
//
// * weight by variable
// local weight_var "gp"
// collapse (sum) `weight_var' if year==1995, by(n3 PR)
// by n3: egen sum_var = sum(`weight_var')
// gen indshr = `weight_var' / sum_var
//
// * impute 0 for non-naics data
// by n3: egen cnt = count(PR)
// preserve
// 	tempfile impute
// 	keep if cnt == 1
// 	expand 2, gen(dup)
// 	replace indshr = 0 if dup == 1
// 	drop if dup != 1
// 	drop dup
// 	replace PR = 1
// 	replace cnt = 2
// 	save `impute'
// restore 
//
// * get a full n3 X PR X capex share data
// append using "`impute'"
// drop cnt
// gsort n3 PR
// tab n3 PR, summ(indshr)
//
// * merge with "variable", which is industry classification variable used in SOI data
// merge m:m n3 using "$output/NAICS_crosswalk"
// drop _merge
// drop if missing(variable)
// drop if PR!=1
// keep variable indshr
// order variable indshr
// tempfile pr_indshr
// save "`pr_indshr'"


***********
* Impute Effective Tax Rates for SOI Industries
***********

* Import SOI data
use "$SOI/aggregate_SOI", clear
/* replace variable = subinstr(variable,"_t_","",.)
replace variable = substr(variable,1,length(variable)-1) if substr(variable,length(variable)-2,length(variable)) == "_" */

drop if variable == "manufact"
drop if variable == "tot_"

gen ETR = Income_Tax_After_Credits / Income_Subject_To_Tax
gen pos_cred = Us_Possessions_Tax_Credit / Income_Subject_To_Tax
replace pos_cred = 0 if pos_cred == .

** Generating weights that are constant over time for each industry based on 1995 levels {
foreach var of var Us_Possessions_Tax_Credit Income_Subject_To_Tax Net_Income Receipts {
bys variable: gen `var'_temp = `var' if inlist(year,1995) //
bys variable: egen `var'_base = mean(`var'_temp)
drop *temp
}

sort variable year
keep if year > 1994 & year < 2008

* renaming variable to be more usable
rename Income_Subject_To_Tax_base ISTT
rename Us_Possessions_Tax_Credit_base USPTC

// **** 0927 merge Share of PR capex by industry ("variable")
// merge m:m variable using "`pr_indshr'"
// drop _merge*
gen indshr = .21

* share of income subject to tax that is covered by the US Possession tax credit
gen share = USPTC / ISTT
* Counterfactual US possessions tax credit holding share of income constant
gen const_cred = share * ISTT
* dollars of lost credits due to the rollback
gen lost_cred = max(0, const_cred - Us_Possessions_Tax_Credit)

* oberserved effective tax rates
* gen p_data = ((Income_Tax_After_Credits + Us_Possessions_Tax_Credit) * `base_link' - Us_Possessions_Tax_Credit) / (Income_Subject_To_Tax * `base_link')
gen p_data = (Income_Tax_After_Credits + Us_Possessions_Tax_Credit) / (Income_Subject_To_Tax)
replace p_data = (Income_Tax_After_Credits) / (Income_Subject_To_Tax) if p_data == .
* Predicted tax rates had S936 not been repealed, correcting for 20% adjustment to other credits
gen p_new = ((Income_Tax_After_Credits + Us_Possessions_Tax_Credit) * indshr - Us_Possessions_Tax_Credit) / (Income_Subject_To_Tax * indshr)
replace p_new = p_data if p_new == .
tempfile soi_data
save "`soi_data'"

***************************************************************
********* Merge with Compustat Data: Set Sample First *********
***************************************************************
use "$data/Replication Data/paper/compu_NETS_ETR_sizecutoff.dta", clear


***************************************************************
********* Now merge to SOI data to get tax_impute var *********
***************************************************************
tostring n3, replace
* Encode NAICS2 as a string
decode n2, gen(n2string)
drop n2
rename n2string n2
* Merge in the SOI industry definitions
merge m:1 n3 using "$output/NAICS_crosswalk"
rename _merge _merge_n3
frame create naics2_crosswalk
frame naics2_crosswalk: use "$output/NAICS_crosswalk"
frame naics2_crosswalk: drop if strlen(n3) == 3
frame naics2_crosswalk: tab n2 // Check unique
frlink m:1 n2, frame(naics2_crosswalk)
frget variable_n2 = variable, from(naics2_crosswalk)
replace variable = variable_n2 if variable == ""
drop _merge*
* Merge in the imputed tax rates
merge m:1 year variable using "`soi_data'"

**********************************************************************************
* Fill missing ETRs with earliest and latest available imputed ETR for each firm *
**********************************************************************************

* Drop groups with no match in the SOI data
bys gvkey: egen has_etr = total(p_data)
keep if has_etr > 0
drop has_etr

* Impute pre-period with earliest tax rate available
foreach var of var p_data p_new {
gen byte missing_`var' = missing(`var')
bys gvkey (missing_`var' year) : gen first_`var' = `var'[1]
bys gvkey (year) : gen missing_pre_`var' = sum(1 - missing_`var') == 0
bys gvkey (year) : replace `var' = first_`var' if missing_pre_`var' == 1
gen ok = 1 - missing_`var'
bys gvkey (ok year) : gen last_`var' = `var'[_N]
bys gvkey (year) : replace `var' = last_`var' if `var' == .
drop ok
drop missing_`var'
drop missing_pre_`var'
}

/* * Looks like NAICS code 337 is missing year 1997 for some reason
frame put _all, into(temp)
frame temp: keep if p_data == .
frame temp: desc */

* Compute counterfactual rates
gen tax_impute = p_data
replace tax_impute = p_new if PR
replace tax_impute = tax_impute * 100
replace capex_base = capex_base * 100

** below is already implemented in clean_compu_analyis.do - DFL_ppe weight construction
// ********************************************************************************
// ***************************** Export for analysis ******************************
// ********************************************************************************
// * some DFL weights first, then drop (for entropy balancing)
// destring n2, replace
// xtile ppe95_binDFL = ppent if year == 1995 , n(5) 
// logit PR i.n2##i.ppe95_binDFL if year == 1995
// drop phat min_phat w_phat
// predict phat, pr 
// *winsor phat, p(.01) g(w_phat) 
// *replace phat = w_phat
// bys gvkey: egen min_phat = min(phat) 
// * ATOT
// gen DFL2 = (PR+(1-PR)*min_phat/(1-min_phat))


tab year PR, sum(tax_impute)


save "$data/Replication Data/R&R2023/paper/onestep_compu_SOI_clean", replace
