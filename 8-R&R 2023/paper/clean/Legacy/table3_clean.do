***************************************************************
//Filename: table3_clean.do
//Author: eddie yu
//Date: 2023-09-20
//Task: This script creates data for table3 (Semi-elasticity of Investment w.r.t. ETR - PR_v49.pdf)

***************************************************************

clear all
set more off
snapshot erase _all

use "$data/Replication Data/R&R2023/paper/onestep_compu_SOI_clean", clear
drop if missing(year) | missing(gvkey)

* relative increase in taxes for PR firms with rollback
gen p_dif = p_data - p_new

keep year gvkey n3 p_data p_dif tax_impute
duplicates drop

*******************************************************
* next using the data from table 2
*******************************************************
merge 1:m gvkey year n3 using "$data/Replication Data/R&R2023/paper/table2_data"
drop _merge


* pulling in tax variation
merge 1:1 gvkey year using "$data/Replication Data/R&R2023/paper/table4_data"
drop _merge

save "$data/Replication Data/R&R2023/paper/table3_data", replace
