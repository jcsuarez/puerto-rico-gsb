***************************************************************
//Filename: table2_clean.do
//Author: eddie yu
//Date: 2023-09-20
//Task: This script cleans from R&R2023/paper/compu_NETS_ETR_sizecutoff.dta to clean
//		relevant variables for Table 2, Table 5 (Foreign Share),
//		Table A5, Table A6
//		All tables here refer to PR_v49.pdf / Overleaf version
**************************************************************

clear all
set more off 
snapshot erase _all

* baseline data 
use "$data/Replication Data/R&R2023/paper/compu_NETS_ETR_sizecutoff.dta", clear
snapshot save


keep gvkey capex capex_base year n2 PR fs_capx ppe95_bins10 ///
ppent ppe tot_ias capxs emps nis DFL_ppe _webal n3 PR_* IK exp_sec exp_ind
save "$data/Replication Data/R&R2023/paper/table2_data", replace

snapshot restore 1
keep gvkey capex_base_p year n2 PR DFL_ppe _webal n3 PR_post IKIK ppe95_bins10 ///
exp_sec exp_ind PR_post2
save "$data/Replication Data/R&R2023/paper/tableA4_data", replace

snapshot restore 1
keep gvkey capex_base_1 year n2 PR DFL_ppe _webal n3 PR_post IK_1 ppe95_bins10 ///
exp_sec exp_ind PR_post2
save "$data/Replication Data/R&R2023/paper/tableA5_data", replace

