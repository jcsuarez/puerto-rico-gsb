***************************************************************
//Filename: figureA12_clean.do
//Author: eddie yu
//Date: 2023-09-20
//Task: Given pr_extract_est.dta, this just creates figureA12_clean.dta
***************************************************************

**************************************************
// Original PR_v43 version: 
// This file creates the data sets for figure A10
// Author: Dan Garrett
// Date: 4-20-2020
**************************************************

/* Data sources:
-NETS
*/

* starting point for all panels is 3-wrds_segments/DFL_nets_emp_2019_06_11_dan

clear all
set more off 
snapshot erase _all

************************************
* Grabbing and organizing the data
************************************
use "$data/Replication Data/R&R2023/paper/NETS_emp.dta", clear

* DFL 
capture: drop q_wgt
xtile q_wgt = base_emp if year == 1995, n(20) 
logit PR i.q_wgt#i.naic2

capture: drop phat min_phat w w_w w_wgt
predict phat, pr 
bys hqduns: egen min_phat = min(phat) 
gen w = w_wgt*(PR+(1-PR)*min_phat/(1-min_phat))

* keeping variables and saving 
keep emp_growth year PR wgt w hqduns naic2 major_ind major_sec
save "$data/Replication Data/R&R2023/paper/figureA12_data", replace


*** Keep top tercile, re-do DFL, and export for robustness figure
use "$data/Replication Data/R&R2023/paper/NETS_emp.dta", clear
keep if emp_bins3==3

* DFL 
capture: drop q_wgt
xtile q_wgt = base_emp if year == 1995, n(5) 
logit PR i.q_wgt#i.naic2

capture: drop phat min_phat w w_w w_wgt
predict phat, pr 
bys hqduns: egen min_phat = min(phat) 
gen w = w_wgt*(PR+(1-PR)*min_phat/(1-min_phat))

* keeping variables and saving 
keep emp_growth year PR wgt w hqduns naic2 major_ind major_sec
save "$data/Replication Data/R&R2023/paper/figureA12_Top3rd_data", replace
