***************************************************************
//Filename: table5.do
//Author: eddie yu
//Date: 2023-09-20
//Task: This script creates table 5 (PR_v49.pdf, Table 4 from PR_v43.pdf)
//		It uses table5_data, which is created from table2_clean.do
***************************************************************

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data/R&R2023/paper/table5_data", clear

* formatting, labeling, and descriptive scalar creation
local FE_fs_capx "n3"
local FE_capex_base "gvkey"
local FE_capex_base_p "gvkey"
local FE_capex_base_1 "gvkey"
local FE_ftax "gvkey"
local FE_fs_capx_word "Industry"
local FE_capex_base_word "Firm"
local FE_capex_base_p_word "Firm"
local FE_capex_base_1_word "Firm"
local FE_ftax_word "Firm"
local fs_capx_sum "fs_capx"
local capex_base_sum "IK"
local capex_base_p_sum "IKIK"
local capex_base_1_sum "IK_1"
local ftax_sum "ftax"
local fs_capx_scalars1 ""
local capex_base_scalars1 "avg Sample Average I/K in 2006" 
local capex_base_p_scalars1 "avg Sample Average I/K in 2006 Relative to 1995"
local capex_base_1_scalars1 "avg Sample Average I/K in 2006"
local ftax_scalars1 ""
local fs_capx_scalars2 ""
local capex_base_scalars2 "elas Percent of 2006 Average"
local capex_base_p_scalars2 "elas Percent of 2006 Average"
local capex_base_1_scalars2 "elas Percent of 2006 Average"
local ftax_scalars2 ""
local fs_capx_scalars3 ""
local capex_base_scalars3 "elast Semi-elasticity of Investment"
local capex_base_p_scalars3 "elast Semi-elasticity of Investment"
local capex_base_1_scalars3 "elast Semi-elasticity of Investment"
local ftax_scalars3 ""
local fs_capx_scalars4 ""
local capex_base_scalars4 "pp_dif Change in Effective Tax Rate"
local capex_base_p_scalars4 "pp_dif Change in Effective Tax Rate"
local capex_base_1_scalars4 "pp_dif Change in Effective Tax Rate"
local ftax_scalars4 ""
local fs_capx_title "Change in Foreign Share of Investment"
local capex_base_title "Change in Investment: $ \frac{I}{K_{1990-1995}} $"
local capex_base_p_title "Percent Change in Investment: $ \frac{I}{I_{1990-1995}} - 1 $"
local capex_base_1_title "Change in Investment: $ \frac{I}{K_{1990-1995}} $"
local ftax_title "Change in Federal Taxes Paid as a Percent of Pretax Income"

global level "gvkey"
global spec1 ", cl(gvkey) a(PR ppe95_bins10##year)"
global spec2 ", cl(gvkey) a($level ppe95_bins10##year)"
global spec3 "if exp_sec, cl(gvkey) a($level ppe95_bins10##year)"
global spec4 "if exp_ind, cl(gvkey) a($level ppe95_bins10##year)"
global spec5 ", cl(gvkey) a($level ppe95_bins10##year n2##ppe95_bins10##year)"
global spec6 "[aw=DFL_ppe], cl(gvkey) a($level ppe95_bins10##year n2##ppe95_bins10##year)"
global spec7 "[aw=_webal], cl(gvkey) a($level ppe95_bins10##year n2##ppe95_bins10##year)"

xtset gvkey year

* scalar values from SOI data analysis:
scalar p_dif_v0 = .0572734
scalar p_dif_v1 = .0699663
scalar p_dif_v2 = .0465333
scalar p_dif_v3 = .0601001


est clear
local i = 0 
foreach y of var fs_capx {	
foreach j of num 1 {	
	local i = `i' + 1
	eststo reg_`y'_`j'_`i': reghdfe `y' PR_post2 $spec1
	lincom PR_post
	sum ``y'_sum' if e(sample) & year == 2006, d
		estadd local avg = string(r(mean), "%8.3fc")
		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
		estadd local elast = string(-_b[PR_post] / r(mean) / p_dif_v0, "%8.2fc")
		estadd local pp_dif = string(p_dif_v0*100, "%8.3fc")
	est save "$output/R&R2023/ster/paper/table5_`i'", replace
	
	local i = `i' + 1
	eststo reg_`y'_`j'_`i': reghdfe `y' PR_post2 $spec2
	lincom PR_post
	sum ``y'_sum' if e(sample) & year == 2006, d
		estadd local avg = string(r(mean), "%8.3fc")
		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
		estadd local elast = string(-_b[PR_post] / r(mean) / p_dif_v0, "%8.2fc")
		estadd local pp_dif = string(p_dif_v0*100, "%8.3fc")
	est save "$output/R&R2023/ster/paper/table5_`i'", replace
	
	local i = `i' + 1
	eststo reg_`y'_`j'_`i': reghdfe `y' PR_post2 $spec3
	lincom PR_post
	sum ``y'_sum' if e(sample) & year == 2006, d
		estadd local avg = string(r(mean), "%8.3fc")
		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
		estadd local elast = string(-_b[PR_post]  / r(mean) / p_dif_v0, "%8.2fc")
		estadd local pp_dif = string(p_dif_v0*100, "%8.3fc")
	est save "$output/R&R2023/ster/paper/table5_`i'", replace
	
	local i = `i' + 1
	eststo reg_`y'_`j'_`i': reghdfe `y' PR_post2 $spec4
	lincom PR_post
	sum ``y'_sum' if e(sample) & year == 2006, d
		estadd local avg = string(r(mean), "%8.3fc")
		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
		estadd local elast = string(-_b[PR_post]  / r(mean) / p_dif_v2, "%8.2fc")
		estadd local pp_dif = string(p_dif_v2*100, "%8.3fc")
	est save "$output/R&R2023/ster/paper/table5_`i'", replace
	
	local i = `i' + 1
	eststo reg_`y'_`j'_`i': reghdfe `y' PR_post2 $spec5
	lincom PR_post
	sum ``y'_sum' if e(sample) & year == 2006, d
		estadd local avg = string(r(mean), "%8.3fc")
		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
		estadd local elast = string(-_b[PR_post]  / r(mean) / p_dif_v3, "%8.2fc")
		estadd local pp_dif = string(p_dif_v3*100, "%8.3fc")
	est save "$output/R&R2023/ster/paper/table5_`i'", replace
	
	local i = `i' + 1
	eststo reg_`y'_`j'_`i': reghdfe `y' PR_post2 $spec6
	lincom PR_post
	sum ``y'_sum'  if e(sample) & year == 2006, d
		estadd local avg = string(r(mean), "%8.3fc")
		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
		estadd local elast = string(-_b[PR_post]  / r(mean) / p_dif_v3, "%8.2fc")
		estadd local pp_dif = string(p_dif_v3*100, "%8.3fc")
	est save "$output/R&R2023/ster/paper/table5_`i'", replace

	local i = `i' + 1
	eststo reg_`y'_`j'_`i': reghdfe `y' PR_post2 $spec7
	lincom PR_post
	sum ``y'_sum'  if e(sample) & year == 2006, d
		estadd local avg = string(r(mean), "%8.3fc")
		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
		estadd local elast = string(-_b[PR_post]  / r(mean) / p_dif_v3, "%8.2fc")
		estadd local pp_dif = string(p_dif_v3*100, "%8.3fc")
	est save "$output/R&R2023/ster/paper/table5_`i'", replace
}
esttab reg_`y'_1_? using "$randr/out/tables/table5.tex", keep(_cons) stats() ///
b(3) se par label ///
noobs nogap nomtitle tex replace nonum nocons postfoot("") prefoot("") posthead("")
esttab reg_`y'_1_? using "$randr/out/tables/table5.tex", keep(PR_post) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) noobs scalars("N Observations" "``y'_scalars1'" "``y'_scalars2'" "``y'_scalars4'" "``y'_scalars3'"  ) /// 
varlabel(PR_post "\hspace{1em}Exposure to Section 936 X Post")  posth("\hline") ///
preh("") postfoot("\hline"  /// 
"Size-by-Year Fixed Effects                    & Y & Y & Y & Y & Y & Y & Y  \\    "      /// 
"Firm Fixed Effects                            &   & Y & Y & Y & Y & Y & Y  \\    "      /// 
"S936 Exposed Sector			               &   &   & Y &   &   &   &    \\    "      ///
"S936 Exposed Industry                         &   &   &   & Y &   &   &    \\    "      /// 
"Size-by-Industry-by-Year Fixed Effects        &   &   &   &   & Y & Y & Y  \\    "      /// 
"DFL Weights                                   &   &   &   &   &   & Y &    \\    "      /// 
"Entropy Balancing Weights                     &   &   &   &   &   &   & Y  \\    \hline\hline" /// 
"\\ " /// 
"\end{tabular} }" )	
}		
