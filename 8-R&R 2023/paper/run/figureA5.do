*******************************************************************************
//Filename: figure2_appendix.do
//Author: eddie yu
//Date: 2023-09-20
//Task: Distribution of Exposure to S936 at County Level in 1995, 
//		for  (1) pr_link / (2) pr_link_ind variable
*******************************************************************************

/* Data sources:
Panel A: NETS establishment shares at county level
Panel B: NETS establishment shares at county level
*/

clear all
set more off


*******************************************************************************
* pr_link variable
use "$data/Replication Data/R&R2023/paper/figure2_pr_link", clear

* Residualized Link 
reg pr_link i.fips_state
predict res_pr_link, res

* winsorizing residualized links
winsor res_pr_link, gen(w_res_pr_link) p(.01)
egen std_w_res_pr_link = std(w_res_pr_link)

** graphing the residualized version
maptile std_w_res_pr_link , geo(county1990) stateoutline(linewidthstyle)  nquantiles(5)   res(.5)
graph export "$randr/out/Graphs/map_pr_link_std_d.png", replace 


*******************************************************************************

* pr_link_ind variable
use "$data/Replication Data/R&R2023/paper/figure2_pr_link_ind", clear

* Residualized Link 
reg pr_link i.fips_state
predict res_pr_link, res

* winsorizing residualized links
winsor res_pr_link, gen(w_res_pr_link) p(.01)
egen std_w_res_pr_link = std(w_res_pr_link)

** graphing the residualized version
maptile std_w_res_pr_link , geo(county1990) stateoutline(linewidthstyle)  nquantiles(5)   res(.5)
graph export "$randr/out/Graphs/map_pr_link_ind_std_d.png", replace 
