********************************************************************************
//Filename: pr_by_year_instr.do
//Author: eddie yu
//Date: 2023-09-20
//Task: This script PR-by-Year as Instrument Variable and conducts IV regression
********************************************************************************

clear all
set more off
snapshot erase _all

global texsettings "posth("\midrule") preh("\begin{tabular}{lcccccc} \toprule")  postfoot("\midrule Size-by-Year FE     & Y & Y & Y & Y & Y &  \\ " "Sector-by-Year FE &   & Y & Y &   &   &  \\ " "Firm FE &   &   & Y &   &   &   \\ " "S936 Exposed Sector        &   &   &   & Y &   &   \\ " "S936 Exposed Industry      &   &   &   &   & Y &   \\ " "Sector-by-Size-by-Year FE  &   &   &   &   &   & Y  \\ " "DFL Weights &   &   &   &   &   & Y \\  \bottomrule" "\end{tabular}" ) replace sty(tex)"


********************************************************************************
******************************** IV Regressions ********************************
********************************************************************************

use "$data/Replication Data/R&R2023/onestep_compu_SOI_clean", clear

* Approach 0922: 7 columns
global level "gvkey"
global spec1 ", cl(gvkey) a(PR ppe95_bins10##year)"
global spec2 ", cl(gvkey) a($level ppe95_bins10##year)"
global spec3 "if exp_sec, cl(gvkey) a($level ppe95_bins10##year)"
global spec4 "if exp_ind, cl(gvkey) a($level ppe95_bins10##year)"
global spec5 ", cl(gvkey) a($level ppe95_bins10##year n2##ppe95_bins10##year)"
global spec6 "[aw=DFL_ppe], cl(gvkey) a($level ppe95_bins10##year n2##ppe95_bins10##year)"
global spec7 "[aw=_webal], cl(gvkey) a($level ppe95_bins10##year n2##ppe95_bins10##year)"


destring n2, replace

* TEST NEW ETR *
gen pre = (year<1995)
gen post1 = (year>1995)*(year<=2003)
gen post2 = (year>2003)

foreach var in pre post1 post2 {
	gen ET_`var' = (`var'==1)*PR
}

foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 spec7 {
		eststo `spec'_est1: reghdfe tax_impute ET_* $`spec' 
		eststo `spec'_est2: reghdfe tax_impute2 ET_* $`spec' 

}		

*** "Industry-specific rho weight" ***	
esttab spec?_est1, keep(ET_post2) cells(b p)
*** "Constant rho weight" ***	
esttab spec?_est2, keep(ET_post2) cells(b p)	



* PR-by-year instruments
local dep1 = "(tax_impute = PR##i.year)"
local dep2 = "(tax_impute2 = PR##i.year)"
foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 spec7 {
		eststo `spec'_est1: qui ivreghdfe capex_base `dep1' $`spec' 
		eststo `spec'_est2: qui ivreghdfe capex_base `dep2' $`spec' 

}		

*** "Industry-specific rho weight" ***	
esttab spec?_est1, keep(tax_impute) cells(b p)
*** "Constant rho weight" ***	
esttab spec?_est2, keep(tax_impute2) cells(b p)	


* PR-by-post instruments
local dep1 = "(tax_impute = PR##i.post2) PR##i.post1 PR##i.pre"
local dep2 = "(tax_impute2 = PR##i.post2) PR##i.post1 PR##i.pre"
foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 spec7 {
		eststo `spec'_est1: qui ivreghdfe capex_base `dep1' $`spec' 
		eststo `spec'_est2: qui ivreghdfe capex_base `dep2' $`spec' 

}		

*** "Industry-specific rho weight" ***	
esttab spec?_est1, keep(tax_impute) cells(b p)
*** "Constant rho weight" ***	
esttab spec?_est2, keep(tax_impute2) cells(b p)	


********************** RESUME ORIGINAL CODE **********************



* PR-by-year instruments	
est clear
local j = 0
local dep = "(tax_impute = PR##i.year)"
foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 spec7 {
		local j = `j' + 1
		eststo reg_`spec'_`j': ivreghdfe capex_base `dep' $`spec' 
		estimates save "$output/R&R2023/ster/paper/PR_by_year_spec`j'", replace
}		
	
esttab using "$output/R&R2023/onestep/3_PR_by_year_instruments.tex", keep(tax_impute) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) noobs scalars("N Observations" ) ///
varlabel( tax_impute "\hspace{1em}Effective Tax Rate") $texsettings
	

* PR-by-year instruments: positive ETR
est clear
preserve
	replace tax_impute = max(0, tax_impute)
	local j = 0
	local dep = "(tax_impute = PR##i.year)"
	foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 spec7 {
		local j = `j' + 1
		eststo reg_`spec'_`j': ivreghdfe capex_base `dep' $`spec' 
		estimates save "$output/R&R2023/ster/paper/PR_by_year_posETR_spec`j'", replace
	}			
restore

esttab using "$output/R&R2023/onestep/5_PR_by_year_instruments_positive_ETR.tex", keep(tax_impute) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) noobs scalars("N Observations" ) ///
varlabel( tax_impute "\hspace{1em}Effective Tax Rate") $texsettings
