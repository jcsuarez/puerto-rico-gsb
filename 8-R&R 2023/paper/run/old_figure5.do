***************************************************************
//Filename: figure5.do
//Author: eddie yu
//Date: 2023-09-20
//Task: This script creates Figure5 under different specifications
***************************************************************

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data/R&R2023/paper/figure5_data", clear

*** labels for some graphs
local fs_capx_name "Foreign Share Investment"
local capex_base_name "Investment/Physical Capital"
local capex_base_p_name "% Change in Investment/Physical Capital"
local capex_base_1_name "Investment/Physical Capital"
local ftax_name "Federal Tax Rate"
 
* combining years and N2
egen yearg = group(year n2)

*figures 5, 6, A5, and A6
xi i.year|PR , noomit
drop _IyeaXPR_1995 

forval i=1990(1)2014 { 
	capture: label var _IyeaXPR_`i' " "
} 
forval i=1991(2)2014 { 
	capture: label var _IyeaXPR_`i' "`i'"
} 

* Spec
global level "gvkey"
global spec1 ", cl(gvkey) a($level ppe95_bins10##year)"

global specname1 "Firm FE + Size-by-Year FE"

global c1 "navy"

snapshot save


xtset gvkey year


*** Store estimates ***
local y "capex_base"
local cnt = 0
foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 spec7{
	local cnt = `cnt' + 1
	reghdfe `y' _IyeaX* PR $`spec'
	estimates save `spec'_base, replace
	
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/`cnt' {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec5", lco($c5) mco($c5) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec5", lco($c5) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec6", lco($c6) mco($c6) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec6", lco($c6) lpa(solid)) ///
	 	(scatter pr_beta fig_year if specn=="spec7", lco($c7) mco($c7) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec7", lco($c7) lpa(solid)) ///
		, yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(size(small) pos(6)  r(4) on order (1 3 5 7 9 11 13) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")  ///
		label(9 "$specname5")  ///
		label(11 "$specname6") ) ///
		yti("Effect of S936 on ``y'_name'", margin(medium))
restore	
}
graph export "$overleaf/graphs/paper/figure5.pdf", replace 

