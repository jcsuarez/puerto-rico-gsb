***************************************************************
//Filename: create_compu_table.do
//Author: eddie yu
//Date: 2023-09-20
//Task: This script creates Main Table 2 Compustat Regression:
//		It constructs 3 sets of panels:
//		- Baseline I/K
//		- ETR (tax_impute)
//		- IV (PR#post, PR#year as instrumental variable)
***************************************************************


	******************************** Set Directory *********************************

	cd "$ster"
	estimates clear
	eststo clear

	******************************** Load Estimates ********************************
	*** Call the non-SOI tables ***
	foreach i in "Investment_" "ETR_taximpute1_" "ETR_taximpute2_" ///
	"IV_PRyear_taximpute1_" "IV_PRyear_taximpute2_" ///
	"IV_PRpost_taximpute1_" "IV_PRpost_taximpute2_" {
		forval j = 1/7 {
			estimates use "Compu_`i'`j'"
			estimates store `i'_spec`j'
		}
	}	


	********************************* Make Tables **********************************
	*** Main Table: Investment *** 
	esttab Investment_* using "$result/Tables/table_capex_base_v3.tex", keep(PR_post2) /// 
		cells(b(fmt(3)) se(par) p) ///
		coeflabel(PR_post2 "\hspace{3mm} Exposure to Section \S936 \hspace{3mm} $\times$ Post") /// 
		nogap nomtitle tex wrap replace label nocons collabels(none) varwidth(40) nomtitle ///
		prefoot("") postf("\hline") posth("\hline \textbf{A. Investment-to-Capital Ratio} \\")
	

	esttab ETR_taximpute2_* using "$result/Tables/table_capex_base_v3.tex", keep(ET_post2) ///  
		cells(b(fmt(3)) se(par) p) ///
		coeflabel(ET_post2 "\hspace{3mm} Exposure to Section \S936 \hspace{3mm} $\times$ Post") /// 
		tex wrap append label mlab(none) collabels(none) nonum varwidth(40) nomtitle /// 
		prefoot("") postfoot("\hline") preh("") posthead("\textbf{B. Effective Tax Rate} \\")

	esttab IV_PRpost_taximpute2_* using "$result/Tables/table_capex_base_v3.tex", keep(tax_impute2) ///  
		cells(b(fmt(3)) se(par) p) ///
		coeflabel(tax_impute2 "\hspace{3mm} Effective Tax Rate") /// 
		tex wrap append mlab(none) label  collabels(none) nonum  varwidth(40) nomtitle /// 
		prefoot("") posthead("\textbf{C. Semi-Elasticity of Investment} \\") /// 
		preh("") postfoot("\hline"  /// 
		"Size  $\times$ Year Fixed Effects                    & Y & Y & Y & Y & Y & Y & Y  \\    "      /// 
		"Firm Fixed Effects                          	   &   & Y & Y & Y & Y & Y & Y  \\    "      /// 
		"S936 Exposed Sector			               &   &   & Y &   &   &   &    \\    "      ///
		"S936 Exposed Industry                         &   &   &   & Y &   &   &    \\    "      /// 
		"Size $\times$ Industry $\times$ Year Fixed Effects        &   &   &   &   & Y & Y & Y  \\    "      /// 
		"DFL Weights                                   &   &   &   &   &   & Y &    \\    "      /// 
		"Entropy Balancing Weights                     &   &   &   &   &   &   & Y  \\    \hline\hline" /// 
		"\\ " /// 
		"\end{tabular} }" )

	
	*** Appendix 1 : PR#year IV *** 
	esttab Investment_* using "$result/Tables/table_capex_base_yearIV.tex", keep(PR_post2) /// 
		cells(b(fmt(3)) se(par) p) ///
		coeflabel(PR_post2 "\hspace{3mm} Exposure to Section \S936 \hspace{3mm} $\times$ Post") /// 
		nogap nomtitle tex wrap replace label nocons collabels(none) varwidth(40) nomtitle ///
		prefoot("") postf("\hline") posth("\hline \textbf{A. Investment-to-Capital Ratio} \\")
	

	esttab ETR_taximpute2_* using "$result/Tables/table_capex_base_yearIV.tex", keep(ET_post2) ///  
		cells(b(fmt(3)) se(par) p) ///
		coeflabel(ET_post2 "\hspace{3mm} Exposure to Section \S936 \hspace{3mm} $\times$ Post") /// 
		tex wrap append label mlab(none) collabels(none) nonum varwidth(40) nomtitle /// 
		prefoot("") postfoot("\hline") preh("") posthead("\textbf{B. Effective Tax Rate} \\")

	esttab IV_PRyear_taximpute2_* using "$result/Tables/table_capex_base_yearIV.tex", keep(tax_impute2) ///  
		cells(b(fmt(3)) se(par) p) ///
		coeflabel(tax_impute2 "\hspace{3mm} Effective Tax Rate") /// 
		tex wrap append mlab(none) label  collabels(none) nonum  varwidth(40) nomtitle /// 
		prefoot("") posthead("\textbf{C. Semi-Elasticity of Investment} \\") /// 
		preh("") postfoot("\hline"  /// 
		"Size  $\times$ Year Fixed Effects                    & Y & Y & Y & Y & Y & Y & Y  \\    "      /// 
		"Firm Fixed Effects                          	   &   & Y & Y & Y & Y & Y & Y  \\    "      /// 
		"S936 Exposed Sector			               &   &   & Y &   &   &   &    \\    "      ///
		"S936 Exposed Industry                         &   &   &   & Y &   &   &    \\    "      /// 
		"Size $\times$ Industry $\times$ Year Fixed Effects        &   &   &   &   & Y & Y & Y  \\    "      /// 
		"DFL Weights                                   &   &   &   &   &   & Y &    \\    "      /// 
		"Entropy Balancing Weights                     &   &   &   &   &   &   & Y  \\    \hline\hline" /// 
		"\\ " /// 
		"\end{tabular} }" )
		
	*** Appendix 2 : Industry Calibration, PR#post IV *** 
	esttab Investment_* using "$result/Tables/table_capex_base_indETR_postIV.tex", keep(PR_post2) /// 
		cells(b(fmt(3)) se(par) p) ///
		coeflabel(PR_post2 "\hspace{3mm} Exposure to Section \S936 \hspace{3mm} $\times$ Post") /// 
		nogap nomtitle tex wrap replace label nocons collabels(none) varwidth(40) nomtitle ///
		prefoot("") postf("\hline") posth("\hline \textbf{A. Investment-to-Capital Ratio} \\")
	

	esttab ETR_taximpute1_* using "$result/Tables/table_capex_base_indETR_postIV.tex", keep(ET_post2) ///  
		cells(b(fmt(3)) se(par) p) ///
		coeflabel(ET_post2 "\hspace{3mm} Exposure to Section \S936 \hspace{3mm} $\times$ Post") /// 
		tex wrap append label mlab(none) collabels(none) nonum varwidth(40) nomtitle /// 
		prefoot("") postfoot("\hline") preh("") posthead("\textbf{B. Effective Tax Rate} \\")

	esttab IV_PRpost_taximpute1_* using "$result/Tables/table_capex_base_indETR_postIV.tex", keep(tax_impute) ///  
		cells(b(fmt(3)) se(par) p) ///
		coeflabel(tax_impute "\hspace{3mm} Effective Tax Rate") /// 
		tex wrap append mlab(none) label  collabels(none) nonum  varwidth(40) nomtitle /// 
		prefoot("") posthead("\textbf{C. Semi-Elasticity of Investment} \\") /// 
		preh("") postfoot("\hline"  /// 
		"Size  $\times$ Year Fixed Effects                    & Y & Y & Y & Y & Y & Y & Y  \\    "      /// 
		"Firm Fixed Effects                          	   &   & Y & Y & Y & Y & Y & Y  \\    "      /// 
		"S936 Exposed Sector			               &   &   & Y &   &   &   &    \\    "      ///
		"S936 Exposed Industry                         &   &   &   & Y &   &   &    \\    "      /// 
		"Size $\times$ Industry $\times$ Year Fixed Effects        &   &   &   &   & Y & Y & Y  \\    "      /// 
		"DFL Weights                                   &   &   &   &   &   & Y &    \\    "      /// 
		"Entropy Balancing Weights                     &   &   &   &   &   &   & Y  \\    \hline\hline" /// 
		"\\ " /// 
		"\end{tabular} }" )
