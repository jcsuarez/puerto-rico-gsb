***************************************************************
//Filename: long_diff.do
//Author: eddie yu
//Date: 2023-09-20
//Task: This script conducts Stacked DiD and outputs estimates
***************************************************************


clear all
set more off
snapshot erase _all

global texsettings "posth("\midrule") preh("\begin{tabular}{lcccccc} \toprule")  postfoot("\midrule Size-by-Year FE     & Y & Y & Y & Y & Y &  \\ " "Sector-by-Year FE &   & Y & Y &   &   &  \\ " "Firm FE &   &   & Y &   &   &   \\ " "S936 Exposed Sector        &   &   &   & Y &   &   \\ " "S936 Exposed Industry      &   &   &   &   & Y &   \\ " "Sector-by-Size-by-Year FE  &   &   &   &   &   & Y  \\ " "DFL Weights &   &   &   &   &   & Y \\  \bottomrule" "\end{tabular}" ) replace sty(tex)"


********************************************************************************
***************************** "Stacked" Regression *****************************
********************************************************************************
use "$data/Replication Data/R&R2023/paper/onestep_compu_SOI_clean", clear
destring n2, replace
ren DFL_ppe DFL2

drop if gvkey==.
keep capex_base tax_impute gvkey year PR ppe95_bins10 n2 DFL2 exp_??? _webal

/*
forvalues yr = 1991/2006 {
	gen esPR_`yr' =(year==`yr')*PR
}
drop esPR_1995
*/

gen pre = year<1995
gen leaveout = year==1995
gen post1 = (year>1995)*(year<=2003)
gen post2 = (year>2003)

foreach var in pre leaveout post1 post2 {
	gen esPR_`var' =(`var'==1)*PR
}
drop esPR_leaveout


local i = 1
foreach var in capex_base tax_impute {
	preserve
		keep `var'  esPR_* gvkey year PR ppe95_bins10 n2 DFL2 exp_??? _webal
		rename * *_`i' // Index for each outcome 
		rename `var'_`i' outcome
		qui reg outcome esPR_* year i.ppe95_bins10 i.n2
		keep if e(sample)
		local i = `i' + 1
		di "`i'"
		
	tempfile `var'_sample
	save "``var'_sample'", replace
	restore
}

use "`capex_base_sample'", clear
append using "`tax_impute_sample'"



* making a single naics variable for clustering SE
gen gvkey = gvkey_1
replace gvkey = gvkey_2 if gvkey == .
	
gen DFL2 = DFL2_1
replace DFL2 = DFL2_2 if DFL2 == .

	
* filling in the missing values to let the regression run
foreach var of varlist *_1 {
	replace `var' = 0 if `var' == .
}	
foreach var of varlist *_2 {
	replace `var' = 0 if `var' == .
}	

foreach var in exp_sec exp_ind {
		replace `var'_1 = 1 if `var'_2==1
		replace `var'_2 = 1 if `var'_1==1
}



* running a stacked regression	
global spec1 ", a(PR_? ppe95_bins10_1##year_1 ppe95_bins10_2##year_2) cl(gvkey)	nocons"
global spec2 ", a(gvkey_? ppe95_bins10_1##year_1 ppe95_bins10_2##year_2) cl(gvkey) nocons"
global spec3 "if exp_sec_1 & exp_sec_2, a(PR_? ppe95_bins10_1##year_1 ppe95_bins10_2##year_2) cl(gvkey)	nocons"
global spec4 "if exp_ind_1 & exp_ind_2, a(PR_? ppe95_bins10_1##year_1 ppe95_bins10_2##year_2) cl(gvkey)	nocons"
global spec5 ", a(gvkey_? ppe95_bins10_1##year_1 ppe95_bins10_2##year_2 n2_1##ppe95_bins10_1##year_1 n2_2##ppe95_bins10_2##year_2) cl(gvkey) nocons"
global spec6 "[aw=DFL2], a(gvkey_? ppe95_bins10_1##year_1 ppe95_bins10_2##year_2 n2_1##ppe95_bins10_1##year_1 n2_2##ppe95_bins10_2##year_2) cl(gvkey) nocons"
global spec7 "[aw=_webal], a(gvkey_? ppe95_bins10_1##year_1 ppe95_bins10_2##year_2 n2_1##ppe95_bins10_1##year_1 n2_2##ppe95_bins10_2##year_2) cl(gvkey) nocons"
		
* Baseline
est clear
foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 spec7 {
	eststo etr_`spec' : reghdfe outcome esPR* $`spec'
		estadd scalar N_ = round(e(N)/2)
	est save "$output/R&R2023/ster/paper/longdiff_etr_`spec'", replace
	eststo inv_`spec': lincom _b[esPR_post2_1] / 100
		estadd scalar b_inv  r(estimate)
		estadd scalar b_se   r(se)
		estadd scalar b_p    r(p)
	est save "$output/R&R2023/ster/paper/longdiff_inv_`spec'", replace
	eststo elas_`spec' : nlcom _b[esPR_post2_1]/_b[esPR_post2_2], post
		estadd scalar N_ = round(e(N)/2)
	est save "$output/R&R2023/ster/paper/longdiff_elas_`spec'", replace
}	
esttab inv_spec?, drop(esPR_*) stats(b_inv b_se b_p, ///
layout(@ (@) @) labels("Investment" "S.E." "p-value" ) fmt(%9.3f %9.3f %9.3f %12.0f))  noobs nomtitle nonotes
esttab etr_spec?, keep(esPR_post2_2) cells(b(fmt(3)) se(par) p) nomtitle nodep noobs
esttab elas_spec?, cells(b(fmt(3)) se(par) p ) keep(_nl_1) stats(N_, layout(@) labels("N obs") fmt(%12.0f)) nomtitle nodep noobs



esttab elas_spec? using "$output/onestep/longdiff_SOI.tex", ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) noobs scalars(N_) ///
varlabel( _nl_1 "\hspace{1em} $\varepsilon^{K}_{\tau_{ETR}}$") $texsettings


// * Positive ETR
// replace outcome = max(0, outcome) if gvkey_2>0
// foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 {
// 	qui reghdfe outcome esPR* $`spec'
// 	eststo eps_posETR_`spec': nlcom _b[esPR_2006_1]/_b[esPR_2006_2], post
// 	estadd scalar N_ = round(e(N)/2)	
// }	
//
// esttab eps_posETR_spec? using "$output/onestep/longdiff_SOI_posETR.tex", ///
// cells(b(fmt(3)) se(par) p) mlab(none) coll(none) noobs scalars(N_ ) ///
// varlabel( _nl_1 "\hspace{1em} $\varepsilon^{K}_{\tau_{ETR}}$") $texsettings

