**************************************************************************
//Filename: tax_haven.do
//Author: eddie yu
//Date: 2023-09-20
//Task: This script creates table that computes Triple difference based on
//		whether the firm had an option for another tax haven pre/post 1995
**************************************************************************

clear all
set more off
snapshot erase _all 

use "$data/Replication Data/R&R2023/paper/tax_haven", replace


local fs_capx_name "Foregin Share"
local capex_base_name "I/K"

* interaction
xi i.year|PR , noomit
drop _IyeaXPR_1995 

forval i=1990(1)2014 { 
	capture: label var _IyeaXPR_`i' " "
} 
forval i=1991(2)2014 { 
	capture: label var _IyeaXPR_`i' "`i'"
}


**** Control versions ****
gen lninv = log(capx)
gen lnat = log(at)
gen ppe_scaled = ppe*1000
sum ppe_scaled ppent
gen lnppe = log(ppe)
gen lnppe2 = log(ppe_scaled)
gen lnppent = log(ppent)
gen lnintan = log(intan)
gen tangibility = ppent/at

* create 1995 bins for ppent, gpop (gross profit / total assets)
xtset gvkey year
xtile gp95_binDFL = gp if year == 1995 , n(5) 
xfill gp95_binDFL, i(gvkey)


*** haven usage
local pre_post_ind "pre"
gsort gvkey year
foreach Q in "th" "pr" "ir" "ba" "hk" "sp" "ci" "sw" {
	local X "`Q'_mention"
	replace `X' = 0 if missing(`X')
	local X_haven "any_`Q'"
	if "`pre_post_ind'" == "post" {
		di "Post 1995"
		bys gvkey: egen `X_haven' = max(`X') if year > 1995 // tax haven after 1995
	} 
	else {
		di "Pre 1995"
		bys gvkey: egen `X_haven' = max(`X') if year <= 1995 // tax haven before 1995
	}
	
	xfill `X_haven', i(gvkey)
}


*** table specs 
global th_name "Any Tax Haven"
global pr_name "Puerto Rico"
global ir_name "Ireland"
global ba_name "Barbados"
global hk_name "Hong Kong"
global sp_name "Singapore"
global ci_name "Cayman Islands"
global sw_name "Switzerland"


** table 
estimates clear

	eststo reg_base: reghdfe fs_capx PR##post , cl(gvkey) a(year)
		mat li r(table)
		
		estadd scalar beta_dbl  = r(table)[1, 8]
		estadd scalar se_dbl    = r(table)[2, 8]
		estadd scalar p_dbl     = r(table)[4, 8]
	
	estimates save "$output/R&R2023/ster/paper/haven_base", replace

local cnt = 0
local new_var_name "Triple Difference"
foreach haven in "th" "ir" "ba" "hk" "sp" "ci" "sw" {
	local cnt = `cnt' + 1
	local hvn_ind "any_`haven'"
	eststo reg_`haven': reghdfe fs_capx `hvn_ind'#PR##post, cl(gvkey) a(year)
		mat li r(table)
		
		estadd scalar beta_dbl  = r(table)[1, 10]
		estadd scalar se_dbl    = r(table)[2, 10]
		estadd scalar p_dbl     = r(table)[4, 10]
		estadd scalar beta_trpl = r(table)[1, 14]
		estadd scalar se_trpl   = r(table)[2, 14]
		estadd scalar p_trpl    = r(table)[4, 14]
		preserve
			keep gvkey `hvn_ind' PR
			duplicates drop
			sum `hvn_ind' if PR == 1
			estadd scalar pr_th_mean r(mean)
		restore
		
	estimates save "$output/R&R2023/ster/paper/haven_`haven'", replace
}

	eststo clear
	foreach i in "base" "th" "ir" "ba" "hk" "sp" "ci" "sw" { 
		estimates use "$output/R&R2023/ster/paper/haven_`i'"
		estimates store haven_`i'
	}
	
	esttab haven_* using "$overleaf/tables/fs_`pre_post_ind'_taxhaven.tex", drop(*) ///
	stats(beta_dbl se_dbl p_dbl beta_trpl se_trpl p_trpl N pr_th_mean, /// 
	layout(@ (@) @ @ (@) @ @) labels("Post $\times$ PR" " " " " ///
	"`pre_post_ind' 1995 Haven $\times$ Post $\times$ PR" " " " " ///
	"Total Number of Observations (Firm-Year)" "Share of PR Firms with `pre_post_ind' haven" ) ///
	fmt(%9.3f %9.3f %9.3f %9.3f %9.3f %9.3f %12.0f %9.3f ) star(beta_dbl beta_trpl)) ///
	mtitle("Baseline" "Any Haven" "Ireland" "Barbados" "Hong Kong" "Singapore" "Cayman Islands" ///
	"Switzerland") replace collabels(none) varwidth(32) nonotes ///
	star(* .1 ** .05 *** .01)
