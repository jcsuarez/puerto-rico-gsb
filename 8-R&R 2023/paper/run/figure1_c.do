// This file estimates semi-elasticities with SOI-estimated tax changes
// includes industry-level variation
// Author: Lysle Boller
// Date: April 25, 2020

clear all
set more off
snapshot erase _all
set seed 12345

***** FIGURE 1.C: FOR DIFFERENT DO FILE *****
use "$data/Replication Data/R&R2023/paper/SOI_imputed_ETR.dta", clear

* Industry shares of gross profits (in 1995)
egen gp_all = sum(gp_agg), by(year)
gen gp_share = gp_agg/gp_all

* 936 Shares
// egen credit_all = sum(Us_Possessions_Tax_Credit), by(year)
// gen cred_share= Us_Possessions_Tax_Credit/credit_all
// gen temp1 = cred_share*(year==1995)
// egen share936_95 = max(temp1), by(variable)


** Exposed Sectors: NAICS codes 22, 31-33, and 48-89
drop if variable == "wholesale_t_" | variable == "retail_t_" ///
	| variable == "ag_t_" | variable == "mining_t_" | variable == "construction_t_"

** Exposed Industries: NAICS codes 321-324, 327, 331, and 336-337.	
drop if variable == "man_wood_" | variable == "man_paper_" ///
	| variable == "man_printing_" | variable == "man_petrol_" ///
	| variable == "man_minerals_" | variable == "man_primary_metals_" ///
	| variable == "man_trans_" | variable == "man_furniture_"
	
* Average across industries
// keep if indshr>0.1
collapse (mean) p_data p_new p_new_fixp [aw=gp_share], by(year) 
*collapse (mean) p_data p_new p_new_fixp , by(year) 
keep if year > 1994 & year < 2008
sort year
sum p_data if year==1995
local p1 = r(mean)
sum p_new if year==1995
local p2 = r(mean)
sum p_new_fixp if year==1995
local p3 = r(mean)
	
replace p_new = p_new - `p2' + `p1'
replace p_new_ = p_new_ - `p3' + `p1'


twoway (scatter p_data year, c(line) lpattern(dash) ) ///
	   (scatter p_new_ year, c(line) lpattern(solid)) ///
	   , xlab(1995(2)2007) ///
		graphregion(fcolor(white)) bgcolor(white) ///
		xtitle("Year") ylab(0.20(0.02)0.32) ///
		ytitle("Effective Tax Rate for Exposed Sectors", margin(medsmall)) ///
		legend(order(2 1) pos(6) ///
		label(1 "Unexposed Firms") ///
		label(2 "S936 Firms") rows(1) ) 	
		
		
graph export "$result/Figures/SOI_ETR_Observed.pdf", replace  	 	

* Report relative difference in 2007 *
sum p_data if year==2007, meanonly
local p1 = r(mean)
sum p_new_fixp if year==2007, meanonly
local p3 = r(mean)	
	
dis round(100*(`p3' - `p1'),.01)
