***************************************************************
//Filename: construct_SOI_NAICS_xwalk.do
//Author: Kevin Roberts
//Date: 2023-09-20
//Task: This script creates crosswalk between SOI industry codes and NAICS
**************************************************************

clear all
set more off
snapshot erase _all

use "$SOI/aggregate_SOI", clear
split NAICS02, parse(,  )
keep variable NAICS*
drop NAICS02

duplicates drop
replace NAICS021 = "312" if NAICS021 == "312-312"
replace NAICS022 = "313" if NAICS021 == "313-314"
replace NAICS021 = "314" if NAICS021 == "313-314"
replace NAICS022 = "32" if NAICS021 == "31-33"
replace NAICS023 = "33" if NAICS021 == "31-33"
replace NAICS021 = "31" if NAICS021 == "31-33"
replace NAICS022 = "45" if NAICS021 == "44-45"
replace NAICS021 = "44" if NAICS021 == "44-45"
replace NAICS023 = "49" if NAICS022 == " 48-49"
replace NAICS022 = "48" if NAICS022 == " 48-49"

reshape long NAICS0, i(variable) j(num)
rename NAICS0 NAICS

drop num
drop if NAICS == ""
rename NAICS n3
replace n3 = strtrim(n3) // test why merge didn't work

rename n3 naics_ind
sort naics_ind

drop if naics_ind=="334" & variable=="man_mach_et_al_" // ad-hoc change for indeterminate match

save "$output/NAICS_crosswalk_SOI", replace
