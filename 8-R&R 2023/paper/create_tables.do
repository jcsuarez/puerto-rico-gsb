***************************************************************
//Filename: create_tables.do
//Author: eddie yu
//Date: 2023-09-20
//Task: This script creates tables for overleaf export
***************************************************************

clear all
set more off
snapshot erase _all


	cd "$ster"

********************************* Make Tables **********************************
	
	* Table - Compustat : Fine Comparison
	eststo clear
	*** Load Long Diff Estimates ***
	forval j = 1/7 {
		*** Long Diff Regression Elasticity ***
		estimates use longdiff_elas_spec`j'
		estimates store elas_spec`j'
		*** Long Diff Regression ETR ***
		estimates use longdiff_etr_spec`j'
		estimates store etr_spec`j'
		*** Long Diff Regression Investment / 100 ***
		estimates use longdiff_inv_spec`j'
		estimates store inv_spec`j'
	}
	
	esttab inv_spec* using "$overleaf/tables/compu_stackedDiD.tex", drop(esPR_*) ///
		stats(b_inv b_se b_p, layout(@ (@) @) labels("Investment/100" " " " ") ///
		fmt(%9.3f %9.3f %9.3f %12.0f) star(b_inv)) ///
		nogap nomtitle tex wrap replace label nocons collabels(none) varwidth(32) nomtitle ///
		star(* .1 ** .05 *** .01)      /// 
		prefoot("") postfoot("")
	
	
	esttab etr_spec* using "$overleaf/tables/compu_stackedDiD.tex", keep(esPR_post2_2) ///
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(esPR_post2_2 "ETR (Stacked DiD)") ///
		tex wrap append label mlab(none) collabels(none) nonum varwidth(32) nomtitle noobs /// 
		star(* .1 ** .05 *** .01)      /// 
		prefoot("") postfoot("") preh("") 
	
	esttab elas_spec* using "$overleaf/tables/compu_stackedDiD.tex", keep(_nl_1) ///
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(_nl_1 "Elasticity") ///
		stats(N_, layout(@) labels("Observations") fmt(%12.0f)) ///
		tex wrap append mlab(none) label collabels(none) nonum varwidth(32) nomtitle /// 
		star(* .1 ** .05 *** .01) prefoot("") /// 
		preh("") postfoot("\hline"  /// 
		"Size-by-Year Fixed Effects                    & Y & Y & Y & Y & Y & Y & Y  \\    "      /// 
		"Firm Fixed Effects                            &   & Y & Y & Y & Y & Y & Y  \\    "      /// 
		"S936 Exposed Sector			               &   &   & Y &   &   &   &    \\    "      ///
		"S936 Exposed Industry                         &   &   &   & Y &   &   &    \\    "      /// 
		"Size-by-Industry-by-Year Fixed Effects        &   &   &   &   & Y & Y & Y  \\    "      /// 
		"DFL Weights                                   &   &   &   &   &   & Y &    \\    "      /// 
		"Entropy Balancing Weights                     &   &   &   &   &   &   & Y  \\    \hline\hline" /// 
		"\\ " /// 
		"\end{tabular} }" )
		

	* Table - Elasticity
	eststo clear
	*** Load Elasticity Estimates ***
	forval j = 1/7 {
		*** Long Diff Regression Elasticity ***
		estimates use longdiff_elas_spec`j'
		estimates store elas_spec`j'
		*** Semi-Elasticity ***
		estimates use semi_elas_spec`j'
		estimates store semi_elas_spec`j'
		*** PR-by-Year instruments ***
		estimates use PR_by_year_spec`j'
		estimates store PR_by_year_spec`j'
		*** PR-by-Year instruments, Positive ETR ***
		estimates use PR_by_year_posETR_spec`j'
		estimates store PR_by_year_posETR_spec`j'
	}
	
	
	esttab elas_spec* using "$overleaf/tables/compu_elasticity.tex", keep(_nl_1) ///
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(_nl_1 "Elasticity") ///
		nogap nomtitle tex wrap replace label nocons collabels(none) varwidth(32) nomtitle noobs ///
		star(* .1 ** .05 *** .01)      /// 
		prefoot("") postfoot("")
	
	
	esttab semi_elas_spec* using "$overleaf/tables/compu_elasticity.tex", keep(tax_impute) ///
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(tax_impute "Semi-Elasticity w.r.t. ETR") ///
		tex wrap append label mlab(none) collabels(none) nonum varwidth(32) nomtitle noobs /// 
		star(* .1 ** .05 *** .01)      /// 
		prefoot("") postfoot("") preh("") 
		
	esttab PR_by_year_spec* using "$overleaf/tables/compu_elasticity.tex", keep(tax_impute) ///
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(tax_impute "PR-by-Year Instrument") ///
		tex wrap append label mlab(none) collabels(none) nonum varwidth(32) nomtitle noobs /// 
		star(* .1 ** .05 *** .01)      /// 
		prefoot("") postfoot("") preh("") 
		
	esttab PR_by_year_posETR_spec* using "$overleaf/tables/compu_elasticity.tex", keep(tax_impute) ///
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(tax_impute "PR-by-Year Instrument, positive ETR") ///
		tex wrap append mlab(none) label collabels(none) nonum varwidth(32) nomtitle /// 
		star(* .1 ** .05 *** .01) prefoot("") /// 
		preh("") postfoot("\hline"  /// 
		"Size-by-Year Fixed Effects                    & Y & Y & Y & Y & Y & Y & Y  \\    "      /// 
		"Firm Fixed Effects                            &   & Y & Y & Y & Y & Y & Y  \\    "      /// 
		"S936 Exposed Sector			               &   &   & Y &   &   &   &    \\    "      ///
		"S936 Exposed Industry                         &   &   &   & Y &   &   &    \\    "      /// 
		"Size-by-Industry-by-Year Fixed Effects        &   &   &   &   & Y & Y & Y  \\    "      /// 
		"DFL Weights                                   &   &   &   &   &   & Y &    \\    "      /// 
		"Entropy Balancing Weights                     &   &   &   &   &   &   & Y  \\    \hline\hline" /// 
		"\\ " /// 
		"\end{tabular} }" )	
		
		
	* Table - Investment
	eststo clear
	foreach i in "table2" "tableA5" "tableA6" { 
		forval j = 1/7 {
			estimates use `i'_`j'
			estimates store `i'_spec`j'
		}
	}
	
	
	esttab table2_spec* using "$overleaf/tables/compu_investment.tex", keep(PR_post2) ///
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(PR_post2 "Exposure to Section 936 X Post") ///
		nogap nomtitle tex wrap replace label nocons collabels(none) varwidth(32) nomtitle noobs ///
		star(* .1 ** .05 *** .01)      /// 
		prefoot("") postfoot("")
	
	
	esttab tableA5_spec* using "$overleaf/tables/compu_investment.tex", keep(PR_post2) ///
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(PR_post2 "Exposure to Section 936 X Post") ///
		tex wrap append label mlab(none) collabels(none) nonum varwidth(32) nomtitle noobs /// 
		star(* .1 ** .05 *** .01)      /// 
		prefoot("") postfoot("") preh("") 
		
	esttab tableA6_spec* using "$overleaf/tables/compu_investment.tex", keep(PR_post2) ///
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(PR_post2 "Exposure to Section 936 X Post") ///
		tex wrap append mlab(none) label collabels(none) nonum varwidth(32) nomtitle /// 
		star(* .1 ** .05 *** .01) prefoot("") /// 
		preh("") postfoot("\hline"  /// 
		"Size-by-Year Fixed Effects                    & Y & Y & Y & Y & Y & Y & Y  \\    "      /// 
		"Firm Fixed Effects                            &   & Y & Y & Y & Y & Y & Y  \\    "      /// 
		"S936 Exposed Sector			               &   &   & Y &   &   &   &    \\    "      ///
		"S936 Exposed Industry                         &   &   &   & Y &   &   &    \\    "      /// 
		"Size-by-Industry-by-Year Fixed Effects        &   &   &   &   & Y & Y & Y  \\    "      /// 
		"DFL Weights                                   &   &   &   &   &   & Y &    \\    "      /// 
		"Entropy Balancing Weights                     &   &   &   &   &   &   & Y  \\    \hline\hline" /// 
		"\\ " /// 
		"\end{tabular} }" )	
		
