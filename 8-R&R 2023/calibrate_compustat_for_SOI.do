***************************************************************
//Filename: calibrate_compustat_for_SOI.do
//Author: eddie yu
//Date: 2023-09-20
//Task: This script calculates share of capex
**************************************************************

clear all
set more off 
snapshot erase _all

* baseline data 
use "$data/Replication Data/compu_NETS_ETR.dta", clear
xtset gvkey year
gsort gvkey year

collapse (sum) capx if year==1995, by(n3 PR)
by n3: egen sum_capx = sum(capx)
gen indshr_capx = capx / sum_capx

* impute 0 for non-naics data
by n3: egen cnt = count(PR)
preserve
	tempfile impute
	keep if cnt == 1
	expand 2, gen(dup)
	replace indshr_capx = 0 if dup == 1
	drop if dup != 1
	drop dup
	replace PR = 1
	replace cnt = 2
	save `impute'
restore 

append using "`impute'"
drop cnt
gsort n3 PR
tab n3 PR, summ(indshr_capx)
save "$data/Replication Data/compu_PR_indshr_capx.dta", replace


// bys n3: egen ind_capx = sum(capx) if year == 1995
// bys n3 PR: egen ind_PRcapx = sum(capx) if year == 1995
//
// tab n3, summ(ind_capx)
// tab n3 PR, summ(indshr_PRcapx)
//
//
// by variable: PR
