clear all
set more off
snapshot erase _all 


global dropbox "/Users/jisangyu/JC's Data Emporium Dropbox/Eddie Yu/Puerto Rico"

** Install Packages 
// ssc install listtex, replace all 
// ssc install binscatter, replace all
// ssc install estout, replace all 
// ssc install cdfplot, replace all
// ssc install winsor, replace all
// ssc install reghdfe, replace all
// ssc install tuples, replace all
// ssc install coefplot, replace all 
// ssc install spmap, replace all 
// ssc install maptile, replace all 
// ssc install ivreg2, replace all 
// ssc install ftools, replace all
// ssc install ivreghdfe, replace all 
// ssc install ivreg2hdfe, replace all
// ssc install reg2hdfe, replace all 
// ssc install moremata, replace all
// ssc install tmpdir, replace all
// ssc install ranktest, replace all
// ssc install dataout, replace
// maptile_install using "http://files.michaelstepner.com/geo_county1990.zip"
// maptile_install using "http://files.michaelstepner.com/geo_state.zip"

** Global Dropbox Paths (raw to clean data)
global output 		"$dropbox/Programs/output"
global data 		"$dropbox/Data"
global analysis 	"$dropbox/Programs"
global linkdata		"$dropbox/Programs/output/base_1995"
global xwalk		"$dropbox/Data/Cross Walk"
global qcewdata		"$dropbox/Data/QCEW"
global bea			"$dropbox/Data/BEA"
global consp		"$dropbox/Data/conspuma"
global additional 	"$dropbox/Data/data_additional"
global output_NETS 	"$dropbox/Programs/output/NETS_20171128"
global output_NETS2 "$dropbox/Programs/output/NETS_20171129_v2"
global output_NETS_control "$dropbox/Programs/output/NETS_control"
global WRDS			"$dropbox/Data/WRDS"
global ASM			"$dropbox/Data/ASM"
global SOI			"$dropbox/Data/SOI Data"
global irs 			"$dropbox/Data/IRS"
global randr		"$analysis/8-R&R 2023/referee_report"
global overleaf		"/Users/jisangyu/JC's Data Emporium Dropbox/Eddie Yu/앱/Overleaf/PR table"
global ster			"$output/R&R2023/ster"

cd "$analysis"

* Build data: Make data from newly pulled compustat historical segments data
do "$randr/build/compustat_merge_manual.do"
do "$randr/build/compustat_ETR_calc_segments.do"

* Full Sample, No Bad Controls

	** BUILD DATA **
	/* This is a critical do file that creates DFL weights exactly same as original paper
	   above, and generates compu_NETS_ETR.dta to be used for *_clean.do, and onestep*.do */ 
	do "$randr/clean/clean_compu_for_analysis.do"
	
	do "$randr/clean/clean_compu_for_analysis_sizecutoff.do" 
	/* produces compu_NETS_ETR_sizecutoff.dta, which will be used in pr_treat_taxhaven_clean.do */
	
	do "$randr/clean/table2_clean.do" // creates table 2, 5, A5, A6 data
	do "$randr/clean/table4_clean.do" // creates table 4, A8, A9 data
	do "$randr/clean/figure5_clean.do" // creates figure 5, figure 6, figure A4, A5, A6, A7 data
	do "$randr/clean/pr_treat_taxhaven_clean.do"
	
	** CREATE TABLES **
	do "$randr/run/table2.do" 
	do "$randr/run/table4.do" 
	do "$randr/run/table5.do" 
	do "$randr/run/tableA5.do" 
	do "$randr/run/tableA6.do" 
	do "$randr/run/tableA8.do"
	do "$randr/run/tableA9.do"
	do "$randr/run/tax_haven.do"


	** CREATE FIGURES **
	do "$randr/run/figure5.do" 
	do "$randr/run/figure6.do" 
	
	
	** MAKE TABLES **
	do "$randr/create_tables.do"
	
	
		** CREATES NARROW COMPARISON DATA **
		do "$randr/narrow_table/table2_clean.do" // creates table 2, 5, A5, A6 data
		do "$randr/narrow_table/table4_clean.do" // creates table 4, A8, A9 data
		do "$randr/narrow_table/figure5_clean.do" // creates table 4, A8, A9 data
		do "$randr/narrow_table/table2.do" 
		do "$randr/narrow_table/table4.do" 
		do "$randr/narrow_table/table5.do" 
		do "$randr/narrow_table/tableA5.do" 
		do "$randr/narrow_table/tableA6.do" 
		do "$randr/narrow_table/tableA8.do"
		do "$randr/narrow_table/tableA9.do"
		do "$randr/narrow_table/figure5.do"
		do "$randr/narrow_table/create_tables_narrow.do"
		