***************************************************************
//Filename: figure6.do
//Author: eddie yu
//Date: 2023-09-20
//Task: This script creates figure6 under different specifications
***************************************************************

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data/R&R2023/figure6_data", clear

*** labels for some graphs
local fs_capx_name "Foreign Share Investment"
local capex_base_name "Investment/Physical Capital"
local capex_base_p_name "% Change in Investment/Physical Capital"
local capex_base_1_name "Investment/Physical Capital"
local ftax_name "Federal Tax Rate"
local cf_ppe_1_name "Foreign Investment / 1995 PPE"

* combining years and N2
egen yearg = group(year n2)

*figures 5, 6, A5, and A6
xi i.year|PR , noomit
drop _IyeaXPR_1995 

forval i=1990(1)2014 { 
	capture: label var _IyeaXPR_`i' " "
} 
forval i=1991(2)2014 { 
	capture: label var _IyeaXPR_`i' "`i'"
} 

snapshot save

* Approach 0922: 3 columns
global level "PR"
global spec1 ", cl(gvkey) a($level year)"
global spec2 "[aw=DFL_ppe], cl(gvkey) a($level  n2##year)" 

xtset gvkey year

global specname1 "(1) Year FE"
global specname6 "(2) DFL Weights & Exposed Industry: Industry-by-Year FE"

xtset gvkey year



global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"
global c7 "olive"


*** Store estimates ***
* create shares of I/K 
gen lcfor_capx = log(cfor_capx)
gen cf_ppe = cfor_capx/ppe95
gen lcf_ppe = log(cfor_capx/ppe95)
gen cf_lppe = cfor_capx/log(ppe95)
replace cf_ppe = 100*cf_ppe


return clear
summ cf_ppe, d
putexcel set "$output/R&R2023/ster/referee_report/figure6_cf_ppe.xlsx", replace
putexcel A1 = "Mean"
putexcel A2 = "Var"
putexcel A3 = "sd"
putexcel A4 = "min"
putexcel A5 = "p1"
putexcel A6 = "p10"
putexcel A7 = "p25"
putexcel A8 = "p50"
putexcel A9 = "p75"
putexcel A10 = "p90"
putexcel A10 = "p99"
putexcel A11 = "max"
putexcel B1 = "`r(mean)'"
putexcel B2 = "`r(Var)'"
putexcel B3 = "`r(sd)'"
putexcel B4 = "`r(min)'"
putexcel B5 = "`r(p1)'"
putexcel B6 = "`r(p10)'"
putexcel B7 = "`r(p25)'"
putexcel B8 = "`r(p50)'"
putexcel B9 = "`r(p75)'"
putexcel B10 = "`r(p90)'"
putexcel B10 = "`r(p99)'"
putexcel B11 = "`r(max)'"

*winsorizing
foreach Q of var cf_ppe {
//domestic_IK domestic_IK_base
sum `Q', d

gen `Q'_1 = `Q'

egen temp1 = pctile(`Q'), p(1) 
egen temp2 = pctile(`Q'), p(99)
replace `Q'_1 = temp1 if `Q'_1 < temp1 & `Q' != .
replace `Q'_1 = temp2 if `Q'_1 > temp2 & `Q' != .
capture: drop temp1 temp2

sum `Q', d
replace `Q' = r(p5) if `Q' < r(p5) & `Q' != .
replace `Q' = r(p95) if `Q' > r(p95) & `Q' != .

}

local y "cf_ppe_1"
summ `y', d
local cnt = 0
foreach spec in spec1  spec2 {

local cnt = `cnt' + 1 // regression counts
reghdfe `y' _IyeaX* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/`cnt' {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
		, yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(size(small) pos(6)  r(4) on order (1 3) ///
		label(1 "$specname1")  ///
		label(3 "$specname2") ) ///
		yti("Effect of S936 on ``y'_name'", margin(medium))
restore	
}

graph export "$overleaf/Graphs/referee_report/figure6_`y'.pdf", replace 

