
******************************** Set Directory *********************************

foreach path in referee_report {
	cd "$ster/`path'"
	eststo clear

	******************************** Load Estimates ********************************
	*** Call the non-SOI tables ***
	foreach i in "table2" "tableA5" "tableA6" "table4" "tableA8" "tableA9" "table5" {
		forval j = 1/6 {
			estimates use `i'_`j'
			estimates store `i'_spec`j'
		}
	}


	********************************* Make Tables **********************************
	*** First Table *** 
	esttab table2_spec* using "$overleaf/table_`path'.tex", keep(PR_post) /// 
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(PR_post "Investment-to-Capital Ratio (Previous Table 2)") /// 
		nogap nomtitle tex wrap replace label nocons collabels(none) varwidth(32) nomtitle ///
		star(* .1 ** .05 *** .01)      /// 
		prefoot("") postfoot("")

	esttab tableA5_spec* using "$overleaf/table_`path'.tex", keep(PR_post) ///  
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(PR_post "Percent Change in Investment  (Previous Table A.5)") /// 
		tex wrap append label mlab(none) collabels(none) nonum varwidth(32) nomtitle /// 
		star(* .1 ** .05 *** .01)      /// 
		prefoot("") postfoot("") preh("") 

	esttab tableA6_spec* using "$overleaf/table_`path'.tex", keep(PR_post) ///  
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(PR_post "Investment-to-Capital Ratio, Winsorized at 1\% \newline (Previous Table A.6)") /// 
		tex wrap append mlab(none) label  collabels(none) nonum  varwidth(32) nomtitle /// 
		star(* .1 ** .05 *** .01)      /// 
		prefoot("") postfoot("") preh("") 
		
	esttab table4_spec* using "$overleaf/table_`path'.tex", keep(PR_post) ///  
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(PR_post "Federal Taxes Paid as a Percent of Global Pretax Income \\ (Previous Table 4)") /// 
		tex wrap append label mlab(none) collabels(none) nonum  varwidth(32) nomtitle ///
		star(* .1 ** .05 *** .01)      /// 
		prefoot("") postfoot("") preh("")
		
		
	esttab tableA8_spec* using "$overleaf/table_`path'.tex", keep(PR_post) ///  
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(PR_post "Federal Tax Paid as Percent of Pretax Income \hfill \break (Previous Table A.8)") /// 
		tex wrap append mlab(none) label  collabels(none) nonum  varwidth(32) nomtitle ///
		star(* .1 ** .05 *** .01)      /// 
		prefoot("") postfoot("") preh("")
		
	esttab tableA9_spec* using "$overleaf/table_`path'.tex", keep(PR_post) ///  
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(PR_post "Change in Total Effective Tax Rate  \\ (Previous Table A.9)") /// 
		tex wrap append label mlab(none) collabels(none) nonum varwidth(32) nomtitle /// 
		star(* .1 ** .05 *** .01) 	  /// 
		prefoot("") postfoot("") preh("")  /// 

	esttab table5_spec* using "$overleaf/table_`path'.tex", keep(PR_post) ///  
		cells(b(fmt(3) star) se(par) p) ///
		coeflabel(PR_post "Foreign Share of Investment (Previous Table 5)") /// 
		tex wrap append mlab(none) label collabels(none) nonum varwidth(32) nomtitle /// 
		star(* .1 ** .05 *** .01) prefoot("") /// 
		preh("") postfoot("\hline"  /// 
		"Year Fixed Effects         		& Y & Y & Y & Y & Y & Y  \\    "      /// 
		"NAICS-by-Year Fixed Effects        &   & Y & Y & Y & Y & Y  \\    "      /// 
		"Firm Fixed Effects					&   &   & Y & Y & Y & Y  \\    "      /// 
		"S936 Exposed Sector                &   &   &   & Y & Y & Y  \\    "      /// 
		"S936 Exposed Industry              &   &   &   &   & Y & Y  \\    "      /// 
		"DFL Weights						&   &   &   &   &   & Y  \\  \hline\hline" /// 
		"\\ " /// 
		"\end{tabular} }" )

	

}
