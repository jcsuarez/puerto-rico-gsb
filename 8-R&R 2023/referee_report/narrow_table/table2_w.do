***************************************************************
//Filename: table2.do
//Author: eddie yu
//Date: 2023-09-20
//Task: This script creates table2 under different specifications
***************************************************************

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data/R&R2023/table2_data", clear

* formatting, labeling, and descriptive scalar creation
local FE_fs_capx "n3"
local FE_capex_base "gvkey"
local FE_capex_base_p "gvkey"
local FE_capex_base_1 "gvkey"
local FE_ftax "gvkey"

local FE_fs_capx_word "Industry"
local FE_capex_base_word "Firm"
local FE_capex_base_p_word "Firm"
local FE_capex_base_1_word "Firm"
local FE_ftax_word "Firm"

local fs_capx_sum "fs_capx"
local capex_base_sum "IK"
local capex_base_p_sum "IKIK"
local capex_base_1_sum "IK_1"
local ftax_sum "ftax"

local fs_capx_scalars1 ""
local capex_base_scalars1 "avg Sample Average I/K in 2006" 
local capex_base_p_scalars1 "avg Sample Average I/K in 2006 Relative to 1995"
local capex_base_1_scalars1 "avg Sample Average I/K in 2006"
local ftax_scalars1 ""

local fs_capx_scalars2 ""
local capex_base_scalars2 "elas Percent of 2006 Average"
local capex_base_p_scalars2 "elas Percent of 2006 Average"
local capex_base_1_scalars2 "elas Percent of 2006 Average"
local ftax_scalars2 ""

local fs_capx_scalars3 ""
local capex_base_scalars3 "elast Semi-elasticity of Investment"
local capex_base_p_scalars3 "elast Semi-elasticity of Investment"
local capex_base_1_scalars3 "elast Semi-elasticity of Investment"
local ftax_scalars3 ""

local fs_capx_scalars4 ""
local capex_base_scalars4 "pp_dif Change in Effective Tax Rate"
local capex_base_p_scalars4 "pp_dif Change in Effective Tax Rate"
local capex_base_1_scalars4 "pp_dif Change in Effective Tax Rate"
local ftax_scalars4 ""

local fs_capx_title "Change in Foreign Share of Investment"
local capex_base_title "Change in Investment: $ \frac{I}{K_{1990-1995}} $"
local capex_base_p_title "Percent Change in Investment: $ \frac{I}{I_{1990-1995}} - 1 $"
local capex_base_1_title "Change in Investment: $ \frac{I}{K_{1990-1995}} $"
local ftax_title "Change in Federal Taxes Paid as a Percent of Pretax Income"
xtset gvkey year

* scalar values from SOI data analysis:
scalar p_dif_v0 = .0572734
scalar p_dif_v1 = .0699663
scalar p_dif_v2 = .0465333
scalar p_dif_v3 = .0601001


snapshot save

estimates clear
local winsor_end_val = 5
* run reg, output table
	forvalues w = 1/`winsor_end_val' {
		snapshot restore 1
		local pw = `w' * 0.01
		* level of winsorization 
		local Q "capex_base"
// 		local w_ = 100 - `w'
// 		local w_l "p`w'"
// 		local w_r "p`w_'"
//
// 		estimates clear
// 		summ `Q', d
// 		replace `Q' = r(`w_l') if `Q' < r(`w_l') & `Q' != .
// 		replace `Q' = r(`w_r') if `Q' > r(`w_r') & `Q' != .
		summ `Q', d
		winsor `Q', p(`pw') gen(`Q'_w`w')
		summ `Q', d
		
		qui {
		local i = 0
		foreach y of var `Q'_w`w' {	

			local i = `i' + 1
			eststo reg_`w'_`i': areg `y' ib(last).year PR PR_post   , cl(gvkey)   a(year)
			lincom PR_post
			sum ``y'_sum' if e(sample) & year == 2006, d
				estadd local avg = string(r(mean), "%8.3fc")
				estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
				estadd local elast = string(-_b[PR_post] / r(mean) / p_dif_v0, "%8.2fc")
				estadd local pp_dif = string(p_dif_v0*100, "%8.3fc")
			est save "$output/R&R2023/ster/referee_report/table2_w`w'_`i'", replace
			
			local i = `i' + 1
			eststo reg_`w'_`i': areg `y' ib(last).year#i.n2 PR PR_post   , cl(gvkey)   a(year)
			lincom PR_post
			sum ``y'_sum' if e(sample) & year == 2006, d
				estadd local avg = string(r(mean), "%8.3fc")
				estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
				estadd local elast = string(-_b[PR_post] / r(mean) / p_dif_v0, "%8.2fc")
				estadd local pp_dif = string(p_dif_v0*100, "%8.3fc")
			est save "$output/R&R2023/ster/referee_report/table2_w`w'_`i'", replace
				
			local i = `i' + 1
			eststo reg_`w'_`i': xtreg `y' ib(last).year#i.n2 PR PR_post   ,  vce(cl gvkey)  fe
			lincom PR_post
			sum ``y'_sum' if e(sample) & year == 2006, d
				estadd local avg = string(r(mean), "%8.3fc")
				estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
				estadd local elast = string(-_b[PR_post]  / r(mean) / p_dif_v0, "%8.2fc")
				estadd local pp_dif = string(p_dif_v0*100, "%8.3fc")
			est save "$output/R&R2023/ster/referee_report/table2_w`w'_`i'", replace
			local i = `i' + 1
			eststo reg_`w'_`i': xtreg `y' ib(last).year#i.n2 PR PR_post    if  exp_sec , vce(cl gvkey)  fe
			lincom PR_post
			sum ``y'_sum' if e(sample) & year == 2006, d
				estadd local avg = string(r(mean), "%8.3fc")
				estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
				estadd local elast = string(-_b[PR_post]  / r(mean) / p_dif_v2, "%8.2fc")
				estadd local pp_dif = string(p_dif_v2*100, "%8.3fc")
			est save "$output/R&R2023/ster/referee_report/table2_w`w'_`i'", replace

			local i = `i' + 1
			eststo reg_`w'_`i': xtreg  `y' ib(last).year#i.n2 PR PR_post     if  exp_ind , vce(cl gvkey)  fe
			lincom PR_post
			sum ``y'_sum' if e(sample) & year == 2006, d
				estadd local avg = string(r(mean), "%8.3fc")
				estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
				estadd local elast = string(-_b[PR_post]  / r(mean) / p_dif_v3, "%8.2fc")
				estadd local pp_dif = string(p_dif_v3*100, "%8.3fc")
			est save "$output/R&R2023/ster/referee_report/table2_w`w'_`i'", replace
			
			local i = `i' + 1
			eststo reg_`w'_`i': xtreg  `y' ib(last).year#i.n2 PR PR_post   [aw=DFL_ppe]  if exp_ind  , vce(cl gvkey)  fe
			lincom PR_post
			sum ``y'_sum'  if e(sample) & year == 2006, d
				estadd local avg = string(r(mean), "%8.3fc")
				estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
				estadd local elast = string(-_b[PR_post]  / r(mean) / p_dif_v3, "%8.2fc")
				estadd local pp_dif = string(p_dif_v3*100, "%8.3fc")
			est save "$output/R&R2023/ster/referee_report/table2_w`w'_`i'", replace

		}
		
	}

* create tables
cd "$ster/referee_report"
eststo clear

local t "table2"
	forval w = 1/`winsor_end_val' {
		forval j = 1/6 {
			estimates use `t'_w`w'_`j'
			estimates store `t'_w`w'_`j'
		}
	}

local w = 1
esttab table2_w`w'_* using "$overleaf/tables/`t'/winsor.tex", keep(PR_post) /// 
	cells(b(fmt(3) star) se(par) p) ///
	coeflabel(PR_post "Table 2 Winsorized at `w' \%") /// 
	nogap nomtitle tex wrap replace label nocons collabels(none) varwidth(32) nomtitle ///
	star(* .1 ** .05 *** .01)      /// 
	prefoot("") postfoot("")
	
local w = `w' + 1
esttab table2_w`w'_* using "$overleaf/tables/`t'/winsor.tex", keep(PR_post) /// 
	cells(b(fmt(3) star) se(par) p) ///
	coeflabel(PR_post "Table 2 Winsorized at `w' \%") /// 
	tex wrap append label mlab(none) collabels(none) nonum varwidth(32) nomtitle /// 
	star(* .1 ** .05 *** .01)      /// 
	prefoot("") postfoot("") preh("") 

local w = `w' + 1
esttab table2_w`w'_* using "$overleaf/tables/`t'/winsor.tex", keep(PR_post) /// 
	cells(b(fmt(3) star) se(par) p) ///
	coeflabel(PR_post "Table 2 Winsorized at `w' \%") /// 
	tex wrap append label mlab(none) collabels(none) nonum varwidth(32) nomtitle /// 
	star(* .1 ** .05 *** .01)      /// 
	prefoot("") postfoot("") preh("") 

local w = `w' + 1
esttab table2_w`w'_* using "$overleaf/tables/`t'/winsor.tex", keep(PR_post) /// 
	cells(b(fmt(3) star) se(par) p) ///
	coeflabel(PR_post "Table 2 Winsorized at `w' \%") /// 
	tex wrap append label mlab(none) collabels(none) nonum varwidth(32) nomtitle /// 
	star(* .1 ** .05 *** .01)      /// 
	prefoot("") postfoot("") preh("") 

local w = `w' + 1
esttab table2_w`w'_* using "$overleaf/tables/`t'/winsor.tex", keep(PR_post) /// 
	cells(b(fmt(3) star) se(par) p) ///
	coeflabel(PR_post "Table 2 Winsorized at `w' \%") /// 
		tex wrap append mlab(none) label collabels(none) nonum varwidth(32) nomtitle /// 
		star(* .1 ** .05 *** .01) prefoot("") /// 
		preh("") postfoot("\hline"  /// 
		"Year Fixed Effects         		& Y & Y & Y & Y & Y & Y  \\    "      /// 
		"NAICS-by-Year Fixed Effects        &   & Y & Y & Y & Y & Y  \\    "      /// 
		"Firm Fixed Effects					&   &   & Y & Y & Y & Y  \\    "      /// 
		"S936 Exposed Sector                &   &   &   & Y & Y & Y  \\    "      /// 
		"S936 Exposed Industry              &   &   &   &   & Y & Y  \\    "      /// 
		"DFL Weights						&   &   &   &   &   & Y  \\  \hline\hline" /// 
		"\\ " /// 
		"\end{tabular} }" )
