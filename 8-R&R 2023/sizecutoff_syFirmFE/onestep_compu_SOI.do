// This file estimates semi-elasticities with SOI-estimated tax changes
// includes industry-level variation
// Author: Lysle Boller
// Date: April 25, 2020

clear all
set more off
snapshot erase _all

global texsettings "posth("\midrule") preh("\begin{tabular}{lcccccc} \toprule")  postfoot("\midrule Size-by-Year FE     & Y & Y & Y & Y & Y &  \\ " "Sector-by-Year FE &   & Y & Y &   &   &  \\ " "Firm FE &   &   & Y &   &   &   \\ " "S936 Exposed Sector        &   &   &   & Y &   &   \\ " "S936 Exposed Industry      &   &   &   &   & Y &   \\ " "Sector-by-Size-by-Year FE  &   &   &   &   &   & Y  \\ " "DFL Weights &   &   &   &   &   & Y \\  \bottomrule" "\end{tabular}" ) replace sty(tex)"


********************************************************************************
******************************** IV Regressions ********************************
********************************************************************************

use "$data/Replication Data/R&R2023/onestep_compu_SOI_clean", clear

* Approach 0922: 7 columns
global level "gvkey"
global spec1 ", cl(gvkey) a(PR ppe95_bins10##year)"
global spec2 ", cl(gvkey) a(PR $level ppe95_bins10##year)"
global spec3 "if exp_sec, cl(gvkey) a($level ppe95_bins10##year)"
global spec4 "if exp_ind, cl(gvkey) a($level ppe95_bins10##year)"
global spec5 ", cl(gvkey) a($level ppe95_bins10##year n2##ppe95_bins10##year)"
global spec6 "[aw=DFL_ppe], cl(gvkey) a($level ppe95_bins10##year n2##ppe95_bins10##year)"
global spec7 "[aw=_webal], cl(gvkey) a($level ppe95_bins10##year n2##ppe95_bins10##year)"



destring n2, replace
	
*** Tables to make ***
// * 1. No instruments
// est clear
// local j = 0
// local dep = "tax_impute"
// foreach spec in spec1 spec2 spec3 spec4 spec5 spec6  {
// 		local j = `j' + 1
// 		eststo reg_`spec'_`j': reghdfe capex_base `dep' $`spec' 
// 		estimates save "$output/R&R2023/ster/SOI_PR_by_year_`i'", replace
// }	
//
// esttab using "$output/onestep/1_no_instruments.tex", keep(tax_impute) ///
// cells(b(fmt(3)) se(par) p) mlab(none) coll(none) noobs scalars("N Observations" ) ///
// varlabel( tax_impute "\hspace{1em}Effective Tax Rate") $texsettingsx
//
// * 2. PR post instruments	
// est clear
// local j = 0
// local dep = "(tax_impute = PR_post)"
// foreach spec in spec1 spec2 spec3 spec4 spec5 spec6  {
// 		local j = `j' + 1
// 		eststo reg_`spec'_`j': ivreghdfe capex_base `dep' $`spec' 
// }	
//
// esttab using "$output/onestep/2_PR_post_instruments.tex", keep(tax_impute) ///
// cells(b(fmt(3)) se(par) p) mlab(none) coll(none) noobs scalars("N Observations" ) ///
// varlabel( tax_impute "\hspace{1em}Effective Tax Rate") $texsettings

* 3. PR-by-year instruments	
est clear
local j = 0
local dep = "(tax_impute = PR##i.year)"
foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 spec7 {
		local j = `j' + 1
		eststo reg_`spec'_`j': ivreghdfe capex_base `dep' $`spec' 
		estimates save "$output/R&R2023/ster/sizecutoff_syFirmFE/SOI_PR_by_year_`j'", replace
}		
	
esttab using "$output/R&R2023/onestep/3_PR_by_year_instruments.tex", keep(tax_impute) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) noobs scalars("N Observations" ) ///
varlabel( tax_impute "\hspace{1em}Effective Tax Rate") $texsettings
	
// * 4. PR post instruments: positive ETR
// est clear
// preserve
// 	replace tax_impute = max(0, tax_impute)
// 	local j = 0
// 	local dep = "(tax_impute = PR_post)"
// 	foreach spec in spec1 spec2 spec3 spec4 spec5 spec6  {
// 		local j = `j' + 1
// 		eststo reg_`spec'_`j': ivreghdfe capex_base `dep' $`spec' 
// 	}			
// restore
//
// esttab using "$output/onestep/4_PR_post_instruments_positive_ETR.tex", keep(tax_impute) ///
// cells(b(fmt(3)) se(par) p) mlab(none) coll(none) noobs scalars("N Observations" ) ///
// varlabel( tax_impute "\hspace{1em}Effective Tax Rate") $texsettings


* 5. PR-by-year instruments: positive ETR
est clear
preserve
	replace tax_impute = max(0, tax_impute)
	local j = 0
	local dep = "(tax_impute = PR##i.year)"
	foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 spec7  {
		local j = `j' + 1
		eststo reg_`spec'_`j': ivreghdfe capex_base `dep' $`spec' 
		estimates save "$output/R&R2023/ster/sizecutoff_syFirmFE/SOI_PR_by_year_instruments_positive_ETR_`j'", replace
	}			
restore

esttab using "$output/R&R2023/onestep/5_PR_by_year_instruments_positive_ETR.tex", keep(tax_impute) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) noobs scalars("N Observations" ) ///
varlabel( tax_impute "\hspace{1em}Effective Tax Rate") $texsettings

*/


*** Columns for each table ***	
	* C1: Size Deciles-by-Year FEs
	* C2: Size Deciles-by-Year FEs + Sector-by-Year FEs
	* C3: Size Deciles-by-Year FEs + Sector-by-Year FEs	+ Firm FEs
	* C4: Size Deciles-by-Year FEs + S936 Exposed Sector
	* C5: Size Deciles-by-Year FEs + S936 Exposed Industry
	* C6: Sector-by-Size Deciles-by-Year FEs + DFL Weights
		
		
		



********************************************************************************
************ Test "Stacked" Approach comparing just long difference ************
********************************************************************************
/*
global spec1 ", cl(gvkey) a(PR ppe95_bins10##year)"
global spec2 ", cl(gvkey) a(PR n2##year ppe95_bins10##year)"
global spec3 ", cl(gvkey) a(gvkey n2##year ppe95_bins10##year)"
global spec4 "if exp_sec, cl(gvkey) a(PR ppe95_bins10##year)"
global spec5 "if exp_ind, cl(gvkey) a(PR ppe95_bins10##year)"
global spec6 "[aw=DFL2], cl(gvkey) a(PR n2##ppe95_bins10##year)"		

use "$data/Replication Data/newCompu_SOI_Clean", clear

*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 {
	qui reghdfe capex_base _IyeaX* $`spec'
	forval yr = 1996/2006{
		local b_cap`yr' = _b[_IyeaXPR_`yr']
	}
	qui reghdfe tax_impute _IyeaX* $`spec'
	local b_etr = _b[_IyeaXPR_2006]
	forval yr = 1996/2006{
		local b_etr`yr' = _b[_IyeaXPR_`yr']
	}	

	dis "Long difference elasticity for `spec'"	
	dis `b_cap2006'/`b_etr2006'
}	


preserve
	replace tax_impute = max(0, tax_impute)
	foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 {
		qui reghdfe capex_base _IyeaX* $`spec'
		forval yr = 1996/2006{
			local b_cap`yr' = _b[_IyeaXPR_`yr']
		}
		qui reghdfe tax_impute _IyeaX* $`spec'
		local b_etr = _b[_IyeaXPR_2006]
		forval yr = 1996/2006{
			local b_etr`yr' = _b[_IyeaXPR_`yr']
		}	

		dis "Long difference elasticity for `spec'"	
		dis `b_cap2006'/`b_etr2006'
	}	
restore
	
*/
	
********************************************************************************
***************************** "Stacked" Regression *****************************
********************************************************************************
use "$data/Replication Data/R&R2023/onestep_compu_SOI_clean", clear
drop if gvkey==.
keep capex_base tax_impute gvkey year PR ppe95_bins10 n2 DFL_ppe exp_??? _webal
destring n2, replace

forvalues yr = 1991/2006 {
	gen esPR_`yr' =(year==`yr')*PR
}
drop esPR_1995

local i = 1
foreach var in capex_base tax_impute {
	preserve
		keep `var'  esPR_* gvkey year PR ppe95_bins10 n2 DFL_ppe exp_??? _webal
		rename * *_`i' // Index for each outcome 
		rename `var'_`i' outcome
		qui reg outcome esPR_* year i.ppe95_bins10 i.n2
		keep if e(sample)
		local i = `i' + 1
		di "`i'"
		
	tempfile `var'_sample
	save "``var'_sample'", replace
	restore
}

use "`capex_base_sample'", clear
append using "`tax_impute_sample'"



* making a single naics variable for clustering SE
gen gvkey = gvkey_1
replace gvkey = gvkey_2 if gvkey == .
	
gen DFL_ppe = DFL_ppe_1
replace DFL_ppe = DFL_ppe_2 if DFL_ppe == .

	
* filling in the missing values to let the regression run
foreach var of varlist *_1 {
	replace `var' = 0 if `var' == .
}	
foreach var of varlist *_2 {
	replace `var' = 0 if `var' == .
}	

foreach var in exp_sec exp_ind {
		replace `var'_1 = 1 if `var'_2==1
		replace `var'_2 = 1 if `var'_1==1
}



* Approach 0922: running a stacked regression	
global spec1 ", a(PR_? ppe95_bins10_1##year_1 ppe95_bins10_2##year_2) cl(gvkey)	nocons"
global spec2 ", a(PR_? gvkey_? ppe95_bins10_1##year_1 ppe95_bins10_2##year_2) cl(gvkey) nocons"
global spec3 "if exp_sec_1 & exp_sec_2, a(gvkey_? ppe95_bins10_1##year_1 ppe95_bins10_2##year_2) cl(gvkey) nocons"
global spec4 "if exp_ind_1 & exp_ind_2, a(gvkey_? ppe95_bins10_1##year_1 ppe95_bins10_2##year_2) cl(gvkey)	nocons"
global spec5 ", a(gvkey_? ppe95_bins10_1##year_1 ppe95_bins10_2##year_2 n2_1##ppe95_bins10_1##year_1 n2_2##ppe95_bins10_2##year_2) cl(gvkey) nocons"
global spec6 "[aw=DFL_ppe], a(gvkey_? ppe95_bins10_1##year_1 ppe95_bins10_2##year_2 n2_1##ppe95_bins10_1##year_1 n2_2##ppe95_bins10_2##year_2) cl(gvkey) nocons"
global spec7 "[aw=_webal], a(gvkey_? ppe95_bins10_1##year_1 ppe95_bins10_2##year_2 n2_1##ppe95_bins10_1##year_1 n2_2##ppe95_bins10_2##year_2) cl(gvkey) nocons"


* Baseline
est clear
local j = 0
foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 spec7 {
	local j = `j' + 1
	qui reghdfe outcome esPR* $`spec'
	eststo eps_`spec' : nlcom _b[esPR_2006_1]/_b[esPR_2006_2], post
	estadd scalar N_ = round(e(N)/2)
	estimates save "$output/R&R2023/ster/sizecutoff_syFirmFE/SOI_longdiff_`j'", replace
}	

esttab eps_spec? using "$output/R&R2023/onestep/longdiff_SOI.tex", ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) noobs scalars(N_) ///
varlabel( _nl_1 "\hspace{1em} $\varepsilon^{K}_{\tau_{ETR}}$") $texsettings


* Positive ETR
est clear
local j = 0
replace outcome = max(0, outcome) if gvkey_2>0
foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 spec7 {
	local j = `j' + 1
	qui reghdfe outcome esPR* $`spec'
	eststo eps_posETR_`spec': nlcom _b[esPR_2006_1]/_b[esPR_2006_2], post
	estadd scalar N_ = round(e(N)/2)
	estimates save "$output/R&R2023/ster/sizecutoff_syFirmFE/SOI_longdiff_positive_ETR_`j'", replace
}	

esttab eps_posETR_spec? using "$output/R&R2023/onestep/longdiff_SOI_posETR.tex", ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) noobs scalars(N_ ) ///
varlabel( _nl_1 "\hspace{1em} $\varepsilon^{K}_{\tau_{ETR}}$") $texsettings

