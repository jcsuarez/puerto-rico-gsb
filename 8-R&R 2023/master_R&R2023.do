clear all
set more off
snapshot erase _all 

global dropbox "/Users/kevinroberts/Dropbox (Personal)/Puerto Rico"

global dropbox "/Users/jisangyu/JC's Data Emporium Dropbox/Eddie Yu/Puerto Rico"

** Install Packages 
// ssc install listtex, replace all 
// ssc install binscatter, replace all
// ssc install estout, replace all 
// ssc install cdfplot, replace all
// ssc install winsor, replace all
// ssc install reghdfe, replace all
// ssc install tuples, replace all
// ssc install coefplot, replace all 
// ssc install spmap, replace all 
// ssc install maptile, replace all 
// ssc install ivreg2, replace all 
// ssc install ftools, replace all
// ssc install ivreghdfe, replace all 
// ssc install ivreg2hdfe, replace all
// ssc install reg2hdfe, replace all 
// ssc install moremata, replace all
// ssc install tmpdir, replace all
// ssc install ranktest, replace all
// ssc install dataout, replace
// maptile_install using "http://files.michaelstepner.com/geo_county1990.zip"
// maptile_install using "http://files.michaelstepner.com/geo_state.zip"

** Global Dropbox Paths (raw to clean data)
global output 		"$dropbox/Programs/output"
global data 		"$dropbox/Data"
global analysis 	"$dropbox/Programs"
global linkdata		"$dropbox/Programs/output/base_1995"
global xwalk		"$dropbox/Data/Cross Walk"
global qcewdata		"$dropbox/Data/QCEW"
global bea			"$dropbox/Data/BEA"
global consp		"$dropbox/Data/conspuma"
global additional 	"$dropbox/Data/data_additional"
global output_NETS 	"$dropbox/Programs/output/NETS_20171128"
global output_NETS2 "$dropbox/Programs/output/NETS_20171129_v2"
global output_NETS_control "$dropbox/Programs/output/NETS_control"
global WRDS			"$dropbox/Data/WRDS"
global ASM			"$dropbox/Data/ASM"
global SOI			"$dropbox/Data/SOI Data"
global irs 			"$dropbox/Data/IRS"
global randr		"$analysis/8-R&R 2023"

cd "$analysis"

* Build data: Make data from newly pulled compustat historical segments data
do "$randr/build/compustat_merge_manual.do"
do "$randr/build/compustat_ETR_calc_segments.do"

// * base : original paper version - uses bad controls IHS_rev, log_tot_emp
//
// 	** BUILD DATA **
// 	/* This is a critical do file that creates DFL weights exactly same as original paper
// 	   above, and generates compu_NETS_ETR.dta to be used for *_clean.do, and onestep*.do */ 
	do "$randr/base/clean_compu_for_analysis.do"

	do "$randr/base/table2_clean.do" // creates table 2, 5, A5, A6 data
	do "$randr/base/table3_clean.do" // creates table 3 data using table2_data
	do "$randr/base/table4_clean.do" // creates table 4, A8, A9 data
	do "$randr/base/onestep_compu_SOI_clean.do" // creates data for onestep imputation
	do "$randr/base/figure5_clean.do" // creates figure 5, figure 6, figure A4, A5, A6, A7 data
//	
// 	** RUN REGRESSIONS **
// 	do "$randr/base/table2.do" 
// 	do "$randr/base/table3.do" 
// 	do "$randr/base/table4.do" 
// 	do "$randr/base/table5.do" 
// 	do "$randr/base/tableA5.do" 
// 	do "$randr/base/tableA6.do" 
// 	do "$randr/base/tableA8.do"
// 	do "$randr/base/tableA9.do"
// 	do "$randr/base/IHS_tax.do" // uses compu_NETS_ETR.dta
// 	do "$randr/base/onestep_compu_SOI.do"

	
* base_nocontrol
	** BUILD DATA **
	/* This is a critical do file that creates DFL weights exactly same as original paper
	   above, and generates compu_NETS_ETR.dta to be used for *_clean.do, and onestep*.do */ 
	do "$randr/base_nocontrol/clean_compu_for_analysis.do"

	do "$randr/base_nocontrol/table2_clean.do" // creates table 2, 5, A5, A6 data
	do "$randr/base_nocontrol/table3_clean.do" // creates table 3 data using table2_data
	do "$randr/base_nocontrol/table4_clean.do" // creates table 4, A8, A9 data
	do "$randr/base_nocontrol/onestep_compu_SOI_clean.do" // creates data for onestep imputation
	do "$randr/base/figure5_clean.do" // creates figure 5, figure 6, figure A4, A5, A6, A7 data
	
	** RUN REGRESSIONS **
// 	do "$randr/base_nocontrol/table2.do" 
// 	do "$randr/base_nocontrol/table3.do" 
	do "$randr/base_nocontrol/table4.do" 
	do "$randr/base_nocontrol/table5.do" 
// 	do "$randr/base_nocontrol/tableA5.do" 
// 	do "$randr/base_nocontrol/tableA6.do" 
	do "$randr/base_nocontrol/tableA8.do"
	do "$randr/base_nocontrol/tableA9.do"
// 	do "$randr/base_nocontrol/IHS_tax.do" // uses compu_NETS_ETR.dta
// 	do "$randr/base_nocontrol/onestep_compu_SOI.do"

	


* size cutoff + size-year FE

	** BUILD DATA **
	/* This is a critical do file that creates DFL weights as i.n2##i.ppe95 (10)
	   above, and generates compu_NETS_ETR_sizecutoff.dta to be used for *_clean.do, and onestep*.do */ 
	do "$randr/sizecutoff_syFE/clean_compu_for_analysis.do"

	do "$randr/sizecutoff_syFE/table2_clean.do" // creates table 2, 5, A5, A6 data
	do "$randr/sizecutoff_syFE/table3_clean.do" // creates table 3 data using table2_data
	do "$randr/sizecutoff_syFE/table4_clean.do" // creates table 4, A8, A9 data
	do "$randr/sizecutoff_syFE/onestep_compu_SOI_clean.do" // creates data for onestep imputation
	
// 	** RUN REGRESSIONS **
// 	do "$randr/sizecutoff_syFE/table2.do" 
// 	do "$randr/sizecutoff_syFE/table3.do" 
// 	do "$randr/sizecutoff_syFE/table4.do" 
// 	do "$randr/sizecutoff_syFE/table5.do" 
// 	do "$randr/sizecutoff_syFE/tableA5.do" 
// 	do "$randr/sizecutoff_syFE/tableA6.do" 
// 	do "$randr/sizecutoff_syFE/tableA8.do"
// 	do "$randr/sizecutoff_syFE/tableA9.do"
// 	do "$randr/sizecutoff_syFE/IHS_tax.do" // uses compu_NETS_ETR.dta
// 	do "$randr/sizecutoff_syFE/onestep_compu_SOI.do"



* size cutoff + size-year FE + Firm FE

	** BUILD DATA **
	/* uses the same data as sizecutoff_syFE, so no need to build data */ 
	
	** RUN REGRESSIONS **
	do "$randr/sizecutoff_syFirmFE/table2.do" 
	do "$randr/sizecutoff_syFirmFE/table3.do" 
	do "$randr/sizecutoff_syFirmFE/table4.do" 
	do "$randr/sizecutoff_syFirmFE/table5.do" 
	do "$randr/sizecutoff_syFirmFE/tableA5.do" 
	do "$randr/sizecutoff_syFirmFE/tableA6.do" 
	do "$randr/sizecutoff_syFirmFE/tableA8.do"
	do "$randr/sizecutoff_syFirmFE/tableA9.do"
	do "$randr/sizecutoff_syFirmFE/IHS_tax.do" // uses compu_NETS_ETR.dta
	do "$randr/sizecutoff_syFirmFE/onestep_compu_SOI.do"

	
	
* finally, output tables
	do "$randr/create_tables.do" 
