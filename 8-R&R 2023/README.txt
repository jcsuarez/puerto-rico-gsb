This directory is made for September 2023 R&R submission preparation.

Running master script, master_R&R2023.do will create all ster results necessary to output tables in Programs/output/R&R2023/ster/* directory. We implement 4 approaches, (1) base, (2) base_nocontrol, (3) sizecutoff with size-by-year FE, (4) sizecutoff with size-by-year FE and firm FE.
Following is the description of some data build files:

1. compustat_merge_manual.do
  - With newly pulled Compustat Historical Segments data pulled 2023-07-25 (WRDS/compu_segments_072523.dta), we construct the following dataset in $WRDS):
     - $WRDS/compustat_merge_manual_0923.dta

2. compustat_ETR_calc_segments.do
  - With newly pulled Compustat Historical Segments data pulled 2023-07-25 (WRDS/compu_segments_072523.dta) and conducting compustat_merge_manual.do,
    we construct the following dataset in $WRDS/ : 
	- ETR_compustat_Segments_0923.dta
	- ETR_compustat_1992_0923.dta
	- All_compu_cusip_0923.txt
	- PR_compu_cusip_0923.txt

3. clean_compu_for_analysis.do
  - After conducting 1, 2, Now we cleanse this Compustat Segments data to prepare for final analysis. This outputs the following dta file:
 	- $data/Replication Data/compu_NETS_ETR.dta
  - This file is important because we will be implementing different DFL weights with respect to paper's specification of logit PR i.n3 i.rev (3-digit NAICS and 20 bins of revenue)

After 3, compu_NETS_ETR.dta is created.
We then use this file to select variables for table*_clean.do OR figure*_clean.do.
One thing to note is that in order not to overwrite existing Data/Replication Data/*.dta files, I create new folder Data/R&R2023/ and save newly created table*.dta, figure*.dta there. This way we don't overwrite existing replication data files.


Then we use table*.do OR figure*.do, that takes R&R2023/table*.dta or R&R2023/figure*.dta, for outputting the respective tables and figures.

The same is done when outputting .tex files for tables, or .pdf files for figures. We don't want to overwrite replicated tables and figures in Programs/output directory. Thus I also create output/R&R2023/Tables, output/R&R2023/Graphs, output/R&R2023/ster to save tables, figures, and stata estimates files. Therefore, when cleaning table*.do or figure*.do files, MAKE SURE THAT $output/Tables is changed to $output/R&R2023/Tables/, likewise!


-------------------------------------------------------------------------------------------
September 25, 2023 Edit

Additional directories: referee_report/, paper/
This directories will store source codes that will produce tables and figures for referee reports, and edited papers/figures that will go into the paper.

