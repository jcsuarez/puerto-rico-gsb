clear all
set more off
snapshot erase _all 

// author: eddie

***************************************************************
********************* Selection into PR ***********************
************ Same as newCompu_Clean_0923.do *******************
***************************************************************
***************************************************************
use "$data/Replication Data/newCompu_GSS_PRselection", replace

* interaction
xi i.year|PR , noomit
drop _IyeaXPR_1995 

forval i=1990(1)2014 { 
	capture: label var _IyeaXPR_`i' " "
} 
forval i=1991(2)2014 { 
	capture: label var _IyeaXPR_`i' "`i'"
}

tab idbflag PR if year==1995
tab fic
drop if PR==0 & idbflag=="D"
 

 
* ppent xtile by gvkey
g ppe95 = ppent if year == 1995
xfill ppe95, i(gvkey)
xtile ppe95_bins10 = ppe95, n(10)
xtile ppe95_bins5 = ppe95, n(5)
xtile ppe95_bins3 = ppe95, n(3)	
	

**** Exposed industry/sector indicators
gen exp_sec = inlist(n2,3,5,6,7,11,12)
gen exp_ind = ( ~inlist(n3,"324","323","337","322","327","336","331","321"))*exp_sec

**** Control versions ****
gen lninv = log(capx)
gen lnat = log(at)
gen ppe_scaled = ppe*1000
sum ppe_scaled ppent
gen lnppe = log(ppe)
gen lnppe2 = log(ppe_scaled)
gen lnppent = log(ppent)
gen lnintan = log(intan)
gen tangibility = ppent/at

* create 1995 bins for ppent, gpop (gross profit / total assets)
xtset gvkey year
xtile ppe95_binDFL = ppent if year == 1995 , n(10) 
xfill ppe95_binDFL, i(gvkey)
xtile gp95_binDFL = gp if year == 1995 , n(5) 
xfill gp95_binDFL, i(gvkey)

* Size Cutoffs
xtile PR_sizeDist = ppe95 if PR==1, n(3)
xtile PR_sizeDist10 = ppe95 if PR==1, n(10)

tab PR if year==1995, sum(ppe95)
tab PR_sizeDist if year==1995, sum(ppe95)
drop if PR_sizeDist10<7


* ALTERNATIVE: limit bins to non-first tercile
xtile ppe95_bins5_ALT = ppe95 if ppe95_bins3>1, n(5)	
xtile ppe95_bins10_ALT = ppe95 if ppe95_bins3>1, n(10)	


*** PR SELECTION: IPW score
global specname1 "Baseline: Industry-by-PPE Decile"
global specname2 "Industry-by-Gross Profit-by-PPE Decile"
global specname3 "Pharma Indicator-by-PPE Decile"
global specname4 "Any R&D Indicator-by-Industry-by-PPE Decile"
global specname5 "Any R&D Indicator-by-PPE Decile"
global specname6 "Any Pharma-by-Any R&D Indicator"
global specname7 "Any Pharma-by-Any R&D-by-PPE Decile"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "emerald"
global c7 "magenta"

global spec1 "i.n2##i.ppe95_binDFL if year == 1995"
global spec2 "i.n2##i.gp95_binDFL##i.ppe95_binDFL if year == 1995"
global spec3 "i.pharma##i.ppe95_binDFL if year == 1995"
global spec4 "i.any_rd##i.n2##i.ppe95_binDFL if year == 1995"
global spec5 "i.any_rd##i.ppe95_binDFL if year == 1995" 
global spec6 "i.any_rd##i.pharma if year == 1995"
global spec7 "i.any_rd##i.pharma#i.ppe95_binDFL if year == 1995"

global rspec "[pw=w_att], cl(gvkey) a(gvkey ppe95_bins10##year)"

local cnt = 0
foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 spec7 { // spec2 spec3 spec4 spec5

	preserve 
	local cnt = `cnt' + 1
	qui logit PR $`spec'
	predict phat, pr
	winsor phat, p(.01) g(w_phat) 
	replace phat = w_phat
	xfill phat, i(gvkey)
	
	* ipw
	gen double w_att = cond(PR==1,1,phat/(1-phat))
	gen double w_ate = cond(PR==1,1/phat,1/(1-phat))
	
	reghdfe capex_base _IyeaX* PR $rspec
	estimates save `spec'_base, replace
		
		estimates use `spec'_base

		** figure years for the graph
		gen fig_year = 1991 in 1
		foreach i of numlist 1/16 {
			local j = `i' + 1990
			replace fig_year = `j' in `i'
		}
		
		** annual betas for the preperiod
		gen pr_beta = 0
		gen pr_beta_lb = 0
		gen pr_beta_ub = 0
		foreach i of numlist 1/4 {
			local j = `i' + 1990
			qui lincom _b[_IyeaXPR_`j'] , level(95)
			replace pr_beta = r(estimate) in `i'
			replace pr_beta_lb = r(lb) in `i'
			replace pr_beta_ub = r(ub) in `i'
		}
		* zero effect in 1995
		qui lincom  0
		replace pr_beta = r(estimate) in 5

		** annual betas for the postperiod
		foreach i of numlist 6/16 {
			local j = `i' + 1990
			qui lincom _b[_IyeaXPR_`j'] , level(95)
			replace pr_beta = r(estimate) in `i'
			replace pr_beta_lb = r(lb) in `i'
			replace pr_beta_ub = r(ub) in `i'
		}
		
		gen specname = "`spec'"
		keep if _n < 17
		keep fig_year pr_beta* specname
		tempfile `spec'_graph
		save ``spec'_graph', replace	
	restore

}
*** Graph Overlay ***
{
di `cnt'
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/`cnt' {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec5", lco($c5) mco($c5) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec5", lco($c5) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec6", lco($c6) mco($c6) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec6", lco($c6) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec7", lco($c7) mco($c7) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec7", lco($c7) lpa(solid)) ///
	 , yline(0 , lcolor(gs12)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(4) on order (1 3 5 7 9 11 13) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3") ///
		label(7 "$specname4") ///
		label(9 "$specname5") ///		
		label(11 "$specname6") ///	
		label(13 "$specname7")  ) ///
		yti("Effect of S936 Exposure on Investment", margin(medium))
	
	graph export "$output/Graphs/eddie/newCompu_GSS_PRselection.pdf", replace 
restore	
}


	
* Table inputs to produce:
* (1) Two rows, report baseline long difference (LD) and interaction LD
* (2) Couple of specification (test baseline and sector-by-PPE bins) - less important
* (3) Two versions:
	* Interaction 1: "anyhaven" pre 1995
	* Interaction 2: "anyhaven" post 1995
* (4) Outcome: capex_base
* (5) New foreign investment outcome	



***************************************************************
****************** Tax Haven Substitute ***********************
************ Same as newCompu_Clean_0923.do *******************
***************************************************************
***************************************************************
clear all
set more off
snapshot erase _all 
use "$data/Replication Data/newCompu_GSS_havenusage", replace

qui{
local fs_capx_name "Foregin Share"
local capex_base_name "I/K"

* interaction
xi i.year|PR , noomit
drop _IyeaXPR_1995 

forval i=1990(1)2014 { 
	capture: label var _IyeaXPR_`i' " "
} 
forval i=1991(2)2014 { 
	capture: label var _IyeaXPR_`i' "`i'"
}

tab idbflag PR if year==1995
tab fic
drop if PR==0 & idbflag=="D"
destring n2, replace
 

 
* ppent xtile by gvkey
g ppe95 = ppent if year == 1995
xfill ppe95, i(gvkey)
xtile ppe95_bins10 = ppe95, n(10)
xtile ppe95_bins5 = ppe95, n(5)
xtile ppe95_bins3 = ppe95, n(3)	
	

**** Exposed industry/sector indicators
gen exp_sec = inlist(n2,3,5,6,7,11,12)
gen exp_ind = ( ~inlist(n3,"324","323","337","322","327","336","331","321"))*exp_sec

**** Control versions ****
gen lninv = log(capx)
gen lnat = log(at)
gen ppe_scaled = ppe*1000
sum ppe_scaled ppent
gen lnppe = log(ppe)
gen lnppe2 = log(ppe_scaled)
gen lnppent = log(ppent)
gen lnintan = log(intan)
gen tangibility = ppent/at

* create 1995 bins for ppent, gpop (gross profit / total assets)
xtset gvkey year
xtile ppe95_binDFL = ppent if year == 1995 , n(10) 
xfill ppe95_binDFL, i(gvkey)
xtile gp95_binDFL = gp if year == 1995 , n(5) 
xfill gp95_binDFL, i(gvkey)

* Size Cutoffs
xtile PR_sizeDist = ppe95 if PR==1, n(3)
xtile PR_sizeDist10 = ppe95 if PR==1, n(10)

tab PR if year==1995, sum(ppe95)
tab PR_sizeDist if year==1995, sum(ppe95)
drop if PR_sizeDist10<7


*** haven usage
global pre_post_ind "Post"
gsort gvkey year
foreach Q in "th" "pr" "ir" "ba" "hk" "sp" "ci" "sw" {
	local X "`Q'_mention"
	replace `X' = 0 if missing(`X')
	local X_haven "any_`Q'"
	bys gvkey: egen `X_haven' = max(`X') if year > 1995 // tax haven after 1995
	xfill `X_haven', i(gvkey)
}


*** figure specs 
global spec1 "_IyeaX*##i.any_th , cl(gvkey) a(gvkey ppe95_bins10##year)"
global spec2 "_IyeaX*##i.any_pr , cl(gvkey) a(gvkey ppe95_bins10##year)"
global spec3 "_IyeaX*##i.any_ir , cl(gvkey) a(gvkey ppe95_bins10##year)"
global spec4 "_IyeaX*##i.any_ba , cl(gvkey) a(gvkey ppe95_bins10##year)"
global spec5 "_IyeaX*##i.any_hk , cl(gvkey) a(gvkey ppe95_bins10##year)"
global spec6 "_IyeaX*##i.any_sp , cl(gvkey) a(gvkey ppe95_bins10##year)"
global spec7 "_IyeaX*##i.any_ci , cl(gvkey) a(gvkey ppe95_bins10##year)"
global spec8 "_IyeaX*##i.any_sw , cl(gvkey) a(gvkey ppe95_bins10##year)"

global specname1 "$pre_post_ind Any Tax Haven"
global specname2 "$pre_post_ind 1995 Puerto Rico"
global specname3 "$pre_post_ind 1995 Ireland"
global specname4 "$pre_post_ind 1995 Barbados"
global specname5 "$pre_post_ind 1995 Hong Kong"
global specname6 "$pre_post_ind 1995 Singapore"
global specname7 "$pre_post_ind 1995 Caymand Islands"
global specname8 "$pre_post_ind 1995 Switzerland"

*** table specs 
local th_name "Any Tax Haven"
local pr_name "Puerto Rico"
local ir_name "Ireland"
local ba_name "Barbados"
local hk_name "Hong Kong"
local sp_name "Singapore"
local ci_name "Caymand Islands"
local sw_name "Switzerland"



global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "emerald"
global c7 "magenta"
global c8 "orange_red"
//
// 	eststo reg_`y'_a: areg `y' ib(last).year#i.n2 PR PR_post  , cl(gvkey)   a(year)
// 	lincom PR_post
// 	sum ``y'_sum' if e(sample) & year == 2006, d
// 		estadd local avg = string(r(mean), "%8.3fc")
// 		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
// 		estadd local elast = string(-_b[PR_post] / r(mean) / p_dif_v0, "%8.2fc")
// 		estadd local pp_dif = string(p_dif_v0*100, "%8.3fc")
//
// 	esttab reg_`y'_? using "$output/Tables/eddie/table2_nocontrol.tex", keep(_IyeaXPR_2006)
// 	 mlab(none) coll(none) cells(b(fmt(3)) se(par) stats() ///
// 	b(3) se par label ///
// 	noobs nogap nomtitle tex replace nonum nocons postfoot("") prefoot("") posthead("")
//
// 	esttab reg_`spec', keep(_IyeaXPR_2006) mlab(none) coll(none) ///
// 	cells(b(fmt(3) se(par) stats()) ///
// 	varlabel(fs_capx "Foreign Share of Investment") 
//	
// 	esttab LD_*, keep(_IyeaXPR_2006) mlab(none) coll(none) cells(b(fmt(3)) se(par) p) 

** table 
}
estimates clear
local cnt = 0
local new_var_name "Triple Difference"
foreach haven in "th" "pr" "ir" "ba" "hk" "sp" "ci" "sw" {
	dis "``haven'_name'"
	qui {
	local cnt = `cnt' + 1
	local hvn_ind "any_`haven'"
	eststo reg_`haven': reghdfe fs_capx `hvn_ind'##PR_post, cl(gvkey) a(gvkey year)
	lincom 1.`hvn_ind'#1.PR_post
// 	ret li
// 		estadd local b_tdiff = _b[1.`hvn_ind'#1.PR_post]
// 		estadd local se_tdiff = _se[1.`hvn_ind'#1.PR_post]
	}
	esttab reg_*, keep(1.`hvn_ind'#1.PR_post) mlab(none) coll(none) ///
	cells(b(fmt(3)) se(par) p)
}
	esttab reg_*, keep(b_tdiff se_tdiff) mlab(none) coll(none) ///
	cells(b(fmt(3)) se(par) stats())
//  keep(b_tdiff se_tdiff) varlabel(PR#post#any_* "Post X PR X $pre_post_ind 1995 Haven") 
	
	
	preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[1._IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[1._IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
	restore
}





local cnt = 0 
foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 spec7 spec8 {
	local cnt = `cnt' + 1
	reghdfe fs_capx $`spec'
	estimates save `spec'_base, replace
			
	preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[1._IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[1._IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
	restore
}
*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/`cnt' {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec5", lco($c5) mco($c5) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec5", lco($c5) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec6", lco($c6) mco($c6) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec6", lco($c6) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec7", lco($c7) mco($c7) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec7", lco($c7) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec8", lco($c8) mco($c8) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec8", lco($c8) lpa(solid)) ///
	 , yline(0 , lcolor(gs12)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(4) on order (1 3 5 7 9 11 13 15) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3") ///
		label(7 "$specname4") ///
		label(9 "$specname5") ///
		label(11 "$specname6") ///
		label(13 "$specname7") ///
		label(15 "$specname8")  ) ///
// 		ti("Having Haven $pre_post_ind 1995: Effect on Foreign Share", margin(medium))
restore	
}


graph export "$output/Graphs/eddie/newCompu_GSS_haven_$pre_post_ind.pdf", replace 	



