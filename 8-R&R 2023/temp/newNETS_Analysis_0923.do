// This file recreates Figure 5
// Author: Dan Garrett
// Date: 3-31-2020

/* Data sources:
various sources including NETS and compustat
*/

clear all
set more off
snapshot erase _all 
*** *** *** *** *** *** *** *** ** *** *** ***
*** *** Section (0): Clean up somewhat *** ***
*** *** *** *** *** *** *** *** ** *** *** ***
use "$data/Replication Data/figure8_data_EY_081523", clear

drop if base_emp ==0 
sum base_emp if year == 1995 & PR , d
gen wgt=base_emp/( r(mean)) if PR 

sum base_emp if year == 1995 & PR==0 , d
replace wgt=base_emp/( r(mean)) if PR==0 

winsor wgt, p(.01) g(w_wgt) 

** Make balanced panel by dropping firms that drop out
bys hqduns95 : egen min_emp = min(emp_US) 
drop if min_emp == 0 



** Size Controls **
xtile emp_bins10 = base_emp, n(10)
xtile emp_bins3 = base_emp, n(3)

xtile emp_bins10_ALT = base_emp if emp_bins3==3, n(10)
xtile emp_bins3_ALT = base_emp if emp_bins3==3, n(3)

** Regression setup **
	xi i.year|PR, noomit 
	drop _IyeaXP*1995

set matsize 4000

save "$data/Replication Data/newNETS_Clean_0923", replace


*** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *
*** *** Section (1): Summarize firm size distribution *** ***
*** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *
use "$data/Replication Data/newNETS_Clean_0923", clear
sum base_emp if year==1995 & PR==1, detail
sum base_emp if year==1995 & PR==0, detail

keep if year==1995
gen firms = 1
gen PR_ = "Puerto Rico Firms" if PR==1
replace PR_ = "Control Firms" if PR==0	
drop if base_emp<4000
drop emp_bins10	
xtile emp_bins10 = base_emp, n(10)
	
* Deciles
preserve
	collapse (sum) firms (mean) base_emp, by(emp_bins10 PR_)
	label variable firms "Number of Firms"


	graph bar firms, over(PR) over(emp_bins10) asyvars ///
		ytitle("Number of Firms") legend(pos(6) rows(1)) blabel(bar)
	graph export "$output/Graphs/kevin/deciles_firms_NETS.pdf", replace 	
		
	graph bar base_emp, over(PR) over(emp_bins10) asyvars ///
		ytitle("Mean Employment, 1995") legend(pos(6) rows(1)) 
	graph export "$output/Graphs/kevin/deciles_emp_NETS.pdf", replace 	
			
restore

* Terciles
preserve
	collapse (sum) firms (mean) base_emp, by(emp_bins3 PR_)
	label variable firms "Number of Firms"


	graph bar firms, over(PR) over(emp_bins3) asyvars ///
		ytitle("Number of Firms") legend(pos(6) rows(1)) blabel(bar)
	graph export "$output/Graphs/kevin/terciles_firms_NETS.pdf", replace 	
		
	graph bar base_emp, over(PR) over(emp_bins3) asyvars ///
		ytitle("Mean Employment, 1995") legend(pos(6) rows(1)) 
	graph export "$output/Graphs/kevin/terciles_emp_NETS.pdf", replace 	
					
restore
*/

*** *** *** *** ***  *** *** ***  *** *** *** *** *** ***
*** *** *** Section (2): Baseline Regressions *** *** ***
*** *** *** *** ***  *** *** ***  *** *** *** *** *** ***
use "$data/Replication Data/newNETS_Clean_0923", clear
drop if base_emp<4000
/*
gen PRexp = major_ind*(1-PR)
drop if PR==1
drop PR
rename PRexp PR
drop _I*
xi i.year|PR , noomit
*/

/*
drop if base_emp<2000
xtile PR_sizeDist = base_emp if PR==1, n(10)
tab PR_sizeDist if year==1995, sum(base_emp)
drop if PR_sizeDist<7
*/

xtile binDFL = base_emp if year == 1995 , n(20) 
logit PR i.m_naic3##i.binDFL if year == 1995
capture: drop phat min_phat w w_phat
predict phat, pr 
*winsor phat, p(.01) g(w_phat) 
*replace phat = w_phat
bys hqduns95: egen min_phat = min(phat) 
* ATOT
gen DFL1 = (PR+(1-PR)*min_phat/(1-min_phat))
gen DFL2 = wgt*(PR+(1-PR)*min_phat/(1-min_phat))
gen DFL3 = base_emp*(PR+(1-PR)*min_phat/(1-min_phat))

*/


gen ln_emp = log(emp_US)


global spec1 "[aw=wgt],vce(cluster hqduns) nocon a(hqduns year)"
global spec2 "[aw=DFL1],vce(cluster hqduns) nocon a(hqduns year)"
global spec3 "[aw=DFL2],vce(cluster hqduns) nocon a(hqduns year)"
global spec4 "[aw=DFL3],vce(cluster hqduns) nocon a(hqduns year)"

*global spec3 "if major_sec [aw=wgt],vce(cluster hqduns) noconst a( m_naic3##year)"
*global spec4 "if major_ind [aw=wgt],vce(cluster hqduns) noconst a( m_naic3##year)"

global specname1 "Baseline"
global specname2 "Size Deciles"
global specname3 "Exp. Sector"
global specname4 "Exp. Industry"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"

tab year PR, sum(emp_growth)
tab year PR if base_emp>100, sum(emp_growth)
tab year PR, sum(ln_emp)

sum emp_growth if PR==1 & year==2001, detail
sum emp_growth if PR==1 & year==2002, detail


*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4 {
reghdfe emp_growth _IyeaXP* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/22 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/22 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 23
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/4 {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
		, yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2012) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7 ) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")   ) ///
		yti("Effect of S936 on Employment Growth", margin(medium))
restore	
}

graph export "$output/Graphs/kevin/NETS_Overlay_Base.pdf", replace 	
	
	
	
*** *** *** *** ***  *** *** ***  *** *** *** *** *** *** ***
*** *** *** Section (3): Size Control Regressions *** *** ***
*** *** *** *** ***  *** *** ***  *** *** *** *** *** *** ***

*** Deciles ***
use "$data/Replication Data/newNETS_Clean_0923", clear
drop if base_emp<10000
xtile PR_sizeDist = base_emp if PR==1, n(10)
tab PR_sizeDist if year==1995, sum(base_emp)
drop if PR_sizeDist<7







global spec1 "[aw=wgt],vce(cluster hqduns) noconst a(year emp_bins10##year)"
global spec2 "[aw=wgt],vce(cluster hqduns) noconst a(m_naic3##year emp_bins10##year)"
global spec3 "if major_sec [aw=wgt],vce(cluster hqduns) noconst a( m_naic3##year emp_bins10##year)"
global spec4 "if major_ind [aw=wgt],vce(cluster hqduns) noconst a( m_naic3##year emp_bins10##year)"

global specname1 "Baseline"
global specname2 "Size Deciles"
global specname3 "Exp. Sector"
global specname4 "Exp. Industry"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"

*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4 {
reghdfe emp_growth _IyeaXP* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/22 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/22 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 23
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/4 {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
		, yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2012) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7 ) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")   ) ///
		yti("Effect of S936 on Employment Growth", margin(medium))
restore	
}

graph export "$output/Graphs/kevin/NETS_Overlay_Deciles.pdf", replace 		

*** Terciles ***
use "$data/Replication Data/newNETS_Clean_0923", clear

global spec1 "[aw=wgt],vce(cluster hqduns) noconst a(year emp_bins3##year)"
global spec2 "[aw=wgt],vce(cluster hqduns) noconst a(m_naic3##year emp_bins3##year)"
global spec3 "if major_sec [aw=wgt],vce(cluster hqduns) noconst a( m_naic3##year emp_bins3##year)"
global spec4 "if major_ind [aw=wgt],vce(cluster hqduns) noconst a( m_naic3##year emp_bins3##year)"

global specname1 "Baseline"
global specname2 "Size Deciles"
global specname3 "Exp. Sector"
global specname4 "Exp. Industry"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"

*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4 {
reghdfe emp_growth _IyeaXP* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/22 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/22 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 23
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/4 {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
		, yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2012) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7 ) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")   ) ///
		yti("Effect of S936 on Employment Growth", margin(medium))
restore	
}

graph export "$output/Graphs/kevin/NETS_Overlay_Terciles.pdf", replace 		




*** *** *** *** ***  *** *** ***  *** *** *** *** *** *** *** ***
*** *** *** Section (4): Keep Large Firms Regressions *** *** ***
*** *** *** *** ***  *** *** ***  *** *** *** *** *** *** *** ***

use "$data/Replication Data/newNETS_Clean_0923", clear
keep if emp_bins3 == 3

global spec1 "[aw=wgt],vce(cluster hqduns) noconst a(year)"
global spec2 "[aw=wgt],vce(cluster hqduns) noconst a(m_naic3##year)"
global spec3 "if major_sec [aw=wgt],vce(cluster hqduns) noconst a( m_naic3##year )"
global spec4 "if major_ind [aw=wgt],vce(cluster hqduns) noconst a( m_naic3##year)"

global specname1 "Baseline"
global specname2 "Size Deciles"
global specname3 "Exp. Sector"
global specname4 "Exp. Industry"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"

*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4 {
reghdfe emp_growth _IyeaXP* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/22 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/22 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 23
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/4 {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
		, yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2012) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7 ) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")   ) ///
		yti("Effect of S936 on Employment Growth", margin(medium))
restore	
}

graph export "$output/Graphs/kevin/NETS_Overlay_KeepBig.pdf", replace 		



*** Deciles ***

global spec1 "[aw=wgt],vce(cluster hqduns) noconst a(year emp_bins10_ALT##year)"
global spec2 "[aw=wgt],vce(cluster hqduns) noconst a(m_naic3##year emp_bins10_ALT##year)"
global spec3 "if major_sec [aw=wgt],vce(cluster hqduns) noconst a( m_naic3##year emp_bins10_ALT##year)"
global spec4 "if major_ind [aw=wgt],vce(cluster hqduns) noconst a( m_naic3##year emp_bins10_ALT##year)"

global specname1 "Baseline"
global specname2 "Size Deciles"
global specname3 "Exp. Sector"
global specname4 "Exp. Industry"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"

*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4 {
reghdfe emp_growth _IyeaXP* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/22 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/22 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 23
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/4 {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
		, yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2012) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7 ) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")   ) ///
		yti("Effect of S936 on Employment Growth", margin(medium))
restore	
}
graph export "$output/Graphs/kevin/NETS_Overlay_KeepBig_Deciles.pdf", replace 		





