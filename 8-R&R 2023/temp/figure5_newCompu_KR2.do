// This file recreates Figure 5
// Author: Dan Garrett
// Date: 3-31-2020

/* Data sources:
various sources including NETS and compustat
*/

clear all
set more off
snapshot erase _all 

use "$data/Replication Data/figure5_data_KR", replace

*** labels for some graphs
local fs_capx_name "Foreign Share Investment"
local capex_base_name "Investment/Physical Capital"
local capex_base_p_name "% Change in Investment/Physical Capital"
local capex_base_1_name "Investment/Physical Capital"
local ftax_name "Federal Tax Rate"
local capxs_name "Capital Expenditure Spending"
local emps_name "Number of Emplyees (Compustat)"
local ias_name "Total Identifiable Assets"
local nis_name "Net Income"
local revts_name "Revenue"
local sales_name "Sales"
local IHS_rev_name "IHS Revenue"
local IHS_capx_name "IHS CapEx"
local IHS_emp_name "IHS Number of Employees"
local log_tot_emp_name "Log Total Employees (Nets)"
local revts_06_name "2006 Revenue (continuous)"
local capxs_9095_name "1990-1995 Capxs (Compustat Historical) Mean (continuous)"
local capex_9095_name "1990-1995 Capex / PPENT (Compustat Annual) Mean (Continuous)"
local revts_06_bins10_name "2006 Revenue Decile"
local capxs_9095_bins10_name "Deciles of 1990-1995 Mean Capxs (Compustat Historical)"
local capex_9095_bins10_name "Deciles of 1990-1995 Mean Capex / PPENT (Compustat Annual)"
local ppe_name "PPE Scaled (Total Assets)"
local ppent_name "PPE"
local ppe_9095_name "1990-1995 Mean of Scaled PPE"
local ppent_9095_name "1990-1995 Mean PPE"
local ppe_9095_bins10_name "Deciles of 1990-1995 Mean of Scaled PPE"
local ppent_9095_bins10_name "Deciles of 1990-1995 Mean PPE"
local at_name "Total Assets"
local at_9095_name "1990-1995 Mean Total Assets"
local at_9095_bins10_name "Deciles of 1990-1995 Mean Total Assets"
local intan_name "Intangible Assets"
local intan_9095_name "1990-1995 Mean Intangible Assets"
local intan_9095_bins10 "Deciles of 1990-1995 Mean Intangible Assets"
local logAT_name "Ln(Total Assets)"
local logAT_9095_name "1990-1995 Mean Ln(Assets)"
local logAT_9095_bins10 "Deciles of 1990-1995 Mean Ln(Assets)"
local logdiff_at_name "(Log Diff) Earliest Year ~ 1995 Asset Growth"
local at95_name "1995 Total Assets"
local at_9095wgt_name "1990-1995 Wgt Av. Total Assets"
local logAT_9095wgt_name "Log 1990-1995 Wgt Av. Total Assets"
 

* combining years and N2
egen yearg = group(year n2)

* Make balanced panel!
gsort gvkey year
local balance = 0 // 0 if FALSE, 1 if TRUE
if `balance' == 1 {
// 	bys gvkey: gen firm_survival = _N
// 	keep if firm_survival == 16 // 46,960 left out of 115,224
// 	drop firm_survival
	by gvkey: gen firm_9095survival = _n if year > 1990 & year <= 1995 & !missing(at)
	by gvkey: egen firm_survival = max(firm_9095survival)
	drop firm_9095survival
	drop if firm_survival != 5
}

*figures 5, 6, A5, and A6
xi i.year|PR , noomit
drop _IyeaXPR_1995 

forval i=1990(1)2014 { 
	capture: label var _IyeaXPR_`i' " "
} 
forval i=1991(2)2014 { 
	capture: label var _IyeaXPR_`i' "`i'"
} 
 
// reghdfe capex_base _IyeaX* PR IHS_rev log_tot_emp, cl(gvkey)   a(yearg gvkey)
// reghdfe capex_base _IyeaX* PR , cl(gvkey)   a(yearg gvkey)
//
// reghdfe capex_base _IyeaX* PR , cl(gvkey)   a(yearg gvkey)



// tab year PR, summ(revts)
// tab year PR, summ(capxs)

* Create 2006 revenue decile by gvkey
// foreach x of var revts {
// 	local new_var "`x'_06"
// 	local new_var_bin "`new_var'_bins10"
// 	tempvar temp1 temp2
// 	gen `temp1' = `x' * (year==2006)
// 	egen `new_var' = max(`temp1'), by(gvkey)
// 	xtile `new_var_bin' = `new_var', n(10)
// }

* Capex/Capxs/ppe/ppent decile by gvkey
foreach x of var capxs capex ppe ppent at intan {
	local start_year=90
	local end_year=95
	local new_var "`x'_`start_year'`end_year'"
	local new_var_bin "`new_var'_bins10"
	tempvar temp1 temp2
	gen `temp1' = `x' * (year<=1900+`end_year' & year >= 1900+`start_year')
	egen `temp2' = mean(`temp1') if `temp1' != 0, by(gvkey)
	egen `new_var' = max(`temp2'), by(gvkey)
	xtile `new_var_bin' = `new_var', n(10)
}

// g logAT_9095 = log(at_9095)


// * truncation
// summ logAT_9095,d
// drop if logAT_9095 < r(p5)
// drop if logAT_9095 > r(p95)

* 9/1 weighted average of 1992-1995
gen temp1a = 0.1*at if year==1992
gen temp2a = 0.2*at if year==1993
gen temp3a = 0.3*at if year==1994
gen temp4a = 0.4*at if year==1995
forvalues i = 1/4 {
       egen temp`i'b = max(temp`i'a), by(gvkey)
}

gen at_9095wgt = temp1b + temp2b + temp3b + temp4b
drop temp*
g logAT_9095wgt = log(at_9095wgt)




* 9/1 exercies: log diff of earliest appearing year - 1995 log asset
	bys gvkey: egen first_yr = min(year) if !missing(at) & at != 0
	bys gvkey: egen first_year = min(first_yr)
	drop first_yr
	g first_at = at if year == first_year
	bys gvkey: egen at_base = max(first_at)
	drop first_at
	g at_95 = at if year == 1995
	bys gvkey: egen at95 = max(at_95)
	drop at_95
	xtile at95_bins10 = at95, n(10)
	xtile at95_bins5 = at95, n(5)
	xtile at95_bins3 = at95, n(3)
	
	g ppe_95 = ppent if year == 1995
	bys gvkey: egen ppe95 = max(ppe_95)
	drop ppe_95
	xtile ppe95_bins10 = ppe95, n(10)
	xtile ppe95_bins5 = ppe95, n(5)
	xtile ppe95_bins3 = ppe95, n(3)	

**** Exposed industry/sector indicators
gen exp_sec = inlist(n2,3,5,6,7,11,12)
gen exp_ind = ( ~inlist(n3,"324","323","337","322","327","336","331","321"))*exp_sec

**** Control versions ****
gen lninv = log(capx)
gen lnat = log(at)
gen lnppe = log(ppe)
gen lnppent = log(ppent)
gen lnintan = log(intan)
gen tangibility = ppent/at

**** What to try ****

* 1. interact size and industry/sector controls
* 2. 

tab at95_bins3 if year==1995, sum(at95)
tab at95_bins3 PR if year==1995, sum(at95)
tab at95_bins3 PR if year==1995 & exp_sec, sum(at95)
tab at95_bins3 PR if year==1995 & exp_ind, sum(at95)
tab at95_bins10 PR if year==1995, sum(at95)


reghdfe capex_base _IyeaX* PR , cl(gvkey) a(gvkey year)
reghdfe capex_base _IyeaX* PR [aw=DFL], cl(gvkey) a(gvkey year)

reghdfe capex_base _IyeaX* PR if exp_ind, cl(gvkey) a(gvkey year)  
reghdfe capex_base _IyeaX* PR if exp_ind [aw=DFL], cl(gvkey) a(gvkey year)

reghdfe capex_base _IyeaX* PR , cl(gvkey) a(gvkey at95_bins3##year)
reghdfe capex_base _IyeaX* PR [aw=DFL], cl(gvkey) a(gvkey at95_bins3##year)

reghdfe capex_base _IyeaX* PR if exp_ind, cl(gvkey) a(gvkey at95_bins3##year)  
reghdfe capex_base _IyeaX* PR if exp_ind [aw=DFL], cl(gvkey) a(gvkey at95_bins3##year)


forvalues i = 1/3{
	reghdfe capex_base _IyeaX* PR if at95_bins3==`i', cl(gvkey) a(gvkey year)  
}


reghdfe capex_base _IyeaX* PR, cl(gvkey) a(gvkey year)
reghdfe capex_base _IyeaX* PR if at95_bins3>1, cl(gvkey) a(gvkey year)
reghdfe capex_base _IyeaX* PR if at95_bins3>1 [aw=DFL], cl(gvkey) a(gvkey year)

* Exposed sectors?
reghdfe capex_base _IyeaX* PR if exp_ind==1, cl(gvkey) a(gvkey year)
reghdfe capex_base _IyeaX* PR if exp_ind==1 & at95_bins3>1, cl(gvkey) a(gvkey year)
reghdfe capex_base _IyeaX* PR if exp_ind==1 & at95_bins3>1 [aw=DFL], cl(gvkey) a(gvkey year)



drop if ppe95_bins3==1
xtile ppe95_bins5_ALT = ppe95, n(5)	
xtile ppe95_bins10_ALT = ppe95, n(10)	

reghdfe capex_base _IyeaX* PR , cl(gvkey) a(gvkey year) 
reghdfe capex_base _IyeaX* PR , cl(gvkey) a(gvkey ppe95_bins5_ALT##year)
reghdfe capex_base _IyeaX* PR , cl(gvkey) a(gvkey ppe95_bins10_ALT##year)
reghdfe capex_base _IyeaX* PR [aw=DFL], cl(gvkey) a(gvkey ppe95_bins10_ALT##year)


*** What if we accept that effect on total investment is positive? ***
* 1. Affected firms also increase foreign shares - still makes sense?

* 2. Employment effects: similar controls yield ~0 effects on domestic employment

* 3. LLM: Still holds? How do we reconcile these results?

* 4. Is there a model where firms choose to operate in PR with lower production/investment relative to other tax havens with higher production?

* 5. Potential intuition: Puerto Rico provides firms with opportunity to generate "idle rents" - shift profits substantially without raising aggregate productivity

* 6. What to make of LLM? Does this reflect contractions in outcomes in other locations, or expansions to others?
	* Three treatment categories:
		* substantial employment exposure prior to 1995
		* no exposure prior to 1995
		* increased exposure after 1995
	* What's the most parismonious way to tease out these effects?
	* How plausible is it that investment went up, but 
		* 
	* Foreign share of investment: are estimates still reliable? 

	


****** Size testing for alternative outcomes *******

*foreach var in capex_base lninv lnat lnppe tangibility lnintan {
foreach var in  lnat lnppent tangibility {	
	*reghdfe `var' _IyeaX* PR , cl(gvkey) a(gvkey year)
	*reghdfe `var' _IyeaX* PR [aw=DFL], cl(gvkey) a(gvkey year)

	reghdfe `var' _IyeaX* PR , cl(gvkey) a(gvkey at95_bins3##year)
	reghdfe `var' _IyeaX* PR [aw=DFL], cl(gvkey) a(gvkey at95_bins3##year)
	
	*reghdfe `var' _IyeaX* PR if exp_ind, cl(gvkey) a(gvkey year)  
	*reghdfe `var' _IyeaX* PR if exp_ind [aw=DFL], cl(gvkey) a(gvkey year)
	
	*reghdfe `var' _IyeaX* PR if exp_ind, cl(gvkey) a(gvkey at95_bins3##year)  
	*reghdfe `var' _IyeaX* PR if exp_ind [aw=DFL], cl(gvkey) a(gvkey at95_bins3##year)	
	
}	

* Interesting: lnat effects much larger than lnppe
* Even stronger with firm size effects.
	* Depending on spec, some evidence that tangible assets decline while intangibles increase dramatically. Would help explain employment changes!
		* Firms shift toward less labor-intensive production
		* Why? Substitute to foreign tax havens for which the 
		* Punchline: causal effect of tax havens on firm intangible investment
		* Model: "low planning cost" tax haven optimal to compete with other tax havens?
		
* Empirics: difference between ppe and ppent unclear, not sure how robust result is
		
		
		
******************************************
foreach y of var capex_base {
		est clear
		*running the regression
		
		*reghdfe `y' _IyeaX* PR if exp_sec [aw=DFL], cl(gvkey) a(gvkey n2##year)  //n2##
		reghdfe `y' _IyeaX* PR, cl(gvkey) a(year)
		
		preserve
		** figure years for the graph
		gen fig_year = 1991 in 1
		foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
		}

		** annual betas for the preperiod
		gen pr_beta = 0
		gen pr_beta_lb = 0
		gen pr_beta_ub = 0
		foreach i of numlist 1/4 {
			local j = `i' + 1990
			qui lincom _b[_IyeaXPR_`j'] , level(95)
			replace pr_beta = r(estimate) in `i'
			replace pr_beta_lb = r(lb) in `i'
			replace pr_beta_ub = r(ub) in `i'
		}
		* zero effect in 1995
		qui lincom  0
		replace pr_beta = r(estimate) in 5

		** annual betas for the postperiod
		foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
		}


		* outputting the graph
		graph twoway (scatter pr_beta fig_year, lcolor(navy) mcolor(navy) ///
		yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
			(rcap pr_beta_lb pr_beta_ub fig_year, lcolor(navy) lpattern(solid)) ///
			, plotregion(fcolor(white) lcolor(white)) ///
			graphregion(fcolor(white) lcolor(white))  ///
			title("Size Weighting, No Controls") xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
			legend(position(6) on order (1 2) label(1 "Puerto Rico Presence")  ///
			label(2 "95% Confidence Interval") r(1)) ///
			yti("Effect of S936 on ``y'_name'", margin(medium))
			
		restore	
	*graph export "$output/Graphs/eddie/fig_`y'_WgtSize_C3.pdf", replace 
}
******************************************



******************************************
foreach y of var capex_base lninv {
		est clear
		*running the regression
		
		reghdfe `y' _IyeaX* PR [aw=DFL], cl(gvkey) a(gvkey year) //n2##
		
		preserve
		** figure years for the graph
		gen fig_year = 1991 in 1
		foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
		}

		** annual betas for the preperiod
		gen pr_beta = 0
		gen pr_beta_lb = 0
		gen pr_beta_ub = 0
		foreach i of numlist 1/4 {
			local j = `i' + 1990
			qui lincom _b[_IyeaXPR_`j'] , level(95)
			replace pr_beta = r(estimate) in `i'
			replace pr_beta_lb = r(lb) in `i'
			replace pr_beta_ub = r(ub) in `i'
		}
		* zero effect in 1995
		qui lincom  0
		replace pr_beta = r(estimate) in 5

		** annual betas for the postperiod
		foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
		}


		* outputting the graph
		graph twoway (scatter pr_beta fig_year, lcolor(navy) mcolor(navy) ///
		yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
			(rcap pr_beta_lb pr_beta_ub fig_year, lcolor(navy) lpattern(solid)) ///
			, plotregion(fcolor(white) lcolor(white)) ///
			graphregion(fcolor(white) lcolor(white))  ///
			title("DFL Weighting, No Controls") xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
			legend(position(6) on order (1 2) label(1 "Puerto Rico Presence")  ///
			label(2 "95% Confidence Interval") r(1)) ///
			yti("Effect of S936 on ``y'_name'", margin(medium))
			
		restore	
	graph export "$output/Graphs/eddie/fig_`y'_WgtDFL_C3.pdf", replace 
}
******************************************


******************************************
foreach y of var capex_base lninv {
		est clear
		*running the regression
		local x1 "at_9095_bins10"
		*local x1 "at95_bins10"
		*local x1 "c.at_9095"
		
		reghdfe `y' _IyeaX* PR, cl(gvkey) a(gvkey `x1'##i.year n2##year)
		
		preserve
		** figure years for the graph
		gen fig_year = 1991 in 1
		foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
		}

		** annual betas for the preperiod
		gen pr_beta = 0
		gen pr_beta_lb = 0
		gen pr_beta_ub = 0
		foreach i of numlist 1/4 {
			local j = `i' + 1990
			qui lincom _b[_IyeaXPR_`j'] , level(95)
			replace pr_beta = r(estimate) in `i'
			replace pr_beta_lb = r(lb) in `i'
			replace pr_beta_ub = r(ub) in `i'
		}
		* zero effect in 1995
		qui lincom  0
		replace pr_beta = r(estimate) in 5

		** annual betas for the postperiod
		foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
		}


		* outputting the graph
		graph twoway (scatter pr_beta fig_year, lcolor(navy) mcolor(navy) ///
		yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
			(rcap pr_beta_lb pr_beta_ub fig_year, lcolor(navy) lpattern(solid)) ///
			, plotregion(fcolor(white) lcolor(white)) ///
			graphregion(fcolor(white) lcolor(white))  ///
			title("Control: `x1' `x2' ") xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
			legend(position(6) on order (1 2) label(1 "Puerto Rico Presence")  ///
			label(2 "95% Confidence Interval") r(1)) ///
			yti("Effect of S936 on ``y'_name'", margin(medium))
			
		restore	
	graph export "$output/Graphs/eddie/fig_`y'_NoSize_C3.pdf", replace 
}
******************************************





******************************************
foreach y of var capex_base lninv {
		est clear
		*running the regression
		local x1 "at_9095_bins10"
		*local x1 "at95_bins10"
		*local x1 "c.at_9095"
		
		*reghdfe `y' _IyeaX* PR `x1'##i.year, cl(gvkey) a(gvkey n2##year) //n2##
		reghdfe `y' _IyeaX* PR if exp_ind, cl(gvkey) a(gvkey `x1'##i.year n2##year)
		
		preserve
		** figure years for the graph
		gen fig_year = 1991 in 1
		foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
		}

		** annual betas for the preperiod
		gen pr_beta = 0
		gen pr_beta_lb = 0
		gen pr_beta_ub = 0
		foreach i of numlist 1/4 {
			local j = `i' + 1990
			qui lincom _b[_IyeaXPR_`j'] , level(95)
			replace pr_beta = r(estimate) in `i'
			replace pr_beta_lb = r(lb) in `i'
			replace pr_beta_ub = r(ub) in `i'
		}
		* zero effect in 1995
		qui lincom  0
		replace pr_beta = r(estimate) in 5

		** annual betas for the postperiod
		foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
		}


		* outputting the graph
		graph twoway (scatter pr_beta fig_year, lcolor(navy) mcolor(navy) ///
		yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
			(rcap pr_beta_lb pr_beta_ub fig_year, lcolor(navy) lpattern(solid)) ///
			, plotregion(fcolor(white) lcolor(white)) ///
			graphregion(fcolor(white) lcolor(white))  ///
			title("Control: `x1' `x2' ") xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
			legend(position(6) on order (1 2) label(1 "Puerto Rico Presence")  ///
			label(2 "95% Confidence Interval") r(1)) ///
			yti("Effect of S936 on ``y'_name'", margin(medium))
			
		restore	
	graph export "$output/Graphs/eddie/fig_`y'_DecSize_C5.pdf", replace 
}
******************************************






******************************************
foreach y of var capex_base lninv {
		est clear
		*running the regression
		local x1 "at_9095_bins10"
		*local x1 "at95_bins10"
		*local x1 "c.at_9095"
		
		*reghdfe `y' _IyeaX* PR `x1'##i.year, cl(gvkey) a(gvkey n2##year) //n2##
		reghdfe `y' _IyeaX* PR if exp_ind [aw=DFL], cl(gvkey) a(gvkey `x1'##i.year n2##year)
		
		preserve
		** figure years for the graph
		gen fig_year = 1991 in 1
		foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
		}

		** annual betas for the preperiod
		gen pr_beta = 0
		gen pr_beta_lb = 0
		gen pr_beta_ub = 0
		foreach i of numlist 1/4 {
			local j = `i' + 1990
			qui lincom _b[_IyeaXPR_`j'] , level(95)
			replace pr_beta = r(estimate) in `i'
			replace pr_beta_lb = r(lb) in `i'
			replace pr_beta_ub = r(ub) in `i'
		}
		* zero effect in 1995
		qui lincom  0
		replace pr_beta = r(estimate) in 5

		** annual betas for the postperiod
		foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
		}


		* outputting the graph
		graph twoway (scatter pr_beta fig_year, lcolor(navy) mcolor(navy) ///
		yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
			(rcap pr_beta_lb pr_beta_ub fig_year, lcolor(navy) lpattern(solid)) ///
			, plotregion(fcolor(white) lcolor(white)) ///
			graphregion(fcolor(white) lcolor(white))  ///
			title("Control: `x1' `x2' ") xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
			legend(position(6) on order (1 2) label(1 "Puerto Rico Presence")  ///
			label(2 "95% Confidence Interval") r(1)) ///
			yti("Effect of S936 on ``y'_name'", margin(medium))
			
		restore	
	graph export "$output/Graphs/eddie/fig_`y'_DecSize_C6.pdf", replace 
}
******************************************






*** No size controls ***


******************************************
foreach y of var capex_base lninv {
		est clear
		*running the regression
		local x1 "at_9095_bins10"
		*local x1 "at95_bins10"
		*local x1 "c.at_9095"
		
		*reghdfe `y' _IyeaX* PR `x1'##i.year, cl(gvkey) a(gvkey n2##year) //n2##
		reghdfe `y' _IyeaX* PR, cl(gvkey) a(gvkey n2##year)
		
		preserve
		** figure years for the graph
		gen fig_year = 1991 in 1
		foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
		}

		** annual betas for the preperiod
		gen pr_beta = 0
		gen pr_beta_lb = 0
		gen pr_beta_ub = 0
		foreach i of numlist 1/4 {
			local j = `i' + 1990
			qui lincom _b[_IyeaXPR_`j'] , level(95)
			replace pr_beta = r(estimate) in `i'
			replace pr_beta_lb = r(lb) in `i'
			replace pr_beta_ub = r(ub) in `i'
		}
		* zero effect in 1995
		qui lincom  0
		replace pr_beta = r(estimate) in 5

		** annual betas for the postperiod
		foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
		}


		* outputting the graph
		graph twoway (scatter pr_beta fig_year, lcolor(navy) mcolor(navy) ///
		yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
			(rcap pr_beta_lb pr_beta_ub fig_year, lcolor(navy) lpattern(solid)) ///
			, plotregion(fcolor(white) lcolor(white)) ///
			graphregion(fcolor(white) lcolor(white))  ///
			title("No Size Control") xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
			legend(position(6) on order (1 2) label(1 "Puerto Rico Presence")  ///
			label(2 "95% Confidence Interval") r(1)) ///
			yti("Effect of S936 on ``y'_name'", margin(medium))
			
		restore	
	graph export "$output/Graphs/eddie/fig_`y'_NoSize_C3.pdf", replace 
}
******************************************





******************************************
foreach y of var capex_base lninv {
		est clear
		*running the regression
		local x1 "at_9095_bins10"
		*local x1 "at95_bins10"
		*local x1 "c.at_9095"
		
		*reghdfe `y' _IyeaX* PR `x1'##i.year, cl(gvkey) a(gvkey n2##year) //n2##
		reghdfe `y' _IyeaX* PR if exp_ind, cl(gvkey) a(gvkey n2##year)
		
		preserve
		** figure years for the graph
		gen fig_year = 1991 in 1
		foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
		}

		** annual betas for the preperiod
		gen pr_beta = 0
		gen pr_beta_lb = 0
		gen pr_beta_ub = 0
		foreach i of numlist 1/4 {
			local j = `i' + 1990
			qui lincom _b[_IyeaXPR_`j'] , level(95)
			replace pr_beta = r(estimate) in `i'
			replace pr_beta_lb = r(lb) in `i'
			replace pr_beta_ub = r(ub) in `i'
		}
		* zero effect in 1995
		qui lincom  0
		replace pr_beta = r(estimate) in 5

		** annual betas for the postperiod
		foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
		}


		* outputting the graph
		graph twoway (scatter pr_beta fig_year, lcolor(navy) mcolor(navy) ///
		yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
			(rcap pr_beta_lb pr_beta_ub fig_year, lcolor(navy) lpattern(solid)) ///
			, plotregion(fcolor(white) lcolor(white)) ///
			graphregion(fcolor(white) lcolor(white))  ///
			title("No Size Control") xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
			legend(position(6) on order (1 2) label(1 "Puerto Rico Presence")  ///
			label(2 "95% Confidence Interval") r(1)) ///
			yti("Effect of S936 on ``y'_name'", margin(medium))
			
		restore	
	graph export "$output/Graphs/eddie/fig_`y'_NoSize_C5.pdf", replace 
}
******************************************






******************************************
foreach y of var capex_base lninv {
		est clear
		*running the regression
		
		*reghdfe `y' _IyeaX* PR `x1'##i.year, cl(gvkey) a(gvkey n2##year) //n2##
		reghdfe `y' _IyeaX* PR if exp_ind [aw=DFL], cl(gvkey) a(gvkey n2##year)
		
		preserve
		** figure years for the graph
		gen fig_year = 1991 in 1
		foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
		}

		** annual betas for the preperiod
		gen pr_beta = 0
		gen pr_beta_lb = 0
		gen pr_beta_ub = 0
		foreach i of numlist 1/4 {
			local j = `i' + 1990
			qui lincom _b[_IyeaXPR_`j'] , level(95)
			replace pr_beta = r(estimate) in `i'
			replace pr_beta_lb = r(lb) in `i'
			replace pr_beta_ub = r(ub) in `i'
		}
		* zero effect in 1995
		qui lincom  0
		replace pr_beta = r(estimate) in 5

		** annual betas for the postperiod
		foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
		}


		* outputting the graph
		graph twoway (scatter pr_beta fig_year, lcolor(navy) mcolor(navy) ///
		yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
			(rcap pr_beta_lb pr_beta_ub fig_year, lcolor(navy) lpattern(solid)) ///
			, plotregion(fcolor(white) lcolor(white)) ///
			graphregion(fcolor(white) lcolor(white))  ///
			title("No Size Control") xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
			legend(position(6) on order (1 2) label(1 "Puerto Rico Presence")  ///
			label(2 "95% Confidence Interval") r(1)) ///
			yti("Effect of S936 on ``y'_name'", margin(medium))
			
		restore	
	graph export "$output/Graphs/eddie/fig_`y'_NoSize_C6.pdf", replace 
}
******************************************












******************************************
foreach y of var capex_base lninv {
		est clear
		*running the regression
		local x1 "at95_bins5"
		*local x1 "at95_bins10"
		*local x1 "c.at_9095"
		
		reghdfe `y' _IyeaX* PR, cl(gvkey) a(gvkey `x1'##i.year n2##year)
		
		preserve
		** figure years for the graph
		gen fig_year = 1991 in 1
		foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
		}

		** annual betas for the preperiod
		gen pr_beta = 0
		gen pr_beta_lb = 0
		gen pr_beta_ub = 0
		foreach i of numlist 1/4 {
			local j = `i' + 1990
			qui lincom _b[_IyeaXPR_`j'] , level(95)
			replace pr_beta = r(estimate) in `i'
			replace pr_beta_lb = r(lb) in `i'
			replace pr_beta_ub = r(ub) in `i'
		}
		* zero effect in 1995
		qui lincom  0
		replace pr_beta = r(estimate) in 5

		** annual betas for the postperiod
		foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
		}


		* outputting the graph
		graph twoway (scatter pr_beta fig_year, lcolor(navy) mcolor(navy) ///
		yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
			(rcap pr_beta_lb pr_beta_ub fig_year, lcolor(navy) lpattern(solid)) ///
			, plotregion(fcolor(white) lcolor(white)) ///
			graphregion(fcolor(white) lcolor(white))  ///
			title("Control: Size Quintiles") xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
			legend(position(6) on order (1 2) label(1 "Puerto Rico Presence")  ///
			label(2 "95% Confidence Interval") r(1)) ///
			yti("Effect of S936 on ``y'_name'", margin(medium))
			
		restore	
	graph export "$output/Graphs/eddie/fig_`y'_Qtle_C3.pdf", replace 
}
******************************************




******************************************
foreach y of var capex_base lninv {
		est clear
		*running the regression
		local x1 "at95_bins3"
		*local x1 "at95_bins10"
		*local x1 "c.at_9095"
		
		reghdfe `y' _IyeaX* PR, cl(gvkey) a(gvkey `x1'##i.year n2##year)
		
		preserve
		** figure years for the graph
		gen fig_year = 1991 in 1
		foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
		}

		** annual betas for the preperiod
		gen pr_beta = 0
		gen pr_beta_lb = 0
		gen pr_beta_ub = 0
		foreach i of numlist 1/4 {
			local j = `i' + 1990
			qui lincom _b[_IyeaXPR_`j'] , level(95)
			replace pr_beta = r(estimate) in `i'
			replace pr_beta_lb = r(lb) in `i'
			replace pr_beta_ub = r(ub) in `i'
		}
		* zero effect in 1995
		qui lincom  0
		replace pr_beta = r(estimate) in 5

		** annual betas for the postperiod
		foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
		}


		* outputting the graph
		graph twoway (scatter pr_beta fig_year, lcolor(navy) mcolor(navy) ///
		yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
			(rcap pr_beta_lb pr_beta_ub fig_year, lcolor(navy) lpattern(solid)) ///
			, plotregion(fcolor(white) lcolor(white)) ///
			graphregion(fcolor(white) lcolor(white))  ///
			title("Control: Size Terciles ") xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
			legend(position(6) on order (1 2) label(1 "Puerto Rico Presence")  ///
			label(2 "95% Confidence Interval") r(1)) ///
			yti("Effect of S936 on ``y'_name'", margin(medium))
			
		restore	
	graph export "$output/Graphs/eddie/fig_`y'_Tcle_C3.pdf", replace 
}
******************************************

