// This file recreates Figure 5
// Author: Dan Garrett
// Date: 3-31-2020

/* Data sources:
various sources including NETS and compustat
*/

clear all
set more off
snapshot erase _all 


*** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *
*** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *
use "$data/Replication Data/newCompu_Clean_0923", replace
tab n3 if exp_sec==1
tab n3 if exp_ind==1

gen exp_PR = (PR==0)*exp_ind
drop if PR==1

* What variation do we have?
tab n2 exp_PR if year==1995
gen temp1 = (exp_PR==1)
gen temp2 = (exp_PR==0)
egen flag1 = max(temp1), by(n2)
egen flag2 = max(temp2), by(n2)
gen flag_All = flag1*flag2
drop flag? temp?

tab n2 exp_PR if year==1995 & flag_All==1
tab n3 exp_PR if year==1995 & flag_All==1

* What's going on with exposed sectors? *
* Non-PR firms in exposed sectors experience big declines?
* Largest PR firms have negative investment when excluding other exposed sector firms


*** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *
*** *** Section (1): Summarize firm size distribution *** ***
*** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *

use "$data/Replication Data/newCompu_Clean_0923", replace
keep if year==1995
gen firms = 1
gen PR_exp = (PR==0)*exp_sec

gen PR_ = "Puerto Rico Firms" if PR==1
replace PR_ = "Control Firms" if PR==0
replace PR_ = "Exposed Controls Firms" if PR_exp==1



* Deciles
preserve
	collapse (sum) firms (mean) at95 at_9095, by(at95_bins10 PR_)
	label variable firms "Number of Firms"


	graph bar firms, over(PR) over(at95_bins10) asyvars ///
		ytitle("Number of Firms") legend(pos(6) rows(1)) blabel(bar)
	graph export "$output/Graphs/kevin/deciles_firms_EXP.pdf", replace 	
		
	graph bar at95, over(PR) over(at95_bins10) asyvars ///
		ytitle("Mean Assets, 1995") legend(pos(6) rows(1)) 
	graph export "$output/Graphs/kevin/deciles_at95_EXP.pdf", replace 	
		
	graph bar at_9095, over(PR) over(at95_bins10) asyvars ///
		ytitle("Mean Assets, 1990-1995") legend(pos(6) rows(1)) 	
	graph export "$output/Graphs/kevin/deciles_at9095_EXP.pdf", replace 		
restore

* Terciles
preserve
	collapse (sum) firms (mean) at95 at_9095, by(at95_bins3 PR_)
	label variable firms "Number of Firms"


	graph bar firms, over(PR) over(at95_bins3) asyvars ///
		ytitle("Number of Firms") legend(pos(6) rows(1)) blabel(bar)
	graph export "$output/Graphs/kevin/terciles_firms_EXP.pdf", replace 	
		
	graph bar at95, over(PR) over(at95_bins3) asyvars ///
		ytitle("Mean Assets, 1995") legend(pos(6) rows(1)) 
	graph export "$output/Graphs/kevin/terciles_at95_EXP.pdf", replace 	
		
	graph bar at_9095, over(PR) over(at95_bins3) asyvars ///
		ytitle("Mean Assets, 1990-1995") legend(pos(6) rows(1)) 	
	graph export "$output/Graphs/kevin/terciles_at9095_EXP.pdf", replace 		
restore
*/

*** *** *** *** *** *** *** *** *** *** *** *** *** ***
*** *** *** Section (2): Exposed Industries *** *** ***
*** *** *** *** *** *** *** *** *** *** *** *** *** ***
use "$data/Replication Data/newCompu_Clean_0923", replace
gen exp_PR = (PR==0)*exp_ind
xi i.year|exp_PR, noomit
drop _IyeaXexp_1995 
drop if PR==1

* What variation do we have?
tab n2 exp_PR if year==1995
gen temp1 = (exp_PR==1)
gen temp2 = (exp_PR==0)
egen flag1 = max(temp1), by(n2)
egen flag2 = max(temp2), by(n2)
gen flag_All = flag1*flag2
drop flag? temp?

tab n2 exp_PR if year==1995 & flag_All==1
tab n3 exp_PR if year==1995 & flag_All==1

global spec1 ", cl(gvkey) a(at95_bins10##year)"
global spec2 "[aw=DFL], cl(gvkey) a(at95_bins10##year)"
global spec3 ", cl(gvkey) a(n2##year at95_bins10##year)"
global spec4 "[aw=DFL], cl(gvkey) a(n2##year at95_bins10##year)"
foreach spec in spec1 spec2 spec3 spec4  {
	reghdfe capex_base _IyeaXexp* PR $`spec'
	reghdfe capex_base _IyeaXexp* PR if flag_All==1 $`spec'
}

*** Baseline ***
global spec1 ", cl(gvkey) a(year)"
global spec2 "[aw=DFL], cl(gvkey) a(year)"
global spec3 ", cl(gvkey) a(gvkey year)"
global spec4 "[aw=DFL], cl(gvkey) a(gvkey year)"

global specname1 "Baseline"
global specname2 "Baseline + DFL"
global specname3 "Firm FE"
global specname4 "Firm FE + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"


*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4  {
reghdfe capex_base _IyeaXexp* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXexp_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXexp_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/4 {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 , yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")) ///
		yti("Effect of Indirect S936 Exposure on Investment", margin(medium))
restore	
}

graph export "$output/Graphs/kevin/fig_Spill_base.pdf", replace 	
	
	
*** Deciles ***
global spec1 ", cl(gvkey) a(at95_bins10##year)"
global spec2 "[aw=DFL], cl(gvkey) a(at95_bins10##year)"
global spec3 ", cl(gvkey) a(gvkey at95_bins10##year)"
global spec4 "[aw=DFL], cl(gvkey) a(gvkey at95_bins10##year)"

global specname1 "Baseline"
global specname2 "Baseline + DFL"
global specname3 "Firm FE"
global specname4 "Firm FE + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"


*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4  {
reghdfe capex_base _IyeaXexp* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXexp_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXexp_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/4 {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 , yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")) ///
		yti("Effect of Indirect S936 Exposure on Investment", margin(medium))
restore	
}

graph export "$output/Graphs/kevin/fig_Spill_Deciles.pdf", replace 	
		
*** Deciles + Sector FEs ***	
keep if flag_All==1

global spec1 ", cl(gvkey) a(n2##year at95_bins10##year)"
global spec2 "[aw=DFL], cl(gvkey) a(n2##year at95_bins10##year)"
global spec3 ", cl(gvkey) a(gvkey n2##year at95_bins10##year)"
global spec4 "[aw=DFL], cl(gvkey) a(gvkey n2##year at95_bins10##year)"

global specname1 "Baseline"
global specname2 "Baseline + DFL"
global specname3 "Firm FE"
global specname4 "Firm FE + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"


*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4  {
reghdfe capex_base _IyeaXexp* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXexp_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXexp_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/4 {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 , yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")) ///
		yti("Effect of Indirect S936 Exposure on Investment", margin(medium))
restore	
}

graph export "$output/Graphs/kevin/fig_Spill_DecN2.pdf", replace 		
		
		

*** *** *** *** *** *** *** ***  *** *** *** *** ***
*** *** *** Section (2): Exposed Sectors *** *** ***
*** *** *** *** *** *** *** ***  *** *** *** *** ***
use "$data/Replication Data/newCompu_Clean_0923", replace
gen exp_PR = (PR==0)*exp_sec
xi i.year|exp_PR, noomit
drop _IyeaXexp_1995 
drop if PR==1
*keep if at95_bins10==10


*** Baseline ***
global spec1 ", cl(gvkey) a(year)"
global spec2 "[aw=DFL], cl(gvkey) a(year)"
global spec3 ", cl(gvkey) a(gvkey year)"
global spec4 "[aw=DFL], cl(gvkey) a(gvkey year)"

global specname1 "Baseline"
global specname2 "Baseline + DFL"
global specname3 "Firm FE"
global specname4 "Firm FE + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"


*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4  {
reghdfe capex_base _IyeaXexp* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXexp_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXexp_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/4 {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 , yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")) ///
		yti("Effect of Indirect S936 Exposure on Investment", margin(medium))
restore	
}

graph export "$output/Graphs/kevin/fig_SpillSec_base.pdf", replace 	
	
*** Deciles ***	
global spec1 ", cl(gvkey) a(at95_bins10##year)"
global spec2 "[aw=DFL], cl(gvkey) a(at95_bins10##year)"
global spec3 ", cl(gvkey) a(gvkey at95_bins10##year)"
global spec4 "[aw=DFL], cl(gvkey) a(gvkey at95_bins10##year)"

global specname1 "Baseline"
global specname2 "Baseline + DFL"
global specname3 "Firm FE"
global specname4 "Firm FE + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"


*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4  {
reghdfe capex_base _IyeaXexp* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXexp_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXexp_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/4 {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 , yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")) ///
		yti("Effect of Indirect S936 Exposure on Investment", margin(medium))
restore	
}

graph export "$output/Graphs/kevin/fig_SpillSec_Deciles.pdf", replace 		


*** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
*** *** *** Section (3): Drop control exposed firms *** *** ***
*** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
use "$data/Replication Data/newCompu_Clean_0923", replace
drop if exp_ind==1 & PR==0

*** Baseline ***
global spec1 ", cl(gvkey) a(year)"
global spec2 "[aw=DFL], cl(gvkey) a(year)"
global spec3 ", cl(gvkey) a(gvkey year)"
global spec4 "[aw=DFL], cl(gvkey) a(gvkey year)"

global specname1 "Baseline"
global specname2 "Baseline + DFL"
global specname3 "Firm FE"
global specname4 "Firm FE + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"


*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4  {
reghdfe capex_base _IyeaX* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/4 {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 , yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")) ///
		yti("Effect of Indirect S936 Exposure on Investment", margin(medium))
restore	
}

graph export "$output/Graphs/kevin/fig_DropExp_Base.pdf", replace 	


*** Deciles ***
global spec1 ", cl(gvkey) a(at95_bins10##year)"
global spec2 "[aw=DFL], cl(gvkey) a(at95_bins10##year)"
global spec3 ", cl(gvkey) a(gvkey at95_bins10##year)"
global spec4 "[aw=DFL], cl(gvkey) a(gvkey at95_bins10##year)"

global specname1 "Baseline"
global specname2 "Baseline + DFL"
global specname3 "Firm FE"
global specname4 "Firm FE + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"


*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4  {
reghdfe capex_base _IyeaXPR* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/4 {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 , yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")) ///
		yti("Effect of S936 Exposure on Investment", margin(medium))
restore	
}

graph export "$output/Graphs/kevin/fig_DropExp_Deciles.pdf", replace 	
			
			
			
			

*** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
*** *** *** Section (3): Drop control exposed firms *** *** ***
*** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
use "$data/Replication Data/newCompu_Clean_0923", replace
drop if exp_sec==1 & PR==0
keep if at95_bins3==3
*keep if at95_bins10==10

*** Baseline ***
global spec1 ", cl(gvkey) a(year)"
global spec2 "[aw=DFL], cl(gvkey) a(year)"
global spec3 ", cl(gvkey) a(gvkey year)"
global spec4 "[aw=DFL], cl(gvkey) a(gvkey year)"

global specname1 "Baseline"
global specname2 "Baseline + DFL"
global specname3 "Firm FE"
global specname4 "Firm FE + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"


*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4  {
reghdfe capex_base _IyeaX* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/4 {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 , yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")) ///
		yti("Effect of Indirect S936 Exposure on Investment", margin(medium))
restore	
}

graph export "$output/Graphs/kevin/fig_DropExp_Base_Large.pdf", replace 	


*** Deciles ***
global spec1 ", cl(gvkey) a(ppe95_bins10_ALT##year)"
global spec2 "[aw=DFL], cl(gvkey) a(ppe95_bins10_ALT##year)"
global spec3 ", cl(gvkey) a(gvkey ppe95_bins10_ALT##year)"
global spec4 "[aw=DFL], cl(gvkey) a(gvkey ppe95_bins10_ALT##year)"

global specname1 "Baseline"
global specname2 "Baseline + DFL"
global specname3 "Firm FE"
global specname4 "Firm FE + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"

foreach spec in spec1 spec2 spec3 spec4  {
	reghdfe capex_base PR_post PR $`spec'
}

*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4  {
reghdfe capex_base _IyeaXPR* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/4 {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 , yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")) ///
		yti("Effect of S936 Exposure on Investment", margin(medium))
restore	
}

graph export "$output/Graphs/kevin/fig_DropExp_Deciles_Large.pdf", replace 	
				
		

*** Deciles + Sector Controls ***
global spec1 ", cl(gvkey) a(n2##year ppe95_bins10_ALT##year)"
global spec2 "[aw=DFL], cl(gvkey) a(n2##year ppe95_bins10_ALT##year)"
global spec3 ", cl(gvkey) a(gvkey n2##year ppe95_bins10_ALT##year)"
global spec4 "[aw=DFL], cl(gvkey) a(gvkey n2##year ppe95_bins10_ALT##year)"

global specname1 "Baseline"
global specname2 "Baseline + DFL"
global specname3 "Firm FE"
global specname4 "Firm FE + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"

foreach spec in spec1 spec2 spec3 spec4  {
	reghdfe capex_base PR_post PR $`spec'
}

*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4  {
reghdfe capex_base _IyeaXPR* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/4 {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 , yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")) ///
		yti("Effect of S936 Exposure on Investment", margin(medium))
restore	
}

graph export "$output/Graphs/kevin/fig_DropExp_DecN2_Large.pdf", replace 	
						
						

						
						

*** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
*** *** *** Section (3): Drop control exposed firms *** *** ***
*** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
use "$data/Replication Data/newCompu_Clean_0923", replace
drop if exp_sec==1 & PR==0
keep if at95_bins3==3
*keep if at95_bins10==10

*** Baseline ***
global spec1 ", cl(gvkey) a(year)"
global spec2 "[aw=DFL], cl(gvkey) a(year)"
global spec3 ", cl(gvkey) a(gvkey year)"
global spec4 "[aw=DFL], cl(gvkey) a(gvkey year)"

global specname1 "Baseline"
global specname2 "Baseline + DFL"
global specname3 "Firm FE"
global specname4 "Firm FE + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"


*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4  {
reghdfe capex_base _IyeaX* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/4 {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 , yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")) ///
		yti("Effect of Indirect S936 Exposure on Investment", margin(medium))
restore	
}

graph export "$output/Graphs/kevin/fig_DropExp_Base_Top3rd.pdf", replace 	


*** Deciles ***
global spec1 ", cl(gvkey) a(ppe95_bins10_ALT##year)"
global spec2 "[aw=DFL], cl(gvkey) a(ppe95_bins10_ALT##year)"
global spec3 ", cl(gvkey) a(gvkey ppe95_bins10_ALT##year)"
global spec4 "[aw=DFL], cl(gvkey) a(gvkey ppe95_bins10_ALT##year)"

global specname1 "Baseline"
global specname2 "Baseline + DFL"
global specname3 "Firm FE"
global specname4 "Firm FE + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"

foreach spec in spec1 spec2 spec3 spec4  {
	reghdfe capex_base PR_post PR $`spec'
}

*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4  {
reghdfe capex_base _IyeaXPR* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/4 {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 , yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")) ///
		yti("Effect of S936 Exposure on Investment", margin(medium))
restore	
}

graph export "$output/Graphs/kevin/fig_DropExp_Deciles_Top3rd.pdf", replace 	
				
		

*** Deciles + Sector Controls ***
global spec1 ", cl(gvkey) a(n2##year ppe95_bins10_ALT##year)"
global spec2 "[aw=DFL], cl(gvkey) a(n2##year ppe95_bins10_ALT##year)"
global spec3 ", cl(gvkey) a(gvkey n2##year ppe95_bins10_ALT##year)"
global spec4 "[aw=DFL], cl(gvkey) a(gvkey n2##year ppe95_bins10_ALT##year)"

global specname1 "Baseline"
global specname2 "Baseline + DFL"
global specname3 "Firm FE"
global specname4 "Firm FE + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"

foreach spec in spec1 spec2 spec3 spec4  {
	reghdfe capex_base PR_post PR $`spec'
}

*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4  {
reghdfe capex_base _IyeaXPR* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/4 {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 , yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")) ///
		yti("Effect of S936 Exposure on Investment", margin(medium))
restore	
}

graph export "$output/Graphs/kevin/fig_DropExp_DecN2_Top3rd.pdf", replace 	
						