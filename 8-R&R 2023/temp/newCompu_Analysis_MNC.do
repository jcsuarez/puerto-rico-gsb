// This file recreates Figure 5
// Author: Dan Garrett
// Date: 3-31-2020

/* Data sources:
various sources including NETS and compustat
*/

clear all
set more off
snapshot erase _all 


*** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *
*** *** Section (0): Summarize firm size distribution *** ***
*** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *

use "$data/Replication Data/newCompu_Clean_0923", replace
tab idbflag PR if year==1995
keep if year==1995
gen firms = 1
drop if PR==0 & idbflag=="D"

gen PR_ = "Puerto Rico Firms" if PR==1
replace PR_ = "Control Firms" if PR==0

* Deciles
preserve
	collapse (sum) firms (mean) at95 at_9095, by(at95_bins10 PR_)
	label variable firms "Number of Firms"


	graph bar firms, over(PR) over(at95_bins10) asyvars ///
		ytitle("Number of Firms") legend(pos(6) rows(1)) blabel(bar)
	graph export "$output/Graphs/kevin/deciles_firms_MNC.pdf", replace 	
		
	graph bar at95, over(PR) over(at95_bins10) asyvars ///
		ytitle("Mean Assets, 1995") legend(pos(6) rows(1)) 
	graph export "$output/Graphs/kevin/deciles_at95_MNC.pdf", replace 	
		
	graph bar at_9095, over(PR) over(at95_bins10) asyvars ///
		ytitle("Mean Assets, 1990-1995") legend(pos(6) rows(1)) 	
	graph export "$output/Graphs/kevin/deciles_at9095_MNC.pdf", replace 		
restore

* Terciles
preserve
	collapse (sum) firms (mean) at95 at_9095, by(at95_bins3 PR_)
	label variable firms "Number of Firms"


	graph bar firms, over(PR) over(at95_bins3) asyvars ///
		ytitle("Number of Firms") legend(pos(6) rows(1)) blabel(bar)
	graph export "$output/Graphs/kevin/terciles_firms_MNC.pdf", replace 	
		
	graph bar at95, over(PR) over(at95_bins3) asyvars ///
		ytitle("Mean Assets, 1995") legend(pos(6) rows(1)) 
	graph export "$output/Graphs/kevin/terciles_at95_MNC.pdf", replace 	
		
	graph bar at_9095, over(PR) over(at95_bins3) asyvars ///
		ytitle("Mean Assets, 1990-1995") legend(pos(6) rows(1)) 	
	graph export "$output/Graphs/kevin/terciles_at9095_MNC.pdf", replace 		
restore



*** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *
*** *** Section (0): Summarize firm size distribution *** ***
*** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *

use "$data/Replication Data/newCompu_Clean_0923", replace
keep if year==1995
gen firms = 1
drop if idbflag=="D"

gen PR_ = "Puerto Rico Firms" if PR==1
replace PR_ = "Control Firms" if PR==0

* Deciles
preserve
	collapse (sum) firms (mean) at95 at_9095, by(at95_bins10 PR_)
	label variable firms "Number of Firms"


	graph bar firms, over(PR) over(at95_bins10) asyvars ///
		ytitle("Number of Firms") legend(pos(6) rows(1)) blabel(bar)
	graph export "$output/Graphs/kevin/deciles_firms_onlyMNC.pdf", replace 	
		
	graph bar at95, over(PR) over(at95_bins10) asyvars ///
		ytitle("Mean Assets, 1995") legend(pos(6) rows(1)) 
	graph export "$output/Graphs/kevin/deciles_at95_onlyMNC.pdf", replace 	
		
	graph bar at_9095, over(PR) over(at95_bins10) asyvars ///
		ytitle("Mean Assets, 1990-1995") legend(pos(6) rows(1)) 	
	graph export "$output/Graphs/kevin/deciles_at9095_onlyMNC.pdf", replace 		
restore


*** *** *** *** *** *** *** *** *** *** *** *** *** ***
*** *** *** Section (1): Drop all Domestic *** *** ***
*** *** *** *** *** *** *** *** *** *** *** *** *** ***
use "$data/Replication Data/newCompu_Clean_0923", replace
tab idbflag PR if year==1995
tab fic
*drop if PR==0 & idbflag=="D"
keep if idbflag=="B"

global spec1 ", cl(gvkey) a(ppe95_bins10##year)"
global spec2 ", cl(gvkey) a(ppe95_bins10##year)"
global spec3 ", cl(gvkey) a(n2##year ppe95_bins10##year)"
global spec4 ", cl(gvkey) a(gvkey n2##year ppe95_bins10##year)"
global spec5 "[aw=DFL], cl(gvkey) a(gvkey n2##year ppe95_bins10##year)"


global specname1 "Baseline: No Controls"
global specname2 "95 PPE: Baseline"
global specname3 "95 PPE: Sector FE"
global specname4 "95 PPE: Firm + Sector FE"
global specname5 "95 PPE: Firm + Sector FE + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"


*** Store estimates ***
local cnt = 0
foreach spec in spec1 spec2 spec3 spec4 spec5 {
local cnt = `cnt' + 1
reghdfe capex_base _IyeaX* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/`cnt' {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec5", lco($c5) mco($c5) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec5", lco($c5) lpa(solid)) ///
	 , yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7 9) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3") ///
		label(7 "$specname4") ///
		label(9 "$specname5")  ) ///
		yti("Effect of S936 Exposure on Investment", margin(medium))
restore	
}

graph export "$output/Graphs/kevin/fig_MNC_Only.pdf", replace 	
	
	

*** *** *** *** *** *** *** *** *** *** *** *** *** ***
*** *** *** Section (2): Drop control Domestic *** *** ***
*** *** *** *** *** *** *** *** *** *** *** *** *** ***
use "$data/Replication Data/newCompu_Clean_0923", replace
tab idbflag PR if year==1995
tab fic
drop if PR==0 & idbflag=="D"

global spec1 ", cl(gvkey) a(ppe95_bins10##year)"
global spec2 ", cl(gvkey) a(ppe95_bins10##year)"
global spec3 ", cl(gvkey) a(n2##year ppe95_bins10##year)"
global spec4 ", cl(gvkey) a(gvkey n2##year ppe95_bins10##year)"
global spec5 "[aw=DFL], cl(gvkey) a(gvkey n2##year ppe95_bins10##year)"

global specname1 "Baseline: No Controls"
global specname2 "95 PPE: Baseline"
global specname3 "95 PPE: Sector FE"
global specname4 "95 PPE: Firm + Sector FE"
global specname5 "95 PPE: Firm + Sector FE + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"


*** Store estimates ***
local cnt = 0
foreach spec in spec1 spec2 spec3 spec4 spec5 {
local cnt = `cnt' + 1
reghdfe capex_base _IyeaX* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/`cnt' {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec5", lco($c5) mco($c5) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec5", lco($c5) lpa(solid)) ///
	 , yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7 9) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3") ///
		label(7 "$specname4") ///
		label(9 "$specname5")  ) ///
		yti("Effect of S936 Exposure on Investment", margin(medium))
restore	
}

graph export "$output/Graphs/kevin/fig_Drop_D_controls.pdf", replace 	
		
		

*** *** *** *** *** *** *** *** *** *** *** *** *** ***
*** *** *** Section (2): Drop control Domestic *** *** ***
*** *** *** *** *** *** *** *** *** *** *** *** *** ***
use "$data/Replication Data/newCompu_Clean_0923", replace
tab idbflag PR if year==1995
tab fic
drop if PR==0 & idbflag=="D"

sum ppe95 if PR==1 & year==1995, detail
cdfplot ppe95 if PR==1 & year==1995
lorenz estimate ppe95 if PR==1 & year==1995
lorenz graph, aspectratio(1) xlabel(, grid)

xtile PR_sizeDist = ppe95 if PR==1, n(3)
xtile PR_sizeDist2 = ppe95 if PR==1, n(4)
xtile PR_sizeDist3 = ppe95 if PR==1, n(5)
xtile PR_sizeDist4 = ppe95 if PR==1, n(20)

sum ppe95 if PR==1 & year==1995, detail


tab PR if year==1995, sum(ppe95)
tab PR_sizeDist if year==1995, sum(ppe95)
drop if PR_sizeDist2<3
*drop if PR_sizeDist3<4
*drop if PR_sizeDist==1 & PR_sizeDist==3

* some DFL weights
xtile ppe95_bin20 = ppent if year == 1995 , n(20) 
logit PR i.n2##i.ppe95_bin20 if year == 1995
capture: drop phat min_phat w w_phat
predict phat, pr 
winsor phat, p(.01) g(w_phat) 
replace phat = w_phat
bys gvkey: egen min_phat = min(phat) 
* ATE
* gen DFL = (PR/min_phat+(1-PR)/(1-min_phat))
* ATOT
gen DFL2 = (PR+(1-PR)*min_phat/(1-min_phat))



global spec1 ", cl(gvkey) a(ppe95_bins10##year)"
global spec2 "[aw=DFL2], cl(gvkey) a(ppe95_bins10##year)"
global spec3 ", cl(gvkey) a(n2##year ppe95_bins10##year)"
global spec4 "[aw=DFL2], cl(gvkey) a(n2##year ppe95_bins10##year)"


global specname1 "95 PPE: Baseline"
global specname2 "95 PPE: Baseline + DFL"
global specname3 "95 PPE: Sector FE"
global specname4 "95 PPE: Sector FE + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"

foreach spec in spec1 spec2 spec3 spec4 {
	reghdfe capex_base _IyeaX* PR $`spec'
}

*** Store estimates ***
local cnt = 0
foreach spec in spec1 spec2 spec3 spec4 {
local cnt = `cnt' + 1
reghdfe capex_base _IyeaX* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/`cnt' {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 , yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7 ) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3") ///
		label(7 "$specname4")  ) ///
		yti("Effect of S936 Exposure on Investment", margin(medium))
restore	
}

graph export "$output/Graphs/kevin/fig_Drop_D_DropTer1st.pdf", replace 	
				
	
	

*** *** *** *** *** *** *** *** *** *** *** *** *** ***
*** *** *** Section (2): Drop control Domestic *** *** ***
*** *** *** *** *** *** *** *** *** *** *** *** *** ***
use "$data/Replication Data/newCompu_Clean_0923", replace
tab idbflag PR if year==1995
tab fic
drop if PR==0 & idbflag=="D"

xtile PR_sizeDist = ppe95 if PR==1, n(3)
xtile PR_sizeDist2 = ppe95 if PR==1, n(4)
xtile PR_sizeDist3 = ppe95 if PR==1, n(5)
tab PR if year==1995, sum(ppe95)
tab PR_sizeDist if year==1995, sum(ppe95)
drop if PR_sizeDist3<4
*drop if PR_sizeDist==1 & PR_sizeDist==3

* some DFL weights
xtile ppe95_bin20 = ppent if year == 1995 , n(20) 
logit PR i.n2##i.ppe95_bin20 if year == 1995
capture: drop phat min_phat w w_phat
predict phat, pr 
winsor phat, p(.01) g(w_phat) 
replace phat = w_phat
bys gvkey: egen min_phat = min(phat) 
* ATE
* gen DFL = (PR/min_phat+(1-PR)/(1-min_phat))
* ATOT
gen DFL2 = (PR+(1-PR)*min_phat/(1-min_phat))



global spec1 ", cl(gvkey) a(gvkey ppe95_bins10##year)"
global spec2 "[aw=DFL2], cl(gvkey) a(gvkey ppe95_bins10##year)"
global spec3 ", cl(gvkey) a(gvkey n2##year ppe95_bins10##year)"
global spec4 "[aw=DFL2], cl(gvkey) a(gvkey n2##year ppe95_bins10##year)"


global specname1 "95 PPE: Baseline"
global specname2 "95 PPE: Baseline + DFL"
global specname3 "95 PPE: Sector FE"
global specname4 "95 PPE: Sector FE + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"

foreach spec in spec1 spec2 spec3 spec4 {
	reghdfe lninv _IyeaX* PR $`spec'
}

*** Store estimates ***
local cnt = 0
foreach spec in spec1 spec2 spec3 spec4 {
local cnt = `cnt' + 1
reghdfe capex_base _IyeaX* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/`cnt' {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 , yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7 ) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3") ///
		label(7 "$specname4")  ) ///
		yti("Effect of S936 Exposure on Investment", margin(medium))
restore	
}

graph export "$output/Graphs/kevin/lninv_Drop_D_DropTer1st.pdf", replace 	
				
		