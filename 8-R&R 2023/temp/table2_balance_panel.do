// This file recreates Table 2
// Author: Dan Garrett
// Date: 4-20-2020

/* Data sources:
NETS and Compustat
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data/table2_data_KR", clear

* Make balanced panel!
* Step 1: Identify firms that survive from 1991 to 2006
sort gvkey year

* Step 2: Filter the original dataset to include only surviving firms (balanced panel)
by gvkey: gen firm_survival = _N
keep if firm_survival == 16 // 46,960 left out of 115,224
drop firm_survival

* formatting, labeling, and descriptive scalar creation
local FE_fs_capx "n3"
local FE_capex_base "gvkey"
local FE_capex_base_p "gvkey"
local FE_capex_base_1 "gvkey"
local FE_ftax "gvkey"

local FE_fs_capx_word "Industry"
local FE_capex_base_word "Firm"
local FE_capex_base_p_word "Firm"
local FE_capex_base_1_word "Firm"
local FE_ftax_word "Firm"

local fs_capx_sum "fs_capx"
local capex_base_sum "IK"
local capex_base_p_sum "IKIK"
local capex_base_1_sum "IK_1"
local ftax_sum "ftax"

local fs_capx_scalars1 ""
local capex_base_scalars1 "avg Sample Average I/K in 2006" 
local capex_base_p_scalars1 "avg Sample Average I/K in 2006 Relative to 1995"
local capex_base_1_scalars1 "avg Sample Average I/K in 2006"
local ftax_scalars1 ""

local fs_capx_scalars2 ""
local capex_base_scalars2 "elas Percent of 2006 Average"
local capex_base_p_scalars2 "elas Percent of 2006 Average"
local capex_base_1_scalars2 "elas Percent of 2006 Average"
local ftax_scalars2 ""

local fs_capx_scalars3 ""
local capex_base_scalars3 "elast Semi-elasticity of Investment"
local capex_base_p_scalars3 "elast Semi-elasticity of Investment"
local capex_base_1_scalars3 "elast Semi-elasticity of Investment"
local ftax_scalars3 ""

local fs_capx_scalars4 ""
local capex_base_scalars4 "pp_dif Change in Effective Tax Rate"
local capex_base_p_scalars4 "pp_dif Change in Effective Tax Rate"
local capex_base_1_scalars4 "pp_dif Change in Effective Tax Rate"
local ftax_scalars4 ""

local fs_capx_title "Change in Foreign Share of Investment"
local capex_base_title "Change in Investment: $ \frac{I}{K_{1990-1995}} $"
local capex_base_p_title "Percent Change in Investment: $ \frac{I}{I_{1990-1995}} - 1 $"
local capex_base_1_title "Change in Investment: $ \frac{I}{K_{1990-1995}} $"
local ftax_title "Change in Federal Taxes Paid as a Percent of Pretax Income"
xtset gvkey year

* scalar values from SOI data analysis:
scalar p_dif_v0 = .0572734
scalar p_dif_v1 = .0699663
scalar p_dif_v2 = .0465333
scalar p_dif_v3 = .0601001

* major sectors:
local if2_1 = "inlist(n2,3,5,6,7,11,12)"



*********************
*** Test controls ***
*********************

* Employment stuff
sum emps, detail
sum tot_emp, detail
sum log_tot_emp, detail
tab year PR, sum(emps)
tab year PR, sum(tot_emp)
tab year PR, sum(log_tot_emp)

* Revenue
sum revts, detail
sum IHS_rev, detail
tab year PR, sum(revts)
tab year PR, sum(IHS_rev)

* Investment rate
sum capex, detail
sum capex_base, detail
tab year PR, sum(capex)
tab year PR, sum(capex_base)

* Capxs
sum capxs, detail
tab year PR, sum(capxs)

* Identifiable Total Assets
sum ias, detail
sum tot_ias, detail
tab year PR, sum(ias)
tab year PR, sum(tot_ias)


* Net Income --> no observation pre 1997; so can't use it
sum nis, detail
tab year PR, sum(nis)

* Sales
sum sales, detail
sum salexg, detail
tab year PR, sum(sales)
tab year PR, sum(salexg)



* better controls
*** Fix variables in 1994 at firm level, interacting with year
foreach x of var capxs capex emps ias nis revts sales {
	di `x'
	tempvar temp1
	local new_var "`x'_94"
	di "`new_var'"
	gen `temp1' = `x' if year==1994 
	summ `temp1'
	egen `new_var' = mean(`temp1'), by(gvkey)
}

*** Fix variables as 1990-1994 mean at firm level, interacting with year
foreach x of var capxs capex emps ias nis revts sales {
	di `x'
	tempvar temp1 temp2
	local new_var "`x'_9094mean"
	di "`new_var'"
	gen `temp1' = `x' if year >= 1991 & year <= 1994
	summ `temp1'
	egen `new_var' = mean(`temp1'), by(gvkey)
}

*** Quintiles, decile version of controls
foreach x of var capxs_* capex_* emps_* ias_* nis_* revts_* sales_* {
	di `x'

	forvalues i=5(5)10 {
		tempvar temp1
		local new_var "`x'_bin`i'"
		di "`new_var'"
		
		xtile `new_var' = `x', n(`i')
		summ `new_var'
	}
}

* (1) Benchmark with no firm size controls
est clear
local y capex_base
local j = 1
// 	eststo reg_`y'_`j'_z: areg `y' ib(last).year PR PR_post, cl(gvkey)   a(year)
// 	lincom PR_post
// 	sum ``y'_sum' if e(sample) & year == 2006, d
// 		estadd local avg = string(r(mean), "%8.3fc")
// 		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
// 		estadd local elast = string(-_b[PR_post] / r(mean) / p_dif_v0, "%8.2fc")
// 		estadd local pp_dif = string(p_dif_v0*100, "%8.3fc")
//		
// 	eststo reg_`y'_`j'_a: areg `y' ib(last).year#i.n2 PR PR_post, cl(gvkey)   a(year)
// 	lincom PR_post
// 	sum ``y'_sum' if e(sample) & year == 2006, d
// 		estadd local avg = string(r(mean), "%8.3fc")
// 		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
// 		estadd local elast = string(-_b[PR_post] / r(mean) / p_dif_v0, "%8.2fc")
// 		estadd local pp_dif = string(p_dif_v0*100, "%8.3fc")
//		
//	
// 	eststo reg_`y'_`j'_b: xtreg `y' ib(last).year#i.n2 PR PR_post,  vce(cl gvkey)  fe
// 	lincom PR_post
// 	sum ``y'_sum' if e(sample) & year == 2006, d
// 		estadd local avg = string(r(mean), "%8.3fc")
// 		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
// 		estadd local elast = string(-_b[PR_post]  / r(mean) / p_dif_v0, "%8.2fc")
// 		estadd local pp_dif = string(p_dif_v0*100, "%8.3fc")
//	
// 	eststo reg_`y'_`j'_d: xtreg `y' ib(last).year#i.n2 PR PR_post  if  `if2_`j''   , vce(cl gvkey)  fe
// 	lincom PR_post
// 	sum ``y'_sum' if e(sample) & year == 2006, d
// 		estadd local avg = string(r(mean), "%8.3fc")
// 		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
// 		estadd local elast = string(-_b[PR_post]  / r(mean) / p_dif_v2, "%8.2fc")
// 		estadd local pp_dif = string(p_dif_v2*100, "%8.3fc")
//
//	
// 	eststo reg_`y'_`j'_e: xtreg `y' ib(last).year#i.n2 PR PR_post if  ~inlist(n3,"324","323","337","322","327","336","331","321") & `if2_`j''  , vce(cl gvkey)  fe
// 	lincom PR_post
// 	sum ``y'_sum' if e(sample) & year == 2006, d
// 		estadd local avg = string(r(mean), "%8.3fc")
// 		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
// 		estadd local elast = string(-_b[PR_post]  / r(mean) / p_dif_v3, "%8.2fc")
// 		estadd local pp_dif = string(p_dif_v3*100, "%8.3fc")
//
// esttab reg_`y'_`j'_? using "$output/Tables/eddie/table2_balpanel_nocontrol.tex", keep(_cons) stats() ///
// b(3) se par label ///
// noobs nogap nomtitle tex replace nonum nocons postfoot("") prefoot("") posthead("")
//
// esttab reg_`y'_`j'_? using "$output/Tables/eddie/table2_balpanel_nocontrol.tex", keep(PR_post) ///
// cells(b(fmt(3)) se(par) p) mlab(none) coll(none) noobs scalars("N Observations" "``y'_scalars1'" "``y'_scalars2'" "``y'_scalars4'" "``y'_scalars3'"  ) /// 
// varlabel(PR_post "\hspace{1em}Exposure to Section 936 X Post")  posth("\hline") ///
// preh("\multicolumn{7}{l}{\textbf{``y'_title'}}\\")  postfoot( ///
// "\hline Year Fixed Effects    & Y & Y & Y & Y & Y  \\ " ///
// "NAICS-by-Year Fixed Effects   &  & Y & Y & Y & Y  \\ " ///
// "`FE_`y'_word' Fixed Effects  &   &  & Y & Y & Y  \\ "  ///
// "S936 Exposed Sector &  &  & & Y  & Y  \\ " /// 
// "S936 Exposed Industry & & & &  &  Y   \\ \hline\hline " ///
// "\end{tabular} }" ) append sty(tex)
// est clear

* (1)' version with original controls, and other controls
// foreach x of var IHS_rev capxs ias nis sales {	
// 	eststo reg_`y'_`j'_`x'_z: areg `y' ib(last).year PR PR_post `x' log_tot_emp  , cl(gvkey)   a(year)
// 	lincom PR_post
// 	sum ``y'_sum' if e(sample) & year == 2006, d
// 		estadd local avg = string(r(mean), "%8.3fc")
// 		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
// 		estadd local elast = string(-_b[PR_post] / r(mean) / p_dif_v0, "%8.2fc")
// 		estadd local pp_dif = string(p_dif_v0*100, "%8.3fc")
//		
// 	eststo reg_`y'_`j'_`x'_a: areg `y' ib(last).year#i.n2 PR PR_post `x' log_tot_emp  , cl(gvkey)   a(year)
// 	lincom PR_post
// 	sum ``y'_sum' if e(sample) & year == 2006, d
// 		estadd local avg = string(r(mean), "%8.3fc")
// 		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
// 		estadd local elast = string(-_b[PR_post] / r(mean) / p_dif_v0, "%8.2fc")
// 		estadd local pp_dif = string(p_dif_v0*100, "%8.3fc")
//		
//	
// 	eststo reg_`y'_`j'_`x'_b: xtreg `y' ib(last).year#i.n2 PR PR_post `x' log_tot_emp  ,  vce(cl gvkey)  fe
// 	lincom PR_post
// 	sum ``y'_sum' if e(sample) & year == 2006, d
// 		estadd local avg = string(r(mean), "%8.3fc")
// 		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
// 		estadd local elast = string(-_b[PR_post]  / r(mean) / p_dif_v0, "%8.2fc")
// 		estadd local pp_dif = string(p_dif_v0*100, "%8.3fc")
//	
// 	eststo reg_`y'_`j'_`x'_d: xtreg `y' ib(last).year#i.n2 PR PR_post  `x' log_tot_emp  if  `if2_`j''   , vce(cl gvkey)  fe
// 	lincom PR_post
// 	sum ``y'_sum' if e(sample) & year == 2006, d
// 		estadd local avg = string(r(mean), "%8.3fc")
// 		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
// 		estadd local elast = string(-_b[PR_post]  / r(mean) / p_dif_v2, "%8.2fc")
// 		estadd local pp_dif = string(p_dif_v2*100, "%8.3fc")
//
//	
// 	eststo reg_`y'_`j'_`x'_e: xtreg `y' ib(last).year#i.n2 PR PR_post `x' log_tot_emp   if  ~inlist(n3,"324","323","337","322","327","336","331","321") & `if2_`j''  , vce(cl gvkey)  fe
// 	lincom PR_post
// 	sum ``y'_sum' if e(sample) & year == 2006, d
// 		estadd local avg = string(r(mean), "%8.3fc")
// 		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
// 		estadd local elast = string(-_b[PR_post]  / r(mean) / p_dif_v3, "%8.2fc")
// 		estadd local pp_dif = string(p_dif_v3*100, "%8.3fc")
//
// esttab reg_`y'_`j'_`x'_? using "$output/Tables/eddie/table2_balpanel_`x'_logemp.tex", keep(_cons) stats() ///
// b(3) se par label ///
// noobs nogap nomtitle tex replace nonum nocons postfoot("") prefoot("") posthead("")
//
// esttab reg_`y'_`j'_`x'_? using "$output/Tables/eddie/table2_balpanel_`x'_logemp.tex", keep(PR_post) ///
// cells(b(fmt(3)) se(par) p) mlab(none) coll(none) noobs scalars("N Observations" "``y'_scalars1'" "``y'_scalars2'" "``y'_scalars4'" "``y'_scalars3'"  ) /// 
// varlabel(PR_post "\hspace{1em}Exposure to Section 936 X Post")  posth("\hline") ///
// preh("\multicolumn{7}{l}{\textbf{``y'_title'}}\\")  postfoot( ///
// "\hline Year Fixed Effects    & Y & Y & Y & Y & Y  \\ " ///
// "NAICS-by-Year Fixed Effects   &  & Y & Y & Y & Y  \\ " ///
// "`FE_`y'_word' Fixed Effects  &   &  & Y & Y & Y  \\ "  ///
// "S936 Exposed Sector &  &  & & Y  & Y  \\ " /// 
// "S936 Exposed Industry & & & &  &  Y   \\ \hline\hline " ///
// "\end{tabular} }" ) append sty(tex)
// }		
// est clear

* (2) version with bad controls
// foreach y of var capex_base {	
// foreach x of var capxs emps ias nis sales  {	
// 	eststo reg_`y'_`j'_`x'_z: areg `y' ib(last).year PR PR_post IHS_rev `x', cl(gvkey)   a(year)
// 	lincom PR_post
// 	sum ``y'_sum' if e(sample) & year == 2006, d
// 		estadd local avg = string(r(mean), "%8.3fc")
// 		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
// 		estadd local elast = string(-_b[PR_post] / r(mean) / p_dif_v0, "%8.2fc")
// 		estadd local pp_dif = string(p_dif_v0*100, "%8.3fc")
//		
// 	eststo reg_`y'_`j'_`x'_a: areg `y' ib(last).year#i.n2 PR PR_post IHS_rev `x', cl(gvkey)   a(year)
// 	lincom PR_post
// 	sum ``y'_sum' if e(sample) & year == 2006, d
// 		estadd local avg = string(r(mean), "%8.3fc")
// 		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
// 		estadd local elast = string(-_b[PR_post] / r(mean) / p_dif_v0, "%8.2fc")
// 		estadd local pp_dif = string(p_dif_v0*100, "%8.3fc")
//		
//	
// 	eststo reg_`y'_`j'_`x'_b: xtreg `y' ib(last).year#i.n2 PR PR_post IHS_rev `x',  vce(cl gvkey)  fe
// 	lincom PR_post
// 	sum ``y'_sum' if e(sample) & year == 2006, d
// 		estadd local avg = string(r(mean), "%8.3fc")
// 		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
// 		estadd local elast = string(-_b[PR_post]  / r(mean) / p_dif_v0, "%8.2fc")
// 		estadd local pp_dif = string(p_dif_v0*100, "%8.3fc")
//	
// 	eststo reg_`y'_`j'_`x'_d: xtreg `y' ib(last).year#i.n2 PR PR_post IHS_rev `x'  if  `if2_`j''   , vce(cl gvkey)  fe
// 	lincom PR_post
// 	sum ``y'_sum' if e(sample) & year == 2006, d
// 		estadd local avg = string(r(mean), "%8.3fc")
// 		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
// 		estadd local elast = string(-_b[PR_post]  / r(mean) / p_dif_v2, "%8.2fc")
// 		estadd local pp_dif = string(p_dif_v2*100, "%8.3fc")
//
//	
// 	eststo reg_`y'_`j'_`x'_e: xtreg `y' ib(last).year#i.n2 PR PR_post IHS_rev `x'  if  ~inlist(n3,"324","323","337","322","327","336","331","321") & `if2_`j''  , vce(cl gvkey)  fe
// 	lincom PR_post
// 	sum ``y'_sum' if e(sample) & year == 2006, d
// 		estadd local avg = string(r(mean), "%8.3fc")
// 		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
// 		estadd local elast = string(-_b[PR_post]  / r(mean) / p_dif_v3, "%8.2fc")
// 		estadd local pp_dif = string(p_dif_v3*100, "%8.3fc")
//
// esttab reg_`y'_`j'_`x'_? using "$output/Tables/eddie/table2_balpanel_`x'.tex", keep(_cons) stats() ///
// b(3) se par label ///
// noobs nogap nomtitle tex replace nonum nocons postfoot("") prefoot("") posthead("")
//
// esttab reg_`y'_`j'_`x'_? using "$output/Tables/eddie/table2_balpanel_`x'.tex", keep(PR_post) ///
// cells(b(fmt(3)) se(par) p) mlab(none) coll(none) noobs scalars("N Observations" "``y'_scalars1'" "``y'_scalars2'" "``y'_scalars4'" "``y'_scalars3'"  ) /// 
// varlabel(PR_post "\hspace{1em}Exposure to Section 936 X Post")  posth("\hline") ///
// preh("\multicolumn{7}{l}{\textbf{``y'_title'}}\\")  postfoot( ///
// "\hline Year Fixed Effects    & Y & Y & Y & Y & Y  \\ " ///
// "NAICS-by-Year Fixed Effects   &  & Y & Y & Y & Y  \\ " ///
// "`FE_`y'_word' Fixed Effects  &   &  & Y & Y & Y  \\ "  ///
// "S936 Exposed Sector &  &  & & Y  & Y  \\ " /// 
// "S936 Exposed Industry & & & &  &  Y   \\ \hline\hline " ///
// "\end{tabular} }" ) append sty(tex)
// }		
// est clear
// }


* (3) version with one of new controls (years, bins)
// local y capex_base
// foreach x of var *_94_bin5 *_94_bin10 {
//     est clear
// 	eststo reg_`y'_`j'_z: areg `y' ib(last).year PR PR_post `x', cl(gvkey)   a(year)
// 	lincom PR_post
// 	sum ``y'_sum' if e(sample) & year == 2006, d
// 		estadd local avg = string(r(mean), "%8.3fc")
// 		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
// 		estadd local elast = string(-_b[PR_post] / r(mean) / p_dif_v0, "%8.2fc")
// 		estadd local pp_dif = string(p_dif_v0*100, "%8.3fc")
//		
// 	eststo reg_`y'_`j'_a: areg `y' ib(last).year#i.n2 PR PR_post `x', cl(gvkey)   a(year)
// 	lincom PR_post
// 	sum ``y'_sum' if e(sample) & year == 2006, d
// 		estadd local avg = string(r(mean), "%8.3fc")
// 		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
// 		estadd local elast = string(-_b[PR_post] / r(mean) / p_dif_v0, "%8.2fc")
// 		estadd local pp_dif = string(p_dif_v0*100, "%8.3fc")
//		
//	
// 	eststo reg_`y'_`j'_b: xtreg `y' ib(last).year#i.n2 PR PR_post `x',  vce(cl gvkey)  fe
// 	lincom PR_post
// 	sum ``y'_sum' if e(sample) & year == 2006, d
// 		estadd local avg = string(r(mean), "%8.3fc")
// 		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
// 		estadd local elast = string(-_b[PR_post]  / r(mean) / p_dif_v0, "%8.2fc")
// 		estadd local pp_dif = string(p_dif_v0*100, "%8.3fc")
//	
// 	eststo reg_`y'_`j'_d: xtreg `y' ib(last).year#i.n2 PR PR_post `x'  if  `if2_`j''   , vce(cl gvkey)  fe
// 	lincom PR_post
// 	sum ``y'_sum' if e(sample) & year == 2006, d
// 		estadd local avg = string(r(mean), "%8.3fc")
// 		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
// 		estadd local elast = string(-_b[PR_post]  / r(mean) / p_dif_v2, "%8.2fc")
// 		estadd local pp_dif = string(p_dif_v2*100, "%8.3fc")
//
//	
// 	eststo reg_`y'_`j'_e: xtreg `y' ib(last).year#i.n2 PR PR_post `x'  if  ~inlist(n3,"324","323","337","322","327","336","331","321") & `if2_`j''  , vce(cl gvkey)  fe
// 	lincom PR_post
// 	sum ``y'_sum' if e(sample) & year == 2006, d
// 		estadd local avg = string(r(mean), "%8.3fc")
// 		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
// 		estadd local elast = string(-_b[PR_post]  / r(mean) / p_dif_v3, "%8.2fc")
// 		estadd local pp_dif = string(p_dif_v3*100, "%8.3fc")
//
// esttab reg_`y'_`j'_? using "$output/Tables/eddie/table2_balpanel_`x'.tex", keep(_cons) stats() ///
// b(3) se par label ///
// noobs nogap nomtitle tex replace nonum nocons postfoot("") prefoot("") posthead("")
//
// esttab reg_`y'_`j'_? using "$output/Tables/eddie/table2_balpanel_`x'.tex", keep(PR_post) ///
// cells(b(fmt(3)) se(par) p) mlab(none) coll(none) noobs scalars("N Observations" "``y'_scalars1'" "``y'_scalars2'" "``y'_scalars4'" "``y'_scalars3'"  ) /// 
// varlabel(PR_post "\hspace{1em}Exposure to Section 936 X Post")  posth("\hline") ///
// preh("\multicolumn{7}{l}{\textbf{``y'_title'}}\\")  postfoot( ///
// "\hline Year Fixed Effects    & Y & Y & Y & Y & Y  \\ " ///
// "NAICS-by-Year Fixed Effects   &  & Y & Y & Y & Y  \\ " ///
// "`FE_`y'_word' Fixed Effects  &   &  & Y & Y & Y  \\ "  ///
// "S936 Exposed Sector &  &  & & Y  & Y  \\ " /// 
// "S936 Exposed Industry & & & &  &  Y   \\ \hline\hline " ///
// "\end{tabular} }" ) append sty(tex)
// }		
// est clear

* (4) interact naics with size
// local y capex_base
// foreach x of var capxs emps ias nis sales  {	
// 	est clear
// 	tempvar temp1
// 	local new_var "`x'_n2"
// 	gen `new_var' = n2 * `x'
// 	eststo reg_`y'_`j'_`x'_z: areg `y' ib(last).year PR PR_post `x' `new_var', cl(gvkey)   a(year)
// 	lincom PR_post
// 	sum ``y'_sum' if e(sample) & year == 2006, d
// 		estadd local avg = string(r(mean), "%8.3fc")
// 		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
// 		estadd local elast = string(-_b[PR_post] / r(mean) / p_dif_v0, "%8.2fc")
// 		estadd local pp_dif = string(p_dif_v0*100, "%8.3fc")
//		
// 	eststo reg_`y'_`j'_`x'_a: areg `y' ib(last).year#i.n2 PR PR_post `x' `new_var', cl(gvkey)   a(year)
// 	lincom PR_post
// 	sum ``y'_sum' if e(sample) & year == 2006, d
// 		estadd local avg = string(r(mean), "%8.3fc")
// 		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
// 		estadd local elast = string(-_b[PR_post] / r(mean) / p_dif_v0, "%8.2fc")
// 		estadd local pp_dif = string(p_dif_v0*100, "%8.3fc")
//		
//	
// 	eststo reg_`y'_`j'_`x'_b: xtreg `y' ib(last).year#i.n2 PR PR_post `x' `new_var',  vce(cl gvkey)  fe
// 	lincom PR_post
// 	sum ``y'_sum' if e(sample) & year == 2006, d
// 		estadd local avg = string(r(mean), "%8.3fc")
// 		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
// 		estadd local elast = string(-_b[PR_post]  / r(mean) / p_dif_v0, "%8.2fc")
// 		estadd local pp_dif = string(p_dif_v0*100, "%8.3fc")
//	
// 	eststo reg_`y'_`j'_`x'_d: xtreg `y' ib(last).year#i.n2 PR PR_post `x' `new_var' if  `if2_`j''   , vce(cl gvkey)  fe
// 	lincom PR_post
// 	sum ``y'_sum' if e(sample) & year == 2006, d
// 		estadd local avg = string(r(mean), "%8.3fc")
// 		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
// 		estadd local elast = string(-_b[PR_post]  / r(mean) / p_dif_v2, "%8.2fc")
// 		estadd local pp_dif = string(p_dif_v2*100, "%8.3fc")
//
//	
// 	eststo reg_`y'_`j'_`x'_e: xtreg `y' ib(last).year#i.n2 PR PR_post `x' `new_var' if  ~inlist(n3,"324","323","337","322","327","336","331","321") & `if2_`j''  , vce(cl gvkey)  fe
// 	lincom PR_post
// 	sum ``y'_sum' if e(sample) & year == 2006, d
// 		estadd local avg = string(r(mean), "%8.3fc")
// 		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
// 		estadd local elast = string(-_b[PR_post]  / r(mean) / p_dif_v3, "%8.2fc")
// 		estadd local pp_dif = string(p_dif_v3*100, "%8.3fc")
//
// esttab reg_`y'_`j'_`x'_? using "$output/Tables/eddie/table2_balpanel_n2_`x'.tex", keep(_cons) stats() ///
// b(3) se par label ///
// noobs nogap nomtitle tex replace nonum nocons postfoot("") prefoot("") posthead("")
//
// esttab reg_`y'_`j'_`x'_? using "$output/Tables/eddie/table2_balpanel_n2_`x'.tex", keep(PR_post) ///
// cells(b(fmt(3)) se(par) p) mlab(none) coll(none) noobs scalars("N Observations" "``y'_scalars1'" "``y'_scalars2'" "``y'_scalars4'" "``y'_scalars3'"  ) /// 
// varlabel(PR_post "\hspace{1em}Exposure to Section 936 X Post")  posth("\hline") ///
// preh("\multicolumn{7}{l}{\textbf{``y'_title'}}\\")  postfoot( ///
// "\hline Year Fixed Effects    & Y & Y & Y & Y & Y  \\ " ///
// "NAICS-by-Year Fixed Effects   &  & Y & Y & Y & Y  \\ " ///
// "`FE_`y'_word' Fixed Effects  &   &  & Y & Y & Y  \\ "  ///
// "S936 Exposed Sector &  &  & & Y  & Y  \\ " /// 
// "S936 Exposed Industry & & & &  &  Y   \\ \hline\hline " ///
// "\end{tabular} }" ) append sty(tex)
//
// }		
// est clear


* (5) 2000 Rev/Emp Controls
* better controls
*** Fix variables in 2000 at firm level, interacting with year
foreach x of var capxs capex emps ias nis revts sales {
	di `x'
	tempvar temp1
	local new_var "`x'_94"
	di "`new_var'"
	gen `temp1' = `x' if year==1994 
	summ `temp1'
	egen `new_var' = mean(`temp1'), by(gvkey)
}

*** Fix variables as 1990-1994 mean at firm level, interacting with year
foreach x of var capxs capex emps ias nis revts sales {
	di `x'
	tempvar temp1 temp2
	local new_var "`x'_9094mean"
	di "`new_var'"
	gen `temp1' = `x' if year >= 1991 & year <= 1994
	summ `temp1'
	egen `new_var' = mean(`temp1'), by(gvkey)
}

*** Quintiles, decile version of controls
foreach x of var capxs_* capex_* emps_* ias_* nis_* revts_* sales_* {
	di `x'

	forvalues i=5(5)10 {
		tempvar temp1
		local new_var "`x'_bin`i'"
		di "`new_var'"
		
		xtile `new_var' = `x', n(`i')
		summ `new_var'
	}
}

