// This file recreates Table 4, foreign share investment
// Author: Dan Garrett
// Date: 4-24-2020

/* Data sources:
NETS and Compustat
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data/table4_data_KR", clear

* formatting, labeling, and descriptive scalar creation
local FE_fs_capx "n3"
local FE_capex_base "gvkey"
local FE_capex_base_p "gvkey"
local FE_capex_base_1 "gvkey"
local FE_ftax "gvkey"

local FE_fs_capx_word "Industry"
local FE_capex_base_word "Firm"
local FE_capex_base_p_word "Firm"
local FE_capex_base_1_word "Firm"
local FE_ftax_word "Firm"

local fs_capx_sum "fs_capx"
local capex_base_sum "IK"
local capex_base_p_sum "IKIK"
local capex_base_1_sum "IK_1"
local ftax_sum "ftax"

local fs_capx_scalars1 ""
local capex_base_scalars1 "avg Sample Average I/K in 2006" 
local capex_base_p_scalars1 "avg Sample Average I/K in 2006 Relative to 1995"
local capex_base_1_scalars1 "avg Sample Average I/K in 2006"
local ftax_scalars1 ""

local fs_capx_scalars2 ""
local capex_base_scalars2 "elas Percent of 2006 Average"
local capex_base_p_scalars2 "elas Percent of 2006 Average"
local capex_base_1_scalars2 "elas Percent of 2006 Average"
local ftax_scalars2 ""

local fs_capx_scalars3 ""
local capex_base_scalars3 "elast Semi-elasticity of Investment"
local capex_base_p_scalars3 "elast Semi-elasticity of Investment"
local capex_base_1_scalars3 "elast Semi-elasticity of Investment"
local ftax_scalars3 ""

local fs_capx_scalars4 ""
local capex_base_scalars4 "pp_dif Change in Effective Tax Rate"
local capex_base_p_scalars4 "pp_dif Change in Effective Tax Rate"
local capex_base_1_scalars4 "pp_dif Change in Effective Tax Rate"
local ftax_scalars4 ""

local fs_capx_title "Change in Foreign Share of Investment"
local capex_base_title "Change in Investment: $ \frac{I}{K_{1990-1995}} $"
local capex_base_p_title "Percent Change in Investment: $ \frac{I}{I_{1990-1995}} - 1 $"
local capex_base_1_title "Change in Investment: $ \frac{I}{K_{1990-1995}} $"
local ftax_title "Change in Federal Taxes Paid as a Percent of Pretax Income"
xtset gvkey year

* scalar values from SOI data analysis:
scalar p_dif_v0 = .0572734
scalar p_dif_v1 = .0699663
scalar p_dif_v2 = .0465333
scalar p_dif_v3 = .0601001

* major sectors:
local if2_1 = "inlist(n2,3,5,6,7,11,12)"


*********************
*** Test controls ***
*********************

* Employment stuff
sum tot_emp, detail
sum log_tot_emp, detail
tab year, sum(tot_emp)
tab year, sum(log_tot_emp)

* Revenue
sum revts, detail
sum IHS_rev, detail
tab year, sum(revts)
tab year, sum(IHS_rev)


* Still useless
gen temp1 = rev*(year==2006)
egen rev_06 = max(temp1), by(gvkey)

gen temp2 = log_tot_emp*(year==2006)
egen emp_06 = max(temp2), by(gvkey)
drop temp?

xtile emp_bins10 = emp_06, n(10)
xtile rev_bins10 = rev_06, n(10)



* Run regressions
reghdfe fs_capx PR PR_post IHS_rev log_tot_emp  ,  vce(cl gvkey)  a(ib(last).year#i.n2 n3)


reghdfe fs_capx PR PR_post  ,  vce(cl gvkey)  a(ib(last).year#i.n2 n3)
reghdfe fs_capx PR PR_post  c.rev_06##i.year  ,  vce(cl gvkey)  a(ib(last).year#i.n2 n3)  // continuous controls for 2006 level
reghdfe fs_capx PR PR_post  ,  vce(cl gvkey)  a(ib(last).year#i.n2 year#rev_bins10 n3)  // continuous controls for 2006 level



reghdfe for_capx PR PR_post IHS_rev log_tot_emp  ,  vce(cl gvkey)  a(ib(last).year#i.n2 n3)
reghdfe for_capx PR PR_post  ,  vce(cl gvkey)  a(ib(last).year#i.n2 n3 )
reghdfe for_capx PR PR_post  c.rev_06##i.year  ,  vce(cl gvkey)  a(ib(last).year#i.n2 n3)  // continuous controls for 2006 level
reghdfe for_capx PR PR_post  ,  vce(cl gvkey)  a(ib(last).year#i.n2 year#rev_bins10 n3)  // continuous controls for 2006 level


* Okay this works - smaller and noisier though
reghdfe capex_base  PR PR_post , absorb(gvkey year#n2 year#rev_bins10) cl(gvkey) // discrete controls for 2006 level
reghdfe capex_base  PR PR_post , absorb(gvkey year#n2 year#emp_bins10 year#rev_bins10) cl(gvkey) // discrete controls for 2006 level

tab emp_bins10
tab rev_bins10

sum emp_06, detail
sum rev_06, detail


foreach y of var fs_capx {	
foreach j of num 1 {	
	eststo reg_`y'_`j'_z: reg `y' ib(last).year PR PR_post IHS_rev log_tot_emp  , cl(gvkey)  
	lincom PR_post
	sum ``y'_sum' if e(sample) & year == 2006, d
		estadd local avg = string(r(mean), "%8.3fc")
		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
		estadd local elast = string(-_b[PR_post] / r(mean) / p_dif_v0, "%8.2fc")
		estadd local pp_dif = string(p_dif_v0*100, "%8.3fc")
		
	eststo reg_`y'_`j'_a: reg `y' ib(last).year#i.n2 PR PR_post IHS_rev log_tot_emp  , cl(gvkey)  
	lincom PR_post
	sum ``y'_sum' if e(sample) & year == 2006, d
		estadd local avg = string(r(mean), "%8.3fc")
		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
		estadd local elast = string(-_b[PR_post] / r(mean) / p_dif_v0, "%8.2fc")
		estadd local pp_dif = string(p_dif_v0*100, "%8.3fc")
		
	
	eststo reg_`y'_`j'_b: reghdfe `y'  PR PR_post IHS_rev log_tot_emp  ,  vce(cl gvkey)  a(ib(last).year#i.n2 `FE_`y'')
	lincom PR_post
	sum ``y'_sum' if e(sample) & year == 2006, d
		estadd local avg = string(r(mean), "%8.3fc")
		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
		estadd local elast = string(-_b[PR_post]  / r(mean) / p_dif_v0, "%8.2fc")
		estadd local pp_dif = string(p_dif_v0*100, "%8.3fc")
	
	eststo reg_`y'_`j'_d: reghdfe `y' PR PR_post  IHS_rev log_tot_emp  if  `if2_`j''   , vce(cl gvkey) a(ib(last).year#i.n2 `FE_`y'')
	lincom PR_post
	sum ``y'_sum' if e(sample) & year == 2006, d
		estadd local avg = string(r(mean), "%8.3fc")
		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
		estadd local elast = string(-_b[PR_post]  / r(mean) / p_dif_v2, "%8.2fc")
		estadd local pp_dif = string(p_dif_v2*100, "%8.3fc")

	
	eststo reg_`y'_`j'_e: reghdfe `y' PR PR_post  IHS_rev log_tot_emp  if   ~inlist(n3,"324","323","337","322","327","336","331","321") & `if2_`j''   , vce(cl gvkey) a(ib(last).year#i.n2 `FE_`y'')
	lincom PR_post
	sum ``y'_sum' if e(sample) & year == 2006, d
		estadd local avg = string(r(mean), "%8.3fc")
		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
		estadd local elast = string(-_b[PR_post]  / r(mean) / p_dif_v3, "%8.2fc")
		estadd local pp_dif = string(p_dif_v3*100, "%8.3fc")
	
	eststo reg_`y'_`j'_f: reghdfe  `y' PR PR_post  IHS_rev log_tot_emp [aw=DFL]  if  ~inlist(n3,"324","323","337","322","327","336","331","321") & `if2_`j''  , vce(cl gvkey) a(ib(last).year#i.n2 `FE_`y'')
	lincom PR_post
	sum ``y'_sum'  if e(sample) & year == 2006, d
		estadd local avg = string(r(mean), "%8.3fc")
		estadd local elas = string(-_b[PR_post] * 100 / r(mean), "%8.1fc") + "\%"
		estadd local elast = string(-_b[PR_post]  / r(mean) / p_dif_v3, "%8.2fc")
		estadd local pp_dif = string(p_dif_v3*100, "%8.3fc")

}

esttab reg_`y'_1_? using "$output/Tables/table4.tex", keep(_cons) stats() ///
b(3) se par label ///
noobs nogap nomtitle tex replace nonum nocons postfoot("") prefoot("") posthead("")

esttab reg_`y'_1_? using "$output/Tables/table4.tex", keep(PR_post) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) noobs scalars("N Observations" "``y'_scalars1'" "``y'_scalars2'" "``y'_scalars4'" "``y'_scalars3'"  ) /// 
varlabel(PR_post "\hspace{1em}Exposure to Section 936 X Post")  posth("\hline") ///
preh("\multicolumn{7}{l}{\textbf{``y'_title'}}\\")  postfoot( ///
"\hline Year Fixed Effects   & Y  & Y & Y & Y & Y & Y \\ " ///
"NAICS-by-Year Fixed Effects   &  & Y & Y & Y & Y & Y \\ " ///
"`FE_`y'_word' Fixed Effects  &   &  & Y & Y & Y & Y \\ "  ///
"S936 Exposed Sector &  &  & & Y  & Y & Y \\ " /// 
"S936 Exposed Industry & & & &  &  Y & Y  \\ " ///
"DFL Weights & & & & & & Y \\  \hline\hline" ///
"\end{tabular} }" ) append sty(tex)
}		
