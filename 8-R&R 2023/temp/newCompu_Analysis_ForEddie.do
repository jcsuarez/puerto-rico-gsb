// This file creates new versions of Figure 5
// Author: Kevin Roberts


clear all
set more off
snapshot erase _all 
*** *** *** *** *** *** *** *** *** *** ***  *** *** *** ***
*** *** *** Section (1): Notes on subsamples *** *** *** ***
*** *** *** *** *** *** *** *** *** *** ***  *** *** *** ***

// Dropping non-PR domestic firms
use "$data/Replication Data/newCompu_Clean_0923", replace
drop if PR==0 & idbflag=="D"


// Dropping PR firms, re-defining treatment as "exposed" industry firms 
use "$data/Replication Data/newCompu_Clean_0923", replace
gen exp_PR = (PR==0)*exp_ind
xi i.year|exp_PR, noomit
drop _IyeaXexp_1995 
drop if PR==1
	
// New DFL Code
* NOTE: I think this is important to do post-sample trimming to get something meaningful from the bins
xtile ppe95_binDFL = ppent if year == 1995 , n(10) 
logit PR i.n2##i.ppe95_binDFL if year == 1995
capture: drop phat min_phat w w_phat
predict phat, pr 
winsor phat, p(.01) g(w_phat) 
replace phat = w_phat
bys gvkey: egen min_phat = min(phat) 
* ATE
* gen DFL = (PR/min_phat+(1-PR)/(1-min_phat))
* ATOT
gen DFL2 = (PR+(1-PR)*min_phat/(1-min_phat))
	
// Drop smaller PR firms 
xtile PR_sizeDist = ppe95 if PR==1, n(3) 
xtile PR_sizeDist2 = ppe95 if PR==1, n(4)
xtile PR_sizeDist3 = ppe95 if PR==1, n(5)
tab PR if year==1995, sum(ppe95)
tab PR_sizeDist if year==1995, sum(ppe95)
drop if PR_sizeDist3<4 //This version drops bottom 60%, but here's where we can play with the cutoff


	

*** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
*** *** *** Section (2): Drop control Domestic  *** *** ***
*** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
clear all
set more off
snapshot erase _all

use "$data/Replication Data/newCompu_Clean_0923", replace
gsort gvkey year
xi i.year|PR, noomit
drop _IyeaXPR_1995

gen n1 = floor(n3/100)
tab idbflag PR if year==1995
tab fic
drop if PR==0 & idbflag=="D"

* create 1995 bins
xtset gvkey year
xtile ppe95_binDFL = ppent if year == 1995 , n(10) 
xfill ppe95_binDFL, i(gvkey)

* some DFL weights
logit PR i.n2##i.ppe95_binDFL if year == 1995
capture: drop phat min_phat w w_phat
predict phat, pr 
winsor phat, p(.01) g(w_phat) 
replace phat = w_phat
xfill phat, i(gvkey)
* ATE
* gen DFL = (PR/phat+(1-PR)/(1-phat))
* ATOT
gen DFL2 = (PR+(1-PR)*phat/(1-phat))

* propensity score
gen double w_att = cond(PR==1,1,phat/(1-phat))
gen double w_ate = cond(PR==1,1/phat,1/(1-phat))

* psm
psmatch2 PR i.n2##i.ppe95_binDFL if year==1995, logit // 292 out of 732 are matched; where 176 is untreated / 116 is treated
xfill _pscore _treated _weight, i(gvkey)

* cem
cem n2 ppe95_binDFL if year==1995, treatment(PR) // 732 out of 732 matched; 375 untreated / 357 treated
xfill cem_matched cem_weights, i(gvkey)

* ebalance : `drop if PR_sizeDist3 < 4' has to be done after constructing weights
ebalance PR i.n1##i.ppe95_binDFL if year==1995, targets(1) // algorithm fails to adjust the mean (1st order moment) of 2.n2; works fine if we don't cut by size distribution
xfill _webal, i(gvkey)
// also works if ebal PR i.n1 i.ppe95_binDFL separately (non-interacted)

* size cutoff
xtile PR_sizeDist = ppe95 if PR==1, n(3)
xtile PR_sizeDist10 = ppe95 if PR==1, n(10)
tab PR if year==1995, sum(ppe95)
tab PR_sizeDist if year==1995, sum(ppe95)
drop if PR_sizeDist10<7

drop ppe95_bins10
xtile ppe95_bins10 = ppe95, n(10)

gen temp1 = (year==1991)
gen temp2 = (year==2006)
egen pre_bal = max(temp1), by(gvkey)
egen post_bal = max(temp2), by(gvkey)
drop temp?
tab year PR if pre_bal



xtset gvkey year 

* spec1~spec5 requires snapshot 3; spec6 requires snapshot 4; spec7 requires snapshot 5
global spec1 ", cl(gvkey) a(gvkey ppe95_bins10##year)"
global spec2 " [aw=DFL2], cl(gvkey) a(gvkey ppe95_bins10##year)"
global spec3 " [pw=w_att], cl(gvkey) a(gvkey ppe95_bins10##year)"
global spec4 " [pw=w_ate], cl(gvkey) a(gvkey ppe95_bins10##year)"
global spec5 " [fw=_weight], cl(gvkey) a(gvkey ppe95_bins10##year)"
global spec6 " [aw=cem_weights], cl(gvkey) a(gvkey ppe95_bins10##year)"
global spec7 " [aw=_webal], cl(gvkey) a(gvkey ppe95_bins10##year)"

global specname1 "95 PPE: Baseline"
global specname2 "95 PPE: Baseline + DFL"
global specname3 "95 PPE: Baseline + ATT IPW"
global specname4 "95 PPE: Baseline + ATE IPW"
global specname5 "95 PPE: Baseline + PS matching"
global specname6 "95 PPE: Baseline + CEM"
global specname7 "95 PPE: Baseline + Entropy Balance"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"
global c7 "khaki"




*** Store estimates ***
local cnt = 0
foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 spec7 {
	preserve 
	local cnt = `cnt' + 1
	reghdfe capex_base _IyeaX* PR $`spec'
	estimates save `spec'_base, replace
		
		estimates use `spec'_base

		** figure years for the graph
		gen fig_year = 1991 in 1
		foreach i of numlist 1/16 {
			local j = `i' + 1990
			replace fig_year = `j' in `i'
		}
		
		** annual betas for the preperiod
		gen pr_beta = 0
		gen pr_beta_lb = 0
		gen pr_beta_ub = 0
		foreach i of numlist 1/4 {
			local j = `i' + 1990
			qui lincom _b[_IyeaXPR_`j'] , level(95)
			replace pr_beta = r(estimate) in `i'
			replace pr_beta_lb = r(lb) in `i'
			replace pr_beta_ub = r(ub) in `i'
		}
		* zero effect in 1995
		qui lincom  0
		replace pr_beta = r(estimate) in 5

		** annual betas for the postperiod
		foreach i of numlist 6/16 {
			local j = `i' + 1990
			qui lincom _b[_IyeaXPR_`j'] , level(95)
			replace pr_beta = r(estimate) in `i'
			replace pr_beta_lb = r(lb) in `i'
			replace pr_beta_ub = r(ub) in `i'
		}
		
		gen specname = "`spec'"
		keep if _n < 17
		keep fig_year pr_beta* specname
		tempfile `spec'_graph
		save ``spec'_graph', replace	
	restore
}



*** Graph Overlay ***
{
di `cnt'
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/`cnt' {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec5", lco($c5) mco($c5) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec5", lco($c5) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec6", lco($c6) mco($c6) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec6", lco($c6) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec7", lco($c7) mco($c7) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec7", lco($c7) lpa(solid)) ///
	 , yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(4) on order (1 3 5 7 9 11 13 ) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3") ///
		label(7 "$specname4")  ///
		label(9 "$specname5")  ///
		label(11 "$specname6") ///
		label(13 "$specname7")  ) ///
		yti("Effect of S936 Exposure on Investment", margin(medium))
	
	graph export "$output/Graphs/eddie/newCompu_WeightBalance.pdf", replace 
restore	
}




* aipw estimator
preserve
	est clear
	eststo: teffects aipw (capex_base _IyeaXPR_1991 ) (PR ppe95_binDFL), pstol(1e-25) pomeans aequations
	eststo: teffects ipw (capex_base)  (PR _IyeaX* n2 ppe95_binDFL)
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		lincom _b[0.PR] - _b[1.PR] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* outputting the graph
	graph twoway (scatter pr_beta fig_year, lcolor(navy) mcolor(navy) ///
	yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
		(rcap pr_beta_lb pr_beta_ub fig_year, lcolor(navy) lpattern(solid)) ///
		, plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		title("Teffects AIPW'") xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(position(6) on order (1 2) label(1 "Puerto Rico Presence")  ///
		label(2 "90% Confidence Interval") r(1)) ///
		yti("Effect of S936 on ``y'_name'", margin(medium))
		
restore
	
	

// global spec1 "sdid capex_base gvkey year _IyeaX* PR "
global spec1 "teffects ipw (capex_base) (PR i.ppe95_bins10##i.year)"
global spec2 "teffects ipwra (capex_base _IyeaX* i.ppe95_bins10##i.year) (PR _IyeaX* i.ppe95_bins10##i.year) "
// global spec2 "[aw=DFL2], cl(gvkey) a(gvkey ppe95_bins10##year)"
// global spec3 ", cl(gvkey) a(gvkey n2##year ppe95_bins10##year)"
// global spec4 "[aw=DFL2], cl(gvkey) a(gvkey n2##year ppe95_bins10##year)"



global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"

foreach spec in spec1 spec2 spec3 spec4 {
	reghdfe lninv _IyeaX* PR $`spec'
}

*** Store estimates ***
local cnt = 0
foreach spec in spec1 spec2 spec3 spec4 {
local cnt = `cnt' + 1
reghdfe capex_base _IyeaX* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/`cnt' {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 , yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7 ) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3") ///
		label(7 "$specname4")  ) ///
		yti("Effect of S936 Exposure on Investment", margin(medium))
restore	
}

*graph export "$output/Graphs/kevin/TEST.pdf", replace 	
				
		

/* make balanced panel for sdid */
gsort gvkey year
local balance = 0 // 0 if FALSE, 1 if TRUE
if `balance' == 1 {
	bys gvkey: gen firm_survival = _N
	keep if firm_survival == 16 // 46,960 left out of 115,224
	drop firm_survival
}
