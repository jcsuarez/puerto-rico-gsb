// This file creates new versions of Figure 5
// Author: Kevin Roberts


clear all
set more off
snapshot erase _all 
	
	
*ssc install sdid, replace
*ssc install winsor, replace
*ssc install ivreghdfe, replace	

*** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
*** *** *** Section (1): New main specification *** *** ***
*** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
use "$data/Replication Data/newCompu_Clean_0923", replace
drop if PR==0 & idbflag=="D"


* some DFL weights first, then drop (for entropy balancing)
xtile ppe95_binDFL = ppent if year == 1995 , n(15) 
logit PR i.n2##i.ppe95_binDFL if year == 1995
capture: drop phat min_phat w w_phat
predict phat, pr 
*winsor phat, p(.01) g(w_phat) 
*replace phat = w_phat
bys gvkey: egen min_phat = min(phat) 
* ATOT
gen DFL2 = (PR+(1-PR)*min_phat/(1-min_phat))

* ebalance weights
gen n1 = floor(n3/100)
ebalance PR i.n1##i.ppe95_binDFL if year==1995 // algorithm fails to adjust the mean (1st order moment) of 2.n2; works fine if we don't cut by size distribution
// also works if ebal PR i.n1 i.ppe95_binDFL separately (non-interacted)
xfill _webal, i(gvkey)


xtile PR_sizeDist = ppe95 if PR==1, n(3)
xtile PR_sizeDist10 = ppe95 if PR==1, n(10)

tab PR if year==1995, sum(ppe95)
tab PR_sizeDist if year==1995, sum(ppe95)
drop if PR_sizeDist10<7

drop ppe95_bins10
xtile ppe95_bins10 = ppe95, n(10)

global spec1 ", cl(gvkey) a(PR ppe95_bins10##year)"
global spec2 ", cl(gvkey) a(PR n2##ppe95_bins10##year)"
global spec3 "if exp_sec, cl(gvkey) a(PR n2##ppe95_bins10##year)"
global spec4 "if exp_ind, cl(gvkey) a(PR n2##ppe95_bins10##year)"
global spec5 "[aw=_webal], cl(gvkey) a(PR n2##ppe95_bins10##year)"
global spec6 "[aw=DFL2], cl(gvkey) a(PR n2##ppe95_bins10##year)"

global specname1 "95 PPE: Baseline"
global specname2 "95 PPE: Sector-by-Size FE"
global specname3 "95 PPE: Exposed Sectors"
global specname4 "95 PPE: Exposed Industries"
global specname5 "95 PPE: Entropy Balancing"
global specname6 "95 PPE: DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red" 

*** What we're doing ***
	* Consider full vs. "fine comparison" sample
	* Consider different outcomes: levels of taxes, IHS of taxes
	* Consider capping taxes at 0 or not
	* Consider other ratios: taxes over current period income
	* 
	

*** Federal Tax ***
gen ftax_level = ftax*pi
winsor ftax_level, p(.01) g(ftax_level_w)
replace ftax_level = ftax_level_w
 
replace ftax_level = 0 if ftax_level<0
gen ihs_ftax_level = log(ftax_level + sqrt(ftax_level^2+1))

*** Global Tax ***
gen gtax = txt/pi
winsor gtax, p(.01) g(gtax_w)

winsor txt, p(.01) g(txt_w)
replace txt = txt_w
 
replace txt = 0 if txt<0
gen ihs_txt = log(txt + sqrt(txt^2+1))




***** TABLE ****
global level "gvkey"
global spec1 ", cl(gvkey) a($level ppe95_bins10##year)"
global spec2 ", cl(gvkey) a($level n2##year ppe95_bins10##year)"
global spec3 "if exp_sec, cl(gvkey) a($level n2##year ppe95_bins10##year)"
global spec4 "if exp_ind, cl(gvkey) a($level n2##year ppe95_bins10##year)"
global spec5 "[aw=_webal], cl(gvkey) a($level n2##year ppe95_bins10##year)"
global spec6 "[aw=DFL2], cl(gvkey) a($level n2##year ppe95_bins10##year)"


global level "PR"
global spec1 ", cl(gvkey) a($level year)"
global spec2 ", cl(gvkey) a($level n2##year year)"
global spec3 "if exp_sec, cl(gvkey) a($level n2##year year)"
global spec4 "if exp_ind, cl(gvkey) a($level n2##year year)"
global spec5 "[aw=_webal], cl(gvkey) a($level n2##year year)"
global spec6 "[aw=DFL2], cl(gvkey) a($level n2##year year)"



est clear
preserve
	keep if pi>4
	local outcome "ftax"
	gen `outcome'_NORM = `outcome'*100
	*gen `outcome'_NORM = `outcome'
	local cnt = 0
	foreach spec in spec1 spec2 spec3 spec4 spec5 spec6  {
		*eststo LD_`spec': reghdfe `outcome'_NORM _IyeaX* $`spec'		
		eststo DD_`spec': reghdfe `outcome'_NORM PR_post $`spec'		
	}		
			
	*esttab LD_*, keep(_IyeaXPR_2006) mlab(none) coll(none) cells(b(fmt(3)) se(par) p) 
	esttab DD_*, keep(PR_post) mlab(none) coll(none) ///
		cells(b(fmt(3)) se(par) p) ///
		varlabel( PR_post "Exposure to Section 936 X Post") 
		
	sum `outcome', detail	
restore	
	





global level "gvkey"
global spec1 ", cl(gvkey) a($level ppe95_bins10##year)"
global spec2 ", cl(gvkey) a($level n2##year ppe95_bins10##year)"
global spec3 "if exp_sec, cl(gvkey) a($level n2##year ppe95_bins10##year)"
global spec4 "if exp_ind, cl(gvkey) a($level n2##year ppe95_bins10##year)"
global spec5 "[aw=_webal], cl(gvkey) a($level n2##year ppe95_bins10##year)"
global spec6 "[aw=DFL2], cl(gvkey) a($level n2##year ppe95_bins10##year)"

*** Store estimates ***
keep if pi>4
local outcome "ihs_ftax_level"
local cnt = 0
foreach spec in spec1 spec2 spec3 spec4 spec5 spec6  {
local cnt = `cnt' + 1
reghdfe `outcome' _IyeaX* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}

*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/`cnt' {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec5", lco($c5) mco($c5) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec5", lco($c5) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec6", lco($c6) mco($c6) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec6", lco($c6) lpa(solid)) ///
	 , yline(0 , lcolor(gs12)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(3) on order (1 3 5 7 9 11) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3") ///
		label(7 "$specname4") ///
		label(9 "$specname5") ///
		label(11 "$specname6")  ) ///
		yti("Effect of S936 Exposure on Investment", margin(medium))
restore	
}
graph export "$output/Graphs/kevin/newCompu_ETR.pdf", replace 
	
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/`cnt' {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 , yline(0 , lcolor(gs12)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(3) on order (1 3 5) ///
		label(1 "$specname2")  ///
		label(3 "$specname3") ///
		label(5 "$specname4") ) ///
		yti("Effect of S936 Exposure on Investment", margin(medium))
restore	
}
graph export "$output/Graphs/kevin/newCompu_ETR_good.pdf", replace 	

		

		
		
		
/*		
		
* Cumulative elasticities		
sum ppe95
local ppe_base = r(mean)		
foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 {
	qui reghdfe capex_base _IyeaX* PR $`spec'
	nlcom (_b[_IyeaXPR_1996] + _b[_IyeaXPR_1997] + _b[_IyeaXPR_1998] + _b[_IyeaXPR_1999] + _b[_IyeaXPR_2000] + _b[_IyeaXPR_2001] + _b[_IyeaXPR_2002] + _b[_IyeaXPR_2003] + _b[_IyeaXPR_2004] + _b[_IyeaXPR_2005] + _b[_IyeaXPR_2006])
	*local b_i = r(b)[1]
	qui reghdfe ftax_w _IyeaX* PR $`spec'
	nlcom _b[_IyeaXPR_1996] + _b[_IyeaXPR_1997] + _b[_IyeaXPR_1998] + _b[_IyeaXPR_1999] + _b[_IyeaXPR_2000] + _b[_IyeaXPR_2001] + _b[_IyeaXPR_2002] + _b[_IyeaXPR_2003] + _b[_IyeaXPR_2004] + _b[_IyeaXPR_2005] + _b[_IyeaXPR_2006]
	*local b_ETR  = r(b)[1]
	*dis `b_i'/`b_ETR'
}	

*** One-step estimation ***

keep if year<=1995 | year>=2000
gen POST = (year>1995)

drop if exp_ind==1 & PR==0
ivreghdfe capex_base (ftax_w = PR#i.POST), cl(gvkey) a(gvkey year)
ivreghdfe capex_base (ftax_w = PR#i.year), cl(gvkey) a(gvkey n2##ib(last).year)
ivreghdfe capex_base (ftax_w = PR#i.year)  , cl(gvkey)   a(gvkey n2##year ppe95_bins10##year)



ivreghdfe capex_base (ftax = PR#i.POST) if ftax>0, cl(gvkey) a(year)
ivreghdfe capex_base (ftax = PR#i.POST) if ftax>0, cl(gvkey) a(gvkey ppe95_bins10##year)
ivreghdfe capex_base (ftax = PR#i.POST) if ftax>0, cl(gvkey)   a(gvkey n2##year ppe95_bins10##year)


*reg1
ivreghdfe capex_base (ftax = PR#i.POST), cl(gvkey) a(year)
ivreghdfe capex_base (ftax = PR#i.POST), cl(gvkey) a(gvkey ppe95_bins10##year)
ivreghdfe capex_base (ftax = PR#i.POST)  , cl(gvkey)   a(gvkey n2##year ppe95_bins10##year)



ivreghdfe capex_base (ftax = PR#i.POST)  if  exp_sec   ,  cl(gvkey) a(gvkey ppe95_bins10##year)
ivreghdfe capex_base (ftax = PR#i.POST)  if  exp_ind ,  cl(gvkey) a(gvkey ppe95_bins10##year)

ivreghdfe capex_base (ftax = PR#i.year) [aw=_webal],   cl(gvkey)   a(gvkey year)
ivreghdfe capex_base (ftax = PR#i.year) [aw=_webal],   cl(gvkey)   a(gvkey ppe95_bins10##year)
