// This file recreates Figure 8
// Author: Dan Garrett
// Date: 4-20-2020

/* Data sources:
Nets
*/

* starting point for all panels is 3-wrds_segments/DFL_nets_emp_2019_06_11_dan

clear all
set more off
snapshot erase _all 

// Define Command for ES graph
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  ti(string) yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(solid)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	title("`ti'") xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Estimate") label(2 "`level'% CI") position(6)) ///
	ytitle("`yti'") ///
	note("`note'") ylab("`ylab'")
graph export "`outname'.pdf", replace

end

*******************************************************
* employment graph at NETS firm level
*******************************************************
use "$data/Replication Data/figure8_data_EY_081523", clear


drop if base_emp ==0 
sum base_emp if year == 1995 & PR , d
gen wgt=base_emp/( r(mean)) if PR 

sum base_emp if year == 1995 & PR==0 , d
replace wgt=base_emp/( r(mean)) if PR==0 

winsor wgt, p(.01) g(w_wgt) 

** Make balanced panel by dropping firms that drop out
bys hqduns95 : egen min_emp = min(emp_US) 
drop if min_emp == 0 

set matsize 4000


*** KR: 9/1, test flexible controls ***
preserve
	*drop if year>2006

	xi i.year|PR, noomit 
	drop _IyeaXP*1995

	xtile emp_bins10 = base_emp, n(10)
	xtile emp_bins5 = base_emp, n(5)
	xtile emp_bins3 = base_emp, n(3)
	
	
	reghdfe emp_growth _IyeaXP*  ///
		   [aw=wgt] , a(i.year)  vce(cluster hqduns) noconst   
	
	
	ES_graph , level(95) ti("Original Figure 8") ///
	yti("Effect of S936 Exposure") ///
	note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
	outname("$output/Graphs/eddie/NETS_KR_comp1") ylab("-.12(.02).04")  
	
		reghdfe emp_growth _IyeaXP*  ///
		   [aw=base_emp] , a(i.year)  vce(cluster hqduns) noconst  
	
		ES_graph , level(95) ti("Figure 8: base_emp weight") ///
	yti("Effect of S936 Exposure") ///
	note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
	outname("$output/Graphs/eddie/NETS_KR_comp2") ylab("-.12(.02).04")  
	
	
	reghdfe emp_growth _IyeaXP* c.base_emp##i.year  ///
		   [aw=wgt] , a(year)  vce(cluster hqduns) noconst 

		ES_graph , level(95) ti("Figure 8: Continuous Employment Size Controls") ///
	yti("Effect of S936 Exposure") ///
	note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
	outname("$output/Graphs/eddie/NETS_KR_comp3") ylab("-.12(.02).04")  	   
	
	reghdfe emp_growth _IyeaXP*  ///
		   [aw=wgt] , a(emp_bins10##year)  vce(cluster hqduns) noconst   	   
	
		ES_graph , level(95) ti("Figure 8: Decile Employment Size Controls") ///
	yti("Effect of S936 Exposure") ///
	note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
	outname("$output/Graphs/eddie/NETS_KR_comp4") ylab("-.12(.02).04") 
	
	
	reghdfe emp_growth _IyeaXP*  ///
		   [aw=wgt] , a(emp_bins3##year)  vce(cluster hqduns) noconst   	   		   
		ES_graph , level(95) ti("Figure 8: Tercile Employment Size Controls") ///
	yti("Effect of S936 Exposure") ///
	note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
	outname("$output/Graphs/eddie/NETS_KR_comp5") ylab("-.12(.02).04") 		   
		   
		   
	reghdfe emp_growth _IyeaXP*  ///
		   [aw=wgt] , a(emp_bins5##year)  vce(cluster hqduns) noconst   	   		   
		ES_graph , level(95) ti("Figure 8: Quintile Employment Size Controls") ///
	yti("Effect of S936 Exposure") ///
	note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
	outname("$output/Graphs/eddie/NETS_KR_comp6") ylab("-.12(.02).04") 		   
		   		   
		   
restore


* DFL 
gen naic2 = floor(m_nai/10)
gen pharma = (m_naic3 == 325)
capture: drop q_wgt

* (0) replicate figure 8
snapshot save

	xtile q_wgt = base_emp if year == 1995, n(20) 
	logit PR i.q_wgt#i.naic2

	capture: drop phat min_phat w w_w w_wgt
	predict phat, pr 
	bys hqduns: egen min_phat = min(phat) 
	* ATE
	* gen w = wgt*(PR/min_phat+(1-PR)/(1-min_phat))
	* ATOT
	gen w = w_wgt*(PR+(1-PR)*min_phat/(1-min_phat))

	* keeping variables and saving 
	keep emp_growth year PR wgt w hqd

	xi i.year|PR, noomit 
	drop _IyeaXP*1995

	* employment
	est clear
	eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
	   [aw=wgt] , a(i.year)  vce(cluster hqduns) noconst      
	  estadd local hasy "Yes"    
	  
	  

	ES_graph , level(95) ti("Original Figure 8 aw=wgt") yti("Effect of S936 Exposure") ///
	note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
	outname("$output/Graphs/eddie/NETS_firm_DFL_06_11_2019_r0_wgt") ylab("-.12(.02).04")  

	
* (1) replicate figure 8; but aw=wgt is wrong, it should be aw=w
snapshot restore 1

	xtile q_wgt = base_emp if year == 1995, n(20) 
	logit PR i.q_wgt#i.naic2

	capture: drop phat min_phat w w_w w_wgt
	predict phat, pr 
	bys hqduns: egen min_phat = min(phat) 
	* ATE
	* gen w = wgt*(PR/min_phat+(1-PR)/(1-min_phat))
	* ATOT
	gen w = w_wgt*(PR+(1-PR)*min_phat/(1-min_phat))

	* keeping variables and saving 
	keep emp_growth year PR wgt w hqd

	xi i.year|PR, noomit 
	drop _IyeaXP*1995

	* employment
	est clear
	eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
	   [aw=w] , a(i.year)  vce(cluster hqduns) noconst      
	  estadd local hasy "Yes"    

	ES_graph , level(95) ti("Original Figure 8 aw=w") yti("Effect of S936 Exposure") ///
	note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
	outname("$output/Graphs/eddie/NETS_firm_DFL_06_11_2019_r0_w") ylab("-.12(.02).04")  

* (2) aw=base_emp
snapshot restore 1

	local output_suffix "base_emp"
	xtile q_wgt = base_emp if year == 1995, n(20) 
	logit PR i.q_wgt#i.naic2

	capture: drop phat min_phat w w_w w_wgt
	predict phat, pr 
	bys hqduns: egen min_phat = min(phat) 
	* ATE
	* gen w = wgt*(PR/min_phat+(1-PR)/(1-min_phat))
	* ATOT
	gen w = w_wgt*(PR+(1-PR)*min_phat/(1-min_phat))

	* keeping variables and saving 
	keep emp_growth year PR wgt w hqd base_emp

	xi i.year|PR, noomit 
	drop _IyeaXP*1995

	* employment
	est clear
	eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
	   [aw=base_emp] , a(i.year)  vce(cluster hqduns) noconst      
	  estadd local hasy "Yes"    

	ES_graph , level(95) ti("Figure 8 aw=base_emp") yti("Effect of S936 Exposure") ///
	note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
	outname("$output/Graphs/eddie/NETS_firm_DFL_`output_suffix'") ylab("-.12(.02).04")  

* (3) Relative Size Weighting aw=w_wgt
snapshot restore 1

	local output_suffix "rel_size_wgt"
	xtile q_wgt = base_emp if year == 1995, n(20) 
	logit PR i.q_wgt#i.naic2

	capture: drop phat min_phat w w_w w_wgt
	predict phat, pr 
	bys hqduns: egen min_phat = min(phat) 
	* ATE
	* gen w = wgt*(PR/min_phat+(1-PR)/(1-min_phat))
	* ATOT
	gen w = w_wgt*(PR+(1-PR)*min_phat/(1-min_phat))

	* keeping variables and saving 
	keep emp_growth year PR wgt w hqd w_wgt

	xi i.year|PR, noomit 
	drop _IyeaXP*1995

	* employment
	est clear
	eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
	   [aw=w_wgt] , a(i.year)  vce(cluster hqduns) noconst      
	  estadd local hasy "Yes"    

	ES_graph , level(95) ti("Figure 8 Relative Size Weighting [aw=w_wgt]") ///
	yti("Effect of S936 Exposure") ///
	note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
	outname("$output/Graphs/eddie/NETS_firm_DFL_`output_suffix'") ylab("-.12(.02).04")  
	
* (4) 95 employment ventiles + NAICS2
snapshot restore 1
	
	local output_suffix "95vent_naics2"
	xtile q_wgt = base_emp if year == 1995, n(20) 
	logit PR i.q_wgt i.naic2 if year == 1995 

	capture: drop phat min_phat w w_w w_wgt
	predict phat, pr 
	bys hqduns: egen min_phat = min(phat) 

	* ATOT
	gen w = w_wgt*(PR+(1-PR)*min_phat/(1-min_phat))

	* keeping variables and saving 
	keep emp_growth year PR wgt w hqd

	xi i.year|PR, noomit 
	drop _IyeaXP*1995

	* employment
	est clear
	eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
	   [aw=w] , a(i.year)  vce(cluster hqduns) noconst      
	  estadd local hasy "Yes"    

	ES_graph , level(95) ti("95 Emp Ventile + NAICS2") yti("Effect of S936 Exposure") ///
	note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
	outname("$output/Graphs/eddie/NETS_firm_DFL_95vent_naics2") ylab("-.12(.02).04") 
	
	
* (5) 95 employment deciles + NAICS2
snapshot restore 1
	
	local output_suffix "95dec_naics2"
	xtile q_wgt = base_emp if year == 1995, n(10) // deciles
	logit PR i.q_wgt i.naic2 if year == 1995 

	capture: drop phat min_phat w w_w w_wgt
	predict phat, pr 
	bys hqduns: egen min_phat = min(phat) 

	* ATOT
	gen w = w_wgt*(PR+(1-PR)*min_phat/(1-min_phat))

	* keeping variables and saving 
	keep emp_growth year PR wgt w hqd

	xi i.year|PR, noomit 
	drop _IyeaXP*1995

	* employment
	est clear
	eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
	   [aw=w] , a(i.year)  vce(cluster hqduns) noconst      
	  estadd local hasy "Yes"    

	ES_graph , level(95) ti("95 Emp Decile + NAICS2") yti("Effect of S936 Exposure") ///
	note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
	outname("$output/Graphs/eddie/NETS_firm_DFL_`output_suffix'") ylab("-.12(.02).04")  
	
	
* (6) 95 employment deciles X NAICS2
snapshot restore 1
	
	local output_suffix "95decXnaics2"
	xtile q_wgt = base_emp if year == 1995, n(10) // deciles
	logit PR i.q_wgt##i.naic2 if year == 1995

	capture: drop phat min_phat w w_w w_wgt
	predict phat, pr 
	bys hqduns: egen min_phat = min(phat) 

	* ATOT
	gen w = w_wgt*(PR+(1-PR)*min_phat/(1-min_phat))

	* keeping variables and saving 
	keep emp_growth year PR wgt w hqd

	xi i.year|PR, noomit 
	drop _IyeaXP*1995

	* employment
	est clear
	eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
	   [aw=w] , a(i.year)  vce(cluster hqduns) noconst      
	  estadd local hasy "Yes"    

	ES_graph , level(95) ti("95 Emp Decile X NAICS2") yti("Effect of S936 Exposure") ///
	note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
	outname("$output/Graphs/eddie/NETS_firm_DFL_`output_suffix'") ylab("-.12(.02).04")  
	

* (7) 95 employment ventiles + NAICS3
snapshot restore 1
	
	local output_suffix "95vent_naics3"
	xtile q_wgt = base_emp if year == 1995, n(20) // deciles
	logit PR i.q_wgt i.m_naic3 if year == 1995 

	capture: drop phat min_phat w w_w w_wgt
	predict phat, pr 
	bys hqduns: egen min_phat = min(phat) 

	* ATOT
	gen w = w_wgt*(PR+(1-PR)*min_phat/(1-min_phat))

	* keeping variables and saving 
	keep emp_growth year PR wgt w hqd

	xi i.year|PR, noomit 
	drop _IyeaXP*1995

	* employment
	est clear
	eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
	   [aw=w] , a(i.year)  vce(cluster hqduns) noconst      
	  estadd local hasy "Yes"    

	ES_graph , level(95) ti("95 Emp Ventile + NAICS3") yti("Effect of S936 Exposure") ///
	note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
	outname("$output/Graphs/eddie/NETS_firm_DFL_`output_suffix'") ylab("-.12(.02).04")  
	
	
* (8) 95 employment ventiles X NAICS3
snapshot restore 1
	
	local output_suffix "95ventXnaics3"
	xtile q_wgt = base_emp if year == 1995, n(20) // deciles
	logit PR i.q_wgt##i.m_naic3 if year == 1995

	capture: drop phat min_phat w w_w w_wgt
	predict phat, pr 
	bys hqduns: egen min_phat = min(phat) 

	* ATOT
	gen w = w_wgt*(PR+(1-PR)*min_phat/(1-min_phat))

	* keeping variables and saving 
	keep emp_growth year PR wgt w hqd

	xi i.year|PR, noomit 
	drop _IyeaXP*1995

	* employment
	est clear
	eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
	   [aw=w] , a(i.year)  vce(cluster hqduns) noconst      
	  estadd local hasy "Yes"    

	ES_graph , level(95) ti("95 Emp Ventile X NAICS3") yti("Effect of S936 Exposure") ///
	note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
	outname("$output/Graphs/eddie/NETS_firm_DFL_`output_suffix'") ylab("-.12(.02).04")  
	
	
