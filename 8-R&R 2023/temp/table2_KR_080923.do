// This file recreates Table 2
// Author: Dan Garrett
// Date: 4-20-2020

/* Data sources:
NETS and Compustat
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data/table2_data_KR", clear
// use "$data/Replication Data/table2_data", clear

* formatting, labeling, and descriptive scalar creation
local FE_fs_capx "n3"
local FE_capex_base "gvkey"
local FE_capex_base_p "gvkey"
local FE_capex_base_1 "gvkey"
local FE_ftax "gvkey"

local FE_fs_capx_word "Industry"
local FE_capex_base_word "Firm"
local FE_capex_base_p_word "Firm"
local FE_capex_base_1_word "Firm"
local FE_ftax_word "Firm"

local fs_capx_sum "fs_capx"
local capex_base_sum "IK"
local capex_base_p_sum "IKIK"
local capex_base_1_sum "IK_1"
local ftax_sum "ftax"

local fs_capx_scalars1 ""
local capex_base_scalars1 "avg Sample Average I/K in 2006" 
local capex_base_p_scalars1 "avg Sample Average I/K in 2006 Relative to 1995"
local capex_base_1_scalars1 "avg Sample Average I/K in 2006"
local ftax_scalars1 ""

local fs_capx_scalars2 ""
local capex_base_scalars2 "elas Percent of 2006 Average"
local capex_base_p_scalars2 "elas Percent of 2006 Average"
local capex_base_1_scalars2 "elas Percent of 2006 Average"
local ftax_scalars2 ""

local fs_capx_scalars3 ""
local capex_base_scalars3 "elast Semi-elasticity of Investment"
local capex_base_p_scalars3 "elast Semi-elasticity of Investment"
local capex_base_1_scalars3 "elast Semi-elasticity of Investment"
local ftax_scalars3 ""

local fs_capx_scalars4 ""
local capex_base_scalars4 "pp_dif Change in Effective Tax Rate"
local capex_base_p_scalars4 "pp_dif Change in Effective Tax Rate"
local capex_base_1_scalars4 "pp_dif Change in Effective Tax Rate"
local ftax_scalars4 ""

local fs_capx_title "Change in Foreign Share of Investment"
local capex_base_title "Change in Investment: $ \frac{I}{K_{1990-1995}} $"
local capex_base_p_title "Percent Change in Investment: $ \frac{I}{I_{1990-1995}} - 1 $"
local capex_base_1_title "Change in Investment: $ \frac{I}{K_{1990-1995}} $"
local ftax_title "Change in Federal Taxes Paid as a Percent of Pretax Income"
xtset gvkey year

* scalar values from SOI data analysis:
scalar p_dif_v0 = .0572734
scalar p_dif_v1 = .0699663
scalar p_dif_v2 = .0465333
scalar p_dif_v3 = .0601001

* major sectors:
local if2_1 = "inlist(n2,3,5,6,7,11,12)"



*********************
*** Test controls ***
*********************
drop if gvkey==15084 // ad-hoc outlier


* Employment stuff
sum emps, detail
sum tot_emp, detail
sum log_tot_emp, detail
tab year PR, sum(emps)
tab year PR, sum(tot_emp)
tab year PR, sum(log_tot_emp)

* Revenue
sum revts, detail
sum IHS_rev, detail
tab year PR, sum(revts)
tab year PR, sum(IHS_rev)

sum revts if year==2000, detail
sum revts if year==2006, detail


* Investment rate
sum capex, detail
sum capex_base, detail
tab year PR, sum(capex)
tab year PR, sum(capex_base)

* Capxs
sum capxs, detail
tab year PR, sum(capxs)

** Data issues in 2001 seem entirely driven by single outlier
sum capxs if year==2000 & capxs<20000, detail
sum capxs if year==2001 & capxs<20000, detail
sum capxs if year==2002 & capxs<20000, detail

* Identifiable Total Assets
sum ias, detail
sum tot_ias, detail
tab year PR, sum(ias)
tab year PR, sum(tot_ias)

sum tot_ias if year==1995 & PR==0, detail
sum tot_ias if year==1995 & PR==1, detail

* Net Income --> no observation pre 1997; so can't use it
sum nis, detail
tab year PR, sum(nis)

* Sales
sum sales, detail
sum salexg, detail
tab year PR, sum(sales)
tab year PR, sum(salexg)


* better controls
*** Fix variables in 1994 at firm level, interacting with year
foreach x of var capxs capex emps ias nis revts sales {
	di `x'
	tempvar temp1
	local new_var "`x'_94"
	di "`new_var'"
	gen `temp1' = `x' if year==1994 
	summ `temp1'
	egen `new_var' = mean(`temp1'), by(gvkey)
}

*** Fix variables as 1990-1994 mean at firm level, interacting with year
foreach x of var capxs capex emps ias nis revts sales {
	di `x'
	tempvar temp1 temp2
	local new_var "`x'_9094mean"
	di "`new_var'"
	gen `temp1' = `x' if year >= 1991 & year <= 1994
	summ `temp1'
	egen `new_var' = mean(`temp1'), by(gvkey)
}

*** Quintiles, decile version of controls
foreach x of var capxs_* capex_* emps_* ias_* nis_* revts_* sales_* {
	di `x'

	forvalues i=5(5)10 {
		tempvar temp1
		local new_var "`x'_bin`i'"
		di "`new_var'"
		
		xtile `new_var' = `x', n(`i')
		summ `new_var'
	}
}


****************************************************************************
***************************** DFL Weights **********************************
****************************************************************************
*local size_var "sales"
*local size_var "ias"
local size_var "revts"
local yr=2006

gen temp1 = `size_var'*(year==`yr')
egen base_size = max(temp1), by(gvkey)
drop temp1

sum base_size if year == `yr' & PR , d
gen wgt=base_size/( r(mean)) if PR 

sum base_size if year == `yr' & PR==0 , d
replace wgt=base_size/( r(mean)) if PR==0 

winsor wgt, p(.01) g(w_wgt) 

set matsize 4000

* DFL 
capture: drop q_wgt
xtile q_wgt = base_size if year == `yr', n(10) 
logit PR i.q_wgt##i.n2 if year == `yr'

capture: drop phat min_phat w w_w w_wgt
predict phat, pr 
bys gvkey: egen min_phat = min(phat) 
* ATE
* gen w = wgt*(PR/min_phat+(1-PR)/(1-min_phat))
* ATOT
gen w = (PR+(1-PR)*min_phat/(1-min_phat))
gen w2 = w_wgt*(PR+(1-PR)*min_phat/(1-min_phat))


reghdfe capex_base  PR PR_post , absorb(year) cl(gvkey)
reghdfe capex_base  PR PR_post , absorb(year#n2) cl(gvkey)

reghdfe capex_base  PR PR_post [aw=w], absorb(year) cl(gvkey)
reghdfe capex_base  PR PR_post [aw=w], absorb(year#n2) cl(gvkey)

reghdfe capex_base  PR PR_post [aw=w2], absorb(year) cl(gvkey)
reghdfe capex_base  PR PR_post [aw=w2], absorb(year#n2) cl(gvkey)




***********************************************************************
************************ EVENT STUDY ESTIMATES ************************
***********************************************************************

* combining years and N2
egen yearg = group(year n2)

*figures 5, 6, A5, and A6
xi i.year|PR , noomit
drop _IyeaXPR_1995 

forval i=1990(1)2014 { 
	capture: label var _IyeaXPR_`i' " "
} 
forval i=1991(2)2014 { 
	capture: label var _IyeaXPR_`i' "`i'"
} 


sort gvkey year
by gvkey: gen firm_count = _n
egen samp_count = max(firm_count), by(gvkey)

reghdfe capex_base _IyeaX*  PR, absorb(year) cl(gvkey) 
reghdfe capex_base  _IyeaX* PR , absorb(year#n2) cl(gvkey) 

reghdfe capex_base _IyeaX*  PR [aw=w], absorb(year) cl(gvkey)
reghdfe capex_base _IyeaX*  PR [aw=w], absorb(year#n2) cl(gvkey)

reghdfe capex_base _IyeaX*  PR [aw=w2], absorb(year) cl(gvkey)
reghdfe capex_base _IyeaX*  PR [aw=w2], absorb(year#n2) cl(gvkey)


* Balanced panel
reghdfe capex_base _IyeaX*  PR if samp_count==16, absorb(year) cl(gvkey) 
reghdfe capex_base  _IyeaX* PR if samp_count==16, absorb(year#n2) cl(gvkey) 

reghdfe capex_base _IyeaX*  PR if samp_count==16 [aw=w], absorb(year) cl(gvkey)
reghdfe capex_base _IyeaX*  PR if samp_count==16 [aw=w], absorb(year#n2) cl(gvkey)

reghdfe capex_base _IyeaX*  PR if samp_count==16 [aw=w2], absorb(year) cl(gvkey)
reghdfe capex_base _IyeaX*  PR if samp_count==16 [aw=w2], absorb(year#n2) cl(gvkey)


*** What about size weighting too? ***
reghdfe capex_base _IyeaX*  PR [aw=w2], absorb(year#n2) cl(gvkey)

*** BASE NEW ***
preserve
foreach y of var  capex_base  {	//

*running the regression
reghdfe capex_base  _IyeaX* PR , absorb(year#n2) cl(gvkey) 

** figure years for the graph
gen fig_year = 1991 in 1
foreach i of numlist 1/16 {
local j = `i' + 1990
replace fig_year = `j' in `i'
}

** annual betas for the preperiod
gen pr_beta = 0
gen pr_beta_lb = 0
gen pr_beta_ub = 0
foreach i of numlist 1/4 {
local j = `i' + 1990
lincom _b[_IyeaXPR_`j'] , level(90)
replace pr_beta = r(estimate) in `i'
replace pr_beta_lb = r(lb) in `i' if `i' != 5
replace pr_beta_ub = r(ub) in `i' if `i' != 5
}
* zero effect in 1995
lincom  0
replace pr_beta = r(estimate) in 5

** annual betas for the postperiod
foreach i of numlist 6/16 {
local j = `i' + 1990
lincom _b[_IyeaXPR_`j'] , level(90)
replace pr_beta = r(estimate) in `i'
replace pr_beta_lb = r(lb) in `i' if `i' != 5
replace pr_beta_ub = r(ub) in `i' if `i' != 5
}
}


* outputting the graph
 graph twoway (scatter pr_beta fig_year, lcolor(navy) mcolor(navy) yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
    (rcap pr_beta_lb pr_beta_ub fig_year, lcolor(navy) lpattern(solid)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white))  ///
	xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
	legend(on order (1 2) label(1 "Puerto Rico Presence")  ///
	label(2 "90% Confidence Interval") r(1) position(6)) ///
	yti("Effect of S936 on ``y'_name'", margin(medium))
graph export "$output/PR Figures/figure_5_BASE.pdf", replace 
restore



*** BALANCED SAMPLE ***
preserve
foreach y of var  capex_base  {	//

*running the regression
reghdfe capex_base  _IyeaX* PR if samp_count==16, absorb(year#n2) cl(gvkey) 

** figure years for the graph
gen fig_year = 1991 in 1
foreach i of numlist 1/16 {
local j = `i' + 1990
replace fig_year = `j' in `i'
}

** annual betas for the preperiod
gen pr_beta = 0
gen pr_beta_lb = 0
gen pr_beta_ub = 0
foreach i of numlist 1/4 {
local j = `i' + 1990
lincom _b[_IyeaXPR_`j'] , level(90)
replace pr_beta = r(estimate) in `i'
replace pr_beta_lb = r(lb) in `i' if `i' != 5
replace pr_beta_ub = r(ub) in `i' if `i' != 5
}
* zero effect in 1995
lincom  0
replace pr_beta = r(estimate) in 5

** annual betas for the postperiod
foreach i of numlist 6/16 {
local j = `i' + 1990
lincom _b[_IyeaXPR_`j'] , level(90)
replace pr_beta = r(estimate) in `i'
replace pr_beta_lb = r(lb) in `i' if `i' != 5
replace pr_beta_ub = r(ub) in `i' if `i' != 5
}
}


* outputting the graph
 graph twoway (scatter pr_beta fig_year, lcolor(navy) mcolor(navy) yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
    (rcap pr_beta_lb pr_beta_ub fig_year, lcolor(navy) lpattern(solid)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white))  ///
	xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
	legend(on order (1 2) label(1 "Puerto Rico Presence")  ///
	label(2 "90% Confidence Interval") r(1) position(6)) ///
	yti("Effect of S936 on ``y'_name'", margin(medium))
graph export "$output/PR Figures/figure_5_BALANCED.pdf", replace 
restore





*** DFL WEIGHTS ***
preserve
foreach y of var  capex_base  {	//

*running the regression
reghdfe capex_base _IyeaX*  PR [aw=w], absorb(year#n2) cl(gvkey)

** figure years for the graph
gen fig_year = 1991 in 1
foreach i of numlist 1/16 {
local j = `i' + 1990
replace fig_year = `j' in `i'
}

** annual betas for the preperiod
gen pr_beta = 0
gen pr_beta_lb = 0
gen pr_beta_ub = 0
foreach i of numlist 1/4 {
local j = `i' + 1990
lincom _b[_IyeaXPR_`j'] , level(90)
replace pr_beta = r(estimate) in `i'
replace pr_beta_lb = r(lb) in `i' if `i' != 5
replace pr_beta_ub = r(ub) in `i' if `i' != 5
}
* zero effect in 1995
lincom  0
replace pr_beta = r(estimate) in 5

** annual betas for the postperiod
foreach i of numlist 6/16 {
local j = `i' + 1990
lincom _b[_IyeaXPR_`j'] , level(90)
replace pr_beta = r(estimate) in `i'
replace pr_beta_lb = r(lb) in `i' if `i' != 5
replace pr_beta_ub = r(ub) in `i' if `i' != 5
}
}


* outputting the graph
 graph twoway (scatter pr_beta fig_year, lcolor(navy) mcolor(navy) yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
    (rcap pr_beta_lb pr_beta_ub fig_year, lcolor(navy) lpattern(solid)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white))  ///
	xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
	legend(on order (1 2) label(1 "Puerto Rico Presence")  ///
	label(2 "90% Confidence Interval") r(1) position(6)) ///
	yti("Effect of S936 on ``y'_name'", margin(medium))
graph export "$output/PR Figures/figure_5_DFL.pdf", replace 
restore







*** DFL WEIGHTS W SIZE ***
preserve
foreach y of var  capex_base  {	//

*running the regression
reghdfe capex_base _IyeaX*  PR [aw=w2], absorb(year#n2) cl(gvkey)

** figure years for the graph
gen fig_year = 1991 in 1
foreach i of numlist 1/16 {
local j = `i' + 1990
replace fig_year = `j' in `i'
}

** annual betas for the preperiod
gen pr_beta = 0
gen pr_beta_lb = 0
gen pr_beta_ub = 0
foreach i of numlist 1/4 {
local j = `i' + 1990
lincom _b[_IyeaXPR_`j'] , level(90)
replace pr_beta = r(estimate) in `i'
replace pr_beta_lb = r(lb) in `i' if `i' != 5
replace pr_beta_ub = r(ub) in `i' if `i' != 5
}
* zero effect in 1995
lincom  0
replace pr_beta = r(estimate) in 5

** annual betas for the postperiod
foreach i of numlist 6/16 {
local j = `i' + 1990
lincom _b[_IyeaXPR_`j'] , level(90)
replace pr_beta = r(estimate) in `i'
replace pr_beta_lb = r(lb) in `i' if `i' != 5
replace pr_beta_ub = r(ub) in `i' if `i' != 5
}
}


* outputting the graph
 graph twoway (scatter pr_beta fig_year, lcolor(navy) mcolor(navy) yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
    (rcap pr_beta_lb pr_beta_ub fig_year, lcolor(navy) lpattern(solid)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white))  ///
	xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
	legend(on order (1 2) label(1 "Puerto Rico Presence")  ///
	label(2 "90% Confidence Interval") r(1) position(6)) ///
	yti("Effect of S936 on ``y'_name'", margin(medium))
graph export "$output/PR Figures/figure_5_DFL2.pdf", replace 
restore



*** DFL WEIGHTS BALANCED ***
preserve
foreach y of var  capex_base  {	//

*running the regression
reghdfe capex_base _IyeaX*  PR if samp_count==16 [aw=w], absorb(year#n2) cl(gvkey)

** figure years for the graph
gen fig_year = 1991 in 1
foreach i of numlist 1/16 {
local j = `i' + 1990
replace fig_year = `j' in `i'
}

** annual betas for the preperiod
gen pr_beta = 0
gen pr_beta_lb = 0
gen pr_beta_ub = 0
foreach i of numlist 1/4 {
local j = `i' + 1990
lincom _b[_IyeaXPR_`j'] , level(90)
replace pr_beta = r(estimate) in `i'
replace pr_beta_lb = r(lb) in `i' if `i' != 5
replace pr_beta_ub = r(ub) in `i' if `i' != 5
}
* zero effect in 1995
lincom  0
replace pr_beta = r(estimate) in 5

** annual betas for the postperiod
foreach i of numlist 6/16 {
local j = `i' + 1990
lincom _b[_IyeaXPR_`j'] , level(90)
replace pr_beta = r(estimate) in `i'
replace pr_beta_lb = r(lb) in `i' if `i' != 5
replace pr_beta_ub = r(ub) in `i' if `i' != 5
}
}


* outputting the graph
 graph twoway (scatter pr_beta fig_year, lcolor(navy) mcolor(navy) yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
    (rcap pr_beta_lb pr_beta_ub fig_year, lcolor(navy) lpattern(solid)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white))  ///
	xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
	legend(on order (1 2) label(1 "Puerto Rico Presence")  ///
	label(2 "90% Confidence Interval") r(1) position(6)) ///
	yti("Effect of S936 on ``y'_name'", margin(medium))
graph export "$output/PR Figures/figure_5_DFL_BALANCED.pdf", replace 
restore
