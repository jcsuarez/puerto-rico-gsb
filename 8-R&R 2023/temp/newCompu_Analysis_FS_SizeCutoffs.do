*** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
*** *** *** Section (1): New main specification *** *** ***
*** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
use "$data/Replication Data/newCompu_Clean_0923", replace

* create new fs_* variables

gen ppes_capx = cfor_capx / ppent
gen as_capx = cfor_capx / at

tab idbflag PR if year==1995
tab fic
drop if PR==0 & idbflag=="D"
*drop if dup_tag>0

* some DFL weights first, then drop (for entropy balancing)
xtile ppe95_binDFL = ppent if year == 1995 , n(5) 
logit PR i.n2##i.ppe95_binDFL if year == 1995
capture: drop phat min_phat w w_phat
predict phat, pr 
*winsor phat, p(.01) g(w_phat) 
*replace phat = w_phat
bys gvkey: egen min_phat = min(phat) 
* ATOT
gen DFL2 = (PR+(1-PR)*min_phat/(1-min_phat))

* ebalance weights
gen n1 = floor(n3/100)
ebalance PR i.n1##i.ppe95_binDFL if year==1995 // algorithm fails to adjust the mean (1st order moment) of 2.n2; works fine if we don't cut by size distribution
// also works if ebal PR i.n1 i.ppe95_binDFL separately (non-interacted)
xfill _webal, i(gvkey)


xtile PR_sizeDist = ppe95 if PR==1, n(3)
xtile PR_sizeDist10 = ppe95 if PR==1, n(10)

tab PR if year==1995, sum(ppe95)
tab PR_sizeDist if year==1995, sum(ppe95)
drop if PR_sizeDist10<7


drop ppe95_bins10
xtile ppe95_bins10 = ppe95, n(10)


gen temp1 = (year==1991)
gen temp2 = (year==2006)
egen pre_bal = max(temp1), by(gvkey)
egen post_bal = max(temp2), by(gvkey)
drop temp?
tab year PR if pre_bal
*keep if pre_bal & post_bal




global spec1 ", cl(gvkey) a(gvkey ppe95_bins10##year)"
global spec2 ", cl(gvkey) a(gvkey n2##ppe95_bins10##year)"
global spec3 "if exp_sec, cl(gvkey) a(gvkey ppe95_bins10##year)"
global spec4 "if exp_ind, cl(gvkey) a(gvkey ppe95_bins10##year)"
global spec5 "[aw=_webal], cl(gvkey) a(gvkey ppe95_bins10##year)"
global spec6 ", cl(gvkey) a(gvkey ppe95_bins10##year)"

global specname1 "95 PPE: Baseline"
global specname2 "95 PPE: Sector-by-Size FE"
global specname3 "95 PPE: Exposed Sectors"
global specname4 "95 PPE: Exposed Industries"
global specname5 "95 PPE: Entropy Balancing"
global specname6 "95 PPE: DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"

*** Store estimates ***
local cnt = 0

foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 {
local cnt = `cnt' + 1
reghdfe cfor_capx _IyeaX* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/`cnt' {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec5", lco($c5) mco($c5) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec5", lco($c5) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec6", lco($c6) mco($c6) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec6", lco($c6) lpa(solid)) ///
	 , yline(0 , lcolor(gs12)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(3) on order (1 3 5 7 9 11) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3") ///
		label(7 "$specname4") ///
		label(9 "$specname5") ///
		label(11 "$specname6")  ) ///
		yti("Effect of S936 Exposure on Investment", margin(medium))
restore	
}

graph export "$output/Graphs/kevin/newCompu_Baseline_`y'_FS.pdf", replace 	
		
	