// This file recreates Table 9, long difference conspuma analysis
// Author: Dan Garrett
// Date: 5-9-2020

/* Data sources:
QCEW, NETS, IRS, BEA
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* starting with data from figure 15
*******************************************************
use "$data/Replication Data/figure15_data", replace

* Regressions on Wage 
eststo mw_d :areg dadjlwage   c.pr_link* i.year  [aw=epop] if year > 1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year > 1990
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

eststo mw_a :areg dadjlwage   c.pr_link* i.year  [aw=epop] if year ==1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year == 1990 
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

* Regressions on Wage 
eststo mwS_d : areg dSadjlwage   c.pr_link* i.year  [aw=epop] if year > 1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year > 1990
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

eststo mwS_a : areg dSadjlwage   c.pr_link* i.year  [aw=epop] if year ==1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year == 1990 
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

* Regressions on Wage 
eststo mwU_d : areg dUadjlwage   c.pr_link* i.year  [aw=epop] if year > 1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year > 1990
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

eststo mwU_a : areg dUadjlwage   c.pr_link* i.year  [aw=epop] if year ==1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year == 1990 
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"


* Regressions on Rent 
eststo mr_d :areg dadjlrent   c.pr_link* i.year  [aw=epop] if year > 1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year > 1990
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

eststo mr_a :areg dadjlrent   c.pr_link* i.year  [aw=epop] if year ==1990    , a(state_fips) cl(state_fips )
count if e(smaple) & year == 1990 
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

* Regressions on Rent 
eststo mv_d :areg dadjlvalue   c.pr_link* i.year  [aw=epop] if year > 1990   , a(state_fips) cl(state_fips )
count if e(smaple) & year > 1990
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

eststo mv_a :areg dadjlvalue   c.pr_link* i.year  [aw=epop] if year ==1990   , a(state_fips) cl(state_fips )
count if e(smaple) & year == 1990 
estadd scalar N_uw = r(N)
estadd local hasy "Yes"
estadd local hass "Yes"

esttab m*d using "$output/Tables/table9.tex", drop(*) stats() ///
b(3) se par label coeflabel(pr_link "Exposure to Section 936 ") ///
noobs nogap nomtitle tex replace nocons postfoot("")

esttab m*d using "$output/Tables/table9.tex", keep(pr_link)  ///
cells(b(fmt(3)) se(par) p)  coll(none) noobs scalars( "N_uw Observations" "hasy Year Fixed Effects" "hass State Fixed Effects") label /// 
coeflabel(pr_link "Exposure to Section 936 ") mlab("All"  "High Skill" "Low Skill" "Rent" "Home Value" ) ///
preh("\multicolumn{6}{l}{\textbf{Stacked Differences After 1990}} \\ & \multicolumn{3}{c}{Wages} & & \\ \cmidrule(lr){2-4} ") postfoot("\hline") append sty(tex) nonum

esttab m*a using "$output/Tables/table9.tex", keep(pr_link)  ///
cells(b(fmt(3)) se(par) p)  coll(none) noobs scalars( "N_uw Observations" "hasy Year Fixed Effects" "hass State Fixed Effects") label /// 
coeflabel(pr_link "Exposure to Section 936 ") mlab("All"  "High Skill" "Low Skill" "Rent" "Home Value" ) ///
preh("\multicolumn{6}{l}{\textbf{Differences Before 1990}} \\ & \multicolumn{3}{c}{Wages} & & \\ \cmidrule(lr){2-4} ") prefoot("\hline") ///
postfoot( ///
"  \\ \hline\hline" ///
"\end{tabular} }" ) append sty(tex) nonum
