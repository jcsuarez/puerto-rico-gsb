// This file recreates Figure A3
// Author: Dan Garrett
// Date: 3-30-2020

clear all
set more off

/* Data sources:
All Panels: IRS SOI public data
*/

// importing data
use "$data/Replication Data/figure2A_abcd_data", clear

** making maps
maptile pr_link , geo(county1990) stateoutline(linewidthstyle)  nquantiles(5) res(.5)
graph export "$output/Graphs/map_pr_link_d.png", replace 

maptile pr_link_state , geo(county1990) stateoutline(linewidthstyle)  nquantiles(5)   res(.5)
graph export "$output/Graphs/map_pr_link_state_d.pdf", replace 

maptile pr_link_czone , geo(county1990) stateoutline(linewidthstyle)  nquantiles(5)   res(.5)
graph export "$output/Graphs/map_pr_link_czone_d.png", replace 

maptile pr_link_conspuma , geo(county1990) stateoutline(linewidthstyle)  nquantiles(5)   res(.5)
graph export "$output/Graphs/map_pr_link_conspuma_d.pdf", replace 
