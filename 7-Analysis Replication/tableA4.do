// This file recreates Table A4
// Author: Dan Garrett
// Date: 4-25-2020

/* Data sources:
Many sources, QCEW, and NETS
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data/tableA4_data", clear

local base " ln_pop "   
local spec1 " realmw rgtowork income_rate_avg salestax propertytax corpinctax rec_val rev_gdp d_tradeusch_pw l_sh_routine33a " 
local spec2a "labor_participation_rate pct_retail pct_agriculture pct_manufacturing_durable pct_manufacturing_nondurable capital_stock  pct_college  pct_less_HS pct_white  pct_black "
local spec2 "`base' `spec2a'" 

capture: gen cons = 1
local FE1 "cons"
local FE2 "statefips"

gen COV = . 
est clear
local i = 1
foreach var of varlist `spec1' {
	replace COV = . 
	replace COV = `var'
	qui: eststo reg1_`i': areg pr_link COV `spec1' [fw=pop] , a(`FE1') cl(statefips ) 
	local i=`i'+1
}

esttab reg1_* , 	star(* .1 ** .05 *** .01)   keep(COV)

local i = 1
foreach var of varlist `spec2a' {
	replace COV = . 
	replace COV = `var'
	qui: eststo reg2_`i': areg pr_link COV `spec2' [fw=pop] , a(`FE2') cl(statefips ) 
	local i=`i'+1
}

esttab reg2_* , 	star(* .1 ** .05 *** .01)   keep(COV)

********************* make outputs 
esttab reg1_1 reg1_2 reg1_3 reg1_4 reg1_5 using "$output/Tables/tableA4.tex", keep(COV)  ///
cells(b(fmt(3)) se(par) p) coll(none) s() noobs mlab("Min Wage" "Right to Work" "Pers. Income Tax" "Sales Tax" "Prop. Tax")  /// 
coeflabel(COV "\hspace{1em}Exposure to Section 936") ///
postfoot("\hline") replace sty(tex)

esttab reg1_6 reg1_7 reg1_8 reg1_9 reg1_10 using "$output/Tables/tableA4.tex", keep(COV)  ///
cells(b(fmt(3)) se(par) p)  coll(none) s() noobs ///
mlab("Corporate Tax" "R\&D Credit" "State Revenue/GDP" "Trade Exposure (China)" "Share of Routine Labor"  "Trade Exposure (Nafta)" ) /// 
coeflabel(COV "\hspace{1em}Exposure to Section 936") ///
preh("") postfoot("\hline") append sty(tex)

esttab reg2_1 reg2_2 reg2_3 reg2_4 reg2_5 using "$output/Tables/tableA4.tex", keep(COV)  ///
cells(b(fmt(3)) se(par) p)  coll(none) s() noobs /// 
mlab("LFPR" "\% Retail" "\% Agriuclture" "\% Manuf. Durable" "\% Manuf. Non-Durable")  ///
coeflabel(COV "\hspace{1em}Exposure to Section 936") ///
preh("") postfoot("\hline") append sty(tex)

esttab reg2_6 reg2_7 reg2_8 reg2_9 reg2_10 using "$output/Tables/tableA4.tex", keep(COV)  ///
cells(b(fmt(3)) se(par) p)  coll(none) s() noobs /// 
mlab("Capital Stock" "\% College" "\% Less HS" "\% White" "\% Black") ///
coeflabel(COV "\hspace{1em}Exposure to Section 936") ///
preh("") prefoot("\hline") ///
postfoot( ///
"  \\ \hline\hline" ///
"\end{tabular} }" ) append sty(tex)