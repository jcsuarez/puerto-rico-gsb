// This file recreates Figure 1
// Author: Dan Garrett
// Date: 3-30-2020


/* Data sources:
Panel A: IRS SOI public data
Panel B: IRS SOI public data
Panel C: IRS SOI public data combined with sector level scalars derived from Compustat and NETS
*/

*******************************************************
* Panels A and B
*******************************************************
 
use "$data/Replication Data/figure1_ab_data", clear

*  creating a graph by generating the percent for each industry-year then graphing 
bys year (group): gen graph_sum = sum(Us)
bys year (group): egen total = max(graph_sum)
gen graph_sum_perc = 100*graph_sum/total

twoway (area graph_sum_perc year if variable == "man_paper_") ///
(area graph_sum_perc year if variable == "man_plastics_") ///
(area graph_sum_perc year if variable == "man_trans_") ///
(area graph_sum_perc year if variable == "man_textile_") ///
(area graph_sum_perc year if variable == "other") ///
(area graph_sum_perc year if variable == "man_misc_") ///
(area graph_sum_perc year if variable == "man_food_bev_") ///
(area graph_sum_perc year if variable == "man_equipment_") /// 
(area graph_sum_perc year if variable == "man_chemicals_")  ///
, xlab(1995(2)2006) ///
	   	graphregion(fcolor(white)) bgcolor(white) ///
		xtitle("Year") ///
		ytitle("Percent of Total", margin(medsmall)) ///
		legend(order(9 8 7 6 5 4 3 2 1) label(9 "Chemical Man.")  label(8 "Equipment Man.") ///
		label(7 "Food/Bev Man.")  label(6 "Other Man.") label(5 "Other Non-Man.")  label(4 "Textile Man.") ///
		label(3 "Transport Man.") label(2 "Plastics Man.") label(1 "Paper Man.")  rows(3) ) 
graph export "$output/Graphs/SOI_industry_perc.pdf" , replace 

* turning credits into millions of dollars
replace graph_sum = graph_sum/1000000
		
* Manually adjusting for inflation to put everything in 2017 dollars (CPI-U):
replace graph_sum = graph_sum * 245.1 / 152.4 if year == 1995
replace graph_sum = graph_sum * 245.1 / 156.9 if year == 1996
replace graph_sum = graph_sum * 245.1 / 160.5 if year == 1997
replace graph_sum = graph_sum * 245.1 / 163.0 if year == 1998
replace graph_sum = graph_sum * 245.1 / 166.6 if year == 1999
replace graph_sum = graph_sum * 245.1 / 172.2 if year == 2000
replace graph_sum = graph_sum * 245.1 / 177.1 if year == 2001
replace graph_sum = graph_sum * 245.1 / 179.9 if year == 2002
replace graph_sum = graph_sum * 245.1 / 184.0 if year == 2003
replace graph_sum = graph_sum * 245.1 / 188.9 if year == 2004
replace graph_sum = graph_sum * 245.1 / 195.3 if year == 2005
replace graph_sum = graph_sum * 245.1 / 201.6 if year == 2005

* graphing totals (cumulative from smallest to largest)
twoway (area graph_sum year if variable == "man_paper_") ///
(area graph_sum year if variable == "man_plastics_") ///
(area graph_sum year if variable == "man_trans_") ///
(area graph_sum year if variable == "man_textile_") ///
(area graph_sum year if variable == "other") ///
(area graph_sum year if variable == "man_misc_") ///
(area graph_sum year if variable == "man_food_bev_") ///
(area graph_sum year if variable == "man_equipment_") /// 
(area graph_sum year if variable == "man_chemicals_")  ///
, xlab(1995(2)2006) ///
	   	graphregion(fcolor(white)) bgcolor(white) ///
		xtitle("Year") ///
		ytitle("Billion of 2017 Dollars", margin(medsmall)) ///
		legend(order(9 8 7 6 5 4 3 2 1) label(9 "Chemical Man.")  label(8 "Equipment Man.") ///
		label(7 "Food/Bev Man.")  label(6 "Other Man.") label(5 "Other Non-Man.")  label(4 "Textile Man.") ///
		label(3 "Transport Man.") label(2 "Plastics Man.") label(1 "Paper Man.")  rows(3) )  ylab(0(1)5)
graph export "$output/Graphs/SOI_industry.pdf" , replace 

*******************************************************
* Panel C
*******************************************************

use "$data/Replication Data/figure1_c_data", replace

* renaming variable to be more usable
rename Income_Subject_To_Tax_base ISTT
rename Us_Possessions_Tax_Credit_base USPTC

** outputting the aggregate figure
local base_link = .0923148 // from NETS, share of US employment at firms with operations in PR
local beta_base "0.816" // from column (3) of Table A1

* share of income subject to tax that is covered by the US Possession tax credit
gen share = USPTC / ISTT

* Counterfactual US possessions tax credit holding share of income constant
gen const_cred = share * Income_Subject_To_Tax

* dollars of lost credits due to the rollback
gen lost_cred = const_cred - Us_Possessions_Tax_Credit

* oberserved effective tax rates
gen p_data = (Income_Tax_After_Credits) / Income_Subject_To_Tax

* Predicted tax rates had S936 not been repealed, correcting for 20% adjustment to other credits
gen p_new = (Income_Tax_After_Credits * `base_link' - `beta_base' * lost_cred ) / (Income_Subject_To_Tax * `base_link')

*graphing 
sort year
twoway (scatter p_new year, c(line) lpattern(dash) ) ///
       (scatter p_data year, c(line) lpattern(solid)) ///
	   , xlab(1995(2)2007) ylab(.16(.02).3) ///
	   	graphregion(fcolor(white)) bgcolor(white) ///
		xtitle("Year") ///
		ytitle("Effective Tax Rate for Exposed Industries", margin(medsmall)) ///
		legend(order(2 1) label(1 "Effective Tax Rate without S936 Repeal") label(2 "Observed Effective Tax Rate")  rows(2) ) 
graph export "$output/Graphs/SOI_counterfact_etr_ATOT_orig.pdf" , replace 




