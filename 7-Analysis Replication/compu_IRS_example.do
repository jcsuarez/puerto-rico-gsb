
*** Setup ***
clear all
set more off
snapshot erase _all 

global PATH "SET MY PATH"

*** Code for Clare ***
import delimited using "$PATH/compuPR_forIRS_Updated.csv", clear
/* 
* (0) Delete duplicate EINs for comparison w/ IRS
egen dup_flag = max(dup_tag), by(gvkey)
tab dup_flag
drop if dup_flag > 0
*/

* (1) Select sample *
drop if pr==0 & idbflag=="D" // drop undesired control firms
drop if pr_sizedist<7 // drop undesired treatment firms


* (2) create event study covariates
xi i.year|pr , noomit
drop _IyeaXpr_1995 


* (3) run event studies
global spec1 ", cl(gvkey) a(ppe95_bins10##year)"
global spec2 ", cl(gvkey) a(n2##year ppe95_bins10##year)"
global spec3 ", cl(gvkey) a(n2##ppe95_bins10##year)"

 foreach spec in spec1 spec2 spec3  {
	* Estimate event study model
	reghdfe capex_base _IyeaX* pr $`spec'
 }



