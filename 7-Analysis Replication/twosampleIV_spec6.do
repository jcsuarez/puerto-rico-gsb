 // Define Command for MYQR
capture program drop twosampleIV_spec6
program define twosampleIV_spec6 , eclass 
syntax, spec(string)

	reghdfe ftax PR PR_post `spec' [aw=DFL] if pi> 4 & ~inlist(n3,"324","323","337","322","327","336","331","321") & inlist(n2,3,5,6,7,11,12) , a( year gvkey)
	
	tempvar ftax_hat
	predict `ftax_hat', xb
	
	reghdfe capex_base `ftax_hat' `spec' [aw=DFL] if  ~inlist(n3,"324","323","337","322","327","336","331","321") & inlist(n2,3,5,6,7,11,12) , a( year gvkey)
	drop `ftax_hat'
	
	tempname coef 
	mat `coef' = e(b) 
	ereturn clear 
	scalar `coef' = el(`coef',1,1)
	ereturn scalar ftaxhat = `coef'
end
