// This file recreates Figure A2 in the appendix
// Author: Dan Garrett
// Date: 3-30-2020


/* Data sources:
Panel A: NETS establishment data
Panel B: NETS establishment data
*/

*******************************************************
* Panels A and B
*******************************************************


use "$data/Replication Data/figure2A_ab_data", clear 

rename fips95 county 
* version using establishment links
maptile pr_link , geo(county1990) stateoutline(linewidthstyle)  nquantiles(5) res(.5)
graph export "$output/Graphs/map_pr_link_d.pdf", replace

* version using employment links
maptile pr_emp_link , geo(county1990) stateoutline(linewidthstyle)  nquantiles(5) res(.5)
graph export "$output/Graphs/map_pr_link_emp_d.pdf", replace
