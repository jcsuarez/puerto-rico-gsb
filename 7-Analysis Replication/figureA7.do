// This file recreates Figure A7
// Author: Dan Garrett
// Date: 4-20-2020

/* Data sources:
various sources including NETS and compustat
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* Panels A and B
*******************************************************
use "$data/Replication Data/figureA7_data", replace

*** labels for some graphs
local fs_capx_name "Foreign Share Investment"
local capex_base_name "Investment/Physical Capital"
local capex_base_p_name "% Change in Investment/Physical Capital"
local capex_base_1_name "Investment/Physical Capital"
local ftax_name "Federal Tax Rate"
 
* combining years and N2
egen yearg = group(year n2)

*figures 5, 6, A5, and A6
xi i.year|PR , noomit
drop _IyeaXPR_1995 

forval i=1990(1)2014 { 
	capture: label var _IyeaXPR_`i' " "
} 
forval i=1991(2)2014 { 
	capture: label var _IyeaXPR_`i' "`i'"
} 
snapshot save
 
***************************************************
* figures A7, all panels
***************************************************
** specifications with firm fixed effects: 
foreach y of var  capex_base capex_base_p capex_base_1 {	
	
snapshot restore 1
* regressions	
reghdfe `y' _IyeaX* PR IHS_rev log_tot_emp [aw=DFL] if  ~inlist(n3,"324","323","337","322","327","336","331","321") & inlist(n2,3,5,6,7,11,12), cl(gvkey)   a(A = gvkey B = yearg)
local plot_mean = (_b[_IyeaXPR_1991] + _b[_IyeaXPR_1992] + _b[_IyeaXPR_1993] + _b[_IyeaXPR_1994] )/ 4

** residualizing out the differences in average FE for each group to correct for changing samples
foreach i of numlist 1991/2006 {
sum A [aw=DFL] if PR == 0 & e(sample) & year == `i'
scalar nonpr_mean`i' = r(mean)
sum A [aw=DFL] if PR == 1 & e(sample) & year == `i'

** taking out variation in regressors at treatment-year level
scalar pr_mean`i' = r(mean)
sum log_tot_emp [aw=DFL] if e(sample) & year == `i'
scalar rev_mean`i' = r(mean)
sum log_tot_emp [aw=DFL] if e(sample) & year == `i'
scalar emp_mean`i' = r(mean)

sum B [aw=DFL] if e(sample) & year == `i'
scalar year_mean`i' = r(mean)

}

* year FE and graphing identifier
gen year_fe = 0
gen fig_year = 1991 in 1
foreach i of numlist 2/16 {
local j = `i' + 1990
replace year_fe = year_mean`j' in `i'
replace fig_year = `j' in `i'
}

* taking out level difference in 1995
scalar level = pr_mean1995 - (nonpr_mean1995 )

gen pr_beta = 0
gen pr_beta_lb = 0
gen pr_beta_ub = 0
foreach i of numlist 1/4 {
local j = `i' + 1990
lincom _b[_IyeaXPR_`j'] + pr_mean`j' - nonpr_mean`j' - level , level(90)
replace pr_beta = r(estimate) in `i'
replace pr_beta_lb = r(lb) in `i' if `i' != 5
replace pr_beta_ub = r(ub) in `i' if `i' != 5
}
lincom  pr_mean1995 - nonpr_mean1995 - level, level(90)
replace pr_beta = r(estimate) in 5
foreach i of numlist 6/16 {
local j = `i' + 1990
lincom _b[_IyeaXPR_`j'] + pr_mean`j' - nonpr_mean`j' - level , level(90)
replace pr_beta = r(estimate) in `i'
replace pr_beta_lb = r(lb) in `i' if `i' != 5
replace pr_beta_ub = r(ub) in `i' if `i' != 5
}

***  total effect version, residualized
** adding in baseline value:
sum ``y'_sum' [aw = DFL] if e(sample) // ``y'_sum'
scalar baseline = r(mean)

** outputting a type of marginal effect graph

graph twoway (scatter pr_beta fig_year, lcolor(navy) mcolor(navy) yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
    (rcap pr_beta_lb pr_beta_ub fig_year, lcolor(navy) lpattern(solid)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white))  ///
	xtitle("Year") xlab(1991(2)2006,val) bgcolor(white) ///
	legend(on order (1 2) label(1 "Puerto Rico Presence")  ///
	label(2 "90% Confidence Interval") r(1)) ///
	yti("Effect of S936 on ``y'_name'", margin(medium))
graph export "$output/Graphs/graph_`y'_ES_spec5_f.pdf", replace 
}
