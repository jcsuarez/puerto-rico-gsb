// This file recreates Figure 3
// Author: Dan Garrett
// Date: 3-30-2020


/* Data sources:
Panel A: NETS establishment shares at county level
Panel B: NETS establishment shares at county level
*/

clear all
set more off

* importing necessary data
use "$data/Replication Data/figure3_ab_data", clear

* Residualized Link 
reg pr_link i.fips_state
predict res_pr_link, res

* winsorizing residualized links
winsor res_pr_link, gen(w_res_pr_link) p(.01)
egen std_w_res_pr_link = std(w_res_pr_link)

** graphing the baseline version
maptile pr_link , geo(county1990) stateoutline(linewidthstyle)  nquantiles(5) res(.5)
graph export "$output/Graphs/map_pr_link_d.pdf", replace 

** graphing the residualized version
maptile std_w_res_pr_link , geo(county1990) stateoutline(linewidthstyle)  nquantiles(5)   res(.5)
graph export "$output/Graphs/map_pr_link_std_d.pdf", replace 
