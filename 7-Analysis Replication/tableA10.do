// This file recreates Table A10
// Author: Dan Garrett
// Date: 4-27-2020

/* Data sources:
NETS
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data/tableA10_data", clear

*making interaction variable
xi i.year|PR, noomit 
drop _IyeaXP*1995

* running regressions
est clear
eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
   [aw=wgt] , a(i.year)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"    

eststo emp_1:  reghdfe emp_growth _IyeaXP*  ///
   [aw=wgt] , a(i.year#i.naic2)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"    
  
eststo emp_2:  reghdfe emp_growth _IyeaXP*  ///
  if major_sec  [aw=wgt] , a(i.year#i.naic2)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"      

eststo emp_3:  reghdfe emp_growth _IyeaXP* ///
  if major_ind [aw=wgt] , a(i.year#i.naic2)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"     

eststo emp_4:  reghdfe emp_growth _IyeaXP* ///
  if major_ind [aw=w] , a(i.year#i.naic2)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"       
  
******** event study table
esttab emp_? using "$output/Tables/tableA10.tex", drop(*) stats() ///
b(3) se par label ///
noobs nogap nomtitle tex replace  nocons postfoot("\multicolumn{5}{l}{\textbf{Exposure to Section 936}} \\") prefoot("") posthead("")

esttab emp_? using "$output/Tables/tableA10.tex", keep(_IyeaXPR_*) ///
 b(3) se par mlab(none) coll(none) s() noobs nonum nogap scalars("N Observations"  )  sfmt(0) /// 
varlabel(_IyeaXPR_1990 "\hspace{1em}X 1990 " /// 
_IyeaXPR_1991 "\hspace{1em}X 1991 " /// 
_IyeaXPR_1992 "\hspace{1em}X 1992 " /// 
_IyeaXPR_1993 "\hspace{1em}X 1993 " /// 
_IyeaXPR_1994 "\hspace{1em}X 1994 " /// 
_IyeaXPR_1996 "\hline \hspace{1em}X 1996 " /// 
_IyeaXPR_1997 "\hspace{1em}X 1997 " /// 
_IyeaXPR_1998 "\hspace{1em}X 1998 " /// 
_IyeaXPR_1999 "\hspace{1em}X 1999 " /// 
_IyeaXPR_2000 "\hspace{1em}X 2000 " /// 
_IyeaXPR_2001 "\hspace{1em}X 2001 " /// 
_IyeaXPR_2002 "\hspace{1em}X 2002 " /// 
_IyeaXPR_2003 "\hspace{1em}X 2003 " /// 
_IyeaXPR_2004 "\hspace{1em}X 2004 " /// 
_IyeaXPR_2005 "\hspace{1em}X 2005 " /// 
_IyeaXPR_2006 "\hspace{1em}X 2006 " /// 
_IyeaXPR_2007 "\hspace{1em}X 2007 " /// 
_IyeaXPR_2008 "\hspace{1em}X 2008 " /// 
_IyeaXPR_2009 "\hspace{1em}X 2009 " /// 
_IyeaXPR_2010 "\hspace{1em}X 2010 " /// 
_IyeaXPR_2011 "\hspace{1em}X 2011 " /// 
_IyeaXPR_2012 "\hspace{1em}X 2012 ")  posth("\hline") ///
preh("")  postfoot( ///
"\hline Year Fixed Effects    & Y & Y & Y & Y & Y \\ " ///
"NAICS-by-Year Fixed Effects    & & Y & Y & Y & Y \\ " ///
"S936 Exposed Sector     & & & Y & Y & Y  \\ "  ///
"S936 Exposed Industry & & &   & Y  & Y  \\ " /// 
"DFL Weights & & & & &  Y   \\  \hline\hline" ///
"\end{tabular} }" ) append sty(tex)
