// This file recreates Figure A12
// Author: Juan Carlos
// Date: 4-20-2020

/* Data sources:
Compustat
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* log US employment on log capital absorbing industries
*******************************************************
use "$data/Replication Data/figureA12_data", clear

areg ln_US_emp l_K i.year ,a(n3)

xi: binscatter ln_US_emp l_K , absorb(n3 )  control(i.year) noadd ///
 xtitle("Log Capital Expenditures") ytitle("Log US Employment")  ///
 reportreg note("Note: {&beta}=0.39***(0.028)")
graph export "$output/Graphs/figureA12.pdf", replace 