// This file recreates Figure 5
// Author: Dan Garrett
// Date: 3-31-2020

/* Data sources:
various sources including NETS and compustat
*/

clear all
set more off
snapshot erase _all 
*** *** *** *** *** *** *** *** ** *** *** ***
*** *** Section (0): Clean up somewhat *** ***
*** *** *** *** *** *** *** *** ** *** *** ***

*use "$data/Replication Data/figure8_data_EY_081523", clear
use "$data/Replication Data/figure8_data_KR_092423", clear



drop if base_emp ==0 
sum base_emp if year == 1995 & PR , d
gen wgt=base_emp/( r(mean)) if PR 

sum base_emp if year == 1995 & PR==0 , d
replace wgt=base_emp/( r(mean)) if PR==0 

winsor wgt, p(.01) g(w_wgt) 

** Make balanced panel by dropping firms that drop out
bys hqduns95 : egen min_emp = min(emp_US) 
drop if min_emp == 0 


** Size Controls **
xtile emp_bins20 = base_emp, n(20)
xtile emp_bins10 = base_emp, n(10)
xtile emp_bins3 = base_emp, n(3)

** Regression setup **
	xi i.year|PR, noomit 
	drop _IyeaXP*1995

set matsize 4000


tostring m_naic3, gen(naic2)
replace naic2 = substr(naic2, 1,2)
replace naic2 = substr(naic2,1,1) if substr(naic2,1,1)=="3"
destring naic2, replace



* Look at distribution
tab emp_bins20 PR if year==1995, sum(base_emp)



save "$data/Replication Data/newNETS_Clean_0923", replace


*** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *
*** *** Section (1): Summarize firm size distribution *** ***
*** *** *** *** *** *** *** *** *** *** *** *** *** *** *** *
/*
use "$data/Replication Data/newNETS_Clean_0923", clear
sum base_emp if year==1995 & PR==1, detail
sum base_emp if year==1995 & PR==0, detail

keep if year==1995
gen firms = 1
gen PR_ = "Puerto Rico Firms" if PR==1
replace PR_ = "Control Firms" if PR==0	
*drop if base_emp<4000
*drop emp_bins10	
*xtile emp_bins10 = base_emp, n(10)
	
* Ventiles
preserve
	collapse (sum) firms (mean) base_emp, by(emp_bins20 PR_)
	label variable firms "Number of Firms"


	graph bar firms, over(PR) over(emp_bins20) asyvars ///
		ytitle("Number of Firms") legend(pos(6) rows(1)) blabel(bar)
	graph export "$output/Graphs/kevin/ventiles_firms_NETS.pdf", replace 	
		
	graph bar base_emp, over(PR) over(emp_bins20) asyvars ///
		ytitle("Mean Employment, 1995") legend(pos(6) rows(1)) 
	graph export "$output/Graphs/kevin/ventiles_emp_NETS.pdf", replace 	
			
restore
	
* Deciles (dropping bottom two terciles)
preserve
	keep if emp_bins3 == 3
	drop emp_bins10	
	xtile emp_bins10 = base_emp, n(10)
	collapse (sum) firms (mean) base_emp, by(emp_bins10 PR_)
	label variable firms "Number of Firms"

	graph bar firms, over(PR) over(emp_bins10) asyvars ///
		ytitle("Number of Firms") legend(pos(6) rows(1)) blabel(bar)
	graph export "$output/Graphs/kevin/deciles_firms_NETS_Top3rd.pdf", replace 	
		
	graph bar base_emp, over(PR) over(emp_bins10) asyvars ///
		ytitle("Mean Employment, 1995") legend(pos(6) rows(1)) 
	graph export "$output/Graphs/kevin/deciles_emp_NETS_Top3rd.pdf", replace 	
			
restore

* Terciles
preserve
	collapse (sum) firms (mean) base_emp, by(emp_bins3 PR_)
	label variable firms "Number of Firms"


	graph bar firms, over(PR) over(emp_bins3) asyvars ///
		ytitle("Number of Firms") legend(pos(6) rows(1)) blabel(bar)
	graph export "$output/Graphs/kevin/terciles_firms_NETS.pdf", replace 	
		
	graph bar base_emp, over(PR) over(emp_bins3) asyvars ///
		ytitle("Mean Employment, 1995") legend(pos(6) rows(1)) 
	graph export "$output/Graphs/kevin/terciles_emp_NETS.pdf", replace 	
					
restore
*/




*** *** *** *** ***  *** *** ***  *** *** *** *** *** ***
*** *** *** Section (2): Baseline Regressions *** *** ***
*** *** *** *** ***  *** *** ***  *** *** *** *** *** ***
use "$data/Replication Data/newNETS_Clean_0923", clear


** Now do DFL
xtile binDFL = base_emp if year == 1995 , n(20) 
logit PR i.naic2##i.binDFL if year == 1995
capture: drop phat min_phat w w_phat DFL?
predict phat, pr 
*winsor phat, p(.01) g(w_phat) 
*replace phat = w_phat
bys hqduns95: egen min_phat = min(phat) 
* ATOT
gen DFL2 = wgt*(PR+(1-PR)*min_phat/(1-min_phat))



tab binDFL PR if year==1995, sum(base_emp)


gen pre = (year<1995)
gen lo = (year==1995)
gen post1 = (year>1995)*(year<=2003)
gen post2 = (year>2003)

foreach var in pre post1 post2 {
	gen ET_`var' = (`var'==1)*PR
}



global level "PR"
global spec1 " [aw=wgt],vce(cluster hqduns) nocon a($level year)"
global spec2 " [aw=wgt],vce(cluster hqduns) nocon a($level naic2##year emp_bins3##year)"
global spec3 "if major_sec [aw=wgt],vce(cluster hqduns) nocon a($level naic2##year  emp_bins3##year)"
global spec4 "if major_ind [aw=wgt],vce(cluster hqduns) nocon a($level naic2##year  emp_bins3##year)"
global spec5 "if major_ind [aw=DFL2], vce(cluster hqduns) nocon a($level naic2##year emp_bins3##year)"



global specname1 "Baseline"
global specname2 "NAIC2"
global specname3 "Exp. Sector"
global specname4 "Exp. Industry"
global specname5 "Exp. Industry + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"



capture: drop emp_growth_w
winsor2 emp_growth, by(PR year) cuts(10 100)

foreach spec in spec1 spec2 spec3 spec4 spec5 {
	eststo `spec'_sto: reghdfe emp_growth_w ET* PR $`spec'
}

esttab spec?_sto, keep(ET_post2) cells(b p)


*** NETS ***
* Baseline, figure A.10, table

	* 1. Original Sample
	* 2. Top3rd
	* 3. Top3rd with winsorizing (left terciles are really sensitive)
	




*** Store estimates ***
local cnt = 0
foreach spec in spec1 spec2 spec3 spec4 spec5 {
local cnt = `cnt' + 1
qui reghdfe emp_growth _IyeaXP* PR $`spec'
estimates save `spec'_base, replace
				
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/22 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/22 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 23
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/`cnt' {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec5", lco($c5) mco($c5) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec5", lco($c5) lpa(solid)) ///
		, yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2012) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7 9 ) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")  ///
		label(9 "$specname5")  ) ///
		yti("Effect of S936 on Employment Growth", margin(medium))
restore	
}

graph export "$output/Graphs/kevin/NETS_Overlay_Original.pdf", replace  	



*** Notes:
	* Baseline, NAIC2 is better
	* Drop outliers, NAIC3 is better

*** *** *** *** ***  *** *** ***  *** *** *** *** *** ***
*** *** *** Section (3): Baseline Regressions *** *** ***
*** *** *** *** ***  *** *** ***  *** *** *** *** *** ***
use "$data/Replication Data/newNETS_Clean_0923", clear


keep if emp_bins3==3
*drop if base_emp<2000



** Now do DFL
xtile binDFL = base_emp if year == 1995 , n(5) 
logit PR naic2##i.binDFL if year == 1995
capture: drop phat min_phat w w_phat DFL?
predict phat, pr 
*winsor phat, p(.01) g(w_phat) 
*replace phat = w_phat
bys hqduns95: egen min_phat = min(phat) 
* ATOT
gen DFL2 = wgt*(PR+(1-PR)*min_phat/(1-min_phat))



* Re-do size weights
drop emp_bins*
xtile emp_bins5 = base_emp, n(5)
xtile emp_bins10 = base_emp, n(10)
tab emp_bins5 PR if year==1995, sum(base_emp)
tab emp_bins10 PR if year==1995, sum(base_emp)



* Make measures of long-run growth outliers
gen temp1 = emp_growth if (year==2012)
egen emp_gr12 = mean(temp1), by(hqduns)
drop temp1

gen temp1 = emp_growth if (year==2006)
egen emp_gr06 = mean(temp1), by(hqduns)
drop temp1

gen temp1 = emp_growth if (year==2000)
egen emp_gr00 = mean(temp1), by(hqduns)
drop temp1

* Make variables for one-step estimations
gen pre = (year<1995)
gen lo = (year==1995)
gen post1 = (year>1995)*(year<=2009)
gen post2 = (year>2009)

foreach var in pre lo post1 post2 {
	gen ET_`var' = (`var'==1)*PR
}
drop ET_lo


* Current iteration
global spec1 "if emp_gr06>-0.8 [aw=wgt],vce(cluster hqduns) nocon a(hqduns year)"
global spec2 "if emp_gr06>-0.8 [aw=wgt],vce(cluster hqduns) nocon a(hqduns m_naic3##year)"
global spec3 "if emp_gr06>-0.8 [aw=wgt],vce(cluster hqduns) nocon a(hqduns m_naic3##emp_bins5##year)"
global spec4 "if major_sec & emp_gr06>-0.8 [aw=wgt],vce(cluster hqduns) nocon a(hqduns m_naic3##emp_bins5##year )"
global spec5 "if major_ind & emp_gr06>-0.8 [aw=wgt],vce(cluster hqduns) nocon a(hqduns m_naic3##emp_bins5##year )"
global spec6 "if emp_gr06>-0.8 [aw=DFL2],vce(cluster hqduns) nocon a(hqduns m_naic3##emp_bins5##year)"

global spec1 "[aw=wgt], vce(cluster hqduns) nocon a(hqduns year)"
global spec2 "[aw=wgt], vce(cluster hqduns) nocon a(hqduns m_naic3##year)"
global spec3 "[aw=wgt], vce(cluster hqduns) nocon a(hqduns m_naic3##emp_bins5##year)"
global spec4 "if major_sec [aw=wgt],vce(cluster hqduns) nocon a(hqduns m_naic3##emp_bins5##year )"
global spec5 "if major_ind [aw=wgt],vce(cluster hqduns) nocon a(hqduns m_naic3##emp_bins5##year )"
global spec6 "if major_ind [aw=DFL2],vce(cluster hqduns) nocon a(hqduns m_naic3##emp_bins5##year)"

global spec1 "[aw=wgt], vce(cluster hqduns) nocon a(hqduns year)"
global spec2 "[aw=wgt], vce(cluster hqduns) nocon a(hqduns naic2##year)"
global spec3 "[aw=wgt], vce(cluster hqduns) nocon a(hqduns naic2##year emp_bins5##year)"
global spec4 "if major_sec [aw=wgt],vce(cluster hqduns) nocon a(hqduns naic2##year )"
global spec5 "if major_ind [aw=wgt],vce(cluster hqduns) nocon a(hqduns naic2##year )"
global spec6 "if major_ind [aw=DFL2],vce(cluster hqduns) nocon a(hqduns naic2##year)"



global specname1 "Baseline"
global specname2 "NAIC3"
global specname3 "NAIC3-by-Size Bins"
global specname4 "Exp. Sector"
global specname5 "Exp. Industry"
global specname6 "DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"


capture: drop emp_growth_w
winsor2 emp_growth, by(PR year) cuts(5 95)

foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 {
	qui eststo `spec'_sto: reghdfe emp_growth_w ET* $`spec'
}

esttab spec?_sto, keep(ET_post2) cells(b p)


*** Store estimates ***
local cnt = 0
foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 {
local cnt = `cnt' + 1
qui reghdfe emp_growth_w _IyeaXP* PR $`spec'
estimates save `spec'_base, replace
				
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/22 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/22 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 23
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/`cnt' {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec5", lco($c5) mco($c5) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec5", lco($c5) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec6", lco($c6) mco($c6) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec6", lco($c6) lpa(solid)) ///
		, yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2012) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7 9 11) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")  ///
		label(9 "$specname5")  ///
		label(11 "$specname6") ) ///
		yti("Effect of S936 on Employment Growth", margin(medium))
restore	
}




graph export "$output/Graphs/kevin/NETS_Overlay_Top3rd_Win10.pdf", replace  	

graph export "$output/Graphs/kevin/NETS_Overlay_Top3rd_80.pdf", replace  		




*** *** *** *** ***  *** *** ***  *** *** *** *** *** *** ***
*** *** *** Section (3): Size Control Regressions *** *** ***
*** *** *** *** ***  *** *** ***  *** *** *** *** *** *** ***

*** Deciles ***
use "$data/Replication Data/newNETS_Clean_0923", clear
drop if base_emp<10000
xtile PR_sizeDist = base_emp if PR==1, n(10)
tab PR_sizeDist if year==1995, sum(base_emp)
drop if PR_sizeDist<7


global spec1 "[aw=wgt],vce(cluster hqduns) noconst a(year emp_bins10##year)"
global spec2 "[aw=wgt],vce(cluster hqduns) noconst a(m_naic3##year emp_bins10##year)"
global spec3 "if major_sec [aw=wgt],vce(cluster hqduns) noconst a( m_naic3##year emp_bins10##year)"
global spec4 "if major_ind [aw=wgt],vce(cluster hqduns) noconst a( m_naic3##year emp_bins10##year)"

global specname1 "Baseline"
global specname2 "Size Deciles"
global specname3 "Exp. Sector"
global specname4 "Exp. Industry"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"

*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4 {
reghdfe emp_growth _IyeaXP* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/22 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/22 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 23
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/4 {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
		, yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2012) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7 ) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")   ) ///
		yti("Effect of S936 on Employment Growth", margin(medium))
restore	
}


graph export "$output/Graphs/kevin/NETS_Overlay_Deciles.pdf", replace 		






*** Terciles ***
use "$data/Replication Data/newNETS_Clean_0923", clear

global spec1 "[aw=wgt],vce(cluster hqduns) noconst a(year emp_bins3##year)"
global spec2 "[aw=wgt],vce(cluster hqduns) noconst a(m_naic3##year emp_bins3##year)"
global spec3 "if major_sec [aw=wgt],vce(cluster hqduns) noconst a( m_naic3##year emp_bins3##year)"
global spec4 "if major_ind [aw=wgt],vce(cluster hqduns) noconst a( m_naic3##year emp_bins3##year)"

global specname1 "Baseline"
global specname2 "Size Deciles"
global specname3 "Exp. Sector"
global specname4 "Exp. Industry"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"

*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4 {
reghdfe emp_growth _IyeaXP* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/22 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/22 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 23
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/4 {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
		, yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2012) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7 ) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")   ) ///
		yti("Effect of S936 on Employment Growth", margin(medium))
restore	
}

graph export "$output/Graphs/kevin/NETS_Overlay_Terciles.pdf", replace 		




*** *** *** *** ***  *** *** ***  *** *** *** *** *** *** *** ***
*** *** *** Section (4): Keep Large Firms Regressions *** *** ***
*** *** *** *** ***  *** *** ***  *** *** *** *** *** *** *** ***
use "$data/Replication Data/newNETS_Clean_0923", clear
keep if emp_bins3 == 3


** Now do DFL
xtile binDFL = base_emp if year == 1995 , n(10) 
logit PR i.naic2##i.binDFL if year == 1995
capture: drop phat min_phat w w_phat DFL?
predict phat, pr 
*winsor phat, p(.01) g(w_phat) 
*replace phat = w_phat
bys hqduns95: egen min_phat = min(phat) 
* ATOT
gen DFL2 = wgt*(PR+(1-PR)*min_phat/(1-min_phat))



tab binDFL PR if year==1995, sum(base_emp)


gen pre = (year<1995)
gen lo = (year==1995)
gen post1 = (year>1995)*(year<=2004)
gen post2 = (year>2004)
*gen post2 = (year>2003)*(year<=2006)
*gen post3 = (year>2006)

foreach var in pre post1 post2 {
	gen ET_`var' = (`var'==1)*PR
}



global level "PR"
global spec1 " [aw=wgt],vce(cluster hqduns) nocon a($level year)"
global spec2 " [aw=wgt],vce(cluster hqduns) nocon a($level naic2##year)"
global spec3 "if major_sec [aw=wgt],vce(cluster hqduns) nocon a($level naic2##year )"
global spec4 "if major_ind [aw=wgt],vce(cluster hqduns) nocon a($level naic2##year )"
global spec5 "if major_ind [aw=DFL2], vce(cluster hqduns) nocon a($level naic2##year)"



global specname1 "Baseline"
global specname2 "NAIC2"
global specname3 "Exp. Sector"
global specname4 "Exp. Industry"
global specname5 "Exp. Industry + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"



capture: drop emp_growth_w
winsor2 emp_growth, by(PR year) cuts(5 95)

foreach spec in spec1 spec2 spec3 spec4 spec5 {
	qui eststo `spec'_sto: reghdfe emp_growth_w ET* PR $`spec'
	*qui eststo `spec'_sto: reghdfe emp_growth_w _IyeaXP* PR $`spec'
}

esttab spec?_sto, keep(ET_post2) cells(b p)
*esttab spec?_sto, keep(_IyeaXPR_2006) cells(b p)



*** Store estimates ***
local cnt = 0
foreach spec in spec1 spec2 spec3 spec4 spec5 {
local cnt = `cnt' + 1
qui reghdfe emp_growth_w _IyeaXP* PR $`spec'
estimates save `spec'_base, replace
				
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/22 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/22 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 23
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/`cnt' {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec5", lco($c5) mco($c5) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec5", lco($c5) lpa(solid)) ///
		, yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2012) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7 9 ) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")  ///
		label(9 "$specname5")  ) ///
		yti("Effect of S936 on Employment Growth", margin(medium))
restore	
}


graph export "$output/Graphs/kevin/NETS_Overlay_Top3rd.pdf", replace 		

