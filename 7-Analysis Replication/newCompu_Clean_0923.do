// This file recreates Figure 5
// Author: Dan Garrett
// Date: 3-31-2020

/* Data sources:
various sources including NETS and compustat
*/

clear all
set more off
snapshot erase _all 

use "$data/Replication Data/figure5_data_KR", replace

* merging with extra tax variables
merge 1:1 gvkey year using "$WRDS/compustat_extra_tax_vars"
keep if _merge == 3
drop _merge
merge 1:1 gvkey year using "$WRDS/compustat_EBIT"
keep if _merge == 3
drop _merge


* total tax defintion
sort gvkey year
gen tottax = 100 * txt / pi

gen pre_ftax = ftax if year < 1996
gen pre_tottax = tottax if year < 1996
bys gvkey: egen ftax_base = mean(pre_ftax)
bys gvkey: egen tottax_base = mean(pre_tottax)
gen ftax_d = ftax - ftax_base
gen tottax_d = tottax - tottax_base

* combining years and N2
egen yearg = group(year n2)

* Make balanced panel!
gsort gvkey year
local balance = 0 // 0 if FALSE, 1 if TRUE
if `balance' == 1 {
// 	bys gvkey: gen firm_survival = _N
// 	keep if firm_survival == 16 // 46,960 left out of 115,224
// 	drop firm_survival
	by gvkey: gen firm_9095survival = _n if year > 1990 & year <= 1995 & !missing(at)
	by gvkey: egen firm_survival = max(firm_9095survival)
	drop firm_9095survival
	drop if firm_survival != 5
}

*figures 5, 6, A5, and A6
xi i.year|PR , noomit
drop _IyeaXPR_1995 

forval i=1990(1)2014 { 
	capture: label var _IyeaXPR_`i' " "
} 
forval i=1991(2)2014 { 
	capture: label var _IyeaXPR_`i' "`i'"
} 
 

 
* Capex/Capxs/ppe/ppent decile by gvkey
foreach x of var capxs capex ppe ppent at intan {
	local start_year=90
	local end_year=95
	local new_var "`x'_`start_year'`end_year'"
	local new_var_bin "`new_var'_bins10"
	tempvar temp1 temp2
	gen `temp1' = `x' * (year<=1900+`end_year' & year >= 1900+`start_year')
	egen `temp2' = mean(`temp1') if `temp1' != 0, by(gvkey)
	egen `new_var' = max(`temp2'), by(gvkey)
	xtile `new_var_bin' = `new_var', n(10)
}

// g logAT_9095 = log(at_9095)


// * truncation
// summ logAT_9095,d
// drop if logAT_9095 < r(p5)
// drop if logAT_9095 > r(p95)

* 9/1 weighted average of 1992-1995
gen temp1a = 0.1*at if year==1992
gen temp2a = 0.2*at if year==1993
gen temp3a = 0.3*at if year==1994
gen temp4a = 0.4*at if year==1995
forvalues i = 1/4 {
       egen temp`i'b = max(temp`i'a), by(gvkey)
}

gen at_9095wgt = temp1b + temp2b + temp3b + temp4b
drop temp*
g logAT_9095wgt = log(at_9095wgt)




* 9/1 exercies: log diff of earliest appearing year - 1995 log asset
	bys gvkey: egen first_yr = min(year) if !missing(at) & at != 0
	bys gvkey: egen first_year = min(first_yr)
	drop first_yr
	g first_at = at if year == first_year
	bys gvkey: egen at_base = max(first_at)
	drop first_at
	g at_95 = at if year == 1995
	bys gvkey: egen at95 = max(at_95)
	drop at_95
	xtile at95_bins10 = at95, n(10)
	xtile at95_bins5 = at95, n(5)
	xtile at95_bins3 = at95, n(3)
	
	g ppe_95 = ppent if year == 1995
	bys gvkey: egen ppe95 = max(ppe_95)
	drop ppe_95
	xtile ppe95_bins10 = ppe95, n(10)
	xtile ppe95_bins5 = ppe95, n(5)
	xtile ppe95_bins3 = ppe95, n(3)	

**** Exposed industry/sector indicators
gen exp_sec = inlist(n2,3,5,6,7,11,12)
gen exp_ind = ( ~inlist(n3,"324","323","337","322","327","336","331","321"))*exp_sec

**** Control versions ****
gen lninv = log(capx)
gen lnat = log(at)
	gen ppe_scaled = ppe*1000
	sum ppe_scaled ppent
gen lnppe = log(ppe)
gen lnppe2 = log(ppe_scaled)
gen lnppent = log(ppent)
gen lnintan = log(intan)
gen tangibility = ppent/at


*** limit bins to non-first tercile
xtile ppe95_bins5_ALT = ppe95 if ppe95_bins3>1, n(5)	
xtile ppe95_bins10_ALT = ppe95 if ppe95_bins3>1, n(10)	


*** Alternative DFL weights ***
* some DFL weights
xtile ppe95_20 = ppe if year == 1995 , n(20) 
destring n3, replace

logit PR i.n3 i.ppe95_20 if year == 1995

capture: drop phat min_phat w w_phat
predict phat, pr 
winsor phat, p(.01) g(w_phat) 
replace phat = w_phat
bys gvkey: egen min_phat = min(phat) 
* ATE
* gen DFL = (PR/min_phat+(1-PR)/(1-min_phat))
* ATOT
gen DFL10 = (PR+(1-PR)*min_phat/(1-min_phat))


*** Things to do:
	* Bins
	* Baseline + Overlays
	* Compare DFL weights

drop __0*

* Impute EINs
/*
replace ein = subinstr(ein,"-","",1)
gen ein_fix = ein

sort gvkey year
by gvkey: replace ein_fix=ein_fix[_n-1] if ein_fix==""
gsort gvkey -year
by gvkey: replace ein_fix=ein_fix[_n-1] if ein_fix==""
gen ein_flag =(ein=="")
drop ein
rename ein_fix ein

duplicates tag ein year, gen(dup_tag) // tag duplicates
*/

* ETR stuff *
winsor ftax, gen(ftax_w) p(0.05)

*** Triple differences (DELETE LATER) ***
gen fakehaven = ppe>0.25
reghdfe capex_base _IyeaX*##fakehaven PR , cl(gvkey) a(gvkey year)

* Table inputs to produce:
* (1) Two rows, report baseline long difference (LD) and interaction LD
* (2) Couple of specification (test baseline and sector-by-PPE bins) - less important
* (3) Two versions:
	* Interaction 1: "anyhaven" pre 1995
	* Interaction 2: "anyhaven" post 1995
* (4) Outcome: capex_base
* (5) New foreign investment outcome	

save "$data/Replication Data/newCompu_Clean_0923", replace







*** Export version for IRS Server ***
use "$data/Replication Data/newCompu_Clean_0923", clear


*** Generate sub-sample ***
xtile PR_sizeDist = ppe95 if PR==1, n(10)

keep gvkey ein PR year idbflag fic ppe95 ppe_9095 ppe95_bins10 n2 n3 PR_sizeDist capex_base lnppent lnat lninv fs_capx
sort gvkey year
order gvkey ein* year n2 n3 idbflag fic PR*

descr


tab PR_sizeDist if ein==""
tab ppe95_bins10 PR if ein==""


export delimited using "$data/Replication Data/compuPR_forIRS_Updated.csv", replace



*** Code for Clare ***
import delimited using "$data/Replication Data/compuPR_forIRS_Updated.csv", clear

* (0) Delete duplicate EINs for comparison w/ IRS
drop if ein==.
duplicates tag ein year, gen(dup_tag)
egen dup_flag = max(dup_tag), by(gvkey)
tab dup_flag
drop if dup_flag > 0
*/

* (1) Select sample *
drop if pr==0 & idbflag=="D" // drop undesired control firms
drop if pr_sizedist<7 // drop undesired treatment firms


* (2) create event study covariates
xi i.year|pr , noomit
drop _IyeaXpr_1995 


* (3) run event studies
global spec1 ", cl(gvkey) a(ppe95_bins10##year)"
global spec2 ", cl(gvkey) a(n2##year ppe95_bins10##year)"
global spec3 ", cl(gvkey) a(n2##ppe95_bins10##year)"

 foreach spec in spec1 spec2 spec3  {
	* Estimate event study model
	reghdfe capex_base _IyeaX* pr $`spec'
 }












