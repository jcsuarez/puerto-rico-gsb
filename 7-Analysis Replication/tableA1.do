// This file recreates Table A1
// Author: Dan Garrett
// Date: 4-25-2020

/* Data sources:
SOI
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data/tableA1_data", clear

est clear

eststo reg_1: reg ETR pos_cred i.year i.id [aw=ISTT ], cl(id) 
	estadd local hasy "Yes" 
	estadd local hasid "Yes"
	estadd local hasistt "Yes"

eststo reg_2: reg ETR pos_cred i.year i.id  Net_Income Receipts [aw=ISTT ], cl(id) 
	estadd local hasy "Yes" 
	estadd local hasid "Yes"
	estadd local hasistt "Yes"
	estadd local hasni "Yes"

eststo reg_3: reg ETR pos_cred i.year i.id  Net_Income Receipts [aw=Net_Income_base ], cl(id) 
	estadd local hasy "Yes" 
	estadd local hasid "Yes"
	estadd local hasistt ""
	estadd local hasni "Yes"
	estadd local hasnib "Yes"

eststo reg_4: reg d_ETR d_pos_cred i.year  [aw=ISTT ], cl(id) 
	estadd local hasy "Yes" 
	estadd local hasid ""
	estadd local hasistt "Yes"
	estadd local hasdif "Yes"

eststo reg_5: reg d_ETR d_pos_cred i.year  Net_Income Receipts [aw=ISTT ], cl(id) 
	estadd local hasy "Yes" 
	estadd local hasid ""
	estadd local hasistt "Yes"
	estadd local hasdif "Yes"
	estadd local hasni "Yes"

eststo reg_6: reg d_ETR d_pos_cred i.year  Net_Income Receipts [aw=Net_Income_base ], cl(id) 
	estadd local hasy "Yes" 
	estadd local hasid ""
	estadd local hasistt ""
	estadd local hasdif "Yes"
	estadd local hasni "Yes"
	estadd local hasnib "Yes"


** outputting table weighted by taxable income
esttab reg_? using "$output/Tables/tableA1.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons nonum posthead(" ") ///
prefoot("") ///
postfoot(" ") ///

esttab reg_? using "$output/Tables/tableA1.tex", keep(pos_cred d_pos_cred)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s() /// 
coeflabel(d_pos_cred "\hspace{1em}Change in Possession Credits" pos_cred "\hspace{1em}Possession Credits") ///
preh("") noobs scalars(  "hasy Year Fixed Effects" ///
"hasid Industry Fixed Effects" "hasni Net Income and Sales Controls"  "hasdif Regression in First Differences" ///
"hasistt Taxable Income Weights" "hasnib Net Income Weights" ) label /// 
 prefoot("\hline") ///
postfoot( ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex)		