// This file recreates Table 5
// Author: Dan Garrett
// Date: 4-27-2020

/* Data sources:
NETS and ASM
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data/table5_data", clear

est clear 
*keep if base_numemp<500000
**** Regression on Capx
eststo reg_capx_1: reghdfe capexT_growth pr_link_post c.pr_link_i ,  a(i.year i.industry_cd ) vce(cluster fips)	
	count if e(sample) 
	estadd scalar N_uw = r(N)
	estadd local hasy "Yes"
	estadd local hasind "Yes"

eststo reg_capx_2: reghdfe capexT_growth  pr_link_post c.pr_link_i ,  a(i.year i.fips i.industry_cd ) vce(cluster fips)	
	count if e(sample) 
	estadd scalar N_uw = r(N)
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassta "Yes"	
	
eststo reg_capx_3: reghdfe capexT_growth pr_link_post c.pr_link_i ,  a(i.pid i.year) vce(cluster fips)	
	count if e(sample) 
	estadd scalar N_uw = r(N)
	estadd local hasy "Yes"
	estadd local hassta_i "Yes"

eststo reg_capx_4: reghdfe capexT_growth pr_link_post c.pr_link_i ,  a(i.pid i.year#i.fips  ) vce(cluster fips)	
	count if e(sample) 
	estadd scalar N_uw = r(N)
	estadd local hasy_s "Yes"
	estadd local hassta_i "Yes"

eststo reg_capx_5: reghdfe capexT_growth pr_link_post c.pr_link_i ,  a(i.pid i.year#i.industry_cd ) vce(cluster fips)	
	count if e(sample) 
	estadd scalar N_uw = r(N)
	estadd local hasy_i "Yes"
	estadd local hassta_i "Yes"
	
esttab reg*capx* using "$output/Tables/table5.tex", drop(*) stats() ///
b(3) se par label coll(none)  ///
noobs nogap nomtitle tex replace nocons postfoot("") nonum posthead("")

esttab reg*capx* using "$output/Tables/table5.tex", keep(pr_link_post)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) noobs scalars( "N_uw Observations" "hasy Year Fixed Effects" "hasind Industry Fixed Effects" ///
"hassta State Fixed Effects" "hassta_i State X Industry Fixed Effects"   "hasy_s Year X State Fixed Effects" "hasy_i Year X Industry Fixed Effects") label /// 
coeflabel(pr_link_post "\hspace{1em}Exposure to Section 936 X Post"  post_mai_pr "\hspace{1em}I(Main Industry) X Exposure to Section 936" post_oth_pr "\hspace{1em}I(Other Industry) X Exposure to Section 936") ///
preh("\multicolumn{1}{l}{\textbf{Growth in Capital Expenditures}}") postfoot( ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex)

