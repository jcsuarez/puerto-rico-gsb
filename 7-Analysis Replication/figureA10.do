// This file recreates Figure A10
// Author: Dan Garrett
// Date: 4-20-2020

/* Data sources:
Nets
*/

* starting point for all panels is 3-wrds_segments/DFL_nets_emp_2019_06_11_dan

clear all
set more off
snapshot erase _all 

// Define Command for ES graph
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}
end 

*******************************************************
* employment graph at NETS firm level
*******************************************************
use "$data/Replication Data/figureA10_data", replace

xi i.year|PR, noomit 
drop _IyeaXP*1995

* employment
est clear
eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
   [aw=wgt] , a(i.year)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"    

ES_graph , level(95) 

rename es_b1 es_0
rename time1 t_0
rename es_ci_l1 es_l0
rename es_ci_h1 es_h0 

eststo emp_1:  reghdfe emp_growth _IyeaXP*  ///
   [aw=wgt] , a(i.year#i.naic2)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"    

ES_graph , level(95) 

rename es_b1 es_1
rename time1 t_1
rename es_ci_l1 es_l1
rename es_ci_h1 es_h1 

eststo emp_2:  reghdfe emp_growth _IyeaXP*  ///
  if major_sec  [aw=wgt] , a(i.year#i.naic2)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"      

ES_graph , level(95) 

rename es_b1 es_2
rename time1 t_2
rename es_ci_l1 es_l2
rename es_ci_h1 es_h2  

eststo emp_3:  reghdfe emp_growth _IyeaXP* ///
  if major_ind [aw=wgt] , a(i.year#i.naic2)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"     

ES_graph , level(95) 

rename es_b1 es_3
rename time1 t_3
rename es_ci_l1 es_l3
rename es_ci_h1 es_h3  

eststo emp_4:  reghdfe emp_growth _IyeaXP* ///
  if major_ind [aw=w] , a(i.year#i.naic2)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"       
  
ES_graph , level(95) 

rename es_b1 es_4
rename time1 t_4
rename es_ci_l1 es_l4
rename es_ci_h1 es_h4  

replace t_0 = t_0 - 0.2
replace t_1 = t_1 - 0.1
replace t_3 = t_3 + 0.1
replace t_4 = t_4 + 0.2
 graph twoway (scatter es_0 t_0, lcolor(maroon) connect(line) mcolor(maroon) msymbol(plus) yline(0 , lcolor(red)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
				(rcap es_l0 es_h0 t_0, lcolor(maroon) lpattern(dash)) ///
				(scatter es_1 t_1, lcolor(navy) connect(line) mcolor(navy) msymbol(circle) msize(small)) ///
				(rcap es_l1 es_h1 t_1, lcolor(navy) lpattern(dash)) ///
				(scatter es_2 t_2, lcolor(orange) connect(line) mcolor(orange) msymbol(square) msize(small)) ///
				(rcap es_l2 es_h2 t_2, lcolor(orange) lpattern(dash)) ///
				(scatter es_3 t_3, lcolor(green) connect(line) mcolor(green) msymbol(diamond) msize(small)) ///
				(rcap es_l3 es_h3 t_3, lcolor(green) lpattern(dash)) ///
				(scatter es_4 t_4, lcolor(black) connect(line) mcolor(black) msymbol(triangle) msize(small)) ///
				(rcap es_l4 es_h4 t_4, lcolor(black) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white))  ///
	xtitle("Year") xlab(1990(5)2007) bgcolor(white) ///
	legend(on order (1 2 3 4 5 6 7 8 9 10) label(1 "All Firms")  ///
	label(2 "95% CI") label(3 "All firms with FE")  ///
	label(4 "95% CI") label(5 "Major Sectors with FE")  ///
	label(6 "95% CI") label(7 "Major Industries with FE")  ///
	label(8 "95% CI") label(9 "Major Industries + DFL with FE")  ///
	label(10 "95% CI") r(5)) ///
	yti("Effect of S936 on Firm Employment", margin(medium))
graph export "$output/Graphs/figureA10.pdf", replace 