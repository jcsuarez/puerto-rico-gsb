// This file recreates Figure A8, 3-yr average Ftax estimates
// Author: Dan Garrett
// Date: 4-20-2020

/* Data sources:
various sources including NETS and compustat
*/

clear all
set more off

*******************************************************
* Panels A and B
*******************************************************
use "$data/Replication Data/figureA8_data", replace

* combining years and N2
egen yearg = group(year n2)

*figures 5, 6, A5, and A6
xi i.year|PR , noomit
drop _IyeaXPR_1995 

forval i=1990(1)2014 { 
	capture: label var _IyeaXPR_`i' " "
} 
forval i=1991(2)2014 { 
	capture: label var _IyeaXPR_`i' "`i'"
} 
snapshot save
 
***************************************************
* figures A7, all panels
***************************************************

* running the regression
reghdfe ftax_1 _IyeaX* PR IHS_rev log_tot_emp i.year [aw=DFL] ///
 if  ~inlist(n3,"324","323","337","322","327","336","331","321") & ///
 inlist(n2,3,5,6,7,11,12) & pi > 4, cl(gvkey)   a(i.year#i.n2)
 
*initiating the matricies 
mat b = 0 
mat b_ub = 0 
mat b_lb = 0 
mat time = 0 

*preperiod
lincom (_IyeaXPR_1991 +_IyeaXPR_1992+_IyeaXPR_1993)/3, l(90)
mat b = (b,r(estimate))
mat b_ub = (b_ub,r(ub))
mat b_lb = (b_lb,r(lb))
mat time = (time,1)

lincom (_IyeaXPR_1992+_IyeaXPR_1993+_IyeaXPR_1994)/3, l(90)
mat b = (b,r(estimate))
mat b_ub = (b_ub,r(ub))
mat b_lb = (b_lb,r(lb))
mat time = (time,2)

lincom (_IyeaXPR_1993+_IyeaXPR_1994 + 0)/3, l(90)
mat b = (b,r(estimate))
mat b_ub = (b_ub,r(ub))
mat b_lb = (b_lb,r(lb))
mat time = (time,3)

lincom (_IyeaXPR_1994 + 0)/ 2, l(90)
mat b = (b,r(estimate))
mat b_ub = (b_ub,r(ub))
mat b_lb = (b_lb,r(lb))
mat time = (time,4)

* 1995
mat b = (b,0)
mat b_ub = (b_ub,0)
mat b_lb = (b_lb,0)
mat time = (time,5)

* postperiod
lincom (_IyeaXPR_1996+_IyeaXPR_1997+_IyeaXPR_1998)/3, l(90)
mat b = (b,r(estimate))
mat b_ub = (b_ub,r(ub))
mat b_lb = (b_lb,r(lb))
mat time = (time,6)

lincom (_IyeaXPR_1997+_IyeaXPR_1998+_IyeaXPR_1999)/3, l(90)
mat b = (b,r(estimate))
mat b_ub = (b_ub,r(ub))
mat b_lb = (b_lb,r(lb))
mat time = (time,7)

lincom (_IyeaXPR_1998+_IyeaXPR_1999+_IyeaXPR_2000)/3, l(90)
mat b = (b,r(estimate))
mat b_ub = (b_ub,r(ub))
mat b_lb = (b_lb,r(lb))
mat time = (time,8)

lincom (_IyeaXPR_1999+_IyeaXPR_2000+_IyeaXPR_2001)/3, l(90)
mat b = (b,r(estimate))
mat b_ub = (b_ub,r(ub))
mat b_lb = (b_lb,r(lb))
mat time = (time,9)

lincom (_IyeaXPR_2000+_IyeaXPR_2001+_IyeaXPR_2002)/3, l(90)
mat b = (b,r(estimate))
mat b_ub = (b_ub,r(ub))
mat b_lb = (b_lb,r(lb))
mat time = (time,10)

lincom (_IyeaXPR_2001+_IyeaXPR_2002+_IyeaXPR_2003)/3, l(90)
mat b = (b,r(estimate))
mat b_ub = (b_ub,r(ub))
mat b_lb = (b_lb,r(lb))
mat time = (time,11)

lincom (_IyeaXPR_2002+_IyeaXPR_2003+_IyeaXPR_2004)/3, l(90)
mat b = (b,r(estimate))
mat b_ub = (b_ub,r(ub))
mat b_lb = (b_lb,r(lb))
mat time = (time,12)

lincom (_IyeaXPR_2003+_IyeaXPR_2004+_IyeaXPR_2005)/3, l(90)
mat b = (b,r(estimate))
mat b_ub = (b_ub,r(ub))
mat b_lb = (b_lb,r(lb))
mat time = (time,13)

lincom (_IyeaXPR_2004+_IyeaXPR_2005+_IyeaXPR_2006)/3, l(90)
mat b = (b,r(estimate))
mat b_ub = (b_ub,r(ub))
mat b_lb = (b_lb,r(lb))
mat time = (time,14)

lincom (_IyeaXPR_2005+_IyeaXPR_2006)/2, l(90)
mat b = (b,r(estimate))
mat b_ub = (b_ub,r(ub))
mat b_lb = (b_lb,r(lb))
mat time = (time,15)

lincom (_IyeaXPR_2006), l(90)
mat b = (b,r(estimate))
mat b_ub = (b_ub,r(ub))
mat b_lb = (b_lb,r(lb))
mat time = (time,16)

*putting the matrices into variables for graphing
mat b = b[1,2...]'
mat b_lb = b_lb[1,2...]'
mat b_ub = b_ub[1,2...]'
mat time = time[1,2...]'

capture: drop b1 b_lb1 b_ub1 time1
svmat b
svmat b_lb
svmat b_ub
svmat time

*labeling the years
label define fig_yearl 1 "1991" 2 "1992" 3 "1993" 4 "1994" 5 "1995" 6 "1996" 7 "1997" 8 "1998" 9 "1999" 10 "2000" 11 "2001" 12 "2002" 13 "2003" 14 "2004" 15 "2005" 16 "2006", replace
label values time fig_yearl

*outputting the figure
graph twoway (scatter b1 time, lcolor(navy) mcolor(navy) yline(0 , lcolor(red)) xline(5, lcolor(black) lpattern(solid)) msize(small)) ///
    (rcap b_lb1 b_ub1 time, lcolor(navy) lpattern(solid)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white))  ///
	xtitle("Year") xlab(1(2)16,val) bgcolor(white) ///
	legend(on order (1 2) label(1 "Puerto Rico Presence")  ///
	label(2 "90% Confidence Interval") r(1)) ///
	yti("Effect of S936 on Effective Federal Tax Rate", margin(medium))
graph export "$output/Graphs/graph_ftax_ES_smooth.pdf", replace 