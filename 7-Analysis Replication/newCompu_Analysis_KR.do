// This file creates new versions of Figure 5
// Author: Kevin Roberts


clear all
set more off
snapshot erase _all 
	
	
*ssc install sdid, replace
	

*** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
*** *** *** Section (1): New main specification *** *** ***
*** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
use "$data/Replication Data/newCompu_Clean_0923", replace
tab idbflag PR if year==1995
tab fic
drop if PR==0 & idbflag=="D"
*drop if dup_tag>0

xtile PR_sizeDist = ppe95 if PR==1, n(3)
xtile PR_sizeDist10 = ppe95 if PR==1, n(10)

tab PR if year==1995, sum(ppe95)
tab PR_sizeDist if year==1995, sum(ppe95)
*drop if PR_sizeDist<3
*keep if PR_sizeDist10>6 | PR_sizeDist10==.
drop if PR_sizeDist10<7
*drop if PR_sizeDist10>6 & PR_sizeDist10<10000

gen temp1 = (year==1991)
gen temp2 = (year==2006)
egen pre_bal = max(temp1), by(gvkey)
egen post_bal = max(temp2), by(gvkey)
drop temp?
tab year PR if pre_bal
*keep if pre_bal & post_bal

* some DFL weights
xtile ppe95_binDFL = ppent if year == 1995 , n(10) 
logit PR i.n2##i.ppe95_binDFL if year == 1995
capture: drop phat min_phat w w_phat
predict phat, pr 
*winsor phat, p(.01) g(w_phat) 
*replace phat = w_phat
bys gvkey: egen min_phat = min(phat) 
* ATOT
gen DFL2 = (PR+(1-PR)*min_phat/(1-min_phat))


global spec1 ", cl(gvkey) a(ppe95_bins10##year)"
global spec2 "[aw=DFL2], cl(gvkey) a(ppe95_bins10##year)"
global spec3 "[aw=DFL2], cl(gvkey) a(gvkey n2##year ppe95_bins10##year)"
global spec4 "[aw=DFL2], cl(gvkey) a(n2##year ppe95_bins10##year)"

global specname1 "95 PPE: Baseline"
global specname2 "95 PPE: Baseline + DFL"
*global specname2 "95 PPE: Baseline + Sector FE"
global specname3 "95 PPE: Firm + Sector FE + DFL"
global specname4 "95 PPE: Sector FE + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"

*** Store estimates ***
local cnt = 0
foreach spec in spec1 spec2 spec3 spec4 {
local cnt = `cnt' + 1
reghdfe capex_base _IyeaX* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}

*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/`cnt' {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 , yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7 ) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3") ///
		label(7 "$specname4")  ) ///
		yti("Effect of S936 Exposure on Investment", margin(medium))
restore	
}

*graph export "$output/Graphs/kevin/TEST.pdf", replace 	
		
	
	
*** *** *** *** *** *** *** *** *** *** *** *** ***
*** *** *** Section (2): Synthetic DiD  *** *** ***
*** *** *** *** *** *** *** *** *** *** *** *** ***
use "$data/Replication Data/newCompu_Clean_0923", replace
tab idbflag PR if year==1995
tab fic
drop if PR==0 & idbflag=="D"
*drop if dup_tag>0

xtile PR_sizeDist = ppe95 if PR==1, n(3)
xtile PR_sizeDist10 = ppe95 if PR==1, n(10)

tab PR if year==1995, sum(ppe95)
tab PR_sizeDist if year==1995, sum(ppe95)
drop if PR_sizeDist10<7
	
gen temp1 = (year==1991)
gen temp2 = (year==2006)
egen pre_bal = max(temp1), by(gvkey)
egen post_bal = max(temp2), by(gvkey)
drop temp?
tab year PR if pre_bal
keep if pre_bal & post_bal
tab year

gen temp1 = (capex_base!=.)
egen years = sum(temp1), by(gvkey)
keep if years==16

tab year PR, sum(capex_base)
tab year, sum(ppent)
tab year, sum(lninv)

gen temp2 = (ppent!=.)
egen years2 = sum(temp2), by(gvkey)
keep if years2==16

gen temp3 = (lninv!=.)
egen years3 = sum(temp3), by(gvkey)
keep if years3==16
*
sdid capex_base gvkey year PR_post, vce(placebo) graph
sdid capex_base gvkey year PR_post, covariates(ppent) vce(placebo) graph
sdid lninv gvkey year PR_post, covariates(ppent) vce(placebo) graph

	
	
	
	
	
	
	
	
	
	
/*		
		
*** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
*** *** *** Section (1): New main specification *** *** ***
*** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
use "$data/Replication Data/newCompu_Clean_0923", replace
tab idbflag PR if year==1995
tab fic
drop if PR==0 & idbflag=="D"

xtile PR_sizeDist = ppe95 if PR==1, n(3)
xtile PR_sizeDist10 = ppe95 if PR==1, n(10)

tab PR if year==1995, sum(ppe95)
tab PR_sizeDist if year==1995, sum(ppe95)
*drop if PR_sizeDist<3
drop if PR_sizeDist10<7

* some DFL weights
xtile ppe95_binDFL = ppent if year == 1995 , n(10) 
logit PR i.n2##i.ppe95_binDFL if year == 1995
capture: drop phat min_phat w w_phat
predict phat, pr 
winsor phat, p(.01) g(w_phat) 
replace phat = w_phat
bys gvkey: egen min_phat = min(phat) 
* ATE
* gen DFL = (PR/min_phat+(1-PR)/(1-min_phat))
* ATOT
gen DFL2 = (PR+(1-PR)*min_phat/(1-min_phat))

global outcome "fs_capx"

global spec1 ", cl(gvkey) a(year)"
global spec2 ", cl(gvkey) a(ppe95_bins10##year)"
global spec3 ", cl(gvkey) a(n2##year ppe95_bins10##year)"
global spec4 ", cl(gvkey) a(gvkey n2##year ppe95_bins10##year)"

global specname1 "No Controls"
global specname2 "95 PPE: Baseline"
global specname3 "95 PPE: Sector FE"
global specname4 "95 PPE: Firm + Sector FE"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"

/*
foreach spec in spec1 spec2 spec3 spec4 {
	reghdfe capex_base _IyeaX* PR $`spec'
	reghdfe lninv _IyeaX* PR $`spec'
	reghdfe fs_capx _IyeaX* PR $`spec'	
}
*/


*** Store estimates ***
local cnt = 0
foreach spec in spec1 spec2 spec3 spec4 {
local cnt = `cnt' + 1
reghdfe $outcome _IyeaX* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}

*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/`cnt' {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 , yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7 ) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3") ///
		label(7 "$specname4")  ) ///
		yti("Effect of S936 Exposure on Investment", margin(medium))
restore	
}

*graph export "$output/Graphs/kevin/TEST.pdf", replace 	
	
	
	

*** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
*** *** *** Section (1): New main specification *** *** ***
*** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
use "$data/Replication Data/newCompu_Clean_0923", replace
tab idbflag PR if year==1995
tab fic
drop if PR==0 & idbflag=="D"
*drop if dup_tag>0

xtile PR_sizeDist = ppe95 if PR==1, n(3)
xtile PR_sizeDist10 = ppe95 if PR==1, n(10)

tab PR if year==1995, sum(ppe95)
tab PR_sizeDist if year==1995, sum(ppe95)
*drop if PR_sizeDist<3
drop if PR_sizeDist10<7

* some DFL weights
xtile ppe95_binDFL = ppent if year == 1995 , n(10) 
logit PR i.n2##i.ppe95_binDFL if year == 1995
capture: drop phat min_phat w w_phat
predict phat, pr 
*winsor phat, p(.01) g(w_phat) 
*replace phat = w_phat
bys gvkey: egen min_phat = min(phat) 
* ATOT
gen DFL2 = (PR+(1-PR)*min_phat/(1-min_phat))


global spec1 ", cl(gvkey) a(ppe95_bins10##year)"
global spec2 "[aw=DFL2], cl(gvkey) a(gvkey ppe95_bins10##year)"
global spec3 "[aw=DFL2], cl(gvkey) a(gvkey n2##year ppe95_bins10##year)"
global spec4 "[aw=DFL2], cl(gvkey) a(gvkey n2##year ppe95_bins10##year)"

global specname1 "95 PPE: Baseline"
global specname2 "95 PPE: Baseline + DFL"
*global specname2 "95 PPE: Baseline + Sector FE"
global specname3 "95 PPE: Firm + Sector FE + DFL"
global specname4 "95 PPE: Sector FE + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"

local outcome "lninv"

*** Store estimates ***
local cnt = 0
foreach spec in spec1 spec2 spec3 spec4 {
local cnt = `cnt' + 1
reghdfe `outcome' _IyeaX* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}

*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/`cnt' {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 , yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7 ) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3") ///
		label(7 "$specname4")  ) ///
		yti("Effect of S936 Exposure on Investment", margin(medium))
restore	
}

*graph export "$output/Graphs/kevin/TEST.pdf", replace 	
					
	