// This file recreates Figure 8
// Author: Dan Garrett
// Date: 4-20-2020

/* Data sources:
Nets
*/

* starting point for all panels is 3-wrds_segments/DFL_nets_emp_2019_06_11_dan

clear all
set more off
snapshot erase _all 

// Define Command for ES graph
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(solid)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Estimate") label(2 "`level'% CI")) ///
	ytitle("`yti'") ///
	note("`note'") ylab("`ylab'")
graph export "`outname'.pdf", replace	

end 

*******************************************************
* employment graph at NETS firm level
*******************************************************
use "$data/Replication Data/figure8_data", replace

xi i.year|PR, noomit 
drop _IyeaXP*1995

* employment
est clear
eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
   [aw=wgt] , a(i.year)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"    

ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
outname("$output/Graphs/NETS_firm_DFL_06_11_2019_r0") ylab("-.12(.02).04")  
