// This file recreates Figure A11
// Author: Dan Garrett
// Date: 4-20-2020

/* Data sources:
Nets
*/

* starting point for all panels is 3-wrds_segments/DFL_nets_emp_2019_06_11_dan

clear all
set more off

// Define Command for ES graph
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(solid)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Estimate") label(2 "`level'% CI")) ///
	ytitle("`yti'") ///
	note("`note'") ylab("`ylab'")
graph export "`outname'.pdf", replace	

end 

*******************************************************
* employment graph at NETS firm level
*******************************************************
use "$data/Replication Data/figureA11_data", replace

xi i.year|PR, noomit 
drop _IyeaXP*1995

* establishments
eststo est_0:  reghdfe est_growth _IyeaXP*  ///
   [aw=wgt] , a(i.year)  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"    

ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Est{sub:it}-Est{sub:i1995})/Est{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
outname("$output/Graphs/NETS_firm_est") ylab("-.08(.02).04")  

/* actual in paper

. eststo est_0:  reghdfe est_growth _IyeaXP*  ///
>    [aw=wgt] , a(i.year)  vce(cluster hqduns) noconst      
(MWFE estimator converged in 1 iterations)

HDFE Linear regression                            Number of obs   =    383,893
Absorbing 1 HDFE group                            F(  22,  16423) =       3.27
Statistics robust to heteroskedasticity           Prob > F        =     0.0000
                                                  R-squared       =     0.5852
                                                  Adj R-squared   =     0.5851
                                                  Within R-sq.    =     0.0006
Number of clusters (hqduns95) =     16,424        Root MSE        =     0.1620

                           (Std. Err. adjusted for 16,424 clusters in hqduns95)
-------------------------------------------------------------------------------
              |               Robust
   est_growth |      Coef.   Std. Err.      t    P>|t|     [95% Conf. Interval]
--------------+----------------------------------------------------------------
_IyeaXPR_1990 |  -.0013949   .0092153    -0.15   0.880    -.0194579     .016668
_IyeaXPR_1991 |  -.0028007   .0090037    -0.31   0.756    -.0204488    .0148475
_IyeaXPR_1992 |  -.0042637   .0087897    -0.49   0.628    -.0214925    .0129651
_IyeaXPR_1993 |  -.0059721   .0044261    -1.35   0.177    -.0146478    .0027036
_IyeaXPR_1994 |  -.0049243   .0032072    -1.54   0.125    -.0112107    .0013621
_IyeaXPR_1996 |  -.0051422   .0037236    -1.38   0.167    -.0124408    .0021564
_IyeaXPR_1997 |   -.008426   .0065774    -1.28   0.200    -.0213185    .0044665
_IyeaXPR_1998 |  -.0072557    .007607    -0.95   0.340    -.0221662    .0076549
_IyeaXPR_1999 |  -.0061858   .0089215    -0.69   0.488    -.0236728    .0113013
_IyeaXPR_2000 |  -.0004774    .009618    -0.05   0.960    -.0193298    .0183749
_IyeaXPR_2001 |  -.0036773   .0102786    -0.36   0.721    -.0238244    .0164698
_IyeaXPR_2002 |  -.0051096   .0106593    -0.48   0.632     -.026003    .0157837
_IyeaXPR_2003 |  -.0186235   .0111853    -1.67   0.096    -.0405479    .0033008
_IyeaXPR_2004 |  -.0238577   .0114115    -2.09   0.037    -.0462256   -.0014899
_IyeaXPR_2005 |  -.0259289   .0123068    -2.11   0.035    -.0500515   -.0018062
_IyeaXPR_2006 |  -.0265387   .0120751    -2.20   0.028    -.0502073   -.0028701
_IyeaXPR_2007 |  -.0271501   .0117933    -2.30   0.021    -.0502663   -.0040339
_IyeaXPR_2008 |  -.0340883   .0115272    -2.96   0.003    -.0566828   -.0114937
_IyeaXPR_2009 |  -.0355485    .011293    -3.15   0.002     -.057684   -.0134129
_IyeaXPR_2010 |  -.0466616   .0117852    -3.96   0.000    -.0697619   -.0235612
_IyeaXPR_2011 |  -.0452693   .0114671    -3.95   0.000     -.067746   -.0227926
_IyeaXPR_2012 |  -.0469163   .0107388    -4.37   0.000    -.0679655   -.0258672

