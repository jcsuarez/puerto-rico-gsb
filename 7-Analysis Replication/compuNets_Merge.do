// This file recreates Figure 5
// Author: Dan Garrett
// Date: 3-31-2020

/* Data sources:
various sources including NETS and compustat
*/

clear all
set more off
snapshot erase _all 

*** *** *** *** ***  *** *** *** *** *** 
*** *** *** Compustat sample *** *** *** 
*** *** *** *** ***  *** *** *** *** *** 
use "$data/Replication Data/newCompu_Clean_0923", clear

* Drop some control firms
drop if PR==0 & idbflag=="D"

* Drop some treated firms
xtile PR_sizeDist10 = ppe95 if PR==1, n(10)
drop if PR_sizeDist10<7

merge n:n gvkey using "$data/Replication Data/hqduns_gvkeys_20201016.dta"
keep if _merge==3
drop _merge
merge 1:1 hqduns95 using "$data/Replication Data/figure8_data_EY_081523"


*** *** *** *** *** *** *** *** *** 
*** *** *** NETS sample *** *** ***
*** *** *** *** *** *** *** *** ***
use "$data/Replication Data/figure8_data_EY_081523", clear
merge n:n hqduns95 using "$data/Replication Data/hqduns_gvkeys_20201016.dta"
keep if _merge==3
drop _merge

merge n:1 gvkey year using "$data/Replication Data/newCompu_Clean_0923"
keep if _merge==3
tab year PR

* Drop some control firms
*drop if PR==0 & idbflag=="D"


drop ppe95_bins10
xtile ppe95_bins10 = ppe95, n(10) // I was doing this correctly!

tab ppe95_bins10, sum(ppe95)
tab ppe95_bins10, sum(base_emp)

reghdfe emp_growth _IyeaXP* PR ,vce(cluster gvkey) nocon a(ppe95_bins10##year)
reghdfe capex_base _IyeaXP* PR ,vce(cluster gvkey) nocon a(ppe95_bins10##year)
