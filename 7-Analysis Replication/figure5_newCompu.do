// This file recreates Figure 5
// Author: Dan Garrett
// Date: 3-31-2020

/* Data sources:
various sources including NETS and compustat
*/

clear all
set more off
snapshot erase _all 

use "$data/Replication Data/figure5_data_KR", replace

*** labels for some graphs
local fs_capx_name "Foreign Share Investment"
local capex_base_name "Investment/Physical Capital"
local capex_base_p_name "% Change in Investment/Physical Capital"
local capex_base_1_name "Investment/Physical Capital"
local ftax_name "Federal Tax Rate"
local capxs_name "Capital Expenditure Spending"
local emps_name "Number of Emplyees (Compustat)"
local ias_name "Total Identifiable Assets"
local nis_name "Net Income"
local revts_name "Revenue"
local sales_name "Sales"
local IHS_rev_name "IHS Revenue"
local IHS_capx_name "IHS CapEx"
local IHS_emp_name "IHS Number of Employees"
local log_tot_emp_name "Log Total Employees (Nets)"
local revts_06_name "2006 Revenue (continuous)"
local capxs_9095_name "1990-1995 Capxs (Compustat) Mean (continuous)"
local capex_9095_name "1990-1995 Capex (NETS) Mean (Continuous)"
local revts_06_bins10_name "2006 Revenue Decile"
local capxs_9095_bins10_name "1990-1995 Mean Capxs (Compustat) Decile"
local capex_9095_bins10_name "1990-1995 Mean Capex (NETS) Decile"
 
* combining years and N2
egen yearg = group(year n2)

* Make balanced panel!
gsort gvkey year
local balance = 0 // 0 if FALSE, 1 if TRUE
if `balance' == 1 {
	bys gvkey: gen firm_survival = _N
	keep if firm_survival == 16 // 46,960 left out of 115,224
	drop firm_survival
}

*figures 5, 6, A5, and A6
xi i.year|PR , noomit
drop _IyeaXPR_1995 

forval i=1990(1)2014 { 
	capture: label var _IyeaXPR_`i' " "
} 
forval i=1991(2)2014 { 
	capture: label var _IyeaXPR_`i' "`i'"
} 
 
// reghdfe capex_base _IyeaX* PR IHS_rev log_tot_emp, cl(gvkey)   a(yearg gvkey)
// reghdfe capex_base _IyeaX* PR , cl(gvkey)   a(yearg gvkey)
//
// reghdfe capex_base _IyeaX* PR , cl(gvkey)   a(yearg gvkey)



tab year PR, summ(revts)
tab year PR, summ(capxs)

* Create 2006 revenue decile by gvkey
foreach x of var revts {
	local new_var "`x'_06"
	local new_var_bin "`new_var'_bins10"
	tempvar temp1 temp2
	gen `temp1' = `x' * (year==2006)
	egen `new_var' = max(`temp1'), by(gvkey)
	xtile `new_var_bin' = `new_var', n(10)
}

* Capex/Capxs decile by gvkey
foreach x of var capxs capex {
	local new_var "`x'_9095"
	local new_var_bin "`new_var'_bins10"
	tempvar temp1 temp2
	gen `temp1' = `x' * (year<=1995)
	egen `temp2' = mean(`temp1') if `temp1' != 0, by(gvkey)
	egen `new_var' = max(`temp2'), by(gvkey)
	xtile `new_var_bin' = `new_var', n(10)
}


*** Indicator fs_sample
gen fs_sample = (fs_capx!=.)

snapshot save




***** KEVIN EDITS *****
* Investigate summaries
foreach x of var capxs capex {
		*sum `x' if year==1995 & PR==0, detail
		*sum `x' if year==1995 & PR==1, detail
		
		sum `x'_9095  if year==1995, detail
		tab `x'_9095_bins10 if year==1995
}
/*
* Deciles
foreach y of var capex_base  {	//

	foreach x of var revts_06_bins10 capex_9095_bins10 {

		*running the regression
		reghdfe `y' ib(last).year _IyeaX* PR, cl(gvkey) a(`x'##i.year year)

	}		
}

* Continuous
foreach y of var capex_base {	//

	foreach x of var revts_06 capex_9095 {

		*running the regression
		reghdfe `y' ib(last).year _IyeaX* PR c.`x'##i.year, cl(gvkey) a(year)

	}		
}
*/

	
// * Add in better controls (deciles)
// areg capex_base ib(last).year PR PR_post i.year#i.rev_bins10 i.year#i.emp_bins10 , cl(gvkey)   a(year)
// reghdfe capex_base  PR PR_post , absorb(gvkey year#n2 i.year#i.rev_bins10 i.year#i.emp_bins10) cl(gvkey)
//
//
// ********************************************************************************
// * expanded sample newly extracted from Compustat (1) No Control
// ********************************************************************************
// local output_suffix "_newCompu_noControl_bal`balance'"
// foreach y of var  capex_base  {	//
// 	snapshot restore 1
// 	*running the regression
// 	reghdfe `y' _IyeaX* PR, cl(gvkey) a(B=yearg)
//
// 	** figure years for the graph
// 	gen fig_year = 1991 in 1
// 	foreach i of numlist 1/16 {
// 	local j = `i' + 1990
// 	replace fig_year = `j' in `i'
// 	}
//
// 	** annual betas for the preperiod
// 	gen pr_beta = 0
// 	gen pr_beta_lb = 0
// 	gen pr_beta_ub = 0
// 	foreach i of numlist 1/4 {
// 		local j = `i' + 1990
// 		lincom _b[_IyeaXPR_`j'] , level(90)
// 		replace pr_beta = r(estimate) in `i'
// 		replace pr_beta_lb = r(lb) in `i'
// 		replace pr_beta_ub = r(ub) in `i'
// 	}
// 	* zero effect in 1995
// 	lincom  0
// 	replace pr_beta = r(estimate) in 5
//
// 	** annual betas for the postperiod
// 	foreach i of numlist 6/16 {
// 	local j = `i' + 1990
// 	lincom _b[_IyeaXPR_`j'] , level(90)
// 	replace pr_beta = r(estimate) in `i'
// 	replace pr_beta_lb = r(lb) in `i'
// 	replace pr_beta_ub = r(ub) in `i'
// 	}
//
//
// 	* outputting the graph
// 	graph twoway (scatter pr_beta fig_year, lcolor(navy) mcolor(navy) ///
// 	 yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
// 		(rcap pr_beta_lb pr_beta_ub fig_year, lcolor(navy) lpattern(solid)) ///
// 		, plotregion(fcolor(white) lcolor(white)) ///
// 		graphregion(fcolor(white) lcolor(white))  ///
// 		title("No Control") xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
// 		legend(position(6) on order (1 2) label(1 "Puerto Rico Presence")  ///
// 		label(2 "90% Confidence Interval") r(1)) ///
// 		yti("Effect of S936 on ``y'_name'", margin(medium))
// 	graph export "$output/Graphs/eddie/figure_5`output_suffix'.pdf", replace 
// }
//
// ********************************************************************************
// * expanded sample newly extracted from Compustat (2) Same specification as paper
// ********************************************************************************
// est clear
// local output_suffix "_newCompu_bal`balance'"
// foreach y of var  capex_base  {	//
// 	snapshot restore 1
// 	*running the regression
// 	reghdfe `y' _IyeaX* PR IHS_rev log_tot_emp, cl(gvkey)   a(B=yearg)
//
// 	** figure years for the graph
// 	gen fig_year = 1991 in 1
// 	foreach i of numlist 1/16 {
// 	local j = `i' + 1990
// 	replace fig_year = `j' in `i'
// 	}
//
// 	** annual betas for the preperiod
// 	gen pr_beta = 0
// 	gen pr_beta_lb = 0
// 	gen pr_beta_ub = 0
// 	foreach i of numlist 1/4 {
// 		local j = `i' + 1990
// 		lincom _b[_IyeaXPR_`j'] , level(90)
// 		replace pr_beta = r(estimate) in `i'
// 		replace pr_beta_lb = r(lb) in `i'
// 		replace pr_beta_ub = r(ub) in `i'
// 	}
// 	* zero effect in 1995
// 	lincom  0
// 	replace pr_beta = r(estimate) in 5
//
// 	** annual betas for the postperiod
// 	foreach i of numlist 6/16 {
// 	local j = `i' + 1990
// 	lincom _b[_IyeaXPR_`j'] , level(90)
// 	replace pr_beta = r(estimate) in `i'
// 	replace pr_beta_lb = r(lb) in `i'
// 	replace pr_beta_ub = r(ub) in `i'
// 	}
//
//
// 	* outputting the graph
// 	graph twoway (scatter pr_beta fig_year, lcolor(navy) mcolor(navy) ///
// 	 yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
// 		(rcap pr_beta_lb pr_beta_ub fig_year, lcolor(navy) lpattern(solid)) ///
// 		, plotregion(fcolor(white) lcolor(white)) ///
// 		graphregion(fcolor(white) lcolor(white))  ///
// 		title("Paper Specification") xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
// 		legend(position(6) on order (1 2) label(1 "Puerto Rico Presence")  ///
// 		label(2 "90% Confidence Interval") r(1)) ///
// 		yti("Effect of S936 on ``y'_name'", margin(medium))
// 	graph export "$output/Graphs/eddie/figure_5`output_suffix'.pdf", replace 
// }
//
//
// ********************************************************************************
// * expanded sample newly extracted from Compustat (3) Experimenting new controls
// ********************************************************************************
// est clear
// foreach y of var capex_base  {	//
//
// 	foreach x of var capxs emps ias nis revts sales IHS_* log_tot_emp {
// 		snapshot restore 1
// 		*running the regression
// 		reghdfe `y' _IyeaX* PR `x', cl(gvkey)   a(B=yearg)
//
// 		** figure years for the graph
// 		gen fig_year = 1991 in 1
// 		foreach i of numlist 1/16 {
// 		local j = `i' + 1990
// 		replace fig_year = `j' in `i'
// 		}
//
// 		** annual betas for the preperiod
// 		gen pr_beta = 0
// 		gen pr_beta_lb = 0
// 		gen pr_beta_ub = 0
// 		foreach i of numlist 1/4 {
// 			local j = `i' + 1990
// 			lincom _b[_IyeaXPR_`j'] , level(90)
// 			replace pr_beta = r(estimate) in `i'
// 			replace pr_beta_lb = r(lb) in `i'
// 			replace pr_beta_ub = r(ub) in `i'
// 		}
// 		* zero effect in 1995
// 		lincom  0
// 		replace pr_beta = r(estimate) in 5
//
// 		** annual betas for the postperiod
// 		foreach i of numlist 6/16 {
// 		local j = `i' + 1990
// 		lincom _b[_IyeaXPR_`j'] , level(90)
// 		replace pr_beta = r(estimate) in `i'
// 		replace pr_beta_lb = r(lb) in `i'
// 		replace pr_beta_ub = r(ub) in `i'
// 		}
//
//
// 		* outputting the graph
// 		graph twoway (scatter pr_beta fig_year, lcolor(navy) mcolor(navy) ///
// 		yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
// 			(rcap pr_beta_lb pr_beta_ub fig_year, lcolor(navy) lpattern(solid)) ///
// 			, plotregion(fcolor(white) lcolor(white)) ///
// 			graphregion(fcolor(white) lcolor(white))  ///
// 			title("Control: ``x'_name'") xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
// 			legend(position(6) on order (1 2) label(1 "Puerto Rico Presence")  ///
// 			label(2 "90% Confidence Interval") r(1)) ///
// 			yti("Effect of S936 on ``y'_name'", margin(medium))
// 		graph export "$output/Graphs/eddie/figure_5_`x'.pdf", replace 
// 		est clear
// 	}
// }

********************************************************************************
* expanded sample newly extracted from Compustat (4) 2006 rev decile, 1990-1995 capex mean decile
********************************************************************************
est clear
foreach y of var capex_base  {	//

	foreach x of var revts_06_bins10 capxs_9095_bins10 capex_9095_bins10 {
		snapshot restore 1
		est clear
		*running the regression
		areg `y' ib(last).year _IyeaX* PR i.year#i.`x', cl(gvkey) a(year)

		** figure years for the graph
		gen fig_year = 1991 in 1
		foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
		}

		** annual betas for the preperiod
		gen pr_beta = 0
		gen pr_beta_lb = 0
		gen pr_beta_ub = 0
		foreach i of numlist 1/4 {
			local j = `i' + 1990
			lincom _b[_IyeaXPR_`j'] , level(90)
			replace pr_beta = r(estimate) in `i'
			replace pr_beta_lb = r(lb) in `i'
			replace pr_beta_ub = r(ub) in `i'
		}
		* zero effect in 1995
		lincom  0
		replace pr_beta = r(estimate) in 5

		** annual betas for the postperiod
		foreach i of numlist 6/16 {
		local j = `i' + 1990
		lincom _b[_IyeaXPR_`j'] , level(90)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
		}


		* outputting the graph
		graph twoway (scatter pr_beta fig_year, lcolor(navy) mcolor(navy) ///
		yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
			(rcap pr_beta_lb pr_beta_ub fig_year, lcolor(navy) lpattern(solid)) ///
			, plotregion(fcolor(white) lcolor(white)) ///
			graphregion(fcolor(white) lcolor(white))  ///
			title("I/K Effect | Control: ``x'_name'") xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
			legend(position(6) on order (1 2) label(1 "Puerto Rico Presence")  ///
			label(2 "90% Confidence Interval") r(1)) ///
			yti("Effect of S936 on ``y'_name'", margin(medium))
		graph export "$output/Graphs/eddie/figure_5_`x'.pdf", replace 
	}
}

********************************************************************************
* expanded sample newly extracted from Compustat (5) 2006 rev, 1990-1995 capex mean continuous
********************************************************************************
est clear
foreach y of var capex_base  {	//

	foreach x of var revts_06 capxs_9095 capex_9095 {
		snapshot restore 1
		est clear
		*running the regression
		areg `y' ib(last).year _IyeaX* PR c.`x'##i.year, cl(gvkey) a(year)

		** figure years for the graph
		gen fig_year = 1991 in 1
		foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
		}

		** annual betas for the preperiod
		gen pr_beta = 0
		gen pr_beta_lb = 0
		gen pr_beta_ub = 0
		foreach i of numlist 1/4 {
			local j = `i' + 1990
			lincom _b[_IyeaXPR_`j'] , level(90)
			replace pr_beta = r(estimate) in `i'
			replace pr_beta_lb = r(lb) in `i'
			replace pr_beta_ub = r(ub) in `i'
		}
		* zero effect in 1995
		lincom  0
		replace pr_beta = r(estimate) in 5

		** annual betas for the postperiod
		foreach i of numlist 6/16 {
		local j = `i' + 1990
		lincom _b[_IyeaXPR_`j'] , level(90)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
		}


		* outputting the graph
		graph twoway (scatter pr_beta fig_year, lcolor(navy) mcolor(navy) ///
		yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
			(rcap pr_beta_lb pr_beta_ub fig_year, lcolor(navy) lpattern(solid)) ///
			, plotregion(fcolor(white) lcolor(white)) ///
			graphregion(fcolor(white) lcolor(white))  ///
			title("I/K Effect | Control: ``x'_name'") xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
			legend(position(6) on order (1 2) label(1 "Puerto Rico Presence")  ///
			label(2 "90% Confidence Interval") r(1)) ///
			yti("Effect of S936 on ``y'_name'", margin(medium))
		graph export "$output/Graphs/eddie/figure_5_`x'.pdf", replace 
	}
}



*************************************************************************
************************ limit to fs_capx sample ************************
*************************************************************************

/*
foreach y of var capex_base fs_capx {
	
	reghdfe `y' ib(last).year _IyeaX* PR if fs_sample==1, cl(gvkey) a( year)
	
	foreach x of var revts_06_bins10 capex_9095_bins10 {

		*running the regression
		reghdfe `y' ib(last).year _IyeaX* PR c.`x'##i.year if fs_sample==1, cl(gvkey) a(year)

	}		
	
}
*/

est clear
foreach y of var capex_base  {	//

	foreach x of var revts_06 capex_9095 {
		*snapshot restore 1
		preserve
		est clear
		*running the regression
		areg `y' ib(last).year _IyeaX* PR c.`x'##i.year if fs_sample==1, cl(gvkey) a(year)

		** figure years for the graph
		gen fig_year = 1991 in 1
		foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
		}

		** annual betas for the preperiod
		gen pr_beta = 0
		gen pr_beta_lb = 0
		gen pr_beta_ub = 0
		foreach i of numlist 1/4 {
			local j = `i' + 1990
			lincom _b[_IyeaXPR_`j'] , level(90)
			replace pr_beta = r(estimate) in `i'
			replace pr_beta_lb = r(lb) in `i'
			replace pr_beta_ub = r(ub) in `i'
		}
		* zero effect in 1995
		lincom  0
		replace pr_beta = r(estimate) in 5

		** annual betas for the postperiod
		foreach i of numlist 6/16 {
		local j = `i' + 1990
		lincom _b[_IyeaXPR_`j'] , level(90)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
		}


		* outputting the graph
		graph twoway (scatter pr_beta fig_year, lcolor(navy) mcolor(navy) ///
		yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
			(rcap pr_beta_lb pr_beta_ub fig_year, lcolor(navy) lpattern(solid)) ///
			, plotregion(fcolor(white) lcolor(white)) ///
			graphregion(fcolor(white) lcolor(white))  ///
			title("I/K Effect | Control: ``x'_name'") xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
			legend(position(6) on order (1 2) label(1 "Puerto Rico Presence")  ///
			label(2 "90% Confidence Interval") r(1)) ///
			yti("Effect of S936 on ``y'_name'", margin(medium))
		graph export "$output/Graphs/eddie/figure_5_`x'_FS_Sample.pdf", replace 
	restore
	}
}


est clear
foreach y of var capex_base  {	//

		preserve
		est clear
		*running the regression
		areg `y' ib(last).year _IyeaX* PR if fs_sample==1, cl(gvkey) a(year)

		** figure years for the graph
		gen fig_year = 1991 in 1
		foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
		}

		** annual betas for the preperiod
		gen pr_beta = 0
		gen pr_beta_lb = 0
		gen pr_beta_ub = 0
		foreach i of numlist 1/4 {
			local j = `i' + 1990
			lincom _b[_IyeaXPR_`j'] , level(90)
			replace pr_beta = r(estimate) in `i'
			replace pr_beta_lb = r(lb) in `i'
			replace pr_beta_ub = r(ub) in `i'
		}
		* zero effect in 1995
		lincom  0
		replace pr_beta = r(estimate) in 5

		** annual betas for the postperiod
		foreach i of numlist 6/16 {
		local j = `i' + 1990
		lincom _b[_IyeaXPR_`j'] , level(90)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
		}


		* outputting the graph
		graph twoway (scatter pr_beta fig_year, lcolor(navy) mcolor(navy) ///
		yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
			(rcap pr_beta_lb pr_beta_ub fig_year, lcolor(navy) lpattern(solid)) ///
			, plotregion(fcolor(white) lcolor(white)) ///
			graphregion(fcolor(white) lcolor(white))  ///
			title("I/K Effect | Base Specification") xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
			legend(position(6) on order (1 2) label(1 "Puerto Rico Presence")  ///
			label(2 "90% Confidence Interval") r(1)) ///
			yti("Effect of S936 on ``y'_name'", margin(medium))
		graph export "$output/Graphs/eddie/figure_5_Base_FS_Sample.pdf", replace 
	restore
}


************ Figure 6 ************

est clear
foreach y of var fs_capx  {	//

	foreach x of var revts_06 capex_9095 {
		*snapshot restore 1
		preserve
		est clear
		*running the regression
		areg `y' ib(last).year _IyeaX* PR c.`x'##i.year if fs_sample==1, cl(gvkey) a(year)

		** figure years for the graph
		gen fig_year = 1991 in 1
		foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
		}

		** annual betas for the preperiod
		gen pr_beta = 0
		gen pr_beta_lb = 0
		gen pr_beta_ub = 0
		foreach i of numlist 1/4 {
			local j = `i' + 1990
			lincom _b[_IyeaXPR_`j'] , level(90)
			replace pr_beta = r(estimate) in `i'
			replace pr_beta_lb = r(lb) in `i'
			replace pr_beta_ub = r(ub) in `i'
		}
		* zero effect in 1995
		lincom  0
		replace pr_beta = r(estimate) in 5

		** annual betas for the postperiod
		foreach i of numlist 6/16 {
		local j = `i' + 1990
		lincom _b[_IyeaXPR_`j'] , level(90)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
		}


		* outputting the graph
		graph twoway (scatter pr_beta fig_year, lcolor(navy) mcolor(navy) ///
		yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
			(rcap pr_beta_lb pr_beta_ub fig_year, lcolor(navy) lpattern(solid)) ///
			, plotregion(fcolor(white) lcolor(white)) ///
			graphregion(fcolor(white) lcolor(white))  ///
			title("I/K Effect | Control: ``x'_name'") xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
			legend(position(6) on order (1 2) label(1 "Puerto Rico Presence")  ///
			label(2 "90% Confidence Interval") r(1)) ///
			yti("Effect of S936 on ``y'_name'", margin(medium))
		graph export "$output/Graphs/eddie/figure_6_`x'_FS_Sample.pdf", replace 
	restore
	}
}
