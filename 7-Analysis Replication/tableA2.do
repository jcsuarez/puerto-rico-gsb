// This file recreates Table A2
// Author: Dan Garrett
// Date: 4-25-2020

/* Data sources:
COMPUSTAT and NETS
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data/tableA2_data", clear

* collecting variables
local General "etr txfed MNE nol logAT"
local Sales "rd ads"
local Assets "ppe lev btd intang"
local Physical "capex"

local vars "`General' `Sales' `Physical' `Assets'"

*PR firm characteristics
estpost tabstat `vars' if PR == 1, statistics(mean sd count ) ///
	columns(statistics)
est store tempA

* non-PR firm characteristics
estpost tabstat `vars' if PR == 0, statistics(mean sd count ) ///
	columns(statistics)
est store tempC

esttab tempA tempC using "$output/Tables/tableA2.tex", replace ///
	refcat(etr "\emph{General Characteristics}" rd "\emph{Spending}" ppe "\emph{Capital and Financing}",) /// 
	cells("mean(fmt(3)) sd(fmt(3)) count(fmt(0))" ) nonum label nomtitle tex ///
	collabels("Mean" "SD" "Count" ) noobs mgroups("S936 Firms" "All Control Firms", pattern(1 1 1) ///
prefix(\multicolumn{@span}{c}{) suffix(})   ///
span erepeat(\cmidrule(lr){@span}))

** NOTE: you must manually delete reference category annotations to match paper formatting