// This file recreates Table A35, quantile regressions
// Author: Dan Garrett
// Date: 5-18-2020

/* Data sources:
NETS, compustat
*/

*** NOTE: These regressions take forever, so I've combined Table A35 and Figure A20D to avoid running twice

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data/tableA35_data", clear

* initiallizing quantile regression ado
do "myqr.ado" // requires file to be in the same directory

* reps for the bootstraps
local reps = 250

*defining the specification
local spec "_Iyear* _Iindustr* _Ifips_stat*"

* estimating bootstrap, clustering at the state level
eststo qreg1: bootstrap pr_link_ind=e(pr_link_ind), reps(`reps') nowarn nodrop cluster(fips_state) ///
idcluster(id_cluster) group(pid2) saving("$output/myqr_res_1", replace) : myqr ,  levelb(.15) spec(`spec')

eststo qreg2: bootstrap pr_link_ind=e(pr_link_ind), reps(`reps') nowarn nodrop cluster(fips_state) ///
idcluster(id_cluster) group(pid2) saving("$output/myqr_res_2", replace) : myqr ,  levelb(.25) spec(`spec')

eststo qreg3: bootstrap pr_link_ind=e(pr_link_ind), reps(`reps') nowarn nodrop cluster(fips_state) ///
idcluster(id_cluster) group(pid2) saving("$output/myqr_res_3", replace) : myqr ,  levelb(.50) spec(`spec')

eststo qreg4: bootstrap pr_link_ind=e(pr_link_ind), reps(`reps') nowarn nodrop cluster(fips_state) ///
idcluster(id_cluster) group(pid2) saving("$output/myqr_res_4", replace) : myqr ,  levelb(.75) spec(`spec')

eststo qreg5: bootstrap pr_link_ind=e(pr_link_ind), reps(`reps') nowarn nodrop cluster(fips_state) ///
idcluster(id_cluster) group(pid2) saving("$output/myqr_res_5", replace) : myqr ,  levelb(.85) spec(`spec')

* taking output data cam combining for table
use "$output/myqr_res_1", clear 
rename pr_link_ind q15 

merge 1:1 _n using "$output/myqr_res_2"
rename pr_link_ind q25 
drop _merge

merge 1:1 _n using "$output/myqr_res_3"
rename pr_link_ind q50 
drop _merge

merge 1:1 _n using "$output/myqr_res_4"
rename pr_link_ind q75 
drop _merge

merge 1:1 _n using "$output/myqr_res_5"
rename pr_link_ind q85 
drop _merge

gen cons = 1
 
collapse (mean) q15 (sd) SE_q15=q15 ///
(mean) q25 (sd) SE_q25=q25 ///
(mean) q50 (sd) SE_q50=q50 ///
(mean) q75 (sd) SE_q75=q75 ///
(mean) q85 (sd) SE_q85=q85 , by(cons) 
	
reshape long q SE_q , i(cons) j(quantile)

*organizing for the table	
mat b = (q[1],q[2],q[3],q[4],q[5])
mat V = (SE_q[1]^2,SE_q[2]^2,SE_q[3]^2,SE_q[4]^2,SE_q[5]^2)
mat V = diag(V)	
	
mat colnames b= "q15:pr" "q25:pr" "q50:pr" "q75:pr"	"q85:pr"		
mat colnames V= "q15:pr" "q25:pr" "q50:pr" "q75:pr"	"q85:pr"		
mat rownames V= "q15:pr" "q25:pr" "q50:pr" "q75:pr"	"q85:pr"		
	
ereturn post b V	
ereturn di 

*outputting the table
esttab using "$output/Tables/tableA35.tex" , unstack ///
 stats() ///
cells(b(fmt(3)) se(par) p) label mlab(none)  coll(none) ///
noobs nogap nomtitle tex replace nocons ///
coeflabel(pr "Exposure to Section 936 ") ///
 prefoot("\hline") ///
postfoot("Year Fixed Effects & Y & Y & Y & Y & Y \\ "  ///
"State Fixed Effects & Y & Y & Y & Y & Y \\ "  ///
"Industry Fixed Effects & Y & Y & Y & Y & Y \\ "  ///
"  \\ \hline\hline" ///
"\end{tabular} }" )  sty(tex)	


* CODE FOR THE GRAPH
gen ciL = q-SE*1.96 
gen ciH = q+SE*1.96 
	
twoway (scatter q quantile, connect(line) yline(0 , lcolor(black)) ) ///
    (rcap ciL ciH quantile,  lcolor(navy) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Quantile")  ///
	legend(label(1 "Estimate") label(2 "95% CI")) ///
	ytitle("{&beta}{sup:q}: Quantile Regression") ylab(-.12(.02)0) xlab( 15 25 50 75 85 ) 
	
	graph export "$output/Graphs/figureA20_D.pdf", replace
	 