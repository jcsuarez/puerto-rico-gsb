// This file estimates semi-elasticities with SOI-estimated tax changes
// Author: Dan Garrett
// Date: 4-20-2020

/* Data sources:
NETS and Compustat
*/

clear all
set more off
snapshot erase _all 

** adding the programs
do "$analysis/7-Analysis Replication/twosampleIV_spec5.ado"
do "$analysis/7-Analysis Replication/twosampleIV_spec6.ado"

*******************************************************
* Starting with SOI tax rates by year from figure 1, panel C
*******************************************************
use "$data/Replication Data/figure1_c_data", replace

* renaming variable to be more usable
rename Income_Subject_To_Tax_base ISTT
rename Us_Possessions_Tax_Credit_base USPTC

** outputting the aggregate figure
local base_link = .0923148 // from NETS, share of US employment at firms with operations in PR
local beta_base "0.816" // from column (3) of Table A1

* share of income subject to tax that is covered by the US Possession tax credit
gen share = USPTC / ISTT

* Counterfactual US possessions tax credit holding share of income constant
gen const_cred = share * Income_Subject_To_Tax

* dollars of lost credits due to the rollback
gen lost_cred = const_cred - Us_Possessions_Tax_Credit

* oberserved effective tax rates
gen p_data = (Income_Tax_After_Credits) / Income_Subject_To_Tax

* Predicted tax rates had S936 not been repealed, correcting for 20% adjustment to other credits
gen p_new = (Income_Tax_After_Credits * `base_link' - `beta_base' * lost_cred ) / (Income_Subject_To_Tax * `base_link')

* relative increase in taxes for PR firms with rollback
gen p_dif = p_data - p_new

keep year p_data p_dif
*******************************************************
* next using the data from table 2
*******************************************************
merge 1:m year using "$data/Replication Data/table2_data"
replace p_data = 0.2761941 if year < 1995
replace p_dif = 0 if year < 1995
drop _merge

gen tax_impute = p_data
replace tax_impute = tax_impute + p_dif if year > 1995 & PR
replace tax_impute = tax_impute * 100

* pulling in tax variation
merge 1:1 gvkey year using "$data/Replication Data/table3_data"
drop _merge

* formatting, labeling, and descriptive scalar creation
local FE_fs_capx "n3"
local FE_capex_base "gvkey"
local FE_capex_base_p "gvkey"
local FE_capex_base_1 "gvkey"
local FE_ftax "gvkey"

local FE_fs_capx_word "Industry"
local FE_capex_base_word "Firm"
local FE_capex_base_p_word "Firm"
local FE_capex_base_1_word "Firm"
local FE_ftax_word "Firm"

local fs_capx_sum "fs_capx"
local capex_base_sum "IK"
local capex_base_p_sum "IKIK"
local capex_base_1_sum "IK_1"
local ftax_sum "ftax"

local fs_capx_scalars1 ""
local capex_base_scalars1 "avg Sample Average I/K in 2006" 
local capex_base_p_scalars1 "avg Sample Average I/K in 2006 Relative to 1995"
local capex_base_1_scalars1 "avg Sample Average I/K in 2006"
local ftax_scalars1 ""

local fs_capx_scalars2 ""
local capex_base_scalars2 "elas Percent of 2006 Average"
local capex_base_p_scalars2 "elas Percent of 2006 Average"
local capex_base_1_scalars2 "elas Percent of 2006 Average"
local ftax_scalars2 ""

local fs_capx_scalars3 ""
local capex_base_scalars3 "elast Semi-elasticity of Investment"
local capex_base_p_scalars3 "elast Semi-elasticity of Investment"
local capex_base_1_scalars3 "elast Semi-elasticity of Investment"
local ftax_scalars3 ""

local fs_capx_scalars4 ""
local capex_base_scalars4 "pp_dif Change in Effective Tax Rate"
local capex_base_p_scalars4 "pp_dif Change in Effective Tax Rate"
local capex_base_1_scalars4 "pp_dif Change in Effective Tax Rate"
local ftax_scalars4 ""

local fs_capx_title "Change in Foreign Share of Investment"
local capex_base_title "Change in Investment: $ \frac{I}{K_{1990-1995}} $"
local capex_base_p_title "Percent Change in Investment: $ \frac{I}{I_{1990-1995}} - 1 $"
local capex_base_1_title "Change in Investment: $ \frac{I}{K_{1990-1995}} $"
local ftax_title "Change in Federal Taxes Paid as a Percent of Pretax Income"
xtset gvkey year

* scalar values from SOI data analysis:
scalar p_dif_v0 = .0572734
scalar p_dif_v1 = .0699663
scalar p_dif_v2 = .0465333
scalar p_dif_v3 = .0601001

* major sectors:
local if2_1 = "inlist(n2,3,5,6,7,11,12)"

*****************************************************
* one step instrument ()
*****************************************************
drop if gvkey == .
egen group_id = group(gvkey)
tsset, clear

winsor ftax, generate(ftax_w) p(0.05)
replace ftax = ftax_w / 100

est clear
bootstrap testi1ng=e(ftaxhat), reps(20) nowarn nodrop cluster(gvkey) ///
idcluster(id_cluster) : twosampleIV_spec5 ,  spec( " IHS_rev log_tot_emp")
estimates store spec5

bootstrap testi1ng=e(ftaxhat), reps(200) nowarn nodrop cluster(gvkey) ///
idcluster(id_cluster) : twosampleIV_spec6 ,  spec( " IHS_rev log_tot_emp")
estimates store spec6

*outputting results
esttab spec? using "$output/Tables/table2_onestep_bootstrap_20200604.tex", drop(*) stats() ///
b(3) se par label ///
noobs nogap nomtitle tex replace nonum nocons postfoot("") prefoot("") posthead("")

esttab spec? using "$output/Tables/table2_onestep_bootstrap_20200604.tex", keep(testi1ng) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) noobs scalars("N Observations" "``y'_scalars1'" ) /// 
varlabel(PR_post "\hspace{1em}Exposure to Section 936 X Post" testi1ng "\hspace{1em}Effective Tax Rate")  posth("\hline") ///
preh("\multicolumn{7}{l}{\textbf{``y'_title'}}\\")  postfoot( ///
"\hline Year Fixed Effects    & Y & Y \\ " ///
"NAICS-by-Year Fixed Effects   & Y & Y \\ " ///
"`FE_`y'_word' Fixed Effects  & Y & Y \\ "  ///
"S936 Exposed Sector  & Y & Y \\ " /// 
"S936 Exposed Industry  Y & Y  \\ " ///
"DFL Weights & & Y \\  \hline\hline" ///
"\end{tabular} }" ) append sty(tex)

