// This file recreates Figure A3 (all panels)
// Author: Dan Garrett
// Date: 3-31-2020

/* Data sources:
various sources including NETS and compustat
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* Panels A and B
*******************************************************
use "$data/Replication Data/figureA4_data", replace

*** labels for some graphs
local fs_capx_name "Foreign Share Investment"
local capex_base_name "Investment/Physical Capital"
local capex_base_p_name "% Change in Investment/Physical Capital"
local capex_base_1_name "Investment/Physical Capital"
local ftax_name "Federal Tax Rate"
 
gen capex_baseline = IK - capex_base
gen capex_baseline_1 = IK_1 - capex_base_1
gen capex_baseline_p = IKIK - capex_base_p

local capex_base_sum "capex_baseline"
local capex_base_p_sum "capex_baseline_p"
local capex_base_1_sum "capex_baseline_1" 
 
* combining years and N2
egen yearg = group(year n2)

*figures 5, 6, A5, and A6
xi i.year|PR , noomit
drop _IyeaXPR_1995 

forval i=1990(1)2014 { 
	capture: label var _IyeaXPR_`i' " "
} 
forval i=1991(2)2014 { 
	capture: label var _IyeaXPR_`i' "`i'"
} 
snapshot save
 
***************************************************
* figures 5, 6, A5, and A6 are all the same code on a different variable
***************************************************
***  total effect version, residualized
foreach y of var  capex_base capex_base_p capex_base_1  {	//
snapshot restore 1

*running the regression
reghdfe `y' _IyeaX* PR IHS_rev log_tot_emp, cl(gvkey)   a(B=yearg)

*generating variables for the levels graph
foreach i of numlist 1991/2006 {
sum B if e(sample) & year == `i'
scalar year_mean`i' = r(mean)

sum log_tot_emp if e(sample) & year == `i'
scalar rev_mean`i' = r(mean)
sum log_tot_emp if e(sample) & year == `i'
scalar emp_mean`i' = r(mean)
}
** year FE and such
gen year_fe = 0
foreach i of numlist 1/16 {
local j = `i' + 1990
replace year_fe = year_mean`j' in `i'
}

** figure years for the graph
gen fig_year = 1991 in 1
foreach i of numlist 1/16 {
local j = `i' + 1990
replace fig_year = `j' in `i'
}

** adding in 1995 baseline value:
sum ``y'_sum' if e(sample) // ``y'_sum'
scalar baseline = r(mean)

** total effect for PR firms
gen pr_beta_tot = 0
gen beta_tot = 0

lincom _b[_IyeaXPR_1991] + year_mean1991 +_b[_cons] + emp_mean1991*_b[log_tot_emp] + rev_mean1991 * _b[IHS_rev] + baseline , level(90)
replace pr_beta_tot = r(estimate) in 1
foreach i of numlist 2/4 {
local j = `i' + 1990
lincom _b[_IyeaXPR_`j'] + year_mean`j' +_b[_cons] + emp_mean`j'*_b[log_tot_emp] + rev_mean`j' * _b[IHS_rev] + baseline  , level(90)
replace pr_beta_tot = r(estimate) in `i'
}
lincom year_mean1995 +_b[_cons] + emp_mean1995 * _b[log_tot_emp] + rev_mean1995 * _b[IHS_rev] + baseline  , level(90)
replace pr_beta_tot = r(estimate) in 5
foreach i of numlist 6/16 {
local j = `i' + 1990
lincom _b[_IyeaXPR_`j'] + year_mean`j' +_b[_cons] + emp_mean`j'*_b[log_tot_emp] + rev_mean`j' * _b[IHS_rev] + baseline , level(90)
replace pr_beta_tot = r(estimate) in `i'
}
** version for non-PR firms that essentially differences out the beta and difference in FE
lincom year_mean1991 +_b[_cons] + emp_mean1991*_b[log_tot_emp] + rev_mean1991 * _b[IHS_rev] + baseline , level(90)
replace beta_tot = r(estimate) in 1
foreach i of numlist 2/4 {
local j = `i' + 1990
lincom year_mean`j' +_b[_cons] + emp_mean`j'*_b[log_tot_emp] + rev_mean`j' * _b[IHS_rev] + baseline , level(90)
replace beta_tot = r(estimate) in `i'
}
lincom year_mean1995 +_b[_cons] + emp_mean1995*_b[log_tot_emp] + rev_mean1995 * _b[IHS_rev] + baseline , level(90)
replace beta_tot = r(estimate) in 5
foreach i of numlist 6/16 {
local j = `i' + 1990
lincom year_mean`j' +_b[_cons] + emp_mean`j'*_b[log_tot_emp] + rev_mean`j' * _b[IHS_rev] + baseline , level(90)
replace beta_tot = r(estimate) in `i'
}

** outputting a type of total effect graph
 graph twoway (scatter pr_beta_tot fig_year, lcolor(navy) mcolor(navy) c(line) xline(1995, lcolor(black) lpattern(solid))) ///
    (scatter beta_tot fig_year, lcolor(maroon) mcolor(maroon) c(line)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white))  ///
	xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
	legend(on order (1 2) label(1 "Puerto Rico Presence")  ///
	label(2 "Other Firms") r(1)) ///
	yti("``y'_name'", margin(medium))
graph export "$output/Graphs/graph_`y'_ES_spec1_tot_f.pdf", replace
}
