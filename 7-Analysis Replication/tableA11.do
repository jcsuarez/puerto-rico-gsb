// This file recreates Table A11
// Author: Dan Garrett
// Date: 4-27-2020

/* Data sources:
NETS
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* Starting with the data
*******************************************************
use "$data/Replication Data/tableA11_data", clear

est clear 
*generating combined variable
capture: drop PR_int
gen PR_int = PR

* running regressions
eststo emp_1:  reghdfe emp_growth PR_int  ///
  if inrange(year,2004,2008) [aw=wgt] , a(year )  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"       
  
replace PR_int = PR_int1
eststo emp_2:  reghdfe emp_growth PR_int PR_int?   ///
  if inrange(year,2004,2008) [aw=wgt] , a(year )  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"       

replace PR_int = PR_int2
eststo emp_3:  reghdfe emp_growth PR_int PR_int?  ///
  if inrange(year,2004,2008) [aw=wgt] , a(year )  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"  
  
replace PR_int = PR_int3
eststo emp_4:  reghdfe emp_growth PR_int PR_int?  ///
  if inrange(year,2004,2008) [aw=wgt] , a(year )  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"  

replace PR_int = PR_int4
eststo emp_5:  reghdfe emp_growth PR_int PR_int?  ///
  if inrange(year,2004,2008) [aw=wgt] , a(year )  vce(cluster hqduns) noconst      
  estadd local hasy "Yes"  
  
esttab emp_? using "$output/Tables/tableA11.tex", drop(*) stats() ///
b(3) se par label ///
noobs nogap nomtitle tex replace  nocons postfoot("") prefoot("") posthead("")
 
 esttab emp_? using "$output/Tables/tableA11.tex", keep(PR_int) ///
cells(b(fmt(3)) se(par) p) mlab("All" "Chemicals" "Food" "Electronics" "Other" ) nonum coll(none) noobs scalars("N Observations" ) ///
varlabel(PR_int "\hspace{1em}Exposure to Section 936")  posth("\hline") ///
preh("\multicolumn{5}{l}{\textbf{Employment Change by 2004-2008}}\\")  postfoot( ///
"\hline Year Fixed Effects   & Y  & Y & Y & Y & Y \\  \hline\hline" ///
"\end{tabular} }" ) append sty(tex) 