// This file recreates Figure 5
// Author: Dan Garrett
// Date: 3-31-2020

/* Data sources:
various sources including NETS and compustat
*/

clear all
set more off
snapshot erase _all 

*** *** *** *** ***  *** *** ***  *** *** *** *** *** ***
*** *** *** Section (2): Baseline Regressions *** *** ***
*** *** *** *** ***  *** *** ***  *** *** *** *** *** ***
use "$data/Replication Data/newCompu_Clean_0923", replace

global spec1 ", cl(gvkey) a(year)"
global spec2 ", cl(gvkey) a(n2##year)"
global spec3 ", cl(gvkey) a(gvkey n2##year)"
global spec4 "if exp_sec, cl(gvkey) a(gvkey n2##year)"
global spec5 "if exp_ind, cl(gvkey) a(gvkey n2##year)"
global spec6 "if exp_ind [aw=DFL], cl(gvkey) a(gvkey n2##year)"

global specname1 "Baseline"
global specname2 "Sector FE"
global specname3 "Firm + Sector FE"
global specname4 "Exp. Sector"
global specname5 "Exp. Industry"
global specname6 "Exp. Industry + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"


foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 {
	reghdfe fs_capx _IyeaX* PR $`spec'
	reghdfe fs_capx PR PR_post $`spec'
}

*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 {
reghdfe fs_capx _IyeaX* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/6 {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec5", lco($c5) mco($c5) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec5", lco($c5) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec6", lco($c6) mco($c6) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec6", lco($c6) lpa(solid)) ///
		, yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7 9 11) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")  ///
		label(9 "$specname5")  ///
		label(11 "$specname6") ) ///
		yti("Effect of S936 on Foreign Share of Investment", margin(medium))
restore	
}
graph export "$output/Graphs/kevin/fs_capx_Overlay_Base.pdf", replace 	
	
	

*** *** *** *** ***  *** *** ***  *** *** *** *** *** *** *** ***
*** *** *** Section (2b): Baseline Regressions w/ DFL *** *** ***
*** *** *** *** ***  *** *** ***  *** *** *** *** *** *** *** ***
use "$data/Replication Data/newCompu_Clean_0923", replace

global spec1 "[aw=DFL], cl(gvkey) a(year)"
global spec2 "[aw=DFL], cl(gvkey) a(n2##year)"
global spec3 "[aw=DFL], cl(gvkey) a(gvkey n2##year)"
global spec4 "if exp_sec [aw=DFL], cl(gvkey) a(gvkey n2##year)"
global spec5 "if exp_ind [aw=DFL], cl(gvkey) a(gvkey n2##year)"

global specname1 "Baseline"
global specname2 "Sector FE"
global specname3 "Firm + Sector FE"
global specname4 "Exp. Sector"
global specname5 "Exp. Industry"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"


*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4 spec5 {
reghdfe fs_capx _IyeaX* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/5 {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec5", lco($c5) mco($c5) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec5", lco($c5) lpa(solid)) ///
		, yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		ylabel(-0.4(0.2)0.2) ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7 9 11) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")  ///
		label(9 "$specname5")  ) ///
		yti("Effect of S936 on Foreign Share of Investment", margin(medium))
restore	
}
graph export "$output/Graphs/kevin/fs_capx_Overlay_DFL.pdf", replace 	
	
	
		

*** *** *** *** ***  *** *** ***  *** *** *** *** *** *** *** ***
*** *** *** Section (2b): Baseline Regressions w/ DFL *** *** ***
*** *** *** *** ***  *** *** ***  *** *** *** *** *** *** *** ***
use "$data/Replication Data/newCompu_Clean_0923", replace
keep if ppe95_bins10==10
global spec1 "[aw=DFL], cl(gvkey) a(year)"
global spec2 "[aw=DFL], cl(gvkey) a(n2##year)"
global spec3 "[aw=DFL], cl(gvkey) a(gvkey n2##year)"
global spec4 "if exp_sec [aw=DFL], cl(gvkey) a(gvkey n2##year)"
global spec5 "if exp_ind [aw=DFL], cl(gvkey) a(gvkey n2##year)"

global specname1 "Baseline"
global specname2 "Sector FE"
global specname3 "Firm + Sector FE"
global specname4 "Exp. Sector"
global specname5 "Exp. Industry"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"


*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4 spec5 {
reghdfe fs_capx _IyeaX* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/5 {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec5", lco($c5) mco($c5) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec5", lco($c5) lpa(solid)) ///
		, yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		ylabel(-0.4(0.2)0.2) ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7 9 11) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")  ///
		label(9 "$specname5")  ) ///
		yti("Effect of S936 on Foreign Share of Investment", margin(medium))
restore	
}
graph export "$output/Graphs/kevin/fs_capx_Overlay_DFL_top10th.pdf", replace 	
	
	

*** *** *** *** ***  *** *** ***  *** *** *** *** *** *** *** ***
*** *** *** Section (2b): Baseline Regressions w/ DFL *** *** ***
*** *** *** *** ***  *** *** ***  *** *** *** *** *** *** *** ***
use "$data/Replication Data/newCompu_Clean_0923", replace
keep if ppe95_bins3==3
global spec1 "[aw=DFL], cl(gvkey) a(year)"
global spec2 "[aw=DFL], cl(gvkey) a(n2##year)"
global spec3 "[aw=DFL], cl(gvkey) a(gvkey n2##year)"
global spec4 "if exp_sec [aw=DFL], cl(gvkey) a(gvkey n2##year)"
global spec5 "if exp_ind [aw=DFL], cl(gvkey) a(gvkey n2##year)"

global specname1 "Baseline"
global specname2 "Sector FE"
global specname3 "Firm + Sector FE"
global specname4 "Exp. Sector"
global specname5 "Exp. Industry"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"



*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4 spec5 {
reghdfe fs_capx _IyeaX* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/5 {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec5", lco($c5) mco($c5) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec5", lco($c5) lpa(solid)) ///
		, yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		ylabel(-0.4(0.2)0.2) ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7 9 11) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")  ///
		label(9 "$specname5")  ) ///
		yti("Effect of S936 on Foreign Share of Investment", margin(medium))
restore	
}
graph export "$output/Graphs/kevin/fs_capx_Overlay_DFL_top3rd.pdf", replace 	
	
		
	
	

*** *** *** *** ***  *** *** ***  *** *** *** *** *** *** ***
*** *** *** Section (3): Size Control Regressions *** *** ***
*** *** *** *** ***  *** *** ***  *** *** *** *** *** *** ***

*** Deciles ***
use "$data/Replication Data/newCompu_Clean_0923", replace

global spec1 ", cl(gvkey) a(at95_bins10##year)"
global spec2 ", cl(gvkey) a(n2##year at95_bins10##year)"
global spec3 ", cl(gvkey) a(gvkey n2##year at95_bins10##year)"
global spec4 "if exp_sec, cl(gvkey) a(gvkey n2##year at95_bins10##year)"
global spec5 "if exp_ind, cl(gvkey) a(gvkey n2##year at95_bins10##year)"
global spec6 "if exp_ind [aw=DFL], cl(gvkey) a(gvkey n2##year at95_bins10##year)"

global specname1 "Baseline"
global specname2 "Sector FE"
global specname3 "Firm + Sector FE"
global specname4 "Exp. Sector"
global specname5 "Exp. Industry"
global specname6 "Exp. Industry + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"


*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 {
reghdfe fs_capx _IyeaX* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/6 {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec5", lco($c5) mco($c5) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec5", lco($c5) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec6", lco($c6) mco($c6) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec6", lco($c6) lpa(solid)) ///
		, yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7 9 11) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")  ///
		label(9 "$specname5")  ///
		label(11 "$specname6") ) ///
		yti("Effect of S936 on Foreign Share of Investment", margin(medium))
restore	
}
graph export "$output/Graphs/kevin/fs_capx_Overlay_Deciles.pdf", replace 		


*** Terciles ***
use "$data/Replication Data/newCompu_Clean_0923", replace

global spec1 ", cl(gvkey) a(at95_bins3##year)"
global spec2 ", cl(gvkey) a(n2##year at95_bins3##year)"
global spec3 ", cl(gvkey) a(gvkey n2##year at95_bins3##year)"
global spec4 "if exp_sec, cl(gvkey) a(gvkey n2##year at95_bins3##year)"
global spec5 "if exp_ind, cl(gvkey) a(gvkey n2##year at95_bins3##year)"
global spec6 "if exp_ind [aw=DFL], cl(gvkey) a(gvkey n2##year at95_bins3##year)"

global specname1 "Baseline"
global specname2 "Sector FE"
global specname3 "Firm + Sector FE"
global specname4 "Exp. Sector"
global specname5 "Exp. Industry"
global specname6 "Exp. Industry + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"


*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 {
reghdfe fs_capx _IyeaX* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/6 {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec5", lco($c5) mco($c5) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec5", lco($c5) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec6", lco($c6) mco($c6) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec6", lco($c6) lpa(solid)) ///
		, yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7 9 11) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")  ///
		label(9 "$specname5")  ///
		label(11 "$specname6") ) ///
		yti("Effect of S936 on Foreign Share of Investment", margin(medium))
restore	
}
graph export "$output/Graphs/kevin/fs_capx_Overlay_Terciles.pdf", replace 		




*** *** *** *** ***  *** *** ***  *** *** *** *** *** *** *** ***
*** *** *** Section (4): Drop Small Firms Regressions *** *** ***
*** *** *** *** ***  *** *** ***  *** *** *** *** *** *** *** ***

use "$data/Replication Data/newCompu_Clean_0923", replace
drop if at95_bins3 == 1

*** Baseline ***
global spec1 ", cl(gvkey) a(year)"
global spec2 ", cl(gvkey) a(n2##year)"
global spec3 ", cl(gvkey) a(gvkey n2##year)"
global spec4 "if exp_sec, cl(gvkey) a(gvkey n2##year)"
global spec5 "if exp_ind, cl(gvkey) a(gvkey n2##year)"
global spec6 "if exp_ind [aw=DFL], cl(gvkey) a(gvkey n2##year)"

global specname1 "Baseline"
global specname2 "Sector FE"
global specname3 "Firm + Sector FE"
global specname4 "Exp. Sector"
global specname5 "Exp. Industry"
global specname6 "Exp. Industry + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"


*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 {
reghdfe fs_capx _IyeaX* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/6 {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec5", lco($c5) mco($c5) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec5", lco($c5) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec6", lco($c6) mco($c6) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec6", lco($c6) lpa(solid)) ///
		, yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7 9 11) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")  ///
		label(9 "$specname5")  ///
		label(11 "$specname6") ) ///
		yti("Effect of S936 on Foreign Share of Investment", margin(medium))
restore	
}
graph export "$output/Graphs/kevin/fs_capx_Overlay_DropSmall.pdf", replace 		



*** Baseline ***
global spec1 ", cl(gvkey) a(year ppe95_bins10_ALT##year)"
global spec2 ", cl(gvkey) a(n2##year ppe95_bins10_ALT##year)"
global spec3 ", cl(gvkey) a(gvkey n2##year ppe95_bins10_ALT##year)"
global spec4 "if exp_sec, cl(gvkey) a(gvkey n2##year ppe95_bins10_ALT##year)"
global spec5 "if exp_ind, cl(gvkey) a(gvkey n2##year ppe95_bins10_ALT##year)"
global spec6 "if exp_ind [aw=DFL], cl(gvkey) a(gvkey n2##year ppe95_bins10_ALT##year)"

global specname1 "Baseline"
global specname2 "Sector FE"
global specname3 "Firm + Sector FE"
global specname4 "Exp. Sector"
global specname5 "Exp. Industry"
global specname6 "Exp. Industry + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"


*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 {
reghdfe fs_capx _IyeaX* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/6 {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec5", lco($c5) mco($c5) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec5", lco($c5) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec6", lco($c6) mco($c6) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec6", lco($c6) lpa(solid)) ///
		, yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7 9 11) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")  ///
		label(9 "$specname5")  ///
		label(11 "$specname6") ) ///
		yti("Effect of S936 on Foreign Share of Investment", margin(medium))
restore	
}
graph export "$output/Graphs/kevin/fs_capx_Overlay_DropSmall_Deciles.pdf", replace 		







*** *** *** *** ***  *** *** ***  *** *** *** *** *** *** *** ***
*** *** *** Section (4): Keep Large Firms Regressions *** *** ***
*** *** *** *** ***  *** *** ***  *** *** *** *** *** *** *** ***

use "$data/Replication Data/newCompu_Clean_0923", replace
keep if at95_bins3 == 3

*** Baseline ***
global spec1 ", cl(gvkey) a(year)"
global spec2 ", cl(gvkey) a(n2##year)"
global spec3 ", cl(gvkey) a(gvkey n2##year)"
global spec4 "if exp_sec, cl(gvkey) a(gvkey n2##year)"
global spec5 "if exp_ind, cl(gvkey) a(gvkey n2##year)"
global spec6 "if exp_ind [aw=DFL], cl(gvkey) a(gvkey n2##year)"

global specname1 "Baseline"
global specname2 "Sector FE"
global specname3 "Firm + Sector FE"
global specname4 "Exp. Sector"
global specname5 "Exp. Industry"
global specname6 "Exp. Industry + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"


*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 {
reghdfe fs_capx _IyeaX* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/6 {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec5", lco($c5) mco($c5) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec5", lco($c5) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec6", lco($c6) mco($c6) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec6", lco($c6) lpa(solid)) ///
		, yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7 9 11) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")  ///
		label(9 "$specname5")  ///
		label(11 "$specname6") ) ///
		yti("Effect of S936 on Foreign Share of Investment", margin(medium))
restore	
}
graph export "$output/Graphs/kevin/fs_capx_Overlay_KeepBig.pdf", replace 		



*** Baseline ***
global spec1 ", cl(gvkey) a(year ppe95_bins10_ALT##year)"
global spec2 ", cl(gvkey) a(n2##year ppe95_bins10_ALT##year)"
global spec3 ", cl(gvkey) a(gvkey n2##year ppe95_bins10_ALT##year)"
global spec4 "if exp_sec, cl(gvkey) a(gvkey n2##year ppe95_bins10_ALT##year)"
global spec5 "if exp_ind, cl(gvkey) a(gvkey n2##year ppe95_bins10_ALT##year)"
global spec6 "if exp_ind [aw=DFL], cl(gvkey) a(gvkey n2##year ppe95_bins10_ALT##year)"

global specname1 "Baseline"
global specname2 "Sector FE"
global specname3 "Firm + Sector FE"
global specname4 "Exp. Sector"
global specname5 "Exp. Industry"
global specname6 "Exp. Industry + DFL"

global c1 "navy"
global c2 "dkorange"
global c3 "midgreen"
global c4 "maroon"
global c5 "midblue"
global c6 "red"


*** Store estimates ***
foreach spec in spec1 spec2 spec3 spec4 spec5 spec6 {
reghdfe fs_capx _IyeaX* PR $`spec'
estimates save `spec'_base, replace
		
preserve
	estimates use `spec'_base

	** figure years for the graph
	gen fig_year = 1991 in 1
	foreach i of numlist 1/16 {
		local j = `i' + 1990
		replace fig_year = `j' in `i'
	}
	
	** annual betas for the preperiod
	gen pr_beta = 0
	gen pr_beta_lb = 0
	gen pr_beta_ub = 0
	foreach i of numlist 1/4 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	* zero effect in 1995
	qui lincom  0
	replace pr_beta = r(estimate) in 5

	** annual betas for the postperiod
	foreach i of numlist 6/16 {
		local j = `i' + 1990
		qui lincom _b[_IyeaXPR_`j'] , level(95)
		replace pr_beta = r(estimate) in `i'
		replace pr_beta_lb = r(lb) in `i'
		replace pr_beta_ub = r(ub) in `i'
	}
	
	gen specname = "`spec'"
	keep if _n < 17
	keep fig_year pr_beta* specname
	tempfile `spec'_graph
	save ``spec'_graph', replace	
restore
}


*** Graph Overlay ***
{
preserve
	use `spec1_graph', replace
	local j = 0
	forvalues i = 2/6 {
		local j = `j' + 0.1
		append using `spec`i'_graph'
		replace fig_year = fig_year+`j' if specname=="spec`i'" & fig_year!=1995
	}
		
	graph twoway ///
	 (scatter pr_beta fig_year if specn=="spec1", lco($c1) mco($c1) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec1", lco($c1) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec2", lco($c2) mco($c2) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec2", lco($c2) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec3", lco($c3) mco($c3) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec3", lco($c3) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec4", lco($c4) mco($c4) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec4", lco($c4) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec5", lco($c5) mco($c5) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec5", lco($c5) lpa(solid)) ///
	 (scatter pr_beta fig_year if specn=="spec6", lco($c6) mco($c6) msize(small)) ///
	 (rcap pr_beta_lb pr_beta_ub fig_year if specn=="spec6", lco($c6) lpa(solid)) ///
		, yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))  ///
		plotregion(fcolor(white) lcolor(white)) ///
		graphregion(fcolor(white) lcolor(white))  ///
		 xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
		legend(pos(6)  r(2) on order (1 3 5 7 9 11) ///
		label(1 "$specname1")  ///
		label(3 "$specname2")  ///
		label(5 "$specname3")  ///
		label(7 "$specname4")  ///
		label(9 "$specname5")  ///
		label(11 "$specname6") ) ///
		yti("Effect of S936 on Foreign Share of Investment", margin(medium))
restore	
}
graph export "$output/Graphs/kevin/fs_capx_Overlay_KeepBig_Deciles.pdf", replace 		

