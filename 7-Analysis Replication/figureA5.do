// This file recreates Figure A5
// Author: Dan Garrett
// Date: 3-31-2020

/* Data sources:
various sources including NETS and compustat
*/

clear all
set more off
snapshot erase _all 

*******************************************************
* Panels A and B
*******************************************************
use "$data/Replication Data/figureA5_data", replace

*** labels for some graphs
local fs_capx_name "Foreign Share Investment"
local capex_base_name "Investment/Physical Capital"
local capex_base_p_name "% Change in Investment/Physical Capital"
local capex_base_1_name "Investment/Physical Capital"
local ftax_name "Federal Tax Rate"
  
* combining years and N2
egen yearg = group(year n2)

*figures 5, 6, A5, and A6
xi i.year|PR , noomit
drop _IyeaXPR_1995 

forval i=1990(1)2014 { 
	capture: label var _IyeaXPR_`i' " "
} 
forval i=1991(2)2014 { 
	capture: label var _IyeaXPR_`i' "`i'"
} 
snapshot save
 
***************************************************
* All Panels are the same code run on 3 different variables
***************************************************
foreach y of var  capex_base_p {	//
snapshot restore 1

*running the regression
reghdfe `y' _IyeaX* PR IHS_rev log_tot_emp, cl(gvkey)   a(B=yearg)

** figure years for the graph
gen fig_year = 1991 in 1
foreach i of numlist 1/16 {
local j = `i' + 1990
replace fig_year = `j' in `i'
}

** annual betas for the preperiod
gen pr_beta = 0
gen pr_beta_lb = 0
gen pr_beta_ub = 0
foreach i of numlist 1/4 {
local j = `i' + 1990
lincom _b[_IyeaXPR_`j'] , level(90)
replace pr_beta = r(estimate) in `i'
replace pr_beta_lb = r(lb) in `i' if `i' != 5
replace pr_beta_ub = r(ub) in `i' if `i' != 5
}
* zero effect in 1995
lincom  0
replace pr_beta = r(estimate) in 5

** annual betas for the postperiod
foreach i of numlist 6/16 {
local j = `i' + 1990
lincom _b[_IyeaXPR_`j'] , level(90)
replace pr_beta = r(estimate) in `i'
replace pr_beta_lb = r(lb) in `i' if `i' != 5
replace pr_beta_ub = r(ub) in `i' if `i' != 5
}

* outputting the graph
 graph twoway (scatter pr_beta fig_year, lcolor(navy) mcolor(navy) yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid)) msize(small)) ///
    (rcap pr_beta_lb pr_beta_ub fig_year, lcolor(navy) lpattern(solid)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white))  ///
	xtitle("Year") xlab(1991(2)2006) bgcolor(white) ///
	legend(on order (1 2) label(1 "Puerto Rico Presence")  ///
	label(2 "90% Confidence Interval") r(1)) ///
	yti("Effect of S936 on ``y'_name'", margin(medium))
graph export "$output/Graphs/figure_A5.pdf", replace 
}