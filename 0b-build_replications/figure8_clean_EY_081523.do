// This file creates the data sets for figure 8
// Author: Dan Garrett
// Date: 4-20-2020

/* Data sources:
-NETS
*/

* starting point for all panels is 3-wrds_segments/DFL_nets_emp_2019_06_11_dan

clear all
set more off 
snapshot erase _all

************************************
* Grabbing and organizing the data
************************************
*** get list of hqduns that actually work by dropping gov't etc... 
use "$output_NETS/pr_extract_est.dta", clear

gen naic4 = floor(naics95/100)
gen naic3 = floor(naics95/1000)
gen naic2 = floor(naics95/10000)

********************
*getting rid of governmental and social non-profit orgs
********************
drop if naic2 == 92 | naic3 == 813
* dropping USPS and the federal government
drop if hqduns95 == 3261245 | hqduns95 == 161906193

* dropping firms with HQ in PR
drop if pr_based == 1 

* dropping county industries with no link
drop if pr_link == 0
bysort hqduns: egen temp_max = max(emp95)
gen temp_keep = (emp95 == temp_max)

keep if temp_keep == 1

* making a list of hqduns to use from the panel data
keep hqduns naic3
bysort hqduns: egen m_naic3 = mode(naic3), minmode
drop naic3
duplicates drop 
tempfile tokeep 
save "`tokeep'"

** old code 
use "$output_NETS/pr_extract_panel.dta", clear 

** Keep only firms at start of the sample 
gen temp = emp_PR if year == 1990
bysort hqd: egen base_emp_PR = min(temp)
drop temp 
keep if base > 0  // This line drops 4,991 observations

** MErge in to keep 
merge m:1 hqduns95 using "`tokeep'"
keep if _merge == 3 // This drops 23 observations from the master, 214 from using

egen firm = group(hqd) 
sum firm, d
// snapshot save

keep hqd year emp_US num_est_US m_naic3
gen PR = 1 

tempfile PRdata
save "`PRdata'" 

/// Get control data 
* Get control Data 
use "$output_NETS_control/hq_est_co_firm_master.dta", clear
drop if emp95 < 6
drop if num_est95 < 3

*sample 1000, count by(firm_group)
keep if fipsstate95 != . 
drop if fipsstate95 > 56
rename fipsstate95 hqfips
 
gen firm_group2 = floor(firm_group/10000)
gen census_div = floor(firm_group2/1000)
gen naic3 = firm_group2-1000*census_div
drop firm_group2
gen PR = 0 

rename naic3 m_naic3
drop hqfips census_div  PR firm_group 

foreach y in 90 91 92 93 94 95 96 97 98 99 00 01 02 03 04 05 06 07 08 09 10 11 12 {
	rename emp`y' emp_`y'
	rename num_est`y' num_`y'
}

reshape long emp_ num_ , i(hqduns m_nai) j(year) string
 
destring year , replace 
replace year = year+1900 if year > 50 
replace year = year+2000 if year < 50 
sort year
rename emp_ emp_US
rename num_ num_est_US
gen PR = 0 

append using "`PRdata'"

************************************************
* major sectors and industries (from SOI, matching compustat regressions)
************************************************

tostring m_naic3, gen(n3)
gen naic2 = substr(n3,1,2)
gen major_sec = inlist(naic2,"22","31","32","33","48","49")
gen major_ind = ~inlist(n3,"324","323","337","322","327","336","331","321") * major_sec
drop naic2 n3

// emp growth 
gen temp = emp_US if year == 1995
bysort hqduns95 : egen base_emp = max(temp)
drop temp

gen emp_growth = (emp_US - base_emp)/base_emp
replace emp_growth = 0 if emp_growth == . 

// estab growth 
gen temp = num_est_US if year == 1995
bysort hqduns95 : egen base_est = max(temp)
drop temp

gen est_growth = (num_est_US - base_est)/base_est
replace est_growth = 0 if est_growth == . 

save "$data/Replication Data/figure8_data_EY_081523", replace


// * replicate figure 8; but aw=wgt is wrong, it should be aw=w
// snapshot save
//	
// 	drop if base_emp ==0 
// 	sum base_emp if year == 1995 & PR , d
// 	gen wgt=base_emp/( r(mean)) if PR 
//
// 	sum base_emp if year == 1995 & PR==0 , d
// 	replace wgt=base_emp/( r(mean)) if PR==0 
//
// 	winsor wgt, p(.01) g(w_wgt) 
//
// 	** Make balanced panel by dropping firms that drop out
// 	bys hqduns95 : egen min_emp = min(emp_US) 
// 	drop if min_emp == 0 
//
// 	set matsize 4000
// 	* DFL 
// 	gen naic2 = floor(m_nai/10)
// 	gen pharma = (m_naic3 == 325)
// 	capture: drop q_wgt
// 	xtile q_wgt = base_emp if year == 1995, n(20) 
// 	logit PR i.q_wgt#i.naic2
//
// 	capture: drop phat min_phat w w_w w_wgt
// 	predict phat, pr 
// 	bys hqduns: egen min_phat = min(phat) 
// 	* ATE
// 	* gen w = wgt*(PR/min_phat+(1-PR)/(1-min_phat))
// 	* ATOT
// 	gen w = w_wgt*(PR+(1-PR)*min_phat/(1-min_phat))
//
// 	* keeping variables and saving 
// 	keep emp_growth year PR wgt w hqd
//
// 	xi i.year|PR, noomit 
// 	drop _IyeaXP*1995
//
// 	* employment
// 	est clear
// 	eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
// 	   [aw=w] , a(i.year)  vce(cluster hqduns) noconst      
// 	  estadd local hasy "Yes"    
//
// 	ES_graph , level(95) ti("Original Figure 8 aw=w") yti("Effect of S936 Exposure") ///
// 	note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
// 	outname("$output/Graphs/eddie/NETS_firm_DFL_06_11_2019_r0") ylab("-.12(.02).04")  
//	
//	
// * 95 employment ventiles + NAICS2
// snapshot restore 1
//	
// 	drop if base_emp ==0 
// 	sum base_emp if year == 1995 & PR , d
// 	gen wgt=base_emp/( r(mean)) if PR 
//
// 	sum base_emp if year == 1995 & PR==0 , d
// 	replace wgt=base_emp/( r(mean)) if PR==0 
//
// 	winsor wgt, p(.01) g(w_wgt) 
//
// 	** Make balanced panel by dropping firms that drop out
// 	bys hqduns95 : egen min_emp = min(emp_US) 
// 	drop if min_emp == 0 
//
// 	set matsize 4000
// 	* DFL 
// 	gen naic2 = floor(m_naic3/10)
// 	gen pharma = (m_naic3 == 325)
// 	capture: drop q_wgt
// 	xtile q_wgt = base_emp if year == 1995, n(20) 
// 	logit PR i.q_wgt i.naic2 if year == 1995 
//
// 	capture: drop phat min_phat w w_w w_wgt
// 	predict phat, pr 
// 	bys hqduns: egen min_phat = min(phat) 
//
// 	* ATOT
// 	gen w = w_wgt*(PR+(1-PR)*min_phat/(1-min_phat))
//
// 	* keeping variables and saving 
// 	keep emp_growth year PR wgt w hqd
//
// 	xi i.year|PR, noomit 
// 	drop _IyeaXP*1995
//
// 	* employment
// 	est clear
// 	eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
// 	   [aw=w] , a(i.year)  vce(cluster hqduns) noconst      
// 	  estadd local hasy "Yes"    
//
// 	ES_graph , level(95) ti("95 Emp Ventile + NAICS2") yti("Effect of S936 Exposure") ///
// 	note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
// 	outname("$output/Graphs/eddie/NETS_firm_DFL_95vent_naics2") ylab("-.12(.02).04")  
//
//
// * 95 employment ventiles X NAICS2
// snapshot restore 1
//	
// 	drop if base_emp ==0 
// 	sum base_emp if year == 1995 & PR , d
// 	gen wgt=base_emp/( r(mean)) if PR 
//
// 	sum base_emp if year == 1995 & PR==0 , d
// 	replace wgt=base_emp/( r(mean)) if PR==0 
//
// 	winsor wgt, p(.01) g(w_wgt) 
//
// 	** Make balanced panel by dropping firms that drop out
// 	bys hqduns95 : egen min_emp = min(emp_US) 
// 	drop if min_emp == 0 
//
// 	set matsize 4000
// 	* DFL 
// 	gen naic2 = floor(m_naic3/10)
// 	gen pharma = (m_naic3 == 325)
// 	capture: drop q_wgt
// 	xtile q_wgt = base_emp if year == 1995, n(20) 
// 	logit PR i.q_wgt##i.naic2 if year == 1995
//
// 	capture: drop phat min_phat w w_w w_wgt
// 	predict phat, pr 
// 	bys hqduns: egen min_phat = min(phat) 
//
// 	* ATOT
// 	gen w = w_wgt*(PR+(1-PR)*min_phat/(1-min_phat))
//
// 	* keeping variables and saving 
// 	keep emp_growth year PR wgt w hqd
//
// 	xi i.year|PR, noomit 
// 	drop _IyeaXP*1995
//
// 	* employment
// 	est clear
// 	eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
// 	   [aw=w] , a(i.year)  vce(cluster hqduns) noconst      
// 	  estadd local hasy "Yes"    
//
// 	ES_graph , level(95) ti("95 Emp Ventile X NAICS2") yti("Effect of S936 Exposure") ///
// 	note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
// 	outname("$output/Graphs/eddie/NETS_firm_DFL_95ventXnaics2") ylab("-.12(.02).04")  
//
//
// * 95 employment deciles + NAICS2
// snapshot restore 1
//	
// 	drop if base_emp ==0 
// 	sum base_emp if year == 1995 & PR , d
// 	gen wgt=base_emp/( r(mean)) if PR 
//
// 	sum base_emp if year == 1995 & PR==0 , d
// 	replace wgt=base_emp/( r(mean)) if PR==0 
//
// 	winsor wgt, p(.01) g(w_wgt) 
//
// 	** Make balanced panel by dropping firms that drop out
// 	bys hqduns95 : egen min_emp = min(emp_US) 
// 	drop if min_emp == 0 
//
// 	set matsize 4000
// 	* DFL 
// 	gen naic2 = floor(m_naic3/10)
// 	gen pharma = (m_naic3 == 325)
// 	capture: drop q_wgt
// 	xtile q_wgt = base_emp if year == 1995, n(10) // deciles
// 	logit PR i.q_wgt i.naic2 if year == 1995 
//
// 	capture: drop phat min_phat w w_w w_wgt
// 	predict phat, pr 
// 	bys hqduns: egen min_phat = min(phat) 
//
// 	* ATOT
// 	gen w = w_wgt*(PR+(1-PR)*min_phat/(1-min_phat))
//
// 	* keeping variables and saving 
// 	keep emp_growth year PR wgt w hqd
//
// 	xi i.year|PR, noomit 
// 	drop _IyeaXP*1995
//
// 	* employment
// 	est clear
// 	eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
// 	   [aw=w] , a(i.year)  vce(cluster hqduns) noconst      
// 	  estadd local hasy "Yes"    
//
// 	ES_graph , level(95) ti("95 Emp Decile + NAICS2") yti("Effect of S936 Exposure") ///
// 	note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
// 	outname("$output/Graphs/eddie/NETS_firm_DFL_95dec_naics2") ylab("-.12(.02).04")  
//	
//	
// * 95 employment deciles X NAICS2
// snapshot restore 1
//	
// 	drop if base_emp ==0 
// 	sum base_emp if year == 1995 & PR , d
// 	gen wgt=base_emp/( r(mean)) if PR 
//
// 	sum base_emp if year == 1995 & PR==0 , d
// 	replace wgt=base_emp/( r(mean)) if PR==0 
//
// 	winsor wgt, p(.01) g(w_wgt) 
//
// 	** Make balanced panel by dropping firms that drop out
// 	bys hqduns95 : egen min_emp = min(emp_US) 
// 	drop if min_emp == 0 
//
// 	set matsize 4000
// 	* DFL 
// 	gen naic2 = floor(m_naic3/10)
// 	gen pharma = (m_naic3 == 325)
// 	capture: drop q_wgt
// 	xtile q_wgt = base_emp if year == 1995, n(10) // deciles
// 	logit PR i.q_wgt##i.naic2 if year == 1995
//
// 	capture: drop phat min_phat w w_w w_wgt
// 	predict phat, pr 
// 	bys hqduns: egen min_phat = min(phat) 
//
// 	* ATOT
// 	gen w = w_wgt*(PR+(1-PR)*min_phat/(1-min_phat))
//
// 	* keeping variables and saving 
// 	keep emp_growth year PR wgt w hqd
//
// 	xi i.year|PR, noomit 
// 	drop _IyeaXP*1995
//
// 	* employment
// 	est clear
// 	eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
// 	   [aw=w] , a(i.year)  vce(cluster hqduns) noconst      
// 	  estadd local hasy "Yes"    
//
// 	ES_graph , level(95) ti("95 Emp Decile X NAICS2") yti("Effect of S936 Exposure") ///
// 	note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
// 	outname("$output/Graphs/eddie/NETS_firm_DFL_95decXnaics2") ylab("-.12(.02).04")  
//
//	
//	
// * 95 employment deciles X NAICS3
// snapshot restore 1
//	
// 	drop if base_emp ==0 
// 	sum base_emp if year == 1995 & PR , d
// 	gen wgt=base_emp/( r(mean)) if PR 
//
// 	sum base_emp if year == 1995 & PR==0 , d
// 	replace wgt=base_emp/( r(mean)) if PR==0 
//
// 	winsor wgt, p(.01) g(w_wgt) 
//
// 	** Make balanced panel by dropping firms that drop out
// 	bys hqduns95 : egen min_emp = min(emp_US) 
// 	drop if min_emp == 0 
//
// 	set matsize 4000
// 	* DFL 
// 	gen naic2 = floor(m_naic3/10)
// 	gen pharma = (m_naic3 == 325)
// 	capture: drop q_wgt
// 	xtile q_wgt = base_emp if year == 1995, n(10) // deciles
// 	logit PR i.q_wgt##i.naic2 if year == 1995
//
// 	capture: drop phat min_phat w w_w w_wgt
// 	predict phat, pr 
// 	bys hqduns: egen min_phat = min(phat) 
//
// 	* ATOT
// 	gen w = w_wgt*(PR+(1-PR)*min_phat/(1-min_phat))
//
// 	* keeping variables and saving 
// 	keep emp_growth year PR wgt w hqd
//
// 	xi i.year|PR, noomit 
// 	drop _IyeaXP*1995
//
// 	* employment
// 	est clear
// 	eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
// 	   [aw=w] , a(i.year)  vce(cluster hqduns) noconst      
// 	  estadd local hasy "Yes"    
//
// 	ES_graph , level(95) ti("95 Emp Decile X NAICS3") yti("Effect of S936 Exposure") ///
// 	note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
// 	outname("$output/Graphs/eddie/NETS_firm_DFL_95decXnaics3") ylab("-.12(.02).04")  
//
//
//	
// * 95 Employment ventile + NAICS3
// snapshot restore 1
//	
// 	drop if base_emp ==0 
// 	sum base_emp if year == 1995 & PR , d
// 	gen wgt=base_emp/( r(mean)) if PR 
//
// 	sum base_emp if year == 1995 & PR==0 , d
// 	replace wgt=base_emp/( r(mean)) if PR==0 
//
// 	winsor wgt, p(.01) g(w_wgt) 
//
// 	** Make balanced panel by dropping firms that drop out
// 	bys hqduns95 : egen min_emp = min(emp_US) 
// 	drop if min_emp == 0 
//
// 	set matsize 4000
// 	* DFL 
// 	gen naic2 = floor(m_naic3/10)
// 	gen pharma = (m_naic3 == 325)
// 	capture: drop q_wgt
// 	xtile q_wgt = base_emp if year == 1995, n(20) 
// 	logit PR i.q_wgt##i.m_naic3 if year == 1995
//
// 	capture: drop phat min_phat w w_w w_wgt
// 	predict phat, pr 
// 	bys hqduns: egen min_phat = min(phat) 
// 	* ATE
// 	* gen w = wgt*(PR/min_phat+(1-PR)/(1-min_phat))
// 	* ATOT
// 	gen w = w_wgt*(PR+(1-PR)*min_phat/(1-min_phat))
//
// 	* keeping variables and saving 
// 	keep emp_growth year PR wgt w hqd
//
// 	xi i.year|PR, noomit 
// 	drop _IyeaXP*1995
//
// 	* employment
// 	est clear
// 	eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
// 	   [aw=w] , a(i.year)  vce(cluster hqduns) noconst      
// 	  estadd local hasy "Yes"    
//
// 	ES_graph , level(95) ti("95 Emp ventile + NAICS3") yti("Effect of S936 Exposure") ///
// 	note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
// 	outname("$output/Graphs/eddie/NETS_firm_DFL_95vent_naics3") ylab("-.12(.02).04")  
//	
//
// * base emp as weight
// snapshot restore 1
//	
// 	drop if base_emp ==0 
// 	sum base_emp if year == 1995 & PR , d
// 	gen wgt=base_emp/( r(mean)) if PR 
//
// 	sum base_emp if year == 1995 & PR==0 , d
// 	replace wgt=base_emp/( r(mean)) if PR==0 
//
// 	winsor wgt, p(.01) g(w_wgt) 
//
// 	** Make balanced panel by dropping firms that drop out
// 	bys hqduns95 : egen min_emp = min(emp_US) 
// 	drop if min_emp == 0 
//
// 	set matsize 4000
// 	* DFL 
// 	gen naic2 = floor(m_naic3/10)
// 	gen pharma = (m_naic3 == 325)
// 	capture: drop q_wgt
// 	xtile q_wgt = base_emp if year == 1995, n(20) 
// 	logit PR i.q_wgt##i.naic2 if year == 1995
//
// 	*capture: drop phat min_phat
// 	predict phat, pr 
// 	bys hqduns: egen min_phat = min(phat) 
// 	* ATE
// 	* gen w = wgt*(PR/min_phat+(1-PR)/(1-min_phat))
// 	* ATOT
// 	gen w3 = w_wgt*(PR+(1-PR)*min_phat/(1-min_phat))
//
//
// 	*** Run robustness
// 	sum base_emp if PR==0 & year==1995, detail
// 	sum base_emp if PR==1 & year==1995, detail
//
// 	xi i.year|PR, noomit 
// 	drop _IyeaXP*1995
//	
//	
// 	* employment
// 	* Firm size weighting
// 	est clear
// 	eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
// 	[aw=base_emp] , a(i.year)  vce(cluster hqduns) noconst      
// 	estadd local hasy "Yes"    
//	   
// 	ES_graph , level(95) ti("aw=base_emp") yti("Effect of S936 Exposure") ///
// 	note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
// 	outname("$output/Graphs/eddie/NETS_firm_DFL_base_emp") ylab("-.12(.02).04")   
//	
// * base emp as weight
// snapshot restore 1
//	
// 	drop if base_emp ==0 
// 	sum base_emp if year == 1995 & PR , d
// 	gen wgt=base_emp/( r(mean)) if PR 
//
// 	sum base_emp if year == 1995 & PR==0 , d
// 	replace wgt=base_emp/( r(mean)) if PR==0 
//
// 	winsor wgt, p(.01) g(w_wgt) 
//
// 	** Make balanced panel by dropping firms that drop out
// 	bys hqduns95 : egen min_emp = min(emp_US) 
// 	drop if min_emp == 0 
//
// 	set matsize 4000
// 	* DFL 
// 	gen naic2 = floor(m_naic3/10)
// 	gen pharma = (m_naic3 == 325)
// 	capture: drop q_wgt
// 	xtile q_wgt = base_emp if year == 1995, n(20) 
// 	logit PR i.q_wgt##i.naic2 if year == 1995
//
// 	*capture: drop phat min_phat
// 	predict phat, pr 
// 	bys hqduns: egen min_phat = min(phat) 
// 	* ATE
// 	* gen w = wgt*(PR/min_phat+(1-PR)/(1-min_phat))
// 	* ATOT
// 	gen w3 = w_wgt*(PR+(1-PR)*min_phat/(1-min_phat))
//
//
// 	*** Run robustness
// 	sum base_emp if PR==0 & year==1995, detail
// 	sum base_emp if PR==1 & year==1995, detail
//
// 	xi i.year|PR, noomit 
// 	drop _IyeaXP*1995
//	
//	
// 	* employment
// 	* Firm size weighting
// 	est clear
// 	eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
// 	[aw=wgt] , a(i.year)  vce(cluster hqduns) noconst      
// 	estadd local hasy "Yes"    
//	   
// 	ES_graph , level(95) ti("aw=wgt") yti("Effect of S936 Exposure") ///
// 	note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
// 	outname("$output/Graphs/eddie/NETS_firm_DFL_wgt") ylab("-.12(.02).04") 
