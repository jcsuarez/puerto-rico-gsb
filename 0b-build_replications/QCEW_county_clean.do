// This file creates the data sets for all QCEW County Figures and Tables
// Author: Dan Garrett
// Date: 4-20-2020

/* Data sources:
-NETS and QCEW
*/

/* figures and tables covered by this file:
Figures 11, 12, 13, 14, A13, A14, A15 (A16 and A17 separate)
Tables 6, 7, A12, A13, A14, A15, A16, A17, A18, A21, A22, A23, A24, A25, A33, A34 (A26  separate)
*/

* starting point is technically 2-main_analysis/es_county_aw but stuff added from every file in that directory

clear all
set more off 
snapshot erase _all

*******************************************************
* Baseline dataset
*******************************************************
// A.0. Get regional crosswalk read 
use "$xwalk/cw_cty_czone.dta", clear
rename cty_fips pid 
duplicates drop 
tempfile cross
save "`cross'"

// A.0. Get link data 
use "$output_NETS/pr_link_est_countyXindustry_d.dta", clear 
gen pr_emp = pr_link*total if industry_cd < 13
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)

gen pr_link_ind = tot_pr/tot_emp 
keep pr_link_ind fips
rename  fips pid
duplicates drop 
tempfile pr 
save "`pr'" 

use "$output_NETS/pr_link_est_county_d.dta", clear 
rename  fips pid
tempfile pr2 
save "`pr2'" 

// A.1. Get QCEW data 
use "$qcewdata/extract_qcew_1990_2012_naic3.dta", clear
keep if strlen(industry_code) == 3
drop if industry_code == "101" | industry_code == "102"

order fips_state fips_county year industry_code
drop if fips_county == 0 & fips_state != 72
drop if fips_state == 72 & fips_county != 0
keep if own_code == 5		// 0-total covered; 1 Federal; 2 State; 3 Local; 4 Intl Govt; 5 Private.
keep fips_state fips_county year industry_code annual_avg_estabs annual_avg_emplvl total_annual_wages
drop if fips_state == 78
collapse (sum) annual_avg_estabs (sum) annual_avg_emplvl (sum) total_annual_wages, by(fips_state fips_county year industry_code)

// A.3. Fill in gaps
gen pid = 1000*fips_state+fips_county

// A.6. Merge in pr link 
merge m:1 pid using "`pr'"
keep if _merg == 3
drop _merge 

merge m:1 pid using "`pr2'"
drop if year == . 
drop if pr_link == . 
drop _merge 
rename pr_link pr_link_all

*replace pr_link_all = pr_link_ind

sum pr_link_al, d 

drop if year == . 

*collapse (rawsum) annual_avg_estabs (rawsum) annual_avg_emplvl (rawsum) total_annual_wages (mean) pr_link_ind (mean) pr_link_all [fw=total] , by( fips_state pid year industry_code) // Dan: this isn't actually collapsing anything

egen pid2 = group(pid indus)

tsset pid2 year
tsfill, full

foreach var of varlist fips_state  {
bysort pid2: egen temp = max(`var')
replace `var' = temp
drop temp
}

foreach var of varlist annual_avg_estabs annual_avg_emplvl total_annual_wages {
replace `var' = 0 if `var' == .
}

// A.4.  Growth rates. Base year: 1995
gen temp = annual_avg_emplvl if year == 1995
bysort pid2: egen base_emp = max(temp)
drop temp

gen emp_growth = (annual_avg_emplvl - base_emp)/base
replace emp_growth = 0 if emp_growth == . 


// Income 
gen inc = total_annual_wages
* /annual_avg_emplvl
gen temp = inc if year == 1995
bysort pid2: egen base_inc = max(temp)
drop temp

gen inc_growth = (inc - base_inc)/base_inc
replace inc_growth = 0 if inc_growth == . 

// Estabs 
gen estab = annual_avg_estabs
gen temp = estab if year == 1995
bysort pid2: egen base_estab = max(temp)
drop temp

gen estab_growth = (estab - base_estab)/base_estab
replace estab_growth = 0 if estab_growth == . 

** For now, keep only balanced industries
bys pid2: gen count = _N
keep if count == 23 
drop count 

drop if base_emp ==0 

sum base_emp if year == 1995, d
gen wgt=base_emp/(r(N) * r(mean))
winsor wgt, p(.01) gen(wgt_w)

*normalizing the pr_link measurements
sum pr_link_i if base_emp > 0, d 
replace pr_link_i = pr_link_i/(r(p75)-r(p25))

drop pr_link_a

tsset pid2 year 
egen ind_st = group(ind fips_state)
destring industry_code, replace

** only keeping what is needed
keep emp_growth year pr_link_* wgt industr fips_state pid pid2 wgt_w base_emp annual_avg_estabs annual_avg_emplvl total_annual_wages
 
tempfile baseline
save "`baseline'", replace

*******************************************************
* Adding all of the robustness variables
*******************************************************
*** PER CAPITA EMPLOYMENT GROWTH
merge m:1 pid year using "$additional/county population estimates/county_pop_ests.dta"
keep if _merge == 3
drop _merge

foreach var of varlist annual_avg_estabs annual_avg_emplvl total_annual_wages {
gen temp_`var' = `var' if `var' != .
replace temp_`var' = `var' / tot_pop
replace temp_`var' = 0 if temp_`var' == .
}
* A.4.  Growth rates. Base year: 1995
gen temp = temp_annual_avg_emplvl if year == 1995
bysort pid2: egen base_emp1 = max(temp)
drop temp

gen temp = annual_avg_emplvl * tot_pop if year == 1995
bysort pid2: egen base_pop = max(temp)
drop temp

gen emp_growth_pc = (temp_annual_avg_emplvl - base_emp1)/base_emp1
replace emp_growth_pc = 0 if emp_growth_pc == . 

** weights based on 1995 employment
sum base_pop if year == 1995, d
gen wgt_pc=base_pop/(r(N) * r(mean))
winsor wgt_pc, p(.01) gen(wgt_pc_w)

*  only keeping what is necessary
keep emp_growth* year pr_link_* wgt* industr fips_state pid pid2 wgt_w base_emp annual_avg_estabs annual_avg_emplvl 
save "`baseline'", replace

*** MD (manually deleting large retailers), FD (foreign dropped), s? (size dependent), and employment
use "$output_NETS/pr_link_est_countyXindustry_naic3.dta", clear 
gen pr_emp_MD = pr_link_MD*total if naic3 < 340 & naic3 > 309
gen pr_emp_FD = pr_link_FD*total if naic3 < 340 & naic3 > 309
gen pr_emp_s1 = pr_link_s1*total if naic3 < 340 & naic3 > 309
gen pr_emp_s2 = pr_link_s2*total if naic3 < 340 & naic3 > 309
gen pr_emp_s3 = pr_link_s3*total if naic3 < 340 & naic3 > 309
gen pr_emp_s4 = pr_link_s4*total if naic3 < 340 & naic3 > 309
gen pr_emp_emp = pr_emp95 if naic3 < 340 & naic3 > 309
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr_MD = total(pr_emp_MD)
bys fips: egen tot_pr_FD = total(pr_emp_FD)
bys fips: egen tot_pr_s1 = total(pr_emp_s1)
bys fips: egen tot_pr_s2 = total(pr_emp_s2)
bys fips: egen tot_pr_s3 = total(pr_emp_s3)
bys fips: egen tot_pr_s4 = total(pr_emp_s4)
bys fips: egen tot_pr_emp = total(pr_emp_emp)

capture: drop pr_link*
gen pr_link_MD = tot_pr_MD/tot_emp
gen pr_link_FD = tot_pr_FD/tot_emp
gen pr_link_s1 = tot_pr_s1/tot_emp
gen pr_link_s2 = tot_pr_s2/tot_emp
gen pr_link_s3 = tot_pr_s3/tot_emp
gen pr_link_s4 = tot_pr_s4/tot_emp
gen pr_link_emp = tot_pr_emp/tot_emp 
keep pr_link* fips
rename  fips pid
duplicates drop

merge 1:m pid using "`baseline'"

capture: drop _merge
keep emp_growth* year pr_link_* wgt* industr fips_state pid pid2 wgt_w base_emp annual_avg_estabs annual_avg_emplvl 
save "`baseline'", replace

*** compustat only sample for the links
use "$output_NETS/pr_link_est_countyXindustry_compu.dta", clear 
gen pr_emp = pr_link*total if industry_cd < 13
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)

gen pr_l = tot_pr/tot_emp 
keep pr_l fips
rename  fips pid
rename pr_l pr_link_compu
duplicates drop 

merge 1:m pid using "`baseline'"
capture: drop _merge

keep emp_growth* year pr_link_* wgt* industr fips_state pid pid2 wgt_w base_emp annual_avg_estabs annual_avg_emplvl 
save "`baseline'", replace

*** pharma shock
gen fips = pid
merge m:1 fips using "$data/QCEW/extract_qcew_pharm.dta"
drop if _merge == 2
drop _merge 
drop pharm_annual_avg_estabs pharm_total_annual_wages 
sum pharm_annual_avg_emplvl, d

sum pharm_annual_avg_emplvl if pharm_annual_avg_emplvl > 0 , d 
replace pharm_annual_avg_emplvl = pharm_annual_avg_emplvl/(r(p75)-r(p25))

rename pharm_annual_avg_emplvl pr_link_pharmfake
keep emp_growth* year pr_link_* wgt* industr fips_state pid pid2 wgt_w base_emp annual_avg_estabs annual_avg_emplvl 
save "`baseline'", replace

*** FAKE SHOCK
use "$output_NETS_control/pr_link_est_countyXindustry_d_fake.dta", clear 
gen pr_emp = pr_link*total if industry_cd < 13
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)

gen pr_l = tot_pr/tot_emp 
keep pr_l fips
rename  fips pid
rename pr_l pr_link_fake
duplicates drop 

merge 1:m pid using "`baseline'"
capture: drop _merge

keep emp_growth* year pr_link_* wgt* industr fips_state pid pid2 wgt_w base_emp annual_avg_estabs annual_avg_emplvl 
save "`baseline'", replace

*** 1993 and 1993 still around in 1995
use "$output_NETS/pr_link_est_countyXindustry_n3_93.dta", clear 
gen pr_emp = pr_link*total if inrange(naic3,300,340)
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)

gen pr_l = tot_pr/tot_emp 
keep pr_l fips
rename  fips pid
rename pr_l pr_link_ind93
duplicates drop 

merge 1:m pid using "`baseline'"
capture: drop _merge

keep emp_growth* year pr_link_* wgt* industr fips_state pid pid2 wgt_w base_emp annual_avg_estabs annual_avg_emplvl 
save "`baseline'", replace

use "$output_NETS/pr_link_est_countyXindustry_n3_93_95f.dta", clear 
gen pr_emp = pr_link*total if inrange(naic3,300,340)
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)

gen pr_l = tot_pr/tot_emp 
keep pr_l fips
rename  fips pid
rename pr_l pr_link_ind93
duplicates drop 

merge 1:m pid using "`baseline'"
capture: drop _merge

keep emp_growth* year pr_link_* wgt* industr fips_state pid pid2 wgt_w base_emp annual_avg_estabs annual_avg_emplvl 

* keeping variables and saving 
save "$data/Replication Data/QCEW_county_data", replace

