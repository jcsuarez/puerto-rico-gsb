// This file creates the data sets for table A2. 
// Author: Dan Garrett
// Date: 4-25-2020

/* Data sources:
-Compustat and NETS
*/

* starting point is 1-descriptive/firm_comparison_20190221

clear all
set more off 
snapshot erase _all

************************************
* Grabbing and organizing the data
************************************
use "$WRDS/ETR_compustat.dta", replace

keep if year == 1995

* labeling variables for display
label var PR "Puerto Rico Presence"

label var etr_change "Change in ETR"
label var txfed "Federal Taxes Paid"
label var MNE "Multinational"
label var nol "Net Operating Loss"
label var logAT "Natural Log of Assets"


label var rd "Research and Development"
label var ads "Advertising"
label var ppe "Plants, Property, and Equipment"
label var lev "Leverage Ratio"
label var btd "Book to Debt Ratio"
label var intang "Intangibles"

label var capex "Capital Expenditures"

local General "etr txfed MNE nol logAT"
local Sales "rd ads"
local Assets "ppe lev btd intang"
local Physical "capex"

local vars "`General' `Sales' `Physical' `Assets'"

* some DFL weights
tostring naics, gen(naic3)
gen n3 = substr(naic3,1,3)
xtile rev = sale if year == 1995 , n(20) 
destring n3, replace

keep `vars' PR

* actual dataset for regressions
save "$data/Replication Data/tableA2_data", replace
