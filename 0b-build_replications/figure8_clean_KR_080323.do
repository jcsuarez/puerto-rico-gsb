// This file creates the data sets for figure 8
// Author: Dan Garrett
// Date: 4-20-2020

/* Data sources:
-NETS
*/

* starting point for all panels is 3-wrds_segments/DFL_nets_emp_2019_06_11_dan

* 1. Confirm sample selection process
	* Dropping small firms?
	* Balanced panel?
	* Industries dropped?
	
* 2. Try different versions of weights
	* Employment weights [aw=base_emp]
	* DFL weights
		* Right now: 95 employment ventiles + NAICS2
		* 1. 95 employment ventiles X NAICS2
		* 2. 95 employment deciles X NAICS2
		* 3. 95 employment deciles X NAICS3
		* 4. Compare 95 employment vs. 90-95 mean employment
	
/*

drop if base_emp ==0 

*** Create "relative size" variable ***
sum base_emp if year == 1995 & PR , d
gen wgt=base_emp/( r(mean)) if PR 

sum base_emp if year == 1995 & PR==0 , d
replace wgt=base_emp/( r(mean)) if PR==0 

winsor wgt, p(.01) g(w_wgt) 

** Make balanced panel by dropping firms that drop out
bys hqduns95 : egen min_emp = min(emp_US) 
drop if min_emp == 0 

set matsize 4000
* DFL 
gen naic2 = floor(m_naic3/10)
gen pharma = (m_naic3 == 325)
capture: drop q_wgt
xtile q_wgt = base_emp if year == 1995, n(20) // ventiles
xtile q_wgt_alt = base_emp if year == 1995, n(10) // deciles
logit PR i.q_wgt i.naic2 if year == 1995 
logit PR i.q_wgt##i.naic2 if year == 1995
logit PR i.q_wgt_alt i.naic2 if year == 1995 
logit PR i.q_wgt_alt##i.naic2 if year == 1995

*** Step 1: construct weights ***
logit PR i.q_wgt##i.naic3 if year == 1995
predict phat, pr 
egen min_phat = min(phat), by(hqduns)


* ATOT
gen w = w_wgt*(PR+(1-PR)*min_phat/(1-min_phat))


PR + (1-PR)*W
W = min_phat/(1-min_phat)

*/
	
	

clear all
set more off 
snapshot erase _all


// Define Command for ES graph
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(maroon)) xline(1995, lcolor(black) lpattern(solid))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(solid)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Estimate") label(2 "`level'% CI")) ///
	ytitle("`yti'") ///
	note("`note'") ylab("`ylab'")
graph export "`outname'.pdf", replace	

end 

************************************
* Grabbing and organizing the data
************************************
*** get list of hqduns that actually work by dropping gov't etc... 
use "$output_NETS/pr_extract_est.dta", clear

gen naic4 = floor(naics95/100)
gen naic3 = floor(naics95/1000)
gen naic2 = floor(naics95/10000)

********************
*getting rid of governmental and social non-profit orgs
********************
drop if naic2 == 92 | naic3 == 813
* dropping USPS and the federal government
drop if hqduns95 == 3261245 | hqduns95 == 161906193

* dropping firms with HQ in PR
drop if pr_based == 1 

* dropping county industries with no link
drop if pr_link == 0
bysort hqduns: egen temp_max = max(emp95)
gen temp_keep = (emp95 == temp_max)

keep if temp_keep == 1

* making a list of hqduns to use from the panel data
keep hqduns naic3 naic4
bysort hqduns: egen m_naic3 = mode(naic3), minmode
bysort hqduns: egen m_naic4 = mode(naic4), minmode

drop naic3 naic4
duplicates drop 
tempfile tokeep 
save "`tokeep'"

** old code 
use "$output_NETS/pr_extract_panel.dta", clear 

** Keep only firms at start of the sample 
gen temp = emp_PR if year == 1990
bysort hqd: egen base_emp_PR = min(temp)
drop temp 
keep if base > 0  // This line drops 4,991 observations

** MErge in to keep 
merge m:1 hqduns95 using "`tokeep'"
keep if _merge == 3 // This drops 23 observations from the master, 214 from using

egen firm = group(hqd) 
sum firm, d
snapshot save

keep hqd year emp_US num_est_US m_naic3 m_naic4
gen PR = 1 

tempfile PRdata
save "`PRdata'" 

/// Get control data 
* Get control Data 
use "$output_NETS_control/hq_est_co_firm_master.dta", clear
drop if emp95 < 6
drop if num_est95 < 3

*sample 1000, count by(firm_group)
keep if fipsstate95 != . 
drop if fipsstate95 > 56
rename fipsstate95 hqfips
 
gen firm_group2 = floor(firm_group/10000)
gen census_div = floor(firm_group2/1000)
gen naic3 = firm_group2-1000*census_div
drop firm_group2

gen PR = 0 

rename naic3 m_naic3
drop hqfips census_div  PR firm_group 

foreach y in 90 91 92 93 94 95 96 97 98 99 00 01 02 03 04 05 06 07 08 09 10 11 12 {
	rename emp`y' emp_`y'
	rename num_est`y' num_`y'
}

reshape long emp_ num_ , i(hqduns m_nai) j(year) string
 
destring year , replace 
replace year = year+1900 if year > 50 
replace year = year+2000 if year < 50 
sort year
rename emp_ emp_US
rename num_ num_est_US
gen PR = 0 

append using "`PRdata'"

************************************************
* major sectors and industries (from SOI, matching compustat regressions)
************************************************

tostring m_naic3, gen(n3)
gen naic2 = substr(n3,1,2)
gen major_sec = inlist(naic2,"22","31","32","33","48","49")
gen major_ind = ~inlist(n3,"324","323","337","322","327","336","331","321") * major_sec
drop naic2 n3

// emp growth 
gen temp = emp_US if year == 1995
bysort hqduns95 : egen base_emp = max(temp)
drop temp

gen emp_growth = (emp_US - base_emp)/base_emp
replace emp_growth = 0 if emp_growth == . 

// estab growth 
gen temp = num_est_US if year == 1995
bysort hqduns95 : egen base_est = max(temp)
drop temp

gen est_growth = (num_est_US - base_est)/base_est
replace est_growth = 0 if est_growth == . 


drop if base_emp ==0 
sum base_emp if year == 1995 & PR , d
gen wgt=base_emp/( r(mean)) if PR 

sum base_emp if year == 1995 & PR==0 , d
replace wgt=base_emp/( r(mean)) if PR==0 

winsor wgt, p(.01) g(w_wgt) 

** Make balanced panel by dropping firms that drop out
bys hqduns95 : egen min_emp = min(emp_US) 
drop if min_emp == 0 

set matsize 4000
* DFL 
gen naic2 = floor(m_naic3/10)
gen pharma = (m_naic3 == 325)
capture: drop q_wgt
xtile q_wgt = base_emp if year == 1995, n(20) 
logit PR i.q_wgt i.naic2 if year == 1995 // KEY LINE 

capture: drop phat min_phat w w_w w_wgt
predict phat, pr 
bys hqduns: egen min_phat = min(phat) 
* ATE
* gen w = wgt*(PR/min_phat+(1-PR)/(1-min_phat))
* ATOT
gen w = w_wgt*(PR+(1-PR)*min_phat/(1-min_phat))

gen w2 = base_emp*(PR+(1-PR)*min_phat/(1-min_phat))


* DFL  (original, interact sector and size)
capture: drop q_wgt

xtile q_wgt = base_emp if year == 1995, n(20) 
logit PR i.q_wgt##i.naic2 if year == 1995

*capture: drop phat min_phat
predict phat, pr 
bys hqduns: egen min_phat = min(phat) 
* ATE
* gen w = wgt*(PR/min_phat+(1-PR)/(1-min_phat))
* ATOT
gen w3 = w_wgt*(PR+(1-PR)*min_phat/(1-min_phat))


*** Run robustness
sum base_emp if PR==0 & year==1995, detail
sum base_emp if PR==1 & year==1995, detail

xi i.year|PR, noomit 
drop _IyeaXP*1995


* Firm size weighting (essentially the same as original)
est clear
eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
   [aw=base_emp], a(i.year)  vce(cluster hqduns) noconst      
ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.")  ylab("-.12(.02).04")    

* DFL Weights
est clear
eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
   [aw=w], a(i.year)  vce(cluster hqduns) noconst      
ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
outname("$output/emp_firm_DFL")  ylab("-.12(.02).04")    

est clear
eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
   [aw=w2], a(i.year)  vce(cluster hqduns) noconst      
ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
outname("$output/emp_firm_DFL2")  ylab("-.12(.02).04")    

est clear
eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
   [aw=w3], a(i.year)  vce(cluster hqduns) noconst      
ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
outname("$output/emp_firm_DFL3")  ylab("-.12(.02).04")    

* DFL x major industry?
est clear
eststo emp_0:  reghdfe emp_growth _IyeaXP* if major_sec ///
   [aw=wgt], a(i.year)  vce(cluster hqduns) noconst      
ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.")  ylab("-.12(.02).04")    

est clear
eststo emp_0:  reghdfe emp_growth _IyeaXP* if major_ind ///
   [aw=wgt], a(i.year)  vce(cluster hqduns) noconst      
ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ylab("-.12(.02).04")    

est clear
eststo emp_0:  reghdfe emp_growth _IyeaXP* if major_sec ///
   [aw=w3], a(i.year)  vce(cluster hqduns) noconst      
ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.")  ylab("-.12(.02).04")  

est clear
eststo emp_0:  reghdfe emp_growth _IyeaXP*  if major_ind ///
   [aw=w3], a(i.year)  vce(cluster hqduns) noconst      
ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ylab("-.12(.02).04")    



* employment effects
est clear
eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
   [aw=wgt] , a(i.year)  vce(cluster hqduns) noconst   
ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
outname("$output/emp_firm_original")  ylab("-.12(.02).04")    

est clear
eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
   [aw=wgt] , a(naic2##i.year)  vce(cluster hqduns) noconst   
ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
outname("$output/emp_firm_n2_yearFE")  ylab("-.12(.02).04")   

est clear
eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
   [aw=wgt] , a(m_naic3##i.year)  vce(cluster hqduns) noconst   
ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
outname("$output/emp_firm_n3_yearFE")  ylab("-.12(.02).04")   


* Firm fixed effects   
est clear
eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
   [aw=wgt] , a(hqduns year)  vce(cluster hqduns) noconst      
ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
outname("$output/emp_firm_FE")  ylab("-.12(.02).04")  

est clear
eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
   [aw=wgt] , a(hqduns naic2##i.year)  vce(cluster hqduns) noconst      
ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
outname("$output/emp_firm_FE_n2_yearFE")  ylab("-.12(.02).04")    
  
eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
   [aw=wgt] , a(hqduns m_naic3##i.year)  vce(cluster hqduns) noconst      
ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
outname("$output/emp_firm_FE_n3_yearFE")  ylab("-.12(.02).04")  


  

* employment (drop smallest firms)
est clear
eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
   [aw=wgt] if base_emp >100 , a(i.year)  vce(cluster hqduns) noconst   
ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
outname("$output/emp_firm_original_gt100")  ylab("-.12(.02).04")    

est clear
eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
   [aw=wgt] if base_emp >100, a(naic2##i.year)  vce(cluster hqduns) noconst   
ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
outname("$output/emp_firm_n2_yearFE_gt100")  ylab("-.12(.02).04")   

est clear
eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
   [aw=wgt] if base_emp >100, a(m_naic3##i.year)  vce(cluster hqduns) noconst   
ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
outname("$output/emp_firm_n3_yearFE_gt100")  ylab("-.12(.02).04")   

  

* employment (drop smallest firms)
est clear
eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
   [aw=wgt] if base_emp >200 , a(i.year)  vce(cluster hqduns) noconst   
ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
outname("$output/emp_firm_original_gt200")  ylab("-.12(.02).04")    

est clear
eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
   [aw=wgt] if base_emp >200, a(naic2##i.year)  vce(cluster hqduns) noconst   
ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
outname("$output/emp_firm_n2_yearFE_gt200")  ylab("-.12(.02).04")   

est clear
eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
   [aw=wgt] if base_emp >200, a(m_naic3##i.year)  vce(cluster hqduns) noconst   
ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
outname("$output/emp_firm_n3_yearFE_gt200")  ylab("-.12(.02).04")   

  
    
  
  
*** Try log outcomes
gen log_emp = log(emp_US)
 
 * employment
est clear
eststo emp_0:  reghdfe log_emp _IyeaXP* ///
   [aw=wgt] , a(hqduns i.year)  vce(cluster hqduns) noconst   
ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
outname("$output/log_emp_firm_original")  ylab("-.12(.02).04") 

   

eststo emp_0:  reghdfe log_emp _IyeaXP*  ///
   [aw=wgt] , a(hqduns naic2##i.year)  vce(cluster hqduns) noconst      
ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
outname("$output/log_emp_firm_n2_yearFE")  ylab("-.12(.02).04")    


eststo emp_0:  reghdfe log_emp _IyeaXP*  ///
   [aw=wgt] , a(hqduns m_naic3##i.year)  vce(cluster hqduns) noconst      
ES_graph , level(95) yti("Effect of S936 Exposure") ///
note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
outname("$output/log_emp_firm_n3_yearFE")  ylab("-.12(.02).04")    
  
collapse (mean) emp_US log_emp, by(year PR)
graph twoway (line emp_US year if PR==0)
graph twoway (line emp_US year if PR==1)
graph twoway (line emp_US year if PR==0) (line emp_US year if PR==1)
graph twoway (line log_emp year if PR==0) (line log_emp year if PR==1)


preserve

	eststo emp_0:  reghdfe emp_growth _IyeaXP*  ///
	   [aw=wgt] , a(hqduns naic2##i.year)  vce(cluster hqduns) noconst      
	ES_graph , level(95) yti("Effect of S936 Exposure") ///
	note("Notes: (Emp{sub:it}-Emp{sub:i1995})/Emp{sub:i1995}={&alpha}{sub:t}+{&beta}{sub:t}S936 Exposure{sub:i}+{&epsilon}{sub:it}. SE's clustered by Firm.") ///
	outname("$output/log_emp_firm_n3_yearFE")  ylab("-.12(.02).04")    
	  
restore

* keeping variables and saving 
*keep emp_growth year PR wgt hqd
*save "$data/Replication Data/figure8_data", replace

