// This file creates the data sets for table A4. 
// Author: Dan Garrett
// Date: 4-25-2020

/* Data sources:
-QCEW and NETS
*/

* starting point is 1-descriptive/balance_table (at the end of the file)

clear all
set more off 
snapshot erase _all

************************************
* Grabbing and organizing the data
************************************
********* This file merge additional data and runs balance table

use "$data/data_additional/St Tax Structure 05_10_2017/state_taxes_analysis.dta", clear
keep if year == 1990 
keep fips salestax propertytax corporate_rate GDP rev_totaltaxes rec_val rdcred rdcred trainingsubsidy jobcreationcred corpinctax totalincentives investment_credit 
rename fips statefips
tempfile incentives 
save "`incentives'"


use "$data/data_additional/Misallocation 5_10_2017/misallocation_paneldata.dta", clear
keep if year == 1990 
keep income_rate_avg fipstate
rename fips statefips
tempfile incentives2 
save "`incentives2'"

  
*use "$output/pr_link_est_county_addvars.dta", clear 
use "$output_NETS/pr_link_est_county_addvars.dta", clear 
merge m:1 statefips using "`incentives'"
drop if _merge == 2 
drop _merge 

merge m:1 statefips using "`incentives2'"
drop if _merge == 2 
drop _merge 

** Add nafta variables 
merge 1:1 fips_county using "$output/import_hakobyanlaren_nafta.dta"
drop if _merge == 2 
drop _merge 

gen ln_pop = ln(population ) 
gen ln_GDP = ln(GDP) 
gen ln_rev = ln(rev_to)
gen rev_gdp = rev_tot/GDP 

gen zero_pr = (pr_link == 0) 

**** Balance table and graphs 
label var realmw "Minimum Wage"
label var rgtowork "Right to Work"
label var income_rate_avg "Personal Income Tax"
label var rec_val "R&D Credit" 
label var rev_gdp "State Revenue/GDP"
label var d_tradeusch_pw "Trade Exposure (China)"
label var locdt_noag "Trade Exposure (Nafta)"
label var l_sh_routine33a "Share of Routine Labor"

label var labor_participation_rate "Labor Force Participation"
label var pct_retail "Percentage Retail"
label var pct_agriculture "Percentage Agriculture"
label var pct_manufacturing_durable "Percentage Manufacturing Durable" 
label var pct_manufacturing_nondurable "Percentage Manufacturing Non-Durable"
label var capital_stock "Capital Stock"
label var pct_college "Percentage College"
label var pct_less_HS "Percentage Less than HS"
label var pct_white "Percentage White"
label var pct_black "Percentage Black"

******************* Tables 
est clear 

local base " ln_pop "   
local spec1 " realmw rgtowork income_rate_avg salestax propertytax corpinctax rec_val rev_gdp d_tradeusch_pw l_sh_routine33a" 
// NOTE: locdt_noag removed because that doesn't show up in the paper
local spec2a "labor_participation_rate pct_retail pct_agriculture pct_manufacturing_durable pct_manufacturing_nondurable capital_stock  pct_college  pct_less_HS pct_white  pct_black "
local spec2 "`base' `spec2a'" 

capture: gen cons = 1
local FE1 "cons"
local FE2 "statefips"

gen COV = . 
* actual dataset for regressions
keep `base' `spec1' `spec2a' `FE1' `FE2' pr_link pop statefips
save "$data/Replication Data/tableA4_data", replace
