// This file creates the data sets for table A1. 
// Author: Dan Garrett
// Date: 4-25-2020

/* Data sources:
-SOI
*/

* starting point is 3-wrds_segments/Industry_etr_change_20190604

clear all
set more off 
snapshot erase _all

************************************
* Grabbing and organizing the data
************************************
use "$SOI/aggregate_SOI", clear
replace variable = subinstr(variable,"_t_","",.)
replace variable = substr(variable,1,length(variable)-1) if substr(variable,length(variable)-2,length(variable)) == "_"

drop if variable == "manufact"
drop if variable == "tot_"
egen id = group(variable)

collapse (sum) Us *Income* Receipts (first) variable, by(year id) 

	* defining ETR variable based on income taxes paid (not total taxes paid)
	gen ETR = Income_Tax_After_Credits / Income_Subject_To_Tax // Income_Subject_To_Tax
	gen net_margin = Net_Income / Receipts
	gen tax = Income_Tax_After_Credits
	gen lntax = ln(tax)

	* defining Us credit as a portion of Taxable income
	gen pos_cred = Us_Possessions_Tax_Credit / Income_Subject_To_Tax
	replace pos_cred = 0 if pos_cred == .
	/* identifying major sectors and industries
	sort pos_cred
	br if year == 1995
	"324" petrol,"323" printing,"337" furniture,"322" paper,"327" nonferrous,"336" cars,"331" nonferrous,"321" sawmills
	
	cutoff choosen at 0.001
	*/
	gen raw_pos = Us_Possessions_Tax_Credit
	gen ln_pos = ln(raw_pos)
	
xtset id year	
gen d_pos_cred = pos_cred - l.pos_cred
gen d_ETR = ETR - l.ETR

gen d_raw_pos = raw_pos - l.raw_pos
gen d_tax = tax - l.tax



** Generating weights that are constant over time for each industry based on 1995 levels {
foreach var of var Income_Subject_To_Tax Net_Income Receipts {
gen `var'_temp = `var' if inlist(year,1994,1995)
bys id: egen `var'_base = mean(`var'_temp)
drop *temp

}
rename Income_Subject_To_Tax_base ISTT

** only using sample with variation in S936
keep if year <= 2006

* actual dataset for regressions
save "$data/Replication Data/tableA1_data", replace
