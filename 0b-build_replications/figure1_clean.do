// This file creates the data sets for figure 1. 
// Author: Dan Garrett
// Date: 3-30-2020

/* Data sources:
Panel A: IRS SOI public data
Panel B: IRS SOI public data
Panel C: IRS SOI public data combined with sector level scalars from NETS
*/

* starting point for panels A and B is SOI_import in the build folder to do the data cleaning
use "$SOI/aggregate_SOI", clear
replace variable = subinstr(variable,"_t_","",.)
replace variable = substr(variable,1,length(variable)-1) if substr(variable,length(variable)-2,length(variable)) == "_"

keep if year <=2006
drop if year <1995

drop if variable == "manufact"
drop if variable == "tot_"
replace variable = "man_trans_" if inlist(variable,"man_motor_vehicles_")
replace variable = "man_paper_" if inlist(variable,"man_printing_")
replace variable = "man_food_bev_" if inlist(variable,"man_food_","man_beverage_","man_tobacco_")
replace variable = "man_equipment_" if inlist(variable,"man_instruments_","man_elec_components_","man_machines_","man_comps_")
replace variable = "man_textile_" if inlist(variable,"man_textile_","man_leather_","man_apparel_")
replace variable = "other" if substr(variable,1,4) != "man_" 

collapse (sum) Us , by(year variable) 

*** Find largest ones 
preserve 
collapse (sum) Us , by(variable) 
gsort -Us
gen variable2 = variable
replace variable2 = "man_misc_" if _n >=10
gen group = _n 
replace group = 4 if group >= 10 
drop Us 
tempfile group 
save "`group'"
restore

merge m:1 variable using "`group'"
drop _merge 

replace variable2 = "man_equipment_" if variable2 == "man_mach_et_al_"
replace variable2 = "man_food_bev_" if variable2 == "man_foodbevtob_"
replace variable2 = "man_misc_" if variable2 == "man_comps_elec_"

collapse (sum) Us , by(year variable2 group)  //(first) variable2
sort year group

save "$data/Replication Data/figure1_ab_data", replace

* starting point for panel C is \Puerto Rico\Programs\3-wrds_segments\Industry_etr_change_20190611.do
** importing SOI data
use "$SOI/aggregate_SOI", clear
replace variable = subinstr(variable,"_t_","",.)
replace variable = substr(variable,1,length(variable)-1) if substr(variable,length(variable)-2,length(variable)) == "_"

drop if variable == "manufact"
drop if variable == "tot_"

collapse (sum) Us *Income* Receipts, by(year) 

	* defining ETR variable based on income taxes paid (not total taxes paid)
	gen ETR = Income_Tax_After_Credits / Income_Subject_To_Tax // Income_Subject_To_Tax
	gen net_margin = Net_Income / Receipts
	gen tax = Income_Tax_After_Credits
	gen lntax = ln(tax)

	* defining Us credit as a portion of Taxable income
	gen pos_cred = Us_Possessions_Tax_Credit / Income_Subject_To_Tax
	replace pos_cred = 0 if pos_cred == .
	gen raw_pos = Us_Possessions_Tax_Credit
	gen ln_pos = ln(raw_pos)
	

** Generating weights that are constant over time for each industry based on 1995 levels {
foreach var of var Us_Possessions_Tax_Credit Income_Subject_To_Tax Net_Income Receipts {
gen `var'_temp = `var' if inlist(year,1995) // 
egen `var'_base = mean(`var'_temp)
drop *temp

}
keep if year > 1994 & year < 2008

save "$data/Replication Data/figure1_c_data", replace





