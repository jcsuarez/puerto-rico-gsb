// This file creates the data sets for figure 3. 
// Author: Dan Garrett
// Date: 3-30-2020

/* Data sources:
Panel A: NETS establishment data
Panel B: NETS establishment data
*/

* starting point for both panels is descriptives/maps_d

clear all
set more off 

// Get link data 
use "$output_NETS/pr_link_est_county_d.dta", clear 
rename fips95 pid
gen fips_state = floor(pid/1000)
drop if fips_state == 72
** fixing miami-dade county
replace pid = 12025 if pid == 12086

replace pr = pr*100
drop total

rename pid county 

save "$data/Replication Data/figure3_ab_data", replace




