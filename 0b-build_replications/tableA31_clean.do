// This file creates the data sets for table A31, event study CAR regressions 
// Author: Dan Garrett
// Date: 5-18-2020

/* Data sources:
-NETS, compustat, CRSP
*/

* starting point is technically 4-wrd_value/event_study_ETR_PR_20170115

clear
capture log close
set more off
set matsize 8000

/* Estimating CARs done first and commented out since it takes so long
*stocks1990-1996 is the old file with only exact name matches
use "$WRDS/stocks_1992-1995_PR", replace // this includes non-PR firms
{
***** Organizing data
* making a list of even dates (checked manually for correctness)
local event_dates "12100 13068" // 13083"

*12100 is Feb 16, 1993, the Tuesday after Clinton and Pryor talked
*13068 is 10-12-1995
}

*generating numbers of trading days instead of actual dates
sort permno date
gen year = year(date)
by permno: gen datenum=_n 
foreach event_date in `event_dates' {
by permno: gen target`event_date'=datenum if date==`event_date'
egen td`event_date'=min(target`event_date'), by(permno)
drop target`event_date'
gen dif`event_date'=datenum-td`event_date'
}
* Defining the estimation and event windows (counting windows to make sure they
* 											 actually are trading on those days)
local window = 100
local min_obs = 70
local prewindow = 5

foreach event_date in `event_dates' {
by permno: gen event`event_date'=1 if dif`event_date'>=0 & dif`event_date'<=15 & ret != .
replace event`event_date'=0 if event`event_date' == .

*including some pre-period
by permno: gen pre_event`event_date'=1 if dif`event_date'>=-`prewindow' & dif`event_date'<=15 & ret != .
replace pre_event`event_date'=0 if pre_event`event_date' == .

* estimation windows will also be used to cap the event windows so there is only 1 dummy
* variable when outputting tables.
forvalues i=0(1)5 {
local j = `i' * 3

by permno: gen estimation`event_date'_`i'=1 if dif`event_date'>= -`window' & dif`event_date'<=`j' & ret != .
replace estimation`event_date'_`i'=0 if estimation`event_date'_`i' == .
* cutting out estimation on firms with limited pre-estimation periods
by permno: egen sample_size = sum(estimation`event_date'_`i')
replace estimation`event_date'_`i'=0 if sample_size < `min_obs' 

* Getting a non-event sample for estimating counterfactuals and CARs
by permno: gen pre_est`event_date'_`i'=1 if dif`event_date'>= -`window' & dif`event_date'< 0 & ret != .
replace pre_est`event_date'_`i'=0 if pre_est`event_date'_`i' == .

*dropping those firms without enough observations for estimation in the pre-sample
replace pre_est`event_date'_`i'=0 if sample_size < `min_obs' 
drop sample_size

* generating interactions for graphical explanation of event studies
gen event_int_`event_date'_`i' =  pre_event`event_date' * (dif`event_date' + `prewindow' + 1) * estimation`event_date'_`i'
}
}

expand 2 if estimation12100_5 == 1, gen(eve1)
expand 2 if estimation13068_5 == 1, gen(eve2)

gen pooled_eve = eve1 + eve2
keep if pooled_eve == 1

*Sometimes dividends are given in two lines, dropping those (does not affect return, price, etc.)
duplicates drop permno date, force

tempfile events
save "`events'", replace

*** Pulling in the FF 3 factors, momentum, and risk free rate.
{
* These data also include market returns but they are less precise than those from CRSP

import delimited "$WRDS/F-F_Research_Data_Factors_daily.txt",  clear delim(" ", collapse) stringc(1)
*turning dates into a form that matches what we already have
rename date date_1
gen date = date(date_1, "YMD")
format date %tdNN/DD/CCYY
drop date_1

tempfile factors
save "`factors'", replace

**** merging factors with stock returns
use "`events'", clear

merge m:1 date using "`factors'"
keep if _merge == 3
drop _merge
save "`events'", replace

* now momentum
import delimited "$WRDS/F-F_Momentum_Factor_daily.txt",  clear delim(" ", collapse) stringc(1)
*turning dates into a form that matches what we already have
rename date date_1
gen date = date(date_1, "YMD")
format date %tdNN/DD/CCYY
drop date_1

tempfile factors
save "`factors'", replace

**** merging factors with stock returns
use "`events'", clear

merge m:1 date using "`factors'"
keep if _merge == 3
drop _merge
rename mom MOM

save "`events'", replace
}

**** Importing information regarding company financials 
// ISSUE: merging on year gives mising values, but merging together is probably false
// For now just using 1992 or earliest financials 
*rename cusip cusip_1
*rename ncusip cusip
merge m:1 cusip using "$WRDS/ETR_compustat_1992.dta"
drop if permno == .

************************************************
* Estimating abnormal returns using 4 factors
************************************************
xtset permno datenum

*creating values from previous trading day for value weighting
*NOTE: Some prices are coded as negatives (not -99 that means missing) so I use the absolute value
*gen val = abs(l.prc * l.shrout)
egen id=group(permno eve1)

* Now testing for significance over longer intervals (Directly implementing Coups regressions)
local spec1 "c.mktrf"
local spec2 "c.mktrf c.smb c.hml c.MOM"

set more off
estimates drop _all

* Regression for CARs
forvalues w=3(1)3 {
forvalues k=1(1)2 {
* spec specific samples
local spec1_other "if pre_est12100_5 == 1 "
local spec2_other "if pre_est13068_5 == 1 "
local spec2_other "if pre_est12100_5 == 1 | pre_est13068_5 == 1 "
qui eststo base_reg_`k'_`w': reg ret i.id#(`spec`k'') i.id `spec`w'_other', r
predict rethat_`k'_`w'

gen AR_`k'_`w' = ret - rethat_`k'_`w'
drop rethat_`k'_`w'
}
}

forvalues k=1(1)2 {
forvalues i=0(1)5 {
local j = `i' * 3 + 2

bysort id: egen CAR_`k'_`i'_1 = sum(AR_`k'_1) if dif12100 > -1 & dif12100 < `j'
bysort id: egen CAR_`k'_`i'_2 = sum(AR_`k'_2) if dif13068 > -1 & dif13068 < `j'
bysort id: egen CAR_`k'_`i'_3 = sum(AR_`k'_3) if (dif13068 > -1 & dif13068 < `j') |  (dif12100 > -1 & dif12100 < `j') 

}
*bysort id: egen CAR_`k'_6_1_1 = sum(AR_`k'_`w') if dif12100 > -6 & dif12100 < `j'
}
* collapsing to the firm level
 collapse (firstnm) permno-_merge  AR_1_1-CAR_2_5_3, by(id)

* With all firms, this takes forever to run so I save the collapsed result
save "$WRDS/ETR_event_CARs_PR.dta", replace
*/
 
use "$WRDS/ETR_event_CARs_PR.dta", clear
keep if PR == 1

set more off
estimates drop _all

gen any_rd = (rd>0) & rd !=. 
gen any_ads = (ads>0) & ads !=. 
gen ln_at = logAT

foreach i of var CAR* {
replace `i' = `i'* 100
}

* for now, only base CAPM model
rename naics naicsh_long
tostring naicsh_long,replace
gen naicsh = substr(naicsh_long,1,2)
egen naics=group(naicsh)


******* Additional specifications from meeting on Tuesday (with demeaned x variables)
/*
-table 1 
	- no industry controls (one with, one without)
	-const and ETR, logAT, RD, ADVERT, Ratio of gross profits to operating capital (separately) 
	-do one version with and one with change 
	
	-approximating operating capital as total assets minus intangibles
*/
*gen gpop = gp / (at - intan)
gen gpop = gp / (at)
gen PRexp = emp_PR / (emp_PR + emp_US)
drop etr_1 etr_2

gen day1993 = (eve1 == 1)
gen day1995 = 1 - day1993

* keeping variables and saving 
keep etr-nol btd etr_change any_rd any_ads gpop PRexp CAR_1_3_3 CAR_2_3_3 naics day1993 day1995
save "$data/Replication Data/tableA31_data", replace