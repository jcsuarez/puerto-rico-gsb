// This file estimates semi-elasticities with SOI-estimated tax changes
// includes industry-level variation
// Author: Lysle Boller
// Date: April 25, 2020

clear all
set more off
snapshot erase _all

***********
* Recreate Panel C but keep industry-level variation
***********

* Import SOI data
use "$SOI/aggregate_SOI", clear
replace variable = subinstr(variable,"_t_","",.)
replace variable = substr(variable,1,length(variable)-1) if substr(variable,length(variable)-2,length(variable)) == "_"

drop if variable == "manufact"
drop if variable == "tot_"

gen ETR = Income_Tax_After_Credits / Income_Subject_To_Tax
gen pos_cred = Us_Possessions_Tax_Credit / Income_Subject_To_Tax
replace pos_cred = 0 if pos_cred == .

** Generating weights that are constant over time for each industry based on 1995 levels {
foreach var of var Us_Possessions_Tax_Credit Income_Subject_To_Tax Net_Income Receipts {
bys variable: gen `var'_temp = `var' if inlist(year,1995) //
bys variable: egen `var'_base = mean(`var'_temp)
drop *temp
}

sort variable year
keep if year > 1994 & year < 2008

* renaming variable to be more usable
rename Income_Subject_To_Tax_base ISTT
rename Us_Possessions_Tax_Credit_base USPTC

** aggregate statistics
local base_link = .0923148 // from NETS, share of US employment at firms with operations in PR
local beta_base "0.816" // from column (3) of Table A1

* share of income subject to tax that is covered by the US Possession tax credit
gen share = USPTC / ISTT
* Counterfactual US possessions tax credit holding share of income constant
gen const_cred = share * ISTT
* dollars of lost credits due to the rollback
gen lost_cred = max(0, const_cred - Us_Possessions_Tax_Credit)

* oberserved effective tax rates
* gen p_data = ((Income_Tax_After_Credits + Us_Possessions_Tax_Credit) * `base_link' - Us_Possessions_Tax_Credit) / (Income_Subject_To_Tax * `base_link')
gen p_data = (Income_Tax_After_Credits + Us_Possessions_Tax_Credit) / Income_Subject_To_Tax
* Predicted tax rates had S936 not been repealed, correcting for 20% adjustment to other credits
gen p_new = ((Income_Tax_After_Credits + Us_Possessions_Tax_Credit) * `base_link' - Us_Possessions_Tax_Credit) / (Income_Subject_To_Tax * `base_link')
tempfile soi_data
save "imputed_tax_rates", replace
save "`soi_data'"

*******************************************************
* next using the data from table 2
*******************************************************

* Bunch of observations are being dropped....
use "$data/Replication Data/table2_data", clear
merge m:n n3 using "$output/NAICS_crosswalk"
keep if _merge == 3
drop _merge
merge m:1 year variable using "`soi_data'"

* keep if _merge == 3

****************************
* Calculate counterfactual tax rates
****************************

* Drop groups with no match in the SOI data
bys gvkey: egen has_etr = total(p_data)
keep if has_etr > 0
drop has_etr

* Impute pre-period with earliest tax rate available
foreach var of var p_data p_new {
gen byte missing_`var' = missing(`var')
bys gvkey (missing_`var' year) : gen first_`var' = `var'[1]
bys gvkey (year) : gen missing_pre_`var' = sum(1 - missing_`var') == 0
bys gvkey (year) : replace `var' = first_`var' if missing_pre_`var' == 1
gen ok = 1 - missing_`var'
bys gvkey (ok year) : gen last_`var' = `var'[_N]
bys gvkey (year) : replace `var' = last_`var' if `var' == .
drop ok
drop missing_`var'
drop missing_pre_`var'
}

* Looks like NAICS code 337 is missing year 1997 for some reason
frame put _all, into(temp)
frame temp: keep if p_data == .
frame temp: desc

* Compute counterfactual rates
gen tax_impute = p_data
replace tax_impute = p_new if PR
replace tax_impute = tax_impute * 100
replace capex_base = capex_base * 100
save "temp.dta", replace

****************************
* Regressions
****************************

* major sectors:
local if2_1 = "inlist(n2,3,5,6,7,11,12)"
drop if gvkey == .
xtset gvkey year

save "$data/Replication Data/tax_impute_TEST.dta", replace

// local y = "capex_base"
// frame put _all, into(weights)
// frame weights: keep if year == 1995
// frame weights: collapse (sum)
encode variable, gen(fac_variable)
reghdfe capex_base tax_impute IHS_rev log_tot_emp, cl(gvkey) a(year variable PR)
reghdfe capex_base tax_impute IHS_rev log_tot_emp, cl(gvkey) a(year)
reghdfe capex_base tax_impute IHS_rev log_tot_emp [aweight=ISTT], cl(gvkey) a(year)

egen group = group(fac_variable)
su group, meanonly
foreach i of num 1/`r(max)' {
	reghdfe capex_base tax_impute IHS_rev log_tot_emp if group == `i', cl(gvkey) a(year)
}

est clear
local j = 1
local y = "capex_base"
	eststo reg_`y'_`j'_z: areg `y' ib(last).year tax_impute IHS_rev log_tot_emp  , cl(gvkey)   a(year)
	sum ``y'_sum' if e(sample) & year == 2006, d
		estadd local avg = string(r(mean), "%8.3fc")

	eststo reg_`y'_`j'_a: areg `y' ib(last).year#i.n2 tax_impute IHS_rev log_tot_emp  , cl(gvkey)   a(year)
	sum ``y'_sum' if e(sample) & year == 2006, d
		estadd local avg = string(r(mean), "%8.3fc")

	eststo reg_`y'_`j'_b: xtreg `y' ib(last).year#i.n2 tax_impute IHS_rev log_tot_emp  ,  vce(cl gvkey)  fe
	sum ``y'_sum' if e(sample) & year == 2006, d
		estadd local avg = string(r(mean), "%8.3fc")

	eststo reg_`y'_`j'_d: xtreg `y' ib(last).year#i.n2 tax_impute  IHS_rev log_tot_emp  if  `if2_`j''   , vce(cl gvkey)  fe
	sum ``y'_sum' if e(sample) & year == 2006, d
		estadd local avg = string(r(mean), "%8.3fc")

	eststo reg_`y'_`j'_e: xtreg  `y' ib(last).year#i.n2 tax_impute  IHS_rev log_tot_emp   if  ~inlist(n3,"324","323","337","322","327","336","331","321") & `if2_`j''  , vce(cl gvkey)  fe
	sum ``y'_sum' if e(sample) & year == 2006, d
		estadd local avg = string(r(mean), "%8.3fc")

	eststo reg_`y'_`j'_f: xtreg  `y' ib(last).year#i.n2 tax_impute  IHS_rev log_tot_emp [aw=DFL]  if  ~inlist(n3,"324","323","337","322","327","336","331","321") & `if2_`j''  , vce(cl gvkey)  fe
	sum ``y'_sum'  if e(sample) & year == 2006, d
		estadd local avg = string(r(mean), "%8.3fc")

esttab reg_`y'_1_? using "$output/Tables/table2_onestep_imputed_20210426_industry2.tex", keep(_cons) stats() ///
b(3) se par label ///
noobs nogap nomtitle tex replace nonum nocons postfoot("") prefoot("") posthead("")

esttab reg_`y'_1_? using "$output/Tables/table2_onestep_imputed_20210426_industry2.tex", keep(tax_impute) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) noobs scalars("N Observations" "``y'_scalars1'" ) ///
varlabel(PR_post "\hspace{1em}Exposure to Section 936 X Post" tax_impute "\hspace{1em}Effective Tax Rate")  posth("\hline") ///
preh("\multicolumn{7}{l}{\textbf{``y'_title'}}\\")  postfoot( ///
"\hline Year Fixed Effects    & Y & Y & Y & Y & Y & Y \\ " ///
"NAICS-by-Year Fixed Effects   &  & Y & Y & Y & Y & Y \\ " ///
"`FE_`y'_word' Fixed Effects  &   &  & Y & Y & Y & Y \\ "  ///
"S936 Exposed Sector &  &  & & Y  & Y & Y \\ " ///
"S936 Exposed Industry & & & &  &  Y & Y  \\ " ///
"DFL Weights & & & & & & Y \\  \hline\hline" ///
"\end{tabular} }" ) append sty(tex)
