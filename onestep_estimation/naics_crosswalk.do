use "$SOI/aggregate_SOI", clear
split NAICS02, parse(,  )
keep variable NAICS*
drop NAICS02
duplicates drop
replace NAICS021 = "312" if NAICS021 == "312-312"
replace NAICS022 = "313" if NAICS021 == "313-314"
replace NAICS021 = "314" if NAICS021 == "313-314"
replace NAICS022 = "32" if NAICS021 == "31-33"
replace NAICS023 = "33" if NAICS021 == "31-33"
replace NAICS021 = "31" if NAICS021 == "31-33"
replace NAICS022 = "45" if NAICS021 == "44-45"
replace NAICS021 = "44" if NAICS021 == "44-45"
replace NAICS023 = "49" if NAICS022 == " 48-49"
replace NAICS022 = "48" if NAICS022 == " 48-49"

reshape long NAICS0, i(variable) j(num)
rename NAICS0 NAICS

drop num
drop if NAICS == ""
rename NAICS n3
duplicates drop
save $output/NAICS_crosswalk, replace
