use "$SOI/aggregate_SOI", clear
split NAICS02, parse(,  )
keep variable NAICS*
drop NAICS02

replace variable = subinstr(variable,"_t_","",.)
replace variable = substr(variable,1,length(variable)-1) if substr(variable,length(variable)-2,length(variable)) == "_"

drop if variable == "manufact"
drop if variable == "tot_"

duplicates drop
replace NAICS021 = "312" if NAICS021 == "312-312"
replace NAICS022 = "313" if NAICS021 == "313-314"
replace NAICS021 = "314" if NAICS021 == "313-314"
replace NAICS022 = "32" if NAICS021 == "31-33"
replace NAICS023 = "33" if NAICS021 == "31-33"
replace NAICS021 = "31" if NAICS021 == "31-33"
replace NAICS022 = "45" if NAICS021 == "44-45"
replace NAICS021 = "44" if NAICS021 == "44-45"
replace NAICS023 = "49" if NAICS022 == " 48-49"
replace NAICS022 = "48" if NAICS022 == " 48-49"

reshape long NAICS0, i(variable) j(num)
rename NAICS0 NAICS

drop num
drop if NAICS == ""
rename NAICS n3
replace n3 = strtrim(n3) // test why merge didn't work

*sort n3
*duplicates drop n3, force
save "$output/NAICS_crosswalk", replace
