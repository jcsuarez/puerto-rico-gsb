* This .do file runs replicates Tables A.1 and A.28 for the MVPF calculations
* Author: Lysle Boller
* Date: 2/22/2021

global dropbox "/Users/kevinroberts/Dropbox (Personal)/Puerto Rico"
global puerto_rico "/Users/kevinroberts/Dropbox (Personal)/puerto_rico/"
global dropbox_local "/Users/kevinroberts/Dropbox (Personal)/puerto_rico/codebase"
global replication_data "$dropbox/Data/Replication Data"
global data "$dropbox/Data"
global output "$dropbox/Programs/output"
global analysis "$dropbox/Programs"
global SOI			"$dropbox/Data/SOI Data"
global WRDS			"$dropbox/Data/WRDS"
global output_NETS 	"$dropbox/Programs/output/NETS_20171128"
global output_NETS2 	"$dropbox/Programs/output/NETS_20171129_v2"
global qcewdata		"$dropbox/Data/QCEW"

// * Produces Table A1
// do "$analysis/0b-build_replications/tableA1_clean.do"           // Produce data
// do "$analysis/7-Analysis Replication/tableA1.do"				// SOI regression

// * Produces Table 2
// *** Cannot run this file, because the NETS data location references an external hard drive, not Dropbox
// *** See the `master replication` file for more information
// *** This file outputs the `pr_extract_est` file that is referenced in the `Industry_employment' do file
// *** do "$analysis/MVPF Calculations/nets_pr_extract.do"						// Extract firms with an establishment in PR, runs on external harddrive
// do "$analysis/MVPF Calculations/Industy_employment_2018_10_03.do"   // Creates industry_cat_emp_sums and industry_cat_emp_sums_v2
// do "$analysis/0b-build_replications/table2_clean.do"           // Produce data
// do "$analysis/7-Analysis Replication/table2.do"           // Produce data

// * Produces Figure 5 modified for an ETR event study
// do "$analysis/0b-build_replications/figure5_clean.do"
// do "$analysis/MVPF Calculations/figure5_etr.do"
