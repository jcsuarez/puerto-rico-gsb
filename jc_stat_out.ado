capture: program drop jc_stat_out
program jc_stat_out
syntax , number(real) name(string) replace(real) deci(real) figs(real)

if `replace' == 1 {
file open myfile using "$output/scalars.txt", write replace
file write myfile "\newcommand{\" "`name'" "}[1]{" %`figs'.`deci'fc (`number') "}" _n
file close myfile
}

if `replace' != 1 {
file open myfile using "$output/scalars.txt", write append
file write myfile "\newcommand{\" "`name'" "}[1]{" %`figs'.`deci'fc (`number') "}" _n
file close myfile
}

end
