clear
capture log close
set more off
capture close
snapshot erase _all
set matsize 8000
***************************************************************
//Filename: event_study_ETR.do
//Author: dgg
//Date: 20180105
//Task: Event studies for 2 dates

// This file in particular focuses on the 1993 event for interaction terms
//  using Dyreng et al. control variables.

// This file takes a different approadh than the other event study files that pool the
// estimation of existing relationships and CARs into the same procedure.

// Here, I estimate the CARs first and then regress the CARs on firm characteristics (as in Ohrn)
***************************************************************

*stocks1990-1996 is the old file with only exact name matches
use "$WRDS/stocks_1992-1995_PR", replace // this includes non-PR firms
{
***** Organizing data
* making a list of even dates (checked manually for correctness)
local event_dates "12100" // 13068 13083"
*11647 is 11/21/1991 Pryor introduces the Prescription Drug Cost Containment Act in previous week.
*12053 is 1/3/1993
*12100 is Feb 16, 1993, the Tuesday after Clinton and Pryor talked
*12145 is 4-2-1993
*12183 is 5-10-1993
*12257 is 7-23-1993
*12276 is 8-11-1993
*12913 is 5-10-1995
*13016 is 8-21-1995 (post weekend)
*13040 is 9-14-1995
*13068 is 10-12-1995
*13083 is 10-27-1995 (note the overlap)
*13291 is 5-22-1996
*13338 is july 8, 1996, the monday after the Senate voted (post-weekend)
*13362 is August 1, 1996, congress killed 936
}


*generating numbers of trading days instead of actual dates
sort permno date
gen year = year(date)
by permno: gen datenum=_n 
foreach event_date in `event_dates' {
by permno: gen target`event_date'=datenum if date==`event_date'
egen td`event_date'=min(target`event_date'), by(permno)
drop target`event_date'
gen dif`event_date'=datenum-td`event_date'
}
* Defining the estimation and event windows (counting windows to make sure they
* 											 actually are trading on those days)
local window = 100
local min_obs = 70
local prewindow = 5

foreach event_date in `event_dates' {
by permno: gen event`event_date'=1 if dif`event_date'>=0 & dif`event_date'<=15 & ret != .
replace event`event_date'=0 if event`event_date' == .

*including some pre-period
by permno: gen pre_event`event_date'=1 if dif`event_date'>=-`prewindow' & dif`event_date'<=15 & ret != .
replace pre_event`event_date'=0 if pre_event`event_date' == .

* estimation windows will also be used to cap the event windows so there is only 1 dummy
* variable when outputting tables.
forvalues i=0(1)5 {
local j = `i' * 3

by permno: gen estimation`event_date'_`i'=1 if dif`event_date'>= -`window' & dif`event_date'<=`j' & ret != .
replace estimation`event_date'_`i'=0 if estimation`event_date'_`i' == .
* cutting out estimation on firms with limited pre-estimation periods
by permno: egen sample_size = sum(estimation`event_date'_`i')
replace estimation`event_date'_`i'=0 if sample_size < `min_obs' 

* Getting a non-event sample for estimating counterfactuals and CARs
by permno: gen pre_est`event_date'_`i'=1 if dif`event_date'>= -`window' & dif`event_date'< 0 & ret != .
replace pre_est`event_date'_`i'=0 if pre_est`event_date'_`i' == .

*dropping those firms without enough observations for estimation in the pre-sample
replace pre_est`event_date'_`i'=0 if sample_size < `min_obs' 
drop sample_size

* generating interactions for graphical explanation of event studies
gen event_int_`event_date'_`i' =  pre_event`event_date' * (dif`event_date' + `prewindow' + 1) * estimation`event_date'_`i'
}
}

expand 2 if estimation12100_5 == 1, gen(eve1)
*expand 2 if estimation13068_5 == 1, gen(eve2)

*gen pooled_eve = eve1 + eve2
keep if eve1 == 1

*Sometimes dividends are given in two lines, dropping those (does not affect return, price, etc.)
duplicates drop permno date, force

tempfile events
save "`events'", replace

*** Pulling in the FF 3 factors, momentum, and risk free rate.
{
* These data also include market returns but they are less precise than those from CRSP

import delimited "$WRDS/F-F_Research_Data_Factors_daily.txt",  clear delim(" ", collapse) stringc(1)
*turning dates into a form that matches what we already have
rename date date_1
gen date = date(date_1, "YMD")
format date %tdNN/DD/CCYY
drop date_1

tempfile factors
save "`factors'", replace

**** merging factors with stock returns
use "`events'", clear

merge m:1 date using "`factors'"
keep if _merge == 3
drop _merge
save "`events'", replace

* now momentum
import delimited "$WRDS/F-F_Momentum_Factor_daily.txt",  clear delim(" ", collapse) stringc(1)
*turning dates into a form that matches what we already have
rename date date_1
gen date = date(date_1, "YMD")
format date %tdNN/DD/CCYY
drop date_1

tempfile factors
save "`factors'", replace

**** merging factors with stock returns
use "`events'", clear

merge m:1 date using "`factors'"
keep if _merge == 3
drop _merge
rename mom MOM

save "`events'", replace
}

**** Importing information regarding company financials
merge m:1 cusip using "$WRDS/ETR_compustat_1992.dta"
drop if permno == .

************************************************
* Estimating abnormal returns using 4 factors
************************************************
xtset permno datenum

*creating values from previous trading day for value weighting
*NOTE: Some prices are coded as negatives (not -99 that means missing) so I use the absolute value
*gen val = abs(l.prc * l.shrout)
egen id=group(permno)

* Now testing for significance over longer intervals (Directly implementing Coups regressions)
local spec1 "c.mktrf"
local spec2 "c.mktrf c.smb c.hml c.MOM"

set more off
estimates drop _all

* Regression for Feb 16, 1993
**** second set of FF regressions won't run due to too many variables, still working on solution
***** Potential SOLUTION (TO BE CODED): standard errors dont matter, run the regression for each company individually
forvalues k=1(1)1 {
* spec specific samples
local spec1_other "if pre_est12100_5 == 1 "
local spec2_other "if pre_est12100_5 == 1 "
qui eststo base_reg_`k': reg ret i.id#(`spec`k'') i.id `spec`k'_other', r
predict rethat_`k'

gen AR_`k' = ret - rethat_`k'

forvalues i=0(1)5 {
local j = `i' * 3 + 1

bysort id: egen CAR_`k'_`i' = sum(AR_`k') if dif12100 > -1 & dif12100 < `j'
}
bysort id: egen CAR_`k'_6 = sum(AR_`k') if dif12100 > -6

}

* collapsing to the firm level
 collapse (firstnm) date-CAR_1_6, by(permno)

* With all firms, this takes forever to run so I save the collapsed result
save "$WRDS/ETR_event_CARs_PR.dta", replace
 
use "$WRDS/ETR_event_CARs_PR.dta", clear
keep if PR == 1
* generating some interaction terms for PR to test what is relevant
foreach Q of varlist etr-nol {
qui sum `Q', d
local med = r(p50)
gen upper_`Q' = `Q' > `med'

gen PRxh`Q' = PR * upper_`Q'
}

set more off
estimates drop _all

gen any_rd = (rd>0) & rd !=. 
gen any_ads = (ads>0) & ads !=. 
gen ln_at = logAT
* de-meaning all of the control (including dummies, so interpretation will be relative to average)
foreach i of var etr-nol btd upper_* PRxh* etr_change any_rd any_ads {
qui sum `i', d
gen mean`i' = r(mean)
replace `i' = `i' - mean`i'
drop mean`i'
}

* for now, only base CAPM model
rename naics naicsh_long
tostring naicsh_long,replace
gen naicsh = substr(naicsh_long,1,2)
egen naics=group(naicsh)

/*
*** specifications
local FE1 "PR"
local FE2 "naics"
local FE3 "naics"
local FE4 "naics"
local FE5 "naics"

local title1 "etr"
local title2 "rd"
local title3 "logAT"

* Regressions explaining CAR 
forvalues j=1(1)3 { 
forvalues k=1(1)5 {
forvalues i=0(1)5 {
local spec1 "PR"
local spec2 "PR"
local spec3 "PR `title`j''"
local spec4 "PR etr-nol btd"
local spec5 "PR etr-nol btd upper_`title`j''"

qui eststo reg_`k'_`i'_`title`j'': reg CAR_1_`i' `spec`k'', r a(`FE`k'')
qui eststo reg_`k'_`i'_`title`j''_v: reg CAR_1_`i' `spec`k'', r a(`FE`k'')

}
}
}

* Outputting tables mirroring Ohrn paper
esttab reg_?_3_etr using "$output/ETR_event_20180107_etr.tex", keep(_cons) stats() ///
b(3) se par label coeflabel(event_dummy "\hspace{1em}Event") ///
noobs nogap tex replace nocons postfoot("") nomti

estout reg_?_3_etr using "$output/ETR_event_20180107_etr.tex", keep(_cons etr upper_etr) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) ///
stats(N, nostar /// 
labels("Observations") fmt(%9.0fc 3)) ///
varlabel(_cons "\hspace{1em}PR Presence" etr "\hspace{1em}Effective Tax Rate" upper_etr "\hspace{1em}Above Median ETR" PRxhetr "\hspace{1em}PR x Above Median ETR") ///
preh("\multicolumn{7}{l}{\textbf{Regression Explaining CAR}}\\") prefoot("\hline")  postfoot( ///
"Industry Controls & & Y & Y & Y & Y   \\ " ///
"All Dyreng et al. Controls & & & & Y & Y   \\ \hline " ///
"\end{tabular} }" ) append sty(tex)


* Outputting tables mirroring Ohrn paper
esttab reg_?_3_rd using "$output/ETR_event_20180107_rd.tex", keep(_cons) stats() ///
b(3) se par label coeflabel(event_dummy "\hspace{1em}Event") ///
noobs nogap tex replace nocons postfoot("") nomti

estout reg_?_3_rd using "$output/ETR_event_20180107_rd.tex", keep(_cons rd upper_rd) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) ///
stats(N, nostar /// 
labels("Observations") fmt(%9.0fc 3)) ///
varlabel(_cons "\hspace{1em}PR Presence" rd "\hspace{1em}R\&D" upper_rd "\hspace{1em}Above Median R\&D" PRxhrd "\hspace{1em}PR x Above Median R\&D") ///
preh("\multicolumn{7}{l}{\textbf{Regression Explaining CAR}}\\") prefoot("\hline")  postfoot( ///
"Industry Controls & & Y & Y & Y & Y   \\ " ///
"All Dyreng et al. Controls & & & & Y & Y   \\ \hline " ///
"\end{tabular} }" ) append sty(tex)


* Outputting tables mirroring Ohrn paper
esttab reg_?_3_logAT using "$output/ETR_event_20180107_at.tex", keep(_cons) stats() ///
b(3) se par label coeflabel(event_dummy "\hspace{1em}Event") ///
noobs nogap tex replace nocons postfoot("") nomti

estout reg_?_3_logAT using "$output/ETR_event_20180107_at.tex", keep(_cons logAT upper_logAT) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) ///
stats(N, nostar /// 
labels("Observations") fmt(%9.0fc 3)) ///
varlabel(_cons "\hspace{1em}PR Presence" logAT "\hspace{1em}Log Assets" upper_logAT "\hspace{1em}Above Median Log Assets" PRxhlogAT "\hspace{1em}PR x Above Median Log Assets") ///
preh("\multicolumn{7}{l}{\textbf{Regression Explaining CAR}}\\") prefoot("\hline")  postfoot( ///
"Industry Controls & & Y & Y & Y & Y   \\ " ///
"All Dyreng et al. Controls & & & & Y & Y   \\ \hline " ///
"\end{tabular} }" ) append sty(tex)


* Outputting tables mirroring Ohrn paper
esttab reg_?_3_etr_v using "$output/ETR_event_20180107_etr_V.tex", keep(_cons) stats() ///
b(3) se par label coeflabel(event_dummy "\hspace{1em}Event") ///
noobs nogap tex replace nocons postfoot("") nomti

estout reg_?_3_etr_v using "$output/ETR_event_20180107_etr_V.tex", keep(_cons etr upper_etr) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) ///
stats(N, nostar /// 
labels("Observations") fmt(%9.0fc 3)) ///
varlabel(_cons "\hspace{1em}PR Presence" etr "\hspace{1em}Effective Tax Rate" upper_etr "\hspace{1em}Above Median ETR" PRxhetr "\hspace{1em}PR x Above Median ETR") ///
preh("\multicolumn{7}{l}{\textbf{Regression Explaining CAR}}\\") prefoot("\hline")  postfoot( ///
"Industry Controls & & Y & Y & Y & Y   \\ " ///
"All Dyreng et al. Controls & & & & Y & Y   \\ \hline " ///
"\end{tabular} }" ) append sty(tex)


* Outputting tables mirroring Ohrn paper
esttab reg_?_3_rd_v using "$output/ETR_event_20180107_rd_V.tex", keep(_cons) stats() ///
b(3) se par label coeflabel(event_dummy "\hspace{1em}Event") ///
noobs nogap tex replace nocons postfoot("") nomti

estout reg_?_3_rd_v using "$output/ETR_event_20180107_rd_V.tex", keep(_cons rd upper_rd) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) ///
stats(N, nostar /// 
labels("Observations") fmt(%9.0fc 3)) ///
varlabel(_cons "\hspace{1em}PR Presence" rd "\hspace{1em}R\&D" upper_rd "\hspace{1em}Above Median R\&D" PRxhrd "\hspace{1em}PR x Above Median R\&D") ///
preh("\multicolumn{7}{l}{\textbf{Regression Explaining CAR}}\\") prefoot("\hline")  postfoot( ///
"Industry Controls & & Y & Y & Y & Y  \\ " ///
"All Dyreng et al. Controls & & & & Y & Y  \\ \hline " ///
"\end{tabular} }" ) append sty(tex)


* Outputting tables mirroring Ohrn paper
esttab reg_?_3_logAT_v using "$output/ETR_event_20180107_at_V.tex", keep(_cons) stats() ///
b(3) se par label coeflabel(event_dummy "\hspace{1em}Event") ///
noobs nogap tex replace nocons postfoot("") nomti

estout reg_?_3_logAT_v using "$output/ETR_event_20180107_at_V.tex", keep(_cons logAT upper_logAT) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) ///
stats(N, nostar /// 
labels("Observations") fmt(%9.0fc 3)) ///
varlabel(_cons "\hspace{1em}PR Presence" logAT "\hspace{1em}Log Assets" upper_logAT "\hspace{1em}Above Median Log Assets" PRxhlogAT "\hspace{1em}PR x Above Median Log Assets") ///
preh("\multicolumn{7}{l}{\textbf{Regression Explaining CAR}}\\") prefoot("\hline")  postfoot( ///
"Industry Controls & & Y & Y & Y & Y   \\ " ///
"All Dyreng et al. Controls & & & & Y & Y   \\ \hline " ///
"\end{tabular} }" ) append sty(tex)
*/
******* Additional specifications from meeting on Tuesday (with demeaned x variables)
/*
-table 1 
	- no industry controls (one with, one without)
	-const and ETR, logAT, RD, ADVERT, Ratio of gross profits to operating capital (separately) 
	-do one version with and one with change 
	
	-defining operating capital as total assets minus intangibles
		-SOME SOURCES SAY IT IS ACTUALLY WORKING CAPITAL? check def
*/
*gen gpop = gp / (at - intan)
gen gpop = gp / (at)
gen PRexp = emp_PR / (emp_PR + emp_US)

foreach i of var gpop PRexp {
qui sum `i', d
gen mean`i' = r(mean)
replace `i' = `i' - mean`i'
drop mean`i'
}

/*
local spec1 "PR"
local spec2 "PR etr"
local spec3 "PR logAT"
local spec4 "PR ads"
local spec5 "PR rd"
local spec6 "PR gpop"
local spec7 "PR etr_change"
local spec8 "PR etr logAT ads rd gpop etr_change"
local spec9 "PR etr logAT ads rd gpop etr_change PRexp"


********************* not value Weighted
set more off
estimates drop _all

forvalues k=1(1)9 {
forvalues i=0(1)5 {
// adjusting numbering such 
qui eststo reg_`k'_`i': reg CAR_1_`i' `spec`k'', r

foreach Q of var `spec`k'' {
qui sum `Q' if e(sample), d
gen mean`Q' = r(mean)
replace `Q' = `Q' - mean`Q'
drop mean`Q'
}
qui eststo reg_`k'_`i': 		 reg CAR_1_`i' `spec`k'', r
qui eststo reg_`k'_`i'_naics: 	areg CAR_1_`i' `spec`k'', r a(naics)
}
}
********************* value Weighted

forvalues k=1(1)9 {
forvalues i=0(1)5 {
// adjusting numbering such 
qui eststo reg_`k'_`i'_v: reg CAR_1_`i' `spec`k'' [aweight=at], r

foreach Q of var `spec`k'' {
qui sum `Q' [aweight=at] if e(sample), d
gen mean`Q' = r(mean)
replace `Q' = `Q' - mean`Q'
drop mean`Q'
}

qui eststo reg_`k'_`i'_v: 		 reg CAR_1_`i' `spec`k'' [aweight=at], r
qui eststo reg_`k'_`i'_naics_v: areg CAR_1_`i' `spec`k'' [aweight=at], r a(naics)
}
}

esttab reg_?_3 using "$output/ETR_event_20180110.tex", keep(_cons) stats() ///
b(3) se par label coeflabel(event_dummy "\hspace{1em}Event") ///
noobs nogap tex replace nocons postfoot("") nomti

estout reg_?_3 using "$output/ETR_event_20180110.tex", keep(_cons etr logAT ads rd gpop etr_change PRexp) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) ///
stats(N, nostar /// 
labels("Observations") fmt(%9.0fc 3)) ///
varlabel(_cons "\hspace{1em}PR Presence" logAT "\hspace{1em}Log Assets" etr "\hspace{1em}Effective Tax Rate"  ///
ads "\hspace{1em}Advertising" rd "\hspace{1em}Research \& Development" gpop "\hspace{1em}Gross Profit / Operating Assets"  ///
etr_change "\hspace{1em}Change in ETR"  PRexp "\hspace{1em}Relative PR Employment") ///
preh("\multicolumn{8}{l}{\textbf{Regression Explaining CAR}}\\") prefoot("\hline")  postfoot( ///
"\hline " ///
"\end{tabular} }" ) append sty(tex)



esttab reg_?_3_naics using "$output/ETR_event_20180110_naics.tex", keep(_cons) stats() ///
b(3) se par label coeflabel(event_dummy "\hspace{1em}Event") ///
noobs nogap tex replace nocons postfoot("") nomti

estout reg_?_3_naics using "$output/ETR_event_20180110_naics.tex", keep(_cons etr logAT ads rd gpop etr_change PRexp) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) ///
stats(N, nostar /// 
labels("Observations") fmt(%9.0fc 3)) ///
varlabel(_cons "\hspace{1em}PR Presence" logAT "\hspace{1em}Log Assets" etr "\hspace{1em}Effective Tax Rate"  ///
ads "\hspace{1em}Advertising" rd "\hspace{1em}Research \& Development" gpop "\hspace{1em}Gross Profit / Operating Assets"  ///
etr_change "\hspace{1em}Change in ETR"  PRexp "\hspace{1em}Relative PR Employment") ///
preh("\multicolumn{8}{l}{\textbf{Regression Explaining CAR}}\\") prefoot("\hline")  postfoot( ///
"\hline " ///
"\end{tabular} }" ) append sty(tex)


esttab reg_?_3_v using "$output/ETR_event_20180110_v.tex", keep(_cons) stats() ///
b(3) se par label coeflabel(event_dummy "\hspace{1em}Event") ///
noobs nogap tex replace nocons postfoot("") nomti

estout reg_?_3_v using "$output/ETR_event_20180110_v.tex", keep(_cons etr logAT ads rd gpop etr_change PRexp) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) ///
stats(N, nostar /// 
labels("Observations") fmt(%9.0fc 3)) ///
varlabel(_cons "\hspace{1em}PR Presence" logAT "\hspace{1em}Log Assets" etr "\hspace{1em}Effective Tax Rate"  ///
ads "\hspace{1em}Advertising" rd "\hspace{1em}Research \& Development" gpop "\hspace{1em}Gross Profit / Operating Assets"  ///
etr_change "\hspace{1em}Change in ETR"  PRexp "\hspace{1em}Relative PR Employment") ///
preh("\multicolumn{8}{l}{\textbf{Regression Explaining CAR}}\\") prefoot("\hline")  postfoot( ///
"\hline " ///
"\end{tabular} }" ) append sty(tex)


esttab reg_?_3_naics_v using "$output/ETR_event_20180110_naics_v.tex", keep(_cons) stats() ///
b(3) se par label coeflabel(event_dummy "\hspace{1em}Event") ///
noobs nogap tex replace nocons postfoot("") nomti

estout reg_?_3_naics_v using "$output/ETR_event_20180110_naics_v.tex", keep(_cons etr logAT ads rd gpop etr_change PRexp) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) ///
stats(N, nostar /// 
labels("Observations") fmt(%9.0fc 3)) ///
varlabel(_cons "\hspace{1em}PR Presence" logAT "\hspace{1em}Log Assets" etr "\hspace{1em}Effective Tax Rate"  ///
ads "\hspace{1em}Advertising" rd "\hspace{1em}Research \& Development" gpop "\hspace{1em}Gross Profit / Operating Assets"  ///
etr_change "\hspace{1em}Change in ETR" PRexp "\hspace{1em}Relative PR Employment") ///
preh("\multicolumn{8}{l}{\textbf{Regression Explaining CAR}}\\") prefoot("\hline")  postfoot( ///
"\hline " ///
"\end{tabular} }" ) append sty(tex)


*/
est clear 

qui eststo reg_1:  reg CAR_1_3 rd any_rd  ,   robust

qui eststo reg_2:  reg CAR_1_3 rd any_rd gpop ,  robust

qui eststo reg_3:  reg CAR_1_3 rd any_rd gpop any_ads ,  robust

qui eststo reg_4:  reg CAR_1_3 rd any_rd gpop any_ads etr_c ,  robust

qui eststo reg_5:  reg CAR_1_3 rd any_rd gpop any_ads etr_c PRexp,  robust

qui eststo reg_6:  areg CAR_1_3 rd any_rd any_ads gpop etr_c PRexp , a(naics)  robust
	estadd local ind1 "Yes"

qui eststo reg_7:  areg CAR_1_3 rd any_rd any_ads gpop etr_c PRexp [awe=ln_at] , a(naics)  robust
	estadd local ind1 "Yes"
	estadd local awe  "Yes"

esttab reg_* using "$output/ETR_event_20180110_JC.tex", keep(_cons) stats() ///
b(3) se par label coeflabel(event_dummy "\hspace{1em}Event") ///
noobs nogap tex replace nocons postfoot("") nomti

esttab reg_* using "$output/ETR_event_20180110_JC.tex", keep(_cons rd gpop any_ads etr_change PRexp) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none)  ///
varlabel(_cons "\hspace{1em}PR Presence" logAT "\hspace{1em}Log Assets" etr "\hspace{1em}Effective Tax Rate"  ///
any_ads "\hspace{1em}Any Advertising" rd "\hspace{1em}Research \& Development" gpop "\hspace{1em}Gross Profit / Operating Assets"  ///
etr_change "\hspace{1em}Change in ETR" PRexp "\hspace{1em}Relative PR Employment") ///
scalars( N "ind1 Industry Fixed Effects" "awe Weighted Least Squares") label /// ///
preh("\multicolumn{8}{l}{\textbf{Regression Explaining CAR}}\\") prefoot("\hline")  postfoot( ///
"\hline " ///
"\end{tabular} }" ) append sty(tex)




/* IDEA: variable for exposure to PR

gen PRexp = emp_PR / (emp_PR + emp_US)

areg CAR_1_3 PR etr logAT ads rd gpop etr_change PRexp, r a(naics)
qui sum PRexp if e(sample), d
gen meanPRexp = r(mean)
replace PRexp = PRexp - meanPRexp
drop meanPRexp
