clear
capture log close
set more off
capture close
snapshot erase _all
set matsize 8000
***************************************************************
//Filename: event_study_ETR.do
//Author: dgg
//Date: 20180105
//Task: Event studies for 2 dates

// This file in particular focuses on the 1993 event for interaction terms
//  using Dyreng et al. control variables.

// This file takes a different approadh than the other event study files that pool the
// estimation of existing relationships and CARs into the same procedure.

// Here, I estimate the CARs first and then regress the CARs on firm characteristics (as in Ohrn)
***************************************************************

*stocks1990-1996 is the old file with only exact name matches
use "$WRDS/stocks_1992-1995_PR", replace // this includes non-PR firms
{
***** Organizing data
* making a list of even dates (checked manually for correctness)
local event_dates "12100" // 13068 13083"
*11647 is 11/21/1991 Pryor introduces the Prescription Drug Cost Containment Act in previous week.
*12053 is 1/3/1993
*12100 is Feb 16, 1993, the Tuesday after Clinton and Pryor talked
*12145 is 4-2-1993
*12183 is 5-10-1993
*12257 is 7-23-1993
*12276 is 8-11-1993
*12913 is 5-10-1995
*13016 is 8-21-1995 (post weekend)
*13040 is 9-14-1995
*13068 is 10-12-1995
*13083 is 10-27-1995 (note the overlap)
*13291 is 5-22-1996
*13338 is july 8, 1996, the monday after the Senate voted (post-weekend)
*13362 is August 1, 1996, congress killed 936
}


*generating numbers of trading days instead of actual dates
sort permno date
gen year = year(date)
by permno: gen datenum=_n 
foreach event_date in `event_dates' {
by permno: gen target`event_date'=datenum if date==`event_date'
egen td`event_date'=min(target`event_date'), by(permno)
drop target`event_date'
gen dif`event_date'=datenum-td`event_date'
}
* Defining the estimation and event windows (counting windows to make sure they
* 											 actually are trading on those days)
local window = 100
local min_obs = 70
local prewindow = 5

foreach event_date in `event_dates' {
by permno: gen event`event_date'=1 if dif`event_date'>=0 & dif`event_date'<=15 & ret != .
replace event`event_date'=0 if event`event_date' == .

*including some pre-period
by permno: gen pre_event`event_date'=1 if dif`event_date'>=-`prewindow' & dif`event_date'<=15 & ret != .
replace pre_event`event_date'=0 if pre_event`event_date' == .

* estimation windows will also be used to cap the event windows so there is only 1 dummy
* variable when outputting tables.
forvalues i=0(1)5 {
local j = `i' * 3

by permno: gen estimation`event_date'_`i'=1 if dif`event_date'>= -`window' & dif`event_date'<=`j' & ret != .
replace estimation`event_date'_`i'=0 if estimation`event_date'_`i' == .
* cutting out estimation on firms with limited pre-estimation periods
by permno: egen sample_size = sum(estimation`event_date'_`i')
replace estimation`event_date'_`i'=0 if sample_size < `min_obs' 

* Getting a non-event sample for estimating counterfactuals and CARs
by permno: gen pre_est`event_date'_`i'=1 if dif`event_date'>= -`window' & dif`event_date'< 0 & ret != .
replace pre_est`event_date'_`i'=0 if pre_est`event_date'_`i' == .

*dropping those firms without enough observations for estimation in the pre-sample
replace pre_est`event_date'_`i'=0 if sample_size < `min_obs' 
drop sample_size

* generating interactions for graphical explanation of event studies
gen event_int_`event_date'_`i' =  pre_event`event_date' * (dif`event_date' + `prewindow' + 1) * estimation`event_date'_`i'
}
}

expand 2 if estimation12100_5 == 1, gen(eve1)
*expand 2 if estimation13068_5 == 1, gen(eve2)

*gen pooled_eve = eve1 + eve2
keep if eve1 == 1

*Sometimes dividends are given in two lines, dropping those (does not affect return, price, etc.)
duplicates drop permno date, force

tempfile events
save "`events'", replace

*** Pulling in the FF 3 factors, momentum, and risk free rate.
{
* These data also include market returns but they are less precise than those from CRSP

import delimited "$WRDS/F-F_Research_Data_Factors_daily.txt",  clear delim(" ", collapse) stringc(1)
*turning dates into a form that matches what we already have
rename date date_1
gen date = date(date_1, "YMD")
format date %tdNN/DD/CCYY
drop date_1

tempfile factors
save "`factors'", replace

**** merging factors with stock returns
use "`events'", clear

merge m:1 date using "`factors'"
keep if _merge == 3
drop _merge
save "`events'", replace

* now momentum
import delimited "$WRDS/F-F_Momentum_Factor_daily.txt",  clear delim(" ", collapse) stringc(1)
*turning dates into a form that matches what we already have
rename date date_1
gen date = date(date_1, "YMD")
format date %tdNN/DD/CCYY
drop date_1

tempfile factors
save "`factors'", replace

**** merging factors with stock returns
use "`events'", clear

merge m:1 date using "`factors'"
keep if _merge == 3
drop _merge
rename mom MOM

save "`events'", replace
}

**** Importing information regarding company financials
merge m:1 cusip using "$WRDS/ETR_compustat_1992.dta"
drop if permno == .

************************************************
* Estimating abnormal returns using 4 factors
************************************************
xtset permno datenum

*creating values from previous trading day for value weighting
*NOTE: Some prices are coded as negatives (not -99 that means missing) so I use the absolute value
*gen val = abs(l.prc * l.shrout)
egen id=group(permno)

* Now testing for significance over longer intervals (Directly implementing Coups regressions)
local spec1 "c.mktrf"
local spec2 "c.mktrf c.smb c.hml c.MOM"

set more off
estimates drop _all

* Regression for Feb 16, 1993
**** second set of FF regressions won't run due to too many variables, still working on solution
***** Potential SOLUTION (TO BE CODED): standard errors dont matter, run the regression for each company individually
forvalues k=1(1)1 {
* spec specific samples
local spec1_other "if pre_est12100_5 == 1 "
local spec2_other "if pre_est12100_5 == 1 "
qui eststo base_reg_`k': reg ret i.id#(`spec`k'') i.id `spec`k'_other', r
predict rethat_`k'

gen AR_`k' = ret - rethat_`k'

forvalues i=0(1)5 {
local j = `i' * 3 + 1

bysort id: egen CAR_`k'_`i' = sum(AR_`k') if dif12100 > -1 & dif12100 < `j'
}
bysort id: egen CAR_`k'_6 = sum(AR_`k') if dif12100 > -6

}

* collapsing to the firm level
 collapse (firstnm) date-CAR_1_6, by(permno)

* With all firms, this takes forever to run so I save the collapsed result
save "$WRDS/ETR_event_CARs_PR.dta", replace
 
use "$WRDS/ETR_event_CARs_PR.dta", clear
keep if PR == 1
* generating some interaction terms for PR to test what is relevant
foreach Q of varlist etr-nol {
qui sum `Q', d
local med = r(p50)
gen upper_`Q' = `Q' > `med'

gen PRxh`Q' = PR * upper_`Q'
}

set more off
estimates drop _all

* for now, only base CAPM model has been estimated for CARs

rename naics naicsh_long
tostring naicsh_long,replace
gen naicsh = substr(naicsh_long,1,2)
egen naics=group(naicsh)

local spec1 "PR"
local spec2 "PR i.naics"
local spec3 "PR i.naics etr"
local spec4 "PR i.naics etr-nol btd"
local spec5 "PR i.naics etr-nol btd upper_etr"
local spec6 "PR i.naics etr-nol btd upper_etr PRxhetr"
reg CAR_1_3 `spec6', r


* Regressions explaining CAR 
forvalues k=1(1)6 {
forvalues i=0(1)5 {
// adjusting numbering such 
qui eststo reg_`k'_`i'_etr: reg CAR_1_`i' `spec`k'', r
}
}
local spec1 "PR"
local spec2 "PR i.naics"
local spec3 "PR i.naics rd"
local spec4 "PR i.naics etr-nol btd"
local spec5 "PR i.naics etr-nol btd upper_rd"
local spec6 "PR i.naics etr-nol btd upper_rd PRxhrd"

* Regressions explaining CAR 
forvalues k=1(1)6 {
forvalues i=0(1)5 {
// adjusting numbering such 
qui eststo reg_`k'_`i'_rd: reg CAR_1_`i' `spec`k'', r
}
}

local spec1 "PR"
local spec2 "PR i.naics"
local spec3 "PR i.naics logAT"
local spec4 "PR i.naics etr-nol btd"
local spec5 "PR i.naics etr-nol btd upper_logAT"
local spec6 "PR i.naics etr-nol btd upper_logAT PRxhlogAT"

* Regressions explaining CAR 
forvalues k=1(1)6 {
forvalues i=0(1)5 {
// adjusting numbering such 
qui eststo reg_`k'_`i'_at: reg CAR_1_`i' `spec`k'', r
}
}


* Outputting tables mirroring Ohrn paper
esttab reg_?_3_etr using "$output/ETR_event_20180107_etr.tex", keep(_cons) stats() ///
b(3) se par label coeflabel(event_dummy "\hspace{1em}Event") ///
noobs nogap tex replace nocons postfoot("") nomti

estout reg_?_3_etr using "$output/ETR_event_20180107_etr.tex", keep(_cons etr upper_etr) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) ///
stats(N, nostar /// 
labels("Observations") fmt(%9.0fc 3)) ///
varlabel(_cons "\hspace{1em}PR Presence" etr "\hspace{1em}Effective Tax Rate" upper_etr "\hspace{1em}Above Median ETR" PRxhetr "\hspace{1em}PR x Above Median ETR") ///
preh("\multicolumn{7}{l}{\textbf{Regression Explaining CAR}}\\") prefoot("\hline")  postfoot( ///
"Industry Controls & & Y & Y & Y & Y & Y  \\ " ///
"All Dyreng et al. Controls & & & & Y & Y & Y  \\ \hline " ///
"\end{tabular} }" ) append sty(tex)


* Outputting tables mirroring Ohrn paper
esttab reg_?_3_rd using "$output/ETR_event_20180107_rd.tex", keep(_cons) stats() ///
b(3) se par label coeflabel(event_dummy "\hspace{1em}Event") ///
noobs nogap tex replace nocons postfoot("") nomti

estout reg_?_3_rd using "$output/ETR_event_20180107_rd.tex", keep(_cons rd upper_rd) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) ///
stats(N, nostar /// 
labels("Observations") fmt(%9.0fc 3)) ///
varlabel(_cons "\hspace{1em}PR Presence" rd "\hspace{1em}R\&D" upper_rd "\hspace{1em}Above Median R\&D" PRxhrd "\hspace{1em}PR x Above Median R\&D") ///
preh("\multicolumn{7}{l}{\textbf{Regression Explaining CAR}}\\") prefoot("\hline")  postfoot( ///
"Industry Controls & & Y & Y & Y & Y & Y  \\ " ///
"All Dyreng et al. Controls & & & & Y & Y & Y  \\ \hline " ///
"\end{tabular} }" ) append sty(tex)


* Outputting tables mirroring Ohrn paper
esttab reg_?_3_at using "$output/ETR_event_20180107_at.tex", keep(_cons) stats() ///
b(3) se par label coeflabel(event_dummy "\hspace{1em}Event") ///
noobs nogap tex replace nocons postfoot("") nomti

estout reg_?_3_at using "$output/ETR_event_20180107_at.tex", keep(_cons logAT upper_logAT) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) ///
stats(N, nostar /// 
labels("Observations") fmt(%9.0fc 3)) ///
varlabel(_cons "\hspace{1em}PR Presence" logAT "\hspace{1em}Log Assets" upper_logAT "\hspace{1em}Above Median Log Assets" PRxhlogAT "\hspace{1em}PR x Above Median Log Assets") ///
preh("\multicolumn{7}{l}{\textbf{Regression Explaining CAR}}\\") prefoot("\hline")  postfoot( ///
"Industry Controls & & Y & Y & Y & Y & Y  \\ " ///
"All Dyreng et al. Controls & & & & Y & Y & Y  \\ \hline " ///
"\end{tabular} }" ) append sty(tex)


********************* value Weighted
set more off
estimates drop _all

local spec1 "PR"
local spec2 "PR i.naics"
local spec3 "PR i.naics etr"
local spec4 "PR i.naics etr-nol btd"
local spec5 "PR i.naics etr-nol btd upper_etr"
local spec6 "PR i.naics etr-nol btd upper_etr PRxhetr"
reg CAR_1_3 `spec6', r


* Regressions explaining CAR 
forvalues k=1(1)6 {
forvalues i=0(1)5 {
// adjusting numbering such 
qui eststo reg_`k'_`i'_etr: reg CAR_1_`i' `spec`k'' [aweight=at], r
}
}
local spec1 "PR"
local spec2 "PR i.naics"
local spec3 "PR i.naics rd"
local spec4 "PR i.naics etr-nol btd"
local spec5 "PR i.naics etr-nol btd upper_rd"
local spec6 "PR i.naics etr-nol btd upper_rd PRxhrd"

* Regressions explaining CAR 
forvalues k=1(1)6 {
forvalues i=0(1)5 {
// adjusting numbering such 
qui eststo reg_`k'_`i'_rd: reg CAR_1_`i' `spec`k'' [aweight=at], r
}
}

local spec1 "PR"
local spec2 "PR i.naics"
local spec3 "PR i.naics logAT"
local spec4 "PR i.naics etr-nol btd"
local spec5 "PR i.naics etr-nol btd upper_logAT"
local spec6 "PR i.naics etr-nol btd upper_logAT PRxhlogAT"

* Regressions explaining CAR 
forvalues k=1(1)6 {
forvalues i=0(1)5 {
// adjusting numbering such 
qui eststo reg_`k'_`i'_at: reg CAR_1_`i' `spec`k'' [aweight=at], r
}
}


* Outputting tables mirroring Ohrn paper
esttab reg_?_3_etr using "$output/ETR_event_20180107_etr_V.tex", keep(_cons) stats() ///
b(3) se par label coeflabel(event_dummy "\hspace{1em}Event") ///
noobs nogap tex replace nocons postfoot("") nomti

estout reg_?_3_etr using "$output/ETR_event_20180107_etr_V.tex", keep(_cons etr upper_etr) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) ///
stats(N, nostar /// 
labels("Observations") fmt(%9.0fc 3)) ///
varlabel(_cons "\hspace{1em}PR Presence" etr "\hspace{1em}Effective Tax Rate" upper_etr "\hspace{1em}Above Median ETR" PRxhetr "\hspace{1em}PR x Above Median ETR") ///
preh("\multicolumn{7}{l}{\textbf{Regression Explaining CAR}}\\") prefoot("\hline")  postfoot( ///
"Industry Controls & & Y & Y & Y & Y & Y  \\ " ///
"All Dyreng et al. Controls & & & & Y & Y & Y  \\ \hline " ///
"\end{tabular} }" ) append sty(tex)


* Outputting tables mirroring Ohrn paper
esttab reg_?_3_rd using "$output/ETR_event_20180107_rd_V.tex", keep(_cons) stats() ///
b(3) se par label coeflabel(event_dummy "\hspace{1em}Event") ///
noobs nogap tex replace nocons postfoot("") nomti

estout reg_?_3_rd using "$output/ETR_event_20180107_rd_V.tex", keep(_cons rd upper_rd) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) ///
stats(N, nostar /// 
labels("Observations") fmt(%9.0fc 3)) ///
varlabel(_cons "\hspace{1em}PR Presence" rd "\hspace{1em}R\&D" upper_rd "\hspace{1em}Above Median R\&D" PRxhrd "\hspace{1em}PR x Above Median R\&D") ///
preh("\multicolumn{7}{l}{\textbf{Regression Explaining CAR}}\\") prefoot("\hline")  postfoot( ///
"Industry Controls & & Y & Y & Y & Y & Y  \\ " ///
"All Dyreng et al. Controls & & & & Y & Y & Y  \\ \hline " ///
"\end{tabular} }" ) append sty(tex)


* Outputting tables mirroring Ohrn paper
esttab reg_?_3_at using "$output/ETR_event_20180107_at_V.tex", keep(_cons) stats() ///
b(3) se par label coeflabel(event_dummy "\hspace{1em}Event") ///
noobs nogap tex replace nocons postfoot("") nomti

estout reg_?_3_at using "$output/ETR_event_20180107_at_V.tex", keep(_cons logAT upper_logAT) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) ///
stats(N, nostar /// 
labels("Observations") fmt(%9.0fc 3)) ///
varlabel(_cons "\hspace{1em}PR Presence" logAT "\hspace{1em}Log Assets" upper_logAT "\hspace{1em}Above Median Log Assets" PRxhlogAT "\hspace{1em}PR x Above Median Log Assets") ///
preh("\multicolumn{7}{l}{\textbf{Regression Explaining CAR}}\\") prefoot("\hline")  postfoot( ///
"Industry Controls & & Y & Y & Y & Y & Y  \\ " ///
"All Dyreng et al. Controls & & & & Y & Y & Y  \\ \hline " ///
"\end{tabular} }" ) append sty(tex)





/*
forvalues k=1(1)4 {
forvalues i=0(1)5 {
* spec specific samples
local spec1_other "if estimation12100_`i' == 1 "
local spec2_other "if estimation12100_`i' == 1 "
local spec3_other "etr if estimation12100_`i' == 1 "
local spec4_other "etr-nol if estimation12100_`i' == 1 "
local j = `i' * 3
qui gen event_dummy= event12100 / (1 + `j') // + event13068 / (1 + `j')
qui eststo reg_`i'_`k': areg ret i.id#(`spec`k'') event_dummy `spec`k'_other', r a(id)
drop event_dummy
}

*Using the pre-period for a seventh column
local spec1_other " "
local spec2_other " "
local spec3_other "etr "
local spec4_other "etr-nol "
qui gen event_dummy= pre_event12100 / (21) // + pre_event13068 / (21)
qui eststo reg_6_`k': areg ret i.id#(`spec`k'') event_dummy `spec`k'_other', r a(id)
drop event_dummy
}


***********************************************************
* Making a graph for the 16 day version of each ( i = 5 )
***********************************************************

set more off
local spec1 "c.mktrf"
local spec2 "`spec1' c.smb c.hml c.MOM"
local event_dates "12100 13068"

local i = 5
forvalues k=1(1)2 {
* with lags and leads
gen x`k' = 0
replace x`k' = event_int_12100_`i' + event_int_13068_`i'
eststo reg_`k': reg ret i.id i.id#(`spec`k'') ib0.x`k' if estimation12100_`i'==1 | ///
 estimation13068_`i'==1,  r

}

*
* Aggregating up individual estimates of AR into CAR (Matching Investment graph style
forvalues k=1(1)2 {

* part 1
estimates restore reg_`k'
gen fig_b_`k'_pre = 0
gen fig_se_`k'_pre = 0
gen fig_b_`k' = 0
gen fig_se_`k' = 0

*counting up from t = -prewindow
local agg "1.x"
forvalues i=1(1)21 {
lincom (`agg')*100

local j = `i' + 1
replace fig_b_`k'_pre=r(estimate) in `j'
replace fig_se_`k'_pre=r(se) in `j'
local agg "`agg' + `j'.x"
}
*counting up from the event
local agg "6.x"
forvalues i=6(1)21 {
lincom (`agg')*100

local j = `i' + 1
replace fig_b_`k'=r(estimate) in `j'
replace fig_se_`k'=r(se) in `j'
local agg "`agg' + `j'.x"
}
gen fig_upper_`k'_pre=fig_b_`k'_pre+1.645*fig_se_`k'_pre
gen fig_lower_`k'_pre=fig_b_`k'_pre-1.645*fig_se_`k'_pre
gen fig_upper_`k'=fig_b_`k'+1.645*fig_se_`k'
gen fig_lower_`k'=fig_b_`k'-1.645*fig_se_`k'
}
gen fig_t = _n - 7 in 1/22
gen fig_t2 = fig_t + .2

* Actual graphs
twoway (line fig_b_1_pre fig_t if fig_t<16 & fig_t>-8, msymbol(diamond) xline(-0.0, lc(gs11)) c(l) lpattern(dash) lcolor(cranberry) lwidth(medthick)) ///
	(rcap fig_upper_1_pre fig_lower_1_pre fig_t  if fig_t<16 & fig_t>-8, lpattern(dash) lcolor(cranberry) c(l) m(i) lwidth(medthick) yline(0, lcolor(black))) ///
	(line fig_b_1 fig_t2 if fig_t<16 & fig_t>-8, msymbol(diamond) c(l) lpattern(solid) lcolor(navy) lwidth(medthick)) ///
	(rcap fig_upper_1 fig_lower_1 fig_t2  if fig_t<16 & fig_t>-8, lpattern(solid) lcolor(navy) c(l) m(i) lwidth(medthick)), /// 
	xlabel(-6 (3) 15) ///
	graphregion(fcolor(white)) ///
	xtitle("Days from Event") ///
	ytitle("CAR (Percent)") ///
	legend(order(1 2 3 4) label(1 "Cumulative Abnormal Return with Leads")  label(2 "90% CI") ///
	label(3 "Cumulative Abnormal Return without Leads") label(4 "90% CI"))
graph export "$output/pooled_event_20171214_CAPM.pdf", replace

twoway (line fig_b_2_pre fig_t if fig_t<16 & fig_t>-8, msymbol(diamond) xline(-0.0, lc(gs11)) c(l) lpattern(dash) lcolor(cranberry) lwidth(medthick)) ///
	(rcap fig_upper_2_pre fig_lower_2_pre fig_t  if fig_t<16 & fig_t>-8, lpattern(dash) lcolor(cranberry) c(l) m(i) lwidth(medthick) yline(0, lcolor(black))) ///
	(line fig_b_2 fig_t2 if fig_t<16 & fig_t>-8, msymbol(diamond) c(l) lpattern(solid) lcolor(navy) lwidth(medthick)) ///
	(rcap fig_upper_2 fig_lower_2 fig_t2  if fig_t<16 & fig_t>-8, lpattern(solid) lcolor(navy) c(l) m(i) lwidth(medthick)), /// 
	xlabel(-6 (3) 15) ///
	graphregion(fcolor(white)) ///
	xtitle("Days from Event") ///
	ytitle("CAR (Percent)") ///
	legend(order(1 2 3 4) label(1 "Cumulative Abnormal Return with Leads")  label(2 "90% CI") ///
	label(3 "Cumulative Abnormal Return without Leads") label(4 "90% CI")) 
graph export "$output/pooled_event_20171214_FF.pdf", replace
