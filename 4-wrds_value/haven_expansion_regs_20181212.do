clear
capture log close
set more off
capture close
***************************************************************
//Author: dgg
//Date: 20181212
//Task: regress haven mentions on PR status
***************************************************************
snapshot erase _all

** importing data
use "$additional/Dyreng_Tax_Havens/dyreng_data_merged", clear

** running regressions
xi i.year|PR , noomit
drop _IyeaXPR_1995

est clear

eststo reg1: reghdfe new_haven _IyeaX* PR if year > 1992 ,  a(i.n3 i.year) cl(n3)
eststo reg2: reghdfe new_haven _IyeaX*  if year > 1992 ,  a(i.n3 i.year i.cik) cl(n3)

eststo reg3: reghdfe any_new_haven _IyeaX* PR  if year > 1992 ,  a(i.n3 i.year) cl(n3)
eststo reg4: reghdfe any_new_haven _IyeaX*  if year > 1992 ,  a(i.n3 i.year i.cik) cl(n3)

eststo reg5: reghdfe new_country _IyeaX* PR  if year > 1992 ,  a(i.n3 i.year) cl(n3)
eststo reg6: reghdfe new_country _IyeaX*  if year > 1992,  a(i.n3 i.year i.cik) cl(n3)

eststo reg7: reghdfe any_new_country _IyeaX* PR  if year > 1992 ,  a(i.n3 i.year) cl(n3)
eststo reg8: reghdfe any_new_country _IyeaX*  if year > 1992 ,  a(i.n3 i.year i.cik) cl(n3)

esttab reg? using "$output/Tables/dyreng_data_table_expansion.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons ///
prefoot(" & \multicolumn{2}{c}{New Tax Haven} & \multicolumn{2}{c}{Any New Tax Haven} & \multicolumn{2}{c}{New Countries} & \multicolumn{2}{c}{Any New Countries}  \\ \cmidrule(lr){2-3} \cmidrule(lr){4-5}\cmidrule(lr){6-7}\cmidrule(lr){8-9} ") ///
postfoot("\multicolumn{1}{l}{\textbf{Exposure to Section 936}}") nonum posthead("")

esttab reg?  using "$output/Tables/dyreng_data_table_expansion.tex",  preh("") ///
 b(3) se par mlab(none) coll(none) s() noobs nogap  /// 
coeflabel(_IyeaXPR_1990 "\hspace{1em}X 1990 " /// 
_IyeaXPR_1991 "\hspace{1em}X 1991 " /// 
_IyeaXPR_1992 "\hspace{1em}X 1992 " /// 
_IyeaXPR_1993 "\hspace{1em}X 1993 " /// 
_IyeaXPR_1994 "\hspace{1em}X 1994 " /// 
_IyeaXPR_1996 "\hline \hspace{1em}X 1996 " /// 
_IyeaXPR_1997 "\hspace{1em}X 1997 " /// 
_IyeaXPR_1998 "\hspace{1em}X 1998 " /// 
_IyeaXPR_1999 "\hspace{1em}X 1999 " /// 
_IyeaXPR_2000 "\hspace{1em}X 2000 " /// 
_IyeaXPR_2001 "\hspace{1em}X 2001 " /// 
_IyeaXPR_2002 "\hspace{1em}X 2002 " /// 
_IyeaXPR_2003 "\hspace{1em}X 2003 " /// 
_IyeaXPR_2004 "\hspace{1em}X 2004 " /// 
_IyeaXPR_2005 "\hspace{1em}X 2005 " /// 
_IyeaXPR_2006 "\hspace{1em}X 2006 " /// 
_IyeaXPR_2007 "\hspace{1em}X 2007 " /// 
_IyeaXPR_2008 "\hspace{1em}X 2008 " /// 
_IyeaXPR_2009 "\hspace{1em}X 2009 " /// 
_IyeaXPR_2010 "\hspace{1em}X 2010 " /// 
_IyeaXPR_2011 "\hspace{1em}X 2011 " /// 
_IyeaXPR_2012 "\hspace{1em}X 2012 " )  label /// 
 prefoot("\hline") ///
postfoot( ///
"Year Fixed Effects & Y & Y & Y & Y & Y & Y & Y & Y \\" ///
"Industry Fixed Effects & Y &  & Y &  & Y &  & Y &  \\" ///
"Firm Fixed Effects &  & Y &  & Y &  & Y &  & Y \\" ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex)


est clear

eststo reg1: reghdfe new_haven _IyeaX* PR c.ncountries#c.ncountries ncountries if year > 1992 ,  a(i.n3 i.year) cl(n3)
eststo reg2: reghdfe new_haven _IyeaX* c.ncountries#c.ncountries ncountries  if year > 1992 ,  a(i.n3 i.year i.cik) cl(n3)

eststo reg3: reghdfe any_new_haven _IyeaX* PR c.ncountries#c.ncountries ncountries  if year > 1992 ,  a(i.n3 i.year) cl(n3)
eststo reg4: reghdfe any_new_haven _IyeaX* c.ncountries#c.ncountries ncountries  if year > 1992 ,  a(i.n3 i.year i.cik) cl(n3)

eststo reg5: reghdfe new_country _IyeaX* PR c.ncountries#c.ncountries ncountries  if year > 1992 ,  a(i.n3 i.year) cl(n3)
eststo reg6: reghdfe new_country _IyeaX* c.ncountries#c.ncountries ncountries  if year > 1992,  a(i.n3 i.year i.cik) cl(n3)

eststo reg7: reghdfe any_new_country _IyeaX* PR c.ncountries#c.ncountries ncountries  if year > 1992 ,  a(i.n3 i.year) cl(n3)
eststo reg8: reghdfe any_new_country _IyeaX* c.ncountries#c.ncountries ncountries  if year > 1992 ,  a(i.n3 i.year i.cik) cl(n3)

esttab reg? using "$output/Tables/dyreng_data_table_expansion_v2.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons ///
prefoot(" & \multicolumn{2}{c}{New Tax Haven} & \multicolumn{2}{c}{Any New Tax Haven} & \multicolumn{2}{c}{New Countries} & \multicolumn{2}{c}{Any New Countries}  \\ \cmidrule(lr){2-3} \cmidrule(lr){4-5}\cmidrule(lr){6-7}\cmidrule(lr){8-9} ") ///
postfoot("\multicolumn{1}{l}{\textbf{Section 936}}") nonum posthead("")

esttab reg?  using "$output/Tables/dyreng_data_table_expansion_v2.tex",  preh("") ///
 b(3) se par mlab(none) coll(none) s() noobs nogap  /// 
coeflabel(_IyeaXPR_1990 "\hspace{1em}X 1990 " /// 
_IyeaXPR_1991 "\hspace{1em}X 1991 " /// 
_IyeaXPR_1992 "\hspace{1em}X 1992 " /// 
_IyeaXPR_1993 "\hspace{1em}X 1993 " /// 
_IyeaXPR_1994 "\hspace{1em}X 1994 " /// 
_IyeaXPR_1996 "\hline \hspace{1em}X 1996 " /// 
_IyeaXPR_1997 "\hspace{1em}X 1997 " /// 
_IyeaXPR_1998 "\hspace{1em}X 1998 " /// 
_IyeaXPR_1999 "\hspace{1em}X 1999 " /// 
_IyeaXPR_2000 "\hspace{1em}X 2000 " /// 
_IyeaXPR_2001 "\hspace{1em}X 2001 " /// 
_IyeaXPR_2002 "\hspace{1em}X 2002 " /// 
_IyeaXPR_2003 "\hspace{1em}X 2003 " /// 
_IyeaXPR_2004 "\hspace{1em}X 2004 " /// 
_IyeaXPR_2005 "\hspace{1em}X 2005 " /// 
_IyeaXPR_2006 "\hspace{1em}X 2006 " /// 
_IyeaXPR_2007 "\hspace{1em}X 2007 " /// 
_IyeaXPR_2008 "\hspace{1em}X 2008 " /// 
_IyeaXPR_2009 "\hspace{1em}X 2009 " /// 
_IyeaXPR_2010 "\hspace{1em}X 2010 " /// 
_IyeaXPR_2011 "\hspace{1em}X 2011 " /// 
_IyeaXPR_2012 "\hspace{1em}X 2012 " )  label /// 
 prefoot("\hline") ///
postfoot( ///
"Year FE & Y & Y & Y & Y & Y & Y & Y & Y \\" ///
"Size Controls & Y & Y & Y & Y & Y & Y & Y & Y \\" ///
"Industry FE & Y &  & Y &  & Y &  & Y &  \\" ///
"Firm FE &  & Y &  & Y &  & Y &  & Y \\" ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex) 


est clear

eststo reg1: reghdfe new_haven _IyeaX* PR c.ncountries#c.ncountries c.ncountries#i.year if year > 1992 ,  a(i.n3 i.year) cl(n3)
eststo reg2: reghdfe new_haven _IyeaX* c.ncountries#c.ncountries c.ncountries#i.year  if year > 1992 ,  a(i.n3 i.year i.cik) cl(n3)

eststo reg3: reghdfe any_new_haven _IyeaX* PR c.ncountries#c.ncountries c.ncountries#i.year  if year > 1992 ,  a(i.n3 i.year) cl(n3)
eststo reg4: reghdfe any_new_haven _IyeaX* c.ncountries#c.ncountries c.ncountries#i.year  if year > 1992 ,  a(i.n3 i.year i.cik) cl(n3)

eststo reg5: reghdfe new_country _IyeaX* PR c.ncountries#c.ncountries c.ncountries#i.year  if year > 1992 ,  a(i.n3 i.year) cl(n3)
eststo reg6: reghdfe new_country _IyeaX* c.ncountries#c.ncountries c.ncountries#i.year  if year > 1992,  a(i.n3 i.year i.cik) cl(n3)

eststo reg7: reghdfe any_new_country _IyeaX* PR c.ncountries#c.ncountries c.ncountries#i.year  if year > 1992 ,  a(i.n3 i.year) cl(n3)
eststo reg8: reghdfe any_new_country _IyeaX* c.ncountries#c.ncountries c.ncountries#i.year  if year > 1992 ,  a(i.n3 i.year i.cik) cl(n3)

esttab reg? using "$output/Tables/dyreng_data_table_expansion_v3.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons ///
prefoot(" & \multicolumn{2}{c}{New Tax Haven} & \multicolumn{2}{c}{Any New Tax Haven} & \multicolumn{2}{c}{New Countries} & \multicolumn{2}{c}{Any New Countries}  \\ \cmidrule(lr){2-3} \cmidrule(lr){4-5}\cmidrule(lr){6-7}\cmidrule(lr){8-9} ") ///
postfoot("\multicolumn{1}{l}{\textbf{Section 936}}") nonum posthead("")

esttab reg?  using "$output/Tables/dyreng_data_table_expansion_v3.tex",  preh("") ///
 b(3) se par mlab(none) coll(none) s() noobs nogap  /// 
coeflabel(_IyeaXPR_1990 "\hspace{1em}X 1990 " /// 
_IyeaXPR_1991 "\hspace{1em}X 1991 " /// 
_IyeaXPR_1992 "\hspace{1em}X 1992 " /// 
_IyeaXPR_1993 "\hspace{1em}X 1993 " /// 
_IyeaXPR_1994 "\hspace{1em}X 1994 " /// 
_IyeaXPR_1996 "\hline \hspace{1em}X 1996 " /// 
_IyeaXPR_1997 "\hspace{1em}X 1997 " /// 
_IyeaXPR_1998 "\hspace{1em}X 1998 " /// 
_IyeaXPR_1999 "\hspace{1em}X 1999 " /// 
_IyeaXPR_2000 "\hspace{1em}X 2000 " /// 
_IyeaXPR_2001 "\hspace{1em}X 2001 " /// 
_IyeaXPR_2002 "\hspace{1em}X 2002 " /// 
_IyeaXPR_2003 "\hspace{1em}X 2003 " /// 
_IyeaXPR_2004 "\hspace{1em}X 2004 " /// 
_IyeaXPR_2005 "\hspace{1em}X 2005 " /// 
_IyeaXPR_2006 "\hspace{1em}X 2006 " /// 
_IyeaXPR_2007 "\hspace{1em}X 2007 " /// 
_IyeaXPR_2008 "\hspace{1em}X 2008 " /// 
_IyeaXPR_2009 "\hspace{1em}X 2009 " /// 
_IyeaXPR_2010 "\hspace{1em}X 2010 " /// 
_IyeaXPR_2011 "\hspace{1em}X 2011 " /// 
_IyeaXPR_2012 "\hspace{1em}X 2012 " )  label /// 
 prefoot("\hline") ///
postfoot( ///
"Year FE & Y & Y & Y & Y & Y & Y & Y & Y \\" ///
"Size Controls & Y & Y & Y & Y & Y & Y & Y & Y \\" ///
"Industry FE & Y &  & Y &  & Y &  & Y &  \\" ///
"Firm FE &  & Y &  & Y &  & Y &  & Y \\" ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex) 
