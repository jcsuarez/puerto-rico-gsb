clear all
set more off
snapshot erase _all 

global dropbox "/Users/kevinroberts/Dropbox (Personal)/Puerto Rico"


global dropbox "/Users/jisangyu/JC's Data Emporium Dropbox/Eddie Yu/Puerto Rico"
* Set STATA to search ado files in dropbox, for maptile *
sysdir set PERSONAL "$dropbox/Programs/ado"

** Install Packages 
// ssc install listtex, replace all 
// ssc install binscatter, replace all
// ssc install estout, replace all 
// ssc install cdfplot, replace all
// ssc install winsor, replace all
// ssc install reghdfe, replace all
// ssc install tuples, replace all
// ssc install coefplot, replace all 
// ssc install spmap, replace all 
// ssc install maptile, replace all 
// ssc install ivreg2, replace all 
// ssc install ftools, replace all
// ssc install ivreghdfe, replace all 
// ssc install ivreg2hdfe, replace all
// ssc install reg2hdfe, replace all 
// ssc install moremata, replace all
// ssc install tmpdir, replace all
// ssc install ranktest, replace all
// ssc install dataout, replace
// net from https://www.sealedenvelope.com/ // click xfill
// maptile_install using "http://files.michaelstepner.com/geo_county1990.zip"
// maptile_install using "http://files.michaelstepner.com/geo_state.zip"

** Global Dropbox Paths (raw to clean data)
global output 		"$dropbox/Programs/output"
global data 		"$dropbox/Data"
global analysis 	"$dropbox/Programs"
global linkdata		"$dropbox/Programs/output/base_1995"
global xwalk		"$dropbox/Data/Cross Walk"
global qcewdata		"$dropbox/Data/QCEW"
global bea			"$dropbox/Data/BEA"
global consp		"$dropbox/Data/conspuma"
global additional 	"$dropbox/Data/data_additional"
global output_NETS 	"$dropbox/Programs/output/NETS_20171128"
global output_NETS2 "$dropbox/Programs/output/NETS_20171129_v2"
global output_NETS_control "$dropbox/Programs/output/NETS_control"
global WRDS			"$dropbox/Data/WRDS"
global ASM			"$dropbox/Data/ASM"
global SOI			"$dropbox/Data/SOI Data"
global irs 			"$dropbox/Data/IRS"


** Global Dropbox Paths (raw to clean data)
global pr_wgt 		""     											// Option: "[aw = wgt]"
global pra_control 	"" 										// Option: "i.year|pr_link_all" 
global pra_control2 ""												// Option: "pr_link_all"      
global pra_control_rob "" 											// Option: "cen_pr_link_a" 
global pra_control_inc ""											// Option: "_IyeaXpr_b*"


*** Start
cd "$analysis"
clear
set more off 
	
*initializing ado files to format additional stats
do "$analysis/jc_add_stats.ado"
do "$analysis/jc_stat_out.ado"

// ***************************************************
// * Original Build (run before replication)
// ***************************************************
********** NETS Pr link build: Run on External Drive 
do "$analysis/0-build/nets_pr_extract.do"						// Extract firms with an establishment in PR, runs on external harddrive
do "$analysis/0-build/nets_pr_extract_panel_v2.do"				// Extract firms with an establishment in PR in panel format, runs on external harddrive
do "$analysis/0-build/nets_pr_extract_naic3.do"					// Extract firms with an establishment in PR, runs on external harddrive and includes all NAICS3 codes (and md, fd, and emp)
do "$analysis/0-build/nets_pr_extract_naic3_1993.do"			// Same as above, but additionally produces "pr_link_est_county_n3_93.dta", "pr_link_est_countyXindustry_n3_93.dta"
do "$analysis/0-build/nets_pr_extract_naic3_93_95firms.do"		// Same as above, but additionally produces "pr_link_est_county_n3_93_95f.dta", "pr_link_est_countyXindustry_n3_93_95f.dta"
do "$analysis/0-build/nets_pr_extract_emp.do"					// Extract firms with an establishment in PR, also saves employment

********** Build Data for NETS Controls: Run on External Drive 
do "$analysis/0-build/Nets_control_panel/get_pr_exposed_characteristics_region2.do"	// selects the 682 firms we care about and then from those selects
											// the actual state and insustries we care about (census region and 3 digit NAICS)
																						
do "$analysis/0-build/Nets_control_panel/get_HQs_controls_region2.do" 		// This code goes over all the estabs in the NETS and selects: 
										//	1- Those that are headquarters only (hqduns == duns definition)
										//	2- Those that are around before 1995 (in 1995 and 1990)
										//	3- Drops Industries and firms that are suspect (gov related duns and NAICS)
										//	4- Keeps Those in 3d NAICS-census region combos that match PR exposed firms 
										//	5- Saves the HQDuns for these firms 
																						
do "$analysis/0-build/Nets_control_panel/get_HQs_size_region2.do"		// This code goes over all the estabs in the NETS and selects: 
										// 1- All estabs linked to control firms (with HQs in list) 
										// 2- Gathers just employment and establishments in 1995
										// 3- collapses at hq level 
										// 4- merges all files and collapses again 
										// 5- keeps only those firms in the relevant size categories (emp and est quitiles)
										// 6- This reduces the number of potential HQ firms. 

do "$analysis/0-build/Nets_control_panel/get_control_panel_region2.do" 		// This code goes over all the estabs in the NETS and selects: 
										//	1- All estabs linked to control firms (with HQs in list by industry and size) 
										//	2- Gathers employment and establishment counts by year 
										//	3- Collapses at the hq level 
										//	4- merges all files and collapses again 
********** NETS FAKE Pr link build: Run on External Drive -- this seems irrelevant with any files
// do "$analysis/0-build/placebo_nets_shock.do" 					// Generates placebo shock of similar firms that are not exposed to S 936 


********** Build Data for Analysis
do "$analysis/0-build/percent_pharma.do" 	 					// creates county level pharma exposure variables (124 positive values)  
do "$analysis/0-build/county_level_data_all.do"   				// Import additional county-level data from various sources, saves "county_level_data_all.dta"
do "$analysis/0-build/county_manufacturing_emp_annual.do"		// Impots annual QCEW data, saves "county_manu_emp_ests.dta"
do "$analysis/0-build/compustat_merge_manual.do" 				// Merge existing NETS data with COMPUSTAT data
do "$analysis/0-build/compustat_ETR_calc.do" 					// Include ETR variables in compustat data
do "$analysis/0-build/compustat_ETR_calc_segments.do" 			// Merge compustat segments data with ETR calculation variables
do "$analysis/0-build/compustat_ETR_CAR_estimate.do" 			// Computes Cumulative Abnormal Returns (CARs) estimates from ETR variables
do "$analysis/0-build/qcew_extract.do"							// Extract QCEW files, produces "extract_qcew_1990_2012.dta"
do "$analysis/0-build/qcew_extract_naic3.do"					// same as above, but extract_naic3 keeps everything at 3 digit NAIC level and produces "extract_qcew_1990_2012_naic3.dta"
do "$analysis/0-build/clean_ASM.do" 							// Imports and cleans ASM data
do "$analysis/0-build/nafta_merge.do" 	 						// saves import_hakobyanlaren_nafta.dta with nafta exposure 
do "$analysis/0-build/CBP_firm_size_extract.do" 	 			// extracts firm size distribution 
do "$analysis/0-build/pr_link_est_county_addvars_d.do"			// Merge NETS and county-level data using the version that carries employment link measures as well, old version -_d
do "$analysis/0-build/county_pop_ests_annual.do" 				// Assembles annual estimates for working age population at the pid level, produces a single dataset with 3 vars: pid, year, and working age pop 15-64 "$additional/county population estimates/county_pop_ests.dta"
do "$analysis/0-build/welfare_import.do" 						// welfare transfers
do "$analysis/0-build/tradeable_naics.do" 						// cleans and import raw data from Mian and Sufi

********** additional build files with some graphs and scalars										
do "$analysis/0-build/SOI_import.do" 								// Import some basic IRS SOI stats about US possessions tax credit
do "$analysis/0-build/pr_link_compustat_sample.do" 					// PR link made with only compustat sample and some relevant scalars
do "$analysis/1-descriptive/Industy_employment_2018_10_03.do"		// uses "extract_qcew_1990_2012_naic3.dta" and "$output_NETS/pr_extract_est.dta" to produce "$output_NETS/industry_cat_emp_sums_v2"

**********************************************
* building replication files (separate data file per figure or table)
**********************************************
do "$analysis/0b-build_replications/figure1_clean.do"				// cleans the SOI data reporting totals, percents, and counterfactual ETRs
* figure 2 is model and does not need data cleaning
do "$analysis/0b-build_replications/figure3_clean.do"				// the maps
do "$analysis/0b-build_replications/figure4_clean.do"				// the balance
do "$analysis/0b-build_replications/figure5_clean.do"				// All firm-level compustat graphs including:
																	// figures 5, 6, A4, A5, A6, A7
do "$analysis/0b-build_replications/figure7_clean.do"				// ASM regression
do "$analysis/0b-build_replications/figure8_clean.do"				// NETS employment regressions

do "$analysis/0b-build_replications/figure9_clean.do"				// non-parametric county empgrowth comparison
do "$analysis/0b-build_replications/figure10_clean.do"				// parametric county empgrowth comparison
*figure 11, the monster with 12 specifications from different places
do "$analysis/0b-build_replications/figure11_clean_baseline.do"		// also covers state trends and FE			
do "$analysis/0b-build_replications/figure11_clean_conspuma.do"			
do "$analysis/0b-build_replications/figure11_clean_czone.do"			
do "$analysis/0b-build_replications/figure11_clean_PC.do"			
do "$analysis/0b-build_replications/figure11_clean_empinst.do"			
do "$analysis/0b-build_replications/figure11_clean_MD.do"			
do "$analysis/0b-build_replications/figure11_clean_FD.do"			
do "$analysis/0b-build_replications/figure11_clean_compu.do"
do "$analysis/0b-build_replications/figure11_clean_syFE.do"			
do "$analysis/0b-build_replications/figure11_clean_size.do"			
do "$analysis/0b-build_replications/figure11_clean_93f.do"			
do "$analysis/0b-build_replications/figure11_clean_9395f.do"

		
do "$analysis/0b-build_replications/figure12_clean.do"				// Placebo tests
do "$analysis/0b-build_replications/figure13_clean.do"				// robustness checks
do "$analysis/0b-build_replications/figure14_clean.do"				// heterogeneity, includes Figure A16 and Table A26
do "$analysis/0b-build_replications/figure15_clean.do"				// census long differences

** TABLES
do "$analysis/0b-build_replications/table1_clean.do"				// Large NETS firms
do "$analysis/0b-build_replications/table2_clean.do"				// investment DiDs with compustat
																	// covers many other tables including: 4, A5, A6, A7 since they are all one variable off 
do "$analysis/0b-build_replications/table3_clean.do"				// tax DiDs with compustat
																	// covers tables including 4, A5, A6, A7 since they are all one variable off 
do "$analysis/0b-build_replications/table5_clean.do"				// ASM DiDs, covers tables A8, A9
* table 6 cleaned with figure 11
do "$analysis/0b-build_replications/table7_clean_ADH.do"			// Robustness, other columns from figure 11
do "$analysis/0b-build_replications/table8_clean_IRSBEAcounty.do"	// long difference IRS and BEA
do "$analysis/0b-build_replications/table8_clean_QCEWcounty.do"		// long difference QCEW county
do "$analysis/0b-build_replications/table8_clean_QCEWconspuma.do"	// long difference QCEW conspuma
do "$analysis/0b-build_replications/table8_clean_QCEWczone.do"		// long difference QCEW czone
*table 9 uses the same data as figure 15
do "$analysis/0b-build_replications/table10_clean.do"				// transfers


*********** appendix
do "$analysis/0b-build_replications/figureA1_clean.do"				// PR employment
do "$analysis/0b-build_replications/figureA2_clean.do"				// the maps appendix version with employment
do "$analysis/0b-build_replications/figureA3_clean.do"				// the maps appendix version with different geographies
* figure A4-A7 are included in figure5
* figure A8 created later in codes for producing final figures
* figure A9 doesn't use any data
do "$analysis/0b-build_replications/figureA10_clean.do"				// NETS employment beautiful mess
do "$analysis/0b-build_replications/figureA11_clean.do"				// NETS establishment ES
do "$analysis/0b-build_replications/figureA12_clean.do"				// Capital employment binscatter
*figure A13-A16 use data from figure 11
*figure A17 uses data from figure 15
do "$analysis/0b-build_replications/figureA18_clean.do"				// stock event study; covers A18, A19, A20 Panel A, B, C


** TABLES
do "$analysis/0b-build_replications/tableA1_clean.do"				// SOI tax change regression
do "$analysis/0b-build_replications/tableA2_clean.do"				// compustat firm descriptives
do "$analysis/0b-build_replications/tableA3_clean.do"				// list of top and bottom counties
do "$analysis/0b-build_replications/tableA4_clean_balancetable.do"	// bunch of balance
//A5-A7 done by tableA2_clean.do, A8-A9 by tableA3_clean.do
do "$analysis/0b-build_replications/tableA10_clean.do"				// NETS employment robustness
do "$analysis/0b-build_replications/tableA11_clean.do"				// NETS employment heterogeneity
*tables A12-A18 are already cleaned for figure 11
do "$analysis/0b-build_replications/tableA19_clean.do"				// ADH baseline
do "$analysis/0b-build_replications/tableA20_clean.do"				// ADH levels
*tables A21 cleaned for figure 11, A22-A23 are already cleaned for figure 12
do "$analysis/0b-build_replications/tableA24_clean.do"				// long robustness table, includes A25
*table A25 cleaned with A24
* table A26 cleaned with figure 14 (same analysis in different form)
* table A27 cleaned with figure 15
* table A28 cleaned with table 10
do "$analysis/0b-build_replications/tableA29_clean.do"				// propensity of being in PR
do "$analysis/0b-build_replications/tableA30_clean.do"				
do "$analysis/0b-build_replications/tableA31_clean.do"				// also covers table A32
* table A32 cleaned with table A31
do "$analysis/0b-build_replications/tableA33_clean.do"				// also covers table A34
* table A34 cleaned with table A33
do "$analysis/0b-build_replications/tableA35_clean.do"


**********************************************
* Starting at the begining of the paper figures before tables
**********************************************
do "$analysis/7-Analysis Replication/figure1.do"					// basic moments from IRS SOI
do "$analysis/7-Analysis Replication/figure2.do"					// figure 2 models
do "$analysis/7-Analysis Replication/figure3.do"					// the main maps
do "$analysis/7-Analysis Replication/figure4.do"					// the main balance figure
do "$analysis/7-Analysis Replication/figure5.do"					// investment response
do "$analysis/7-Analysis Replication/figure6.do"					// foreign share
do "$analysis/7-Analysis Replication/figure7.do"					// ASM
do "$analysis/7-Analysis Replication/figure8.do"					// Firm-Level Employment from NETS

do "$analysis/7-Analysis Replication/figure9.do"					// non-parametric county empgrowth comparison
do "$analysis/7-Analysis Replication/figure10.do"					// baseline counties
do "$analysis/7-Analysis Replication/figure11.do"					// all different versions
do "$analysis/7-Analysis Replication/figure12.do"					// placebos
do "$analysis/7-Analysis Replication/figure13.do"					// robustness checks
do "$analysis/7-Analysis Replication/figure14.do"					// heterogeneity
do "$analysis/7-Analysis Replication/figure15.do"					// Census conspuma long differences

** TABLES
do "$analysis/7-Analysis Replication/table1.do"						// NOTE: TABLE NEEDS MINOR FORMATTING TO GO INTO PAPER BECAUSE OF DATAOUT (strip first and end lines)
do "$analysis/7-Analysis Replication/table2.do"						// investment
do "$analysis/7-Analysis Replication/table3.do"						// ftax
do "$analysis/7-Analysis Replication/table4.do"						// foreign share
do "$analysis/7-Analysis Replication/table5.do"						// ASM capex

do "$analysis/7-Analysis Replication/table6.do"						// main LLM table
do "$analysis/7-Analysis Replication/table7.do"						// robustness LLM table
do "$analysis/7-Analysis Replication/table8.do"						// long difference table
do "$analysis/7-Analysis Replication/table9.do"						// long difference wages table
do "$analysis/7-Analysis Replication/table10.do"					// transfers


**********************************************
* Appendix
**********************************************
do "$analysis/7-Analysis Replication/figureA1.do"				// PR employment
do "$analysis/7-Analysis Replication/figureA2.do"				// the maps appendix version with employment
do "$analysis/7-Analysis Replication/figureA3.do"				// the maps appendix version with different geographies
do "$analysis/7-Analysis Replication/figureA4.do"				// investment response levels
do "$analysis/7-Analysis Replication/figureA5.do"				// percent change in investment
do "$analysis/7-Analysis Replication/figureA6.do"				// 1% winsorizing
do "$analysis/7-Analysis Replication/figureA7.do"				// investment regressions with all controls
do "$analysis/0b-build_replications/investment_ETR_joint_2019_07_01_ftax_test.do" 		// Produces Figure A8; ftax smoothed regression

do "$analysis/7-Analysis Replication/figureA9.do"				// depreciation simulations, no data

do "$analysis/7-Analysis Replication/figureA10.do"				// Employment NETS robustness
do "$analysis/7-Analysis Replication/figureA11.do"				// establishments NETS
do "$analysis/7-Analysis Replication/figureA12.do"				// Capital employment binscatter
do "$analysis/7-Analysis Replication/figureA13.do"				// ADH
do "$analysis/7-Analysis Replication/figureA14.do"				// State Time Trends
do "$analysis/7-Analysis Replication/figureA15.do"				// large firms
*figure A16 created with figure 14 since they are identical with and without error bars
do "$analysis/7-Analysis Replication/figureA17.do"				// conspuma long diff adjusted (uses figure 15 data)
do "$analysis/7-Analysis Replication/figureA18_A19.do"			// event studies
do "$analysis/7-Analysis Replication/figureA20_abc.do"			// Panel A, B, C. (D is created by tableA35.do)

** TABLES
do "$analysis/7-Analysis Replication/tableA1.do"				// SOI regression
do "$analysis/7-Analysis Replication/tableA2.do"				// compustat balance NOTE: TABLE NEEDS MINOR FORMATTING BY HAND TO GO INTO PAPER
do "$analysis/7-Analysis Replication/tableA3.do"				// top and bottom counties
do "$analysis/7-Analysis Replication/tableA4_balancetable.do"					// bunch of county characteristic balance (tableA4.do does not exactly produce the same estimates; off by 0.00Xs)

do "$analysis/7-Analysis Replication/tableA5.do"				// investment percent change
do "$analysis/7-Analysis Replication/tableA6.do"				// investment 1% winsorizing
do "$analysis/7-Analysis Replication/tableA7.do"				// investment robustness
do "$analysis/7-Analysis Replication/tableA8.do"				// federal taxes paid
do "$analysis/7-Analysis Replication/tableA9.do"				// totaltax

do "$analysis/7-Analysis Replication/tableA10.do"				// NETS robustness
do "$analysis/7-Analysis Replication/tableA11.do"				// NETS heterogeneity

do "$analysis/7-Analysis Replication/tableA12.do"				// employment instrument
do "$analysis/7-Analysis Replication/tableA13.do"				// MD
do "$analysis/7-Analysis Replication/tableA14.do"				// FD
do "$analysis/7-Analysis Replication/tableA15.do"				// compustat only
do "$analysis/7-Analysis Replication/tableA16.do"				// size 
do "$analysis/7-Analysis Replication/tableA17.do"				// 93F
do "$analysis/7-Analysis Replication/tableA18.do"				// 9395F

do "$analysis/7-Analysis Replication/tableA19.do"				// ADH %
do "$analysis/7-Analysis Replication/tableA20.do"				// ADH PP

do "$analysis/0b-build_replications/es_county_SY_FE_20190617.do"	// Creates Table A21; state trends and FE
do "$analysis/7-Analysis Replication/tableA22.do"				// placebo
do "$analysis/7-Analysis Replication/tableA23.do"				// placebo pharma
do "$analysis/7-Analysis Replication/tableA24_A25.do"			// robustness table (split into 2)
do "$analysis/7-Analysis Replication/tableA26.do"				// placebo pharma
* table A26 is created with figure 14, but there is a separate file if we want, takes some time to run
do "$analysis/7-Analysis Replication/tableA27.do"				// long diff table without adjusting
do "$analysis/7-Analysis Replication/tableA28.do"				// transfers preperiod (like table 10)

do "$analysis/7-Analysis Replication/tableA29.do"				// propensity of being in PR
do "$analysis/7-Analysis Replication/tableA30.do"				// stock event study panels (needs to run)
do "$analysis/7-Analysis Replication/tableA31.do"				// stock event study robustness CAPM
do "$analysis/7-Analysis Replication/tableA32.do"				// stock event study robustness FF3 + MOM

do "$analysis/7-Analysis Replication/tableA33_A34.do"			// interactions tables

do "$analysis/7-Analysis Replication/tableA35.do"				// quantile regression (DOESN'T CURRENTLY CONVERGE), also creates figure A20 Panel D
