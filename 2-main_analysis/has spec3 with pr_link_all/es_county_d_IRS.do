* 06/2016
* Merge NTES Link Data and QCEW Outcome Data 
* Runs Event-Study Analyses 

clear all
set more off
 
// First Define Command for ES 
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(dash))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Estimate") label(2 "`level'% CI")) ///
	ytitle("`yti'") ///
	note("`note'") ylab("`ylab'")
graph export "`outname'.pdf", replace	

end 


// A - Merge Data for Analysis 

// A.0. Get link data 
use "$output_NETS/pr_link_est_countyXindustry_d.dta", clear 
gen pr_emp = pr_link*total if industry_cd < 13
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)

gen pr_l = tot_pr/tot_emp 
keep pr_l fips
rename  fips pid
rename pr_l pr_link_ind
duplicates drop 
tempfile pr 
save `pr' 

use "$output_NETS/pr_link_est_county_d.dta", clear 
rename  fips pid
tempfile pr2 
save `pr2' 

// A.1. Get BEA data 
use "$irs/irs_income.dta", clear
keep RETURNS GROSSINCOME fips* year 
rename RETURNS emp 
rename GROSSINCOME inc 
keep if year >= 1990
keep if year < 2013

order fips_state fips_county year 
duplicates drop 

// A.3. Fill in gaps
gen pid = 1000*fips_state+fips_county

tsset pid year
tsfill, full

// A.4.  Growth rates. Base year: 1995

foreach var of varlist inc emp { 
	gen temp = `var' if year == 1995
	bysort pid: egen base_`var' = max(temp)
	drop temp

	gen `var'_growth = (`var' - base_`var')/base_`var'
	replace `var'_growth = 0 if `var'_growth == . 
}
	
// A.6. Merge in pr link 
merge m:1 pid using `pr'
keep if _merg == 3
drop _merge 

merge m:1 pid using `pr2'
drop if year == . 
drop if pr_link == . 
drop _merge 
rename pr_link pr_link_all


** For now, keep only balanced industries . Matt: can you fix it so we don't have to do this? 

sum pr_link_i, d 
replace pr_link_i = pr_link_i/(r(p75)-r(p25))

sum pr_link_al, d 
replace pr_link_al = pr_link_al/(r(p75)-r(p25))



*gen ln_pr = ln(1+pr_link_a) 
*egen std_ln = std(ln_pr)
*winsor std_ln , g(w_std_ln) p(.01) 
/*
// B. Graphs with All PR Link 
cd "$output" 
set more off 
xi i.year|pr_link_i, noomit
drop _IyeaXp*1995

drop if base_emp <=2000 

winsor base_emp , g(w_emp_base) p(.025)
replace base_emp = w_emp_base
sum base_emp, d 
drop if base_emp <= r(p1) 

est clear 
set more off 
eststo emp_1:  reghdfe emp_growth _IyeaX* [aw=base_emp]  , a( i.year ) cl(fips_state )
	estadd local hasiy "Yes"
    estadd local hascty ""
eststo emp_2:  reghdfe emp_growth _IyeaX* [aw=base_emp]  , a(i.year ) cl(fips_state  )
	estadd local hasiy "Yes"
	estadd local hascty "Yes"

ES_graph , level(95) yti("Effect S936 Exposure (IQR)") ///
note("Notes: (Emp{sub:ict}-Emp{sub:ic1995})/Emp{sub:ic1995}={&alpha}{sub:c}+{&gamma}{sub:it}+{&beta}{sub:t}PR Link{sub:c}+{&epsilon}{sub:ict}. SE's clustered by State and Industry.") ///
outname("$output/Graphs/ES_BEA") ylab("-.12(.02).04")

xi i.year|pr_link_a, noomit
drop _IyeaXp*1995	
eststo emp_3:  reghdfe emp_growth _IyeaX* [fw=base_emp]  , a( i.pid i.year#i.industr) cl(fips_state indust )
	estadd local hasiy "Yes"
	estadd local hascty "Yes"	
	estadd local hasall "Yes"	

xi i.year|pr_link_i, noomit
drop _IyeaXp*1995

eststo emp_4:  reghdfe emp_growth _IyeaX* [fw=base_emp]  , a( i.pid2 i.year#i.indust ) cl(fips_state indust )
	estadd local hasiy "Yes"
	estadd local hasicty "Yes"

eststo emp_5:  reghdfe emp_growth _IyeaX* [fw=w_emp_base]  , a( i.pid i.year#i.indust ) cl(fips_state indust )
	estadd local hasiy "Yes"
	estadd local hascty "Yes"
	estadd local hasww "Yes"	

eststo emp_6:  reghdfe emp_growth _IyeaX* if base_emp > 1000 [fw=base_emp]  , a( i.pid i.year#i.indust ) cl(fips_state indust )
	estadd local hasiy "Yes"
	estadd local hascty "Yes"
	estadd local hasds "Yes"
 
	
esttab emp* using "$output/Tables/table_ES_QCEW.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons ///
postfoot("\multicolumn{6}{l}{\textbf{Exposure to Section 936}}\\")


esttab emp*   using "$output/Tables/table_ES_QCEW.tex",  preh("") ///
 b(3) par mlab(none) coll(none) s() noobs  /// 
coeflabel(_IyeaXpr__1990 "\hspace{1em}X 1990 " /// 
_IyeaXpr__1991 "\hspace{1em}X 1991 " /// 
_IyeaXpr__1992 "\hspace{1em}X 1992 " /// 
_IyeaXpr__1993 "\hspace{1em}X 1993 " /// 
_IyeaXpr__1994 "\hspace{1em}X 1994 " /// 
_IyeaXpr__1996 "\hspace{1em}X 1996 " /// 
_IyeaXpr__1997 "\hspace{1em}X 1997 " /// 
_IyeaXpr__1998 "\hspace{1em}X 1998 " /// 
_IyeaXpr__1999 "\hspace{1em}X 1999 " /// 
_IyeaXpr__2000 "\hspace{1em}X 2000 " /// 
_IyeaXpr__2001 "\hspace{1em}X 2001 " /// 
_IyeaXpr__2002 "\hspace{1em}X 2002 " /// 
_IyeaXpr__2003 "\hspace{1em}X 2003 " /// 
_IyeaXpr__2004 "\hspace{1em}X 2004 " /// 
_IyeaXpr__2005 "\hspace{1em}X 2005 " /// 
_IyeaXpr__2006 "\hspace{1em}X 2006 " /// 
_IyeaXpr__2007 "\hspace{1em}X 2007 " /// 
_IyeaXpr__2008 "\hspace{1em}X 2008 " /// 
_IyeaXpr__2009 "\hspace{1em}X 2009 " /// 
_IyeaXpr__2010 "\hspace{1em}X 2010 " /// 
_IyeaXpr__2011 "\hspace{1em}X 2011 " /// 
_IyeaXpr__2012 "\hspace{1em}X 2012 " )   scalars(  "hasiy Industry-by-Year Fixed Effects" "hascty County Fixed Effects" ///
"hasall Exposure from All Industries"  "hasicty Industry-by-County Fixed Effects" ///
"hasww Winsorized Weights" "hasds Drops Small County-Industries (<1000)" ) label /// 
 prefoot("\hline") ///
postfoot( ///
"  \\ \hline\hline" ///
"\end{tabular} }" ) append sty(tex)

*/
/*
eststo emp_3:  reghdfe estab_growth _IyeaX* [fw=base_emp]  , a( i.year i.pid) cl(fips_state indust )

ES_graph , level(95) yti("Effect of IQR of All PR Link") ///
note("Notes: (Emp{sub:ict}-Emp{sub:ic1995})/Emp{sub:ic1995}={&alpha}{sub:ic}+{&gamma}{sub:t}+{&beta}{sub:t}PR Link{sub:c}+{&epsilon}{sub:ict}. SE's clustered by State.") ///
outname("temp") ylab("-.12(.02).04")

eststo emp_3:  reghdfe inc_growth _IyeaX* [fw=base_emp]  , a( i.pid i.year#i.industr) cl(fips_state indust )

ES_graph , level(95) yti("Effect of IQR of All PR Link") ///
note("Notes: (Emp{sub:ict}-Emp{sub:ic1995})/Emp{sub:ic1995}={&alpha}{sub:ic}+{&gamma}{sub:t}+{&beta}{sub:t}PR Link{sub:c}+{&epsilon}{sub:ict}. SE's clustered by State.") ///
outname("temp") ylab("-.12(.02).04")
*/

drop if base_emp < 10000
**** Main Post/Pre table 
est clear 
gen temp = pr_link_i
replace pr_link_i = pr_link_a

eststo emp_1:   reghdfe emp_growth  c.pr_link_i    [fw=base_emp] if inrange(year,2004,2008) , a( i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	
eststo premp_1:   reghdfe emp_growth  c.pr_link_i    [fw=base_emp] if inrange(year,1990,1995) , a( i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hasind "Yes"

eststo inc_1:  reghdfe inc_growth c.pr_link_i  [fw=base_emp] if inrange(year,2004,2008) , a(  i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hasind "Yes"

eststo princ_1:	reghdfe inc_growth  c.pr_link_i    [fw=base_emp] if inrange(year,1990,1995) , a(  i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	
replace pr_link_i = temp 
	
eststo emp_2:   reghdfe emp_growth  c.pr_link_i    [fw=base_emp] if inrange(year,2004,2008) , a(i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"	
	
eststo premp_2:   reghdfe emp_growth  c.pr_link_i    [fw=base_emp] if inrange(year,1990,1995) , a(i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"	
	
eststo inc_2:  reghdfe inc_growth c.pr_link_i  [fw=base_emp] if inrange(year,2004,2008) , a(i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"	
	
eststo princ_2:	reghdfe inc_growth  c.pr_link_i    [fw=base_emp] if inrange(year,1990,1995) , a(i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"		


esttab emp* inc* using "$output/Tables/table_pre_post_IRS.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons ///
postfoot(" & \multicolumn{2}{l}{Employment Growth} &\multicolumn{2}{l}{Income Growth} \\")

esttab emp*   inc*  using "$output/Tables/table_pre_post_IRS.tex", keep(pr_link_ind )  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s() noobs /// 
coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 " pr_link_all "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{6}{l}{\textbf{Years: 2004-2008}}\\") postfoot("\hline") append sty(tex)

esttab premp*  princ*  using "$output/Tables/table_pre_post_IRS.tex", keep(pr_link_ind)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s()  /// 
coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{6}{l}{\textbf{Years: 1990-1995}}\\") noobs scalars(  "hasy Year Fixed Effects" "hasind Industry Fixed Effects" ///
"hassel Exposure from S936 Industries") label /// 
 prefoot("\hline") ///
postfoot( ///
"  \\ \hline\hline" ///
"\end{tabular} }" ) append sty(tex)

