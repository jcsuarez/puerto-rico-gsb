* 
* Runs Event-Study Analysis robustness table  
* Author: dan and juan carlos
* original date: 06/2016
*
* date of update: 20180129

// 20180129 Changes:(1) added ES_graph_data and exports output data to file: "$qcewdata/ES_graph.dta"
// 					 NOTE: this means es_county_d needs to be run first.
//					(2) took out PR Link wording from graphs

// 20180219 changes: (1) changed all files to naic3 versions and took out industry classifications

// This version uses base_emp/emp aweights instead of fweights
// NOTE: (1) Updated the IQR normalization to use the weights as well otherwise interpretation is off

clear all
set more off
snapshot erase _all



// First Define Command for ES data 
capture program drop ES_graph_data
program define ES_graph_data
syntax,  level(real) yti(string) tshifter(real)


qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 


forval i = 1/23 { 
	mat time = (time,`i'-6+1995 + `tshifter')
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci_l es_ci_h time 
svmat es_b 
rename es_b `yti'_es_b
svmat time
rename time `yti'_time
svmat es_ci_l
rename es_ci_l `yti'_es_ci_l
svmat es_ci_h 
rename es_ci_h `yti'_es_ci_h
}

end  
 
// Second Define Command for ES 
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(dash))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Estimate") label(2 "`level'% CI")) ///
	ytitle("`yti'") ///
	note("`note'") ylab("`ylab'")
graph export "`outname'.pdf", replace	

end 


// A - Merge Data for Analysis 
// A.0. Get regional crosswalk read 
use "$xwalk/cw_cty_czone.dta", clear
rename cty_fips pid 
duplicates drop 
tempfile cross
save "`cross'"

// A.0. Get link data 
use "$output_NETS/pr_link_est_countyXindustry_naic3.dta", clear 
gen pr_emp = pr_link*total if naic3 < 340 & naic3 > 309
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)

gen pr_l = tot_pr/tot_emp 
keep pr_l fips
rename  fips pid
rename pr_l pr_link_ind
duplicates drop 
tempfile pr 
save "`pr'" 

use "$output_NETS/pr_link_est_county_naic3.dta", clear 
rename  fips pid
tempfile pr2 
save "`pr2'" 

// A.1. Get QCEW data 
use "$qcewdata/extract_qcew_1990_2012_naic3.dta", clear
keep if strlen(industry_code) == 3
drop if industry_code == "101" | industry_code == "102"

order fips_state fips_county year industry_code
drop if fips_county == 0 & fips_state != 72
drop if fips_state == 72 & fips_county != 0
keep if own_code == 5		// 0-total covered; 1 Federal; 2 State; 3 Local; 4 Intl Govt; 5 Private.
keep fips_state fips_county year industry_code annual_avg_estabs annual_avg_emplvl total_annual_wages
drop if fips_state == 78

/*
// A.2. Label industries		
gen industry_cd = .
replace industry_cd = 0 if industry_code == "10"	// "Total, all industries"
replace industry_cd = 1 if industry_code == "311"
replace industry_cd = 2 if industry_code == "314"
replace industry_cd = 3 if industry_code == "315"
replace industry_cd = 4 if industry_code == "325"	// note: include 3254
replace industry_cd = 5 if industry_code == "3254"
replace industry_cd = 6 if industry_code == "326"
replace industry_cd = 7 if industry_code == "316"
replace industry_cd = 8 if industry_code == "332"
replace industry_cd = 9 if industry_code == "333"
replace industry_cd = 10 if industry_code == "335"
*replace industry_cd = 11 if naic3 == 
replace industry_cd = 12 if industry_code == "31-33"
replace industry_cd = 13 if industry_code == "52" | industry_code == "53"
replace industry_cd = 14 if inlist(industry_code,"54","55","61","62","71","72","81")
replace industry_cd = 15 if inlist(industry_code,"42","44-45")
*replace industry_cd = 16 if industry_code == .

label define l_naic3 0 "Total, all industries" 1 "Food Mfg" 2 "Textile mill products" 3 "Apparel" 4 "Chemicals" 5 "Pharmaceuticals" ///
 6 "Rubber and Plastic" 7 "Leather" 8 "Fabricated metal" 9 "Machinery" 10 "Electrical equip" ///
 11 "Instruments" 12 "Other mfg" 13 "Finance, insurance, real estate" 14 "Services" ///
 15 "Wholesale and retail" 16 "Other non-mfg", replace
label values industry_cd l_naic3

drop if industry_cd == 0
*/
collapse (sum) annual_avg_estabs (sum) annual_avg_emplvl (sum) total_annual_wages, by(fips_state fips_county year industry_code)

// A.3. Fill in gaps
gen pid = 1000*fips_state+fips_county

// A.6. Merge in pr link 
merge m:1 pid using "`pr'"
keep if _merg == 3
drop _merge 

merge m:1 pid using "`pr2'"
drop if year == . 
drop if pr_link == . 
drop _merge 
rename pr_link pr_link_all

*replace pr_link_all = pr_link_ind

sum pr_link_al, d 

drop if year == . 

*collapse (rawsum) annual_avg_estabs (rawsum) annual_avg_emplvl (rawsum) total_annual_wages (mean) pr_link_ind (mean) pr_link_all [fw=total] , by( fips_state pid year industry_code) // Dan: this isn't actually collapsing anything

egen pid2 = group(pid indus)

tsset pid2 year
tsfill, full

foreach var of varlist fips_state  {
bysort pid2: egen temp = max(`var')
replace `var' = temp
drop temp
}

foreach var of varlist annual_avg_estabs annual_avg_emplvl total_annual_wages {
replace `var' = 0 if `var' == .
}

// A.4.  Growth rates. Base year: 1995
gen temp = annual_avg_emplvl if year == 1995
bysort pid2: egen base_emp = max(temp)
drop temp

gen emp_growth = (annual_avg_emplvl - base_emp)/base
replace emp_growth = 0 if emp_growth == . 


// Income 
gen inc = total_annual_wages
* /annual_avg_emplvl
gen temp = inc if year == 1995
bysort pid2: egen base_inc = max(temp)
drop temp

gen inc_growth = (inc - base_inc)/base_inc
replace inc_growth = 0 if inc_growth == . 

// Estabs 
gen estab = annual_avg_estabs
gen temp = estab if year == 1995
bysort pid2: egen base_estab = max(temp)
drop temp

gen estab_growth = (estab - base_estab)/base_estab
replace estab_growth = 0 if estab_growth == . 

** For now, keep only balanced industries . Matt: can you fix it so we don't have to do this? 
bys pid2: gen count = _N
keep if count == 23 
drop count 

drop if base_emp ==0 

sum base_emp if year == 1995, d
gen wgt=base_emp/(r(N) * r(mean))
winsor wgt, p(.01) gen(wgt_w)

sum pr_link_i, d 
replace pr_link_i = pr_link_i/(r(p75)-r(p25))

sum pr_link_al, d 
replace pr_link_al = pr_link_al/(r(p75)-r(p25))

tsset pid2 year 
egen ind_st = group(ind fips_state)

// B. Graphs with All PR Link 
cd "$output" 
set more off 
xi i.year|pr_link_i, noomit
drop _IyeaXp*1995

*keep if base_emp > 400  
winsor base_emp , g(w_emp_base) p(.025)
*replace base_emp = w_emp_base
sum base_emp, d 
*drop if base_emp <= r(p1) 

snapshot save 
gen naics = industry_code
*drop if base_emp >= r(p99)
destring industry_code, replace
merge m:1 naics using "$additional/tradable definition/trade_cats_3dNAICS"
drop _merge

* running the same regression 5 times. each time has a different variable as the notable interaction
* notable interactions will be put out in a table. All results in all 5 columns actually come from a single regression

drop if wgt == .
drop if has_yes == . 

gen base = 1
eststo emp_1:  reghdfe emp_growth c.base#(c._IyeaX*) [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust )
	estadd local hasiy "Yes"
	estadd local hascty "Yes"
	
local base2 "has_yes"
local base3 "has_no"
local base4 "has_other"
local base5 "has_const"

local other2 "c.has_no#(c._IyeaX*) c.has_other#(c._IyeaX*) c.has_const#(c._IyeaX*)"
local other3 "c.has_yes#(c._IyeaX*) c.has_other#(c._IyeaX*) c.has_const#(c._IyeaX*)"
local other4 "c.has_yes#(c._IyeaX*) c.has_no#(c._IyeaX*) c.has_const#(c._IyeaX*)"
local other5 "c.has_yes#(c._IyeaX*) c.has_no#(c._IyeaX*) c.has_other#(c._IyeaX*)"
forval i = 2/5 { 

replace base = `base`i''
	
eststo emp_`i':  reghdfe emp_growth c.base#(c._IyeaX*) `other`i'' [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust )
	estadd local hasiy "Yes"
	estadd local hascty "Yes"

}	
	
esttab emp* using "$output/Tables/table_HE_QCEW_naic3.tex", drop(*) stats() ///
b(3) se par label   ///
noobs nogap nomtitle tex replace nocons nonum ///
postfoot("\multicolumn{6}{l}{\textbf{Exposure to Section 936}}\\")


esttab emp*   using "$output/Tables/table_HE_QCEW_naic3.tex",  preh("") keep(c.base*) ///
 b(3) se par nonum mlab("Average Effect" "Tradable" "Non-Tradable" "Other" "Construction" ) coll(none) s() noobs  ///
coeflabel(c.base#c._IyeaXpr__1990 "\hspace{1em}X 1990 " /// 
c.base#c._IyeaXpr__1991 "\hspace{1em}X 1991 " /// 
c.base#c._IyeaXpr__1992 "\hspace{1em}X 1992 " /// 
c.base#c._IyeaXpr__1993 "\hspace{1em}X 1993 " /// 
c.base#c._IyeaXpr__1994 "\hspace{1em}X 1994 " /// 
c.base#c._IyeaXpr__1996 "\hspace{1em}X 1996 " /// 
c.base#c._IyeaXpr__1997 "\hspace{1em}X 1997 " /// 
c.base#c._IyeaXpr__1998 "\hspace{1em}X 1998 " /// 
c.base#c._IyeaXpr__1999 "\hspace{1em}X 1999 " /// 
c.base#c._IyeaXpr__2000 "\hspace{1em}X 2000 " /// 
c.base#c._IyeaXpr__2001 "\hspace{1em}X 2001 " /// 
c.base#c._IyeaXpr__2002 "\hspace{1em}X 2002 " /// 
c.base#c._IyeaXpr__2003 "\hspace{1em}X 2003 " /// 
c.base#c._IyeaXpr__2004 "\hspace{1em}X 2004 " /// 
c.base#c._IyeaXpr__2005 "\hspace{1em}X 2005 " /// 
c.base#c._IyeaXpr__2006 "\hspace{1em}X 2006 " /// 
c.base#c._IyeaXpr__2007 "\hspace{1em}X 2007 " /// 
c.base#c._IyeaXpr__2008 "\hspace{1em}X 2008 " /// 
c.base#c._IyeaXpr__2009 "\hspace{1em}X 2009 " /// 
c.base#c._IyeaXpr__2010 "\hspace{1em}X 2010 " /// 
c.base#c._IyeaXpr__2011 "\hspace{1em}X 2011 " /// 
c.base#c._IyeaXpr__2012 "\hspace{1em}X 2012 " )   scalars(  "hasiy Industry-by-Year Fixed Effects" "hascty County Fixed Effects" ) label /// 
 prefoot("\hline") ///
postfoot( ///
"  \\ \hline\hline" ///
"\end{tabular} }" ) append sty(tex)


*** same thing using discrete defintions for trade categories
gen yes = has_yes > 0
gen no = (has_no > 0)* (has_yes == 0)
gen other = (has_other > 0) * (has_no == 0)* (has_yes == 0)
gen const = (has_const > 0) * (has_other == 0) * (has_no == 0)* (has_yes == 0)

est clear
replace base = 1
eststo emp_1:  reghdfe emp_growth c.base#(c._IyeaX*) [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust )
	estadd local hasiy "Yes"
	estadd local hascty "Yes"
	
local base2 "yes"
local base3 "no"
local base4 "other"
local base5 "const"

local other2 "c.no#(c._IyeaX*) c.other#(c._IyeaX*) c.const#(c._IyeaX*)"
local other3 "c.yes#(c._IyeaX*) c.other#(c._IyeaX*) c.const#(c._IyeaX*)"
local other4 "c.yes#(c._IyeaX*) c.no#(c._IyeaX*) c.const#(c._IyeaX*)"
local other5 "c.yes#(c._IyeaX*) c.no#(c._IyeaX*) c.other#(c._IyeaX*)"
forval i = 2/5 { 

replace base = `base`i''
	
eststo emp_`i':  reghdfe emp_growth c.base#(c._IyeaX*) `other`i'' [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust )
	estadd local hasiy "Yes"
	estadd local hascty "Yes"

}	
	
esttab emp* using "$output/Tables/table_HE_QCEW_naic3_v2.tex", drop(*) stats() ///
b(3) se par label   ///
noobs nogap nomtitle tex replace nocons nonum ///
postfoot("\multicolumn{6}{l}{\textbf{Exposure to Section 936}}\\")


esttab emp*   using "$output/Tables/table_HE_QCEW_naic3_v2.tex",  preh("") keep(c.base*) ///
 b(3) se par nonum mlab("Average Effect" "Tradable" "Non-Tradable" "Other" "Construction" ) coll(none) s() noobs  ///
coeflabel(c.base#c._IyeaXpr__1990 "\hspace{1em}X 1990 " /// 
c.base#c._IyeaXpr__1991 "\hspace{1em}X 1991 " /// 
c.base#c._IyeaXpr__1992 "\hspace{1em}X 1992 " /// 
c.base#c._IyeaXpr__1993 "\hspace{1em}X 1993 " /// 
c.base#c._IyeaXpr__1994 "\hspace{1em}X 1994 " /// 
c.base#c._IyeaXpr__1996 "\hspace{1em}X 1996 " /// 
c.base#c._IyeaXpr__1997 "\hspace{1em}X 1997 " /// 
c.base#c._IyeaXpr__1998 "\hspace{1em}X 1998 " /// 
c.base#c._IyeaXpr__1999 "\hspace{1em}X 1999 " /// 
c.base#c._IyeaXpr__2000 "\hspace{1em}X 2000 " /// 
c.base#c._IyeaXpr__2001 "\hspace{1em}X 2001 " /// 
c.base#c._IyeaXpr__2002 "\hspace{1em}X 2002 " /// 
c.base#c._IyeaXpr__2003 "\hspace{1em}X 2003 " /// 
c.base#c._IyeaXpr__2004 "\hspace{1em}X 2004 " /// 
c.base#c._IyeaXpr__2005 "\hspace{1em}X 2005 " /// 
c.base#c._IyeaXpr__2006 "\hspace{1em}X 2006 " /// 
c.base#c._IyeaXpr__2007 "\hspace{1em}X 2007 " /// 
c.base#c._IyeaXpr__2008 "\hspace{1em}X 2008 " /// 
c.base#c._IyeaXpr__2009 "\hspace{1em}X 2009 " /// 
c.base#c._IyeaXpr__2010 "\hspace{1em}X 2010 " /// 
c.base#c._IyeaXpr__2011 "\hspace{1em}X 2011 " /// 
c.base#c._IyeaXpr__2012 "\hspace{1em}X 2012 " )   scalars(  "hasiy Industry-by-Year Fixed Effects" "hascty County Fixed Effects" ) label /// 
 prefoot("\hline") ///
postfoot( ///
"  \\ \hline\hline" ///
"\end{tabular} }" ) append sty(tex)


************ making a graph (works from last estimates)
estimates restore emp_5

gen const_tab = .
gen yes_tab = .
gen no_tab = .
gen other_tab = .
 
forval i = 1/5 {
local j = `i' + 1989

replace const_tab = _b[c.base#c._IyeaXpr__`j'] in `i'

foreach Q of var yes no other { 

replace `Q'_tab = _b[c.`Q'#c._IyeaXpr__`j'] in `i'
}
}
forval i = 7/23 {
local j = `i' + 1989

replace const_tab = _b[c.base#c._IyeaXpr__`j'] in `i'

foreach Q of var yes no other { 

replace `Q'_tab = _b[c.`Q'#c._IyeaXpr__`j'] in `i'
}
}

estimates restore emp_1

gen avg_tab = .

forval i = 1/5 {
local j = `i' + 1989

replace avg_tab = _b[c.base#c._IyeaXpr__`j'] in `i'
}
forval i = 7/23 {
local j = `i' + 1989

replace avg_tab = _b[c.base#c._IyeaXpr__`j'] in `i'
}


gen time = .
forval i = 1/23 {
replace time = `i' + 1989 in `i'
}

 twoway (line avg_tab time, lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid)))  ///
		(line yes_tab time, lpattern(dash))      ///
		(line no_tab time, lpattern(dash))      ///
		(line other_tab time, lpattern(dash))      ///
		(line const_tab time, lpattern(dash))      ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Average Effect") ///
	label(2 "Tradable")   ///
	label(3 "Non-Tradable")  ///
	label(4 "Other")  ///
	label(5 "Construction"))   ///
	ytitle("Percent Employment Growth") 
graph export "$output/Graphs/het_effects_tab_V2.pdf", replace	

***** table of average growth rates later on

foreach i of num 2000 2006 {
foreach Q of var yes no other const { 
gen `Q'_`i' = emp_gr if year == `i' & `Q' == 1

}
}

label var yes_2000 "Employment growth 1995-2000, Tradable"
label var yes_2006 "Employment growth 1995-2006, Tradable"
label var no_2000 "Employment growth 1995-2000, Non-Tradable"
label var no_2006 "Employment growth 1995-2006, Non-Tradable"
label var other_2000 "Employment growth 1995-2000, Other"
label var other_2006 "Employment growth 1995-2006, Other"
label var const_2000 "Employment growth 1995-2000, Construction"
label var const_2006 "Employment growth 1995-2006, construction"

local y2000 "yes_2000 no_2000 other_2000 const_2000"
local y2006 "yes_2006 no_2006 other_2006 const_2006"

estpost tabstat `y2000' `y2006' [aw=wgt], statistics(count mean sd p5 p25 p50 p75 p95) ///
	columns(statistics)
est store tempA

esttab tempA using "$output/Tables/sum_stats_HE_20180228.tex", replace ///
	refcat(yes_2000 "\emph{Employment Growth Rates in 2000}" yes_2006 "\emph{Employment Growth Rates in 2006}", nolab) /// 
	cells("count mean(fmt(3)) sd(fmt(3)) p5(fmt(2)) p25(fmt(2)) p50(fmt(2)) p75(fmt(2)) p95(fmt(2))" ) nonum label nomtitle tex collabels("Count" "Mean" "SD" "$5^{th}$" "$25^{th}$" "$50^{th}$" "75$^{th}$" "$95^{th}$") noobs sfmt(3)

* same thing only including counties without any PR link
foreach i of num 2000 2006 {
foreach Q of var yes no other const { 
gen `Q'_`i'_b = emp_gr if year == `i' & `Q' == 1 & pr_link_i == 0

}
}

label var yes_2000_b "Employment growth 1995-2000, Tradable"
label var yes_2006_b "Employment growth 1995-2006, Tradable"
label var no_2000_b "Employment growth 1995-2000, Non-Tradable"
label var no_2006_b "Employment growth 1995-2006, Non-Tradable"
label var other_2000_b "Employment growth 1995-2000, Other"
label var other_2006_b "Employment growth 1995-2006, Other"
label var const_2000_b "Employment growth 1995-2000, Construction"
label var const_2006_b "Employment growth 1995-2006, construction"

local y2000_b "yes_2000_b no_2000_b other_2000_b const_2000_b"
local y2006_b "yes_2006_b no_2006_b other_2006_b const_2006_b"

estpost tabstat `y2000_b' `y2006_b' [aw=wgt], statistics(count mean sd p5 p25 p50 p75 p95) ///
	columns(statistics)
est store tempA_b

esttab tempA_b using "$output/Tables/sum_stats_HE_20180228_b.tex", replace ///
	refcat(yes_2000_b "\emph{Employment Growth Rates in 2000}" yes_2006_b "\emph{Employment Growth Rates in 2006}", nolab) /// 
	cells("count mean(fmt(3)) sd(fmt(3)) p5(fmt(2)) p25(fmt(2)) p50(fmt(2)) p75(fmt(2)) p95(fmt(2))" ) nonum label nomtitle tex collabels("Count" "Mean" "SD" "$5^{th}$" "$25^{th}$" "$50^{th}$" "75$^{th}$" "$95^{th}$") noobs sfmt(3)

