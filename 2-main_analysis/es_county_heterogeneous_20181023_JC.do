* This file estimates separate event studies for a bunch of pr shocks by subsectors
* Author: dan
* date: 20181023

clear all
set more off
snapshot erase _all

// First Define Command for ES data 
capture program drop ES_graph_data
program define ES_graph_data
syntax,  level(real) yti(string) tshifter(real)


qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 


forval i = 1/23 { 
	mat time = (time,`i'-6+1995 + `tshifter')
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci_l es_ci_h time 
svmat es_b 
rename es_b `yti'_es_b
svmat time
rename time `yti'_time
svmat es_ci_l
rename es_ci_l `yti'_es_ci_l
svmat es_ci_h 
rename es_ci_h `yti'_es_ci_h
}

end  
 
// Second Define Command for ES 
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(dash))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Estimate") label(2 "`level'% CI")) ///
	ytitle("`yti'") ///
	note("`note'") ylab("`ylab'")
graph export "`outname'.pdf", replace	

end 


// A - Merge Data for Analysis 
// A.0. Get regional crosswalk read 
use "$xwalk/cw_cty_czone.dta", clear
rename cty_fips pid 
duplicates drop 
tempfile cross
save "`cross'"

// A.1. Get QCEW data 
use "$qcewdata/extract_qcew_1990_2012_naic3.dta", clear
keep if strlen(industry_code) == 3
drop if industry_code == "101" | industry_code == "102"

order fips_state fips_county year industry_code
drop if fips_county == 0 & fips_state != 72
drop if fips_state == 72 & fips_county != 0
keep if own_code == 5		// 0-total covered; 1 Federal; 2 State; 3 Local; 4 Intl Govt; 5 Private.
keep fips_state fips_county year industry_code annual_avg_estabs annual_avg_emplvl total_annual_wages
drop if fips_state == 78
collapse (sum) annual_avg_estabs (sum) annual_avg_emplvl (sum) total_annual_wages, by(fips_state fips_county year industry_code)

// A.3. Fill in gaps
gen pid = 1000*fips_state+fips_county

// A.6. Merge in pr link 
merge m:1 pid using "$additional/county_shocks"
keep if _merg == 3
drop _merge 

drop if year == . 

egen pid2 = group(pid indus)

tsset pid2 year
tsfill, full

foreach var of varlist fips_state  {
bysort pid2: egen temp = max(`var')
replace `var' = temp
drop temp
}

foreach var of varlist annual_avg_estabs annual_avg_emplvl total_annual_wages {
replace `var' = 0 if `var' == .
}

// A.4.  Growth rates. Base year: 1995
gen temp = annual_avg_emplvl if year == 1995
bysort pid2: egen base_emp = max(temp)
drop temp

gen emp_growth = (annual_avg_emplvl - base_emp)/base
replace emp_growth = 0 if emp_growth == . 

// Income 
gen inc = total_annual_wages
* /annual_avg_emplvl
gen temp = inc if year == 1995
bysort pid2: egen base_inc = max(temp)
drop temp

gen inc_growth = (inc - base_inc)/base_inc
replace inc_growth = 0 if inc_growth == . 

// Estabs 
gen estab = annual_avg_estabs
gen temp = estab if year == 1995
bysort pid2: egen base_estab = max(temp)
drop temp

gen estab_growth = (estab - base_estab)/base_estab
replace estab_growth = 0 if estab_growth == . 

** For now, keep only balanced industries.
bys pid2: gen count = _N
keep if count == 23 
drop count 

drop if base_emp ==0 

sum base_emp if year == 1995, d
gen wgt=base_emp/(r(N) * r(mean))
winsor wgt, p(.01) gen(wgt_w)

*normalizing all prl and fakel variables by an IQR for the baseline pr_link_ind variable
sum prl_base $pr_wgt if base_emp > 0, d
foreach Q of var prl_* fakel_* { 
replace `Q' = `Q'/(r(p75)-r(p25))
}

tsset pid2 year 
egen ind_st = group(ind fips_state)

// B. Regressions
cd "$output" 
set more off 

destring industry_code, replace
est clear 
set more off

*replace prl_chem=prl_chem+prl_pharm
replace prl_food=prl_food+prl_cloth
*replace prl_food=prl_food+prl_cloth
*prl_other

areg prl_all prl_food prl_elec prl_chem prl_other [aw=wgt]  if year == 1995, a( industr ) 
predict pr_resid, residual
bys pid: egen min_pr_resid = min(pr_resid)
replace pr_resid = min_pr 

xi i.year|prl_chem i.year|prl_food i.year|prl_elec i.year|prl_other i.year|pr_resid, noomit 

drop _IyeaXp*1995
eststo emp_`Q':  reghdfe_old emp_growth _IyeaX* [aw=wgt]  , a( i.year#i.industr i.pid) cl(fips_state indust )

mat b = e(b) 
mat b_chem = b[1,1..22]'
mat b_food = b[1,23..44]'
mat b_elec = b[1,45..66]'
mat b_oth = b[1,67..88]'

mat b_chem = (b_chem[1..5,1]',0,b_chem[6..22,1]' )' 
mat b_food = (b_food[1..5,1]',0,b_food[6..22,1]' )' 
mat b_elec = (b_elec[1..5,1]',0,b_elec[6..22,1]' )' 
mat b_oth = (b_oth[1..5,1]',0,b_oth[6..22,1]' )' 


svmat b_chem 
rename b_chem s_prl_chem_es_b

svmat b_food 
rename b_food s_prl_food_es_b

svmat b_elec 
rename b_elec s_prl_elec_es_b

svmat b_oth
rename b_oth s_prl_oth_es_b

xi i.year|prl_base, noomit 

drop _IyeaXp*1995
eststo emp_`Q':  reghdfe_old emp_growth _IyeaX* [aw=wgt]  , a( i.year#i.industr i.pid) cl(fips_state indust )
ES_graph_data , level(95) yti("s_prl_base") tshifter(0)


* Graph output of the specifications
 twoway (line s_prl_base_es_b s_prl_base_time, lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid)))  ///
		(rcap s_prl_base_es_ci_l s_prl_base_es_ci_h s_prl_base_time,  lcolor(navy) lpattern(dash))  ///
		(line s_prl_food_es_b s_prl_base_time, lcolor(orange) lpattern(solid))      ///
		 (line s_prl_chem_es_b s_prl_base_time, lcolor(green) lpattern(solid))    ///
		(line s_prl_elec_es_b s_prl_base_time, lcolor(purple) lpattern(solid))    ///
		(line s_prl_oth_es_b s_prl_base_time, lcolor(red) lpattern(solid))    ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Baseline") ///
	label(3 "Food, Bev, Tobacco")   ///
	label(4 "Chemicals")  ///
	label(5 "Electronics")  ///
	label(6 "Other")  ///
	order(1 4 5 3 6)) ///
	ytitle("Percent Employment Growth") 
graph export "$output/fig_es_by_industry_JC.pdf", replace	


* Graph output of the specifications
 twoway (line s_prl_base_es_b s_prl_base_time, lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid)))  ///
		(rcap s_prl_base_es_ci_l s_prl_base_es_ci_h s_prl_base_time,  lcolor(navy) lpattern(dash))  ///
		 (line s_prl_chem_es_b s_prl_base_time, lcolor(green) lpattern(solid))    ///
		(line s_prl_elec_es_b s_prl_base_time, lcolor(purple) lpattern(solid))    ///
		(line s_prl_oth_es_b s_prl_base_time, lcolor(red) lpattern(solid))    ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Baseline") ///
	label(3 "Chemicals")  ///
	label(4 "Electronics")  ///
	label(5 "Other")  ///
	order(1 3 4 5)) ///
	ytitle("Percent Employment Growth") 
graph export "$output/fig_es_by_industry_JC_2.pdf", replace	
