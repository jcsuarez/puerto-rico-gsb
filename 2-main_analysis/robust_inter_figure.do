// This file creates a figure using marginsplot that showcases interaction terms
// code adopted by dan
// date: 6-26-2018

use "$data/data_additional/St Tax Structure 05_10_2017/state_taxes_analysis.dta", clear
keep if year == 1990 
keep fips salestax propertytax corporate_rate GDP rev_totaltaxes rec_val rdcred rdcred trainingsubsidy jobcreationcred corpinctax totalincentives investment_credit 
rename fips statefips
tempfile incentives 
save "`incentives'"


use "$data/data_additional/Misallocation 5_10_2017/misallocation_paneldata.dta", clear
keep if year == 1990 
keep income_rate_avg fipstate
rename fips statefips
tempfile incentives2 
save "`incentives2'"


clear all
set more off
 
// First Define Command for ES 
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(dash))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Estimate") label(2 "`level'% CI")) ///
	ytitle("`yti'") ///
	note("`note'") ylab("`ylab'")
graph export "`outname'.pdf", replace	

end 


// A - Merge Data for Analysis 
// A.0. Get regional crosswalk read 
use "$xwalk/cw_cty_czone.dta", clear
rename cty_fips pid 
duplicates drop 
tempfile cross
save "`cross'"

// A.0. Get link data 
use "$output_NETS/pr_link_est_countyXindustry_naic3.dta", clear 
gen pr_emp = pr_link*total if naic3 < 340 & naic3 > 309
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)

gen pr_l = tot_pr/tot_emp 
keep pr_l fips
rename  fips pid
rename pr_l pr_link_ind
duplicates drop 
tempfile pr 
save "`pr'" 

use "$output_NETS/pr_link_est_county_naic3.dta", clear 
rename  fips pid
tempfile pr2 
save "`pr2'" 

// A.1. Get QCEW data 
use "$qcewdata/extract_qcew_1990_2012_naic3.dta", clear
keep if strlen(industry_code) == 3
drop if industry_code == "101" | industry_code == "102"

order fips_state fips_county year industry_code
drop if fips_county == 0 & fips_state != 72
drop if fips_state == 72 & fips_county != 0
keep if own_code == 5		// 0-total covered; 1 Federal; 2 State; 3 Local; 4 Intl Govt; 5 Private.
keep fips_state fips_county year industry_code annual_avg_estabs annual_avg_emplvl total_annual_wages
drop if fips_state == 78

/*   */
collapse (sum) annual_avg_estabs (sum) annual_avg_emplvl (sum) total_annual_wages, by(fips_state fips_county year industry_code)
// A.3. Fill in gaps
gen pid = 1000*fips_state+fips_county

// A.6. Merge in pr link 
merge m:1 pid using "`pr'"
keep if _merg == 3
drop _merge 

merge m:1 pid using "`pr2'"
drop if year == . 
drop if pr_link == . 
drop _merge 
rename pr_link pr_link_all

*replace pr_link_all = pr_link_ind

sum pr_link_al, d 
drop if year == . 

*collapse (rawsum) annual_avg_estabs (rawsum) annual_avg_emplvl (rawsum) total_annual_wages (mean) pr_link_ind (mean) pr_link_all [fw=total] , by( fips_state pid year industry_code)

egen pid2 = group(pid indus)

tsset pid2 year
tsfill, full

foreach var of varlist fips_state  {
bysort pid2: egen temp = max(`var')
replace `var' = temp
drop temp
}

foreach var of varlist annual_avg_estabs annual_avg_emplvl total_annual_wages {
replace `var' = 0 if `var' == .
}

// A.4.  Growth rates. Base year: 1995
gen temp = annual_avg_emplvl if year == 1995
bysort pid2: egen base_emp = max(temp)
drop temp

gen emp_growth = (annual_avg_emplvl - base_emp)/base
replace emp_growth = 0 if emp_growth == . 


// Income 
gen inc = total_annual_wages/annual_avg_emplvl
gen temp = inc if year == 1995
bysort pid2: egen base_inc = max(temp)
drop temp

gen inc_growth = (inc - base_inc)/base_inc
replace inc_growth = 0 if inc_growth == . 

// Estabs 
gen estab = annual_avg_estabs
gen temp = estab if year == 1995
bysort pid2: egen base_estab = max(temp)
drop temp

gen estab_growth = (estab - base_estab)/base_estab
replace estab_growth = 0 if estab_growth == . 


// A.5. Define PR Growth Data
/*
gen temp = emp_growth if fips_state == 72
bysort year ind: egen emp_growth_PR = max(temp) 
drop temp
drop if emp_growth_PR == . 
*/
bys pid2: gen count = _N
keep if count == 23 
drop count 

sum base_emp, d
gen wgt=base_emp/(r(N) * r(mean))
winsor wgt, p(.01) gen(wgt_w)

sum pr_link_i $pr_wgt if base_emp > 0, d 
replace pr_link_i = pr_link_i/(r(p75)-r(p25))

sum pr_link_al $pr_wgt if base_emp > 0, d 
replace pr_link_al = pr_link_al/(r(p75)-r(p25))


** Keeping only relevant years to make this not crash
keep if inlist(year,1995,2004,2005,2006,2007,2008)

// Add demographics and other shocks 
rename fips_county temp_fips_county
gen fips_county = pid 
merge m:1 fips_county using "$additional/county_level_data_all.dta"
drop if _merge == 2 
drop _merge 

merge m:1 statefips  using "`incentives'"
drop if _merge == 2
drop _merge 

merge m:1 statefips using "`incentives2'"
drop if _merge == 2
drop _merge 


** Add nafta variables 
merge m:1 fips_county using "$output/import_hakobyanlaren_nafta.dta"
drop if _merge == 2 
drop _merge 

tsset pid2 year 
egen ind_st = group(ind fips_state)


**** Merge in share of firm size dist 
merge m:1 pid year using "$output/CBP_firm_size_dist"
drop if _merge == 2
drop _merge 

/*
foreach var of varlist n1_4-n250p { 
	sum `var', d
	replace `var' = `var'/(r(p75)-r(p25))
	
	gen temp = `var' if year == 1995
	bysort pid2: egen base_`var' = max(temp)
	drop temp

	gen `var'_growth = (`var' - base_`var')/base_`var'
	replace `var'_growth = 0 if `var'_growth == . 
}
*/

*gen ln_pr = ln(1+pr_link_a) 
*egen std_ln = std(ln_pr)
*winsor std_ln , g(w_std_ln) p(.01) 

bys statefips  : egen state_pop = total(population )
replace rev_totaltaxes = rev_tot/state_pop
replace capital_stock = capital_stock/state_pop

keep if inrange(year,2004,2008)

rename total total_orig

rename totalincentives total
rename jobcreationcred job
rename trainingsubsidy train
rename realmw mw
rename rgtowork rtw
rename rdcred rd
rename investment_credit ic
rename corporate_rate corp
rename income_rate_avg ptax
rename propertytax prop
rename salestax sales
rename d_tradeusch_pw trade
rename rev_totaltaxes rev
rename l_sh_routine33a routine
rename locdt_noag nafta 
rename capital_stock capital

/*
rename n5_9_growth n1
rename n10_19_growth n2
rename n20_49_growth n3
rename n50_99_growth n4
rename n100_249_growth n5
rename n250p_growth n6
*/
gen samp1 = 1

local vars "total job train mw rtw rd ic corp ptax prop sales trade rev routine nafta" // capital

foreach Q of var `vars' { 
replace samp1 = 0 if `Q' == .
sum `Q' [aw=wgt] if inrange(year,2004,2008) & samp1 == 1, d
gen double temp_mu = r(mean) 
gen double temp_z = r(sd)
gen double std_`Q' = (`Q' - temp_mu) / temp_z 

capture: drop temp_*
}

*** need to solidify sample before standardizing 
sum pr_link_ind $pr_wgt if inrange(year,2004,2008) & samp1 == 1, d
gen cen_pr_link_i = pr_link_i -r(mean) 

sum pr_link_all $pr_wgt if inrange(year,2004,2008) & samp1 == 1, d
gen cen_pr_link_a = pr_link_a -r(mean) 

/*
foreach Q of var n1 n2 n3 n4 n5 n6 { 
sum `Q' [aw=wgt] if inrange(year,2004,2008) & samp1 == 1, d
gen double temp_mu = r(mean) 
gen double temp_z = r(sd)
gen double fs_std_`Q' = (`Q' - temp_mu) / temp_z 

capture: drop temp_*
}
*/

*** winsorizing
foreach var of varlist std_* { 
	winsor `var', g(w_`var') p(.01)
	replace `var' = w_`var'
}

destring industry_code, replace

// First take at interactions 
est clear 
eststo rob_0: reghdfe emp_growth cen_pr_link_i $pra_control_rob std_* [aw=wgt] if inrange(year,2004,2008) & samp1 == 1 ,  a( i.year  i.industr) cl(fips_state  i.industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hass "Yes"		
	estadd local hasc "Yes"		
foreach var of varlist std_* { 
		eststo rob_`var': reghdfe emp_growth cen_pr_link_i $pra_control_rob std_* c.cen_pr_link_i#c.`var' [aw=wgt] if inrange(year,2004,2008) & samp1 == 1 ,  a( i.year i.industr) cl(fips_state  i.industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hass "Yes"		
	estadd local hasc "Yes"		
	} 
eststo rob_all: reghdfe emp_growth cen_pr_link_i std_* $pra_control_rob c.cen_pr_link_i#(c.std_*) [aw=wgt] if inrange(year,2004,2008) & samp1 == 1 ,  a( i.year  i.industr) cl(fips_state industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hass "Yes"		
	estadd local hasc "Yes"			
	
** existing graph with individual interaction terms
	
	coefplot rob_all /// 
 	, keep(*cen_pr_link_i*) ciopts(recast(rcap)) drop(cen_pr_link_i) ///
	graphregion(color(white)) xlab(-.1(.02).1) xline(0) ///
	rename(c.cen_pr_link_i#c.std_total = "Total Incentives (Bartik)"  ///
	c.cen_pr_link_i#c.std_job ="Job Creation Incentive" /// 
	c.cen_pr_link_i#c.std_train ="Job Training Subsidy" /// 
	c.cen_pr_link_i#c.std_mw ="Real Minimum Wage" /// 
	c.cen_pr_link_i#c.std_rtw ="Right to Work" /// 
	c.cen_pr_link_i#c.std_rd ="RD Tax Credit" /// 
	c.cen_pr_link_i#c.std_ic ="Investment Tax Credit" /// 
	c.cen_pr_link_i#c.std_corp ="Corporate Income Tax" /// 
	c.cen_pr_link_i#c.std_ptax ="Personal Income Tax" /// 
	c.cen_pr_link_i#c.std_prop ="Property Tax"  /// 
	c.cen_pr_link_i#c.std_sales ="Sales Tax" /// 
	c.cen_pr_link_i#c.std_capital ="Capital Stock" /// 
	c.cen_pr_link_i#c.std_trade ="Trade Exposure (China)" /// 
	c.cen_pr_link_i#c.std_rev ="State Revenue Per Capita" /// 
	c.cen_pr_link_i#c.std_routine ="Share Routine Workers" ///
	c.cen_pr_link_i#c.std_nafta ="Trade Exposure (Nafta)" /// 
	) levels(90)
	
	graph export "$output/Graphs/inter_terms.pdf", replace 

*** split tables
esttab rob_0 rob_std_total rob_std_job rob_std_train ///
rob_std_mw rob_std_rtw rob_std_rd rob_std_ic rob_std_corp ///
 using "$output/Tables/table_inter_QCEW_1.tex", keep(cen_pr_link_i) stats() ///
cells(b(fmt(3)) se(par) p) mlab(none) label  ///
noobs nogap nomtitle tex replace coll(none) nocons ///
postfoot("\multicolumn{10}{l}{Interaction of Exposure with:} \\") coeflabel(cen_pr_link_i "Exposure to Section 936 ")

esttab rob_0 rob_std_total rob_std_job rob_std_train ///
rob_std_mw rob_std_rtw rob_std_rd rob_std_ic rob_std_corp ///
  using "$output/Tables/table_inter_QCEW_1.tex", drop(std* cen_pr_link_i $pra_control_rob _cons)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) nonum s() noobs scalars(  "hasy Year Fixed Effects" "hasind Industry Fixed Effects" ///
 "hasc Includes all Controls") /// 
coeflabel(cen_pr_link_i "Exposure to Section 936 "  /// 
c.cen_pr_link_i#c.std_total "Total Incentives (Bartik)"  /// 
c.cen_pr_link_i#c.std_job "Job Creation Incentive" /// 
c.cen_pr_link_i#c.std_train "Job Training Subsidy" /// 
c.cen_pr_link_i#c.std_mw "Real Minimum Wage" /// 
c.cen_pr_link_i#c.std_rtw "Right to Work" /// 
c.cen_pr_link_i#c.std_rd "RD Tax Credit" /// 
c.cen_pr_link_i#c.std_ic "Investment Tax Credit" /// 
c.cen_pr_link_i#c.std_corp "Corporate Income Tax" /// 
c.cen_pr_link_i#c.std_ptax "Personal Income Tax" /// 
c.cen_pr_link_i#c.std_prop "Property Tax"  /// 
c.cen_pr_link_i#c.std_sales "Sales Tax" /// 
c.cen_pr_link_i#c.std_capital "Capital Stock" /// 
c.cen_pr_link_i#c.std_trade "Trade Exposure (China)" /// 
c.cen_pr_link_i#c.std_rev "State Revenue Per Capita" /// 
c.cen_pr_link_i#c.std_routine "Share Routine Workers " ///
c.cen_pr_link_i#c.std_nafta "Trade Exposure (Nafta)" /// 
c.cen_pr_link_i#c.pr_link_ind "\hspace{1em}Exposure to Section 936 ") ///
label /// 
 prefoot("\hline") ///
postfoot( ///
"  \hline\hline" ///
"\end{tabular} }" ) preh(" ") append sty(tex)


//
esttab  rob_0 rob_std_ptax rob_std_prop rob_std_sales /// 
 rob_std_trade rob_std_rev rob_std_routine rob_std_nafta rob_all ///
 using "$output/Tables/table_inter_QCEW_2.tex", coll(none) keep(cen_pr_link_i) stats() ///
cells(b(fmt(3)) se(par) p) label ///
noobs nogap nomtitle tex replace nocons ///
postfoot("\multicolumn{10}{l}{Interaction of Exposure with:} \\") coeflabel(cen_pr_link_i "Exposure to Section 936 ")

esttab  rob_0 rob_std_ptax rob_std_prop rob_std_sales /// 
 rob_std_trade rob_std_rev rob_std_routine rob_std_nafta rob_all ///
  using "$output/Tables/table_inter_QCEW_2.tex", drop(std* cen_pr_link_i $pra_control_rob _cons)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s() nonum noobs nogap scalars(  "hasy Year Fixed Effects" "hasind Industry Fixed Effects" ///
"hasc Includes all Controls") /// 
coeflabel(cen_pr_link_i "Exposure to Section 936 "  /// 
c.cen_pr_link_i#c.std_total "Total Incentives (Bartik)"  /// 
c.cen_pr_link_i#c.std_job "Job Creation Incentive" /// 
c.cen_pr_link_i#c.std_train "Job Training Subsidy" /// 
c.cen_pr_link_i#c.std_mw "Real Minimum Wage" /// 
c.cen_pr_link_i#c.std_rtw "Right to Work" /// 
c.cen_pr_link_i#c.std_rd "RD Tax Credit" /// 
c.cen_pr_link_i#c.std_ic "Investment Tax Credit" /// 
c.cen_pr_link_i#c.std_corp "Corporate Income Tax" /// 
c.cen_pr_link_i#c.std_ptax "Personal Income Tax" /// 
c.cen_pr_link_i#c.std_prop "Property Tax"  /// 
c.cen_pr_link_i#c.std_sales "Sales Tax" /// 
c.cen_pr_link_i#c.std_capital "Capital Stock" /// 
c.cen_pr_link_i#c.std_trade "Trade Exposure (China)" /// 
c.cen_pr_link_i#c.std_rev "State Revenue Per Capita" /// 
c.cen_pr_link_i#c.std_routine "Share Routine Workers " ///
c.cen_pr_link_i#c.std_nafta "Trade Exposure (Nafta)" /// 
c.cen_pr_link_i#c.pr_link_ind "\hspace{1em}Exposure to Section 936 ") ///
label /// 
 prefoot("\hline") ///
postfoot( ///
"  \hline\hline" ///
"\end{tabular} }" ) preh(" ")  append sty(tex)

// New shorter version of the interactions tables to be put in actual paper text 

local other "c.std_rev c.std_sales c.std_prop c.std_ptax c.std_ic c.std_rd c.std_rtw c.std_mw c.std_train c.std_total"

foreach var of varlist std_job std_corp std_trade std_routine std_nafta { 
		eststo rob_`var'_1: reghdfe emp_growth cen_pr_link_i $pra_control_rob std_* c.cen_pr_link_i#c.`var' c.cen_pr_link_i#(`other') [aw=wgt] if inrange(year,2004,2008) & samp1 == 1 ,  a( i.year i.industr) cl(fips_state  i.industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hass "Yes"		
	estadd local hasc "Yes"		
	} 

esttab rob_0 rob_std_job_1 rob_std_corp_1  rob_std_trade_1 rob_std_routine_1 rob_std_nafta_1 rob_all ///
 using "$output/Tables/table_inter_QCEW_short_allinter.tex", keep(cen_pr_link_i) stats() ///
cells(b(fmt(3)) se(par) p) mlab(none) label  ///
noobs nogap nomtitle tex replace coll(none) nocons ///
postfoot("\multicolumn{8}{l}{Interaction of Exposure with:} \\") coeflabel(cen_pr_link_i "Exposure to Section 936 ")

esttab  rob_0 rob_std_job_1 rob_std_corp_1  rob_std_trade_1 rob_std_routine_1 rob_std_nafta_1 rob_all ///
  using "$output/Tables/table_inter_QCEW_short_allinter.tex",  ///
keep(c.cen_pr_link_i#c.std_job c.cen_pr_link_i#c.std_corp c.cen_pr_link_i#c.std_trade c.cen_pr_link_i#c.std_nafta c.cen_pr_link_i#c.std_routine) ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) nonum s() noobs scalars(  "hasy Other Policy Interactions" "hasind Industry Fixed Effects" ///
 "hasc Includes all Controls") /// 
coeflabel(cen_pr_link_i "Exposure to Section 936 "  /// 
c.cen_pr_link_i#c.std_total "Total Incentives (Bartik)"  /// 
c.cen_pr_link_i#c.std_job "Job Creation Incentive" /// 
c.cen_pr_link_i#c.std_train "Job Training Subsidy" /// 
c.cen_pr_link_i#c.std_mw "Real Minimum Wage" /// 
c.cen_pr_link_i#c.std_rtw "Right to Work" /// 
c.cen_pr_link_i#c.std_rd "RD Tax Credit" /// 
c.cen_pr_link_i#c.std_ic "Investment Tax Credit" /// 
c.cen_pr_link_i#c.std_corp "Corporate Income Tax" /// 
c.cen_pr_link_i#c.std_ptax "Personal Income Tax" /// 
c.cen_pr_link_i#c.std_prop "Property Tax"  /// 
c.cen_pr_link_i#c.std_sales "Sales Tax" /// 
c.cen_pr_link_i#c.std_capital "Capital Stock" /// 
c.cen_pr_link_i#c.std_trade "Trade Exposure (China)" /// 
c.cen_pr_link_i#c.std_rev "State Revenue Per Capita" /// 
c.cen_pr_link_i#c.std_routine "Share Routine Workers " ///
c.cen_pr_link_i#c.std_nafta "Trade Exposure (Nafta)" /// 
c.cen_pr_link_i#c.pr_link_ind "\hspace{1em}Exposure to Section 936 ") ///
label /// 
 prefoot("\hline") ///
postfoot( ///
"  \hline\hline" ///
"\end{tabular} }" ) preh(" ") append sty(tex)	
	
****** graph with evaluation of the interaction at different points
estimates restore rob_all
sum std_corp [aw = wgt], d
sum std_job [aw = wgt], d
sum std_trade [aw = wgt], d
sum std_routine [aw = wgt], d
sum std_nafta [aw = wgt], d

* primary spec
	estimates restore rob_all
	local beta = _b[cen_pr_link_i]
	local mini = round(`beta'*2, -.03)
	local mini = max(`mini', -0.15)		
margins, dydx(cen_pr_link_i) at(std_ptax=1)  at(std_prop=1)  at(std_sales=1) ///
		 at(std_rev=1) at(std_train=1)  at(std_mw=1)  at(std_rtw=1) ///
		 at(std_rd=1) at(std_ic=1) at(std_nafta=1)  at(std_routine=1)  at(std_trade=1) ///
		 at(std_corp=-1) at(std_job=1) at(std_corp=0)
		
marginsplot /// 
 	, ciopts(recast(rcap)) ytitle("") recast(scatter) ///
	graphregion(color(white)) xline(`beta', lpattern(dash) lcolor(navy)) ///
	xline(0) bgcolor(white) xtitle("Marginal Effect of Exposure to S936 on Emp Growth, 2004-08") ///
	title("") nolab xdim(_atopt,lab("+1{&sigma} Personal Tax Rate" "+1{&sigma} Prop Tax Rate" ///
	"+1{&sigma} Sales Tax Rate"  ///
	"+1{&sigma} State Tax Revenue" "+1{&sigma} Job Training" "+1{&sigma} Minimum Wage" ///
	"+1{&sigma} Right to Work"  ///
	"+1{&sigma} R&D Credit" "+1{&sigma} Invest Incentive" "+1{&sigma} Nafta Exposure" "+1{&sigma} Routine Workers" ///
	"+1{&sigma} China Exposure"  ///
	"-1{&sigma} Corp Tax Rate" "+1{&sigma} Job Incentive" "ME at Average")) ///
    horiz
 
	graph export "$output/Graphs/inter_figure.pdf", replace  		

*** heterogeneous effects by geography
reghdfe emp_growth cen_pr_link_i std_* $pra_control_rob c.cen_pr_link_i#(c.std_*) [aw=wgt] if inrange(year,2004,2008) ,  a( i.year  i.industr) cl(fips_state industr) 

gen inter = _b[c.cen_pr_link_i#c.std_corp]*c.std_corp+_b[c.cen_pr_link_i#c.std_mw]*std_mw ///
+_b[c.cen_pr_link_i#c.std_ptax]*c.std_ptax+_b[c.cen_pr_link_i#c.std_ic]*c.std_ic ///
+_b[c.cen_pr_link_i#c.std_trade]*c.std_trade+_b[c.cen_pr_link_i#c.std_rev]*c.std_rev ///
+_b[c.cen_pr_link_i#c.std_routine]*c.std_routine+_b[c.cen_pr_link_i#c.std_nafta]*c.std_nafta

reghdfe emp_growth cen_pr_link_i std_* cen_pr_link_a c.inter#c.cen_pr_link_i [aw=wgt] if inrange(year,2004,2008) ,  a( i.year   i.industr) cl(fips_state industr) 

gen tot_eff = _b[cen_pr_link_i]+_b[c.inter#c.cen_pr_link_i]*inter 

sum tot_eff [fw=base_emp] , d 
gen w_tot_eff = tot_eff 
replace w_tot_eff = r(p1) if tot_eff<r(p1)
replace w_tot_eff = r(p99) if tot_eff>r(p99)

winsor tot_eff , g(w_tot_eff2) p(.05)

preserve 
keep w_tot_eff pid base_emp wgt
duplicates drop 
gen county = pid

// SCALARS for the text
sum w_tot_eff [aw=wgt]

do "$analysis/jc_stat_out.ado"
local b = -r(mean)*100
jc_stat_out,  number(`b') name("RESULTXXXIII") replace(0) deci(1) figs(3)

cdfplot w_tot_eff [aw=wgt] , yline(0) xline(0, lcolor(black) lpattern(dash) ) xline(`r(mean)', lcolor(red))	graphregion(fcolor(white)) ///
	xtitle("Esimates of Heterogeneous Effect") ///
	ytitle("Cummulative Distribution Function") ylab(0(.25)1) xlab(-.15(.05).05)
graph export "$output/Graphs/het_eff_cdf.pdf", replace 

keep w_tot_eff pid 
duplicates drop 
gen county = pid
save "$output/Graphs/het_eff_map_data", replace
maptile w_tot_eff , geo(county1990) stateoutline(linewidthstyle)  nquantiles(4)   revcolor  res(.5)
 
graph export "$output/Graphs/het_eff_map.pdf", replace 
restore 
	
	
		
	/*	
margins, dydx(cen_pr_link_i) at(std_ptax=1)  at(std_prop=1)  at(std_sales=1) ///
		 at(std_rev=1) at(std_train=1)  at(std_mw=1)  at(std_rtw=1) ///
		 at(std_rd=1) at(std_ic=1) at(std_corp=0)		

marginsplot /// 
 	, ciopts(recast(rcap)) ytitle("") recast(scatter) ///
	graphregion(color(white)) xline(-0.0841, lpattern(dash) lcolor(navy)) ///
	xline(0) bgcolor(white) xtitle("Marginal Effect of Exposure to S936 on Emp Growth, 2004-08") ///
	title("") nolab xdim(_atopt,lab("+1{&sigma} Personal Tax Rate" "+1{&sigma} Prop Tax Rate" ///
	"+1{&sigma} Sales Tax Rate"  ///
	"+1{&sigma} State Tax Revenue" "+1{&sigma} Job Training" "+1{&sigma} Minimum Wage" ///
	"+1{&sigma} Right to Work"  ///
	"+1{&sigma} R&D Credit" "+1{&sigma} Invest Incentive" "ME at Average")) ///
    horiz
 
	graph export "$output/Graphs/inter_figure_app1.pdf", replace  			 
		/* 
margins, dydx(cen_pr_link_i) at(std_ptax=1)  at(std_prop=1)  at(std_sales=1) ///
		 at(std_capital=1) at(std_rev=1) at(std_corp=0)		
		
		
marginsplot /// 
 	, ciopts(recast(rcap)) ytitle("") recast(scatter) ///
	graphregion(color(white)) xline(-0.0841, lpattern(dash) lcolor(navy)) ///
	xline(0) bgcolor(white) xtitle("Marginal Effect of Exposure to S936 on Emp Growth, 2004-08") ///
	title("") nolab xdim(_atopt,lab("+1{&sigma} Personal Tax" "+1{&sigma} Prop Tax Rate" ///
	"+1{&sigma} Sales Tax"  ///
	"-1{&sigma} Capital Stock" "+1{&sigma} Tax Revenue" "ME at Average")) ///
    horiz
 
	graph export "$output/Graphs/inter_figure_app2.pdf", replace  			
/*
* test spec
margins, dydx(cen_pr_link_i) at(std_nafta=1.0755)  at(std_routine=1.3902)  at(std_trade=1.462) ///
		 at(std_corp=-2.19) at(std_job=1.2207) at(std_corp=0)
marginsplot /// 
 	, ciopts(recast(rcap)) ytitle("") recast(scatter) ///
	graphregion(color(white)) xline(-0.0841, lpattern(dash) lcolor(navy)) ///
	xline(0) bgcolor(white) xtitle("Marginal Effect of Exposure on Emp Growth, 2004-08") ///
	title("") nolab xdim(_atopt,lab("95th Pct Nafta Exposure" "95th Pct Routine Workers" ///
	"95th Pct China Exposure"  ///
	"Zero Corp Tax Rate" "99th Pct Job Incentive" "ME at Average")) ///
    horiz
 
	graph export "$output/Graphs/inter_figure_v2.pdf", replace  		
	