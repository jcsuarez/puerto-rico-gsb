* This .do file makes a massive table of all ES Graph Data in one graph
*
* Author: Dan Garrett
* Date of Creation: 1-29-2018

clear
set more off 

*** Importing long data 
use "$qcewdata/ES_graph.dta", clear

* moving times around to kind of work
replace spec1_time = spec1_time - .1
replace spec2_time = spec2_time - .1
*replace spec3_time = spec3_time - .4
replace spec4_time = spec4_time - .2
replace spec5_time = spec5_time + .1
replace spec6_time = spec6_time + .1

foreach i of numlist 1 2 4 5 6 {
gen spec`i'_time_a = spec`i'_time - .033
gen spec`i'_time_b = spec`i'_time + .033
}

foreach Q of var *spec?_es_* {
replace `Q' = `Q' // * 100 
}


// DAN's new graph just combining # 2 from conspuma and CZ
 twoway (line cons_spec1_es_b spec1_time			, lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid)))  ///
		(rcap cons_spec1_es_ci_l cons_spec1_es_ci_h spec1_time	, lcolor(navy)  lpattern(solid))  ///
		(line czone_spec1_es_b spec2_time							, lcolor(orange)  lpattern(dash))      ///
		(rcap czone_spec1_es_ci_l czone_spec1_es_ci_h spec2_time	, lcolor(orange)  lpattern(dash))   /// 
		(line spec2_inst_es_b spec6_time							, lcolor(red)  lpattern(shortdash))      ///
		(rcap spec2_inst_es_ci_l spec2_inst_es_ci_h spec6_time	, lcolor(red)  lpattern(shortdash))   /// 
		(line spec2_es_b spec4_time							, lcolor(black)  lpattern(longdash))      ///
		(rcap spec2_es_ci_l spec2_es_ci_h spec4_time	, lcolor(black)  lpattern(longdash))   /// 
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") ytitle("Percent Employment Growth" , height(3) margin(medsmall))  ///
	xlab(1990(5)2012) bgcolor(white) ///
	legend(label(1 "Consupuma") ///
	label(3 "Commuting Zone") label(5 "Instrument for Employment Link") label(7 "Benchmark") order(7 1 3 5)) 
	
graph export "$output/Graphs/fig_7_robust_cons_and_cz.pdf", replace	

// md, fd, and compu
*** Importing long data 
merge 1:1 year using  "$qcewdata/ES_graph_md.dta"

drop *_time

* moving times around to kind of work
gen spec1_time = year - .2
gen spec2_time = year - .066
gen spec4_time = year + .066
gen spec5_time = year + .2


 twoway (line spec2_md_es_b spec1_time						, lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid)))  ///
		(rcap spec2_md_es_ci_l spec2_md_es_ci_h spec1_time	, lcolor(navy)  lpattern(solid))  ///
		(line spec2_fd_es_b spec2_time						, lcolor(orange)  lpattern(dash))      ///
		(rcap spec2_fd_es_ci_l spec2_fd_es_ci_h spec2_time	, lcolor(orange)  lpattern(dash))   /// 
		(line spec2_compu_es_b spec5_time							, lcolor(red)  lpattern(shortdash))      ///
		(rcap spec2_compu_es_ci_l spec2_compu_es_ci_h spec5_time	, lcolor(red)  lpattern(shortdash))   /// 
		(line spec2_es_b spec4_time							, lcolor(black)  lpattern(longdash))      ///
		(rcap spec2_es_ci_l spec2_es_ci_h spec4_time	, lcolor(black)  lpattern(longdash))   /// 
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") ytitle("Percent Employment Growth" , height(3) margin(medsmall))  ///
	xlab(1990(5)2012) bgcolor(white) ///
	legend(label(1 "Large Retailers Dropped") ///
	label(3 "Uncertain HQ Dropped") label(5 "Non-Compustat Dropped") label(7 "Benchmark") order(7 1 3 5)) 
	
graph export "$output/Graphs/fig_7_robust_md_and_fd.pdf", replace	

// differently combined versions

 twoway (line cons_spec1_es_b spec1_time			, lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid)))  ///
		(rcap cons_spec1_es_ci_l cons_spec1_es_ci_h spec1_time	, lcolor(navy)  lpattern(solid))  ///
		(line czone_spec1_es_b spec2_time							, lcolor(orange)  lpattern(dash))      ///
		(rcap czone_spec1_es_ci_l czone_spec1_es_ci_h spec2_time	, lcolor(orange)  lpattern(dash))   /// 
		(line spec2_PC_es_b spec5_time							, lcolor(red)  lpattern(shortdash))      ///
		(rcap spec2_PC_es_ci_l spec2_PC_es_ci_h spec5_time	, lcolor(red)  lpattern(shortdash))   /// 
		(line spec2_es_b spec4_time							, lcolor(black)  lpattern(longdash))      ///
		(rcap spec2_es_ci_l spec2_es_ci_h spec4_time	, lcolor(black)  lpattern(longdash))   /// 
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") ytitle("Percent Employment Growth" , height(3) margin(medsmall))  ///
	xlab(1990(5)2012) bgcolor(white) ///
	legend(label(1 "Consupuma") ///
	label(3 "Commuting Zone") label(5 "Per Capita") label(7 "Benchmark") order(7 1 3 5)) 

graph export "$output/Graphs/fig_7_robust_new1.pdf", replace	


 twoway (line spec2_md_es_b spec1_time						, lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid)))  ///
		(rcap spec2_md_es_ci_l spec2_md_es_ci_h spec1_time	, lcolor(navy)  lpattern(solid))  ///
		(line spec2_fd_es_b spec2_time						, lcolor(orange)  lpattern(dash))      ///
		(rcap spec2_fd_es_ci_l spec2_fd_es_ci_h spec2_time	, lcolor(orange)  lpattern(dash))   /// 
		(line spec2_compu_es_b spec5_time							, lcolor(red)  lpattern(shortdash))      ///
		(rcap spec2_compu_es_ci_l spec2_compu_es_ci_h spec5_time	, lcolor(red)  lpattern(shortdash))   /// 
		(line spec2_inst_es_b spec4_time							, lcolor(black)  lpattern(longdash))      ///
		(rcap spec2_inst_es_ci_l spec2_inst_es_ci_h spec4_time	, lcolor(black)  lpattern(longdash))   /// 
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") ytitle("Percent Employment Growth" , height(3) margin(medsmall))  ///
	xlab(1990(5)2012) bgcolor(white) ///
	legend(label(1 "Large Retailers Dropped") ///
	label(3 "Uncertain HQ Dropped") label(5 "Non-Compustat Dropped") label(7 "Instrument for Employment Link") order(7 1 3 5)) 


graph export "$output/Graphs/fig_7_robust_new2.pdf", replace

/* OLD STUFF

 twoway (line spec1_es_b spec1_time					, lcolor(orange) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid)))  ///
		(rcap spec1_es_ci_l spec1_es_ci_h spec1_time, lcolor(orange)  lpattern(dash))  ///
		(line spec2_es_b spec2_time					, lcolor(navy)  lpattern(solid))      ///
		(rcap spec2_es_ci_l spec2_es_ci_h spec2_time, lcolor(navy)  lpattern(dash))   /// 
		(line spec3_es_b spec3_time					, lcolor(yellow)  lpattern(solid))     ///
		(rcap spec3_es_ci_l spec3_es_ci_h spec3_time, lcolor(yellow)  lpattern(dash))    ///
		(line spec4_es_b spec4_time					, lcolor(forest_green)  lpattern(solid))    ///
		(rcap spec4_es_ci_l spec4_es_ci_h spec4_time, lcolor(forest_green)  lpattern(dash))    ///
		(line spec5_es_b spec5_time					, lcolor(maroon)  lpattern(solid))   ///
		(rcap spec5_es_ci_l spec5_es_ci_h spec5_time, lcolor(maroon)  lpattern(dash))   ///
		(line spec6_es_b spec6_time					, lcolor(teal)  lpattern(solid))    ///
		(rcap spec6_es_ci_l spec6_es_ci_h spec6_time, lcolor(teal)  lpattern(dash)) ///
		/// The following are the conspumas
		(line cons_spec1_es_b spec1_time_a							, lcolor(black)  lpattern(solid))  ///
		(rcap cons_spec1_es_ci_l cons_spec1_es_ci_h spec1_time_a	, lcolor(black)  lpattern(dash))  ///
		(line cons_spec2_es_b spec2_time_a							, lcolor(cranberry)  lpattern(solid))      ///
		(rcap cons_spec2_es_ci_l cons_spec2_es_ci_h spec2_time_a	, lcolor(cranberry)  lpattern(dash))   /// 
		(line cons_spec3_es_b spec3_time_a							, lcolor(eltblue)  lpattern(solid))     ///
		(rcap cons_spec3_es_ci_l cons_spec3_es_ci_h spec3_time_a	, lcolor(eltblue)  lpattern(dash))    ///
		(line cons_spec4_es_b spec4_time_a							, lcolor(magenta)  lpattern(solid))    ///
		(rcap cons_spec4_es_ci_l cons_spec4_es_ci_h spec4_time_a	, lcolor(magenta)  lpattern(dash))    ///
		(line cons_spec5_es_b spec5_time_a							, lcolor(khaki)  lpattern(solid))   ///
		(rcap cons_spec5_es_ci_l cons_spec5_es_ci_h spec5_time_a	, lcolor(khaki)  lpattern(dash))   ///
		(line cons_spec6_es_b spec6_time_a							, lcolor(emerald)  lpattern(solid))    ///
		(rcap cons_spec6_es_ci_l cons_spec6_es_ci_h spec6_time_a	, lcolor(emerald)  lpattern(dash)) ///
		/// The following are the czones
		(line czone_spec1_es_b spec1_time_b							, lcolor(orange_red)  lpattern(solid))  ///
		(rcap czone_spec1_es_ci_l czone_spec1_es_ci_h spec1_time_b	, lcolor(orange_red)  lpattern(dash))  ///
		(line czone_spec2_es_b spec2_time_b							, lcolor(erose)  lpattern(solid))      ///
		(rcap czone_spec2_es_ci_l czone_spec2_es_ci_h spec2_time_b	, lcolor(erose)  lpattern(dash))   /// 
		(line czone_spec3_es_b spec3_time_b							, lcolor(lime)  lpattern(solid))     ///
		(rcap czone_spec3_es_ci_l czone_spec3_es_ci_h spec3_time_b	, lcolor(lime)  lpattern(dash))    ///
		(line czone_spec4_es_b spec4_time_b							, lcolor(midblue)  lpattern(solid))    ///
		(rcap czone_spec4_es_ci_l czone_spec4_es_ci_h spec4_time_b	, lcolor(midblue)  lpattern(dash))    ///
		(line czone_spec5_es_b spec5_time_b							, lcolor(red)  lpattern(solid))   ///
		(rcap czone_spec5_es_ci_l czone_spec5_es_ci_h spec5_time_b	, lcolor(red)  lpattern(dash))   ///
		(line czone_spec6_es_b spec6_time_b							, lcolor(olive_teal)  lpattern(solid))    ///
		(rcap czone_spec6_es_ci_l czone_spec6_es_ci_h spec6_time_b	, lcolor(olive_teal)  lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 " ") ///
	label(3 " ")   ///
	label(5 " ")  ///
	label(7 " ")  ///
	label(9 " ")  ///
	label(11 " ")  ///
	label(13 " ") ///
	label(15 " ")   ///
	label(17 " ")  ///
	label(19 " ")  ///
	label(21 " ")  ///
	label(23 " ")  ///
	label(25 " ") ///
	label(27 " ")   ///
	label(29 " ")  ///
	label(31 " ")  ///
	label(33 " ")  ///
	label(35 " ") colgap(-2) keygap(0)  ///
	r(4) order( - "Spec:     IndXYear" - "          Loc. FE" - "          All Ind" - "          IndXLoc" - "         Wins. Wgts" - "        Drop<1K" - "" ///
	- "County:" 1 3 5 7 9 11 - "Consp:" 13 15 17 19 21 23 - "Czone:" 25 27 29 31 33 35)) ///
	ytitle("Percent Employment Growth", height(12)) 
graph export "$output/fig_7_robust_all.pdf", replace	


/// JC Selection 

 twoway (line spec1_es_b spec1_time					, lcolor(orange) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid)))  ///
		(rcap spec1_es_ci_l spec1_es_ci_h spec1_time, lcolor(orange)  lpattern(dash))  ///
		(line spec2_es_b spec2_time					, lcolor(navy)  lpattern(solid))      ///
		(rcap spec2_es_ci_l spec2_es_ci_h spec2_time, lcolor(navy)  lpattern(dash))   /// 
	///	(line spec3_es_b spec3_time					, lcolor(yellow)  lpattern(solid))     ///
	///	(rcap spec3_es_ci_l spec3_es_ci_h spec3_time, lcolor(yellow)  lpattern(dash))    ///
		(line spec4_es_b spec4_time					, lcolor(forest_green)  lpattern(solid))    ///
		(rcap spec4_es_ci_l spec4_es_ci_h spec4_time, lcolor(forest_green)  lpattern(dash))    ///
	///	(line spec5_es_b spec5_time					, lcolor(maroon)  lpattern(solid))   ///
	///	(rcap spec5_es_ci_l spec5_es_ci_h spec5_time, lcolor(maroon)  lpattern(dash))   ///
	///	(line spec6_es_b spec6_time					, lcolor(teal)  lpattern(solid))    ///
	///	(rcap spec6_es_ci_l spec6_es_ci_h spec6_time, lcolor(teal)  lpattern(dash)) ///
		/// The following are the conspumas
		(line cons_spec1_es_b spec1_time_a							, lcolor(black)  lpattern(solid))  ///
		(rcap cons_spec1_es_ci_l cons_spec1_es_ci_h spec1_time_a	, lcolor(black)  lpattern(dash))  ///
		(line cons_spec2_es_b spec2_time_a							, lcolor(cranberry)  lpattern(solid))      ///
		(rcap cons_spec2_es_ci_l cons_spec2_es_ci_h spec2_time_a	, lcolor(cranberry)  lpattern(dash))   /// 
	///	(line cons_spec3_es_b spec3_time_a							, lcolor(eltblue)  lpattern(solid))     ///
	///	(rcap cons_spec3_es_ci_l cons_spec3_es_ci_h spec3_time_a	, lcolor(eltblue)  lpattern(dash))    ///
		(line cons_spec4_es_b spec4_time_a							, lcolor(magenta)  lpattern(solid))    ///
		(rcap cons_spec4_es_ci_l cons_spec4_es_ci_h spec4_time_a	, lcolor(magenta)  lpattern(dash))    ///
	///	(line cons_spec5_es_b spec5_time_a							, lcolor(khaki)  lpattern(solid))   ///
	///	(rcap cons_spec5_es_ci_l cons_spec5_es_ci_h spec5_time_a	, lcolor(khaki)  lpattern(dash))   ///
	///	(line cons_spec6_es_b spec6_time_a							, lcolor(emerald)  lpattern(solid))    ///
	///	(rcap cons_spec6_es_ci_l cons_spec6_es_ci_h spec6_time_a	, lcolor(emerald)  lpattern(dash)) ///
		/// The following are the czones
		(line czone_spec1_es_b spec1_time_b							, lcolor(orange_red)  lpattern(solid))  ///
		(rcap czone_spec1_es_ci_l czone_spec1_es_ci_h spec1_time_b	, lcolor(orange_red)  lpattern(dash))  ///
		(line czone_spec2_es_b spec2_time_b							, lcolor(erose)  lpattern(solid))      ///
		(rcap czone_spec2_es_ci_l czone_spec2_es_ci_h spec2_time_b	, lcolor(erose)  lpattern(dash))   /// 
	///	(line czone_spec3_es_b spec3_time_b							, lcolor(lime)  lpattern(solid))     ///
	///	(rcap czone_spec3_es_ci_l czone_spec3_es_ci_h spec3_time_b	, lcolor(lime)  lpattern(dash))    ///
		(line czone_spec4_es_b spec4_time_b							, lcolor(midblue)  lpattern(solid))    ///
		(rcap czone_spec4_es_ci_l czone_spec4_es_ci_h spec4_time_b	, lcolor(midblue)  lpattern(dash))    ///
	///	(line czone_spec5_es_b spec5_time_b							, lcolor(red)  lpattern(solid))   ///
	///	(rcap czone_spec5_es_ci_l czone_spec5_es_ci_h spec5_time_b	, lcolor(red)  lpattern(dash))   ///
	///	(line czone_spec6_es_b spec6_time_b							, lcolor(olive_teal)  lpattern(solid))    ///
		(rcap czone_spec6_es_ci_l czone_spec6_es_ci_h spec6_time_b	, lcolor(olive_teal)  lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") ytitle("Percent Employment Growth" , height(3))  ///
	xlab(1990(5)2012) ///
	legend(label(1 "1") ///
	label(3 "2")   ///
	label(5 "3")  ///
	label(7 "4")  ///
	label(9 "5")  ///
	label(11 "6")  ///
	label(13 "1") ///
	label(15 "2")   ///
	label(17 "3")  ///
	label(19 "4")  ///
	label(21 "5")  ///
	label(23 "6")  ///
	label(25 "1") ///
	label(27 "2")   ///
	label(29 "3")  ///
	label(31 "4")  ///
	label(33 "5")  ///
	label(35 "6") colgap(0) keygap(0)  ///
	r(3) order( - "County:" 1 3 5 7 9 11 - "Conspuma:" 13 15 17 19 21 23 - "Czone:" 25 27 29 31 33 35)) 
	
graph export "$output/fig_7_robust_all_selected.pdf", replace	
*/
