* Runs regression using county-year observations on the transfers as a percent of population
* includes extra county level covariates
* date: 4-27-2018
clear
clear all
set more off
snapshot erase _all


// First Define Command for ES data 
capture program drop ES_graph_data
program define ES_graph_data
syntax,  level(real) yti(string) tshifter(real)


qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 


forval i = 1/23 { 
	mat time = (time,`i'-6+1995 + `tshifter')
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci_l es_ci_h time 
svmat es_b 
rename es_b `yti'_es_b
svmat time
rename time `yti'_time
svmat es_ci_l
rename es_ci_l `yti'_es_ci_l
svmat es_ci_h 
rename es_ci_h `yti'_es_ci_h
}

end  
 
// Second Define Command for ES 
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(dash))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Estimate") label(2 "`level'% CI")) ///
	ytitle("`yti'") ///
	note("`note'") ylab("`ylab'")
graph export "`outname'.pdf", replace	

end 


// A - Merge Data for Analysis 
// A.0. Get regional crosswalk read 
use "$xwalk/cw_cty_czone.dta", clear
rename cty_fips pid 
duplicates drop 
tempfile cross
save "`cross'"

// A.0. Get link data 
use "$output_NETS/pr_link_est_countyXindustry.dta", clear 
gen pr_emp = pr_link*total if industry_cd < 13
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)

gen pr_l = tot_pr/tot_emp 
keep pr_l fips
rename  fips pid
rename pr_l pr_link_ind
duplicates drop 
tempfile pr 
save "`pr'" 

use "$output_NETS/pr_link_est_county.dta", clear 
rename  fips pid
tempfile pr2 
save "`pr2'" 

// A.1. Getting the outcome and other county data
use "$data/BEA_CA35/tansfers_clean", replace

rename pid fips_county

merge m:1 fips_county using "$output_NETS/pr_link_est_county_addvars_emp.dta"

keep if _merge == 3
drop _merge

rename fips_county pid

// A.6. Merge in pr link 
merge m:1 pid using "`pr'"
keep if _merg == 3
drop _merge 

merge m:1 pid using "`pr2'"
drop if year == . 
drop if pr_link == . 
drop _merge 
rename pr_link pr_link_all

sum pr_link_al, d 
drop if year == . 

tsset pid year
tsfill, full

// Generating the transfer measure (not per capita in this file)
drop if czone == .

snapshot save

**** taking out veteran transfers
replace trans_tot = trans_tot - trans_vet

foreach Q of var trans_tot trans_unemp trans_SSA trans_medicare  trans_public_med trans_income trans_educ trans_vet  {
gen `Q'_p = `Q' / tot_pop
}

// A.4.  Growth rates. Base year: 1995
gen temp = tot_pop if year == 1995
bysort pid: egen base_pop = max(temp) // this is now total population
drop temp

foreach Q of var trans_tot trans_unemp trans_SSA trans_medicare  trans_public_med trans_income trans_educ  {
gen temp = `Q'_p if year == 1995
bysort pid: egen base_`Q'_p = max(temp) // this is now working age population instead of employment
drop temp
}


foreach Q of var trans_tot trans_unemp trans_SSA trans_medicare  trans_public_med trans_income trans_educ  {
gen `Q'_growth =  (`Q'_p - base_`Q'_p)/base_`Q'_p // (`Q'_p - base_`Q'_p)*1000  
replace `Q'_growth = 0 if `Q'_growth == .
}
foreach Q of var trans_tot trans_unemp trans_SSA trans_medicare  trans_public_med trans_income trans_educ  {
gen `Q'_growth_levels =  (`Q'_p - base_`Q'_p)*1000  
replace `Q'_growth = 0 if `Q'_growth == .
}

// A.5. Define PR Growth Data
** For now, keep only balanced industries . Matt: can you fix it so we don't have to do this? 
bys pid: gen count = _N
keep if count == 23 
drop count 

sum base_pop, d
gen wgt=base_pop/(r(N) * r(mean))

sum pr_link_i $pr_wgt if base_pop > 0, d 
replace pr_link_i = pr_link_i/(r(p75)-r(p25))

sum pr_link_al  $pr_wgt if base_pop > 0, d 
replace pr_link_al = pr_link_al/(r(p75)-r(p25))

// B. Graphs with All PR Link 
cd "$output" 
set more off 

*random cleaning:
rename timepwt48 tpwt48
winsor wgt, p(.01) gen(wgt1)

gen total_transfers_p =  trans_tot_p + trans_vet_p
foreach Q of var trans_tot trans_unemp trans_SSA trans_medicare  trans_public_med trans_income trans_educ  {
gen `Q'_weight =  `Q'_p / total_transfers_p
replace `Q'_p = `Q'_p * 1000
}


est clear 
set more off
foreach Q of var trans_tot trans_unemp trans_income trans_educ trans_SSA trans_medicare  trans_public_med  {

* long diff table outputs

*percents
eststo emp_2_`Q'_p:   reghdfe `Q'_growth c.pr_link_i  $pra_control2 [aw=wgt] if inrange(year,2004,2008) , a( i.year) cl(statefips)
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"	

	
eststo premp_2_`Q'_p:   reghdfe `Q'_growth c.pr_link_i  $pra_control2  [aw=wgt] if inrange(year,1990,1995) , a( i.year) cl(statefips)
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"	

*levels
eststo emp_2_`Q'_l:   reghdfe `Q'_growth_levels c.pr_link_i   $pra_control2 [aw=wgt] if inrange(year,2004,2008) , a( i.year) cl(statefips)
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"	
* percent of transfers from category
sum `Q'_weight [aw=total_transfers_p] if e(sample), d
		estadd local trans_weight = string(r(mean)*100, "%8.1f") + "\%"
* Average transfers from category
sum `Q'_p 	   [aw=total_transfers_p] if e(sample),d 
		estadd local trans_mean = string(r(mean), "%8.1fc") 
	
eststo premp_2_`Q'_l:   reghdfe `Q'_growth_levels c.pr_link_i  $pra_control2  [aw=wgt] if inrange(year,1990,1995) , a( i.year) cl(statefips)
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"	
* percent of transfers from category
sum `Q'_weight [aw=total_transfers_p] if e(sample), d
		estadd local trans_weight = string(r(mean)*100, "%8.1f") + "\%"
* Average transfers from category
sum `Q'_p 	   						  if e(sample),d 
		estadd local trans_mean = string(r(mean), "%8.1fc") 
	
	
}

*trans_tot trans_unemp trans_SSA trans_medical trans_income trans_vet trans_educ

* putting out one table with the after estimates for the paper
esttab emp_2_*_p using "$output/Tables/table_post_transfers.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons  ///
prehead("{\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}\begin{tabular}{l*{7}{c}} \hline\hline &  & Unem- & Income  & Educ-  & Retire- & & Public   \\   & Total & ployment &  Replace-  & ation & ment and & Medicare & Medical   \\   & Transfers & Benefits & ment  & Benefits & Disability & Benefits & Benefits   \\  ") ///
postfoot("") ///

esttab emp_2_*_p  using "$output/Tables/table_post_transfers.tex", keep(pr_link_ind )  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) nonum s() noobs /// 
coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 " pr_link_all "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{8}{l}{\textit{Panel A. Percent change in transfers per capita relative to 1995}} \\") posth("") postfoot("") append sty(tex)

esttab emp_2_*_l using "$output/Tables/table_post_transfers.tex", keep(pr_link_ind)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) nonum s()  /// 
coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 ")  ///
preh("\multicolumn{8}{l}{\textit{Panel B. Dollar change in transfers per capita relative to 1995}} \\") noobs posth("") sfmt(3 1) ///
scalars("trans_weight Sample Proportion of Transfers" "trans_mean Sample Transfers Per Capita" "hasy \hline Year Fixed Effects") label /// 
 prefoot("\hline") ///
postfoot( ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex)	
	* putting out one table with the after estimates for the presentations
	local list_l "emp_2_trans_tot_l emp_2_trans_unemp_l emp_2_trans_income_l emp_2_trans_SSA_l emp_2_trans_medicare_l" 
	local list_p "emp_2_trans_tot_p emp_2_trans_unemp_p emp_2_trans_income_p emp_2_trans_SSA_p emp_2_trans_medicare_p" 
	
	esttab `list_p' using "$output/Tables/table_post_transfers_pres.tex", drop(*) stats() ///
	b(3) se par label  ///
	noobs nogap nomtitle tex replace nocons  ///
	prehead("{\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}\begin{tabular}{l*{5}{c}} \hline\hline &  & Unem- & Income   & Retire- &   \\   & Total & ployment &  Replace-   & ment and & Medicare   \\   & Transfers & Benefits & ment  & Disability & Benefits   \\  ") ///
	postfoot("") ///
	
	esttab `list_p'  using "$output/Tables/table_post_transfers_pres.tex", keep(pr_link_ind )  ///
	cells(b(fmt(3)) se(par) p) mlab(none) coll(none) nonum s() noobs /// 
	coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 " pr_link_all "\hspace{1em}Exposure to Section 936 ") ///
	preh("\multicolumn{6}{l}{\textit{Panel A. Percent change in transfers per capita relative to 1995}} \\") posth("") postfoot("") append sty(tex)

	esttab `list_l' using "$output/Tables/table_post_transfers_pres.tex", keep(pr_link_ind)  ///
	cells(b(fmt(3)) se(par) p) mlab(none) coll(none) nonum s()  /// 
	coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 ")  ///
	preh("\multicolumn{6}{l}{\textit{Panel B. Dollar change in transfers per capita relative to 1995}} \\") noobs posth("") sfmt(3 1) ///
	scalars("trans_weight Sample Proportion of Transfers" "trans_mean Sample Transfers Per Capita" "hasy \hline Year Fixed Effects") label /// 
	prefoot("\hline") ///
	postfoot( ///
	" \hline\hline" ///
	"\end{tabular} }" ) append sty(tex)	

* putting out one table with the pre estimates for the appendix
esttab premp_2_*_p using "$output/Tables/table_pre_transfers.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons  ///
prehead("{\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}\begin{tabular}{l*{7}{c}} \hline\hline &  & Unem- & Income  & Educ-  & Retire- & & Public   \\   & Total & ployment &  Replace-  & ation & ment and & Medicare & Medical   \\   & Transfers & Benefits & ment  & Benefits & Disability & Benefits & Benefits   \\  ") ///
postfoot("") ///

esttab premp_2_*_p  using "$output/Tables/table_pre_transfers.tex", keep(pr_link_ind )  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) nonum s() noobs /// 
coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 " pr_link_all "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{8}{l}{\textit{Panel A. Percent change in transfers per capita relative to 1995}} \\") posth("") postfoot("") append sty(tex)

esttab premp_2_*_l using "$output/Tables/table_pre_transfers.tex", keep(pr_link_ind)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) nonum s()  /// 
coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 ")  ///
preh("\multicolumn{8}{l}{\textit{Panel B. Dollar change in transfers per capita relative to 1995}} \\") noobs posth("") sfmt(3 1) ///
scalars("trans_weight Sample Proportion of Transfers" "trans_mean Sample Transfers Per Capita" "hasy \hline Year Fixed Effects") label /// 
 prefoot("\hline") ///
postfoot( ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex)	

*** some scalars
estimates restore emp_2_trans_unemp_l
do "$analysis/jc_stat_out.ado"
local b = _b[pr_link_ind]
jc_stat_out,  number(`b') name("RESULTunemp") replace(0) deci(0) figs(2)
estimates restore emp_2_trans_income_l
do "$analysis/jc_stat_out.ado"
local b = _b[pr_link_ind]
jc_stat_out,  number(`b') name("RESULTincome") replace(0) deci(0) figs(2)


*****************************************************8
* version with "OTHER" column

snapshot restore 1
**** not taking out veteran transfers
replace trans_tot = trans_tot

gen trans_other = trans_vet + trans_SSA + trans_medicare + trans_public_med

foreach Q of var trans_tot trans_unemp trans_SSA trans_medicare  trans_public_med trans_income trans_educ trans_vet trans_other  {
gen `Q'_p = `Q' / tot_pop
}

// A.4.  Growth rates. Base year: 1995
gen temp = tot_pop if year == 1995
bysort pid: egen base_pop = max(temp)  // this is now total population
drop temp

foreach Q of var trans_tot trans_unemp trans_SSA trans_medicare  trans_public_med trans_income trans_educ trans_vet trans_other   {
gen temp = `Q'_p if year == 1995
bysort pid: egen base_`Q'_p = max(temp)  // this is now total population
drop temp
}


foreach Q of var trans_tot trans_unemp trans_SSA trans_medicare  trans_public_med trans_income trans_educ trans_vet trans_other   {
gen `Q'_growth =  (`Q'_p - base_`Q'_p)/base_`Q'_p // (`Q'_p - base_`Q'_p)*1000  
replace `Q'_growth = 0 if `Q'_growth == .
}
foreach Q of var trans_tot trans_unemp trans_SSA trans_medicare  trans_public_med trans_income trans_educ trans_vet trans_other   {
gen `Q'_growth_levels =  (`Q'_p - base_`Q'_p)*1000  
replace `Q'_growth = 0 if `Q'_growth == .
}

// A.5. Define PR Growth Data
** For now, keep only balanced industries . Matt: can you fix it so we don't have to do this? 
bys pid: gen count = _N
keep if count == 23 
drop count 

sum base_pop, d
gen wgt=base_pop/(r(N) * r(mean))

sum pr_link_i $pr_wgt if base_pop > 0, d 
replace pr_link_i = pr_link_i/(r(p75)-r(p25))

sum pr_link_al  $pr_wgt if base_pop > 0, d 
replace pr_link_al = pr_link_al/(r(p75)-r(p25))

// B. Graphs with All PR Link 
cd "$output" 
set more off 

*random cleaning:
rename timepwt48 tpwt48
winsor wgt, p(.01) gen(wgt1)

gen total_transfers_p =  trans_tot_p 
foreach Q of var trans_tot trans_unemp trans_SSA trans_medicare  trans_public_med trans_income trans_educ trans_other  {
gen `Q'_weight =  `Q'_p / total_transfers_p
replace `Q'_p = `Q'_p * 1000
}


est clear 
set more off
foreach Q of var trans_tot trans_unemp trans_income trans_educ trans_other  {

* long diff table outputs

*percents
eststo emp_2_`Q'_p:   reghdfe `Q'_growth c.pr_link_i  $pra_control2  [aw=wgt] if inrange(year,2004,2008) , a( i.year) cl(statefips)
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"	

	
eststo premp_2_`Q'_p:   reghdfe `Q'_growth c.pr_link_i   $pra_control2 [aw=wgt] if inrange(year,1990,1995) , a( i.year) cl(statefips)
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"	

*levels
eststo emp_2_`Q'_l:   reghdfe `Q'_growth_levels c.pr_link_i   $pra_control2 [aw=wgt] if inrange(year,2004,2008) , a( i.year) cl(statefips)
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"	
* percent of transfers from category
sum `Q'_weight [aw=total_transfers_p] if e(sample), d
		estadd local trans_weight = string(r(mean)*100, "%8.1f") + "\%"
* Average transfers from category
sum `Q'_p 	   [aw=total_transfers_p] if e(sample),d 
		estadd local trans_mean = string(r(mean), "%8.1fc") 
	
eststo premp_2_`Q'_l:   reghdfe `Q'_growth_levels c.pr_link_i  $pra_control2  [aw=wgt] if inrange(year,1990,1995) , a( i.year) cl(statefips)
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"	
* percent of transfers from category
sum `Q'_weight [aw=total_transfers_p] if e(sample), d
		estadd local trans_weight = string(r(mean)*100, "%8.1f") + "\%"
* Average transfers from category
sum `Q'_p 	   						  if e(sample),d 
		estadd local trans_mean = string(r(mean), "%8.1fc") 
	
	
}

*trans_tot trans_unemp trans_SSA trans_medical trans_income trans_vet trans_educ

* putting out one table with the after estimates for the paper
esttab emp_2_*_p using "$output/Tables/table_post_transfers_FULL.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons  ///
prehead("{\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}\begin{tabular}{l*{5}{c}} \hline\hline &  & Unem- & Income  & Educ-  &    \\   & Total & ployment &  Replace-  & ation & Other   \\   & Transfers & Benefits & ment  & Benefits & Benefits   \\  ") ///
postfoot("") ///

esttab emp_2_*_p  using "$output/Tables/table_post_transfers_FULL.tex", keep(pr_link_ind )  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) nonum s() noobs /// 
coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 " pr_link_all "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{6}{l}{\textit{Panel A. Percent change in transfers per capita relative to 1995}} \\") posth("") postfoot("") append sty(tex)

esttab emp_2_*_l using "$output/Tables/table_post_transfers_FULL.tex", keep(pr_link_ind)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) nonum s()  /// 
coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 ")  ///
preh("\multicolumn{6}{l}{\textit{Panel B. Dollar change in transfers per capita relative to 1995}} \\") noobs posth("") sfmt(3 1) ///
scalars("trans_weight Sample Proportion of Transfers" "trans_mean Sample Transfers Per Capita" "hasy \hline Year Fixed Effects") label /// 
 prefoot("\hline") ///
postfoot( ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex)	

* putting out one table with the pre estimates for the appendix
esttab premp_2_*_p using "$output/Tables/table_pre_transfers_FULL.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons  ///
prehead("{\def\sym#1{\ifmmode^{#1}\else\(^{#1}\)\fi}\begin{tabular}{l*{5}{c}} \hline\hline &  & Unem- & Income  & Educ-  &    \\   & Total & ployment &  Replace-  & ation & Other   \\   & Transfers & Benefits & ment  & Benefits & Benefits   \\  ") ///
postfoot("") ///

esttab premp_2_*_p  using "$output/Tables/table_pre_transfers_FULL.tex", keep(pr_link_ind )  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) nonum s() noobs /// 
coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 " pr_link_all "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{6}{l}{\textit{Panel A. Percent change in transfers per capita relative to 1995}} \\") posth("") postfoot("") append sty(tex)

esttab premp_2_*_l using "$output/Tables/table_pre_transfers_FULL.tex", keep(pr_link_ind)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) nonum s()  /// 
coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 ")  ///
preh("\multicolumn{6}{l}{\textit{Panel B. Dollar change in transfers per capita relative to 1995}} \\") noobs posth("") sfmt(3 1) ///
scalars("trans_weight Sample Proportion of Transfers" "trans_mean Sample Transfers Per Capita" "hasy \hline Year Fixed Effects") label /// 
 prefoot("\hline") ///
postfoot( ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex)	
