* 
* Runs Event-Study Analysis robustness table  
* Author: dan and juan carlos
* original date: 06/2016
*
* date of update: 20180129

// 20180129 Changes:(1) added ES_graph_data and exports output data to file: "$qcewdata/ES_graph.dta"
// 					 NOTE: this means es_county_d needs to be run first.
//					(2) took out PR Link wording from graphs

// This version uses base_emp/emp aweights instead of fweights
// NOTE: (1) Updated the IQR normalization to use the weights as well otherwise interpretation is off

// NOTE2: This version has pr_links where some large retail companies have been manually deleted.

clear all
set more off
snapshot erase _all


// First Define Command for ES data 
capture program drop ES_graph_data
program define ES_graph_data
syntax,  level(real) yti(string) tshifter(real)


qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 


forval i = 1/23 { 
	mat time = (time,`i'-6+1995 + `tshifter')
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci_l es_ci_h time 
svmat es_b 
rename es_b `yti'_es_b
svmat time
rename time `yti'_time
svmat es_ci_l
rename es_ci_l `yti'_es_ci_l
svmat es_ci_h 
rename es_ci_h `yti'_es_ci_h
}

end  
 
// Second Define Command for ES 
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(dash))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Estimate") label(2 "`level'% CI")) ///
	ytitle("`yti'") ///
	note("`note'") ylab("`ylab'")
graph export "`outname'.pdf", replace	

end 


// A - Merge Data for Analysis 
// A.0. Get regional crosswalk read 
use "$xwalk/cw_cty_czone.dta", clear
rename cty_fips pid 
duplicates drop 
tempfile cross
save "`cross'"

// A.0. Get link data 
use "$output_NETS/pr_link_est_countyXindustry_md.dta", clear 
gen pr_emp = pr_link*total if industry_cd < 13
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)

gen pr_l = tot_pr/tot_emp 
keep pr_l fips
rename  fips pid
rename pr_l pr_link_ind
duplicates drop 
tempfile pr 
save "`pr'" 

use "$output_NETS/pr_link_est_county_md.dta", clear 
rename  fips pid
tempfile pr2 
save "`pr2'" 

// A.1. Get QCEW data 
use "$qcewdata/extract_qcew_1990_2012.dta", clear


order fips_state fips_county year industry_code
drop if fips_county == 0 & fips_state != 72
drop if fips_state == 72 & fips_county != 0
keep if own_code == 5		// 0-total covered; 1 Federal; 2 State; 3 Local; 4 Intl Govt; 5 Private.
keep fips_state fips_county year industry_code annual_avg_estabs annual_avg_emplvl total_annual_wages
drop if fips_state == 78

// A.2. Label industries		
gen industry_cd = .
replace industry_cd = 0 if industry_code == "10"	// "Total, all industries"
replace industry_cd = 1 if industry_code == "311"
replace industry_cd = 2 if industry_code == "314"
replace industry_cd = 3 if industry_code == "315"
replace industry_cd = 4 if industry_code == "325"	// note: include 3254
replace industry_cd = 5 if industry_code == "3254"
replace industry_cd = 6 if industry_code == "326"
replace industry_cd = 7 if industry_code == "316"
replace industry_cd = 8 if industry_code == "332"
replace industry_cd = 9 if industry_code == "333"
replace industry_cd = 10 if industry_code == "335"
*replace industry_cd = 11 if naic3 == 
replace industry_cd = 12 if industry_code == "31-33"
replace industry_cd = 13 if industry_code == "52" | industry_code == "53"
replace industry_cd = 14 if inlist(industry_code,"54","55","61","62","71","72","81")
replace industry_cd = 15 if inlist(industry_code,"42","44-45")
*replace industry_cd = 16 if industry_code == .

label define l_naic3 0 "Total, all industries" 1 "Food Mfg" 2 "Textile mill products" 3 "Apparel" 4 "Chemicals" 5 "Pharmaceuticals" ///
 6 "Rubber and Plastic" 7 "Leather" 8 "Fabricated metal" 9 "Machinery" 10 "Electrical equip" ///
 11 "Instruments" 12 "Other mfg" 13 "Finance, insurance, real estate" 14 "Services" ///
 15 "Wholesale and retail" 16 "Other non-mfg", replace
label values industry_cd l_naic3

drop if industry_cd == 0
collapse (sum) annual_avg_estabs (sum) annual_avg_emplvl (sum) total_annual_wages, by(fips_state fips_county year industry_cd)

// A.3. Fill in gaps
gen pid = 1000*fips_state+fips_county

// A.6. Merge in pr link 
merge m:1 pid using "`pr'"
keep if _merg == 3
drop _merge 

merge m:1 pid using "`pr2'"
drop if year == . 
drop if pr_link == . 
drop _merge 
rename pr_link pr_link_all

*replace pr_link_all = pr_link_ind

sum pr_link_al, d 
*replace pr_link_al = pr_link_al/(r(p75)-r(p25))
*egen std_pr_link_al = std(pr_link_al)
*replace pr_link_al = std_pr_link_al
** Matt: Check this merge. merge == 1 are weird. What is county == 999 ? 
** Matt the industry codes between both datasets look off. See: 
/*
. tab year if pr_link == . 

       year |      Freq.     Percent        Cum.
------------+-----------------------------------
       1990 |        488        0.81        0.81
       1991 |        499        0.83        1.64
       1992 |        502        0.83        2.47
       1993 |        505        0.84        3.31
       1994 |        510        0.85        4.16
       1995 |        521        0.86        5.02
       1996 |        525        0.87        5.89
       1997 |        534        0.89        6.78
       1998 |        537        0.89        7.67
       1999 |        544        0.90        8.57
       2000 |        549        0.91        9.48
       2001 |      4,543        7.54       17.02
       2002 |      4,545        7.54       24.56
       2003 |      4,534        7.52       32.09
       2004 |      4,580        7.60       39.69
       2005 |      4,584        7.61       47.29
       2006 |      4,603        7.64       54.93
       2007 |      4,532        7.52       62.45
       2008 |      4,575        7.59       70.04
       2009 |      4,549        7.55       77.59
       2010 |      4,521        7.50       85.10
       2011 |      4,490        7.45       92.55
       2012 |      4,492        7.45      100.00
------------+-----------------------------------
      Total |     60,262      100.00
*/
drop if year == . 

collapse (rawsum) annual_avg_estabs (rawsum) annual_avg_emplvl (rawsum) total_annual_wages (mean) pr_link_ind (mean) pr_link_all [fw=total] , by( fips_state pid year industry_cd)


egen pid2 = group(pid indus)

tsset pid2 year
tsfill, full

foreach var of varlist fips_state  {
bysort pid2: egen temp = max(`var')
replace `var' = temp
drop temp
}

foreach var of varlist annual_avg_estabs annual_avg_emplvl total_annual_wages {
replace `var' = 0 if `var' == .
}

// A.4.  Growth rates. Base year: 1995
gen temp = annual_avg_emplvl if year == 1995
bysort pid2: egen base_emp = max(temp)
drop temp

gen emp_growth = (annual_avg_emplvl - base_emp)/base
replace emp_growth = 0 if emp_growth == . 


// Income 
gen inc = total_annual_wages
* /annual_avg_emplvl
gen temp = inc if year == 1995
bysort pid2: egen base_inc = max(temp)
drop temp

gen inc_growth = (inc - base_inc)/base_inc
replace inc_growth = 0 if inc_growth == . 

// Estabs 
gen estab = annual_avg_estabs
gen temp = estab if year == 1995
bysort pid2: egen base_estab = max(temp)
drop temp

gen estab_growth = (estab - base_estab)/base_estab
replace estab_growth = 0 if estab_growth == . 


// A.5. Define PR Growth Data
/*
gen temp = emp_growth if fips_state == 72
bysort year ind: egen emp_growth_PR = max(temp) 
drop temp
drop if emp_growth_PR == . 
*/



** For now, keep only balanced industries . Matt: can you fix it so we don't have to do this? 
bys pid2: gen count = _N
keep if count == 23 
drop count 

sum pr_link_i, d 
replace pr_link_i = pr_link_i/(r(p75)-r(p25))

sum pr_link_al, d 
replace pr_link_al = pr_link_al/(r(p75)-r(p25))

tsset pid2 year 
egen ind_st = group(ind fips_state)

*gen ln_pr = ln(1+pr_link_a) 
*egen std_ln = std(ln_pr)
*winsor std_ln , g(w_std_ln) p(.01) 

// B. Graphs with All PR Link 
cd "$output" 
set more off 
xi i.year|pr_link_i, noomit
drop _IyeaXp*1995


drop if base_emp ==0 

sum base_emp if year == 1995, d
gen wgt=base_emp/(r(N) * r(mean))
winsor wgt, p(.01) gen(wgt_w)

*keep if base_emp > 400  
winsor base_emp , g(w_emp_base) p(.025)
*replace base_emp = w_emp_base
sum base_emp, d 
*drop if base_emp <= r(p1) 
*drop if base_emp >= r(p99)

est clear 
set more off

/*  // outputting the data at this point to check things 
keep pid pr_link_i pr_link_a
duplicates drop
save "$qcewdata/ES_selected_industries_links.dta", replace
*/

eststo emp_1:  reghdfe emp_growth _IyeaX* [aw=wgt]  , a( i.year#i.industr ) cl(fips_state indust )
	estadd local hasiy "Yes"
    estadd local hascty ""

*ES_graph_data , level(95) yti("spec1") tshifter(-.2)
	
eststo emp_2:  reghdfe emp_growth _IyeaX* [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust )
	estadd local hasiy "Yes"
	estadd local hascty "Yes"

	/*
ES_graph_data , level(95) yti("spec2")	tshifter(-0.1)
	*/
ES_graph , level(95) yti("Effect S936 Exposure (IQR)") ///
note("Notes: (Emp{sub:ict}-Emp{sub:ic1995})/Emp{sub:ic1995}={&alpha}{sub:c}+{&gamma}{sub:it}+{&beta}{sub:t}S936 Exposure{sub:c}+{&epsilon}{sub:ict}. SE's clustered by State and Industry.") ///
outname("$output/Graphs/ES_QCEW_dropped_retailers") ylab("-.12(.02).04")
/*
xi i.year|pr_link_a, noomit
drop _IyeaXp*1995	
eststo emp_3:  reghdfe emp_growth _IyeaX* [aw=wgt]  , a( i.pid i.year#i.industr) cl(fips_state indust )
	estadd local hasiy "Yes"
	estadd local hascty "Yes"	
	estadd local hasall "Yes"

*ES_graph_data , level(95) yti("spec3")	tshifter(0.3)	

xi i.year|pr_link_i, noomit
drop _IyeaXp*1995
*/
eststo emp_4:  reghdfe emp_growth _IyeaX* [aw=wgt]  , a( i.pid2 i.year#i.indust ) cl(fips_state indust )
	estadd local hasiy "Yes"
	estadd local hasicty "Yes"

*ES_graph_data , level(95) yti("spec4")	tshifter(0.2)

eststo emp_5:  reghdfe emp_growth _IyeaX* [aw=wgt_w] , a( i.pid i.year#i.indust ) cl(fips_state indust )
	estadd local hasiy "Yes"
	estadd local hascty "Yes"
	estadd local hasww "Yes"	

*ES_graph_data , level(95) yti("spec5")	tshifter(0)

eststo emp_6:  reghdfe emp_growth _IyeaX* if base_emp > 1000 [aw=wgt] , a( i.pid i.year#i.indust ) cl(fips_state indust )
	estadd local hasiy "Yes"
	estadd local hascty "Yes"
	estadd local hasds "Yes"

	/*
ES_graph_data , level(95) yti("spec6")	tshifter(0.1)
* Graph output mirroring figure 3 in http://gabriel-zucman.eu/files/teaching/FuestEtal16.pdf
 twoway (line spec1_es_b spec1_time, lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid)))  ///
		(rcap spec1_es_ci_l spec1_es_ci_h spec1_time,  lcolor(navy) lpattern(dash))  ///
		(line spec2_es_b spec2_time, lcolor(orange) lpattern(solid))      ///
		(rcap spec2_es_ci_l spec2_es_ci_h spec2_time,  lcolor(orange) lpattern(dash))   /// 
		/// (line spec3_es_b spec3_time, lcolor(orange) lpattern(solid))     ///
		/// (rcap spec3_es_ci_l spec3_es_ci_h spec3_time,  lcolor(orange) lpattern(dash))    ///
		/// (line spec4_es_b spec4_time, lcolor(green) lpattern(solid))    ///
		/// (rcap spec4_es_ci_l spec4_es_ci_h spec4_time,  lcolor(green) lpattern(dash))    ///
		(line spec5_es_b spec5_time, lcolor(red) lpattern(solid))   ///
		(rcap spec5_es_ci_l spec5_es_ci_h spec5_time,  lcolor(red) lpattern(dash))   ///
		(line spec6_es_b spec6_time, lcolor(purple) lpattern(solid))    ///
		(rcap spec6_es_ci_l spec6_es_ci_h spec6_time,  lcolor(purple) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "(1)") ///
	label(3 "(2)")   ///
	label(5 "(5)")  ///
	label(7 "(6)")  ///
	r(1) order( - "Specification:" 1 3 5 7)) ///
	ytitle("Percent Employment Growth") 
graph export "$output/fig-7-robust.pdf", replace	
*/
	
esttab emp* using "$output/Tables/table_ES_QCEW_aw_md.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons ///
postfoot("\multicolumn{1}{l}{\textbf{Exposure to Section 936}}") nonum


esttab emp*   using "$output/Tables/table_ES_QCEW_aw_md.tex",  preh("") ///
 b(3) se par mlab(none) coll(none) s() noobs nogap /// 
coeflabel(_IyeaXpr__1990 "\hspace{1em}X 1990 " /// 
_IyeaXpr__1991 "\hspace{1em}X 1991 " /// 
_IyeaXpr__1992 "\hspace{1em}X 1992 " /// 
_IyeaXpr__1993 "\hspace{1em}X 1993 " /// 
_IyeaXpr__1994 "\hspace{1em}X 1994 " /// 
_IyeaXpr__1996 "\hline \hspace{1em}X 1996 " /// 
_IyeaXpr__1997 "\hspace{1em}X 1997 " /// 
_IyeaXpr__1998 "\hspace{1em}X 1998 " /// 
_IyeaXpr__1999 "\hspace{1em}X 1999 " /// 
_IyeaXpr__2000 "\hspace{1em}X 2000 " /// 
_IyeaXpr__2001 "\hspace{1em}X 2001 " /// 
_IyeaXpr__2002 "\hspace{1em}X 2002 " /// 
_IyeaXpr__2003 "\hspace{1em}X 2003 " /// 
_IyeaXpr__2004 "\hspace{1em}X 2004 " /// 
_IyeaXpr__2005 "\hspace{1em}X 2005 " /// 
_IyeaXpr__2006 "\hspace{1em}X 2006 " /// 
_IyeaXpr__2007 "\hspace{1em}X 2007 " /// 
_IyeaXpr__2008 "\hspace{1em}X 2008 " /// 
_IyeaXpr__2009 "\hspace{1em}X 2009 " /// 
_IyeaXpr__2010 "\hspace{1em}X 2010 " /// 
_IyeaXpr__2011 "\hspace{1em}X 2011 " /// 
_IyeaXpr__2012 "\hspace{1em}X 2012 " )   scalars(  "hasiy Industry-by-Year Fixed Effects" "hascty County Fixed Effects" ///
 "hasicty Industry-by-County Fixed Effects" ///
"hasww Winsorized Weights" "hasds Drops Small County-Industries (<1000)" ) label /// 
 prefoot("\hline") ///
postfoot( ///
"  \\ \hline\hline" ///
"\end{tabular} }" ) append sty(tex)

/*
eststo emp_3:  reghdfe estab_growth _IyeaX* [fw=base_emp]  , a( i.year i.pid) cl(fips_state indust )

ES_graph , level(95) yti("Effect of IQR of All PR Link") ///
note("Notes: (Emp{sub:ict}-Emp{sub:ic1995})/Emp{sub:ic1995}={&alpha}{sub:ic}+{&gamma}{sub:t}+{&beta}{sub:t}PR Link{sub:c}+{&epsilon}{sub:ict}. SE's clustered by State.") ///
outname("temp") ylab("-.12(.02).04")

eststo emp_3:  reghdfe inc_growth _IyeaX* [fw=base_emp]  , a( i.pid i.year#i.industr) cl(fips_state indust )

ES_graph , level(95) yti("Effect of IQR of All PR Link") ///
note("Notes: (Emp{sub:ict}-Emp{sub:ic1995})/Emp{sub:ic1995}={&alpha}{sub:ic}+{&gamma}{sub:t}+{&beta}{sub:t}PR Link{sub:c}+{&epsilon}{sub:ict}. SE's clustered by State.") ///
outname("temp") ylab("-.12(.02).04")
*/

**** Main Post/Pre table 
est clear 
gen temp = pr_link_i
replace pr_link_i = pr_link_a

eststo emp_1:   reghdfe emp_growth  c.pr_link_i    [aw=wgt] if inrange(year,2004,2008) , a( i.year i.industr) cl(fips_state indust )
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	
eststo premp_1:   reghdfe emp_growth  c.pr_link_i    [aw=wgt] if inrange(year,1990,1995) , a( i.year i.industr) cl(fips_state indust )
	estadd local hasy "Yes"
	estadd local hasind "Yes"

eststo est_1: reghdfe estab_growth c.pr_link_i  [aw=wgt] if inrange(year,2004,2008) , a(  i.year i.industr) cl(fips_state indust )
	estadd local hasy "Yes"
	estadd local hasind "Yes"

eststo prest_1:  reghdfe estab_growth  c.pr_link_i    [aw=wgt] if inrange(year,1990,1995) , a(  i.year i.industr) cl(fips_state indust )
	estadd local hasy "Yes"
	estadd local hasind "Yes"

eststo inc_1:  reghdfe inc_growth c.pr_link_i  [aw=wgt] if inrange(year,2004,2008) , a(  i.year i.industr) cl(fips_state indust )
	estadd local hasy "Yes"
	estadd local hasind "Yes"

eststo princ_1:	reghdfe inc_growth  c.pr_link_i   [aw=wgt] if inrange(year,1990,1995) , a(  i.year i.industr) cl(fips_state indust )
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	
replace pr_link_i = temp 
	
eststo emp_2:   reghdfe emp_growth  c.pr_link_i   [aw=wgt] if inrange(year,2004,2008) , a( i.year i.industr) cl(fips_state indust )
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"	
	
eststo premp_2:   reghdfe emp_growth  c.pr_link_i   [aw=wgt] if inrange(year,1990,1995) , a( i.year i.industr) cl(fips_state indust )
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"	
	
eststo est_2: reghdfe estab_growth c.pr_link_i  [aw=wgt] if inrange(year,2004,2008) , a(  i.year i.industr) cl(fips_state indust )
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"	
	
eststo prest_2:  reghdfe estab_growth  c.pr_link_i  [aw=wgt] if inrange(year,1990,1995) , a(  i.year i.industr) cl(fips_state indust )
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"	
	
eststo inc_2:  reghdfe inc_growth c.pr_link_i [aw=wgt] if inrange(year,2004,2008) , a(  i.year i.industr) cl(fips_state indust )
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"	
	
eststo princ_2:	reghdfe inc_growth  c.pr_link_i [aw=wgt] if inrange(year,1990,1995) , a(  i.year i.industr) cl(fips_state indust )
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"		


esttab emp*  est* inc* using "$output/Tables/table_pre_post_QCEW_aw_md.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons ///
postfoot(" & \multicolumn{2}{l}{Employment Growth}& \multicolumn{2}{l}{Establishment Growth} &\multicolumn{2}{l}{Income Growth} \\")

esttab emp*  est* inc*  using "$output/Tables/table_pre_post_QCEW_aw_md.tex", keep(pr_link_ind )  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s() noobs /// 
coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 " pr_link_all "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{6}{l}{\textbf{Years: 2004-2008}}\\") postfoot("\hline") append sty(tex)

esttab premp*  prest* princ*  using "$output/Tables/table_pre_post_QCEW_aw_md.tex", keep(pr_link_ind)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s()  /// 
coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{6}{l}{\textbf{Years: 1990-1995}}\\") noobs scalars(  "hasy Year Fixed Effects" "hasind Industry Fixed Effects" ///
"hassel Exposure from S936 Industries") label /// 
 prefoot("\hline") ///
postfoot( ///
"  \\ \hline\hline" ///
"\end{tabular} }" ) append sty(tex)

