* 
* Runs Event-Study Analysis robustness table  
* Author: dan and juan carlos
* original date: 06/2016
*
* date of update: 20180129

// 20180129 Changes:(1) added ES_graph_data and exports output data to file: "$qcewdata/ES_graph.dta"
// 					 NOTE: this means es_county_d needs to be run first.
//					(2) took out PR Link wording from graphs

// 20180219 changes: (1) changed all files to naic3 versions and took out industry classifications

// This version uses base_emp/emp aweights instead of fweights
// NOTE: (1) Updated the IQR normalization to use the weights as well otherwise interpretation is off

clear all
set more off
snapshot erase _all



// First Define Command for ES data 
capture program drop ES_graph_data
program define ES_graph_data
syntax,  level(real) yti(string) tshifter(real)


qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 


forval i = 1/23 { 
	mat time = (time,`i'-6+1995 + `tshifter')
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci_l es_ci_h time 
svmat es_b 
rename es_b `yti'_es_b
svmat time
rename time `yti'_time
svmat es_ci_l
rename es_ci_l `yti'_es_ci_l
svmat es_ci_h 
rename es_ci_h `yti'_es_ci_h
}

end  
 
// Second Define Command for ES 
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(dash))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Estimate") label(2 "`level'% CI")) ///
	ytitle("`yti'") ///
	note("`note'") ylab("`ylab'")
graph export "`outname'.pdf", replace	

end 


// A - Merge Data for Analysis 
// A.0. Get regional crosswalk read 
use "$xwalk/cw_cty_czone.dta", clear
rename cty_fips pid 
duplicates drop 
tempfile cross
save "`cross'"

// A.0. Get link data 
use "$output_NETS/pr_link_est_countyXindustry_naic3.dta", clear 
gen pr_emp = pr_link*total if naic3 < 340 & naic3 > 309
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)

gen pr_l = tot_pr/tot_emp 
keep pr_l fips
rename  fips pid
rename pr_l pr_link_ind
duplicates drop 
tempfile pr 
save "`pr'" 

use "$output_NETS/pr_link_est_county_naic3.dta", clear 
rename  fips pid
tempfile pr2 
save "`pr2'" 

// A.1. Get QCEW data 
use "$qcewdata/extract_qcew_1990_2012_naic3.dta", clear
keep if strlen(industry_code) == 3
drop if industry_code == "101" | industry_code == "102"

order fips_state fips_county year industry_code
drop if fips_county == 0 & fips_state != 72
drop if fips_state == 72 & fips_county != 0
keep if own_code == 5		// 0-total covered; 1 Federal; 2 State; 3 Local; 4 Intl Govt; 5 Private.
keep fips_state fips_county year industry_code annual_avg_estabs annual_avg_emplvl total_annual_wages
drop if fips_state == 78

/*
// A.2. Label industries		
gen industry_cd = .
replace industry_cd = 0 if industry_code == "10"	// "Total, all industries"
replace industry_cd = 1 if industry_code == "311"
replace industry_cd = 2 if industry_code == "314"
replace industry_cd = 3 if industry_code == "315"
replace industry_cd = 4 if industry_code == "325"	// note: include 3254
replace industry_cd = 5 if industry_code == "3254"
replace industry_cd = 6 if industry_code == "326"
replace industry_cd = 7 if industry_code == "316"
replace industry_cd = 8 if industry_code == "332"
replace industry_cd = 9 if industry_code == "333"
replace industry_cd = 10 if industry_code == "335"
*replace industry_cd = 11 if naic3 == 
replace industry_cd = 12 if industry_code == "31-33"
replace industry_cd = 13 if industry_code == "52" | industry_code == "53"
replace industry_cd = 14 if inlist(industry_code,"54","55","61","62","71","72","81")
replace industry_cd = 15 if inlist(industry_code,"42","44-45")
*replace industry_cd = 16 if industry_code == .

label define l_naic3 0 "Total, all industries" 1 "Food Mfg" 2 "Textile mill products" 3 "Apparel" 4 "Chemicals" 5 "Pharmaceuticals" ///
 6 "Rubber and Plastic" 7 "Leather" 8 "Fabricated metal" 9 "Machinery" 10 "Electrical equip" ///
 11 "Instruments" 12 "Other mfg" 13 "Finance, insurance, real estate" 14 "Services" ///
 15 "Wholesale and retail" 16 "Other non-mfg", replace
label values industry_cd l_naic3

drop if industry_cd == 0
*/
collapse (sum) annual_avg_estabs (sum) annual_avg_emplvl (sum) total_annual_wages, by(fips_state fips_county year industry_code)

// A.3. Fill in gaps
gen pid = 1000*fips_state+fips_county

// A.6. Merge in pr link 
merge m:1 pid using "`pr'"
keep if _merg == 3
drop _merge 

merge m:1 pid using "`pr2'"
drop if year == . 
drop if pr_link == . 
drop _merge 
rename pr_link pr_link_all

*replace pr_link_all = pr_link_ind

sum pr_link_al, d 

drop if year == . 

*collapse (rawsum) annual_avg_estabs (rawsum) annual_avg_emplvl (rawsum) total_annual_wages (mean) pr_link_ind (mean) pr_link_all [fw=total] , by( fips_state pid year industry_code) // Dan: this isn't actually collapsing anything

egen pid2 = group(pid indus)

tsset pid2 year
tsfill, full

foreach var of varlist fips_state  {
bysort pid2: egen temp = max(`var')
replace `var' = temp
drop temp
}

foreach var of varlist annual_avg_estabs annual_avg_emplvl total_annual_wages {
replace `var' = 0 if `var' == .
}

// A.4.  Growth rates. Base year: 1995
gen temp = annual_avg_emplvl if year == 1995
bysort pid2: egen base_emp = max(temp)
drop temp

gen emp_growth = (annual_avg_emplvl - base_emp)/base
replace emp_growth = 0 if emp_growth == . 


// Income 
gen inc = total_annual_wages
* /annual_avg_emplvl
gen temp = inc if year == 1995
bysort pid2: egen base_inc = max(temp)
drop temp

gen inc_growth = (inc - base_inc)/base_inc
replace inc_growth = 0 if inc_growth == . 

// Estabs 
gen estab = annual_avg_estabs
gen temp = estab if year == 1995
bysort pid2: egen base_estab = max(temp)
drop temp

gen estab_growth = (estab - base_estab)/base_estab
replace estab_growth = 0 if estab_growth == . 

** For now, keep only balanced industries . Matt: can you fix it so we don't have to do this? 
bys pid2: gen count = _N
keep if count == 23 
drop count 

drop if base_emp ==0 

sum base_emp if year == 1995, d
gen wgt=base_emp/(r(N) * r(mean))
winsor wgt, p(.01) gen(wgt_w)

sum pr_link_i, d 
replace pr_link_i = pr_link_i/(r(p75)-r(p25))

sum pr_link_al, d 
replace pr_link_al = pr_link_al/(r(p75)-r(p25))

tsset pid2 year 
egen ind_st = group(ind fips_state)

*keep if base_emp > 400  
winsor base_emp , g(w_emp_base) p(.025)
*replace base_emp = w_emp_base
sum base_emp, d 
*drop if base_emp <= r(p1) 
*drop if base_emp >= r(p99)
destring industry_code, replace

**** Merge in share of firm size dist 
merge m:1 pid year using "$output/CBP_firm_size_dist"
drop if _merge == 2
drop _merge 

foreach var of varlist n1_4-n250p { 
	sum `var', d
	replace `var' = `var'/(r(p75)-r(p25))
	
	gen temp = `var' if year == 1995
	bysort pid2: egen base_`var' = max(temp)
	drop temp

	gen `var'_growth = (`var' - base_`var')/base_`var'
	replace `var'_growth = 0 if `var'_growth == . 
}

// B. Graphs with All PR Link 
cd "$output" 
set more off 
xi i.year|pr_link_i, noomit
drop _IyeaXp*1995


/*
xi i.year|pr_link_i ///
	i.year|n1_4_g   ///
	i.year|n5_9_g   ///
	i.year|n10_19_g   ///
	i.year|n20_49_g   ///
	i.year|n50_99_g   ///
	i.year|n100_249_g   ///
	i.year|n250p_g   ///	
      	 , noomit
	 
drop _IyeaXp*1995 _IyeaXn*1995 	 


eststo emp_1:  reghdfe emp_growth _IyeaXp* _IyeaXn* ///
  [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust )

ES_graph_data , level(95) yti("spec0") tshifter(0)
  
foreach var of varlist spec0_es_b spec0_es_ci_l spec0_es_ci_h   {
	replace `var' = . if _n == 24
}
  
 twoway (scatter spec0_es_b spec0_time, lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid)))  ///
		(rcap spec0_es_ci_l spec0_es_ci_h spec0_time,  lcolor(navy) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Exposure to Pharma") ///
	label(3 "Exposure to S936")   ///
	r(1) order( - "Specification:" 1 3 5 7)) ///
	ytitle("Percent Employment Growth") ylab("-.12(.02).04")
	graph export "$output/ES_QCEW_fsd_growthXyear_naics3.pdf", replace	  
	
capture: drop spec0*
	
/*
xi i.year|pr_link_i ///
	i.year|n1_4   ///
	i.year|n5_9   ///
	i.year|n10_19   ///
	i.year|n20_49   ///
	i.year|n50_99   ///
	i.year|n100_249   ///
	i.year|n250p   ///	
      	 , noomit
*/
	 
xi i.year|pr_link_i ///
	i.year|n10_19   ///
	i.year|n20_49   ///
	i.year|n50_99   ///
	i.year|n100_249   ///
	i.year|n250p   ///	
      	 , noomit	 
	 
drop _IyeaXp*1995 _IyeaXn*1995 	 


eststo emp_1:  reghdfe emp_growth _IyeaXp* _IyeaXn* ///
  [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust )

ES_graph_data , level(95) yti("spec0") tshifter(0)
  
foreach var of varlist spec0_es_b spec0_es_ci_l spec0_es_ci_h   {
	replace `var' = . if _n == 24
}
  
 twoway (scatter spec0_es_b spec0_time, lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid)))  ///
		(rcap spec0_es_ci_l spec0_es_ci_h spec0_time,  lcolor(navy) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Exposure to Pharma") ///
	label(3 "Exposure to S936")   ///
	r(1) order( - "Specification:" 1 3 5 7)) ///
	ytitle("Percent Employment Growth") ylab("-.12(.02).04")	
	graph export "$output/ES_QCEW_fsd_Xyear_naics3.pdf", replace	  

*/

**********************

***** growth in firms size distribution 
capture: drop spec1* spec2*

eststo emp_1:  reghdfe emp_growth _IyeaXp* ///
  [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust )
  
ES_graph_data , level(95) yti("spec1") tshifter(-.2)

eststo emp_1:  reghdfe emp_growth _IyeaXp* ///
    n5_9_growth n10_19_growth n20_49_growth n50_99_growth n100_249_growth n250p_growth   ///
  [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust )
  

ES_graph_data , level(95) yti("spec2") tshifter(.2)
  
foreach var of varlist spec2_es_b spec2_es_ci_l spec2_es_ci_h   {
	replace `var' = . if _n == 24
}
  
 twoway (scatter spec1_es_b spec1_time, lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid)))  ///
		(rcap spec1_es_ci_l spec1_es_ci_h spec1_time,  lcolor(navy) lpattern(dash)) ///
		(scatter spec2_es_b spec2_time, color(orange) lpattern(solid))      ///
		(rcap spec2_es_ci_l spec2_es_ci_h spec2_time,  lcolor(orange) lpattern(dash))   /// 
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Exposure to Pharma") ///
	label(3 "Exposure to S936")   ///
	r(1) order( - "Specification:" 1 3 5 7)) ///
	ytitle("Percent Employment Growth") ylab("-.12(.02).04")
graph export "$output/ES_QCEW_fsd_growth_both_naics3.pdf", replace	  

***** level in firms size distribution 
capture: drop spec1* spec2*

eststo emp_1:  reghdfe emp_growth _IyeaXp* ///
  [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust )
  
ES_graph_data , level(95) yti("spec1") tshifter(-.2)

eststo emp_1:  reghdfe emp_growth _IyeaXp* ///
    n5_9 n10_19 n20_49 n50_99 n100_249 n250p   ///
  [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust )
  

ES_graph_data , level(95) yti("spec2") tshifter(.2)
  
foreach var of varlist spec2_es_b spec2_es_ci_l spec2_es_ci_h   {
	replace `var' = . if _n == 24
}
  
 twoway (scatter spec1_es_b spec1_time, lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid)))  ///
		(rcap spec1_es_ci_l spec1_es_ci_h spec1_time,  lcolor(navy) lpattern(dash)) ///
		(scatter spec2_es_b spec2_time, color(orange) lpattern(solid))      ///
		(rcap spec2_es_ci_l spec2_es_ci_h spec2_time,  lcolor(orange) lpattern(dash))   /// 
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Exposure to Pharma") ///
	label(3 "Exposure to S936")   ///
	r(1) order( - "Specification:" 1 3 5 7)) ///
	ytitle("Percent Employment Growth") ylab("-.12(.02).04")
graph export "$output/ES_QCEW_fsd_both_naics3.pdf", replace	  

