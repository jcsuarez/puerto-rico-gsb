* Runs regression using county-year observations on the ADH-style outcome
* includes extra county level covariates
* date: 2-28-2018

clear all
set more off
snapshot erase _all


// First Define Command for ES data 
capture program drop ES_graph_data
program define ES_graph_data
syntax,  level(real) yti(string) tshifter(real)


qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 


forval i = 1/23 { 
	mat time = (time,`i'-6+1995 + `tshifter')
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci_l es_ci_h time 
svmat es_b 
rename es_b `yti'_es_b
svmat time
rename time `yti'_time
svmat es_ci_l
rename es_ci_l `yti'_es_ci_l
svmat es_ci_h 
rename es_ci_h `yti'_es_ci_h
}

end  
 
// Second Define Command for ES 
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(dash))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Estimate") label(2 "`level'% CI")) ///
	ytitle("`yti'") ///
	note("`note'") ylab("`ylab'")
graph export "`outname'.pdf", replace	

end 


// A - Merge Data for Analysis 
// A.0. Get regional crosswalk read 
use "$xwalk/cw_cty_czone.dta", clear
rename cty_fips pid 
duplicates drop 
tempfile cross
save "`cross'"

// A.0. Get link data 
use "$output_NETS/pr_link_est_countyXindustry_md.dta", clear 
gen pr_emp = pr_link*total if industry_cd < 13
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)

gen pr_l = tot_pr/tot_emp 
keep pr_l fips
rename  fips pid
rename pr_l pr_link_ind
duplicates drop 
tempfile pr 
save "`pr'" 

use "$output_NETS/pr_link_est_county_md.dta", clear 
rename  fips pid
tempfile pr2 
save "`pr2'" 

// A.1. Getting the outcome and other county data
use "$additional/county population estimates/county_pop_ests.dta", replace
merge 1:1 pid year using "$additional/county population estimates/county_manu_emp_ests.dta"
drop _merge

replace manu_emp = 0 if manu_emp == .
drop if working_pop == .

rename pid fips_county

merge m:1 fips_county using "$output_NETS/pr_link_est_county_addvars_emp.dta"

keep if _merge == 3
drop _merge

rename fips_county pid

// A.6. Merge in pr link 
merge m:1 pid using "`pr'"
keep if _merg == 3
drop _merge 

merge m:1 pid using "`pr2'"
drop if year == . 
drop if pr_link == . 
drop _merge 
rename pr_link pr_link_all

sum pr_link_al, d 
drop if year == . 

tsset pid year
tsfill, full

// Generating the ADH measure
gen ADH = manu_emp / working_pop
drop if czone == .

// A.4.  Growth rates. Base year: 1995
gen temp = working_pop if year == 1995
bysort pid: egen base_pop = max(temp) // this is now working age population instead of employment
drop temp
gen temp = ADH if year == 1995
bysort pid: egen base_ADH = max(temp) // this is now working age population instead of employment
drop temp

gen ADH_growth = (ADH - base_ADH)*100 // Difference of the levels file!
replace ADH_growth = 0 if ADH_growth == . 

// A.5. Define PR Growth Data
** For now, keep only balanced industries . Matt: can you fix it so we don't have to do this? 
bys pid: gen count = _N
keep if count == 23 
drop count 

sum base_pop, d
gen wgt=base_pop/(r(N) * r(mean))

sum pr_link_i, d 
replace pr_link_i = pr_link_i/(r(p75)-r(p25))

sum pr_link_al, d 
replace pr_link_al = pr_link_al/(r(p75)-r(p25))


// B. Graphs with All PR Link 
cd "$output" 
set more off 
xi i.year|pr_link_i, noomit
drop _IyeaXp*1995

*random cleaning:
rename timepwt48 tpwt48

est clear 
set more off

eststo emp_1:  reghdfe ADH_growth _IyeaX* [aw=wgt]  , a( i.year ) cl(statefips )
	estadd local hasy "Yes"
    estadd local hascty ""

*ES_graph_data , level(95) yti("spec1") tshifter(-.2)
	
eststo emp_2:  reghdfe ADH_growth _IyeaX* [aw=wgt]  , a(i.pid i.year ) cl(statefips )
	estadd local hasy "Yes"
	estadd local hascty "Yes"

	/*
ES_graph_data , level(95) yti("spec2")	tshifter(-0.1)
	*/
ES_graph , level(95) yti("Effect S936 Exposure (IQR)") ///
note("Notes: Epop{sub:ict}-Epop{sub:ic1995}={&alpha}{sub:c}+{&gamma}{sub:it}+{&beta}{sub:t}S936 Exposure{sub:c}+{&epsilon}{sub:ict}. SE's clustered by State and Industry.") ///
outname("$output/Graphs/ADH_county_coefs_levels") ylab("-3.5(1).5")
/*
xi i.year|pr_link_a, noomit
drop _IyeaXp*1995	
eststo emp_3:  reghdfe ADH_growth _IyeaX* [aw=wgt]  , a( i.pid i.year) cl(statefips)
	estadd local hasy "Yes"
	estadd local hascty "Yes"	
	estadd local hasall "Yes"

*ES_graph_data , level(95) yti("spec3")	tshifter(0.3)	

xi i.year|pr_link_i, noomit
drop _IyeaXp*1995
*/
eststo emp_4:  reghdfe ADH_growth _IyeaX* [aw=wgt]  , a(i.pid i.year#i.statefips) cl(statefips )
	estadd local hasy "Yes"
	estadd local hasicty "Yes"

*ES_graph_data , level(95) yti("spec4")	tshifter(0.2)
winsor wgt, p(.01) gen(wgt1)

eststo emp_5:  reghdfe ADH_growth _IyeaX* [aw=wgt1] , a( i.pid i.year ) cl(statefips )
	estadd local hasy "Yes"
	estadd local hascty "Yes"
	estadd local hasww "Yes"	

*ES_graph_data , level(95) yti("spec5")	tshifter(0)

eststo emp_6:  reghdfe ADH_growth _IyeaX* if base_pop > 1000 [aw=wgt] , a( i.pid i.year) cl(statefips)
	estadd local hasy "Yes"
	estadd local hascty "Yes"
	estadd local hasds "Yes"

	/*
ES_graph_data , level(95) yti("spec6")	tshifter(0.1)
* Graph output mirroring figure 3 in http://gabriel-zucman.eu/files/teaching/FuestEtal16.pdf
 twoway (line spec1_es_b spec1_time, lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid)))  ///
		(rcap spec1_es_ci_l spec1_es_ci_h spec1_time,  lcolor(navy) lpattern(dash))  ///
		(line spec2_es_b spec2_time, lcolor(orange) lpattern(solid))      ///
		(rcap spec2_es_ci_l spec2_es_ci_h spec2_time,  lcolor(orange) lpattern(dash))   /// 
		/// (line spec3_es_b spec3_time, lcolor(orange) lpattern(solid))     ///
		/// (rcap spec3_es_ci_l spec3_es_ci_h spec3_time,  lcolor(orange) lpattern(dash))    ///
		/// (line spec4_es_b spec4_time, lcolor(green) lpattern(solid))    ///
		/// (rcap spec4_es_ci_l spec4_es_ci_h spec4_time,  lcolor(green) lpattern(dash))    ///
		(line spec5_es_b spec5_time, lcolor(red) lpattern(solid))   ///
		(rcap spec5_es_ci_l spec5_es_ci_h spec5_time,  lcolor(red) lpattern(dash))   ///
		(line spec6_es_b spec6_time, lcolor(purple) lpattern(solid))    ///
		(rcap spec6_es_ci_l spec6_es_ci_h spec6_time,  lcolor(purple) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "(1)") ///
	label(3 "(2)")   ///
	label(5 "(5)")  ///
	label(7 "(6)")  ///
	r(1) order( - "Specification:" 1 3 5 7)) ///
	ytitle("Percent Employment Growth") 
graph export "$output/fig-7-robust.pdf", replace	
*/
	
esttab emp* using "$output/Tables/table_county_ADH_levels.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons ///
postfoot("\multicolumn{1}{l}{\textbf{Exposure to Section 936}}") nonum


esttab emp*   using "$output/Tables/table_county_ADH_levels.tex",  preh("") ///
 b(3) se par mlab(none) coll(none) s() noobs nogap /// 
coeflabel(_IyeaXpr__1990 "\hspace{1em}X 1990 " /// 
_IyeaXpr__1991 "\hspace{1em}X 1991 " /// 
_IyeaXpr__1992 "\hspace{1em}X 1992 " /// 
_IyeaXpr__1993 "\hspace{1em}X 1993 " /// 
_IyeaXpr__1994 "\hspace{1em}X 1994 " /// 
_IyeaXpr__1996 "\hline \hspace{1em}X 1996 " /// 
_IyeaXpr__1997 "\hspace{1em}X 1997 " /// 
_IyeaXpr__1998 "\hspace{1em}X 1998 " /// 
_IyeaXpr__1999 "\hspace{1em}X 1999 " /// 
_IyeaXpr__2000 "\hspace{1em}X 2000 " /// 
_IyeaXpr__2001 "\hspace{1em}X 2001 " /// 
_IyeaXpr__2002 "\hspace{1em}X 2002 " /// 
_IyeaXpr__2003 "\hspace{1em}X 2003 " /// 
_IyeaXpr__2004 "\hspace{1em}X 2004 " /// 
_IyeaXpr__2005 "\hspace{1em}X 2005 " /// 
_IyeaXpr__2006 "\hspace{1em}X 2006 " /// 
_IyeaXpr__2007 "\hspace{1em}X 2007 " /// 
_IyeaXpr__2008 "\hspace{1em}X 2008 " /// 
_IyeaXpr__2009 "\hspace{1em}X 2009 " /// 
_IyeaXpr__2010 "\hspace{1em}X 2010 " /// 
_IyeaXpr__2011 "\hspace{1em}X 2011 " /// 
_IyeaXpr__2012 "\hspace{1em}X 2012 " )   scalars(  "hasy Year Fixed Effects" "hascty County Fixed Effects" ///
"hasicty Year-by-State Fixed Effects" ///
"hasww Winsorized Weights" "hasds Drops Small County-Industries (<1000)" ) label /// 
 prefoot("\hline") ///
postfoot( ///
"  \\ \hline\hline" ///
"\end{tabular} }" ) append sty(tex)
