* 
* Runs Event-Study Analysis robustness table  
* Author: dan and juan carlos
* original date: 06/2016
*
* date of update: 20180129

// 20180129 Changes:(1) added ES_graph_data and exports output data to file: "$qcewdata/ES_graph.dta"
// 					 NOTE: this means es_county_d needs to be run first.
//					(2) took out PR Link wording from graphs

// 20180219 changes: (1) changed all files to naic3 versions and took out industry classifications

// This version uses base_emp/emp aweights instead of fweights
// NOTE: (1) Updated the IQR normalization to use the weights as well otherwise interpretation is off

clear all
set more off
snapshot erase _all



// First Define Command for ES data 
capture program drop ES_graph_data
program define ES_graph_data
syntax,  level(real) yti(string) tshifter(real)


qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 


forval i = 1/23 { 
	mat time = (time,`i'-6+1995 + `tshifter')
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci_l es_ci_h time 
svmat es_b 
rename es_b `yti'_es_b
svmat time
rename time `yti'_time
svmat es_ci_l
rename es_ci_l `yti'_es_ci_l
svmat es_ci_h 
rename es_ci_h `yti'_es_ci_h
}

end  
 
// Second Define Command for ES 
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(dash))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Estimate") label(2 "`level'% CI")) ///
	ytitle("`yti'") ///
	note("`note'") ylab("`ylab'")
graph export "`outname'.pdf", replace	

end 


// A - Merge Data for Analysis 
// A.0. Get regional crosswalk read 
use "$xwalk/cw_cty_czone.dta", clear
rename cty_fips pid 
duplicates drop 
tempfile cross
save "`cross'"

// A.0. Get link data 
use "$output_NETS/pr_link_est_countyXindustry_naic3.dta", clear 
gen pr_emp = pr_link*total if naic3 < 340 & naic3 > 309
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)

gen pr_l = tot_pr/tot_emp 
keep pr_l fips
rename  fips pid
rename pr_l pr_link_ind
duplicates drop 
tempfile pr 
save "`pr'" 

use "$output_NETS/pr_link_est_county_naic3.dta", clear 
rename  fips pid
tempfile pr2 
save "`pr2'" 

// A.1. Get QCEW data 
use "$qcewdata/extract_qcew_1990_2012_naic3.dta", clear
keep if strlen(industry_code) == 3
drop if industry_code == "101" | industry_code == "102"

order fips_state fips_county year industry_code
drop if fips_county == 0 & fips_state != 72
drop if fips_state == 72 & fips_county != 0
keep if own_code == 5		// 0-total covered; 1 Federal; 2 State; 3 Local; 4 Intl Govt; 5 Private.
keep fips_state fips_county year industry_code annual_avg_estabs annual_avg_emplvl total_annual_wages
drop if fips_state == 78

/*
// A.2. Label industries		
gen industry_cd = .
replace industry_cd = 0 if industry_code == "10"	// "Total, all industries"
replace industry_cd = 1 if industry_code == "311"
replace industry_cd = 2 if industry_code == "314"
replace industry_cd = 3 if industry_code == "315"
replace industry_cd = 4 if industry_code == "325"	// note: include 3254
replace industry_cd = 5 if industry_code == "3254"
replace industry_cd = 6 if industry_code == "326"
replace industry_cd = 7 if industry_code == "316"
replace industry_cd = 8 if industry_code == "332"
replace industry_cd = 9 if industry_code == "333"
replace industry_cd = 10 if industry_code == "335"
*replace industry_cd = 11 if naic3 == 
replace industry_cd = 12 if industry_code == "31-33"
replace industry_cd = 13 if industry_code == "52" | industry_code == "53"
replace industry_cd = 14 if inlist(industry_code,"54","55","61","62","71","72","81")
replace industry_cd = 15 if inlist(industry_code,"42","44-45")
*replace industry_cd = 16 if industry_code == .

label define l_naic3 0 "Total, all industries" 1 "Food Mfg" 2 "Textile mill products" 3 "Apparel" 4 "Chemicals" 5 "Pharmaceuticals" ///
 6 "Rubber and Plastic" 7 "Leather" 8 "Fabricated metal" 9 "Machinery" 10 "Electrical equip" ///
 11 "Instruments" 12 "Other mfg" 13 "Finance, insurance, real estate" 14 "Services" ///
 15 "Wholesale and retail" 16 "Other non-mfg", replace
label values industry_cd l_naic3

drop if industry_cd == 0
*/
collapse (sum) annual_avg_estabs (sum) annual_avg_emplvl (sum) total_annual_wages, by(fips_state fips_county year industry_code)

// A.3. Fill in gaps
gen pid = 1000*fips_state+fips_county

// A.6. Merge in pr link 
merge m:1 pid using "`pr'"
keep if _merg == 3
drop _merge 

merge m:1 pid using "`pr2'"
drop if year == . 
drop if pr_link == . 
drop _merge 
rename pr_link pr_link_all

*replace pr_link_all = pr_link_ind

sum pr_link_al, d 

drop if year == . 

*collapse (rawsum) annual_avg_estabs (rawsum) annual_avg_emplvl (rawsum) total_annual_wages (mean) pr_link_ind (mean) pr_link_all [fw=total] , by( fips_state pid year industry_code) // Dan: this isn't actually collapsing anything

egen pid2 = group(pid indus)

tsset pid2 year
tsfill, full

foreach var of varlist fips_state  {
bysort pid2: egen temp = max(`var')
replace `var' = temp
drop temp
}

foreach var of varlist annual_avg_estabs annual_avg_emplvl total_annual_wages {
replace `var' = 0 if `var' == .
}

// A.4.  Growth rates. Base year: 1995
gen temp = annual_avg_emplvl if year == 1995
bysort pid2: egen base_emp = max(temp)
drop temp

gen emp_growth = (annual_avg_emplvl - base_emp)/base
replace emp_growth = 0 if emp_growth == . 


// Income 
gen inc = total_annual_wages
* /annual_avg_emplvl
gen temp = inc if year == 1995
bysort pid2: egen base_inc = max(temp)
drop temp

gen inc_growth = (inc - base_inc)/base_inc
replace inc_growth = 0 if inc_growth == . 

// Estabs 
gen estab = annual_avg_estabs
gen temp = estab if year == 1995
bysort pid2: egen base_estab = max(temp)
drop temp

gen estab_growth = (estab - base_estab)/base_estab
replace estab_growth = 0 if estab_growth == . 

** For now, keep only balanced industries . Matt: can you fix it so we don't have to do this? 
bys pid2: gen count = _N
keep if count == 23 
drop count 

drop if base_emp ==0 

sum base_emp if year == 1995, d
gen wgt=base_emp/(r(N) * r(mean))
winsor wgt, p(.01) gen(wgt_w)

sum pr_link_i $pr_wgt if base_emp > 0, d 
replace pr_link_i = pr_link_i/(r(p75)-r(p25))

sum pr_link_al  $pr_wgt if base_emp > 0, d 
replace pr_link_al = pr_link_al/(r(p75)-r(p25))

tsset pid2 year 
egen ind_st = group(ind fips_state)

// B. Graphs with All PR Link 
cd "$output" 
set more off 
xi i.year|pr_link_i  $pra_control, noomit
drop _IyeaXp*1995

*keep if base_emp > 400  
winsor base_emp , g(w_emp_base) p(.025)
*replace base_emp = w_emp_base
sum base_emp, d 
*drop if base_emp <= r(p1) 

snapshot save 
gen naics = industry_code
destring industry_code, replace
merge m:1 naics using "$additional/tradable definition/trade_cats_3dNAICS"
drop _merge

* running the same regression 5 times. each time has a different variable as the notable interaction
* notable interactions will be put out in a table. All results in all 5 columns actually come from a single regression

drop if wgt == .
drop if has_yes == . 

expand 5
bys pid year industry: gen sector = _n
gen sector_share = has_yes if sector ==1 
replace sector_share = has_no if sector ==2 
replace sector_share = has_oth if sector ==3 
replace sector_share = has_con if sector ==4 
replace sector_share = 1 if sector ==5

replace wgt = wgt*sector_share
drop if wgt == 0 

egen tot_emp = sum(annual_avg_emplvl * (sector == 5) * (year == 1995))
gen wgt_emp = sector_share * annual_avg_emplvl * (year == 1995)
gen temp = wgt_emp / tot_emp
bys sector: egen percent_emp = sum(temp)

** Dan: Make table with 5 columns on LR effect before and after and add stats for weigted average to table notes: 
foreach i of numlist 5 1 2 3 4 {
eststo reg_pre_`i': reghdfe emp_growth c.pr_link_ind if inrange(year,1990,1994) & sector == `i' [aw=wgt], a(i.sector#i.year i.year#i.industry) cl(fips_state  )
	estadd local hassy "Yes"
		qui sum percent_emp if e(sample),d 
		estadd scalar base_emp_share = r(mean)
		qui sum emp_growth [aw=wgt] if inrange(year,2004,2008) & sector == `i',d 
		estadd scalar emp_growth_mean = r(mean)
	
eststo reg_post_`i': reghdfe emp_growth c.pr_link_ind if inrange(year,2004,2008) & sector == `i' [aw=wgt], a(i.sector#i.year i.year#i.industry)  cl(fips_state  )
}

esttab reg_pre_? using "$output/Tables/table_mian_sufi_long_diff.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons posthead("") ///
prefoot("") ///
postfoot("") ///

esttab reg_post_?  using "$output/Tables/table_mian_sufi_long_diff.tex", keep(pr_link_ind )  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s() noobs nonum  /// 
coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 " pr_link_all "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{1}{l}{\textbf{Years: 2004-2008}} & All Industries & Tradable & Non-Tradable & Other & Construction \\") postfoot("\hline") append sty(tex)

esttab reg_pre_? using "$output/Tables/table_mian_sufi_long_diff.tex", keep(pr_link_ind)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s() nonum  /// 
coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{1}{l}{\textbf{Years: 1990-1995}} & All Industries & Tradable & Non-Tradable & Other & Construction \\") noobs sfmt(3 3 1) ///
scalars("emp_growth_mean Weighted Average Employment Growth" "base_emp_share Share of Total Employment in 1995"  "hassy \hline Industry by Year Fixed Effects") label /// 
 prefoot("\hline") ///
postfoot( ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex)	

** Add these stats to table notes:
 
* Mean employment by sector (scalars put into table footnotes)
xi i.sector, noomit
mean _Isector_* if sector != 5 [aw=wgt]
mat r = r(table) 
mat r = r[1,1...]

do "$analysis/jc_stat_out.ado"

eststo reg_post_`i': reghdfe emp_growth i.sector#c.pr_link_ind if inrange(year,2004,2008) [aw=wgt], a(i.sector#i.year i.year#i.industry) 
* Linear combination
lincom _b[1b.sector#c.pr_link_ind]*r[1,1]  ///
+_b[2.sector#c.pr_link_ind]*r[1,2] ///
+_b[3.sector#c.pr_link_ind]*r[1,3] ///
+_b[4.sector#c.pr_link_ind]*r[1,4]

local b1 = r(estimate)
local b2 = r(se)
local b3 = r(p)

jc_stat_out,  number(`b1') name("combMEAN") replace(0) deci(3) figs(5)
jc_stat_out,  number(`b2') name("combSE") replace(0) deci(3) figs(5)
jc_stat_out,  number(`b3') name("combP") replace(0) deci(3) figs(5)

* Test showing FE gets the average right 
nlcom (_b[1b.sector#c.pr_link_ind]*r[1,1]  ///
+_b[2.sector#c.pr_link_ind]*r[1,2] ///
+_b[3.sector#c.pr_link_ind]*r[1,3] ///
+_b[4.sector#c.pr_link_ind]*r[1,4] ///
-_b[5.sector#c.pr_link_ind])/_b[5.sector#c.pr_link_ind]

xi i.year|pr_link_i, noomit
drop _IyeaXp*1995
/// As robustness, weight by interaction of fraction of sector and weight 
eststo emp_1:  reghdfe emp_growth c._IyeaX*  if sector ==5 [aw=wgt]  , a(i.pid i.year#i.industr ) cl(fips_state indust )
ES_graph_data , level(95) yti("spec1")	tshifter(-0.2)
	estadd local hasiy "Yes"
	estadd local hascty "Yes"

eststo emp_2:  reghdfe emp_growth c._IyeaX*  if sector ==1 [aw=wgt]   , a(i.pid i.year#i.industr ) cl(fips_state indust )
ES_graph_data , level(95) yti("spec2")	tshifter(-0.1)
	estadd local hasiy "Yes"
	estadd local hascty "Yes"
	
eststo emp_3:  reghdfe emp_growth c._IyeaX* if sector ==2 [aw=wgt]    , a(i.pid i.year#i.industr ) cl(fips_state indust )
ES_graph_data , level(95) yti("spec3")	tshifter(-0)
	estadd local hasiy "Yes"
	estadd local hascty "Yes"
	
eststo emp_4:  reghdfe emp_growth c._IyeaX* if sector ==3 [aw=wgt]   , a(i.pid i.year#i.industr ) cl(fips_state indust )
ES_graph_data , level(95) yti("spec4")	tshifter(0.1)
	estadd local hasiy "Yes"
	estadd local hascty "Yes"
	
eststo emp_5:  reghdfe emp_growth c._IyeaX* if sector ==4 [aw=wgt]   , a(i.pid i.year#i.industr ) cl(fips_state indust )
ES_graph_data , level(95) yti("spec5")	tshifter(0.2)
	estadd local hasiy "Yes"
	estadd local hascty "Yes"
	
esttab emp* using "$output/Tables/table_HE_Mian_Sufi_v3.tex", drop(*) stats() ///
b(3) se par label   ///
noobs nogap nomtitle tex replace nocons nonum ///
postfoot("\multicolumn{1}{l}{\textbf{Exposure to Section 936}}") nonum posthead("")


esttab emp* using "$output/Tables/table_HE_Mian_Sufi_v3.tex",  preh("")  ///
 b(3) se par nogap mlab("Average Effect" "Tradable" "Non-Tradable" "Other" "Construction" ) coll(none) s() noobs  ///
coeflabel(_IyeaXpr__1990 "\hspace{1em}X 1990 " /// 
_IyeaXpr__1991 "\hspace{1em}X 1991 " /// 
_IyeaXpr__1992 "\hspace{1em}X 1992 " /// 
_IyeaXpr__1993 "\hspace{1em}X 1993 " /// 
_IyeaXpr__1994 "\hspace{1em}X 1994 " /// 
_IyeaXpr__1996 "\hline \hspace{1em}X 1996 " /// 
_IyeaXpr__1997 "\hspace{1em}X 1997 " /// 
_IyeaXpr__1998 "\hspace{1em}X 1998 " /// 
_IyeaXpr__1999 "\hspace{1em}X 1999 " /// 
_IyeaXpr__2000 "\hspace{1em}X 2000 " /// 
_IyeaXpr__2001 "\hspace{1em}X 2001 " /// 
_IyeaXpr__2002 "\hspace{1em}X 2002 " /// 
_IyeaXpr__2003 "\hspace{1em}X 2003 " /// 
_IyeaXpr__2004 "\hspace{1em}X 2004 " /// 
_IyeaXpr__2005 "\hspace{1em}X 2005 " /// 
_IyeaXpr__2006 "\hspace{1em}X 2006 " /// 
_IyeaXpr__2007 "\hspace{1em}X 2007 " /// 
_IyeaXpr__2008 "\hspace{1em}X 2008 " /// 
_IyeaXpr__2009 "\hspace{1em}X 2009 " /// 
_IyeaXpr__2010 "\hspace{1em}X 2010 " /// 
_IyeaXpr__2011 "\hspace{1em}X 2011 " /// 
_IyeaXpr__2012 "\hspace{1em}X 2012 " )   scalars(  "hasiy Industry-by-Year Fixed Effects" "hascty County Fixed Effects" ) label /// 
 prefoot("\hline") ///
postfoot( ///
"  \hline\hline" ///
"\end{tabular} }" ) append sty(tex)
	
twoway 		(scatter spec1_es_b spec1_time, msymbol(0) connect(line) lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid)  ))  ///
		(rcap spec1_es_ci_l spec1_es_ci_h spec1_time,  lcolor(navy) lpattern(solid))  ///
		(scatter spec2_es_b spec2_time, msymbol(D) connect(line) lcolor(orange) mcolor(orange) lpattern(dash))      ///
		(rcap spec2_es_ci_l spec2_es_ci_h spec2_time,  lcolor(orange) lpattern(dash))   /// 
		(scatter spec3_es_b spec3_time, msymbol(T) connect(line) lcolor(red) mcolor(red) lpattern(longdash))     ///
		(rcap spec3_es_ci_l spec3_es_ci_h spec3_time,  lcolor(red) lpattern(longdash))    ///
		(scatter spec4_es_b spec4_time, msymbol(S) connect(line) lcolor(green) mcolor(green) lpattern(shortdash))    ///
		(rcap spec4_es_ci_l spec4_es_ci_h spec4_time,  lcolor(green) lpattern(shortdash))    ///
		(scatter spec5_es_b spec5_time, msymbol(+) connect(line) lcolor(black) mcolor(black) lpattern(dash_dot))   ///
		(rcap spec5_es_ci_l spec5_es_ci_h spec5_time,  lcolor(black) lpattern(dash_dot))   ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "All") ///
	label(3 "Tradable")   ///
	label(5 "Non-Tradable")  ///
	label(7 "Other")  ///
	label(9 "Construction")  ///
	r(2) order( - "Sector:" 1 3 5 9 7)) ///
	ytitle("Percent Employment Growth") ylab(-.2(.05).05)	
graph export "$output/Graphs/het_effects_tab_Mian_Sufi_v3.pdf", replace		
	
twoway 		(scatter spec1_es_b spec1_time, msymbol(0) connect(line) lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid)  ))  ///
		/// (rcap spec1_es_ci_l spec1_es_ci_h spec1_time,  lcolor(navy) lpattern(solid))  ///
		(scatter spec2_es_b spec2_time, msymbol(D) connect(line) lcolor(orange) mcolor(orange) lpattern(dash))      ///
		/// (rcap spec2_es_ci_l spec2_es_ci_h spec2_time,  lcolor(orange) lpattern(dash))   /// 
		(scatter spec3_es_b spec3_time, msymbol(T) connect(line) lcolor(red) mcolor(red) lpattern(longdash))     ///
		/// (rcap spec3_es_ci_l spec3_es_ci_h spec3_time,  lcolor(red) lpattern(longdash))    ///
		(scatter spec4_es_b spec4_time, msymbol(S) connect(line) lcolor(green) mcolor(green) lpattern(shortdash))    ///
		/// (rcap spec4_es_ci_l spec4_es_ci_h spec4_time,  lcolor(green) lpattern(shortdash))    ///
		(scatter spec5_es_b spec5_time, msymbol(+) connect(line) lcolor(black) mcolor(black) lpattern(dash_dot))   ///
		/// (rcap spec5_es_ci_l spec5_es_ci_h spec5_time,  lcolor(black) lpattern(dash_dot))   ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "All") ///
	label(2 "Tradable")   ///
	label(3 "Non-Tradable")  ///
	label(4 "Other")  ///
	label(5 "Construction")  ///
	r(2) order( - "Sector:" 1 2 3 5 4)) ///
	ytitle("Percent Employment Growth") ylab(-.2(.05).05)		
graph export "$output/Graphs/het_effects_tab_Mian_Sufi_noCI_v3.pdf", replace	
