* 
* Gets event study scalars for discussion of implied spillovers 
* Author: dan
* original date: 4-3-2018

// A - Merge Data for Analysis 
// A.0. Get regional crosswalk read 
use "$xwalk/cw_cty_czone.dta", clear
rename cty_fips pid 
duplicates drop 
tempfile cross
save "`cross'"

// A.0. Get link data 
use "$output_NETS/pr_link_est_countyXindustry_emp.dta", clear 
gen pr_emp = pr_link*total if industry_cd < 13
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)

gen pr_l = tot_pr/tot_emp 
bys fips: egen tot_emp1 = total(emp95)
bys fips: egen tot_emp1pr = total(pr_emp95)
gen pr_link_emp = tot_emp1pr / tot_emp1 

keep pr_l fips pr_link_emp tot_emp1pr
rename  fips pid
rename pr_l pr_link_ind
duplicates drop 
tempfile pr 
save "`pr'" 

use "$output_NETS/pr_link_est_county_d.dta", clear 
rename  fips pid
tempfile pr2 
save "`pr2'" 

// A.1. Get QCEW data 
use "$qcewdata/extract_qcew_1990_2012_naic3.dta", clear
keep if strlen(industry_code) == 3
drop if industry_code == "101" | industry_code == "102"

order fips_state fips_county year industry_code
drop if fips_county == 0 & fips_state != 72
drop if fips_state == 72 & fips_county != 0
keep if own_code == 5		// 0-total covered; 1 Federal; 2 State; 3 Local; 4 Intl Govt; 5 Private.
keep fips_state fips_county year industry_code annual_avg_estabs annual_avg_emplvl total_annual_wages
drop if fips_state == 78

collapse (sum) annual_avg_estabs (sum) annual_avg_emplvl (sum) total_annual_wages, by(fips_state fips_county year)

// A.3. Fill in gaps
gen pid = 1000*fips_state+fips_county

// A.6. Merge in pr link 
merge m:1 pid using "`pr'"
keep if _merg == 3
drop _merge 

merge m:1 pid using "`pr2'"
drop if year == . 
drop if pr_link == . 
drop _merge 
rename pr_link pr_link_all

*replace pr_link_all = pr_link_ind

sum pr_link_al, d 

drop if year == . 

// scalar \RESULT30
do "$analysis/jc_stat_out.ado"

sum annual_avg_emplvl if year == 1995,d
local b = r(mean)
jc_stat_out,  number(`b') name("RESULTXXX") replace(0) deci(0) figs(6)

estimates use "$output/table_pre_post_QCEW_county_emp"

local beta = _b[pr_link_ind]
local b1 =  -`beta' * `b' 
di `b1'
*jc_stat_out,  number(`b1') name("RESULTXXXI") replace(0) deci(0) figs(5)

// employment in S936 businesses
/*
	5,233 (actually 1,962 is 25-75 and the original was 25-95) is \RESULT32a
	487 is \RESULT32b
	3.16 is \RESULT32c with all results coming from Sec63scalars.do
	9.3 is \RESULT4 from estimates save "$output/comparison_emp", replace (see above)
*/

sum pr_link_emp if year == 1995,d
di `b' * (r(p75) - r(p25))    // average employment times the change in percent employment from S936 at 25-75
local b2 = `b' * (r(p75) - r(p25))
*jc_stat_out,  number(`b2') name("RESULTXXXIIa") replace(0) deci(0) figs(5)
di `b2'

estimates use "$output/comparison_emp"
local beta1 = -_b[_IyeaXPR_2012]
local b3 =  `b2' * `beta1'
*jc_stat_out,  number(`b3') name("RESULTXXXIIb") replace(0) deci(0) figs(3)
di `b3'

local multiplier = (`b1' - `b3') / `b3'
*jc_stat_out,  number(`multiplier') name("RESULTXXXIIc") replace(0) deci(2) figs(4)
di `multiplier'

/* JC edit on 4/3/2018 
*/
** get mean employment 
sum annual_avg_emplvl if year == 1995,d
local b = r(mean)
di `b'

** Get mean loss of jobs from mean exposure (excluding zeros)
estimates use "$output/table_pre_post_QCEW_county_emp"
** Get pr_link to be as in regression 
sum pr_link_ind  if year == 1995 & annual_avg_emplvl > 0 , d
capture: drop pr2
gen pr2 = pr_link_ind /(r(p75)-r(p25))
** Get mean effect with positive pr link 
sum pr2 if year == 1995, d
di r(mean)
** Mean effect on employment growth 
local beta = -r(mean)*_b[pr_link_ind]
di `beta'


** This is the effect reported in the paper:
* local beta = .069
* The difference is that the normalization depends on industries that have positive base_emp
* The mean effect is slighlty differnt in this case but gives similar estimates 6.4 instead of 7. 
* This does not change the 

** Mean effect on number of jobs
local b1 = `beta'*`b'
di `b1'

jc_stat_out,  number(`b1') name("RESULTXXXI") replace(0) deci(0) figs(5)

** Get firm level effect
estimates use "$output/comparison_emp_long4"
local beta1 = -_b[PR]
di  `beta1'

** average employment at 936 firms
sum tot_emp1pr if year == 1995 
di r(mean) 
local b2 = r(mean)
jc_stat_out,  number(`b2') name("RESULTXXXIIa") replace(0) deci(0) figs(5)
** layoffs at 936 firms 
local b3 =  `beta1'*r(mean)
di  `b3'

jc_stat_out,  number(`b3') name("RESULTXXXIIb") replace(0) deci(0) figs(3)

local multiplier = (`b1' - `b3') / `b3'
di `multiplier'
jc_stat_out,  number(`multiplier') name("RESULTXXXIIc") replace(0) deci(2) figs(4)













