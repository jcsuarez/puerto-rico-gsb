* 
* Runs Event-Study Analysis robustness table  
* Author: dan and juan carlos
* original date: 06/2016
*
* date of update: 20180129

// 20180129 Changes:(1) added ES_graph_data and exports output data to file: "$qcewdata/ES_graph.dta"
// 					 NOTE: this means es_county_d needs to be run first.
//					(2) took out PR Link wording from graphs

// This version uses base_emp/emp aweights instead of fweights
// NOTE: (1) Updated the IQR normalization to use the weights as well otherwise interpretation is off

// Population update: 5-13-2018 
//			Added county level graphs for population regressions

clear all
set more off
snapshot erase _all

// First Define Command for ES data 
capture program drop ES_graph_data
program define ES_graph_data
syntax,  level(real) yti(string) tshifter(real)


qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 


forval i = 1/23 { 
	mat time = (time,`i'-6+1995 + `tshifter')
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci_l es_ci_h time 
svmat es_b 
rename es_b `yti'_es_b
svmat time
rename time `yti'_time
svmat es_ci_l
rename es_ci_l `yti'_es_ci_l
svmat es_ci_h 
rename es_ci_h `yti'_es_ci_h
}

end  
 
// Define Command for Event Study Graph
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(dash))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Estimate") label(2 "`level'% CI")) ///
	ytitle("`yti'") ///
	note("`note'") ylab("`ylab'")
graph export "`outname'.pdf", replace	

end 


// A - Merge Data for Analysis 
// A.0. Get link data 
use "$output_NETS/pr_link_est_countyXindustry_d.dta", clear 
gen pr_emp = pr_link*total if industry_cd < 13
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)

gen pr_l = tot_pr/tot_emp 
keep pr_l fips
rename  fips pid
rename pr_l pr_link_ind
duplicates drop 
tempfile pr 
save "`pr'" 

use "$output_NETS/pr_link_est_county_d.dta", clear 
rename  fips pid
tempfile pr2 
save "`pr2'" 

// A.1. Get QCEW data 
use "$qcewdata/extract_qcew_1990_2012_naic3.dta", clear
keep if strlen(industry_code) == 3
drop if industry_code == "101" | industry_code == "102"

order fips_state fips_county year industry_code
drop if fips_county == 0 & fips_state != 72
drop if fips_state == 72 & fips_county != 0
keep if own_code == 5		// 0-total covered; 1 Federal; 2 State; 3 Local; 4 Intl Govt; 5 Private.
keep fips_state fips_county year industry_code annual_avg_estabs annual_avg_emplvl total_annual_wages
drop if fips_state == 78

collapse (sum) annual_avg_estabs (sum) annual_avg_emplvl (sum) total_annual_wages, by(fips_state fips_county year industry_code)

// A.3. Fill in gaps
gen pid = 1000*fips_state+fips_county

// A.6. Merge in pr link 
merge m:1 pid using "`pr'"
keep if _merg == 3
drop _merge 

merge m:1 pid using "`pr2'"
drop if year == . 
drop if pr_link == . 
drop _merge 
rename pr_link pr_link_all

*replace pr_link_all = pr_link_ind

sum pr_link_al, d 

drop if year == . 
merge m:1 pid year using "$additional/county population estimates/county_pop_ests.dta"
keep if _merge == 3
drop _merge

*collapse (rawsum) annual_avg_estabs (rawsum) annual_avg_emplvl (rawsum) total_annual_wages (mean) pr_link_ind (mean) pr_link_all [fw=total] , by( fips_state pid year industry_code) // Dan: this isn't actually collapsing anything

*** SAVING DATA TO BE USED WITH CONSPUMA AND CZONES
snapshot save 

egen pid2 = group(pid indus)

tsset pid2 year
tsfill, full

foreach var of varlist fips_state  {
bysort pid2: egen temp = max(`var')
replace `var' = temp
drop temp
}

foreach var of varlist annual_avg_estabs annual_avg_emplvl total_annual_wages {
replace `var' = 0 if `var' == .
replace `var' = `var' / tot_pop
}

// A.4.  Growth rates. Base year: 1995
gen temp = annual_avg_emplvl if year == 1995
bysort pid2: egen base_emp1 = max(temp)
drop temp

gen temp = annual_avg_emplvl * tot_pop if year == 1995
bysort pid2: egen base_pop = max(temp)
drop temp

gen emp_growth = (annual_avg_emplvl - base_emp1)/base_emp1
replace emp_growth = 0 if emp_growth == . 
// Income 
gen inc = total_annual_wages
* /annual_avg_emplvl
gen temp = inc if year == 1995
bysort pid2: egen base_inc = max(temp)
drop temp

gen inc_growth = (inc - base_inc)/base_inc
replace inc_growth = 0 if inc_growth == . 


* balancing industries
bys pid2: gen count = _N
keep if count == 23 
drop count 

drop if base_emp ==0 
drop if base_pop ==0 

sum base_pop if year == 1995, d
gen wgt=base_pop/(r(N) * r(mean))
winsor wgt, p(.01) gen(wgt_w)

sum pr_link_i $pr_wgt if base_emp > 0, d 
replace pr_link_i = pr_link_i/(r(p75)-r(p25))

sum pr_link_al  $pr_wgt if base_emp > 0, d 
replace pr_link_al = pr_link_al/(r(p75)-r(p25))

tsset pid2 year 
egen ind_st = group(ind fips_state)

// B. Graphs with All PR Link 
cd "$output" 
set more off 
xi i.year|pr_link_i  $pra_control, noomit
drop _IyeaXp*1995

*keep if base_emp > 400  
winsor base_emp , g(w_emp_base) p(.025)
*replace base_emp = w_emp_base
sum base_emp, d 
*drop if base_emp <= r(p1) 
*drop if base_emp >= r(p99)
destring industry_code, replace
est clear 
set more off

**** Main Post/Pre table 
est clear 

reghdfe emp_growth _IyeaX* [aw=wgt]  , a(i.industry i.year) cl(fips_state industry)

ES_graph , level(95) yti("Effect S936 Exposure (IQR)") ///
note("Notes: (Emp{sub:ict}-Emp{sub:ic1995})/Emp{sub:ic1995}={&alpha}{sub:c}+{&gamma}{sub:it}+{&beta}{sub:t}S936 Exposure{sub:c}+{&epsilon}{sub:ict}. SE's clustered by State and Industry.") ///
outname("$output/Graphs/ES_per_capita") ylab("-.08(.02).04")


reghdfe emp_growth _IyeaX* [aw=wgt]  , a(i.pid i.industry i.year) cl(fips_state industry)

ES_graph , level(95) yti("Effect S936 Exposure (IQR)") ///
note("Notes: (Emp{sub:ict}-Emp{sub:ic1995})/Emp{sub:ic1995}={&alpha}{sub:c}+{&gamma}{sub:it}+{&beta}{sub:t}S936 Exposure{sub:c}+{&epsilon}{sub:ict}. SE's clustered by State and Industry.") ///
outname("$output/Graphs/ES_per_capita_county") ylab("-.08(.02).04")

/*
reghdfe emp_growth _IyeaX* [aw=wgt]  , a(i.pid i.industry#i.year) cl(fips_state industry)

ES_graph , level(95) yti("Effect S936 Exposure (IQR)") ///
note("Notes: (Emp{sub:ict}-Emp{sub:ic1995})/Emp{sub:ic1995}={&alpha}{sub:c}+{&gamma}{sub:it}+{&beta}{sub:t}S936 Exposure{sub:c}+{&epsilon}{sub:ict}. SE's clustered by State and Industry.") ///
outname("$output/Graphs/ES_per_capita_county_i-y") ylab("-.08(.02).04")
*/

** outputting data to be used in joint graph
preserve
ES_graph_data , level(95) yti("spec2_PC") tshifter(0)

keep spec2_PC_time spec2*
keep if spec2_PC_time != . 
rename spec2_PC_time year

merge 1:1 year using "$qcewdata/ES_graph_md.dta"
drop _merge
save "$qcewdata/ES_graph_md.dta", replace
restore

/*
collapse (rawsum) annual_avg_estabs (rawsum) annual_avg_emplvl tot_pop (rawsum) total_annual_wages (mean) pr_link_ind (mean) pr_link_all [fw=total] , by(pid year)
gen temp = annual_avg_emplvl if year == 1995
bysort pid: egen base_emp1 = max(temp)
drop temp

gen emp_growth = (annual_avg_emplvl - base_emp1)/base_emp1
replace emp_growth = 0 if emp_growth == . 

xi i.year|pr_link_i, noomit
drop _IyeaXp*1995
eststo test: reghdfe emp_growth _IyeaX* [aw=tot_pop]  , a(i.year) cl(pid)
estimates save 	"$output/table_QCEW_PC_v2_spec1", replace 	

*/

eststo test: reghdfe emp_growth _IyeaX* [aw=wgt]  , a(i.year) cl(fips_state  industry)
estimates save 	"$output/table_QCEW_PC_spec1", replace 	
/*
** regressions
eststo county_emp:   reghdfe emp_growth  c.pr_link_i $pra_control2    [aw=wgt] if inrange(year,2004,2008) , a( i.year) cl(fips_state)
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"	
	
eststo county_premp:   reghdfe emp_growth  c.pr_link_i  $pra_control2   [aw=wgt] if inrange(year,1990,1995) , a( i.year) cl(fips_state)
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"	
		
eststo county_inc:  reghdfe inc_growth c.pr_link_i $pra_control2  [aw=wgt] if inrange(year,2004,2008) , a(  i.year) cl(fips_state)
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"	
	
eststo county_princ:	reghdfe inc_growth  c.pr_link_i $pra_control2  [aw=wgt] if inrange(year,1990,1995) , a(  i.year) cl(fips_state)
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"	
	
	
	
	
	
	
	
*** saving and doing conspuma
use "$xwalk/consp2cty9-22-13.dta", clear
gen pid = state_fips * 1000 + county_fips
duplicates drop 
tempfile cross_cons
save "`cross_cons'"

snapshot restore 1		
merge m:1 pid using "`cross_cons'"
keep if _merge == 3
drop _merge 

collapse (rawsum) annual_avg_estabs (rawsum) annual_avg_emplvl tot_pop (rawsum) total_annual_wages (mean) pr_link_ind (mean) pr_link_all [fw=total] , by(fips_state conspuma year industry_code) 

rename conspuma pid
egen pid2 = group(pid indus)
drop if pid2 == .

tsset pid2 year
tsfill, full

foreach var of varlist fips_state  {
bysort pid2: egen temp = max(`var')
replace `var' = temp
drop temp
}

foreach var of varlist annual_avg_estabs annual_avg_emplvl total_annual_wages {
replace `var' = 0 if `var' == .
replace `var' = `var' / tot_pop
}

// A.4.  Growth rates. Base year: 1995
gen temp = annual_avg_emplvl if year == 1995
bysort pid2: egen base_emp1 = max(temp)
drop temp

gen temp = annual_avg_emplvl * tot_pop if year == 1995
bysort pid2: egen base_pop = max(temp)
drop temp

gen emp_growth = (annual_avg_emplvl - base_emp1)/base_emp1
replace emp_growth = 0 if emp_growth == .
// Income 
gen inc = total_annual_wages
gen temp = inc if year == 1995
bysort pid2: egen base_inc = max(temp)
drop temp

gen inc_growth = (inc - base_inc)/base_inc
replace inc_growth = 0 if inc_growth == . 

bys pid2: gen count = _N
keep if count == 23 
drop count 

drop if base_emp ==0 

sum base_pop if year == 1995, d
gen wgt=base_pop/(r(N) * r(mean))
winsor wgt, p(.01) gen(wgt_w)

sum pr_link_i $pr_wgt if base_emp > 0, d 
replace pr_link_i = pr_link_i/(r(p75)-r(p25))

sum pr_link_al  $pr_wgt if base_emp > 0, d 
replace pr_link_al = pr_link_al/(r(p75)-r(p25))

tsset pid2 year 
egen ind_st = group(ind fips_state)

// B. Graphs with All PR Link 
cd "$output" 
set more off 
xi i.year|pr_link_i  $pra_control, noomit
drop _IyeaXp*1995

winsor base_emp , g(w_emp_base) p(.025)
sum base_emp, d 
destring industry_code, replace
	
**** Main Post/Pre table 	
eststo conspuma_emp:   reghdfe emp_growth  c.pr_link_i $pra_control2   [aw=wgt] if inrange(year,2004,2008) , a( i.year) cl(fips_state )
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"	
	
eststo conspuma_premp:   reghdfe emp_growth  c.pr_link_i $pra_control2   [aw=wgt] if inrange(year,1990,1995) , a( i.year) cl(fips_state )
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"	
		
eststo conspuma_inc:  reghdfe inc_growth c.pr_link_i $pra_control2  [aw=wgt] if inrange(year,2004,2008) , a(  i.year) cl(fips_state )
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"	
	
eststo conspuma_princ:	reghdfe inc_growth  c.pr_link_i $pra_control2  [aw=wgt] if inrange(year,1990,1995) , a(  i.year) cl(fips_state )
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"	
	
	
	
	
	
*** saving and doing czone
// A.0. Get regional crosswalk read 
use "$xwalk/cw_cty_czone.dta", clear
rename cty_fips pid 
duplicates drop 
tempfile cross_cz
save "`cross_cz'"

snapshot restore 1		
drop if year == . 
merge m:1 pid using "`cross_cz'"
keep if _merge == 3
drop _merge 

preserve 
replace cty_pop = cty_pop/1000
collapse (sum) cty_pop , by(fips_state czone) 
bys czone : gen count = _N
bys czone : gen count2 = _n
keep if count == count2 
rename fips_state major_fips
keep czone major_fips 
drop if major_fips == . 
duplicates drop 
tempfile major_fips
save "`major_fips'"
restore

collapse (rawsum) annual_avg_estabs (rawsum) annual_avg_emplvl tot_pop (rawsum) total_annual_wages (mean) pr_link_ind (mean) pr_link_all [fw=total] , by(czone year industry_code) 
merge m:1 czone using "`major_fips'"
keep if _merge == 3 
drop _merge 

rename czone pid
egen pid2 = group(pid indus)

tsset pid2 year
tsfill, full

foreach var of varlist major_fips  {
bysort pid2: egen temp = max(`var')
replace `var' = temp
drop temp
}


foreach var of varlist annual_avg_estabs annual_avg_emplvl total_annual_wages {
replace `var' = 0 if `var' == .
replace `var' = `var' / tot_pop
}

// A.4.  Growth rates. Base year: 1995
gen temp = annual_avg_emplvl if year == 1995
bysort pid2: egen base_emp1 = max(temp)
drop temp

gen temp = annual_avg_emplvl * tot_pop if year == 1995
bysort pid2: egen base_pop = max(temp)
drop temp

gen emp_growth = (annual_avg_emplvl - base_emp1)/base_emp1
replace emp_growth = 0 if emp_growth == . 
// Income 
gen inc = total_annual_wages
gen temp = inc if year == 1995
bysort pid2: egen base_inc = max(temp)
drop temp

gen inc_growth = (inc - base_inc)/base_inc
replace inc_growth = 0 if inc_growth == . 


** For now, keep only balanced industries 
bys pid2: gen count = _N
keep if count == 23 
drop count 

drop if base_emp ==0 

sum base_pop if year == 1995, d
gen wgt=base_pop/(r(N) * r(mean))
winsor wgt, p(.01) gen(wgt_w)

tsset pid2 year 
egen ind_st = group(ind major_fips)

sum pr_link_i $pr_wgt if base_emp > 0, d 
replace pr_link_i = pr_link_i/(r(p75)-r(p25))

sum pr_link_al  $pr_wgt if base_emp > 0, d 
replace pr_link_al = pr_link_al/(r(p75)-r(p25))


// B. Graphs with All PR Link 
cd "$output" 
set more off 
xi i.year|pr_link_i  $pra_control, noomit
drop _IyeaXp*1995

winsor base_emp , g(w_emp_base) p(.025)
sum base_emp, d 
destring industry_code, replace
	
**** Main Post/Pre table 	
eststo czone_emp:   reghdfe emp_growth  c.pr_link_i $pra_control2   [aw=wgt] if inrange(year,2004,2008) , a( i.year) cl(pid )
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"	
	
eststo czone_premp:   reghdfe emp_growth  c.pr_link_i $pra_control2   [aw=wgt] if inrange(year,1990,1995) , a( i.year ) cl(pid )
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"	
		
eststo czone_inc:  reghdfe inc_growth c.pr_link_i $pra_control2  [aw=wgt] if inrange(year,2004,2008) , a(  i.year) cl(pid )
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"	
	
eststo czone_princ:	reghdfe inc_growth  c.pr_link_i $pra_control2  [aw=wgt] if inrange(year,1990,1995) , a(  i.year) cl(pid )
	estadd local hasy "Yes"
	estadd local hasind "Yes"
	estadd local hassel "Yes"		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
// A.0. Get link data 
use "$output_NETS/pr_link_est_countyXindustry_d.dta", clear 
gen pr_emp = pr_link*total if industry_cd < 13
bys fips: egen tot_emp = total(total) 
bys fips: egen tot_pr = total(pr_emp)

gen pr_l = tot_pr/tot_emp 
keep pr_l fips
rename  fips pid
rename pr_l pr_link_ind
duplicates drop 
tempfile pr 
save "`pr'" 

use "$output_NETS/pr_link_est_county_d.dta", clear 
rename  fips pid
tempfile pr2 
save "`pr2'" 
	
// A.0. Get IRS data 
use "$irs/irs_income.dta", clear
keep RETURNS GROSSINCOME fips* year 
rename RETURNS emp 
rename GROSSINCOME inc 
keep if year >= 1990
keep if year < 2013

order fips_state fips_county year 
duplicates drop 

// A.3. Fill in gaps
gen pid = 1000*fips_state+fips_county

tsset pid year
tsfill, full
	
tempfile irs_dat
save "`irs_dat'", replace
	
	// A.1. Get BEA data 
use "$bea/personalincome_employment_allyears.dta", clear
keep Employment PersonalIncome2 state_fips county_fips year 
rename Emp emp_BEA 
rename Per inc_BEA 
keep if year >= 1990
keep if year < 2013

rename state_fips fips_state 
rename county_fips fips_county
order fips_state fips_county year 

// A.3. Fill in gaps
gen pid = 1000*fips_state+fips_county

tsset pid year
tsfill, full

merge 1:1 pid year using "`irs_dat'"	
drop _merge

// A.6. Merge in pr link 
merge m:1 pid using "`pr'"
keep if _merg == 3
drop _merge 

merge m:1 pid using "`pr2'"
drop if year == . 
drop if pr_link == . 
drop _merge 
rename pr_link pr_link_all


merge m:1 pid year using "$additional/county population estimates/county_pop_ests.dta"
keep if _merge == 3
drop _merge
	
// A.7.  Growth rates. Base year: 1995

gen temp = emp if year == 1995
	bysort pid: egen base_e = max(temp)
	drop temp
	
foreach var of varlist inc emp inc_BEA emp_BEA { 
	replace `var' = `var' / tot_pop
}	

foreach var of varlist inc emp inc_BEA emp_BEA { 
	gen temp = `var' if year == 1995
	bysort pid: egen base_`var' = max(temp)
	drop temp

	gen `var'_growth = (`var' - base_`var')/base_`var'
	replace `var'_growth = 0 if `var'_growth == . 
}	
	

** For now, keep only balanced industries . Matt: can you fix it so we don't have to do this? 


sum pr_link_i  if base_e > 0, d 
replace pr_link_i = pr_link_i/(r(p75)-r(p25))

sum pr_link_al if base_e > 0, d 
replace pr_link_al = pr_link_al/(r(p75)-r(p25))


drop if base_e < 10000
**** Main Post/Pre table 
	
eststo emp_2IRS:   reghdfe emp_growth  c.pr_link_i $pra_control2    [aw=base_e] if inrange(year,2004,2008) , a(i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"	
	
eststo premp_2IRS:   reghdfe emp_growth  c.pr_link_i $pra_control2    [aw=base_e] if inrange(year,1990,1995) , a(i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"	
	
eststo inc_2IRS:  reghdfe inc_growth c.pr_link_i $pra_control2  [aw=base_e] if inrange(year,2004,2008) , a(i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"	
	
eststo princ_2IRS:	reghdfe inc_growth  c.pr_link_i $pra_control2    [aw=base_e] if inrange(year,1990,1995) , a(i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"		
	
eststo emp_2:   reghdfe emp_BEA_growth  c.pr_link_i $pra_control2    [aw=base_e] if inrange(year,2004,2008) , a(i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"	
	
eststo premp_2:   reghdfe emp_BEA_growth  c.pr_link_i  $pra_control2   [aw=base_e] if inrange(year,1990,1995) , a(i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"	
	
eststo inc_2:  reghdfe inc_BEA_growth c.pr_link_i $pra_control2  [aw=base_e] if inrange(year,2004,2008) , a(i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"	
	
eststo princ_2:	reghdfe inc_BEA_growth  c.pr_link_i $pra_control2    [aw=base_e] if inrange(year,1990,1995) , a(i.year) cl(fips_state  )
	estadd local hasy "Yes"
	estadd local hassel "Yes"		
	


// Outputting a single table with base panel on top, pr panel on bottom 
local list "emp_2IRS emp_2 county_emp  conspuma_emp czone_emp inc_2IRS inc_2 county_inc  conspuma_inc czone_inc"
local listpr " premp_2IRS premp_2 county_premp conspuma_premp czone_premp princ_2IRS princ_2 county_princ conspuma_princ czone_princ"

esttab `list' using "$output/Tables/table_pre_post_long_pc.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons nonum posthead(" & \multicolumn{5}{c}{Employment Growth}& \multicolumn{5}{c}{Income Growth} \\ \cmidrule(lr){2-6} \cmidrule(lr){7-11}") ///
prefoot("\textbf{Geography} & \multicolumn{3}{c}{County}& Conspuma & C-Zone & \multicolumn{3}{c}{County}& Conspuma & C-Zone \\ \cmidrule(lr){2-4} \cmidrule(lr){7-9}") ///
postfoot("\textbf{Data Source} & IRS & BEA & QCEW  & QCEW & QCEW & IRS & BEA & QCEW  & QCEW  & QCEW   \\ ") ///


esttab `list'  using "$output/Tables/table_pre_post_long_pc.tex", keep(pr_link_ind )  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s() noobs /// 
coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 " pr_link_all "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{1}{l}{\textbf{Years: 2004-2008}}") postfoot("\hline") append sty(tex)

esttab `listpr' using "$output/Tables/table_pre_post_long_pc.tex", keep(pr_link_ind)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s()  /// 
coeflabel(pr_link_ind "\hspace{1em}Exposure to Section 936 ") ///
preh("\multicolumn{1}{l}{\textbf{Years: 1990-1995}}") noobs scalars(  "hasy Year Fixed Effects") label /// 
 prefoot("\hline") ///
postfoot( ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex)	
	
	