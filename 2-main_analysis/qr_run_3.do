// Run first QR regressions 

** For JC Laptop
global dropbox "/Users/jcsuarez/Dropbox (JC's Data Emporium)/RESEARCH/Puerto Rico"
** For JC on Work drive in Office 
*global dropbox "/Volumes/DropboxOffice/DropboxOffice/Dropbox (JC's Data Emporium)/RESEARCH/Puerto Rico"
global analysis 	"$dropbox/Programs"
global output 		"$dropbox/Programs/output"

use "$output/qr_data" , clear 
 
do "$analysis/2-main_analysis/myqr.ado"

local reps = 250

local spec "i.year i.indust i.fips_st"

eststo qreg3: bootstrap pr_link_ind=e(pr_link_ind), reps(`reps') nowarn nodrop cluster(fips_state) ///
idcluster(id_cluster) group(pid2) saving("$output/myqr_res_3", replace) : myqr ,  levelb(.5) spec(`spec')

est save  "$output/qr_est_3"  , replace
