

//update 03-05-2018: Dan added aweights


use "$data/data_additional/St Tax Structure 05_10_2017/state_taxes_analysis.dta", clear
keep if year == 1990 
keep fips salestax propertytax corporate_rate GDP rev_totaltaxes rec_val rdcred rdcred trainingsubsidy jobcreationcred corpinctax totalincentives investment_credit 
rename fips statefips
tempfile incentives 
save "`incentives'"


use "$data/data_additional/Misallocation 5_10_2017/misallocation_paneldata.dta", clear
keep if year == 1990 
keep income_rate_avg fipstate
rename fips statefips
tempfile incentives2 
save "`incentives2'"

* 06/2016
* Merge NTES Link Data and QCEW Outcome Data 
* Runs Event-Study Analyses 

clear all
set more off

/*
******* Set paths 
* For JC 
global dropbox "/Users/jcsuarez/Dropbox (JC's Data Emporium)/RESEARCH/ra tasks"
* For Matt 
*global dropbox "/Users/mattpanhans/Dropbox (JC's Data Emporium)"

**** Relative Paths 
global linkdata "$dropbox/matt ra work/Puerto Rico/output/base_1995"
global qcewdata "$dropbox/matt ra work/Puerto Rico/QCEW"     
global output   "$dropbox/matt ra work/Puerto Rico/QCEW/output"
global cross 	"$dropbox/../completed projects/Local_Econ_Corp_Tax/Data/Regional Crosswalk Data/Conspuma2CTY"
 */
 
// First Define Command for ES 
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(dash))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Estimate") label(2 "`level'% CI")) ///
	ytitle("`yti'") ///
	note("`note'") ylab("`ylab'")
graph export "`outname'.pdf", replace	

end 


// A - Merge Data for Analysis 
// A.0. Get regional crosswalk read 
use "$xwalk/cw_cty_czone.dta", clear
rename cty_fips pid 
duplicates drop 
tempfile cross
save "`cross'"

// A.1. Get QCEW data 
use "$qcewdata/extract_qcew_1990_2012_naic3.dta", clear
keep if strlen(industry_code) == 3
drop if industry_code == "101" | industry_code == "102"

order fips_state fips_county year industry_code
drop if fips_county == 0 & fips_state != 72
drop if fips_state == 72 & fips_county != 0
keep if own_code == 5		// 0-total covered; 1 Federal; 2 State; 3 Local; 4 Intl Govt; 5 Private.
keep fips_state fips_county year industry_code annual_avg_estabs annual_avg_emplvl total_annual_wages
drop if fips_state == 78

collapse (sum) annual_avg_estabs (sum) annual_avg_emplvl (sum) total_annual_wages, by(fips_state fips_county year industry_code)
// A.3. Fill in gaps
gen pid = 1000*fips_state+fips_county

// A.6. Merge in pr link 
merge m:1 pid using "$additional/county_shocks"
drop if _merge != 3
drop _merge

rename prl_all pr_link_all
rename prl_base pr_link_ind

egen pid2 = group(pid indus)

tsset pid2 year
tsfill, full

foreach var of varlist fips_state  {
bysort pid2: egen temp = max(`var')
replace `var' = temp
drop temp
}

foreach var of varlist annual_avg_estabs annual_avg_emplvl total_annual_wages {
replace `var' = 0 if `var' == .
}

// A.4.  Growth rates. Base year: 1995
gen temp = annual_avg_emplvl if year == 1995
bysort pid2: egen base_emp = max(temp)
drop temp

gen emp_growth = (annual_avg_emplvl - base_emp)/base
replace emp_growth = 0 if emp_growth == . 


// Income 
gen inc = total_annual_wages/annual_avg_emplvl
gen temp = inc if year == 1995
bysort pid2: egen base_inc = max(temp)
drop temp

gen inc_growth = (inc - base_inc)/base_inc
replace inc_growth = 0 if inc_growth == . 

// Estabs 
gen estab = annual_avg_estabs
gen temp = estab if year == 1995
bysort pid2: egen base_estab = max(temp)
drop temp

gen estab_growth = (estab - base_estab)/base_estab
replace estab_growth = 0 if estab_growth == . 


// A.5. Define PR Growth Data

bys pid2: gen count = _N
keep if count == 23 
drop count 

// Add demographics and other shocks 
rename fips_county temp_fips_county
gen fips_county = pid 
merge m:1 fips_county using "$additional/county_level_data_all.dta"
drop if _merge == 2 
drop _merge 

merge m:1 statefips  using "`incentives'"
drop if _merge == 2
drop _merge 

merge m:1 statefips using "`incentives2'"
drop if _merge == 2
drop _merge 

** Add nafta variables 
merge m:1 fips_county using "$output/import_hakobyanlaren_nafta.dta"
drop if _merge == 2 
drop _merge 

/*
**** Merge in share of firm size dist 
merge m:1 pid year using "$output/CBP_firm_size_dist"
drop if _merge == 2
drop _merge 
*/

** Keeping only relevant years to make this not crash
keep if inlist(year,1995,2004,2005,2006,2007,2008)
/*
foreach var of varlist n1_4-n250p { 
	sum `var', d
	replace `var' = `var'/(r(p75)-r(p25))
	
	gen temp = `var' if year == 1995
	bysort pid2: egen base_`var' = max(temp)
	drop temp

	gen `var'_growth = (`var' - base_`var')/base_`var'
	replace `var'_growth = 0 if `var'_growth == . 
}
*/

*gen ln_pr = ln(1+pr_link_a) 
*egen std_ln = std(ln_pr)
*winsor std_ln , g(w_std_ln) p(.01) 

bys statefips  : egen state_pop = total(population )
replace rev_totaltaxes = rev_tot/state_pop
replace capital_stock = capital_stock/state_pop

egen std_total = std(totalincentives )
egen std_job = std(jobcreationcred )
egen std_train = std(trainingsubsidy )
egen std_mw = std(realmw  )
egen std_rtw = std(rgtowork  )
egen std_rd = std(rdcred )
egen std_ic = std(investment_credit )
egen std_corp = std(corporate_rate  )
egen std_ptax = std(income_rate_avg  )
egen std_prop = std(propertytax )
egen std_sales = std(salestax )
*egen std_capital = std(capital_stock )
egen std_trade = std(d_tradeusch_pw )
egen std_rev = std(rev_totaltaxes )
egen std_routine = std(l_sh_routine33a )
egen std_nafta = std(locdt_noag)
*egen std_prl_pharm = std(prl_pharm)
egen sd_pharma = std(pharma)
egen sd_fake = std(fakel_base)

/*
egen fs_std_n1 = std( n5_9_growth )
egen fs_std_n2 = std( n10_19_growth ) 
egen fs_std_n3 = std( n20_49_growth )
egen fs_std_n4 = std( n50_99_growth )
egen fs_std_n5 = std( n100_249_growth ) 
egen fs_std_n6 = std( n250p_growth )
*/
/*
egen std_n1 = std(n1_4)
egen std_n2 = std(n20_49)
egen std_n3 = std(n250p)
*/

foreach var of varlist std_* { 
	winsor `var', g(w_`var') p(.01)
	replace `var' = w_`var'
}

// Added aweights 3-5-2018 ***************

sum base_emp, d
gen wgt=base_emp/(r(N) * r(mean))
winsor wgt, p(.01) gen(wgt_w)

sum pr_link_i $pr_wgt if base_emp > 0, d 
replace pr_link_i = pr_link_i/(r(p75)-r(p25))

sum pr_link_all $pr_wgt if base_emp > 0, d 
replace pr_link_all = pr_link_al/(r(p75)-r(p25))

sum pr_link_ind $pr_wgt if inrange(year,2004,2008), d
gen cen_pr_link_i = pr_link_i - r(mean) 

sum pr_link_all $pr_wgt if inrange(year,2004,2008), d 
gen cen_pr_link_a = pr_link_all - r(mean)

destring industry_code, replace

** Robustness tables 
est clear 
eststo rob_0: reghdfe emp_growth cen_pr_link_i $pra_control_rob [aw=wgt] if inrange(year,2004,2008) ,  a( i.year  i.industr) cl(fips_state  i.industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"
foreach var of varlist std_* { 
	eststo rob_`var': reghdfe emp_growth cen_pr_link_i $pra_control_rob `var' [aw=wgt] if inrange(year,2004,2008) ,  a( i.year  i.industr) cl(fips_state  i.industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"

	} 
	
eststo rob_all: reghdfe emp_growth cen_pr_link_i $pra_control_rob std_* [aw=wgt] if inrange(year,2004,2008) ,  a( i.year  i.industr) cl(fips_state  i.industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"	

eststo rob_pharm: reghdfe emp_growth cen_pr_link_i $pra_control_rob sd_pharma [aw=wgt] if inrange(year,2004,2008) ,  a( i.year  i.industr) cl(fips_state  i.industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"	
eststo rob_fake: reghdfe emp_growth cen_pr_link_i $pra_control_rob sd_fake [aw=wgt] if inrange(year,2004,2008) ,  a( i.year  i.industr) cl(fips_state  i.industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"	
	
	
*eststo rob_syfe: reghdfe emp_growth cen_pr_link_i $pra_control_rob [aw=wgt] if inrange(year,2004,2008) ,  a( i.year#fips_state  i.industr) cl(fips_state  i.industr) 
*	estadd local hasy "Yes"
*	estadd local hasind "Yes"		
*	estadd local hassyfe "Yes"	
	/*
eststo rob_fsd: reghdfe emp_growth cen_pr_link_i $pra_control_rob fs_std_n2-fs_std_n6 std_* [aw=wgt] if inrange(year,2004,2008) ,  a( i.year  i.industr) cl(fips_state  i.industr) 
	estadd local hasy "Yes"
	estadd local hasind "Yes"		
	estadd local hasfsd "Yes"	
*/
	estimates restore rob_0
	local beta = _b[cen_pr_link_i]
	local mini = round(`beta'*2, -.03)
	local mini = max(`mini', -0.15)
label var cen_pr_link_i "Robustness of Effect of Exposure to S936 on Employment Growth 2004-2008"
	coefplot (rob_0,m(circle) msiz(medlarge)) (rob_std_total,m(square_hollow) msiz(medlarge)) (rob_std_job,m(diamond) msiz(medlarge)) ///
	(rob_std_train,m(triangle_hollow) msiz(medlarge)) (rob_std_mw,m(smdiamond) msiz(medlarge)) ///
	(rob_std_rtw,m(smtriangle_hollow) msiz(medlarge)) (rob_std_rd,m(triangle) msiz(medlarge)) (rob_std_ic,m(diamond_hollow) msiz(medlarge)) ///
	(rob_std_corp,m(square) msiz(medlarge)) (rob_std_ptax,m(X) msiz(medlarge)) (rob_std_prop,m(smcircle_hollow) msiz(medlarge)) ///
	(rob_std_sales,m(smsquare_hollow) msiz(medlarge)) (rob_std_trade,m(smtriangle) msiz(medlarge)) (rob_std_rev,m(smdiamond_hollow) msiz(medlarge)) ///
	(rob_std_routine,m(smcircle) msiz(medlarge)) (rob_std_nafta,m(circle_hollow) msiz(medlarge)) ///
	(rob_all,m(smsquare) msiz(medlarge)) (rob_pharm,m(plus) msiz(medlarge)) (rob_fake,m(plus) msiz(medlarge))  ///
	,  keep(cen_pr_link_i)  bgcolor(white)  ///
	graphregion(color(white)) vertical ylab(`mini'(.03)0) yline(0) ciopts(recast(rcap)) yline(`beta' , lpattern(dash) lcolor(navy)) ///
	legend(  label(2 "Base") label(4 "Total Incentives") ///
	label(6 "Job Incentives") ///
	label(8 "Training Subsidy") ///
	label(10 "Min Wage") ///
	label(12 "Right to Work") ///
	label(14 "R&D Cred") ///
	label(16 "Investment Cred") ///
	label(18 "Corporate Tax") ///
	label(20 "Personal Tax") ///
	label(22 "Property Tax") ///
	label(24 "Sales Tax Rate") ///
	label(26 "Trade (China)") ///
	label(28 "Tax Revenue ") ///
	label(30 "Routine Jobs") ///	
	label(32 "Trade (Nafta)") ///
	label(34 "All Policy Controls")	///
	label(36 "Exposure to Pharma") ///		
	label(38 "Comparison Firms") cols(4)) ///
	ytitle("Effect of Exposure to S936 on Emp. Growth", margin(small)) scale(.93)
	
	graph export "$output/Graphs/robust_2018_10_23.pdf", replace 
/*
** Separate into two tables 
esttab rob_0 rob_std_total rob_std_job rob_std_train ///
rob_std_mw rob_std_rtw rob_std_rd rob_std_ic rob_std_corp   using "$output/Tables/table_robust_QCEW_2018_10_22_1.tex",   ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s() noobs nogap scalars(  "hasy Year Fixed Effects" "hasind Industry Fixed Effects") /// 
drop($pra_control_rob _cons) ///
coeflabel(cen_pr_link_i "Exposure to Section 936 "  /// 
std_total "Total Incentives (Bartik)"  /// 
std_job "Job Creation Incentive" /// 
std_train "Job Training Subsidy" /// 
std_mw "Real Minimum Wage" /// 
std_rtw "Right to Work" /// 
std_rd "RD Tax Credit" /// 
std_ic "Investment Tax Credit" /// 
std_corp "Corporate Income Tax" /// 
std_ptax "Personal Income Tax" /// 
std_prop "Property Tax"  /// 
std_sales "Sales Tax" /// 
std_capital "Capital Stock" /// 
std_trade "Trade Exposure (ADH)" /// 
std_rev "State Revenue Per Capita" /// 
std_routine "Share Routine Workers (Autor)" ///
pr_link_ind "\hspace{1em}Exposure to Section 936 ") ///
label /// 
 prefoot("\hline") ///
postfoot( ///
"  \hline\hline" ///
"\end{tabular} }" ) replace sty(tex)

/*
esttab rob_0 rob_std_ptax rob_std_prop rob_std_sales /// 
 rob_std_trade rob_std_rev rob_std_routine rob_std_nafta  rob_all rob_fsd ///
 using "$output/Tables/table_robust_QCEW_2.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons nonum ///
postfoot("")
*/

esttab rob_0 rob_std_ptax rob_std_prop rob_std_sales /// 
 rob_std_trade rob_std_rev rob_std_routine rob_std_nafta  rob_all rob_syfe ///
 using "$output/Tables/table_robust_QCEW_2018_10_22_2.tex", drop($pra_control_rob _cons)  ///
cells(b(fmt(3)) se(par) p) mlab(none) coll(none) s() noobs nogap scalars(  "hasy Year Fixed Effects" "hasind Industry Fixed Effects" ///
"hassyfe State-by-Year Fixed Effects") /// 
coeflabel(cen_pr_link_i "Exposure to Section 936 "  /// 
std_total "Total Incentives (Bartik)"  /// 
std_job "Job Creation Incentive" /// 
std_train "Job Training Subsidy" /// 
std_mw "Real Minimum Wage" /// 
std_rtw "Right to Work" /// 
std_rd "RD Tax Credit" /// 
std_ic "Investment Tax Credit" /// 
std_corp "Corporate Income Tax" /// 
std_ptax "Personal Income Tax" /// 
std_prop "Property Tax"  /// 
std_sales "Sales Tax" /// 
std_capital "Capital Stock" /// 
std_trade "Trade Exposure (China)" /// 
std_rev "State Revenue Per Capita" /// 
std_routine "Share Routine Workers" ///
std_nafta "Trade Exposure (Nafta)" /// 
pr_link_ind "\hspace{1em}Exposure to Section 936 ") ///
label /// 
 prefoot("\hline") ///
postfoot( ///
" \hline\hline" ///
"\end{tabular} }" ) replace sty(tex)
