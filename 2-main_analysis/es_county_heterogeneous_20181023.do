* This file estimates separate event studies for a bunch of pr shocks by subsectors
* Author: dan
* date: 20181023

clear all
set more off
snapshot erase _all

// First Define Command for ES data 
capture program drop ES_graph_data
program define ES_graph_data
syntax,  level(real) yti(string) tshifter(real)


qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 


forval i = 1/23 { 
	mat time = (time,`i'-6+1995 + `tshifter')
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci_l es_ci_h time 
svmat es_b 
rename es_b `yti'_es_b
svmat time
rename time `yti'_time
svmat es_ci_l
rename es_ci_l `yti'_es_ci_l
svmat es_ci_h 
rename es_ci_h `yti'_es_ci_h
}

end  
 
// Second Define Command for ES 
capture program drop ES_graph
program define ES_graph
syntax,  level(real) [  yti(string) note(string) outname(string) ylab(string) ]

qui { 
mat b = e(b)
mat V = e(V) 
mat es_b = 0 
mat es_ci_l = 0 
mat es_ci_h = 0 
mat time = 0 

// for 95% CI 
if `level' == 99 { 
	local z= 2.6  
}
if `level' == 95 { 
	local z= 1.96  
}
// for 90% CI 
if `level' == 90 { 
	local z= 1.645 
} 
	
forval i = 1/23 { 
	mat time = (time,`i'-6+1995)
	mat es_b = (es_b, b[1,`i'])
	mat es_ci_l = (es_ci_l, b[1,`i']-`z'*sqrt(V[`i',`i'])   )
	mat es_ci_h = (es_ci_h, b[1,`i']+`z'*sqrt(V[`i',`i'])   )		
	}
mat es_b =  (es_b[1,2..6],0,es_b[1,7...])'
mat es_ci_l = (es_ci_l[1,2..6],0,es_ci_l[1,7...])'  
mat es_ci_h = (es_ci_h[1,2..6],0,es_ci_h[1,7...])' 
mat time = time[1,2...]' 

capture: drop es_b es_ci* time 
svmat es_b 
svmat time
svmat es_ci_l
svmat es_ci_h 
}

twoway (scatter es_b time, yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(dash))) ///
    (rcap es_ci_h es_ci_l time,  lcolor(navy) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Estimate") label(2 "`level'% CI")) ///
	ytitle("`yti'") ///
	note("`note'") ylab("`ylab'")
graph export "`outname'.pdf", replace	

end 


// A - Merge Data for Analysis 
// A.0. Get regional crosswalk read 
use "$xwalk/cw_cty_czone.dta", clear
rename cty_fips pid 
duplicates drop 
tempfile cross
save "`cross'"

// A.1. Get QCEW data 
use "$qcewdata/extract_qcew_1990_2012_naic3.dta", clear
keep if strlen(industry_code) == 3
drop if industry_code == "101" | industry_code == "102"

order fips_state fips_county year industry_code
drop if fips_county == 0 & fips_state != 72
drop if fips_state == 72 & fips_county != 0
keep if own_code == 5		// 0-total covered; 1 Federal; 2 State; 3 Local; 4 Intl Govt; 5 Private.
keep fips_state fips_county year industry_code annual_avg_estabs annual_avg_emplvl total_annual_wages
drop if fips_state == 78
collapse (sum) annual_avg_estabs (sum) annual_avg_emplvl (sum) total_annual_wages, by(fips_state fips_county year industry_code)

// A.3. Fill in gaps
gen pid = 1000*fips_state+fips_county

// A.6. Merge in pr link 
merge m:1 pid using "$additional/county_shocks"
keep if _merg == 3
drop _merge 

drop if year == . 

egen pid2 = group(pid indus)

tsset pid2 year
tsfill, full

foreach var of varlist fips_state  {
bysort pid2: egen temp = max(`var')
replace `var' = temp
drop temp
}

foreach var of varlist annual_avg_estabs annual_avg_emplvl total_annual_wages {
replace `var' = 0 if `var' == .
}

// A.4.  Growth rates. Base year: 1995
gen temp = annual_avg_emplvl if year == 1995
bysort pid2: egen base_emp = max(temp)
drop temp

gen emp_growth = (annual_avg_emplvl - base_emp)/base
replace emp_growth = 0 if emp_growth == . 

// Income 
gen inc = total_annual_wages
* /annual_avg_emplvl
gen temp = inc if year == 1995
bysort pid2: egen base_inc = max(temp)
drop temp

gen inc_growth = (inc - base_inc)/base_inc
replace inc_growth = 0 if inc_growth == . 

// Estabs 
gen estab = annual_avg_estabs
gen temp = estab if year == 1995
bysort pid2: egen base_estab = max(temp)
drop temp

gen estab_growth = (estab - base_estab)/base_estab
replace estab_growth = 0 if estab_growth == . 

** For now, keep only balanced industries.
bys pid2: gen count = _N
keep if count == 23 
drop count 

drop if base_emp ==0 

sum base_emp if year == 1995, d
gen wgt=base_emp/(r(N) * r(mean))
winsor wgt, p(.01) gen(wgt_w)

*normalizing all prl and fakel variables by an IQR for the baseline pr_link_ind variable
sum prl_base $pr_wgt if base_emp > 0, d
foreach Q of var prl_* fakel_* { 
replace `Q' = `Q'/(r(p75)-r(p25))
}

tsset pid2 year 
egen ind_st = group(ind fips_state)

// B. Regressions
cd "$output" 
set more off 

destring industry_code, replace
est clear 
set more off

foreach Q of var prl_* {
xi i.year|`Q'  $pra_control, noomit
drop _IyeaXp*1995

eststo emp_`Q':  reghdfe emp_growth _IyeaX* [aw=wgt]  , a( i.year#i.industr i.pid) cl(fips_state indust )
	estadd local hasiy "Yes"
    estadd local hascty "Yes"

ES_graph_data , level(95) yti("s_`Q'") tshifter(0)
}

* Graph output of the specifications
 twoway (line s_prl_base_es_b s_prl_base_time, lcolor(navy) yline(0 , lcolor(black)) xline(1995, lcolor(black) lpattern(solid)))  ///
		(rcap s_prl_base_es_ci_l s_prl_base_es_ci_h s_prl_base_time,  lcolor(navy) lpattern(dash))  ///
		(line s_prl_food_es_b s_prl_base_time, lcolor(orange) lpattern(solid))      ///
		(rcap s_prl_food_es_ci_l s_prl_food_es_ci_h s_prl_base_time,  lcolor(orange) lpattern(dash))   /// 
		 (line s_prl_cloth_es_b s_prl_base_time, lcolor(orange) lpattern(solid))     ///
		 (rcap s_prl_cloth_es_ci_l s_prl_cloth_es_ci_h s_prl_base_time,  lcolor(orange) lpattern(dash))    ///
		 (line s_prl_chem_es_b s_prl_base_time, lcolor(green) lpattern(solid))    ///
		 (rcap s_prl_chem_es_ci_l s_prl_chem_es_ci_h s_prl_base_time,  lcolor(green) lpattern(dash))    ///
		(line s_prl_pharm_es_b s_prl_base_time, lcolor(red) lpattern(solid))   ///
		(rcap s_prl_pharm_es_ci_l s_prl_pharm_es_ci_h s_prl_base_time,  lcolor(red) lpattern(dash))   ///
		(line s_prl_elec_es_b s_prl_base_time, lcolor(purple) lpattern(solid))    ///
		(rcap s_prl_elec_es_ci_l s_prl_elec_es_ci_h s_prl_base_time,  lcolor(purple) lpattern(dash)) ///
		(line s_prl_other_es_b s_prl_base_time, lcolor(black) lpattern(solid))    ///
		(rcap s_prl_other_es_ci_l s_prl_other_es_ci_h s_prl_base_time,  lcolor(black) lpattern(dash)) ///
	, plotregion(fcolor(white) lcolor(white)) ///
	graphregion(fcolor(white) lcolor(white)) ///
	xtitle("Year") xlab(1990(5)2012) ///
	legend(label(1 "Baseline") ///
	label(3 "Food, Bev, Tobacco")   ///
	label(5 "Clothing")  ///
	label(11 "Electronics")  ///
	label(7 "Chemicals")  ///
	label(9 "Pharma")  ///
	label(13 "Other Manufacturing")  ///
	order(1 3 5 7 9 11 13)) ///
	ytitle("Percent Employment Growth") 
graph export "$output/fig_es_by_industry.pdf", replace	


* outputting table
local vars "prl_base prl_food prl_cloth prl_chem prl_pharm prl_elec prl_other"	
	
foreach Q of var `vars' {
local var = "`var' emp_`Q'"
}
	
esttab `var' using "$output/Tables/table_ES_QCEW_industry.tex", drop(*) stats() ///
b(3) se par label  ///
noobs nogap nomtitle tex replace nocons ///
postfoot("& Base & Food & Clothing & Chem & Pharma & Elec & Other \\") nonum posthead("")


esttab `var' using "$output/Tables/table_ES_QCEW_industry.tex",  preh("") ///
 b(3) se par mlab(none) coll(none) s() noobs nogap keep(_IyeaXprl_*)  /// 
coeflabel(_IyeaXprl_1990 "\hspace{1em}X 1990 " /// 
_IyeaXprl_1991 "\hspace{1em}X 1991 " /// 
_IyeaXprl_1992 "\hspace{1em}X 1992 " /// 
_IyeaXprl_1993 "\hspace{1em}X 1993 " /// 
_IyeaXprl_1994 "\hspace{1em}X 1994 " /// 
_IyeaXprl_1996 "\hline \hspace{1em}X 1996 " /// 
_IyeaXprl_1997 "\hspace{1em}X 1997 " /// 
_IyeaXprl_1998 "\hspace{1em}X 1998 " /// 
_IyeaXprl_1999 "\hspace{1em}X 1999 " /// 
_IyeaXprl_2000 "\hspace{1em}X 2000 " /// 
_IyeaXprl_2001 "\hspace{1em}X 2001 " /// 
_IyeaXprl_2002 "\hspace{1em}X 2002 " /// 
_IyeaXprl_2003 "\hspace{1em}X 2003 " /// 
_IyeaXprl_2004 "\hspace{1em}X 2004 " /// 
_IyeaXprl_2005 "\hspace{1em}X 2005 " /// 
_IyeaXprl_2006 "\hspace{1em}X 2006 " /// 
_IyeaXprl_2007 "\hspace{1em}X 2007 " /// 
_IyeaXprl_2008 "\hspace{1em}X 2008 " /// 
_IyeaXprl_2009 "\hspace{1em}X 2009 " /// 
_IyeaXprl_2010 "\hspace{1em}X 2010 " /// 
_IyeaXprl_2011 "\hspace{1em}X 2011 " /// 
_IyeaXprl_2012 "\hspace{1em}X 2012 " )   scalars(  "hasiy Industry-by-Year Fixed Effects" ///
 "hascty County Fixed Effects" ) label /// 
 prefoot("\hline") ///
postfoot( ///
" \hline\hline" ///
"\end{tabular} }" ) append sty(tex)
